//
//  UILabel+Boldify.h
//  whatsalon
//
//  Created by Graham Connolly on 17/07/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

/*!
 @header UILabel+Boldify.h
  
 @brief This is the header for UILabel+Boldify.
  
 This file contains the header for UILabel+Boldify for bolding a subsection of a UILabel.
  
 @author Hugues Bernet-Rollande on 1/7/14.
 @copyright  2014 Yerdle. All rights reserved.
 @version   -
 */
#import <UIKit/UIKit.h>

@interface UILabel (Boldify)

/*! @brief bolds a substring of a UILabel.
    @param substring the substring to be put in bold.
 */
- (void) boldSubstring: (NSString*) substring;

/*! @brief bolds a substring of a UILabel.
    @param range bolds a substring based on an NSRange using a starting point and a count.
 
 */
- (void) boldRange: (NSRange) range;
@end
