//
//  BookTheLookViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 30/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "BookTheLookViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GetDirectionsViewController.h"
#import "Salon.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"

@interface BookTheLookViewController ()<UIViewControllerTransitioningDelegate>
- (IBAction)cancel:(id)sender;
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *descriptionView;
@property (strong, nonatomic) IBOutlet UILabel *salonName;
@property (strong, nonatomic) IBOutlet UIImageView *salonStarRating;
@property (strong, nonatomic) IBOutlet UILabel *stylistName;
@property (strong, nonatomic) IBOutlet UIImageView *stylistStarRating;
@property (strong, nonatomic) IBOutlet UILabel *addressLine1;
@property (strong, nonatomic) IBOutlet UILabel *addressLine2;
@property (strong, nonatomic) IBOutlet UILabel *addressLine3;
@property (strong, nonatomic) IBOutlet UILabel *serviceType;

@property (strong, nonatomic) IBOutlet UIView *viewForDescription;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancel;

@end

@implementation BookTheLookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //make navigation bar completely transparent
    [self.navBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    self.navBar.shadowImage = [UIImage new];
    self.navBar.translucent = YES;
    self.navBar.backgroundColor = [UIColor clearColor];
    self.navBar.tintColor=[UIColor whiteColor];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navBar.titleTextAttributes = textAttributes;
    self.descriptionView.textColor=[UIColor whiteColor];

    //[WSCore addBottomLine:self.navBar :kCellLinesColour];
    //[WSCore addBottomLine:self.imageView :kCellLinesColour];
    //[WSCore addBottomLine:self.viewForDescription :kCellLinesColour];
    
    [WSCore addBottomIndentedLine:self.viewForService :kCellLinesColour];
    [WSCore addTopLine:self.addressView :kCellLinesColour :0.5f];
    [WSCore addTopLine:self.viewForService :kCellLinesColour :0.5f];
    
    self.salonName.text = self.inspire.salon_name;
    self.salonName.adjustsFontSizeToFitWidth=YES;
    self.stylistName.text = self.inspire.stylist_name;
    self.addressLine1.text = self.inspire.salon_address_1;
    self.addressLine2.text = self.inspire.salon_address_2;
    self.addressLine3.text = self.inspire.salon_address_3;
    
    self.viewForService.backgroundColor=[UIColor clearColor];
    self.viewForTime.backgroundColor=[UIColor clearColor];
    self.addressView.backgroundColor=[UIColor clearColor];
 

    
    self.serviceType.text = @"Blow dry";
    
    self.imageView.contentMode=UIViewContentModeScaleAspectFill;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:self.inspire.image_name]
                   placeholderImage:[UIImage imageNamed:@"empty_cell"]];
    
    self.findAppointment.backgroundColor = kWhatSalonBlue;
    
    self.descriptionView.text = [NSString stringWithFormat:@"Created by %@ at %@.",self.inspire.stylist_name,self.inspire.salon_name];
    NSLog(@"salon rating %@",self.inspire.salon_rating);
    
    
    int salonRating = [self.inspire.salon_rating intValue];

    
    switch (salonRating) {
        case 5:{
            UIImage *templateImage = [[UIImage imageNamed:@"white_five_star_rating"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            self.salonStarRating.image = templateImage;
        }
            break;
        case 4:{
            UIImage *templateImage = [[UIImage imageNamed:@"white_four_star_rating"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            self.salonStarRating.image = templateImage;
        }
            
            break;
        case 3:{
            UIImage * templateImage = [[UIImage imageNamed:@"white_three_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            self.salonStarRating.image = templateImage;

        }
            
            break;
        case 2:{
            UIImage * templateImage = [[UIImage imageNamed:@"white_two_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            self.salonStarRating.image = templateImage;
        }
            
            break;
        case 1:{
            UIImage * templateImage = [[UIImage imageNamed:@"white_one_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            self.salonStarRating.image = templateImage;
        }
            
            break;

        default:{
            UIImage * templateImage = [[UIImage imageNamed:@"white_no_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            self.salonStarRating.image = templateImage;

        }
            break;
    }
  
    UIImage * stylistTemplate = [[UIImage imageNamed:@"white_no_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.stylistStarRating.image = stylistTemplate;
    self.salonStarRating.tintColor=kWhatSalonBlue;
    self.stylistStarRating.tintColor=kWhatSalonBlue;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)selectTime:(id)sender {
}
- (IBAction)findAppointment:(id)sender {
}
- (IBAction)getDirections:(id)sender {
    GetDirectionsViewController *viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
    viewController.modalPresentationStyle = UIModalPresentationCustom;
    viewController.transitioningDelegate = self;
    Salon *salon = [[Salon alloc] init];
    salon.salon_name = self.inspire.salon_name;
    salon.salon_lat = [self.inspire.salon_lat doubleValue];
    salon.salon_long = [self.inspire.salon_lon doubleValue];
    viewController.salonMapData=salon;
    [self presentViewController:viewController animated:YES completion:NULL];
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return [[DismissDetailTransition alloc] init];
}

@end
