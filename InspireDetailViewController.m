//
//  InspireDetailViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "InspireDetailViewController.h"
#import "InspireImage.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "User.h"
#import "SocialShareViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "BookTheLookViewController.h"

@interface InspireDetailViewController ()<UIScrollViewDelegate,UIViewControllerTransitioningDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) IBOutlet UIPageControl * pageControl;

@property (nonatomic,strong) NSMutableArray* pageViews;

-(void)loadVisiblePages;
-(void)loadPage: (NSInteger)page;
-(void)purgePage: (NSInteger)page;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *bookTheLook;

@property (nonatomic) UIButton * favButton;

@end

@implementation InspireDetailViewController

#pragma mark - View Controllers Life Cycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
    self.view.backgroundColor=[UIColor colorWithWhite:0.0f alpha:0.85f];
    self.scrollView.delegate=self;
    
    self.pageImages = [[NSArray alloc] initWithArray:self.imagesToDisplay];
    self.pageImages = self.imagesToDisplay;
    
    NSInteger pageCount = self.pageImages.count;
    
    UIPageControl * pageController = [[UIPageControl alloc] initWithFrame:CGRectMake(20, 20, 200, 100)];
    self.pageControl = pageController;
    self.pageControl.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.pageControl];
    
    // 2
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    // 3
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
    self.pageControl.hidden=YES;
    
    self.favImageView.hidden=YES;
    self.backButton.hidden=YES;
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToDismiss)];
    tap.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tap];
    
    UISwipeGestureRecognizer * swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDown)];
    swipeDown.direction=UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDown];
    
    UISwipeGestureRecognizer * swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUp)];
    swipeUp.direction=UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUp];
    
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Set up the content size of the scroll view
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.pageImages.count, pagesScrollViewSize.height);
    
    // Set initial content offset of scrollview
    self.scrollView.contentOffset = CGPointMake(pagesScrollViewSize.width * self.listIndex, self.scrollView.contentOffset.y);
    
    // Load the initial set of pages that are on screen
    [self loadVisiblePages];
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    
}


#pragma mark - UITapGesture Recognizer methods
-(void)swipeDown{
    
    [UIView transitionWithView:self.optionsCaptionsView duration:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.optionsCaptionsView.frame = CGRectMake(self.optionsCaptionsView.frame.origin.x, self.view.frame.size.height, self.optionsCaptionsView.frame.size.width, self.optionsCaptionsView.frame.size.height);
    } completion:nil];
}

-(void)swipeUp{
    [UIView transitionWithView:self.optionsCaptionsView duration:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.optionsCaptionsView.frame = CGRectMake(self.optionsCaptionsView.frame.origin.x, self.view.frame.size.height-100, self.optionsCaptionsView.frame.size.width, self.optionsCaptionsView.frame.size.height);
    } completion:nil];
    
}

-(void)tapToDismiss{
    if (!self.isFromFavPage) {
        [self postNotificationScrollToIndex:[NSString stringWithFormat:@"%ld",(long)self.pageControl.currentPage]];
    }
    else{
        [self postNotificationScrollToIndexFav:[NSString stringWithFormat:@"%ld",(long)self.pageControl.currentPage]];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIScrollViewDelegate Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    //load the pages that are no on screen
    [self loadVisiblePages];
}

#pragma mark - Lazy loading methods
-(void)loadVisiblePages{
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
	// Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        
        [self loadPage:i];
    }
    
	// Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.pageImages.count; i++) {
        [self purgePage:i];
    }
    
}

-(void)purgePage:(NSInteger)page{
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}
-(void)loadPage:(NSInteger)page{
    
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // 1
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        // 2
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        // 3
        
        UIView * view = [[UIView alloc] initWithFrame:frame];
        InspireImage * insp = [self.pageImages objectAtIndex:page];
        UIImageView * newPageView = [[UIImageView alloc] init];
        [newPageView sd_setImageWithURL:[NSURL URLWithString:insp.image_name]
                       placeholderImage:[UIImage imageNamed:@"empty_cell"]];
        newPageView.contentMode = UIViewContentModeScaleAspectFit;
        newPageView.frame = CGRectInset(view.bounds, 10, 0);
        [view addSubview:newPageView];
        
        UIView * viewHolder = [[UIView alloc] initWithFrame:CGRectMake(0, view.frame.size.height-100, self.view.frame.size.width, 100)];
        viewHolder.alpha=0.8;
        viewHolder.backgroundColor=[UIColor blackColor];
        [view addSubview:viewHolder];
        
        UILabel * infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 280, 44)];
        infoLabel.textColor=[UIColor whiteColor];
        infoLabel.text = [NSString stringWithFormat:@"Created by %@ at %@.",insp.stylist_name,insp.salon_name];
        infoLabel.font = [UIFont systemFontOfSize: 11.0f];
        infoLabel.textAlignment = NSTextAlignmentCenter;
        infoLabel.adjustsFontSizeToFitWidth=YES;
        [viewHolder addSubview:infoLabel];
        
        self.favButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 60, 105, 40)];
        [self.favButton addTarget:self action:@selector(favButton:) forControlEvents:UIControlEventTouchUpInside];
        self.favButton.accessibilityHint = insp.style_id;
        self.favButton.tag=[insp.style_id intValue];
        if (insp.is_favourite) {
            [self.favButton setImage:[UIImage imageNamed:@"White_Heart_Solid"] forState:UIControlStateNormal];
        }
        else{
            [self.favButton setImage:[UIImage imageNamed:@"White_Heart"] forState:UIControlStateNormal];
        }
        
        [viewHolder addSubview:self.favButton];
        
        UIButton * shareButton = [[UIButton alloc] initWithFrame:CGRectMake(215, 60, 105, 40)];
        [shareButton setImage:[UIImage imageNamed:@"Share"] forState:UIControlStateNormal];
        [shareButton addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
        [viewHolder addSubview:shareButton];
        
        if (insp.isBookable) {
            UIButton * bookButton = [[UIButton alloc] initWithFrame:CGRectMake(105, 60, 110, 40)];
            bookButton.tag=[insp.salon_id intValue];
            bookButton.accessibilityHint=insp.style_id;
            [bookButton addTarget:self action:@selector(bookButton:) forControlEvents:UIControlEventTouchUpInside];
            [bookButton setTitle:@"Book The Look" forState:UIControlStateNormal];
            [bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            bookButton.titleLabel.adjustsFontSizeToFitWidth=YES;
            [viewHolder addSubview:bookButton];
        }
        
        
        [self.scrollView addSubview:view];
        
        [self.pageViews replaceObjectAtIndex:page withObject:view];
    }
}

-(void)share: (id)sender{
    SocialShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"socialVC"];
    shareVC.modalPresentationStyle=UIModalPresentationCustom;
    shareVC.transitioningDelegate=self;
    [self presentViewController:shareVC animated:YES completion:nil];
}
-(void)bookButton: (id) sender{
    
    UIButton * bookButton = (UIButton*)sender;
    NSString* styleId = bookButton.accessibilityHint;
    NSLog(@"Style id %@",styleId);
    for (int i=0; i<self.imagesToDisplay.count; i++) {
        InspireImage *insp = [self.imagesToDisplay objectAtIndex:i];
        if (bookButton.tag ==[insp.salon_id intValue] && [styleId isEqualToString:insp.style_id]) {
            NSLog(@"Style id %@ and salon id %@",insp.style_id,insp.salon_id);
            
            BookTheLookViewController * bookTheLook = [self.storyboard instantiateViewControllerWithIdentifier:@"bookTheLookVC"];
            self.modalPresentationStyle=UIModalPresentationCurrentContext;
            bookTheLook.inspire=insp;
            
            [self presentViewController:bookTheLook animated:YES completion:nil];
            return;
        }
    }
}
#pragma mark - Favourite Button
-(void)favButton: (id) sender{
    
    UIButton *temp = (UIButton*) sender;
    NSString * stId= temp.accessibilityHint;
    
    for (int i=0; i<self.imagesToDisplay.count; i++) {
        InspireImage * insp = [self.imagesToDisplay objectAtIndex:i];
        if ([stId isEqualToString:insp.style_id]) {
            
            if (insp.is_favourite) {
                //set to no
                if (temp.tag==[stId intValue]) {
                    [temp setImage:[UIImage imageNamed:@"White_Heart"] forState:UIControlStateNormal];
                    [self unMarkAsFavourite:[stId intValue]];
                    //set favourite bool
                    insp.is_favourite=NO;
                    [self.imagesToDisplay replaceObjectAtIndex:i withObject:insp];
                }
                
            }
            else{
                //set to yes
                if (temp.tag==[stId intValue]) {
                    [temp setImage:[UIImage imageNamed:@"White_Heart_Solid"] forState:UIControlStateNormal];
                    
                    [self markAsFavourite:[stId intValue]];
                    //set favourite bool
                    insp.is_favourite=YES;
                    [self.imagesToDisplay replaceObjectAtIndex:i withObject:insp];
                }
                
                
                
                
            }
            return;
        }
        
    }
    
}

#pragma mark - Marking & Unmarking as favourite methods
-(void)unMarkAsFavourite: (int) inspId{
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_Style_Favourite_URL]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance]fetchAccessToken ]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&style_id=%d",inspId]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&unmark=false"]];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        if (data && dict!=nil) {
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    NSLog(@"Data dict /n %@",dict);
                    if ([dict[@"success"] isEqualToString:@"true"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if (self.isFromFavPage) {
                                
                                [self postNotificationFavRemoveFavourite:[NSString stringWithFormat:@"%d",inspId]];
                            }
                            else{
                                [self postNotificationWithInspireRemoveFavourite:[NSString stringWithFormat:@"%d",inspId]];
                            }
                            
                        });
                        
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oh Oh" message:@"A network error has occurred." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                            [alert show];
                        });
                    }
                    break;
                    
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                        [WSCore showServerErrorAlert];
                    });
                    break;
            }
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        [task resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
    
}

-(void)markAsFavourite: (int) inspId{
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_Style_Favourite_URL]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&style_id=%d",inspId]];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        if (data && dict!=nil) {
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    
                    if ([dict[@"success"] isEqualToString:@"true"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if (self.isFromFavPage) {
                                
                                [self postNotificationFavIsFavourite:[NSString stringWithFormat:@"%d",inspId]];
                            }
                            else{
                                [self postNotificationWithInspireIsFavourite:[NSString stringWithFormat:@"%d",inspId]];
                            }
                            
                            
                            
                        });
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oh Oh" message:@"A network error occured." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                        });
                    }
                    break;
                    
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                        [WSCore showServerErrorAlert];
                    });
                    break;
            }
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        [task resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
    
}

#pragma mark - NSNotificationMethods

- (void)postNotificationWithInspireIsFavourite:(NSString *)inspId //post notification method and logic
{
    NSString *notificationName = @"inspireIsFav";
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:inspId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

- (void)postNotificationWithInspireRemoveFavourite:(NSString *)inspId //post notification method and logic
{
    NSString *notificationName = @"inspireNotFav";
    NSString *key = @"Inspire_id";
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:inspId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

- (void)postNotificationFavIsFavourite:(NSString *)inspId //post notification method and logic
{
    NSString *notificationName = @"favIsFav";
    NSString *key = @"Inspire_id";
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:inspId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

- (void)postNotificationFavRemoveFavourite:(NSString *)inspId //post notification method and logic
{
    NSString *notificationName = @"favNotFav";
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:inspId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

- (void)postNotificationScrollToIndex: (NSString *)index//post notification method and logic
{
    NSString *notificationName = @"scrollToIndex";
    NSString *key = @"index";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:index forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

- (void)postNotificationScrollToIndexFav: (NSString *)index//post notification method and logic
{
    NSString *notificationName = @"scrollToIndexFav";
    NSString *key = @"index";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:index forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}






@end
