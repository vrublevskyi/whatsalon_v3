//
//  InspireCollectionViewCell.m
//  
//
//  Created by Graham Connolly on 16/04/2014.
//
//

#import "InspireCollectionViewCell.h"

@implementation InspireCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
