//
//  InspireDetailViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InspireDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *favImageView;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic,weak) NSString * imageName;
@property (weak, nonatomic) IBOutlet UIImageView *detailImageView;
@property (nonatomic,strong) NSArray * pageImages;//This will hold all the images to display – 1 per page.
@property (nonatomic,strong) NSMutableArray * imagesToDisplay;
@property (nonatomic) NSInteger listIndex;
@property (weak, nonatomic) IBOutlet UIView *optionsCaptionsView;

@property (nonatomic,assign) BOOL isFromFavPage;

@end
