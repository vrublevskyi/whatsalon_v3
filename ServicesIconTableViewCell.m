//
//  ServicesIconTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 24/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "ServicesIconTableViewCell.h"

@implementation ServicesIconTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [WSCore addTopLine:self :[UIColor lightGrayColor] :0.5];
        
    }
    
    return self;
    
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUpCellWithSalonObject: (Salon *) saloObj{
    int i = 0;
    NSMutableArray * array = [NSMutableArray array];
    NSLog(@"*** \n salons categories %@  *** \n",saloObj.salonCategories);
    for(NSString * key in saloObj.salonCategories) {
        NSString* value = [saloObj.salonCategories objectForKey:key];
        NSLog(@"value %@ key %@",value,key);
        if ([value boolValue]) {
            [array addObject:key];
            i++;
        }
    }
    
    CGFloat iconW = 45;
    CGFloat totalIconW = iconW*i;
    CGFloat iconSpacing = (self.bounds.size.width-totalIconW)/(i+1);
    CGFloat previousButtonWidth=0;
    self.clipsToBounds=YES;
    
    NSLog(@"Buttons %d",i);
    CGRect frame=CGRectMake(0, 20, iconW, iconW);
 
   // NSLog(@"\n\n ** all services %@  ** \n\n",[saloObj.salonCategories allKeys]);
    
    NSMutableArray * categories = [NSMutableArray arrayWithObjects:@"Hair",@"Hair Removal",@"Nails",@"Massage",@"Face",@"Body", nil];
    
    for (NSString * cat in categories) {
    
        for(NSString *key in [saloObj.salonCategories allKeys]) {
            UIImageView * image = [[UIImageView alloc] init];
            [self.contentView addSubview:image];
            if ([cat isEqualToString:@"Hair"]&&[key isEqualToString:@"Hair"] && [[saloObj.salonCategories valueForKey:@"Hair"] boolValue]) {
                
                frame.origin.x += previousButtonWidth +iconSpacing;
                previousButtonWidth = frame.size.width;
                image.frame=frame;
                
                image.image = [UIImage imageNamed:@"hair_service_icon"];
                
                
            } else if ([cat isEqualToString:@"Nails"]&&[key isEqualToString:@"Nails"] && [[saloObj.salonCategories valueForKey:@"Nails"] boolValue]) {
                frame.origin.x += previousButtonWidth +iconSpacing;
                previousButtonWidth = frame.size.width;
                image.frame=frame;
                
                image.image=[UIImage imageNamed:@"nail_service_icon"];
                
                
            }
            else if ([cat isEqualToString:@"Massage"]&&[key isEqualToString:@"Massage"] && [[saloObj.salonCategories valueForKey:@"Massage"] boolValue]) {
                frame.origin.x += previousButtonWidth +iconSpacing;
                previousButtonWidth = frame.size.width;
                image.frame=frame;
                
                image.image=[UIImage imageNamed:@"massage_service_icon"];
                
            }
            else  if ([cat isEqualToString:@"Hair Removal"]&&[key isEqualToString:@"Hair Removal"] && [[saloObj.salonCategories valueForKey:@"Hair Removal"] boolValue]) {
                frame.origin.x += previousButtonWidth +iconSpacing;
                previousButtonWidth = frame.size.width;
                image.frame=frame;
                
                image.image=[UIImage imageNamed:@"wax_service_icon"];
                
            }
            else  if ([cat isEqualToString:@"Face"]&&[key isEqualToString:@"Face"] && [[saloObj.salonCategories valueForKey:@"Face"] boolValue]) {
                frame.origin.x += previousButtonWidth +iconSpacing;
                previousButtonWidth = frame.size.width;
                image.frame=frame;
                
                image.image=[UIImage imageNamed:@"face_service_icon"];
                
            }
            else if ([cat isEqualToString:@"Body"]&&[key isEqualToString:@"Body"] && [[saloObj.salonCategories valueForKey:@"Body"] boolValue]) {
                frame.origin.x += previousButtonWidth +iconSpacing;
                previousButtonWidth = frame.size.width;
                image.frame=frame;
                
                NSLog(@"Body Icon");
                image.image=[UIImage imageNamed:@"body_service_icon"];
                NSLog(@"image frame %@", NSStringFromCGRect(image.frame));
            }
            
            image.image = [image.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [image setTintColor:kWhatSalonBlue];
        }

    }
    

}

@end
