//
//  SearchSalonModel.h
//  whatsalon
//
//  Created by Graham Connolly on 27/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SearchSalonsDelegate <NSObject>

@optional
-(void)reloadSearchSalonsData;

@end

@interface SearchSalonModel : NSObject
@property (nonatomic,assign) id<SearchSalonsDelegate> myDelegate;
@property (nonatomic) NSString * loginKey;
@property (nonatomic) NSString * searchTerm;
@property (nonatomic) NSMutableArray * tier1Results;
@property (nonatomic) NSMutableArray *otherTiersResults;
@property (nonatomic) NSMutableArray * allResults;
@property (nonatomic) int totalNumberOfPages;

-(void)load: (NSURL *)url withParams: (NSString *)params;

-(NSMutableArray*)fetchTier1Salons;
-(NSMutableArray*)fetchOtherTierSalons;
-(NSMutableArray *)fetchAllSalons;
-(void)removeSearchSalonObjectsFromArray;

-(int)fetchTotalNumberOfPages;


@end
