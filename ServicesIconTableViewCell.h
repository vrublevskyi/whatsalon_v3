//
//  ServicesIconTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 24/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"

@interface ServicesIconTableViewCell : UITableViewCell

@property (nonatomic) Salon * saloObj;

@property (nonatomic) int numberOfCategories;

-(void)setUpCellWithSalonObject: (Salon *) saloObj;
@end
