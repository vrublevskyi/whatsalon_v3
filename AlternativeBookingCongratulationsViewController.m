//
//  AlternativeBookingCongratulationsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 15/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "AlternativeBookingCongratulationsViewController.h"
#import "UILabel+Boldify.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DismissDetailTransition.h"
#import "UILabel+Boldify.h"
#import <UIImage+ImageEffects.h>
#import "GetDirectionsViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "SocialShareViewController.h"


@interface AlternativeBookingCongratulationsViewController ()<UIViewControllerTransitioningDelegate>


@property (strong, nonatomic) IBOutlet UILabel *bookingDescription;
@property (strong, nonatomic) IBOutlet UILabel *chargeDescription;
@property (strong, nonatomic) IBOutlet UILabel *thankYouLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIButton *reminderButton;
@property (strong, nonatomic) IBOutlet UIButton *callButton;
@property (strong, nonatomic) IBOutlet UIButton *directionsButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *closeButton;
@property (nonatomic,strong) EKEventStore * eventStore;
@property BOOL eventStoreAccessGranted;
@property (weak, nonatomic) IBOutlet UIView *reminderView;
@property (weak, nonatomic) IBOutlet UIDatePicker *reminderDatePicker;
@property (weak, nonatomic) IBOutlet UIButton *cancelReminder;
@property (weak, nonatomic) IBOutlet UIButton *setReminderButton;
@property (nonatomic) NSString * number;

- (IBAction)setReminder:(id)sender;
- (IBAction)cancelReminder:(id)sender;
- (IBAction)getDirections:(id)sender;
- (IBAction)addReminder:(id)sender;
- (IBAction)call:(id)sender;
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
- (IBAction)cancel:(id)sender;
@end

@implementation AlternativeBookingCongratulationsViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title=@"Congratulations!";
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.view.backgroundColor=[UIColor blackColor];
    [self.navBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navBar.shadowImage = [UIImage new];
    self.navBar.translucent = YES;
    self.navBar.backgroundColor = [UIColor clearColor];
    
    
    //change font style of navigation bar title
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont systemFontOfSize: 20],
      NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil]];


    
    
    self.bookingDescription.font=[UIFont systemFontOfSize: 15.0f];
    self.chargeDescription.font = [UIFont systemFontOfSize: 15.0f];
    self.thankYouLabel.font = [UIFont systemFontOfSize: 15.0f];
    self.remainingPrice = [self.price doubleValue] /100*90;
    self.chargedPrice = [self.price doubleValue] /100*10;
    
    NSLog(@"Salon Data name %@",self.salonData.salon_name);
    //if SalonData is nil
    if (self.salonData!=nil) {
        self.bookingDescription.text = [NSString stringWithFormat: @"Appointment booked & confirmed for an %@ at %@, %@.",self.chosenService,self.salonData.salon_name,self.chosenDate];
        [self.bookingDescription boldSubstring:self.chosenService];
        [self.bookingDescription boldSubstring:self.salonData.salon_name];
        [self.bookingDescription boldSubstring:self.chosenDate];
        NSString * chargePrStr = [NSString stringWithFormat:@"€%.2lf",self.chargedPrice];
        NSString * remainingPrStr = [NSString stringWithFormat:@"€%.2lf",self.remainingPrice];
        self.chargeDescription.text = [NSString stringWithFormat:@"%@ has been charged on your card and the remaining %@ can be paid on arrival at this Salon.",chargePrStr,remainingPrStr];
        [self.chargeDescription boldSubstring:chargePrStr];
        [self.chargeDescription boldSubstring:remainingPrStr];
        
        self.chargeDescription.adjustsFontSizeToFitWidth=YES;
    }
    
    self.bookingDescription.adjustsFontSizeToFitWidth=YES;
    
    
    
    
}

//cancel button only for map view
-(void)cancelButtonPressed{
    NSLog(@"Cancel is pressed");
    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"popToBrowse" object:self];
}



- (IBAction)cancel:(id)sender {
    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"popToBrowse" object:self];
   
    
}

#pragma mark - IBActons
- (IBAction)getDirections:(id)sender {
    
    //get directions;
    GetDirectionsViewController *destinationViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
    destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
    destinationViewController.transitioningDelegate = self;
    destinationViewController.salonMapData = self.salonData;
    
    [self presentViewController:destinationViewController animated:YES completion:NULL];

}

- (IBAction)call:(id)sender {
    
    //add call
    
    self.number=@"";
    self.number = self.salonData.salon_phone;
    if (self.number.length==0) {
        self.number=self.salonData.salon_landline;
    }
    
    if (self.number.length==0) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"No number" message:@"The salon has not provided a contact number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Call" message:[NSString stringWithFormat:@"Are you sure you want to call %@?",self.number] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=2;
    [alert show];
    
    
}


- (IBAction)addReminder:(id)sender {
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(40, 161, 260, 223)];
    view.backgroundColor = [UIColor lightGrayColor];
    view.layer.cornerRadius=3.0;
    view.clipsToBounds=YES;
    
    
    UIDatePicker * dp = [[UIDatePicker alloc] initWithFrame:CGRectMake(-50, 0, 160, 162)];
    dp.datePickerMode=UIDatePickerModeDateAndTime;
    dp.minimumDate=[NSDate date];
    self.reminderDatePicker = dp;
    [view addSubview:dp];
    
    
    UIButton * reminderButton = [UIButton buttonWithType:UIButtonTypeSystem];
    reminderButton.backgroundColor = kWhatSalonBlue;
    [reminderButton addTarget:self action:@selector(setReminder:) forControlEvents:UIControlEventTouchUpInside];
    [reminderButton setTitle:@"Set Reminder" forState:UIControlStateNormal];
    [reminderButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    reminderButton.frame = CGRectMake(130, 183, 130, 40);
    self.setReminderButton=reminderButton;
    [view addSubview:self.setReminderButton];
    
    UIButton * cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    cancelButton.backgroundColor = kWhatSalonBlue;
    [cancelButton addTarget:self action:@selector(cancelReminder:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(0, 183, 130, 40);
    [WSCore addRightLineToView:cancelButton withWidth:0.5f];
    self.cancelReminder = cancelButton;
    [view addSubview:self.cancelReminder];
    
    self.reminderView = view;
    [self.view addSubview:self.reminderView];
    self.reminderView.center = self.view.center;
    [self disableControls];
    
}


#pragma mark - Reminder methods
-(void)createReminder
{
    EKReminder *reminder = [EKReminder
                            reminderWithEventStore:self.eventStore];
    
    reminder.title = [NSString stringWithFormat:@"Appointment booked for a %@ at %@, %@",self.chosenService,self.salonData.salon_name,self.chosenDate];
    
    reminder.calendar = [_eventStore defaultCalendarForNewReminders];
    
    NSDate *date = [self.reminderDatePicker date];
    EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:date];
    
    [reminder addAlarm:alarm];
    
    NSError *error = nil;
    
    [_eventStore saveReminder:reminder commit:YES error:&error];
    
    if (error){
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@",[error localizedDescription]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        alert.tag=1;
        NSLog(@"error = %@", error);
        NSLog(@"%@",[self.reminderDatePicker date]);
        
    }
    [self enableControls];
    /*
    else{
        
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Reminder" message:@"Reminder set" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        NSLog(@"%@",[self.reminderDatePicker date]);
        [self enableControls];
    }
     */
    
    
}

- (IBAction)setReminder:(id)sender {
    if (self.eventStore == nil)
    {
        self.eventStore = [[EKEventStore alloc]init];
        
        [self.eventStore requestAccessToEntityType:EKEntityTypeReminder completion:^(BOOL granted, NSError *error) {
            
            if (!granted)
                NSLog(@"Access to store not granted");
        }];
    }
    
    if (self.eventStore != nil)
        [self createReminder];
    
    
}


- (IBAction)cancelReminder:(id)sender {
    [self enableControls];
}

#pragma mark - Enable & disable controls
-(void)disableControls{
    // self.reminderView.hidden=NO;
    self.closeButton.enabled=NO;
    self.directionsButton.userInteractionEnabled=NO;
    self.directionsButton.alpha=0.8;
    self.callButton.userInteractionEnabled=NO;
    self.callButton.alpha=0.8;
    self.reminderButton.userInteractionEnabled=NO;
    self.reminderButton.alpha=0.8;
    
    
}

-(void)enableControls{
    self.reminderView.hidden=YES;
    self.closeButton.enabled=YES;
    self.directionsButton.userInteractionEnabled=YES;
    self.directionsButton.alpha=1.0;
    self.callButton.userInteractionEnabled=YES;
    self.callButton.alpha=1.0;
    self.reminderButton.userInteractionEnabled=YES;
    self.reminderButton.alpha=1.0;
    
}

#pragma mark - alertView delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        if (buttonIndex ==[alertView cancelButtonIndex]) {
            [self enableControls];
        }
    }
    else if(alertView.tag==2){
        if (buttonIndex!=[alertView cancelButtonIndex]) {
           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.number]]];
        }
        
    }
}



#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}

- (IBAction)share:(id)sender {
    
    SocialShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"socialVC"];
    shareVC.modalPresentationStyle=UIModalPresentationCustom;
    shareVC.transitioningDelegate=self;
    [self presentViewController:shareVC animated:YES completion:nil];

}
@end
