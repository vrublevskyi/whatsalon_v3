//
//  SalonReviewListViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 18/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header ViewController.h
  
 @brief This is the header file where my super-code is contained.
  
 This file contains the most importnant method and properties decalaration. It's parted by two methods in total, which can be used to perform temperature conversions.
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd
 @version    1.0.3
 
 */
#import <UIKit/UIKit.h>

@interface SalonReviewListViewController : UIViewController

/*! @brief represents the salon object. */
@property (nonatomic) Salon *salonData;


//@property (nonatomic) BOOL isList;
@end
