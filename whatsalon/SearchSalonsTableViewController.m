//
//  SearchSalonsTableViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 04/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SearchSalonsTableViewController.h"
#import "SalonIndividualDetailViewController.h"

#import "UITableView+ReloadTransition.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "Salon.h"
#import "OpeningDay.h"
#import "SearchTableViewCell.h"
#import "LocationObject.h"
#import "UIView+AlertCompatibility.h"

NSString const * API_KEY = @"AIzaSyD0RrghF_SmwrHUfHZ_EcTBNrvpMCQs0Nk";
NSString * CURRENT_LOCATION =@"Current Location";

@interface SearchSalonsTableViewController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,GCNetworkManagerDelegate>

//search
@property (weak, nonatomic) IBOutlet UISearchBar *locationSearchBar;

@property (nonatomic) BOOL isFiltered;

@property (nonatomic) NSString * searchString;
@property (nonatomic) BOOL isModal;

@property (nonatomic) NSMutableArray * suggestedStrings;
@property (nonatomic) NSMutableArray * filteredStrings;

@property (nonatomic) GCNetworkManager * networkManager;

@property (nonatomic) NSTimer * myTimer;

//google maps
@property (nonatomic) NSMutableArray * locationArray;
@property (nonatomic) BOOL isLocationSearch;

@property (nonatomic) CLLocation *location;
@property (nonatomic) UIImageView * poweredByGoogle;
@end

@implementation SearchSalonsTableViewController


#pragma mark - GCNetwork Manager
- (void)handleLocationSearchSuccessResult:(NSDictionary *)jsonDictionary {
    NSArray *locationArray = [[jsonDictionary valueForKey:@"results"] valueForKey:@"formatted_address"];
    NSArray *latitudeLongitude = [[jsonDictionary valueForKey:@"results"] valueForKey:@"geometry"];
    
    int total = (int)locationArray.count;
    NSLog(@"locationArray count: %lu", (unsigned long)locationArray.count);
    
    for (int i = 0; i < total; i++)
    {
        NSString *statusString = [jsonDictionary valueForKey:@"status"];
        NSLog(@"JSON Response Status:%@", statusString);
        LocationObject *address= [[LocationObject alloc] init];
        NSLog(@"Address: %@", [locationArray objectAtIndex:i]);
        address.fullAddress = [locationArray objectAtIndex:i];
        address.lat=[latitudeLongitude objectAtIndex:i][@"location"][@"lat"];
        address.lon=[latitudeLongitude objectAtIndex:i][@"location"][@"lng"];
        NSLog(@"Lat and Long %@,%@",[latitudeLongitude objectAtIndex:i][@"location"][@"lat"],[latitudeLongitude objectAtIndex:i][@"location"][@"lng"]);
        
        [self.locationArray addObject:address];
    }
    
    self.isFiltered=NO;
    [self.searchSuggestionsTableView reloadData];
}

- (void)handleSalonSearchSuccessResult:(NSDictionary *)jsonDictionary {
    self.filteredStrings = [NSMutableArray array];
    
    NSLog(@"sdcit %@",jsonDictionary);
    NSDictionary *dataDict = jsonDictionary;
    NSArray * salonsArray = dataDict[@"message"][@"results"];
    for (NSDictionary *sDict in salonsArray) {
        
        Salon *salon = [Salon salonWithID:[sDict[@"salon_id"] stringValue] ];
        if ([sDict[@"salon_latest_review"]count]!=0) {
            
            salon.hasReview=YES;
            SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:sDict[@"salon_latest_review"][@"user_name"] AndWithLastName:sDict[@"salon_latest_review"][@"user_last_name"]];
            NSLog(@"first name %@",sDict[@"salon_latest_review"][@"user_name"]);
            salonReview.message = sDict[@"salon_latest_review"][@"message"];
            salonReview.review_image=sDict[@"salon_latest_review"][@"review_image"];
            salonReview.salon_rating=sDict[@"salon_latest_review"][@"salon_rating"];
            salonReview.stylist_rating=sDict[@"salon_latest_review"][@"stylist_rating"];
            salonReview.date_reviewed=sDict[@"salon_latest_review"][@"date_reviewed"];
            salonReview.user_avatar_url=sDict[@"salon_latest_review"][@"user_avatar"];
            salon.salonReview=salonReview;
            
        }
        if (sDict[@"salon_name"] !=[NSNull null]) {
            salon.salon_name=sDict[@"salon_name"];
        }
        if (sDict[@"salon_description"] !=[NSNull null]) {
            salon.salon_description = sDict[@"salon_description"];
        }
        if (sDict[@"salon_phone"] != [NSNull null]) {
            salon.salon_phone = sDict[@"salon_phone"];
        }
        if (sDict[@"salon_lat"] !=[NSNull null]) {
            salon.salon_lat = [sDict[@"salon_lat"] doubleValue];
        }
        if (sDict[@"salon_lon"] != [NSNull null]) {
            salon.salon_long = [sDict[@"salon_lon"] doubleValue];
        }
        if (sDict[@"salon_address_1"] != [NSNull null]) {
            salon.salon_address_1 = sDict[@"salon_address_1"];
        }
        if (sDict[@"salon_address_2"] != [NSNull null]) {
            salon.salon_address_2 = sDict[@"salon_address_2"];
        }
        if (sDict[@"salon_address_3"] != [NSNull null]) {
            salon.salon_address_3 = sDict[@"salon_address_3"];
        }
        if (sDict[@"city_name"] != [NSNull null]) {
            salon.city_name = sDict[@"city_name"];
        }
        if (sDict[@"county_name"]) {
            salon.country_name =sDict[@"county_name"];
        }
        if (sDict[@"country_name"] != [NSNull null]) {
            salon.country_name = sDict[@"country_name"];
        }
        if (sDict[@"salon_landline"] != [NSNull null]) {
            
            salon.salon_landline = sDict[@"salon_landline"];
        }
        if (sDict[@"salon_website"] !=[NSNull null]) {
            salon.salon_website = sDict[@"salon_website"];
        }
        if (sDict[@"salon_image"] != [NSNull null]) {
            salon.salon_image = sDict[@"salon_image"];
        }
        if (sDict[@"salon_type"] != [NSNull null]) {
            salon.salon_type = sDict[@"salon_type"];
        }
        if (sDict[@"rating"] != [NSNull null]) {
            salon.ratings = [sDict[@"rating"] doubleValue];
        }
        if (sDict[@"reviews"] != [NSNull null]) {
            salon.reviews = [sDict[@"reviews"] doubleValue];
        }
        if (sDict[@"salon_tier"] != [NSNull null]) {
            salon.salon_tier = [sDict[@"salon_tier"] doubleValue];
        }
        if (sDict[@"is_favourite"] !=[NSNull null]) {
            salon.is_favourite = [sDict[@"is_favourite"] boolValue];
        }
        if (sDict[@"distance"] !=[NSNull null]) {
            salon.distance = [sDict[@"distance"] doubleValue];
        }
        if (sDict[@"salon_description"] !=[NSNull null]) {
            salon.salon_description= sDict[@"salon_description"];
        }
        if (sDict[@"salon_images"]!=[NSNull null]) {
            
            NSArray *messageArray = sDict[@"salon_images"];
            
            for (NSDictionary * dataDict in messageArray) {
                
                NSString * imagePath = [dataDict objectForKey:@"image_path"];
                
                
                [salon.slideShowGallery addObject:imagePath];
            }
        }
        
        if (sDict[@"rating"]!=[NSNull null]) {
            salon.ratings = [sDict[@"rating"] doubleValue];
        }
        if (sDict[@"reviews"]!=[NSNull null]) {
            salon.reviews = [sDict[@"reviews"] doubleValue];
        }
        
        if ([sDict[@"salon_categories"] count]!=0) {
            salon.salonCategories =  @{
                                       @"Hair" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair"] boolValue]],
                                       @"Nails" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Nails"]boolValue]],
                                       @"Face" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Face"] boolValue]],
                                       @"Body" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Body"] boolValue]],
                                       @"Massage" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Massage"] boolValue]],
                                       @"Hair Removal" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair Removal"] boolValue]],
                                       };
        }
        
        NSLog(@"%@ categories %@",salon.salon_name,salon.salonCategories);
        
        if ([sDict[@"salon_opening_hours"] count]!=0) {
            
            NSArray * openingHours =sDict[@"salon_opening_hours"];
            for (NSDictionary* day in openingHours) {
                NSLog(@"%@ day %@",salon.salon_name, day);
                OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
                
                [salon.openingHours addObject:openingDay];
            }
            
            
        }
        
        OpeningDay * openingDay = salon.openingHours[0];
        NSLog(@"%@ opening hours %@ %@",salon.salon_name,openingDay.startTime,openingDay.dayName);
        
        [self.filteredStrings addObject:salon];
        
        
    }
    
   // [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
      NSLog(@"filtered array %lu",(unsigned long)self.filteredStrings.count);
    NSLog(@"Reload table");
    NSLog(@"is filtered %d",self.isFiltered);
    self.isFiltered=YES;//added
    [self.searchSuggestionsTableView reloadData];
  

}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
     NSLog(@"jsonDictionary %@",jsonDictionary);
    
    if (self.isLocationSearch) {
        self.isFiltered=NO;
        [self handleLocationSearchSuccessResult:jsonDictionary];
        
        
    }
    else{
        
        [self handleSalonSearchSuccessResult:jsonDictionary];
    }
    
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    /*
    if (self.isLocationSearch) {
        if ([[jsonDictionary[@"status"] lowercaseString] isEqualToString:@"unknown_error"]) {
            
            [UIView showSimpleAlertWithTitle:@"Google Server Error" message:@"A request could not be processed due to a server error. Please try again" cancelButtonTitle:@"OK"];
        }
        else if([jsonDictionary[@"message"] isEqualToString:@"Sorry, there are no available Salons in your area!"]){
             [UIView showSimpleAlertWithTitle:jsonDictionary[@"message"] message:nil cancelButtonTitle:@"OK"];
            //[WSCore showServerErrorAlert];
        }
        else{
             [UIView showSimpleAlertWithTitle:@"Failure" message:@"A request could not be processed due to a server error. Please try again." cancelButtonTitle:@"OK"];
        }

    }
    else{
        [UIView showSimpleAlertWithTitle:jsonDictionary[@"message"] message:nil cancelButtonTitle:@"OK"];
    }
     */                  
    NSLog(@"Error jsonDictionary %@",jsonDictionary);
}

#pragma mark - UIView LifeCycle

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.networkManager cancelTask];
}
- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.searchBarHolder.backgroundColor=kWhatSalonBlue;
    [self setUpSearchBar:self.salonsSearchBar];
    [self setUpSearchBar:self.locationSearchBar];
    self.searchSuggestionsTableView.delegate =self;
    self.searchSuggestionsTableView.dataSource=self;
    
    //search bar
    self.salonsSearchBar.delegate=self;

    
    self.searchSuggestionsTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    

    if ([self isBeingPresented]) {
        self.view.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.4];
        self.searchSuggestionsTableView.backgroundColor=[UIColor clearColor];
        self.isModal=YES;
    }
    
    self.suggestedStrings = [[NSMutableArray alloc] initWithArray:[WSCore searchSuggestions]];

    if (self.searchTerm.length!=0) {
        self.salonsSearchBar.text=self.searchTerm;
    }
    
    self.networkManager =[[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    [self.searchSuggestionsTableView registerClass:[SearchTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    self.searchSuggestionsTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    
    [self searchByCurrentLocation];
    
    self.locationSearchBar.text=CURRENT_LOCATION;
    
    self.view.backgroundColor=kWhatSalonBlue;
    
    
    if (self.wasSearchPressed) {
        [self.salonsSearchBar becomeFirstResponder];
    }
    if (self.catString.length>0 && [self.catID intValue]!=0) {
        self.salonsSearchBar.text = self.catString;
        [self performSearchWithSearchString:@""];
    }
}


- (void)locationArrayResetAndInit {
    
    self.locationArray = [NSMutableArray array];
    LocationObject *currentLocation = [[LocationObject alloc] init];
    NSLog(@"");
    currentLocation.lat=[NSString stringWithFormat:@"%f",[[User getInstance] fetchUsersLocation].coordinate.latitude];
    currentLocation.lon=[NSString stringWithFormat:@"%f",[[User getInstance] fetchUsersLocation].coordinate.longitude];
    currentLocation.fullAddress=@"Current Location";
    currentLocation.isCurrentLocation=YES;
    [self.locationArray addObject:currentLocation];
    
}

-(void)searchByCurrentLocation{
    
    //location search
     UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
     footerView.backgroundColor=[UIColor clearColor];
     UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"powered-by-google-on-white"]];
     imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
     imageView.frame = CGRectMake(110,10,110,20);
     [footerView addSubview:imageView];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.center=CGPointMake(footerView.center.x, imageView.frame.origin.y);
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor:kWhatSalonBlue];
    self.poweredByGoogle = imageView;
    self.poweredByGoogle.hidden=YES;
    
    
     self.searchSuggestionsTableView.tableFooterView=footerView;
    
    //v2
    
     UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
     self.navigationItem.leftBarButtonItem = cancelButton;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                               [UIColor whiteColor],NSForegroundColorAttributeName,
                                nil];
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
     self.title=@"Search";
    
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(searchForSalon:)];
    NSDictionary *attributes2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor whiteColor],NSForegroundColorAttributeName,
                                nil];
    self.navigationItem.rightBarButtonItem = submitButton;
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:attributes2 forState:UIControlStateNormal];
    
    
     self.automaticallyAdjustsScrollViewInsets = NO;
     
     [self setUpSearchBar:self.locationSearchBar];
    
    
    for (UIView *subView in self.locationSearchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;

                
                UIImageView *iconView = (id)searchBarTextField.leftView;
                iconView.image = [iconView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                iconView.tintColor = kWhatSalonBlue;
                //set font color here
                searchBarTextField.textColor = kWhatSalonBlue;
                searchBarTextField.font=[UIFont boldSystemFontOfSize:14.0f];
          
                
                break;
            }
        }
    }

    
    [self locationArrayResetAndInit];
  
    
    self.salonsSearchBar.showsCancelButton=NO;
    self.salonsSearchBar.placeholder=@"Search Salons                                        ";
    
    self.title=@"Search";
    NSLog(@"Set Up");
    self.locationSearchBar.delegate=self;
    
    self.location = [[CLLocation alloc]initWithLatitude:[[User getInstance] fetchUsersLocation].coordinate.latitude
                                              longitude:[[User getInstance] fetchUsersLocation].coordinate.longitude];
    
}

-(void)setUpSearchBar:(UISearchBar *)searchBar{
    
    //[WSCore addLightBlurAsSubviewToView:self.searchBarHolder];
    
    searchBar.backgroundColor=[UIColor clearColor];
    
    // Search Bar Cancel Button Color
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:self.view.tintColor];
    
    // set Search Bar texfield corder radius
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    txfSearchField.layer.cornerRadius = 3.0f;
    
    searchBar.backgroundImage=[UIImage new];
    [self.searchBarHolder bringSubviewToFront:searchBar];
    
    
    UIView * grayLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.searchBarHolder.frame.size.height-0.5, self.searchBarHolder.frame.size.width, 0.5)];
    grayLine.backgroundColor=[UIColor lightGrayColor];
    [self.searchBarHolder addSubview:grayLine];
    [self.searchBarHolder bringSubviewToFront:grayLine];
    
    if (searchBar==self.locationSearchBar) {
        [self.locationSearchBar setImage:[UIImage imageNamed:@"map_pin_filled"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    
    }
    
}
-(void)cancel{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)searchForLocation {
    
    [self locationArrayResetAndInit];
    NSLog(@"Location Search now");
    //[UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    NSString * params = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&&sensor=true&components=country:IE&key=%@",self.locationSearchBar.text,API_KEY];
    params = [params stringByReplacingOccurrencesOfString:@" "  withString:@"+"];
    [self.networkManager loadWithURL:[NSURL URLWithString:params] withParams:@"" WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
}

- (void)searchForSalon:(NSString *)text {
    
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSearch_Salons_URL]];
    NSString * params = [NSString string];
    
    params = [params stringByAppendingString:@"&per_page=15"];
    
    NSLog(@"Search String %@",text);
 
    NSString * search = @"";
    if ( self.salonsSearchBar.text && [self.salonsSearchBar.text caseInsensitiveCompare:@"body"] ) {
       self.catID = @"6";
        NSLog(@"Is body");
    }
    else if ([self.salonsSearchBar.text isEqualToString:@"Face"]) {
        
    }else if ([self.salonsSearchBar.text isEqualToString:@"Hair"]) {
        
    }else if ([self.salonsSearchBar.text isEqualToString:@"Hair Removal"]) {
        
        
    }else if ([self.salonsSearchBar.text isEqualToString:@"Massage"]) {
        
    }
    else if ([self.salonsSearchBar.text isEqualToString:@"Nails"]) {
        
        
        
    }else{
        search = self.salonsSearchBar.text;
    }

    params = [params stringByAppendingString:[NSString stringWithFormat:@"&search_string=%@",search]];
    
    if (self.catID.length>0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&filter_category_id=%@",self.catID]];
    }
    
    
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%f&longitude=%f",self.location.coordinate.latitude,self.location.coordinate.longitude]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%@",kSearchRadius]];
    if ([[User getInstance] isUserLoggedIn]) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken ]]];
    }
    
    NSLog(@"params %@",params);
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
}

-(void)performSearchWithSearchString: (NSString *) text{
    if (self.myTimer) {
        if ([self.myTimer isValid])
        {
           text = self.myTimer.userInfo;
        }
        
    }
    else{
        text=self.salonsSearchBar.text;
    }
    
    if (self.isLocationSearch) {
        
        [self searchForLocation];
        
        
    }
    else{
        
        [self searchForSalon:text];
 
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

     [WSCore transparentNavigationBarOnView:self];
    [WSCore statusBarColor:StatusBarColorWhite];
    
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar*)searchBar {
    
    
 
    if (searchBar==self.locationSearchBar) {
        self.poweredByGoogle.hidden=NO;
        self.isLocationSearch=YES;
        self.isFiltered=NO;//added
        [self.searchSuggestionsTableView reloadData];
    }
    else{
        
        if ([self.salonsSearchBar.text isEqualToString:@"Body"]||[self.salonsSearchBar.text isEqualToString:@"Face"]||[self.salonsSearchBar.text isEqualToString:@"Hair Removal"]||[self.salonsSearchBar.text isEqualToString:@"Hair"]|| [self.salonsSearchBar.text isEqualToString:@"Massage"]||[self.salonsSearchBar.text isEqualToString:@"Nails"] ) {
            searchBar.text=@"";
            
                    }

        self.poweredByGoogle.hidden=YES;
        NSLog(@"search bar become present");
        self.isLocationSearch=NO;
        self.isFiltered=YES;//added
        
        if (self.locationSearchBar.text.length==0) {
            NSLog(@"Change");
            self.locationSearchBar.text = CURRENT_LOCATION;
            [self locationArrayResetAndInit];
        }
        [self.searchSuggestionsTableView reloadData];
    }
    return YES;
}

- (UITextField *) findTextFieldFromControl:(UIView *) view
{
    for (UIView *subview in view.subviews)
    {
        if ([subview isKindOfClass:[UITextField class]])
        {
            return (UITextField *)subview;
        }
        else if ([subview.subviews count] > 0)
        {
            return [self findTextFieldFromControl:subview];
        }
    }
    return nil;
}
#pragma mark - UISearchBar delegate and datasource methods


-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    if (searchBar==self.locationSearchBar) {
        if ([self.locationSearchBar.text isEqualToString:CURRENT_LOCATION]) {
            self.locationSearchBar.text=@"";
        }
    }
    
    
    return YES;
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchBar.text ==0 ||searchBar.text.length<3) {
        
        self.isFiltered=NO;
        if (self.myTimer)
        {
            if ([self.myTimer isValid])
            {
                [self.myTimer invalidate];
            }
            self.myTimer=nil;
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
        }
        
    }else if (searchBar.text.length>=3) {
        NSLog(@"Text Did Change");
        
        if (!self.isLocationSearch) {
             self.isFiltered=YES;
        }
       
       
        if (self.myTimer)
        {
            if ([self.myTimer isValid])
            {
                //[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [self.myTimer invalidate];
            }
            self.myTimer=nil;
        }
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(performSearchWithSearchString:) userInfo:searchText repeats:NO];
       
    
        
    }
}



-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{

    [self.view endEditing:YES];
    
    NSLog(@"Search bar button pressed");
    [self performSearchWithSearchString:searchBar.text];
    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    self.searchSuggestionsTableView.delegate=nil;
    self.searchSuggestionsTableView.dataSource=nil;
    self.salonsSearchBar.delegate=self;
    
    if([self isBeingPresented] ||self.isModal){
        NSLog(@"Modal");
        [self dismissViewControllerAnimated:YES completion:^{
            self.isModal=NO;
        }];
    }
    else{
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.type = kCATransitionFade;
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController popViewControllerAnimated:NO];

    }
    
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
#pragma mark - Table view delegate and data source
- (UITableViewCell *)searchCell:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self.searchSuggestionsTableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = [self.filteredStrings objectAtIndex:indexPath.row];
    
    
    return cell;
}

- (void)didSelectLocationCell:(NSIndexPath *)indexPath {
    
    //to empty cell
    if (self.filteredStrings.count>0) {
        [self.filteredStrings removeAllObjects];
    }
   
    [self.searchSuggestionsTableView reloadData];
    
    if (self.locationArray.count>0) {
        LocationObject *loc = [self.locationArray objectAtIndex:indexPath.row];
        self.locationSearchBar.text = loc.fullAddress;
        
        self.location = [[CLLocation alloc]initWithLatitude:[loc.lat doubleValue]
                                                  longitude:[loc.lon doubleValue]];
        [self.salonsSearchBar becomeFirstResponder];
        self.isLocationSearch=NO;
        [self searchForSalon:self.salonsSearchBar.text];\
        
        NSLog(@"** Address: %@ \n Lat %@ \n lon %@",loc.fullAddress,loc.lat,loc.lon);

    }
}

- (void)didSelectSalonCell:(NSIndexPath *)indexPath {
    SalonIndividualDetailViewController * salonVc = [self.storyboard instantiateViewControllerWithIdentifier:@"salonVC"];
    if (self.filteredStrings.count>0) {
        
        if ([[self.filteredStrings objectAtIndex:indexPath.row] isKindOfClass:[Salon class]]) {
            salonVc.salonData = [self.filteredStrings objectAtIndex:indexPath.row];
            salonVc.unwindBackToSearchFilter=YES;
            [self.navigationController pushViewController:salonVc animated:YES];

        }
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (self.isLocationSearch) {
        
        
        [self didSelectLocationCell:indexPath];
        
        
    }
    else{
        
        [self didSelectSalonCell:indexPath];
    }
    
    
}
- (UITableViewCell *)locationCellSetUp:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    UITableViewCell * cell;
    
    
    LocationObject * loc = [self.locationArray objectAtIndex:indexPath.row];
    
    
   
    if (loc.isCurrentLocation && indexPath.row==0) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentLocationCell" forIndexPath:indexPath];
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 20, 20)];
        imgView.backgroundColor=[UIColor clearColor];
        [imgView.layer setCornerRadius:8.0f];
        [imgView.layer setMasksToBounds:YES];
        [imgView setImage:[UIImage imageNamed:@"location"]];
        imgView.image = [imgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [imgView setTintColor:kWhatSalonBlue];
        [cell.contentView addSubview:imgView];
        imgView.center=CGPointMake(imgView.center.x, cell.contentView.center.y);
        cell.imageView.image = [UIImage new];
        cell.textLabel.textColor=kWhatSalonBlue;
       
        
        
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"LocationCell" forIndexPath:indexPath];
       
        cell.imageView.image=nil;
        cell.textLabel.textColor=[UIColor blackColor];
       
        
        
    }
    cell.textLabel.text=loc.fullAddress;
    return cell;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isLocationSearch) {
        
        
        UITableViewCell *cell;
        cell = [self locationCellSetUp:indexPath tableView:tableView];
        
        return cell;
    }
    
    
    
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (self.isFiltered == YES) {
        
        Salon * salon = nil;
        salon = [self.filteredStrings objectAtIndex:indexPath.row];
        [cell setUpCellWithSalon:salon];
       
    }
    
    
    return cell;
    
    

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
  
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (self.isLocationSearch) {
        
        return self.locationArray.count;
    }
    else{
        
        if (self.isFiltered) {
            return self.filteredStrings.count;
        }
    }
    
    return 0;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.isFiltered){
        return 100.0f;

    }
    return 44;
}

@end
