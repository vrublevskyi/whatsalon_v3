//
//  GCFacebookHelper.h
//
//
//  Created by Graham Connolly on 17/09/2015.
//  Copyright © 2015 GrahamConnolly. All rights reserved.
//

/*!
 @header GCFacebookHelper.h
  
 @brief This is the header file for the GCFacebookHelper class.
  
 This file contains methods for logging in via Facebook.
  
 @author Graham Connolly
 @copyright  2015 Graham Connolly
 @version    1.0.1
 */

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


/*!
     @protocol GCFacebookDelegate
  
     @brief The GCFacebook protocol
  
     It's a protocol used to notify the delegate that an apearance update needs to be made.
 */

@protocol GCFacebookDelegate <NSObject>

/*! @brief notifies the delegate that an error occured when logging in via Facebook.
 
    @param error - The error that occurred when logging in. 
 
 */
-(void)errorOccurredWhenLoggingIn: (NSError *) error;

/*! @brief notifies the delegate that the user canceled logging in. */
-(void)userDidCancelLogin;

/*! @brief notifies the delegate that the user has logged in. */
-(void)userDidLogin;

/*! @brief notifies the delegate that the permissions to Facebook have been revoked. */
-(void)permissionsRevoked;

/*! @brief notifies the delegate that a revoking permissions has failed due to an error. */
-(void)revokeErrorOccured: (NSError *) error;

@end
@interface GCFacebookHelper : NSObject

/*! @brief represents the CGFacebookDelegate object. */
@property (nonatomic) id<GCFacebookDelegate> delegate;

/*! @brief represents the FBSDKLoginManager object. */
@property (nonatomic)  FBSDKLoginManager *loginManager;

/*! @brief makes a request to login in to Facebook to recieve user details.
 
    @param permissions the permissions that are asked.
 
    @param parentController - the parent view controller.
 
 */
-(void)loginFacebookUserWithPermissions: (NSArray *) permissions FromViewController: (UIViewController *) parentController;

/*! @brief makes a request to log the user out of Facebook. */
-(void)logout;

/*! @brief makes a request to revoke permissions. */
-(void)revokePermissions;
@end
