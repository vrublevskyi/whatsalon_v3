//
//  FavMessageView.m
//  whatsalon
//
//  Created by Graham Connolly on 02/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "FavMessageView.h"

@implementation FavMessageView


-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        //1. load .xib
        [[NSBundle mainBundle] loadNibNamed:@"FavMessage" owner:self options:nil];
        
        //2 adjust bounds
        self.bounds = self.view.bounds;
        
        self.messageImage.contentMode = UIViewContentModeScaleAspectFit;
        self.messageText.adjustsFontSizeToFitWidth=YES;
        self.messageText.textColor=kCellLinesColour;
    
        self.messageTitle.textColor=kCellLinesColour;
        self.messageTitle.adjustsFontSizeToFitWidth=YES;
        self.messageTitle.text = [self.messageTitle.text uppercaseString];
        
        UIImage * templateImage = [[UIImage imageNamed:@"HeartSolid" ] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.messageImage.image = templateImage;
        self.messageImage.tintColor=kWhatSalonBlue;
       
        UIImage *image = [[UIImage imageNamed:@"plus_filled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.plusButton setImage:image forState:UIControlStateNormal];
        self.plusButton.tintColor = kWhatSalonBlue;
        self.plusButton.contentMode=UIViewContentModeScaleAspectFill;
        self.plusButton.hidden=YES;
        
        
        self.startDiscoveringBtn.backgroundColor=[UIColor clearColor];
        self.startDiscoveringBtn.buttonColor = kWhatSalonBlue;
        self.startDiscoveringBtn.shadowColor = kWhatSalonBlueShadow;
        self.startDiscoveringBtn.shadowHeight = 1.0f;
        self.startDiscoveringBtn.cornerRadius = 3.0f;
        [self.startDiscoveringBtn setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
        
        [self addSubview:self.view];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //load the interface file from .xib
        [[NSBundle mainBundle] loadNibNamed:@"FavMessage" owner:self options:nil];
        
        //Add as a subview
        [self addSubview:self.view];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
