//
//  SalonStylistPickerData.h
//  whatsalon
//
//  Created by Graham Connolly on 16/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StylistPickerDelegate <NSObject>

-(void)didSelectStylist:(NSString *) stylist;

@end

@interface SalonStylistPickerData : NSObject <UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic) NSMutableArray * arrayOfStyles;
@property (nonatomic,assign) id<StylistPickerDelegate> myDelegate;

-(void)loadStyles;

@end
