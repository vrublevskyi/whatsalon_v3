//
//  ViewController.h
//  NewWalkthrought
//
//  Created by Graham Connolly on 08/12/2015.
//  Copyright © 2015 GrahamConnolly. All rights reserved.
//

#import <UIKit/UIKit.h>


/*!
     @protocol TutorialDelegate
  
     @brief The TutorialDelegate protocol
  
     It's a protocol used to notify the delegate that a change has occurred.
 */
@protocol TutorialDelegate <NSObject>

@optional

/*! @brief the action which is triggered when the walkthrough is dismissed. */
-(void)walkthroughIsDismissed;

/*! @brief the go to login sign up action. */
-(void)goToLoginSignUp;

@end
@interface TutorialViewController : UIViewController<UIScrollViewDelegate>

/*! @brief represents the TutorialDelegate object. */
@property (nonatomic,assign) id<TutorialDelegate> myDelegate;

/*! @brief represents the scrollview. */
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

/*! @brief represents the page control. */
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;

/*! @brief determines if it is coming from the settings screen. */
@property (nonatomic) BOOL isFromSettings;


@end

