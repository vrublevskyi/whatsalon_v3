//
//  ExperiencesViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 25/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "ExperiencesViewController.h"
#import "ExperiencesTableViewCell.h"
#import "GCNetworkManager.h"
#import "ExperienceItem.h"
#import "ExperienceDetailViewController.h"
#import "User.h"


@interface ExperiencesViewController ()<UITableViewDataSource,UITableViewDelegate,GCNetworkManagerDelegate,ExperienceCellDelegate,ExperienceDetailDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) GCNetworkManager * mananger;
@property (nonatomic) NSMutableArray * experiencesArray;
@end

@implementation ExperiencesViewController

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
     [self.mananger cancelTask];
}


#pragma mark - ExperiencedCellDelegate
-(void)didFavouriteBlogPostWithID:(NSString *)b_id{
    for (int i=0; i<self.experiencesArray.count; i++) {
        ExperienceItem * item = [self.experiencesArray objectAtIndex:i];
        if ([item.experience_id isEqualToString:b_id]) {
            item.is_user_favourite=YES;
            [self.experiencesArray replaceObjectAtIndex:i withObject:item];
            NSLog(@"b_id %@",b_id);
        }
        
        
        
    }

}

-(void)didUnFavouriteBlogPostWithID:(NSString *)b_id{
    
    for (int i=0; i<self.experiencesArray.count; i++) {
        ExperienceItem * item = [self.experiencesArray objectAtIndex:i];
        if ([item.experience_id isEqualToString:b_id]) {
            item.is_user_favourite=NO;
            [self.experiencesArray replaceObjectAtIndex:i withObject:item];
            NSLog(@"Exp id %@",b_id);
        }
    }
}
    
    
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    [self.tableView registerClass:[ExperiencesTableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.tableView.backgroundColor=[UIColor clearColor];
    self.tableView.backgroundView=nil;
    
    self.view.backgroundColor=kBackgroundColor;
    
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    footerView.backgroundColor=[UIColor clearColor];
    self.tableView.tableFooterView=footerView;
    
    self.mananger = [[GCNetworkManager alloc] init];
    self.mananger.delegate=self;
    self.mananger.parentView=self.view;
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    [self.mananger loadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Experiences_URL]] withParams: params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
    self.experiencesArray =[NSMutableArray array];
    
    
}


-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Success %@",jsonDictionary);
    
    for (id key in jsonDictionary[@"message"]) {
        ExperienceItem * exp = [[ExperienceItem alloc] init];
        if (key[@"description"]!=[NSNull null]) {
            exp.expDescription = key[@"description"];
        }
        if (key[@"image"] !=[NSNull null]) {
            exp.expImageURL = key[@"image"];
            
        }
        if (key[@"link"] !=[NSNull null]) {
            exp.link = key[@"link"];
        }
        if (key[@"title"] !=[NSNull null]) {
            exp.title = key[@"title"];
            
        }
        if (key[@"timestamp"] !=[NSNull null]) {
            exp.timestamp = [key[@"timestamp"] doubleValue];
        }
        if (key[@"experience_id"] !=[NSNull null]) {
            exp.experience_id = [NSString stringWithFormat:@"%@",key[@"experience_id"]];
        }
        if (key[@"favourites"] != [NSNull null]) {
            exp.favourites = [key[@"favourites"] intValue];
        }
        if (key[@"is_user_favourite"] != [NSNull null]){
           
            exp.is_user_favourite = [key[@"is_user_favourite"] intValue];
        
        }
        if (key[@"favourites_display"] != [NSNull null]){
            exp.favourite_dispaly = key[@"favourites_display"];
        }
        if (key[@"date"] != [NSNull null]){
            exp.experienceDateToDisplay = key[@"date"];
        }
        
        NSLog(@"Only show favourites %@ and is exp favour %@",StringFromBOOL(self.showOnlyFavourites),StringFromBOOL(exp.is_user_favourite));
        
        if (self.showOnlyFavourites && exp.is_user_favourite) {
            NSLog(@"Add");
            [self.experiencesArray addObject:exp];
        }
        else if(!self.showOnlyFavourites){
            [self.experiencesArray addObject:exp];
        }
        
    }
    
    [self.tableView reloadData];
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Fail %@",jsonDictionary);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.tabBarController) {
        self.navigationController.navigationBarHidden=NO;
    }
   // [WSCore setDefaultNavBar:self];
    [WSCore flatNavBarOnView:self];
}
#pragma mark - tableViewDelegate & Datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ExperiencesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.myDelegate=self;
    ExperienceItem * exp = [self.experiencesArray objectAtIndex:indexPath.row];
    [cell setUpExperiencesCellWithExperienceItem:exp];
        return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.experiencesArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kTwoThirdsCellHeight;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"experienceDetailSegue" sender:self];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"experienceDetailSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ExperienceDetailViewController * experienceDetailVC = (ExperienceDetailViewController *)segue.destinationViewController;
        experienceDetailVC.expItem = [self.experiencesArray objectAtIndex:indexPath.row];
        experienceDetailVC.myDelegate=self;
    }
}

#pragma mark - ExperienceDetailDelegate
-(void)updateBlogPostAsFavouriteWithBlogID:(NSString *)b_id{
    NSLog(@"Add as fav");
    for (int i=0; i<self.experiencesArray.count; i++) {
        ExperienceItem * item = [self.experiencesArray objectAtIndex:i];
        if ([item.experience_id isEqualToString:b_id]) {
            item.is_user_favourite=YES;
            [self.experiencesArray replaceObjectAtIndex:i withObject:item];
            NSLog(@"b_id %@",b_id);
        }
        
        
        
    }

    [self.tableView reloadData];
}

-(void)updateBlogPostAsNotFavouriteWithBlogID:(NSString *)b_id{
    NSLog(@"Updated as not fav");
    for (int i=0; i<self.experiencesArray.count; i++) {
        ExperienceItem * item = [self.experiencesArray objectAtIndex:i];
        if ([item.experience_id isEqualToString:b_id]) {
            item.is_user_favourite=NO;
            [self.experiencesArray replaceObjectAtIndex:i withObject:item];
            NSLog(@"Exp id %@",b_id);
        }
        
        
        
    }
    [self.tableView reloadData];
}

@end
