//
//  TabDiscoveryViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 30/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabDiscoveryViewController.h"
#import "DiscoveryImageTableViewCell.h"
#import "DiscoveryServicesTableViewCell.h"
#import "SalonTableViewCell.h"
#import "RESideMenu.h"
#import "DiscoveryTableViewCell.h"
#import "NSString+Validations.h"
#import "Harpy.h"
#import "WalkthroughViewController.h"
#import "SettingsViewController.h"
#import "DiscoveryItem.h"
#import "FavouriteViewController.h"
#import "SearchSalonsTableViewController.h"
#import "TabBrowseViewController.h"
#import "INTULocationManager.h"
#import "UIView+AlertCompatibility.h"
#import "LastMinuteSearchViewController.h"
#import "User.h"
#import "Salon.h"
#import "OpeningDay.h"
#import "SearchSalonsFilterViewController.h"
#import "GCNetworkManager.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "RightSideMenuController.h"
#import "TabLoginOrSignUpViewController.h"
#import "TwilioVerifiyViewController.h"
#import "ExperiencesViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ColoredImage.h"
#import "LocationObject.h"
#import "TabNewSalonDetailViewController.h"
//for core spotlight
#import <CoreSpotlight/CoreSpotlight.h>//weak linked
#import <MobileCoreServices/MobileCoreServices.h>
#import "CategoriesCell.h"

#import "TutorialViewController.h"
#import "whatsalon-Swift.h"
#import "PopupView.h"

/*!
 * @brief TabDiscoveryViewController - Handles the discovery stream
 */

@interface TabDiscoveryViewController ()<UITableViewDataSource,UITableViewDelegate,WalkthroughDelegate,GCNetworkManagerDelegate,UIViewControllerTransitioningDelegate,UITabBarControllerDelegate,LoginDelegate,TwilioDelegate, UIViewControllerPreviewingDelegate,TutorialDelegate,SearchServiceVCDelegate, AddAddressVCDelegate, SearchResultsVCDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *showSearchButton;

/*!
 @brief discoveryItems - hosts discovery items downloaded from the server.
 */

@property (nonatomic) NSMutableArray * discoveryItems;
@property (nonatomic) NSMutableArray * searchedItems;

/*!
 @brief represents the CGNetworkManager for making requests.
 */

@property (nonatomic) GCNetworkManager *manager;

@property (nonatomic) NSMutableArray * services;
@property (nonatomic) NSMutableArray * servicesImages;

@property (nonatomic) BOOL searchSalonIsActive;
@property (nonatomic) BOOL searchedListIsActive;

@property (nonatomic) BOOL selectLocationIsActive;
@property (nonatomic) BOOL shouldLoadDiscovery;

@property (nonatomic) NSString * catID;
@property (nonatomic) GCNetworkManager * networkManager;
@property (nonatomic) LocationObject * currentLocation;

/*! @brief represents the NSMutableArray for the location results. */
@property (nonatomic) NSMutableArray * locationArray;

/*! @brief determines if a location search has taken place. */
@property (nonatomic) BOOL isLocationSearch;

/*! @brief represents the location object. */
@property (nonatomic) CLLocation *location;

@property (nonatomic, strong) SearchServiceVC *searchVC;
@property (nonatomic, strong) PopupView *searchpopup;
@property (nonatomic, strong) PopupView *addressPopup;
@property (nonatomic, strong) NSString *categoryToSearch;
@property (nonatomic, strong) TabSearchSalonsTableViewController *oldSearchVC;
@property (nonatomic, strong) AddAddressVC *addressVC;
@property (nonatomic, strong) NSString *searchLocations;

@end

@implementation TabDiscoveryViewController

/*
 Set up CoreSpotlightSearch
 */
-(void)setUpCoreSpotlightSearch{
    
    if ([CSSearchableItemAttributeSet class]) {
        CSSearchableItemAttributeSet * attributeSet = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *)kUTTypeImage];
        attributeSet.title=@"WhatSalon";
        attributeSet.contentDescription = @"Book Hair & Beauty Appointments\nin an Instant";
        attributeSet.keywords = [NSArray arrayWithObjects:
                                 @"salon",@"salons",
                                 @"Hair",@"hair",
                                 @"Nails",@"nails",
                                 @"Nail",@"Nail",
                                 @"nail",@"nails",
                                 @"nail",@"nail",
                                 @"Beauty",@"beauty",
                                 @"experiences",@"Experiences",
                                 @"Discover",@"discover",
                                 @"Discovery",@"discovery",
                                 @"Salons Nearby",@"salons nearby",
                                 @"hair removal",@"Hair Removal",
                                 @"Massage",@"massage",
                                 @"Face",@"face",
                                 @"Make Up",@"make up",
                                 @"Body",@"body",
                                 nil];
        
        /*
         Grabs the app icon
         */
        UIImage *logoImage;
        if ([[[[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIcons"] objectForKey:@"CFBundlePrimaryIcon"] objectForKey:@"CFBundleIconFiles"] objectAtIndex:0]) {
            logoImage = [UIImage imageNamed: [[[[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIcons"] objectForKey:@"CFBundlePrimaryIcon"] objectForKey:@"CFBundleIconFiles"] objectAtIndex:0]];
        }
        else{
            NSLog(@"Using AppIcon 60x60");
            logoImage = [UIImage imageNamed:@"AppIcon60x60"];
        }
        NSData * imageData = [NSData dataWithData:UIImagePNGRepresentation(logoImage)];
        attributeSet.thumbnailData=imageData;
        
        CSSearchableItem * item = [[CSSearchableItem alloc] initWithUniqueIdentifier:@"com.whatsalon" domainIdentifier:@"spotlight.whatsalon.discovery" attributeSet:attributeSet];
        
        [[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:@[item] completionHandler:^(NSError * _Nullable error) {
            if (!error) {
                NSLog(@"Search item indexed");
            }
        }];

    }
    else{
        NSLog(@"Not supported");
    }
    
}
/*
 viewDidLayoutSubviews
 handles the position of the UITableView. AutoLayout is turned off to prevent odd layout after menu is presented and dismissed a couple of times.
 
 */

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];

}

/*
 viewDidAppear
 
 if shouldLoadDiscovery
    and then count ==0
    then loadDiscovery
 
 
 */
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (self.shouldLoadDiscovery) {
        if (self.discoveryItems.count==0) {
            [self loadDiscovery];
        }
        
        
        self.shouldLoadDiscovery=NO;
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];

    [self.networkManager destroy];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    
    self.tabBarController.delegate=self;
    self.automaticallyAdjustsScrollViewInsets=NO;
}

/*
 loadDiscovery
    make request to download Discovery
 
 */
- (void)loadDiscovery {
    //perform discovery
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Discovery_Objects_URL]];
    NSString * param = [NSString stringWithFormat:@"version=1"];
    [self.manager loadWithURL:url withParams:param WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
}


- (void)checkIfLocationIsDenied {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess || status == INTULocationStatusTimedOut) {
                                                 
                                                 // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                 // currentLocation contains the device's current location.
                                                 //NSLog(@"Current Location %f %f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
                                                 

                                                 /*
                                                  if user is logged in then grab their location
                                                  */
                                                 if ([[User getInstance] isUserLoggedIn]) {
                                                     [[User getInstance] updateUsersLocationWithLocation:currentLocation];
                                                     
                                                     [WSCore updateUsersCityAndCountry:currentLocation];
                                                    
                                                 }
                                                 /*
                                                  else
                                                  
                                                   set up guest users location
                                                  */
                                                 else{
                                                     
                                                     [[User getInstance] updateGuestUsersLocation:currentLocation];
                                                 }
                                                 
                                             }
                                             
                                             else {
                                                 // An error occurred, more info is available by looking at the specific status returned.
                                                 
                                                 switch (status) {
                                                     case INTULocationStatusServicesDenied:
                                                         
                                                         if (IS_IOS_8_OR_LATER) {
                                                             
                                                             UIAlertController * alert=   [UIAlertController
                                                                                           alertControllerWithTitle:@"Location Error"
                                                                                           message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app"
                                                                                           preferredStyle:UIAlertControllerStyleAlert];
                                                             
                                                             UIAlertAction* ok = [UIAlertAction
                                                                                  actionWithTitle:@"Open Settings"
                                                                                  style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action)
                                                                                  {
                                                                                      
                                                                                      if (&UIApplicationOpenSettingsURLString != NULL) {
                                                                                          NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                                          [[UIApplication sharedApplication] openURL:appSettings];
                                                                                      }
                                                                                      
                                                                                  }];
                                                             UIAlertAction* cancel = [UIAlertAction
                                                                                      actionWithTitle:@"Cancel"
                                                                                      style:UIAlertActionStyleDestructive
                                                                                      handler:^(UIAlertAction * action)
                                                                                      {
                                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                          
                                                                                      }];
                                                             
                                                             [alert addAction:cancel];
                                                             [alert addAction:ok];
                                                             
                                                             [self presentViewController:alert animated:YES completion:nil];
                                                         }
                                                         else{
                                                             
                                                             [UIView showSimpleAlertWithTitle:@"Location Error" message:@"You have denied WhatSalon access to location services. In order to use this app, please accept access. " cancelButtonTitle:@"OK"];
                                                         }
                                                         break;
                                                         
                                                         
                                                         
                                                     case INTULocationStatusServicesRestricted:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location services are restricted for WhatSalon. e.g (parental controls corporate policy, etc)" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                     case INTULocationStatusServicesDisabled:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                     case INTULocationStatusError:
                                                         
//                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"An error occurred while using the system location services." cancelButtonTitle:@"OK"];
                                                         break;
                                                     default:
                                                         break;
                                                 }
                                                 
                                             }
                                             //NSLog(@"Status %ld",(long)status);
                                             
                                         }];
}

- (void)tableViewSetUp {

    [self.tableView registerClass:[DiscoveryTableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView setSeparatorColor:[UIColor whiteColor]];
    [self.navigationController setNavigationBarHidden:NO];
    self.tableView.backgroundColor=[UIColor clearColor];
}

- (void)networkManagerSetUp {
    self.manager = [[GCNetworkManager alloc] init];
    self.manager.delegate=self;
    self.manager.parentView=self.view;
}


/*
 either show the walkthrough or load in discovery
 */
- (void)shouldShowWalkThrough {
    
    
    if ([WSCore showWalkthrough]) {
        [self performSelector:@selector(showWalkthroughView
                                        ) withObject:self afterDelay:0.5];
    }
    else{
        [self loadDiscovery];
    }
}

-(void) registerTableViewCells {
        [self.tableView registerNib:[UINib nibWithNibName:@"SearchCategoriesCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"SearchCategoriesCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"CategoriesCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"CategoriesCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SearchResultCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"SearchResultCell"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //removes back button title
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self checkIfLocationIsDenied];
    
   
    [[Harpy sharedInstance] checkVersion];
    [WSCore statusBarColor:StatusBarColorBlack];
    
    
    self.networkManager =[[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    [self tableViewSetUp];
    
    [self networkManagerSetUp];
    
    [self shouldShowWalkThrough];
    [self registerTableViewCells];
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.tabBarController.tabBar.frame.size.height)];
    self.tableView.tableFooterView=footerView;
    
    [self setUpCoreSpotlightSearch];
    

    [self searchByCurrentLocation];
    self.searchedListIsActive = false;
    
    
    _showSearchButton.layer.cornerRadius = 2;

    [NSNotificationCenter.defaultCenter addObserverForName:@"BookSuccessFinish" object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self bookingSuccessed];
        });
    }];
}

- (void)bookingSuccessed {
    self.tabBarController.selectedIndex = 2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}   

-(void)showWalkthroughView{
    
    TutorialViewController * tVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialVC"];
    tVC.myDelegate=self;
    [self presentViewController:tVC animated:YES completion:^{
        [WSCore walkthroughHasBeenShown];
    }];
    
}

-(IBAction)discoveryUnwind:(UIStoryboardSegue *)segue {
    self.shouldLoadDiscovery = YES;
    
    if (self.shouldLoadDiscovery) {
        if (self.discoveryItems.count==0) {
            [self loadDiscovery];
        }
        
        
        self.shouldLoadDiscovery=NO;
    }
}

- (IBAction)showSearchAction:(id)sender {
    
    [self selectLocation];
    
//    SearchServiceVC * serachVC = [[SearchServiceVC alloc]
//     initWithNibName:@"SearchServiceVC" bundle:nil];
//    serachVC.delegate = self;
//    serachVC.providesPresentationContextTransitionStyle = true;
//    serachVC.definesPresentationContext = true;
//    serachVC.modalPresentationStyle = UIModalPresentationCurrentContext;
//
//    serachVC.view.backgroundColor = [UIColor whiteColor];
//
//    serachVC.view.bounds = CGRectMake(0, 0, self.view.bounds.size.width - 60, 260);
//    serachVC.view.layer.cornerRadius = 10;
//    serachVC.view.clipsToBounds = YES;
//
//    PopupView *popup = [PopupView popupViewWithContentView:serachVC.view];
//
//    [popup show];
//
//    self.searchVC = serachVC;
//    self.searchpopup = popup;
}

-(void)selectedSearchedServiceWithSalon:(Salon *)salon on:(SearchServiceVC *)controller
    {
        [controller dismissViewControllerAnimated:true completion:^{
            [self showSelectedSalon:salon];
        }];
}

- (void)didSelectCategoryWithCategory:(NSDictionary<NSString *,NSString *> *)category on:(SearchServiceVC *)controller {
    [self.searchpopup dismiss:YES];
    self.categoryToSearch = category.allKeys.firstObject;
    if (self.categoryToSearch) {
        [self selectLocation];
    }
}

- (void)selectLocation {
    AddAddressVC *vc = [AddAddressVC new];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:true];
    /*
    vc.view.bounds = CGRectMake(0, 0, self.view.bounds.size.width - 60, 360);
    vc.view.layer.cornerRadius = 10;
    vc.view.clipsToBounds = YES;
    PopupView *popup = [PopupView popupViewWithContentView:vc.view];
    [popup show];
    self.addressPopup = popup;
    self.addressVC = vc;
     */
}

- (void)onAddressSelectedWithLocation:(CLLocation *)location address:(NSString *)address on:(AddAddressVC *)controller {
    [controller.navigationController popViewControllerAnimated:true];
    [self.addressPopup dismiss:YES];
    TabSearchSalonsTableViewController *oldSearchVC = [TabSearchSalonsTableViewController new];
    [oldSearchVC viewDidLoad];
    oldSearchVC.networkManager.delegate = self;

    if (!self.categoryToSearch) {
        self.searchLocations = address;
        [oldSearchVC searchForSalon:[NSString stringWithFormat:@"%@", self.categoryToSearch] location:location];
        self.oldSearchVC = oldSearchVC;
    }
}

-(void)showSelectedSalon:(Salon*)salon {
    
    TabNewSalonDetailViewController * salonDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"newSalonDetailVC"];//new viewcontroller instead of tableviewcontroller
    salonDetail.salonData = salon;
    [WSCore setBookingType:BookingTypeMybookings];
    salonDetail.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:salonDetail animated:true];
    self.hidesBottomBarWhenPushed=NO;
}
#pragma mark - UIViewControllerTransitioningDelegate


-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return [[DismissDetailTransition alloc] init];
}

- (void)searchedSalonSelected:(Salon *)salon on:(SearchResultsVC *)controller {
    [self showSelectedSalon:salon];
}

#pragma mark - GCNetworkManager delegate
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    [UIView showSimpleAlertWithTitle:@"No Salons in your area" message:@"" cancelButtonTitle:@"OK"];
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{

    if (manager == self.oldSearchVC.networkManager) {
        [self.oldSearchVC handleSalonSearchSuccessResult:jsonDictionary];
        SearchResultsVC *vc = [[SearchResultsVC alloc] initWithNibName:@"SearchResultsVC" bundle:nil];
        vc.searchedCategory = self.categoryToSearch;
        vc.location = self.searchLocations;
        vc.salons = self.oldSearchVC.filteredSalons;
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }

    if (self.searchSalonIsActive) {
        [self handleSalonSearchSuccessResult:jsonDictionary];
    } else {
        self.discoveryItems = [NSMutableArray array];
        for (id key in jsonDictionary[@"message"]) {
            
            DiscoveryItem *disc = [[DiscoveryItem alloc] init];
            
            if (key[@"title"] !=[NSNull null]) {
                disc.title = key[@"title"];
            }
            if (key[@"image_url"]!=[NSNull null]) {
                disc.imageURL = key[@"image_url"];
            }
            if (key[@"longitude"]!=[NSNull null]) {
                disc.lon =  [NSString stringWithFormat:@"%@",key[@"longitude"] ];
            }
            if (key[@"latitude"]!=[NSNull null]) {
                disc.lat = [ NSString stringWithFormat:@"%@", key[@"latitude"] ];
            }
            if (key[@"sort_id"]!=[NSNull null]) {
                disc.sort_id = [ NSString stringWithFormat:@"%@", key[@"sort_id"] ];
            }
            if (key[@"type"]!=[NSNull null]) {
                disc.type = key[@"type"];
            }
            if (key[@"subtitle"]!=[NSNull null]) {
                disc.subtitle = key[@"subtitle"];
            }
            
            if ([disc.type intValue]==5 || [disc.type intValue]==11 || [disc.type intValue]==22 || [disc.type intValue] ==33 || [disc.type intValue] == 44 || [disc.type intValue] ==55 || [disc.type intValue] ==66) {
                
                [self.discoveryItems addObject:disc];
            }
            
        }
        [UIView transitionWithView: self.tableView
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCrossDissolve
                        animations: ^(void)
         {
             [self.tableView reloadData];
         }
                        completion: nil];
    }
}


- (void)handleSalonSearchSuccessResult:(NSDictionary *)jsonDictionary {
    self.searchedItems = [NSMutableArray array];
    self.searchedListIsActive = !self.searchedListIsActive;
    
    NSDictionary *dataDict = jsonDictionary;
    NSArray * salonsArray = dataDict[@"message"][@"results"];
    for (NSDictionary *sDict in salonsArray) {
        
        Salon *salon = [Salon salonWithID:[sDict[@"salon_id"] stringValue] ];
        if ([sDict[@"salon_latest_review"]count]!=0) {
            
            salon.hasReview=YES;
            SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:sDict[@"salon_latest_review"][@"user_name"] AndWithLastName:sDict[@"salon_latest_review"][@"user_last_name"]];
            //NSLog(@"first name %@",sDict[@"salon_latest_review"][@"user_name"]);
            salonReview.message = sDict[@"salon_latest_review"][@"message"];
            salonReview.review_image=sDict[@"salon_latest_review"][@"review_image"];
            salonReview.salon_rating=sDict[@"salon_latest_review"][@"salon_rating"];
            salonReview.stylist_rating=sDict[@"salon_latest_review"][@"stylist_rating"];
            salonReview.date_reviewed=sDict[@"salon_latest_review"][@"date_reviewed"];
            salonReview.user_avatar_url=sDict[@"salon_latest_review"][@"user_avatar"];
            salon.salonReview=salonReview;
            
        }
        if (sDict[@"salon_name"] !=[NSNull null]) {
            salon.salon_name=sDict[@"salon_name"];
        }
        if (sDict[@"salon_description"] !=[NSNull null]) {
            salon.salon_description = sDict[@"salon_description"];
        }
        if (sDict[@"salon_phone"] != [NSNull null]) {
            salon.salon_phone = sDict[@"salon_phone"];
        }
        if (sDict[@"salon_lat"] !=[NSNull null]) {
            salon.salon_lat = [sDict[@"salon_lat"] doubleValue];
        }
        if (sDict[@"salon_lon"] != [NSNull null]) {
            salon.salon_long = [sDict[@"salon_lon"] doubleValue];
        }
        if (sDict[@"salon_address_1"] != [NSNull null]) {
            salon.salon_address_1 = sDict[@"salon_address_1"];
        }
        if (sDict[@"salon_address_2"] != [NSNull null]) {
            salon.salon_address_2 = sDict[@"salon_address_2"];
        }
        if (sDict[@"salon_address_3"] != [NSNull null]) {
            salon.salon_address_3 = sDict[@"salon_address_3"];
        }
        if (sDict[@"city_name"] != [NSNull null]) {
            salon.city_name = sDict[@"city_name"];
        }
        if (sDict[@"county_name"]) {
            salon.country_name =sDict[@"county_name"];
        }
        if (sDict[@"country_name"] != [NSNull null]) {
            salon.country_name = sDict[@"country_name"];
        }
        if (sDict[@"salon_landline"] != [NSNull null]) {
            
            salon.salon_landline = sDict[@"salon_landline"];
        }
        if (sDict[@"salon_website"] !=[NSNull null]) {
            salon.salon_website = sDict[@"salon_website"];
        }
        if (sDict[@"salon_image"] != [NSNull null]) {
            salon.salon_image = sDict[@"salon_image"];
        }
        if (sDict[@"salon_type"] != [NSNull null]) {
            salon.salon_type = sDict[@"salon_type"];
        }
        if (sDict[@"rating"] != [NSNull null]) {
            salon.ratings = [sDict[@"rating"] doubleValue];
        }
        if (sDict[@"reviews"] != [NSNull null]) {
            salon.reviews = [sDict[@"reviews"] doubleValue];
        }
        if (sDict[@"salon_tier"] != [NSNull null]) {
            salon.salon_tier = [sDict[@"salon_tier"] doubleValue];
        }
        if (sDict[@"is_favourite"] !=[NSNull null]) {
            salon.is_favourite = [sDict[@"is_favourite"] boolValue];
        }
        if (sDict[@"distance"] !=[NSNull null]) {
            salon.distance = [sDict[@"distance"] doubleValue];
        }
        if (sDict[@"salon_description"] !=[NSNull null]) {
            salon.salon_description= sDict[@"salon_description"];
        }
        if (sDict[@"salon_images"]!=[NSNull null]) {
            
            NSArray *messageArray = sDict[@"salon_images"];
            
            for (NSDictionary * dataDict in messageArray) {
                
                NSString * imagePath = [dataDict objectForKey:@"image_path"];
                
                
                [salon.slideShowGallery addObject:imagePath];
            }
        }
        
        if (sDict[@"rating"]!=[NSNull null]) {
            salon.ratings = [sDict[@"rating"] doubleValue];
        }
        if (sDict[@"reviews"]!=[NSNull null]) {
            salon.reviews = [sDict[@"reviews"] doubleValue];
        }
        
        if ([sDict[@"salon_categories"] count]!=0) {
            salon.salonCategories =  @{
                                       @"Hair" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair"] boolValue]],
                                       @"Nails" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Nails"]boolValue]],
                                       @"Face" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Face"] boolValue]],
                                       @"Body" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Body"] boolValue]],
                                       @"Massage" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Massage"] boolValue]],
                                       @"Hair Removal" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair Removal"] boolValue]],
                                       };
        }
        
        
        
        if ([sDict[@"salon_opening_hours"] count]!=0) {
            
            NSArray * openingHours =sDict[@"salon_opening_hours"];
            for (NSDictionary* day in openingHours) {
                
                OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
                
                [salon.openingHours addObject:openingDay];
            }
            
            
        }
        
        /*
         Used to determine source type which refers to Phorest, iSalon etc
         */
        if (sDict[@"source_id"] != [NSNull null]) {
            salon.source_id = sDict[@"source_id"];
        }
        [self.searchedItems addObject:salon];
        
    }
    
    [UIView transitionWithView: self.tableView
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    animations: ^(void)
     {
         [self.tableView reloadData];
     }
                    completion: nil];
}

#pragma mark - UITableView Delegate & Datasoure
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    DiscoveryItem * disc = [self.discoveryItems objectAtIndex:indexPath.row];
        CategoriesCell *cell = (CategoriesCell *) [self.tableView dequeueReusableCellWithIdentifier:@"CategoriesCell"];
        
        [self configureCetegories:cell with:disc];
    return cell;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return self.discoveryItems.count;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 200;
    }

    return 0;

}

-(void) configureCetegories:(CategoriesCell* ) categoriesCell with:(DiscoveryItem* ) item {
    NSNumber * type = @([item.type intValue]);
    categoriesCell.categoryNameLabel.text = item.title;
    [categoriesCell.backGroundImageView sd_setImageWithURL:[NSURL URLWithString:item.imageURL]
                                placeholderImage:kPlace_Holder_Image];
    [categoriesCell categoryLogo:type];
    
    categoriesCell.categoryLogoImageView.tintColor = [UIColor cloudsColor];
    categoriesCell.categoryLogoImageView.image = [categoriesCell.categoryLogoImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
            [self performSegueWithIdentifier:@"discoveryToBrowse" sender:self];

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"discoveryToBrowse"]) {
        
        
        NSIndexPath * selectedIndexPath = [self.tableView indexPathForSelectedRow];
        DiscoveryItem * disc = [self.discoveryItems objectAtIndex:selectedIndexPath.row];
        TabBrowseViewController * browse = segue.destinationViewController;
        browse.browseTitle=disc.subtitle;
        browse.hidesBottomBarWhenPushed=YES;
        self.hidesBottomBarWhenPushed =NO;
        if  ([disc.type intValue]==2) {
            
            
            if ([disc.lat intValue]!=0 && [disc.lon intValue] !=0) {
                browse.hasDiscoveryLatLong=YES;
                browse.discoveryLat = disc.lat;
                browse.discoveryLon = disc.lon;
                
            }
            
            
        }
        
        if ([disc.type intValue] == 11 || [disc.type intValue] == 22 || [disc.type intValue] == 33 || [disc.type intValue] ==44 || [disc.type intValue] ==55 || [disc.type intValue] ==66) {
            browse.filterByDiscoveryCategory=YES;
            browse.discoveryObjectIdType = [disc.type intValue];
        }
        
        
        
    }
    else if([segue.identifier isEqualToString:@"toLastMinute"]){
        LastMinuteSearchViewController * lm = (LastMinuteSearchViewController *)segue.destinationViewController;
        lm.hidesBottomBarWhenPushed=YES;
        self.hidesBottomBarWhenPushed=NO;
    }
    
}

-(void) showDetails:(Salon*) salon {
    TabNewSalonDetailViewController * detailSalon = [self.storyboard instantiateViewControllerWithIdentifier:@"newSalonDetailVC"];
    detailSalon.salonData = salon;
    
    [self.navigationController pushViewController:detailSalon animated:YES];
}

#pragma mark - Walkthrough delegate
-(void)walkthroughIsDismissed{
    NSLog(@"walkthrough is dismissed");
}

-(void)goToLoginSignUp{
    
    if ([[User getInstance] isUserLoggedIn]) {
        self.shouldLoadDiscovery=YES;
        [self loadDiscovery];
    }
    else{
        [self performSelector:@selector(goToLoginSignUpView) withObject:nil afterDelay:0.5];
    }
    
}

-(void)goToLoginSignUpView{
    TabLoginOrSignUpViewController * loginSignUpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignUpVC"];
    loginSignUpViewController.delegate=self;
    [WSCore setLoginType:LoginTypeWalkthrough];
    [self presentViewController:loginSignUpViewController animated:YES completion:nil];
}


-(void)didCancelLoginSignUpView{
    self.shouldLoadDiscovery=YES;
}

- (void)searchForSalon:(NSString *)text {
    
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSearch_Salons_URL]];
    NSString * params = [NSString stringWithFormat:@"per_page=15"];
    
    
    NSString * search = @"";
    
    /*
     Updated to handle free text typing of face, body etc
     
     */
    if ([text isEqual:@"Body"]) {
        self.catID = @"6";
    }
    else if ([text isEqual: @"Face"]) {
        self.catID=@"5";
        
    }else if ([text isEqual:@"Hair"]) {
        self.catID=@"1";
        
    }
    else if ([text isEqual:@"Hair Removal"]) {
        self.catID=@"2";
        
    }
    else if ([text isEqual:@"Massage"]) {
        self.catID=@"4";
        
    }
    else if ([text isEqual:@"Nails"]) {
        self.catID=@"3";
    }
    else{
        search = text;
    }
    
    
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&search_string=%@",search]];
    
    if (self.catID.length>0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&filter_category_id=%@",self.catID]];
    }
    
    
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%@&longitude=%@",self.currentLocation.lat,self.currentLocation.lon]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%@",kWorldCircumference]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&tier_1=1"]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_2=1"]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_3=1"]];
    
    if ([[User getInstance] isUserLoggedIn]) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken ]]];
    }
    
    params = [params stringByAppendingString:@"&client_version=3"];
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
    
}

/*
 performs loadDiscovery if there are no items in the discoveryItems array
 */
-(void)makeDiscoveryNetworkRequestIfArrayIsEmpty{
    if (self.discoveryItems.count==0) {
        [self loadDiscovery];
    }
}
#pragma Tab bar delegate
- (void)tabBarController:(UITabBarController *)theTabBarController didSelectViewController:(UIViewController *)viewController {
    NSUInteger indexOfTab = [theTabBarController.viewControllers indexOfObject:viewController];
    
    
    
    switch (indexOfTab) {
        case 0:
            [WSCore setTabType:TabTypeDefault];
            break;
        case 1:
            [WSCore setTabType:TabTypeSearch];
            break;
        case 2:
            [WSCore setTabType:TabTypeFavourites];
            break;
        case 3:
            [WSCore setTabType:TabTypeMyBookings];
            break;
        default:
            [WSCore setTabType:TabTypeDefault];
            break;
    }
    
}

/*
 if the tab bar index 4 is  pressed the RightSideMenuController is show instead of the tab
 */
- (BOOL) tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    NSUInteger indexOfTab = [tabBarController.viewControllers indexOfObject:viewController];
    
    
//    if (indexOfTab == 4) {
//        RightSideMenuController *m = [self.storyboard instantiateViewControllerWithIdentifier:@"MVC"];
//        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:m];
//        navCon.transitioningDelegate=self;
//        navCon.modalPresentationStyle = UIModalPresentationCustom;
//        [self.tabBarController presentViewController:navCon animated:YES completion:nil];
//        return NO;
//    }
    
    return YES;
}

#pragma mark - LOCATION

-(void)searchByCurrentLocation{

    self.currentLocation = [[LocationObject alloc] init];

    //init cllocation with users location
    if ([[User getInstance] fetchUsersLocation]!=nil) {
        self.currentLocation.lat=[NSString stringWithFormat:@"%f",[[User getInstance] fetchUsersLocation].coordinate.latitude];
        self.currentLocation.lon=[NSString stringWithFormat:@"%f",[[User getInstance] fetchUsersLocation].coordinate.longitude];
    }
    
}

@end
