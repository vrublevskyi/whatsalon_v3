//
//  FeedbackViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 02/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackViewController : UIViewController

/*! @brief shows the menu */
- (IBAction)showMenu:(id)sender;

@end
