//
//  SalonAnnotationView.m
//  whatsalon
//
//  Created by Graham Connolly on 15/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonAnnotationView.h"

@implementation SalonAnnotationView

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self != nil)
    {
        
        CGRect frame = self.frame;
        frame.size = CGSizeMake(40.0, 40.0);
        self.frame = frame;
        self.backgroundColor = [UIColor clearColor];
        self.centerOffset = CGPointMake(-20, -20);
        
    }
    return self;
}

-(void) drawRect:(CGRect)rect
{
    [[UIImage imageNamed:self.salonImageType] drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
                                                                //10,10,60,60
}

@end
