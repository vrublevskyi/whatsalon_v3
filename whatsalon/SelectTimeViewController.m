//
//  SelectTimeViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 10/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SelectTimeViewController.h"
#import "SalonAvailableTime.h"
#import "FUIButton.h"
#import "ServiceTimeCell.h"

@interface SelectTimeViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *contentViewHolder;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SelectTimeViewController

#pragma mark - Life cycle


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonSubTextColor,
                              NSFontAttributeName:[UIFont systemFontOfSize:17]}];
    
    self.navigationController.navigationBar.tintColor = kWhatSalonSubTextColor;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.title = @"Pick a time";
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonBlue}];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureTableView];
    [self configureNavigationBar];
}
    
    -(void) configureNavigationBar {
        CGRect frame = CGRectMake(0,0, 25,25);
        
        //BACK button
        UIImageView* backImage = [[UIImageView alloc] init];
        backImage.image = [UIImage imageNamed:@"back_arrow.png"];
        backImage.image = [backImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [backImage setTintColor:kWhatSalonSubTextColor];
        
        UIButton *backBttn = [[UIButton alloc] initWithFrame:frame];
        [backBttn setBackgroundImage:backImage.image forState:UIControlStateNormal];
        [backBttn addTarget:self action:@selector(back)
           forControlEvents:UIControlEventTouchUpInside];
        [backBttn setShowsTouchWhenHighlighted:YES];
        UIBarButtonItem *backButton =[[UIBarButtonItem alloc] initWithCustomView:backBttn];
        
        //CHECK MARK Button
        UIImageView* checkMark = [[UIImageView alloc] init];
        checkMark.image = [UIImage imageNamed:@"checkmark_icon"];
        checkMark.image = [checkMark.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [checkMark setTintColor:kWhatSalonSubTextColor];
        [checkMark setContentMode: UIViewContentModeScaleAspectFit];
        
        UIButton *checkBttn = [[UIButton alloc] initWithFrame:frame];
        [checkBttn setBackgroundImage:checkMark.image forState:UIControlStateNormal];
        
        [checkBttn addTarget:self action:@selector(stylistSeelcted)
            forControlEvents:UIControlEventTouchUpInside];
        [checkBttn setShowsTouchWhenHighlighted:YES];
        checkBttn.layer.cornerRadius = 12.5;
        checkBttn.layer.borderWidth = 1;
        checkBttn.layer.borderColor = kWhatSalonSubTextColor.CGColor;
        
        
        [checkBttn.widthAnchor constraintEqualToConstant:25].active = YES;
        [checkBttn.heightAnchor constraintEqualToConstant:25].active = YES;
        
        [backBttn.widthAnchor constraintEqualToConstant:25].active = YES;
        [backBttn.heightAnchor constraintEqualToConstant:25].active = YES;
        
        UIBarButtonItem *checkMarkButton =[[UIBarButtonItem alloc] initWithCustomView:checkBttn];
        
        self.navigationItem.leftBarButtonItem = backButton;
        self.navigationItem.rightBarButtonItem = checkMarkButton;
        
        [self.navigationController.navigationBar
         setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonSubTextColor,
                                  NSFontAttributeName:[UIFont systemFontOfSize:17]}];
        
        self.navigationController.navigationBar.tintColor = kWhatSalonSubTextColor;
        self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
        self.title = @"Pick a Stylist";
    }
    
-(void) back {
    [self dismissViewControllerAnimated:true completion:nil];
}
    
-(void)stylistSeelcted
{
    [self.delegate onTimeSelected:@"9:15AM" sender:self];
}

#pragma mark - TableView

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ServiceTimeCell *cell = (ServiceTimeCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    
    cell.stateSelected = true;
//    SalonAvailableTime * time = [self.timesArray objectAtIndex:indexPath.row];

}

    - (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
    {
        ServiceTimeCell *cell = (ServiceTimeCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        cell.stateSelected = false;
        
    }
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
ServiceTimeCell *cell = (ServiceTimeCell *) [self.tableView dequeueReusableCellWithIdentifier:@"ServiceTimeCell"];
//    SalonAvailableTime * time = [self.timesArray objectAtIndex:indexPath.row];
    
//    cell.serviceNameLabel.text = [NSString stringWithFormat:@"%@",[time getTimeFromDate:time.start_time] ];
    cell.serviceNameLabel.text = @"9:15AM";
    cell.stateSelected = false;


    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
//    return self.timesArray.count;
}

-(void) configureTableView {
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 80)];
    view.backgroundColor=[UIColor clearColor];
    self.tableView.tableFooterView=view;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ServiceTimeCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"ServiceTimeCell"];
}

@end
