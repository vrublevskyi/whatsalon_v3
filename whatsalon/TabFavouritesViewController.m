//
//  TabFavouritesViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 24/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabFavouritesViewController.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "Salon.h"
#import "BrowseTableViewCell.h"
#import "FavMessageView.h"
#import "BrowseViewController.h"
#import "SettingsViewController.h"
#import "UITableView+ReloadTransition.h"
#import "OpeningDay.h"
#import "LoginView.h"
#import "TabLoginOrSignUpViewController.h"
#import "TabNewSalonDetailViewController.h"
#import "BrowseSalonTableViewCell.h"
#import "INTULocationManager.h"
#import "EmptyFavouriteView.h"
#import "MapViewController.h"


/*! @brief represents the favourite load more spinner. */
const int kFavSpinnerCell = 22222;
@interface TabFavouritesViewController ()<GCNetworkManagerDelegate,LoginDelegate,EmptyFavouriteViewDelegate, BrowseSalonTableViewCellDelegate>

/*! @brief represents an array of favourite salons. */
@property (nonatomic) NSMutableArray * favArray;


/*! @brief represents the network manager used to make network requests. */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief represents the location object. */
@property (nonatomic) CLLocation * location;

/*! @brief represent the current page. */
@property (nonatomic) NSInteger currentPage;

/*! @brief represents the total pages. */
@property (nonatomic) NSInteger totalPages;

/*! @brief represents the refresh control. */
@property (nonatomic) UIRefreshControl *  refreshControl;

/*! @brief determines if the login view is showing. */
@property (nonatomic) BOOL loginViewIsShowing;

/*! @brief represents the login view controller place holder. */
@property (nonatomic) UIView *loginViewPlaceHolder;


@end

@implementation TabFavouritesViewController


/*
 notification set up
 sets up notification which is called when the user logs in or logs out via the side menu and refreshes the contents
 */
 -(void)notificationSetUp{
 NSLog(@"Refresh favourite controller");
 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFavScreen) name:kTab_2_Notification object:nil];
 }

/*
 refreshFavScreen
 */
-(void)refreshFavScreen{
    
    
    [self downloadSalonsSetUp];
}

/*
 downloadSalonsSetup
 */
- (void)downloadSalonsSetUp {
    
    self.favArray = [NSMutableArray array];
 
    if ([[User getInstance] isUserLoggedIn]) {
      
        if (self.loginViewPlaceHolder) {
            [self removeUserIsNotLoggedInView];
        }
        self.networkManager = [[GCNetworkManager alloc] init];
        self.networkManager.delegate=self;
        self.networkManager.parentView=self.view;
        
        self.location = [[User getInstance] fetchUsersLocation];
        
        
        [self fetchSalons];
        
        
    }else{
        
        if (self.favArray.count>0) {
            [self resetPage];
        }
       
        [self userIsNotLoggedInView];
    }
}


/*
 removeUserIsNotLoggedInView
 */
-(void)removeUserIsNotLoggedInView{
    NSLog(@"remove the view");
    
    
    self.loginViewPlaceHolder.hidden=YES;
    self.loginViewIsShowing=NO;
    [self.loginViewPlaceHolder removeFromSuperview];
    self.loginViewPlaceHolder = nil;

    
    
}

-(void)didClickOnMapPin:(Salon* )salon {
    MapViewController*viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"mapVC"];
    viewController.salonArray = self.favArray;
    viewController.selelctedSalon = salon;
    viewController.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:viewController animated:YES];
    self.navigationController.hidesBottomBarWhenPushed=NO;
}

/*
 user is not logged in
 */
-(void)userIsNotLoggedInView{
    
    if (!self.loginViewPlaceHolder) {
        
        self.loginViewPlaceHolder = [[UIView alloc] initWithFrame:self.view.frame];
        self.loginViewIsShowing=YES;
        
        UIImageView * backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.loginViewPlaceHolder.frame.size.width, self.loginViewPlaceHolder.frame.size.height)];
        backgroundImage.image = [UIImage imageNamed:kMy_Favourites_placeholder_Image_Name];
        backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
        
        [self.loginViewPlaceHolder addSubview:backgroundImage];
        
        LoginView * noMessage = [[LoginView alloc] init];
        noMessage.view.backgroundColor = [UIColor clearColor];
        [self.loginViewPlaceHolder addSubview:noMessage];
        noMessage.center = CGPointMake(CGRectGetWidth(self.loginViewPlaceHolder.bounds)/2, (CGRectGetHeight(self.loginViewPlaceHolder.bounds)/2)+60.0f);
        
        noMessage.titleLabel.text = @"MY FAVOURITES";
        noMessage.titleLabel.font = [UIFont boldSystemFontOfSize:26.0f];
        noMessage.descriptionLabel.text = @"No Favourites yet \n\n Browse all your favourite salon destinations. Heart a Salon and we'll store them here for you.";
        noMessage.descriptionLabel.font = [UIFont systemFontOfSize:16.0f];
        [noMessage.loginButton addTarget:self action:@selector(showLoginViewForFavourites) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:self.loginViewPlaceHolder];
    }
}

/*
 userDidLoginWithFacebook
 */
-(void)userDidLoginWithFacebook{
    NSLog(@"User did sign up with facebook");
    
    
}

/*
 showLoginViewForFavourites
 
 */
-(void)showLoginViewForFavourites{
    //Go to login controller
    TabLoginOrSignUpViewController * tab = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignUpVC"];
    tab.delegate=self;
    [WSCore setLoginType:LoginTypeFavourites];
    [self presentViewController:tab animated:YES completion:nil];
}

/*
 viewDidAppear
 */
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    [self downloadSalonsSetUp];
    
    
}


/*
 viewWillDisappear
 */
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.networkManager destroy];
    
    
}

/*
 viewWillAppear
 */

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore flatNavBarOnView:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}

/*
 addNoFavSalons
 */

-(void)addNoFavSalons{
    self.tableView.hidden=NO;
    UIView * noDataView = [[UIView alloc] initWithFrame:self.tableView.frame];
    noDataView.backgroundColor=kBackgroundColor;
    
    UIImageView * backgroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    backgroundImage.image = [UIImage imageNamed:kMy_Favourites_placeholder_Image_Name];
    backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    [noDataView addSubview:backgroundImage];
    UIView * transparentView = [[UIView alloc] initWithFrame:self.view.frame];
    transparentView.backgroundColor=kGoldenTintForOverView;
    [backgroundImage addSubview:transparentView];
    
    self.tableView.backgroundView=nil;
    
    FavMessageView * favMessage = [[FavMessageView alloc] init];
    [noDataView addSubview:favMessage];
    favMessage.view.backgroundColor=[UIColor clearColor];
     favMessage.center = CGPointMake(CGRectGetWidth(noDataView.bounds)/2, (CGRectGetHeight(noDataView.bounds)/2)+60.0f);
    favMessage.messageTitle.textColor = [UIColor whiteColor];
    favMessage.messageTitle.font = [UIFont boldSystemFontOfSize:26.0f];
    favMessage.messageText.textColor = [UIColor whiteColor];
    favMessage.messageText.font= [UIFont systemFontOfSize:16.0f];
    favMessage.plusButton.imageView.image =  [[UIImage imageNamed:@"plus_filled"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    favMessage.plusButton.tintColor = kWhatSalonBlue;
    [favMessage.startDiscoveringBtn addTarget:self action:@selector(toBrowseTab) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_iPHONE4) {
        favMessage.startDiscoveringBtn.hidden=YES;
    }

        if (IS_iPHONE5_or_Above) {
        UITapGestureRecognizer * tapHeart = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toBrowseTab)];
        tapHeart.numberOfTapsRequired=1;
        [favMessage.messageImage addGestureRecognizer:tapHeart];
        
        [favMessage.plusButton addTarget:self action:@selector(toBrowseTab) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        favMessage.plusButton.hidden=YES;
    }
    
    
    
    
    self.tableView.backgroundView =noDataView;
    
}


-(void) registerCells {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"BrowseSalonTableViewCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"BrowseSalonTableViewCell"];
}

- (void)addNoFavouritesView {
    
    UIView * view = [[UIView alloc] initWithFrame:self.tableView.frame];
    view.backgroundColor=[UIColor blueColor];
    
    EmptyFavouriteView * emptyView = [[EmptyFavouriteView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    emptyView.delegate = self;
    [view addSubview:emptyView];
    self.tableView.backgroundView=view;
    
}

-(void)toBrowseTab{
    
  [self.tabBarController setSelectedIndex:0];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        if (buttonIndex==[alertView cancelButtonIndex]) {
            
        [self.tabBarController setSelectedIndex:4];
        }
    }
}
- (void)fetchSalons {
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params =[params stringByAppendingString:@"&only_favourited=true"];
   

    if (self.location!=nil) {
        params=[params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%f&longitude=%f",self.location.coordinate.latitude,self.location.coordinate.longitude]];
    }
    else{
        params=[params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%f&longitude=%f",self.location.coordinate.latitude,self.location.coordinate.longitude]];
    }
    
    
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%@",kWorldCircumference]];//return all favourites
    params = [params stringByAppendingPathComponent:[NSString stringWithFormat:@"&page=%ld",(long)self.currentPage]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&tier_1=1"]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_2=1"]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_3=1"]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&page_size=30"]];
    params = [params stringByAppendingString:@"&client_version=3"];

    
    NSString * urlST = [NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSearch_Salons_URL];
    
    [self.networkManager loadWithURL:[NSURL URLWithString:urlST] withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
}

-(void)resetPage{

    self.currentPage=1;
    self.currentPage=1;
    self.totalPages=0;
    [self.favArray removeAllObjects];
    
    [self.tableView reloadDataAnimated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore setBookingType:BookingTypeFavourite];
    self.currentPage=1;
  
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.tabBarController.tabBar.frame.size.height+10)];
    footerView.backgroundColor=[UIColor clearColor];
    self.tableView.tableFooterView = footerView;
    
    self.view.backgroundColor = kBackgroundColor;
    self.tableView.backgroundColor = kBackgroundColor;
    
    self.favArray = [NSMutableArray array];
    
    
     [[self tableView] registerClass:[BrowseSalonTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = kBackgroundColor;
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatestSalons)
                  forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    
    [self notificationSetUp];
    [self registerCells];

}

-(void)getLatestSalons{
    self.currentPage=1;
    self.totalPages=0;
    [self.favArray removeAllObjects];
    [self fetchSalons];
    
    
    [self.tableView reloadDataAnimated:YES];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
    
}

- (void)reloadData
{
    
    [self.tableView reloadDataAnimated:YES];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
   
    if(self.favArray.count>0){
        [self.favArray removeAllObjects];
        [self.tableView reloadData];
    }
    if (jsonDictionary[@"message"]!=[NSNull null] && [jsonDictionary[@"message"] isKindOfClass:[NSString class]]) {
        if ([jsonDictionary[@"message"] isEqualToString:@"Sorry, there are no available Salons in your area!"]) {
            [self addNoFavSalons];
        }
    }
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    NSDictionary *dataDict = jsonDictionary;
    
    [self.tableView reloadData];
    if ([dataDict[@"success"] isEqualToString:@"true"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.totalPages=[dataDict[@"message"][@"total_pages"] intValue];
            NSArray * salonsArray = dataDict[@"message"][@"results"];
            for (NSDictionary *sDict in salonsArray) {
                
                Salon *salon = [Salon salonWithID:[sDict[@"salon_id"] stringValue] ];
                
                if ([sDict[@"salon_latest_review"]count]!=0) {
                    
                    salon.hasReview=YES;
                    SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:sDict[@"salon_latest_review"][@"user_name"] AndWithLastName:sDict[@"salon_latest_review"][@"user_last_name"]];
                   
                    salonReview.message = sDict[@"salon_latest_review"][@"message"];
                    salonReview.review_image=sDict[@"salon_latest_review"][@"review_image"];
                    salonReview.salon_rating=sDict[@"salon_latest_review"][@"salon_rating"];
                    salonReview.stylist_rating=sDict[@"salon_latest_review"][@"stylist_rating"];
                    salonReview.date_reviewed=sDict[@"salon_latest_review"][@"date_reviewed"];
                    salonReview.user_avatar_url=sDict[@"salon_latest_review"][@"user_avatar"];
                    salon.salonReview=salonReview;
                    
                }

                if (sDict[@"salon_name"] !=[NSNull null]) {
                    salon.salon_name=sDict[@"salon_name"];
                }
                if (sDict[@"salon_description"] !=[NSNull null]) {
                    salon.salon_description = sDict[@"salon_description"];
                }
                if (sDict[@"salon_phone"] != [NSNull null]) {
                    salon.salon_phone = sDict[@"salon_phone"];
                }
                if (sDict[@"salon_lat"] !=[NSNull null]) {
                    salon.salon_lat = [sDict[@"salon_lat"] doubleValue];
                }
                if (sDict[@"salon_lon"] != [NSNull null]) {
                    salon.salon_long = [sDict[@"salon_lon"] doubleValue];
                }
                if (sDict[@"salon_address_1"] != [NSNull null]) {
                    salon.salon_address_1 = sDict[@"salon_address_1"];
                }
                if (sDict[@"salon_address_2"] != [NSNull null]) {
                    salon.salon_address_2 = sDict[@"salon_address_2"];
                }
                if (sDict[@"salon_address_3"] != [NSNull null]) {
                    salon.salon_address_3 = sDict[@"salon_address_3"];
                }
                if (sDict[@"city_name"] != [NSNull null]) {
                    salon.city_name = sDict[@"city_name"];
                }
                if (sDict[@"county_name"]) {
                    salon.country_name =sDict[@"county_name"];
                }
                if (sDict[@"country_name"] != [NSNull null]) {
                    salon.country_name = sDict[@"country_name"];
                }
                if (sDict[@"salon_landline"] != [NSNull null]) {
                    
                    salon.salon_landline = sDict[@"salon_landline"];
                }
                if (sDict[@"salon_website"] !=[NSNull null]) {
                    salon.salon_website = sDict[@"salon_website"];
                }
                if (sDict[@"salon_image"] != [NSNull null]) {
                    salon.salon_image = sDict[@"salon_image"];
                }
                if (sDict[@"salon_type"] != [NSNull null]) {
                    salon.salon_type = sDict[@"salon_type"];
                }
                if (sDict[@"rating"] != [NSNull null]) {
                    salon.ratings = [sDict[@"rating"] doubleValue];
                }
                if (sDict[@"reviews"] != [NSNull null]) {
                    salon.reviews = [sDict[@"reviews"] doubleValue];
                }
                if (sDict[@"salon_tier"] != [NSNull null]) {
                    salon.salon_tier = [sDict[@"salon_tier"] doubleValue];
                }
                if (sDict[@"is_favourite"] !=[NSNull null]) {
                    salon.is_favourite = [sDict[@"is_favourite"] boolValue];
                }
                if (sDict[@"distance"] !=[NSNull null]) {
                    salon.distance = [sDict[@"distance"] doubleValue];
                }
                if (sDict[@"salon_description"] !=[NSNull null]) {
                    salon.salon_description= sDict[@"salon_description"];
                }
                if (sDict[@"salon_images"]!=[NSNull null]) {
                    
                    NSArray *messageArray = sDict[@"salon_images"];
                    
                    for (NSDictionary * dataDict in messageArray) {
                        
                        NSString * imagePath = [dataDict objectForKey:@"image_path"];
                        
                        
                        [salon.slideShowGallery addObject:imagePath];
                    }
                }
                
                if (sDict[@"rating"]!=[NSNull null]) {
                    salon.ratings = [sDict[@"rating"] doubleValue];
                }
                if (sDict[@"reviews"]!=[NSNull null]) {
                    salon.reviews = [sDict[@"reviews"] doubleValue];
                }
                
                if ([sDict[@"salon_categories"] count]!=0) {
                    salon.salonCategories =  @{
                                               @"Hair" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair"] boolValue]],
                                               @"Nails" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Nails"]boolValue]],
                                               @"Face" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Face"] boolValue]],
                                               @"Body" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Body"] boolValue]],
                                               @"Massage" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Massage"] boolValue]],
                                               @"Hair Removal" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair Removal"] boolValue]],
                                               };
                }
                
               
                
                if ([sDict[@"salon_opening_hours"] count]!=0) {
                    
                    NSArray * openingHours =sDict[@"salon_opening_hours"];
                    for (NSDictionary* day in openingHours) {
                        //NSLog(@"day %@",day);
                        OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
                        
                        [salon.openingHours addObject:openingDay];
                    }
                    
                    
                }
                
                
                /*
                 Used to determine source type which refers to Phorest, iSalon etc
                 */
                if (sDict[@"source_id"] != [NSNull null]) {
                    salon.source_id = sDict[@"source_id"];
                }

                
                
                [self.favArray addObject:salon];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (self.favArray.count==0) {
                    [self addNoFavSalons];
                
                }
                else{
                    self.tableView.backgroundView=nil;
                }
                if (self.refreshControl) {
                  
                    [self.tableView reloadDataAnimated:YES];
                }
                if (self.favArray.count == 0 || self.favArray.count == nil) {
                    [self addNoFavouritesView];
                }
            });
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BrowseSalonTableViewCell *)browseTableViewCell:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    
    BrowseSalonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BrowseSalonTableViewCell" forIndexPath:indexPath];
    cell.myDelegate = self;
    Salon * salon = [self.favArray objectAtIndex:indexPath.row];
    
    if (indexPath.row==0) {
        cell.isNearest=YES;
        
    }
    else{
        cell.isNearest=NO;
    }
    [cell setUpCellWithSalon:salon];

    return cell;
}

-(UITableViewCell *)loadingCell{
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    UIActivityIndicatorView * acitivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    acitivityIndicator.center=cell.center;
    [cell addSubview:acitivityIndicator];
    [acitivityIndicator startAnimating];
    cell.tag = kFavSpinnerCell;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell.tag ==kFavSpinnerCell) {
        self.currentPage++;
        [self fetchSalons];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row<self.favArray.count) {
     return [self browseTableViewCell:indexPath tableView:tableView];
    }
    else{
        
        return [self loadingCell];
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
  
    TabNewSalonDetailViewController * detailSalon = [self.storyboard instantiateViewControllerWithIdentifier:@"newSalonDetailVC"];//new viewcontroller instead of tableviewcontroller
    detailSalon.salonData = [self.favArray objectAtIndex:indexPath.row];
    detailSalon.isFromFavourite=YES;
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailSalon animated:YES];
    self.hidesBottomBarWhenPushed=NO;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.currentPage<self.totalPages) {
        return self.favArray.count+1;
    }
    return self.favArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellHeight;
}

/*
 unwindToTablFavouritesAfterLoginOrSignUp
 */
- (IBAction)unwindToTabFavouritesAfterLoginOrSignUp:(UIStoryboardSegue *)unwindSegue
{
    NSLog(@"unwind to tab favourites");
    [WSCore resetLoginType];
    [WSCore statusBarColor:StatusBarColorBlack];
}

- (void) onStardDiscoveringTapped: (EmptyFavouriteView *) sender {
    self.tabBarController.selectedIndex = 0;
}

@end
