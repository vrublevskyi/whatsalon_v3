//
//  FavouriteViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 23/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "FavouriteViewController.h"
#import "RESideMenu.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "Salon.h"
#import "BrowseTableViewCell.h"
#import "SalonIndividualDetailViewController.h"
#import "FavMessageView.h"
#import "BrowseViewController.h"
#import "SettingsViewController.h"
#import "UITableView+ReloadTransition.h"
#import "OpeningDay.h"




const int kSpinnerCell = 22222;
@interface FavouriteViewController ()<GCNetworkManagerDelegate>

@property (nonatomic) NSMutableArray * favArray;

@property (nonatomic) GCNetworkManager * networkManager;
@property (nonatomic) CLLocation * location;

@property (nonatomic) NSInteger currentPage;
@property (nonatomic) NSInteger totalPages;
@property (nonatomic) UIRefreshControl *  refreshControl;

@end

@implementation FavouriteViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [WSCore setDefaultNavBar:self];

    
    if (!self.isMovingToParentViewController && !self.tabBarController) {
        if (self.favArray.count>0) {
             [self.favArray removeAllObjects];
        }
       
       [self fetchSalons]; 
    }
    
    /*
    if ([self tabBarController]) {
        NSLog(@"Is tab bar");
    }
    else {
        NSLog(@"Is not tab bar");
    }
     */
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
}
-(void)addNoFavSalons{
    UIView * noDataView = [[UIView alloc] initWithFrame:self.tableView.frame];
    noDataView.backgroundColor=kBackgroundColor;
    
    UIImageView * backgroundImage = [[UIImageView alloc] initWithFrame:noDataView.frame];
    backgroundImage.image = [UIImage imageNamed:@"background_candy_stripe"];
    backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    [noDataView addSubview:backgroundImage];
    
    self.tableView.backgroundView=nil;
    
        FavMessageView * favMessage = [[FavMessageView alloc] init];
    favMessage.view.backgroundColor=[UIColor colorWithWhite:1.0 alpha:0.7];
        [noDataView addSubview:favMessage];
        //self.tableView.backgroundView=favMessage;
        favMessage.center=CGPointMake(noDataView.frame.size.width/2.0, noDataView.frame.size.height/2.0);
        favMessage.messageImage.userInteractionEnabled=YES;
    
    if (IS_iPHONE5_or_Above) {
        UITapGestureRecognizer * tapHeart = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toBrowse)];
        tapHeart.numberOfTapsRequired=1;
        [favMessage.messageImage addGestureRecognizer:tapHeart];
        
        [favMessage.plusButton addTarget:self action:@selector(toBrowse) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        favMessage.plusButton.hidden=YES;
    }
    
    
    
    
    self.tableView.backgroundView =noDataView;

}

-(void)toBrowse{
    
    BrowseViewController*viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"browseController"];
    /*
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:
     UIViewAnimationTransitionFlipFromRight
                           forView:self.navigationController.view cache:NO];
    */
    [self.navigationController pushViewController:viewController animated:YES];
    //[UIView commitAnimations];

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        if (buttonIndex==[alertView cancelButtonIndex]) {
            
            SettingsViewController*viewController =
            [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
            [UIView beginAnimations:@"View Flip" context:nil];
            [UIView setAnimationDuration:0.80];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            
            [UIView setAnimationTransition:
             UIViewAnimationTransitionFlipFromRight
                                   forView:self.navigationController.view cache:NO];
            
            [self.navigationController pushViewController:viewController animated:YES];
            [UIView commitAnimations];
        }
    }
}
- (void)fetchSalons {
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params =[params stringByAppendingString:@"&only_favourited=true"];
    
    if (self.location!=nil) {
        params=[params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%f&longitude=%f",self.location.coordinate.latitude,self.location.coordinate.longitude]];
    }

    params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%@",kSearchRadius]];
    params = [params stringByAppendingPathComponent:[NSString stringWithFormat:@"&page=%ld",(long)self.currentPage]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&tier=1"]];
    
    NSString * urlST = [NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSearch_Salons_URL];
    
    NSURL * url = [NSURL URLWithString:urlST];
    
    NSLog(@"Load Salons");
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [WSCore statusBarColor:StatusBarColorBlack];
    self.currentPage=1;
    if (self.navigationController.navigationBar.tintColor !=self.view.tintColor) {
        [WSCore nonTransparentNavigationBarOnView:self];
        self.navigationController.navigationBar.tintColor=self.view.tintColor;
    }
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = footerView;
    
    self.view.backgroundColor = kBackgroundColor;
    self.tableView.backgroundColor = kBackgroundColor;

    self.favArray = [NSMutableArray array];
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    self.location = [[User getInstance] fetchUsersLocation];

    if ([[User getInstance] fetchAccessToken]==nil) {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Login or Sign up" message:@"In order to use the favourites feature, please login, or sign up." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        alert.tag=1;
        
        //[WSCore showLoginSignUpAlert];
        
        return;
    }

    [self fetchSalons];
    
    [[self tableView] registerClass:[BrowseTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = kBackgroundColor;
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatestSalons)
                  forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    
    if (self.isFromMenu) {
        UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menuBarButtonItem"] style:UIBarButtonItemStylePlain target:self action:@selector(showMenu:)];
        self.navigationItem.leftBarButtonItem = menuButton;
    }

}

-(void)getLatestSalons{
    self.currentPage=1;
    self.totalPages=0;
    [self.favArray removeAllObjects];
    [self fetchSalons];
    

    [self.tableView reloadDataAnimated:YES];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }


}

- (void)reloadData
{
    // Reload table data
    //[self.tableView reloadData];
    [self.tableView reloadDataAnimated:YES];
    
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
     NSLog(@"JSON object %@",jsonDictionary);
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    NSDictionary *dataDict = jsonDictionary;
    NSLog(@"JSON object %@",jsonDictionary);
    
    if ([dataDict[@"success"] isEqualToString:@"true"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.totalPages=[dataDict[@"message"][@"total_pages"] intValue];
            NSArray * salonsArray = dataDict[@"message"][@"results"];
            for (NSDictionary *sDict in salonsArray) {
                
                Salon *salon = [Salon salonWithID:[sDict[@"salon_id"] stringValue] ];
                if (sDict[@"salon_name"] !=[NSNull null]) {
                    salon.salon_name=sDict[@"salon_name"];
                }
                if (sDict[@"salon_description"] !=[NSNull null]) {
                    salon.salon_description = sDict[@"salon_description"];
                }
                if (sDict[@"salon_phone"] != [NSNull null]) {
                    salon.salon_phone = sDict[@"salon_phone"];
                }
                if (sDict[@"salon_lat"] !=[NSNull null]) {
                    salon.salon_lat = [sDict[@"salon_lat"] doubleValue];
                }
                if (sDict[@"salon_lon"] != [NSNull null]) {
                    salon.salon_long = [sDict[@"salon_lon"] doubleValue];
                }
                if (sDict[@"salon_address_1"] != [NSNull null]) {
                    salon.salon_address_1 = sDict[@"salon_address_1"];
                }
                if (sDict[@"salon_address_2"] != [NSNull null]) {
                    salon.salon_address_2 = sDict[@"salon_address_2"];
                }
                if (sDict[@"salon_address_3"] != [NSNull null]) {
                    salon.salon_address_3 = sDict[@"salon_address_3"];
                }
                if (sDict[@"city_name"] != [NSNull null]) {
                    salon.city_name = sDict[@"city_name"];
                }
                if (sDict[@"county_name"]) {
                    salon.country_name =sDict[@"county_name"];
                }
                if (sDict[@"country_name"] != [NSNull null]) {
                    salon.country_name = sDict[@"country_name"];
                }
                if (sDict[@"salon_landline"] != [NSNull null]) {
                    
                    salon.salon_landline = sDict[@"salon_landline"];
                }
                if (sDict[@"salon_website"] !=[NSNull null]) {
                    salon.salon_website = sDict[@"salon_website"];
                }
                if (sDict[@"salon_image"] != [NSNull null]) {
                    salon.salon_image = sDict[@"salon_image"];
                }
                if (sDict[@"salon_type"] != [NSNull null]) {
                    salon.salon_type = sDict[@"salon_type"];
                }
                if (sDict[@"rating"] != [NSNull null]) {
                    salon.ratings = [sDict[@"rating"] doubleValue];
                }
                if (sDict[@"reviews"] != [NSNull null]) {
                    salon.reviews = [sDict[@"reviews"] doubleValue];
                }
                if (sDict[@"salon_tier"] != [NSNull null]) {
                    salon.salon_tier = [sDict[@"salon_tier"] doubleValue];
                }
                if (sDict[@"is_favourite"] !=[NSNull null]) {
                    salon.is_favourite = [sDict[@"is_favourite"] boolValue];
                }
                if (sDict[@"distance"] !=[NSNull null]) {
                    salon.distance = [sDict[@"distance"] doubleValue];
                }
                if (sDict[@"salon_description"] !=[NSNull null]) {
                    salon.salon_description= sDict[@"salon_description"];
                }
                if (sDict[@"salon_images"]!=[NSNull null]) {
                    
                    NSArray *messageArray = sDict[@"salon_images"];
                    
                    for (NSDictionary * dataDict in messageArray) {
                        
                        NSString * imagePath = [dataDict objectForKey:@"image_path"];
                        
                        
                        [salon.slideShowGallery addObject:imagePath];
                    }
                }
                
                if (sDict[@"rating"]!=[NSNull null]) {
                    salon.ratings = [sDict[@"rating"] doubleValue];
                }
                if (sDict[@"reviews"]!=[NSNull null]) {
                    salon.reviews = [sDict[@"reviews"] doubleValue];
                }
                
                if ([sDict[@"salon_categories"] count]!=0) {
                    salon.salonCategories =  @{
                                               @"Hair" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair"] boolValue]],
                                               @"Nails" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Nails"]boolValue]],
                                               @"Face" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Face"] boolValue]],
                                               @"Body" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Body"] boolValue]],
                                               @"Massage" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Massage"] boolValue]],
                                               @"Hair Removal" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair Removal"] boolValue]],
                                               };
                }
                
                NSLog(@"%@ categories %@",salon.salon_name,salon.salonCategories);
                
                if ([sDict[@"salon_opening_hours"] count]!=0) {
                    
                    NSArray * openingHours =sDict[@"salon_opening_hours"];
                    for (NSDictionary* day in openingHours) {
                        NSLog(@"day %@",day);
                        OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
                        
                        [salon.openingHours addObject:openingDay];
                    }
                    
                    
                }
                
                NSLog(@"%@ opening hours %@",salon.salon_name,salon.openingHours);
                
                [self.favArray addObject:salon];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (self.favArray.count==0) {
                    [self addNoFavSalons];
                }
                else{
                    self.tableView.backgroundView=nil;
                }
                if (self.refreshControl) {
                     //[self.tableView reloadData];
                    [self.tableView reloadDataAnimated:YES];
                }
               
            });
        });
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BrowseTableViewCell *)browseTableViewCell:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    BrowseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Salon * salon = nil;
    salon = [self.favArray objectAtIndex:indexPath.row];
    [cell setUpCellWithSalon:salon];
    
    return cell;
}

-(UITableViewCell *)loadingCell{
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    UIActivityIndicatorView * acitivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    acitivityIndicator.center=cell.center;
    [cell addSubview:acitivityIndicator];
    [acitivityIndicator startAnimating];
    cell.tag = kSpinnerCell;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell.tag ==kSpinnerCell) {
        self.currentPage++;
        [self fetchSalons];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row<self.favArray.count) {
        return [self browseTableViewCell:indexPath tableView:tableView];
    }
    else{
        
        return [self loadingCell];
    }
    BrowseTableViewCell *cell;
    cell = [self browseTableViewCell:indexPath tableView:tableView];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"favToDetailB" sender:self];

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.currentPage<self.totalPages) {
        return self.favArray.count+1;
    }
    return self.favArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellHeight;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"favToDetailB"]){
        SalonIndividualDetailViewController * detailSalon = (SalonIndividualDetailViewController *)segue.destinationViewController;
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        detailSalon.salonData = [self.favArray objectAtIndex:indexPath.row];
        detailSalon.isFromFavourite=YES;
        
    }

}


- (IBAction)showMenu:(id)sender {
    
    [self.sideMenuViewController presentMenuViewController];
}

- (IBAction)unwindToFavourites:(UIStoryboardSegue *)unwindSegue
{
    NSLog(@"Unwind to favourites");

}
@end
