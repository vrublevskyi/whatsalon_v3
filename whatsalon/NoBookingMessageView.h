//
//  NoBookingMessageView.h
//  whatsalon
//
//  Created by Graham Connolly on 02/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"

@interface NoBookingMessageView : UIView

/*! @brief represents the title label. */
@property (strong, nonatomic) IBOutlet UILabel *bookingTitle;

/*! @brief represents the booking image view. */
@property (strong, nonatomic) IBOutlet UIImageView *bookingImage;

/*! @brief represents the booking text label. */
@property (strong, nonatomic) IBOutlet UILabel *bookingText;

/*! @brief represents the get booking button. */
@property (weak, nonatomic) IBOutlet FUIButton *getBookingBtn;

/*! @brief represents the plus image view. */
@property (weak, nonatomic) IBOutlet UIImageView *plusImageView;

/*! @brief represents the view for holdig the content.*/
@property (strong, nonatomic) IBOutlet UIView *view;
@end
