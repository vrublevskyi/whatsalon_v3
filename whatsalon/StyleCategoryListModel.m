//
//  StyleCategoryListModel.m
//  whatsalon
//
//  Created by Graham Connolly on 04/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "StyleCategoryListModel.h"
#import "StyleObject.h"

@implementation StyleCategoryListModel

static StyleCategoryListModel * singleton;

+(instancetype)getInstance{
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

-(void)setStyles{
    NSMutableDictionary * messageDict = [[NSUserDefaults standardUserDefaults] objectForKey:kStyleListKey];
    femaleArray = [[NSMutableArray alloc] init];
    NSMutableDictionary * femaleDict = messageDict[@"female"];
    for (id key in femaleDict) {
        StyleObject * style =[[StyleObject alloc] init];
        style.styleTypeId= key[@"style_type_id"];
        style.styleTypeName = key[@"style_type_name"];
        
        [femaleArray addObject:style];
    }
    
    maleArray = [[NSMutableArray alloc] init];
    NSMutableDictionary * maleDict = messageDict[@"male"];
    for (id key in maleDict) {
        StyleObject * style =[[StyleObject alloc] init];
        style.styleTypeId= key[@"style_type_id"];
        style.styleTypeName = key[@"style_type_name"];
        
        [maleArray addObject:style];
    }
}

-(NSMutableArray *)fetchFemaleStylesList{
    
    return femaleArray;
}

-(NSMutableArray *)fetchMaleStylesList{
    
    return maleArray;
}

-(int)fetchMaleStartPage{
    
    StyleObject * style = [maleArray objectAtIndex:0];
    
    return [style.styleTypeId intValue];
}

-(int)fetchFemalStartPage{
    StyleObject * style = [femaleArray objectAtIndex:0];
    
    return [style.styleTypeId intValue];
}
@end
