//
//  CategoriesCell.m
//  whatsalon
//
//  Created by admin on 9/25/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import "CategoriesCell.h"

@implementation CategoriesCell

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(void) categoryLogo:(NSNumber *) Id  {
    int number = [Id intValue];

    switch (number) {
        case 0:
        self.categoryLogoImageView.image = [UIImage imageNamed:@"instant_booking_filled_discovery"];
        break;
        case 1:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"heart_filled_discovery"];
        break;
        case 2:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"compass_filled_discovery"];
        break;
        case 3:
         self.categoryLogoImageView.image=[UIImage imageNamed:@"clock_filled_discovery"];
        break;
        case 4:
         self.categoryLogoImageView.image=[UIImage imageNamed:@"experiences_discovery_filled"];
        break;
        case 5:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"map_pin"];
        break;
        case 6:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"jewel_discovery_filled"];
        break;
        case  11:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"hair_service_icon"];
        break;
        case 22:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"wax_service_icon"];
        break;
        case 33:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"nail_service_icon"];
        break;
        case 44:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"massage_service_icon"];
        break;
        case 55:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"face_service_icon"];
        break;
        case 66:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"body_service_icon"];
        break;
    default:
         self.categoryLogoImageView.image = [UIImage imageNamed:@"salons_nearby_discovery_filled"];
        break;
    }
}

@end
