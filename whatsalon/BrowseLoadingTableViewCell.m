//
//  BrowseLoadingTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 20/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "BrowseLoadingTableViewCell.h"

@implementation BrowseLoadingTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundPlaceHolder = [[UIView alloc] init];
        [self.contentView addSubview:self.backgroundPlaceHolder];
        
        self.heartImage = [[UIImageView alloc] init];
        [self.contentView addSubview:self.heartImage];
        
        self.letterBox = [[UIImageView alloc] init];
        [self.contentView addSubview:self.letterBox];
        
        self.titleHolder = [[UIView alloc] init];
        [self.contentView addSubview:self.titleHolder];
        
        self.addressHolder = [[UIView alloc] init];
        [self.contentView addSubview:self.addressHolder];
        
        self.addressHolder2 = [[UIView alloc] init];
        [self.contentView addSubview:self.addressHolder2];
        
        self.kmHolder = [[UIView alloc] init];
        [self.contentView addSubview:self.kmHolder];
    }
    return self;
}

-(void)configureCell{
    CGRect frame = CGRectMake(0, 0, self.frame.size.width, CellHeight);
    self.backgroundPlaceHolder.frame =frame;
    self.backgroundPlaceHolder.backgroundColor = [UIColor silverColor];
  
    CGRect heartFrame = CGRectMake(self.frame.size.width-34, 10, 24, 23);
    self.heartImage.frame = heartFrame;
    self.heartImage.image = [UIImage imageNamed:@"White_Heart_Solid"];
    self.heartImage.image = [self.heartImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.heartImage.tintColor = [UIColor cloudsColor];
    
    CGRect letterBoxFrame = CGRectMake(-5, CellHeight-71, self.frame.size.width+10, 70);
    self.letterBox.frame = letterBoxFrame;
    //self.letterBox.backgroundColor = [UIColor cloudsColor];
    self.letterBox.image = [UIImage imageNamed:@"letter_box_2.png"];
    self.letterBox.image = [self.letterBox.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.letterBox.tintColor=[UIColor whiteColor];
    
    CGRect titleHolderFrame = CGRectMake(8, 177, 160, 14);
    self.titleHolder.frame = titleHolderFrame;
    self.titleHolder.backgroundColor = [UIColor cloudsColor];
    self.titleHolder.layer.cornerRadius=kCornerRadius;
    self.titleHolder.layer.masksToBounds=YES;
    
    CGRect address1Frame = CGRectMake(8, 195, 115, 12);
    self.addressHolder.frame = address1Frame;
    self.addressHolder.backgroundColor= [UIColor
                                          cloudsColor];
    self.addressHolder.layer.cornerRadius=kCornerRadius;
    self.addressHolder.layer.masksToBounds=YES;
    
    CGRect address2Frame = CGRectMake(8, 212, 98, 12);
    self.addressHolder2.frame=address2Frame;
    self.addressHolder2.backgroundColor = [UIColor cloudsColor];
    self.addressHolder2.layer.cornerRadius=kCornerRadius;
    self.addressHolder2.layer.masksToBounds=YES;
    
    CGRect kmFrame = CGRectMake(self.frame.size.width-48, 206, 40, 15);
    self.kmHolder.frame = kmFrame;
    self.kmHolder.backgroundColor = [UIColor cloudsColor];
    self.kmHolder.layer.cornerRadius=kCornerRadius;
    self.kmHolder.layer.masksToBounds=YES;
    
    UIActivityIndicatorView * activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = CGPointMake(self.frame.size.width/2.0f, CellHeight/2.0f);
    activityIndicator.color=[UIColor cloudsColor];
    [self.contentView addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
}
@end
