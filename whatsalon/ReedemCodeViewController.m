//
//  ReedemCodeViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 09/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "ReedemCodeViewController.h"
#import "GCNetworkManager.h"
#import "User.h"

@interface ReedemCodeViewController ()<GCNetworkManagerDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewForRedeemTextField;
@property (weak, nonatomic) IBOutlet UITextField *redeemTextField;
@property (nonatomic) GCNetworkManager * networkManager;

@end

@implementation ReedemCodeViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.warningMessage.text = [NSString stringWithFormat:@"A maximum of %@10 is allowed on the account at any one time, per promotion.",CURRENCY_SYMBOL];
    self.warningMessage.numberOfLines=0;
    self.warningMessage.textColor=kWhatSalonSubTextColor;
    self.subWarningMessage.text = @"Credit can be redeemed against the WhatSalon booking fee.";
    self.subWarningMessage.numberOfLines=0;
    self.subWarningMessage.textColor=kWhatSalonSubTextColor;
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate =self;
    self.networkManager.parentView = self.view;
    [WSCore addTopLine:self.viewForRedeemTextField :[UIColor lightGrayColor]:0.5f];
    [WSCore addBottomLine:self.viewForRedeemTextField :[UIColor lightGrayColor]];
    self.redeemTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStyleBordered target:self action:@selector(submitRedeemCode)];
    self.navigationItem.rightBarButtonItem = doneButton;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{

    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",jsonDictionary[kMessageIndex]] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
   
    [[User getInstance] updateUsersBalance:jsonDictionary[@"message"][@"balance"]];
    //[[NSUserDefaults standardUserDefaults] setObject:jsonDictionary[@"message"][@"balance"] forKey:kUserBalanceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
        
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:jsonDictionary[@"message"][@"message"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    NSLog(@"%@",jsonDictionary[@"message"][@"message"]);
    alert.tag = 1;
    [alert show];
    
    
}
-(void)submitRedeemCode{
    
    [self.view endEditing:YES];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kRedeem_Code_URL]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken ]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&discount_code=%@",self.redeemTextField.text]];
    if ([[User getInstance] fetchKey].length!=0) {
        params =[params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
       // NSLog(@"Params %@",params);
    }
     
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
   
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag ==1) {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            //[self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
