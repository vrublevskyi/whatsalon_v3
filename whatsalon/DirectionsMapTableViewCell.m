//
//  DirectionsMapTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 24/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "DirectionsMapTableViewCell.h"

@implementation DirectionsMapTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.mapImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        self.mapImageView.image = [UIImage imageNamed:@"dummy_map"];
        self.mapImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.mapImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        [self.contentView addSubview:self.mapImageView];
        
        UIView * blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 90)];
        blackView.backgroundColor=[UIColor blackColor];
        blackView.alpha=0.5;
        [self.contentView addSubview:blackView];
        
        self.directionsMapIcon = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 30, 30)];
        self.directionsMapIcon.image = [UIImage imageNamed:@"directions"];
        self.directionsMapIcon.contentMode = UIViewContentModeCenter;
        self.directionsMapIcon.layer.borderColor=[UIColor whiteColor].CGColor;
        self.directionsMapIcon.layer.borderWidth=1.0;
        self.directionsMapIcon.layer.cornerRadius=self.directionsMapIcon.frame.size.width/2.0f;
        self.directionsMapIcon.image = [self.directionsMapIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.directionsMapIcon.tintColor=[UIColor whiteColor];
        [self.contentView addSubview:self.directionsMapIcon];
        
        self.directionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(46, 14, 229, 51)];
        self.directionsLabel.font = [UIFont systemFontOfSize:15.0f];
        self.directionsLabel.textColor=[UIColor whiteColor];
        [self.contentView addSubview:self.directionsLabel];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setImageWithURL : (NSString *)url{
    
    NSString * str = [NSString stringWithString:url];
    NSLog(@"URL %@",url);
    [self.mapImageView sd_setImageWithURL:[NSURL URLWithString:str]
                         placeholderImage:[UIImage imageNamed:@"empty_cell"]];
    self.mapImageView.contentMode=UIViewContentModeScaleToFill;
    
}

@end
