//
//  DiscoveryImageTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 12/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "DiscoveryImageTableViewCell.h"

@implementation DiscoveryImageTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.mainImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, self.contentView.bounds.size.height)];
        self.mainImage.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.mainImage.image = [UIImage imageNamed:@"ucc.jpg"];
        //self.mainImage.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView insertSubview:self.mainImage atIndex:0];;
        
        UIView * grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, self.contentView.bounds.size.height)];
        grayView.backgroundColor = [UIColor darkGrayColor];
        grayView.alpha = 0.3;
        grayView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        [self.contentView insertSubview:grayView atIndex:1];
        
        UIView * holderView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, 320, 100)];
        holderView.backgroundColor = [UIColor clearColor];
        holderView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.contentView insertSubview:holderView atIndex:2];
        holderView.center = self.contentView.center;

        
        self.titleLabel =[[UILabel alloc] initWithFrame:CGRectMake(0, 20, 320, 28)];
        self.titleLabel.text = @"Salons Near Me?";
        self.titleLabel.font = [UIFont boldSystemFontOfSize:24.0f];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.adjustsFontSizeToFitWidth=YES;
        self.titleLabel.textAlignment=NSTextAlignmentCenter;
        //self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        //[self.contentView insertSubview:self.titleLabel atIndex:2];
        [holderView addSubview:self.titleLabel];
        
        self.descriptionLabel =[[UILabel alloc] initWithFrame:CGRectMake(0, 54, 320, 20)];
        self.descriptionLabel.text = @"Cork";
        self.descriptionLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        self.descriptionLabel.textColor = [UIColor whiteColor];
        self.descriptionLabel.adjustsFontSizeToFitWidth=YES;
        self.descriptionLabel.textAlignment=NSTextAlignmentCenter;
        
        [holderView addSubview:self.descriptionLabel];
        self.contentView.clipsToBounds=YES;
        self.clipsToBounds = YES;
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCellImageWithURL: (NSString *)url{
    
    if (url.length<1) {
        //add default iamge
    }
    else{
        //add image
    }
}
@end
