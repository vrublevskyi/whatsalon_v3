//
//  Salon.m
//  whatsalon
//
//  Created by Graham Connolly on 24/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "Salon.h"
#import <objc/runtime.h>
#import "OpeningDay.h"

@implementation Salon

-(instancetype)initWithSalonID : (NSString *)s_id{
    self = [super init];
    if (self) {
        self.salon_id=s_id;
        self.imageGallery = [NSMutableArray array];
        self.slideShowGallery = [NSMutableArray array];
        self.openingHours =[NSMutableArray array];
        //[self getSalonImageGallery];
        
        [self setUpCurrency];
    }
    return self;
}

-(id)init{
    self = [super init];
    if (self) {
        self.slideShowGallery = [NSMutableArray array];
        self.imageGallery =[NSMutableArray array];
        
        [self setUpCurrency];
    }
    return self;
}
+(instancetype) salonWithID:(NSString *)s_id{
    return [[self alloc] initWithSalonID:s_id];
}

-(void)setUpCurrency{
  
    self.currencyName = @"Euro";
    self.currencyId = @"1";
    self.currencySymbolUtf8 = @"\u20ac";
    //\u00A3
    //self.currencySymbolUtf8=@"\u00A3";
}

-(instancetype)initWithDicionary:(NSDictionary *)dictionary{
    if (self = [super init]) {
        
        self.salon_id=dictionary[@"salon_id"];
        self.salon_address_1=dictionary[@"salon_address_1"];
        self.salon_address_2=dictionary[@"salon_address_2"];
        self.salon_address_3=dictionary[@"salon_address_3"];
        self.salon_description=dictionary[@"salon_description"];
        self.salon_image=dictionary[@"salon_image"];
        self.salon_lat=[dictionary[@"salon_lat"] doubleValue];
        self.salon_long=[dictionary[@"salon_long"] doubleValue];
        self.salon_name=dictionary[@"salon_name"];
        self.slideShowGallery = [[NSMutableArray alloc] init];
        
        
        [self setUpCurrency];
        
        
        
        if (dictionary[@"salon_images"]==[NSArray class]) {
            
            NSArray *messageArray = dictionary[@"salon_images"];
            
            for (NSDictionary * dataDict in messageArray) {
                
                NSString * imagePath = [dataDict objectForKey:@"image_path"];
                
                
                [self.slideShowGallery addObject:imagePath];
            }
        }
        
        
    }
    return self;
}


-(NSMutableArray *)placeHolderImageGallery{
    
    NSMutableArray * array = [[NSMutableArray alloc] initWithObjects:@"empty_cell.png",@"empty_cell.png",@"empty_cell.png",@"empty_cell.png",@"empty_cell.png", nil];
    return array;
}


-(NSString *)fetchFullAddress{
    NSString *str = [NSString string];
    if (self.salon_address_1.length!=0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"%@",self.salon_address_1]];
    }
    if(self.salon_address_2.length!=0){
        str = [str stringByAppendingString:[NSString stringWithFormat:@", %@",self.salon_address_2]];
        
    }
    if (self.salon_address_3.length!=0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@", %@",self.salon_address_3]];
        
    }
    if (self.city_name.length!=0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@", %@",self.city_name]];
        
    }
    
    
    return str;
}

-(NSMutableArray *)getSalonImageGalleryWithArray{
    
    NSMutableArray * array = [NSMutableArray array];
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSalon_image_gallery_URL]];
    NSString * params = [NSString stringWithFormat:@"salon_id=%@",self.salon_id];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSession * session= [NSURLSession sharedSession];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        if (data &&dataDict!=nil) {
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    
                    
                    if ([dataDict objectForKey:@"message"] && [dataDict objectForKey:@"message"]!=[NSNull null]) {
                        for (NSDictionary * dict in dataDict[@"message"]) {
                            
                            NSString * url = dict[@"image_path"];
                            [array addObject:url];
                        }
                        
                    }
                    break;
                default:
                    break;
            }
            
        }
        else{
           // NSLog(@"ERROR getting images %@",self.salon_name);
        }
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        [task resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
    
    return array;
}


-(void)getSalonImageGallery{
    
    
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSalon_image_gallery_URL]];
    NSString * params = [NSString stringWithFormat:@"salon_id=%@",self.salon_id];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSession * session= [NSURLSession sharedSession];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
      
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
      
        if (data &&dataDict!=nil) {
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    
                
                    if ([dataDict objectForKey:@"message"] && [dataDict objectForKey:@"message"]!=[NSNull null]) {
                        for (NSDictionary * dict in dataDict[@"message"]) {
                            
                            NSString * url = dict[@"image_path"];
                            [self.imageGallery addObject:url];
                        }

                    }
                    break;
                default:
                    break;
            }
          
        }
        else{
            //NSLog(@"ERROR getting images %@",self.salon_name);
        }
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        [task resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
    
    
}

-(NSMutableArray *)fetchImageGalleryURLS{
   
    return self.imageGallery;
}

-(NSString *)fetchSalonDescription{

    return [NSString stringWithFormat:@"Salon name: %@, Salon id: %@, salon description: %@, Salon_phone %@, Salon lat %f, Salon long %f, Salon Address %@, %@, %@, city Name %@, salon_landline %@",self.salon_name,self.salon_id,self.salon_description,self.salon_phone,self.salon_lat,self.salon_long,self.salon_address_1,self.salon_address_2,self.salon_address_3, self.city_name,self.salon_landline];
        ;
    
}

-(NSMutableArray *)fetchSlideShowGallery{
    
    //NSLog(@"slide show gallery length %lu",(unsigned long)self.slideShowGallery.count);
    return self.slideShowGallery;
}
- (NSDictionary *)dictionaryRepresentation {
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
            [dictionary setObject:value forKey:key];
    }
    
    free(properties);
    
    return dictionary;
}


- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.salon_id = [decoder decodeObjectForKey:@"salon_id"];
        self.salon_name = [decoder decodeObjectForKey:@"salon_name"];
        self.salon_description = [decoder decodeObjectForKey:@"salon_description"];
        self.salon_phone = [decoder decodeObjectForKey:@"salon_phone"];
        self.salon_lat =[decoder decodeDoubleForKey:@"salon_lat"];
        self.salon_long= [decoder decodeDoubleForKey:@"salon_long"];
        self.salon_address_1= [decoder decodeObjectForKey:@"salon_address_1"];
        self.salon_address_2=[decoder decodeObjectForKey:@"salon_address_2"];
        self.salon_address_3=[decoder decodeObjectForKey:@"salon_address_3"];
        self.city_name = [decoder decodeObjectForKey:@"city_name"];
        self.country_name = [decoder decodeObjectForKey:@"country_name"];
        self.county_name = [decoder decodeObjectForKey:@"county_name"];
        self.salon_landline = [decoder decodeObjectForKey:@"salon_landline"];
        self.salon_website = [decoder decodeObjectForKey:@"salon_website"];
        self.ratings = [decoder decodeDoubleForKey:@"ratings"];
        self.reviews = [decoder decodeDoubleForKey:@"reviews"];
        self.is_favourite = [decoder decodeBoolForKey:@"is_favourite"];
        self.salon_tier = [decoder decodeDoubleForKey:@"salon_tier"];
        self.distance = [decoder decodeDoubleForKey:@"distance"];
        self.open = [decoder decodeBoolForKey:@"open"];
        self.isReBooking = [decoder decodeBoolForKey:@"isReBooking"];
        self.imageGallery = [NSMutableArray array];
        self.imageGallery = [decoder decodeObjectForKey:@"imageGallery"];
        self.slideShowGallery = [NSMutableArray array];
        self.slideShowGallery = [decoder decodeObjectForKey:@"slideShowGallery"];
        self.bookingDeposit = [decoder decodeObjectForKey:@"bookingDeposit"];
        self.bookingTotal = [decoder decodeObjectForKey:@"bookingTotal"];
        self.bookingStartTime = [decoder decodeObjectForKey:@"bookingStartTime"];
        self.bookingStylist = [decoder decodeObjectForKey:@"bookingStylist"];
        self.bookingID = [decoder decodeObjectForKey:@"bookingID"];
        self.salonCategories = [[NSMutableDictionary alloc] init];
        self.salonCategories = [decoder decodeObjectForKey:@"salonCategories"];
        self.openingHours = [NSMutableArray array];
        self.openingHours = [decoder decodeObjectForKey:@"openingHours"];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.salon_id forKey:@"salon_id"];
    [encoder encodeObject:self.salon_name forKey:@"salon_name"];
    [encoder encodeObject:self.salon_description forKey:@"salon_description"];
    [encoder encodeObject:self.salon_phone forKey:@"salon_phone"];
    [encoder encodeDouble:self.salon_lat forKey:@"salon_lat"];
    [encoder encodeDouble:self.salon_long forKey:@"salon_long"];
    [encoder encodeObject:self.salon_address_1 forKey:@"salon_address_1"];
    [encoder encodeObject:self.salon_address_2 forKey:@"salon_address_2"];
    [encoder encodeObject:self.salon_address_3 forKey:@"salon_address_3"];
    [encoder encodeObject:self.city_name forKey:@"city_name"];
    [encoder encodeObject:self.country_name forKey:@"country_name"];
    [encoder encodeObject:self.county_name forKey:@"county_name"];
    [encoder encodeObject:self.salon_landline forKey:@"salon_landline"];
    [encoder encodeObject:self.salon_website forKey:@"salon_website"];
    [encoder encodeDouble:self.ratings forKey: @"ratings"];
    [encoder encodeDouble:self.reviews forKey:@"reviews"];
    [encoder encodeBool:self.is_favourite forKey:@"is_favourite"];
    [encoder encodeDouble:self.salon_tier forKey:@"salon_tier"];
    [encoder encodeDouble:self.distance forKey:@"distance"];
    [encoder encodeBool:self.open forKey:@"open"];
    [encoder encodeBool:self.isReBooking forKey:@"isReBooking"];
    [encoder encodeObject:self.imageGallery forKey:@"imageGallery"];
    [encoder encodeObject:self.slideShowGallery forKey:@"slideShowGallery"];
    [encoder encodeObject:self.bookingDeposit forKey:@"bookingDeposit"];
    [encoder encodeObject:self.bookingTotal forKey:@"bookingTotal"];
    [encoder encodeObject:self.bookingStartTime forKey:@"bookingStartTime"];
    [encoder encodeObject:self.bookingStylist forKey:@"bookingStylist"];
    [encoder encodeObject:self.bookingID forKey:@"bookingID"];
    [encoder encodeObject:self.salonCategories forKey:@"salonCategories"];
    [encoder encodeObject:self.openingHours forKey:@"openingHours"];

}


-(BOOL)isSalonOpen: (int)day{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
   // NSLog(@"*****1");
    for (OpeningDay * openingDay in self.openingHours) {
     //   NSLog(@"*****2 %d %d",[openingDay.dayOfWeek intValue],day);
        NSDate *date = [dateFormat dateFromString:openingDay.endTime];
       // NSLog(@"Date%@",date);
        if ([openingDay.dayOfWeek intValue]==day) {
         //   NSLog(@"Found %d %d",(int)openingDay.dayOfWeek,day);
            if (openingDay.isOpened) {
                if ([date timeIntervalSinceNow] < 0.0) {
                    return NO;
                }
            }
            return openingDay.isOpened;
        }
    }
    return NO;
}


-(void)updatedCurrencyName: (NSString *) currencyName WithCurrencyId: (NSString *) currencyId AndWithSymbolUtf8: (NSString *) symbol{
    
    
    self.currencyName = currencyName;
    self.currencyId = currencyId;
    self.currencySymbolUtf8 = symbol;
}

-(SalonType)fetchSalonType{
    switch ([self.source_id intValue]) {
        case 0:
            self.source = @"Phorest";
            self.salonType = SalonTypePhorest;
            break;
        case 1:
            self.source = @"Premier";
            self.salonType = SalonTypePremier;
            break;
        case 2:
            self.source = @"iSalon";
            self.salonType = SalonTypeIsalon;
            break;
            
        default:
            self.source = @"Phorest";
            self.salonType = SalonTypePhorest;
            break;
    }
    return self.salonType;
}
-(NSString *)fetchSalonSource{
 
    switch ([self.source_id intValue]) {
        case 0:
            self.source = @"Phorest";
            self.salonType = SalonTypePhorest;
            break;
        case 1:
            self.source = @"Premier";
            self.salonType = SalonTypePremier;
            break;
        case 2:
            self.source = @"iSalon";
            self.salonType = SalonTypeIsalon;
            break;
            
        default:
            self.source = @"Phorest";
            self.salonType = SalonTypePhorest;
            break;
    }
    return self.source;

}

-(void)updateSalonDetailsWithJsonDictionary: (NSDictionary *) json{
    if (json[@"salon_name"] !=[NSNull null]) {
        self.salon_name=json[@"salon_name"];
    }
    if (json[@"salon_description"] !=[NSNull null]) {
        self.salon_description = json[@"salon_description"];
    }
    if (json[@"salon_phone"] != [NSNull null]) {
        self.salon_phone = json[@"salon_phone"];
    }
    if (json[@"salon_lat"] !=[NSNull null]) {
        self.salon_lat = [json[@"salon_lat"] doubleValue];
    }
    if (json[@"salon_lon"] != [NSNull null]) {
        self.salon_long = [json[@"salon_lon"] doubleValue];
    }
    if (json[@"salon_address_1"] != [NSNull null]) {
        self.salon_address_1 = json[@"salon_address_1"];
    }
    if (json[@"salon_address_2"] != [NSNull null]) {
        self.salon_address_2 = json[@"salon_address_2"];
    }
    if (json[@"salon_address_3"] != [NSNull null]) {
        self.salon_address_3 = json[@"salon_address_3"];
    }
    if (json[@"city_name"] != [NSNull null]) {
        self.city_name = json[@"city_name"];
    }
    if (json[@"county_name"]) {
        self.country_name =json[@"county_name"];
    }
    if (json[@"country_name"] != [NSNull null]) {
        self.country_name = json[@"country_name"];
    }
    if (json[@"salon_landline"] != [NSNull null]) {
        
        self.salon_landline = json[@"salon_landline"];
    }
    if (json[@"salon_website"] !=[NSNull null]) {
        self.salon_website = json[@"salon_website"];
    }
    if (json[@"salon_image"] != [NSNull null]) {
        self.salon_image = json[@"salon_image"];
    }
    if (json[@"salon_type"] != [NSNull null]) {
        self.salon_type = json[@"salon_type"];
    }
    if (json[@"rating"] != [NSNull null]) {
        self.ratings = [json[@"rating"] doubleValue];
    }
    if (json[@"reviews"] != [NSNull null]) {
        self.reviews = [json[@"reviews"] doubleValue];
    }
    if (json[@"salon_tier"] != [NSNull null]) {
        self.salon_tier = [json[@"salon_tier"] doubleValue];
    }
    if (json[@"is_favourite"] !=[NSNull null]) {
        self.is_favourite = [json[@"is_favourite"] boolValue];
    }
    if (json[@"distance"] !=[NSNull null]) {
        self.distance = [json[@"distance"] doubleValue];
    }
    if (json[@"salon_description"] !=[NSNull null]) {
        self.salon_description= json[@"salon_description"];
    }
    
    
    if (json[@"salon_images"]!=[NSNull null]) {
        
        NSArray *messageArray = json[@"salon_images"];
        
        for (NSDictionary * dataDict in messageArray) {
            
            NSString * imagePath = [dataDict objectForKey:@"image_path"];
            
            
            [self.slideShowGallery addObject:imagePath];
        }
    }
    
    if (json[@"rating"]!=[NSNull null]) {
        self.ratings = [json[@"rating"] doubleValue];
    }
    if (json[@"reviews"]!=[NSNull null]) {
        self.reviews = [json[@"reviews"] doubleValue];
    }
    
    if ([json[@"salon_categories"] count]!=0) {
        self.salonCategories =  @{
                                   @"Hair" : [NSNumber numberWithBool:[json[@"salon_categories"][@"Hair"] boolValue]],
                                   @"Nails" : [NSNumber numberWithBool:[json[@"salon_categories"][@"Nails"]boolValue]],
                                   @"Face" : [NSNumber numberWithBool:[json[@"salon_categories"][@"Face"] boolValue]],
                                   @"Body" : [NSNumber numberWithBool:[json[@"salon_categories"][@"Body"] boolValue]],
                                   @"Massage" : [NSNumber numberWithBool:[json[@"salon_categories"][@"Massage"] boolValue]],
                                   @"Hair Removal" : [NSNumber numberWithBool:[json[@"salon_categories"][@"Hair Removal"] boolValue]],
                                   };
    }
    
    
    
    if ([json[@"salon_opening_hours"] count]!=0) {
        
        NSArray * openingHours =json[@"salon_opening_hours"];
        for (NSDictionary* day in openingHours) {
            
            OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
            
            [self.openingHours addObject:openingDay];
        }
        
        
    }
    
    /*
     Used to determine source type which refers to Phorest, iSalon etc
     */
    //NSLog(@"Salon Name %@ Source ID %@",self.salon_name,json[@"source_id"]);
    if (json[@"source_id"] != [NSNull null]) {
        self.source_id = json[@"source_id"];
    }
    //NSLog(@"************* \n\n salon_name %@ \n\n salon id %@ \n\n *************",self.salon_name, self.salon_id);
    [self updateCurrenecyWithJsonDictionary:json];
    [self updateSalonReviewWithJsonDictionary:json];
}
-(void)updateSalonReviewWithJsonDictionary:(NSDictionary *)json{
    
    if ([json[@"salon_latest_review"]count]!=0) {
        
        self.hasReview=YES;
        SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:json[@"salon_latest_review"][@"user_name"] AndWithLastName:json[@"salon_latest_review"][@"user_last_name"]];
       // NSLog(@"first name %@",json[@"salon_latest_review"][@"user_name"]);
        salonReview.message = json[@"salon_latest_review"][@"message"];
        salonReview.review_image=json[@"salon_latest_review"][@"review_image"];
        salonReview.salon_rating=json[@"salon_latest_review"][@"salon_rating"];
        salonReview.stylist_rating=json[@"salon_latest_review"][@"stylist_rating"];
        salonReview.date_reviewed=json[@"salon_latest_review"][@"date_reviewed"];
        salonReview.user_avatar_url=json[@"salon_latest_review"][@"user_avatar"];
        self.salonReview=salonReview;
        
        
        
    }

}

-(void) updateCurrenecyWithJsonDictionary: (NSDictionary *) json{
   
    if (json[@"salon_currency"][@"currency_name"] !=[NSNull null] && json[@"salon_currency"][@"currency_id"] !=[NSNull null] && json[@"salon_currency"][@"symbol_utf8"] !=[NSNull null]) {
        
            [self updatedCurrencyName:json[@"salon_currency"][@"currency_name"] WithCurrencyId:json[@"salon_currency"][@"currency_id"] AndWithSymbolUtf8:json[@"salon_currency"][@"symbol_utf8"]];
        
        
    }
}
@end









