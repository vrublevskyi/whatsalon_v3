//
//  LoginViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 18/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;

- (IBAction)showMenu:(id)sender;
- (IBAction)signUp:(id)sender;
- (IBAction)login:(id)sender;

@end
