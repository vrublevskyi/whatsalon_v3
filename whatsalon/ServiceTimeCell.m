//
//  ServiceTimeCell.m
//  whatsalon
//
//  Created by admin on 10/1/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import "ServiceTimeCell.h"
#import "whatsalon-Swift.h"
@implementation ServiceTimeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCheckMarkImageView:(UIImageView *)checkMarkImageView {
    _checkMarkImageView = checkMarkImageView;
    _checkMarkImageView.image = [UIImage imageNamed:@"checkmark_icon"];
    [_checkMarkImageView setTintColor:[UIColor whiteColor]];

    _checkMarkImageView.image = [_checkMarkImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _checkMarkImageView.layer.cornerRadius =  _checkMarkImageView.bounds.size.width/2;
    _checkMarkImageView.layer.borderWidth = 1;
    _checkMarkImageView.layer.borderColor = kWhatSalonSubTextColor.CGColor;
    }

-(void) setStateSelected:(BOOL)stateSelected {
    _stateSelected = stateSelected;
    self.serviceNameLabel.textColor = _stateSelected ?  kCongratsWordsColor : kWhatSalonSubTextColor;
    _checkMarkImageView.layer.borderColor = _stateSelected ? [UIColor whiteColor].CGColor : kWhatSalonSubTextColor.CGColor;

    if (stateSelected) {
        [Gradients addPinkToPurpleDiagonalGradientWithView:_checkMarkImageView];
    } else {
        _checkMarkImageView.backgroundColor = [UIColor whiteColor];
    }
}
@end
