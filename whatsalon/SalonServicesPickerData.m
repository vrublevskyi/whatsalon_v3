//
//  SalonServicesPickerData.m
//  whatsalon
//
//  Created by Graham Connolly on 16/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonServicesPickerData.h"

@implementation SalonServicesPickerData

/*
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self loadServices];
    }
    return self;
}
*/
- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        [self loadServices];
    }
    return self;
}

-(void)loadServices{
    
    self.arrayOfServices = [[NSMutableArray alloc] initWithObjects:@"",@"Blow Dry(short hair)",@"Blow Dry (Med hair)",@"Blow Dry (long hair)",@"Curly Blow Dry",@"GHD Curls",@"Ladies Cut (stylist)",@"Ladies Cut (senior stylist)",@"Upstyle",@"Upstyle (bridal)",@"Dry Styling Bar",@"Gents Cut (stylist)",@"Gents Cut (senior stylist)",@"Gents Hot Towel Shave",@"3 Week Polish",@"3 Week Polish & Removal",@"Shellac",@"Manicure",@"Mini Manicure",@"Gel Nails",@"Gel Refill",@"Acrylic Nails",@"Acrylic Refill",@"Pedicure",@"Mini Pedicure",@"Paraffin Hand Treatment",@"Paraffin Foot Treatment", nil];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return self.arrayOfServices.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [self.arrayOfServices objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    NSString * selectedRow = [self.arrayOfServices objectAtIndex:row];
    NSLog(@"Picked a service");
    if([self.myDelegate respondsToSelector:@selector(didSelectService:)])
    {
        [self.myDelegate didSelectService:selectedRow];
    }
    
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString * title;
   
    title = [self.arrayOfServices objectAtIndex:row];
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
