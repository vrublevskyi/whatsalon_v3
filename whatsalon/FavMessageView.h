//
//  FavMessageView.h
//  whatsalon
//
//  Created by Graham Connolly on 02/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"

@interface FavMessageView : UIView

/*! @brief represents the view for hosting the content. */
@property (strong, nonatomic) IBOutlet UIView *view;

/*! @brief represents the start discovering button. */
@property (weak, nonatomic) IBOutlet FUIButton *startDiscoveringBtn;

/*! @brief represents the message image. */
@property (strong, nonatomic) IBOutlet UIImageView *messageImage;

/*! @brief represents the title label. */
@property (strong, nonatomic) IBOutlet UILabel *messageTitle;

/*! @brief represents the message label. */
@property (strong, nonatomic) IBOutlet UILabel *messageText;

/*! @brief represents the plus button. */
@property (strong, nonatomic) IBOutlet UIButton *plusButton;


@end
