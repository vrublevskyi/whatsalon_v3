//
//  ResultsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 16/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "ResultsViewController.h"
#import "SearchResultsTableViewMainCell.h"
#import "KAProgressLabel.h"
#import "AlternativeBookingCongratulationsViewController.h"
#import "Job.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "User.h"

@interface ResultsViewController ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@property (nonatomic,assign) BOOL isLeftViewSwipedLeft;
@property (nonatomic,assign) BOOL isLeftViewSwipedRight;
@property (nonatomic) CGRect leftViewFrame;
@property (nonatomic) KAProgressLabel * progressLabel;
@property (nonatomic) NSTimer * timer;
@property (nonatomic) NSURLSessionDataTask * dataTask;
@property (nonatomic) NSTimer *timerListener;
@property (nonatomic) UILabel * altTimeCountLabel;
@property (nonatomic) NSURLSessionDataTask * listenTask;
@property (nonatomic) NSString * confirmID;

@end
int counterValue;
@implementation ResultsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    counterValue = 180;
    
    self.view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.8];
    self.resultsTableView.delegate=self;
    self.resultsTableView.dataSource=self;
    self.tableViewForAlternativeTimes.delegate=self;
    self.tableViewForAlternativeTimes.dataSource=self;
    
    // Register Class for Cell Reuse Identifier
    [self.resultsTableView registerClass:[SearchResultsTableViewMainCell class] forCellReuseIdentifier:@"TimeCell"];
     [self.tableViewForAlternativeTimes registerClass:[SearchResultsTableViewMainCell class] forCellReuseIdentifier:@"AltCell"];
    
    UIView * tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.resultsTableView.tableFooterView=tableFooterView;
    
    UIToolbar * mainToolbar = [[UIToolbar alloc] initWithFrame:self.resultsTableView.frame];
    mainToolbar.alpha=0.9;
    mainToolbar.barStyle=UIBarStyleDefault;
    self.resultsTableView.backgroundView = mainToolbar;
    
    //make navigation bar completely transparent
    [self.navBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    self.navBar.shadowImage = [UIImage new];
    self.navBar.translucent = YES;
    
    [WSCore addBottomLine:self.viewForNavBar :[UIColor blackColor]];
    self.viewForNavBar.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.8];
    
    UIToolbar * toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(5, 0, self.viewForAlternativeTimes.frame.size.width, self.viewForAlternativeTimes.frame.size.height)];
    toolbar.barStyle= UIBarStyleBlack;
    //[self.viewForAlternativeTimes insertSubview:toolbar belowSubview:self.tableViewForAlternativeTimes];
    self.tableViewForAlternativeTimes.backgroundView=nil;
    self.tableViewForAlternativeTimes.backgroundColor=[UIColor clearColor];
    
    UISwipeGestureRecognizer * swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
    swipe.delegate =self;
    [self.viewForAlternativeTimes addGestureRecognizer:swipe];
    swipe.direction = UISwipeGestureRecognizerDirectionLeft;
    
    UISwipeGestureRecognizer * rightSwipePanGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipePanGesture:)];
    rightSwipePanGesture.delegate =self;
    [self.viewForAlternativeTimes addGestureRecognizer:rightSwipePanGesture];
    rightSwipePanGesture.direction=UISwipeGestureRecognizerDirectionRight;

    self.isLeftViewSwipedLeft=NO;
    self.isLeftViewSwipedRight=YES;
    
    [WSCore addBottomLine:self.headerViewTableView :[UIColor whiteColor]];
    
    self.altTimeCountLabel= [[UILabel alloc] initWithFrame:CGRectMake(self.headerViewTableView.frame.size.width-45, (self.headerViewTableView.frame.size.height/2)-15, 30, 30)];
    self.altTimeCountLabel.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    self.altTimeCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.altJobsArray.count];
    self.altTimeCountLabel.font = [UIFont systemFontOfSize: 14.0f];
    self.altTimeCountLabel.textAlignment=NSTextAlignmentCenter;
    self.altTimeCountLabel.layer.masksToBounds=YES;
    //label.layer.cornerRadius = roundf(label.frame.size.width/2);
    self.altTimeCountLabel.layer.cornerRadius=12;
    [self.headerViewTableView addSubview:self.altTimeCountLabel];
    
    [self createProgressLabel];
        
    NSLog(@"Number of jobs %lu",(unsigned long)self.jobsArray.count);

    
    //REMOVE IN DEALLOC
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(confirmJob:) name:@"book_job" object:nil];
    
    if (self.timerListener==nil) {
        NSLog(@"Timer");
        self.timerListener = [NSTimer scheduledTimerWithTimeInterval:15
                                                              target:self
                                                            selector:@selector(listenForAppointment)
                                                            userInfo:nil
                                                             repeats:YES ];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    if (self.jobsArray.count==0 && self.altJobsArray.count>0) {
        NSLog(@"IN here, show drawer");
        [UIView transitionWithView:self.viewForAlternativeTimes duration:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.viewForAlternativeTimes.frame = CGRectMake(50, 64, self.viewForAlternativeTimes.frame.size.width, self.viewForAlternativeTimes.frame.size.height);
        } completion:^(BOOL finished) {
            self.isLeftViewSwipedLeft=YES;
        }];
        
    }

}

-(void)confirmJob:(NSNotification*)notif{
    NSDictionary *dict = [notif userInfo];
    NSString * confirm_id =[dict valueForKey:@"confirm_id"];
    NSString * online_payment = [dict valueForKey:@"online_pay"];
    NSString * full_price = [dict valueForKey:@"full_price"];
    self.confirmID=confirm_id;
    [self bookJob:confirm_id WithPayment:online_payment WithFullPrice:full_price];
}

-(void)bookJob:(NSString *) confirm_id WithPayment: (NSString *) online_pay WithFullPrice: (NSString *) full_pr{
    
    NSLog(@"Confirm ID %@",confirm_id);
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kPayment_Auth_URL]];
    NSString * params=[NSString stringWithFormat:@"confirm_id=%@",confirm_id];
    
    double currentBalance = [[[User getInstance] fetchUsersBalance] doubleValue];
    double online_payment_price = [online_pay doubleValue];
    NSLog(@"Current Balance %f  Online Payment price %f",currentBalance,online_payment_price);
    if (online_payment_price>currentBalance) {
       //params = [params stringByAppendingString:[NSString stringWithFormat:@"carddetails"]];
    }
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:60.0f];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"Booking job dict %@",dataDict);
        if (!error) {
            if (data && dataDict!=nil) {
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                        if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                            dispatch_async(dispatch_get_main_queue()
                                           , ^{
                                               [WSCore dismissNetworkLoading];
                                               UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Booked" message:dataDict[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                               [alert show];
                                               [WSCore getUsersBalance];
                                               alert.tag=3;
                                               
                                           });
                            
                        }
                        else{
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [WSCore dismissNetworkLoading];
                                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Ooops" message:dataDict[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert show];
                            });
                        }
                        break;
                        
                    default:
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [WSCore dismissNetworkLoading];
                            [WSCore showServerErrorAlert];
                        });
                        break;
                }
            }
 
        }
        else{
            [WSCore dismissNetworkLoading];
            [WSCore errorAlert:error];
        }
            }];
    
    if ([WSCore isNetworkReachable]) {
        [WSCore showNetworkLoading];
        [self.dataTask resume];
        
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.listenTask cancel];
    [self.dataTask cancel];
    if (self.isMovingFromParentViewController || self.isBeingDismissed) {
        // Do your stuff here
        [self.timer invalidate];
        self.timer=nil;
    }
}

-(void)createProgressLabel{
    UIView * timerView =[[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-150, self.view.frame.size.width, 150)];
    timerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:timerView];
    
    self.progressLabel = [[KAProgressLabel alloc] initWithFrame:CGRectMake((timerView.frame.size.width/2)-40, (timerView.frame.size.height/2)-40, 80, 80)];
    self.progressLabel.userInteractionEnabled=NO;
    self.progressLabel.numberOfLines=2;
    self.progressLabel.textColor = kDefaultTintColor;
    self.progressLabel.font = [UIFont systemFontOfSize:14.0];
    self.progressLabel.backgroundColor=[UIColor colorWithWhite:1.0 alpha:0.6];
    self.progressLabel.layer.cornerRadius=_progressLabel.frame.size.width/2.0;
    self.progressLabel.layer.masksToBounds=YES;
    self.progressLabel.textAlignment=NSTextAlignmentCenter;
    
    [self.progressLabel setBorderWidth:3.0];
    [self.progressLabel setColorTable: @{
                                         NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[UIColor clearColor],
                                         NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):kDefaultTintColor
                                         }];
    self.progressLabel.text = @"180 seconds";
    self.progressLabel.adjustsFontSizeToFitWidth=YES;
    counterValue = 180;
    [timerView addSubview:self.progressLabel];
    
    [self startTimer];
}


-(void)updateLabel{
    if(counterValue == 1){
		[self killTimer];
	}
    
    
    self.progressLabel.text = [NSString stringWithFormat:@"%d\n seconds", counterValue-1];
    
    counterValue--;
    
}

- (void)killTimer{
	if(self.timer){
		[self.timer invalidate];
		self.timer = nil;
	}
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Times Up" message:@"Unfortunately you have run out of time to make a booking. Please try again soon." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    alert.tag=2;
    [alert show];
}

-(void)startTimer{
    
    if (self.timer==nil) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    selector:@selector(updateLabel)
                                                    userInfo:nil
                                                     repeats:YES ];

    }
    
    [self.progressLabel setProgress:1
                             timing:TPPropertyAnimationTimingLinear
                           duration:180.0
                              delay:0.0];
}


-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    CGRect secondViewFrame;
    secondViewFrame = self.viewForAlternativeTimes.frame;
    secondViewFrame.origin.x +=270;
    self.viewForAlternativeTimes.frame = secondViewFrame;
    self.leftViewFrame = self.viewForAlternativeTimes.frame;
}



-(void)rightSwipePanGesture: (UIGestureRecognizer *)recognizer{
    
    if (self.isLeftViewSwipedLeft == YES) {
        
        
        [UIView transitionWithView:self.viewForAlternativeTimes duration:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            NSLog(@"left view frame %@d",NSStringFromCGRect(self.leftViewFrame));
            self.viewForAlternativeTimes.frame = self.leftViewFrame;
        } completion:^(BOOL finished) {
            self.isLeftViewSwipedLeft=NO;
        }];
        
        
    }
}
-(void)swipeDetected: (UISwipeGestureRecognizer *) swipeDirection{
    if (swipeDirection.direction == UISwipeGestureRecognizerDirectionLeft) {
       
        [UIView transitionWithView:self.viewForAlternativeTimes duration:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.viewForAlternativeTimes.frame = CGRectMake(50, 64, self.viewForAlternativeTimes.frame.size.width, self.viewForAlternativeTimes.frame.size.height);
        } completion:^(BOOL finished) {
            self.isLeftViewSwipedLeft=YES;
        }];

    }
}

-(void)handlePan: (UIPanGestureRecognizer *)recognizer{
    CGPoint translation = [recognizer translationInView:self.viewForAlternativeTimes];
    
    
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SearchResultsTableViewMainCell * cell;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (tableView == self.tableViewForAlternativeTimes) {
        cell =[tableView dequeueReusableCellWithIdentifier:@"AltCell" forIndexPath:indexPath];
        
        Job * job = [self.altJobsArray objectAtIndex:indexPath.row];
        cell.priceLabel.text = [NSString stringWithFormat:@"%@%@",job.currency_sign,job.full_price];
        cell.job_online_payment = job.online_payment_price;
        
        cell.titleLabel.text = job.salon_name;
        cell.addressLabel.text = job.address;
        cell.addressLabel.adjustsFontSizeToFitWidth=YES;
        cell.reviewsLabel.text = [NSString stringWithFormat:@"%@ review(s)",job.total_reviews];
        cell.textInfo.text =job.salon_description_full;
        cell.job_confirm_id = job.confirm_ID;
        cell.job_price=job.full_price;
        
        
        if (job.appt_time.length) {
            NSLog(@"Job time %@",job.appt_time);
            NSString * jobTime = job.appt_time;
            NSString * time = [jobTime substringFromIndex:[jobTime length]-5];
            NSLog(@"Time %@",time);
            NSString * hours;
            hours= [time substringToIndex:2];
            NSLog(@"Hours %@",hours);
            NSString * mins = [time substringFromIndex:[time length] -2];
            NSLog(@"Mins %@",mins);
            cell.hoursLabel.text = hours;
            cell.minsLabel.text = mins;
        }
        
        if (job.image_url.length <1) {
            cell.salonImageV.image = [UIImage imageNamed:@"empty_cell"];
            cell.salonImageV.contentMode=UIViewContentModeScaleAspectFill;
            
            
        }
        else{
            [cell.salonImageV sd_setImageWithURL:[NSURL URLWithString:job.image_url]
                                placeholderImage:[UIImage imageNamed:@"empty_cell"]];
        }
        
        if (job.rating ==5) {
            
            UIImage *templateImage = [[UIImage imageNamed:@"white_five_star_rating"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            cell.starImageView.image = templateImage;
            
            
        }else if (job.rating==4){
            UIImage *templateImage = [[UIImage imageNamed:@"white_four_star_rating"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            cell.starImageView.image = templateImage;
            
        }else if (job.rating==3){
            UIImage * templateImage = [[UIImage imageNamed:@"white_three_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.starImageView.image = templateImage;
            
        }
        else if(job.rating==2){
            UIImage * templateImage = [[UIImage imageNamed:@"white_two_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.starImageView.image = templateImage;
            
        }
        else if(job.rating==1){
            UIImage * templateImage = [[UIImage imageNamed:@"white_one_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.starImageView.image = templateImage;
            
        }
        else{
            UIImage * templateImage = [[UIImage imageNamed:@"white_no_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.starImageView.image = templateImage;
            
        }
        cell.starImageView.tintColor=[UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:0.0 alpha:1.0f];

        cell.titleLabel.textColor = [UIColor whiteColor];
        cell.hoursLabel.textColor = [UIColor whiteColor];
        cell.minsLabel.textColor = [UIColor whiteColor];
        cell.addressLabel.textColor = [UIColor whiteColor];
        cell.reviewsLabel.textColor= [UIColor whiteColor];
        cell.priceLabel.textColor = [UIColor whiteColor];
        cell.priceLabel.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        cell.reviewsLabel.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        cell.bookButton.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        cell.progressBarSecondsLabel.textColor = [UIColor whiteColor];
        cell.progressBar.barBackgroundColor= [UIColor colorWithWhite:1.0 alpha:0.5];
        cell.bookButton.backgroundColor = kDefaultTintColor;
        cell.textColor = [UIColor whiteColor];
        cell.textInfo.textColor=[UIColor whiteColor];
        cell.dotsImageView.image=[UIImage imageNamed:@"whitedotsSmall"];
    }
    
    if (tableView==self.resultsTableView) {
       cell = [tableView dequeueReusableCellWithIdentifier:@"TimeCell" forIndexPath:indexPath];
        Job * job = [self.jobsArray objectAtIndex:indexPath.row];
        cell.priceLabel.text = [NSString stringWithFormat:@"%@%@",job.currency_sign,job.full_price];
        cell.job_online_payment = job.online_payment_price;
        
        cell.titleLabel.text = job.salon_name;
        cell.addressLabel.text = job.address;
        cell.addressLabel.adjustsFontSizeToFitWidth=YES;
        cell.reviewsLabel.text = [NSString stringWithFormat:@"%@ review(s)",job.total_reviews];
        cell.textInfo.text =job.salon_description_full;
        cell.job_confirm_id = job.confirm_ID;
        cell.job_price=job.full_price;
        
    
        if (job.appt_time.length) {
            NSLog(@"Job time %@",job.appt_time);
            NSString * jobTime = job.appt_time;
            NSString * time = [jobTime substringFromIndex:[jobTime length]-5];
            NSLog(@"Time %@",time);
            NSString * hours;
            hours= [time substringToIndex:2];
            NSLog(@"Hours %@",hours);
            NSString * mins = [time substringFromIndex:[time length] -2];
            NSLog(@"Mins %@",mins);
            cell.hoursLabel.text = hours;
            cell.minsLabel.text = mins;
        }
        
        if (job.image_url.length <1) {
            cell.salonImageV.image = [UIImage imageNamed:@"empty_cell"];
            cell.salonImageV.contentMode=UIViewContentModeScaleAspectFill;
            
            
        }
        else{
            [cell.salonImageV sd_setImageWithURL:[NSURL URLWithString:job.image_url]
                            placeholderImage:[UIImage imageNamed:@"empty_cell"]];
        }
        
        if (job.rating ==5) {
            
            UIImage *templateImage = [[UIImage imageNamed:@"white_five_star_rating"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            cell.starImageView.image = templateImage;
            
            
        }else if (job.rating==4){
            UIImage *templateImage = [[UIImage imageNamed:@"white_four_star_rating"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            cell.starImageView.image = templateImage;
            
        }else if (job.rating==3){
            UIImage * templateImage = [[UIImage imageNamed:@"white_three_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.starImageView.image = templateImage;
            
        }
        else if(job.rating==2){
            UIImage * templateImage = [[UIImage imageNamed:@"white_two_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.starImageView.image = templateImage;
            
        }
        else if(job.rating==1){
            UIImage * templateImage = [[UIImage imageNamed:@"white_one_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.starImageView.image = templateImage;
            
        }
        else{
            UIImage * templateImage = [[UIImage imageNamed:@"white_no_star_rating"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.starImageView.image = templateImage;
            
        }
        cell.starImageView.tintColor=[UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:0.0 alpha:1.0f];


        cell.bookButton.backgroundColor = kWhatSalonBlue;
        cell.textColor = [UIColor blackColor];

    }
   
        return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView==self.resultsTableView) {
        
        
    }
    
    if (tableView==self.tableViewForAlternativeTimes) {
        
       
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 157;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView==self.resultsTableView) {
        return self.jobsArray.count;
    }
    else if(tableView==self.tableViewForAlternativeTimes){
        return self.altJobsArray.count;
    }
    return 2;
}




- (IBAction)cancel:(id)sender {
    
    UIAlertView * cancelAlert = [[UIAlertView alloc] initWithTitle:@"Cancel" message:@"Stop looking for appointments?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    cancelAlert.tag=1;
    [cancelAlert show];
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (alertView.tag) {
        case 0:
            break;
        case 1:
            if (buttonIndex == 1) {
                [self.timer invalidate];
                self.timer = nil;
                
                [WSCore cancelSearchWithSearchId:self.searchID];
                [self.timer invalidate];
                self.timer=nil;
                [self.timerListener invalidate];
                self.timerListener=nil;
                [self.dataTask cancel];
                [self.listenTask cancel];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            break;
        case 2:
            [self.timer invalidate];
            self.timer = nil;
            [self.dataTask cancel];
            [self.timerListener invalidate];
            self.timerListener=nil;
            [self.dataTask cancel];
            [self.listenTask cancel];
            [self dismissViewControllerAnimated:YES completion:nil];
            
            break;
        case 3:{
            
            
            if (buttonIndex==[alertView cancelButtonIndex]) {
                [self.timer invalidate];
                self.timer=nil;
                [self.timerListener invalidate];
                self.timerListener=nil;
                [self.dataTask cancel];
                [self.listenTask cancel];

                
                AlternativeBookingCongratulationsViewController * altVC = [self.storyboard instantiateViewControllerWithIdentifier:@"altBooking"];
                altVC.isMapView=YES;
                altVC.modalPresentationStyle=UIModalPresentationCurrentContext;
                for (int i =0; i<self.jobsArray.count; i++) {
                    Job * j = [self.jobsArray objectAtIndex:i];
                    if ([j.confirm_ID isEqualToString:self.confirmID]) {
                        
                    }
                }
                
                
                
                [self presentViewController:altVC animated:YES completion:nil];
            }
            break;
        }
        default:
            
            [self dismissViewControllerAnimated:YES completion:^{
              
            }];
            break;
    }
}


//find jbos
-(void)listenForAppointment{
    
    if (self.timerListener) {
        NSLog(@"IN2Listner");
        NSURL * jobsUrl = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kBooking_answers_Tier2_URL]];
        NSString * params = [NSString stringWithFormat:@"search_id=%@",self.searchID];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:jobsUrl cachePolicy:0 timeoutInterval:60.0f];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
        self.listenTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
            NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            if (!error) {
                switch (httpResp.statusCode) {
                    case kOKStatusCode:{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            id item = dataDict[@"message"];
                            if ([dataDict[@"success"] isEqualToString:@"true"] && ![item isKindOfClass:[NSString class]]) {
                                NSLog(@"Got a job %@",dataDict);
                                
                                Job * job = [Job jboWithConfirmID:dataDict[@"message"][@"confirm_id"]];
                                job.address = dataDict[@"message"][@"address"];
                                job.appt_time=dataDict[@"message"][@"appointment_time"];
                                job.currency_sign=dataDict[@"message"][@"currency"][@"sign"];
                                job.full_price=dataDict[@"message"][@"full_price"];
                                job.image_url=dataDict[@"message"][@"image"];
                                job.online_payment_price=dataDict[@"message"][@"online_payment_price"];
                                job.rating = [dataDict[@"message"][@"rating"] intValue];
                                job.salon_lat = [dataDict[@"message"][@"salon_lat"] floatValue];
                                job.salon_lon = [dataDict[@"message"][@"salon_lon"]floatValue];
                                job.salon_phone = dataDict[@"message"][@"salon_phone"];
                                job.total_reviews=dataDict[@"message"][@"total_reviews"];
                                job.salon_description_full=dataDict[@"message"][@"salon_description_full"];
                                job.salon_name=dataDict[@"message"][@"salon_name"];
                                job.proposed_time1=dataDict[@"message"][@"proposed_time1"];
                                
                                if (job.proposed_time1.length==0 || job.proposed_time1 ==nil) {
                                    [self.jobsArray addObject:job];
                                    
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self.resultsTableView reloadData];
                                        
                                    });
                                }
                                else{
                                    [self.altJobsArray addObject:job];
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        self.altTimeCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.altJobsArray.count ];
                                        [self.tableViewForAlternativeTimes reloadData];
                                    });
                                }
                                // alt jobs
                                
                                
                            }
                            
                        });
                    }
                        break;
                        
                    default:
                        break;
                }
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [WSCore dismissNetworkLoading];
                    [WSCore errorAlert:error];
                });
            }
            
        }];
        
        if ([WSCore isNetworkReachable]) {
            [self.listenTask resume];
        }
        else{
            [WSCore showServerErrorAlert];
        }
        
    }
    
    
}


@end
