//
//  LinkCCViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 22/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "LinkCCViewController.h"
#import "WSCore.h"
#import "SalonDetailViewController.h"
#import "User.h"
#import "NSString+Validations.h"
//#import "CardIO.h"
#import "UIView+AlertCompatibility.h"
#import "SettingsViewController.h"

@interface LinkCCViewController ()<UIPickerViewDataSource,UIPickerViewDelegate/*,CardIOPaymentViewControllerDelegate*/>

@property (nonatomic,weak) UIView * cardTypeView;
@property (nonatomic,strong) NSArray * cardArray;
@property (nonatomic,strong) UIPickerView * cardPicker;
@property (weak, nonatomic) IBOutlet UIView *viewForCard;
@property (weak, nonatomic) IBOutlet UILabel *cardLabel;
@property (nonatomic,assign) BOOL isCardPicked;
@property (strong, nonatomic) IBOutlet UIButton *scanCard;
@property (nonatomic,assign) BOOL isBackSpacePressed;
- (IBAction)scanCard:(id)sender;

@property (nonatomic) UIView * blackView;


//month label

@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (nonatomic) BOOL isExpiryPicker;
@property (nonatomic) NSMutableArray *years;
@property (nonatomic) NSMutableArray *months;
@property (nonatomic) UIView * expiryView;
@property (nonatomic) NSInteger monthRow;
@property (nonatomic) NSInteger yearRow;
@property (weak, nonatomic) IBOutlet UIImageView *lockImage;

@property (nonatomic) NSInteger cardRow;
@end

/*
 Handles four scenarios
 Settings
 Tier1
 Tier2
 Map
 
 
 */
@implementation LinkCCViewController
- (IBAction)cardType:(id)sender {
    
    [self.view endEditing:YES];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds)-180, CGRectGetWidth(self.view.bounds), 180)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(254, 0, 66, 44)];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneCardType) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 66, 44)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelCardType) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancelButton];
    
    self.cardTypeView = view;
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, 50, 320, 110)];
    picker.delegate=self;
    picker.dataSource=self;
    self.cardPicker =picker;
    [self.cardTypeView addSubview:self.cardPicker];

    [self.view addSubview:self.cardTypeView];
    

    if (self.parentViewController == [SalonDetailViewController class]) {
        self.onlyAddCard =YES;
    }
    

    self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.viewForNavigationBar.frame), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-CGRectGetHeight(self.cardTypeView.bounds) - CGRectGetHeight(self.viewForNavigationBar.frame))];
    self.blackView.backgroundColor=[UIColor lightGrayColor];
    self.blackView.alpha=0.7;
    [self.view addSubview:self.blackView];
    
}



-(void)doneCardType{
    self.cardRow = [self.cardPicker selectedRowInComponent:0];
    self.cardLabel.text = [NSString stringWithFormat:@"%@",[self.cardArray objectAtIndex:self.cardRow]];
    self.cardLabel.textColor = [UIColor blackColor];

    self.isCardPicked=YES;
    self.cardTypeView.hidden=YES;
    self.blackView.hidden=YES;
    [self.blackView removeFromSuperview];
    self.blackView=nil;
    [self.cardTypeView removeFromSuperview];
    [self.cardPicker removeFromSuperview];
    

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // [CardIOUtilities preload];
}
-(void)cancelCardType{
    self.isCardPicked=NO;
    self.cardTypeView.hidden=YES;
    [self.cardTypeView removeFromSuperview];
    [self.cardPicker removeFromSuperview];
    
    self.cardLabel.text = @"Card Type";
    self.cardLabel.textColor = [UIColor lightGrayColor];
    
    self.blackView.hidden=YES;
    [self.blackView removeFromSuperview];
    self.blackView=nil;
}


#pragma mark - UIPickerView Datasource
// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (self.isExpiryPicker) {
        
        if (component == 0) {
            return self.months.count;
        }
        else {
            return self.years.count;
        }
    }
    
    return self.cardArray.count;
}
#pragma mark - UIPickerView Delegate
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    if (self.isExpiryPicker) {
        
        return 2;
    }
    
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (self.isExpiryPicker) {
        
        if (component == 0) {
            return [self.months objectAtIndex:row];
        }
        else {
            
            return  [NSString stringWithFormat:@"%@",[self.years objectAtIndex:row]];
        }
    }
    
    return [self.cardArray objectAtIndex:row];
}

/*
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (self.isExpiryPicker) {
        
        self.yearRow = [pickerView selectedRowInComponent:1];
        self.monthRow= [pickerView selectedRowInComponent:0];
        
    }
    else{
        
        self.cardRow = [pickerView selectedRowInComponent:0];
        
    }
}

*/
#pragma mark - UIViewController delegate methods

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.monthLabel.textColor=[UIColor lightGrayColor];
    self.monthLabel.userInteractionEnabled=YES;
    

    if (self.onlyAddCard)
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    NSLog(@"IS %d",self.isFromLocalSignUpBookingScreen);
    [WSCore addTopLine:self.viewForCCNumber :kCellLinesColour: 0.5f];
    [WSCore addBottomIndentedLine:self.viewForCCNumber :kCellLinesColour];
    [WSCore addBottomIndentedLine:self.viewForFirstName :kCellLinesColour];
    [WSCore addBottomIndentedLine:self.viewForExpiration :kCellLinesColour];
    [WSCore addBottomLine:self.viewForCard :kCellLinesColour];
    [WSCore addBottomLine:self.viewForNavigationBar :kCellLinesColour];
    
    
    self.linkButton.buttonColor = kWhatSalonBlue;
    self.linkButton.shadowColor = kWhatSalonBlueShadow;
    self.linkButton.shadowHeight = 1.5f;
    self.linkButton.cornerRadius = 3.0f;
    [self.linkButton setTitle:@"Save" forState:UIControlStateNormal];
    [self.linkButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    
    
    self.cardNumberTextField.delegate = self;
    self.cVVTextField.delegate =self;
    self.firstNameTextField.delegate =self;
    self.lastNameTextField.delegate =self;


    
  

    [self tapGestureSetUp];
    
   
    NSLog(@"Is being presented");
    [self navigationBarSetUp];
    
    self.cancelBarButton = nil;
    self.cancelBarButton.title=nil;
    
    if (self.onlyAddCard) {
        [self.linkButton setTitle:@"Add Card" forState:UIControlStateNormal];
    }
    
    self.cardArray =[[NSArray alloc] initWithArray:[WSCore getSupportedCardsArray]];
    self.cardPicker.delegate=self;
    self.cardPicker.dataSource=self;
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cardType:)];
    tap.numberOfTapsRequired=1;
    [self.viewForCard addGestureRecognizer:tap];
    
    
    self.isCardPicked=NO;
    
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleDidBecomeActive)
                                                 name: UIApplicationDidBecomeActiveNotification
                                               object: nil];
    
    
    
    self.firstNameTextField.autocapitalizationType=UITextAutocapitalizationTypeSentences;
    self.lastNameTextField.autocapitalizationType=UITextAutocapitalizationTypeSentences;
    
    UITapGestureRecognizer * selectMonthGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectMonth)];
    selectMonthGesture.numberOfTapsRequired=1;
    [self.monthLabel addGestureRecognizer:selectMonthGesture];
    
    self.years = [[NSMutableArray alloc] initWithArray:[WSCore getYearsArrayWithNumberOfYears:5]];
    self.months =[[NSMutableArray alloc] initWithArray:[WSCore getMonthsArray]];
    self.monthLabel.backgroundColor=[UIColor whiteColor];
    
    self.yearRow=0;
    self.monthRow=0;
    
    
    if (IS_iPHONE4) {
        self.lockImage.hidden=YES;
    }

    self.cVVTextField.keyboardType=UIKeyboardTypeNumberPad;
    self.cardNumberTextField.keyboardType=UIKeyboardTypeNumberPad;
    self.cardNumberTextField.autocorrectionType=UITextAutocorrectionTypeNo;
    self.firstNameTextField.autocorrectionType=UITextAutocorrectionTypeNo;
    self.lastNameTextField.autocorrectionType=UITextAutocorrectionTypeNo;
    self.cVVTextField.autocorrectionType=UITextAutocorrectionTypeNo;
}

-(void)selectMonth{
    self.isExpiryPicker=YES;
    
    [self.view endEditing:YES];
    
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-260, self.view.frame.size.width, 260)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(254, 0, 66, 44)];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneExpiryView) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 66, 44)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelExpiryView) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancelButton];
    
    self.expiryView = view;
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 160)];
    picker.delegate=self;
    picker.dataSource=self;
    [picker selectRow:self.monthRow inComponent:0 animated:NO];
    [picker selectRow:self.yearRow inComponent:1 animated:NO];
    self.cardPicker =picker;
    [self.expiryView addSubview:self.cardPicker];
    
    [self.view addSubview:self.expiryView];
    
    
    
    self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.navigationController.navigationBar.frame), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-CGRectGetHeight(self.expiryView.bounds) - CGRectGetHeight(self.navigationController.navigationBar.frame))];
    self.blackView.backgroundColor=[UIColor lightGrayColor];
    self.blackView.alpha=0.7;
    [self.view addSubview:self.blackView];

}

-(void)doneExpiryView{
    
    self.yearRow = [self.cardPicker selectedRowInComponent:1];
    self.monthRow= [self.cardPicker selectedRowInComponent:0];

    self.monthLabel.text = [NSString stringWithFormat:@"%@/%@",[self.months objectAtIndex:self.monthRow],[self.years objectAtIndex:self.yearRow]];
    self.monthLabel.textColor = [UIColor blackColor];
    
    self.isExpiryPicker=NO;
    self.blackView.hidden=YES;
    [self.blackView removeFromSuperview];
    self.blackView=nil;
    self.expiryView.hidden=YES;
    [self.expiryView removeFromSuperview];
    [self.cardPicker removeFromSuperview];
    self.expiryView=nil;
    
    
}

-(void)cancelExpiryView{
    
    self.isExpiryPicker=NO;
    self.expiryView.hidden=YES;
    [self.expiryView removeFromSuperview];
    [self.cardPicker removeFromSuperview];
    
    
    self.blackView.hidden=YES;
    [self.blackView removeFromSuperview];
    self.blackView=nil;
    self.expiryView=nil;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)handleDidBecomeActive{
    
    self.cVVTextField.hidden=NO;
    self.cardNumberTextField.hidden=NO;;
    self.monthLabel.hidden=NO;;
}
-(void)handleEnteredBackground{
  
    self.cVVTextField.hidden=YES;
    self.cardNumberTextField.hidden=YES;
    self.monthLabel.hidden=YES;
}
#pragma mark - Set up methods
- (void)tapGestureSetUp
{
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
}


-(void)navigationBarSetUp{
   
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
  
    //change font style of navigation bar title
    [self.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont systemFontOfSize: 18.0f],
      NSFontAttributeName, nil]];
    
    [self.cancelBarButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize: 18.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    
}



- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

#pragma mark - UITextfield delegate methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self resignFirstResponder];
    
    if (textField == self.cVVTextField) {
        if ([self.cVVTextField.text length] != 0 && [self.firstNameTextField.text length] !=0 && [self.lastNameTextField.text length] !=0 && [self.monthLabel.text length] != 0) {
          
            [self resignFirstResponder];
            
        }
    }
}



#pragma mark - IBActions

- (IBAction)cancel:(id)sender {
    
    
    
    if (self.onlyAddCard) {
       
        //[self performSegueWithIdentifier:@"unwindToSalonDetail" sender:self];
        //unwind to salon detail b
        [self performSegueWithIdentifier:@"unwindToBook" sender:self];
    }
    else{
      [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    
}

-(void)udpateCreditCardDetails{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kPayment_add_card_URL]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&carddetails=%@",[self carddetailsJSON]]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        id datDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error]; // Try to convert your data
        
        if (datDict !=nil && data) {
            switch (httpResp.statusCode) {
                   
                case kOKStatusCode:{
                    dispatch_async(dispatch_get_main_queue(), ^{
                       // [WSCore dismissNetworkLoading];
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        
                        if ([datDict[kSuccessIndex] isEqualToString:@"true"]) {
                            NSLog(@"Add Credit card");
                            [[User getInstance] updateCreditCardDetailsWithFirstName:datDict[@"message"][@"card_first_name"] withLastName:datDict[@"message"][@"card_last_name"] withCardNumber:datDict[@"message"][@"card_number"] withCardType:datDict[@"message"][@"card_type"] withExp:datDict[@"message"][@"expiry"] withPaymentRef:datDict[@"message"][@"payment_ref"] ];
                            
                            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Card Added" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            alert.tag=33;
                            [alert show];
                            
                            
                        }
                        else{
                            
                            [UIView showSimpleAlertWithTitle:@"Oops" message:datDict[@"message"] cancelButtonTitle:@"OK"];
                           
                        }
                        
                        
                    });
                }
                    break;
                    
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[WSCore dismissNetworkLoading];
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        [WSCore showServerErrorAlert];
                        
                    });
                    break;
            }
        }
        else{
            //[WSCore dismissNetworkLoading];
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
                [WSCore showServerErrorAlert];
               
            });
            
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        //[WSCore showNetworkLoading];
        [WSCore showNetworkLoadingOnView:self.view];
        [dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
}

-(NSString *)carddetailsJSON{
    
   
    NSString * str=[NSString stringWithFormat:@"[{\"cardnumber\":\"%@\"},{\"cardname\":\"%@ %@\"},{\"cardtype\":\"%@\"},{\"expdate\":\"%@\"},{\"cvn\":\"%@\"}]",self.cardNumberTextField.text,self.firstNameTextField.text,self.lastNameTextField.text,self.cardLabel.text,self.monthLabel.text,self.cVVTextField.text];
    
    
    
    
    return str;
}

- (IBAction)linkCCCard:(id)sender {
    
    //add save card method
    
    if (self.cardNumberTextField.text.length==0 || self.firstNameTextField.text.length==0 || self.lastNameTextField.text.length==0 || self.isCardPicked==NO) {
        
      
        [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please fill out the form." cancelButtonTitle:@"OK"];
        
        
        return;
    }
    else if(![NSString isNumber:self.cardNumberTextField.text]){
     
        
        [UIView showSimpleAlertWithTitle:@"Card must be numeric" message:@"" cancelButtonTitle:@"OK"];
    
        return;
    }
    else if(![NSString isNumber:self.cVVTextField.text]){
        
        [UIView showSimpleAlertWithTitle:@"CVV must be numeric." message:@"" cancelButtonTitle:@"OK"];
        
        return;
    }
    else if(![NSString validateStringIsExpiryDate:self.monthLabel.text]){
        
        NSLog(@"4");
        
        [UIView showSimpleAlertWithTitle:@"Incorrect Format." message:@"The expiry date must follow MM/YYYY." cancelButtonTitle:@"OK"];
        return;
    }
    else if(![NSString isValidFirstName:self.firstNameTextField.text]){
        
        
        [UIView showSimpleAlertWithTitle:@"First Name Error" message:@"Your first name must be composed of standard english letters. No special characters." cancelButtonTitle:@"OK"];
       
        return;
    }
    else if(![NSString isValidSurname:self.lastNameTextField.text]){
        
        [UIView showSimpleAlertWithTitle:@"Last Name Error" message:@"Your last name must be composed of standard english letters. No special characters." cancelButtonTitle:@"OK"];
        
        return;
    }


    [self udpateCreditCardDetails];
    
    
}

- (IBAction)skipCCEntry:(id)sender {
   
    
   
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:@"Are you sure you want to skip Credit Card entry? You will not be able to continue with your booking." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Skip", nil];
         alert.tag=1;
        [alert show];
    
        
    
    
}


#pragma mark - UIAlertView DelegateMethods
-(void)beginAppearanceTransition:(BOOL)isAppearing animated:(BOOL)animated
{
    [super beginAppearanceTransition:isAppearing animated:animated];
    NSLog(@"**begin %@", self);
}

-(void)endAppearanceTransition
{
    [super endAppearanceTransition];
    NSLog(@"**end** %@", self);
}
- (void)skipCCAlertViewActions:(NSInteger)buttonIndex
{
    if (buttonIndex==1 ) {
        
        NSLog(@"Skip");
        
        if ([self.delegate respondsToSelector:@selector(didSkipCard)]) {
            
            
            
            [self dismissViewControllerAnimated:YES completion:^{
                [self.delegate didSkipCard];
            }];
            
        }else if(self.isFromLocalSignUpBookingScreen){
            
            //pops to bookings screen
            [self popToBookings];
        }
       
        else if(self.onlyAddCard){
           
            
            NSLog(@"Skip and show booking");
            //unwind to salon detail b
            //[self performSegueWithIdentifier:@"unwindToBook" sender:self];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
        
        else{
        
            //pop to settings
            
            [[User getInstance] setLocalSignUp:self.isFromSettings];
            [self popToSettings];

       
        }
        
        
        
    }
}

-(void)popToBookings{
   
    NSLog(@"Unwind Segue");
    [self performSegueWithIdentifier:@"unwindToBook" sender:self];
    
}
-(void)popToSettings{
    
    [self performSegueWithIdentifier:@"backToSettings" sender:self];

}
- (void)linkCardAlertViewActions
{
    
   
    if ([self.delegate respondsToSelector:@selector(didSkipCard)]) {
        
        
        
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate didSkipCard];
        }];
        
    }
    else if(self.isFromLastMin){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if(self.isFromLocalSignUpBookingScreen){
        NSLog(@"Dismiss");
       //[self.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        [self popToBookings];
    }
    
    else if(self.onlyAddCard){
        
        
        NSLog(@"Skip and show booking");
        //unwind to salon detail b
        //[self performSegueWithIdentifier:@"unwindToBook" sender:self];
        //[self dismissViewControllerAnimated:YES completion:nil];
        [self popToBookings];
        
    }
    else{
        
       
        [[User getInstance] setLocalSignUp:self.isFromSettings];
        
        NSLog(@"Back To Settings **");
        [self popToSettings];
        
    }

    
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
  
    switch (alertView.tag) {
        case 1:
            [self skipCCAlertViewActions:buttonIndex];
            break;
        case 33:
             [self linkCardAlertViewActions];
            break;
        default:
            break;
    }
    
    
}

- (IBAction)scanCard:(id)sender {
    /*
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    [self presentViewController:scanViewController animated:YES completion:nil];
     */
}

/*
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    
    // Handle user cancellation here...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}
 */

/*
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    // The full card number is available as info.cardNumber, but don't log that!
    
    // Use the card info...
    
    if (info.cardNumber.length !=0) {
        self.cardNumberTextField.text = info.cardNumber;
    }
    
    if (info.expiryYear != 0 || info.expiryMonth != 0) {
        self.monthLabel.text = [NSString stringWithFormat:@"%02lu/%lu",(unsigned long)info.expiryMonth,(unsigned long)info.expiryYear];
        self.monthLabel.textColor=[UIColor blackColor];
    }
    
    if (info.cvv.length !=0) {
        self.cVVTextField.text = info.cvv;
    }
    
    for (NSString *n in self.cardArray){
        
        if ([n.lowercaseString isEqualToString:[CardIOCreditCardInfo displayStringForCardType:info.cardType usingLanguageOrLocale: [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2]].lowercaseString]) {
            
            self.cardLabel.text = [NSString stringWithFormat:@"%@",n];
            self.cardLabel.textColor = [UIColor blackColor];
            self.isCardPicked=YES;

        }
    }
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}
*/
@end
