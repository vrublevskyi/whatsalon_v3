//
//  FilterSearchViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 03/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "FilterSearchViewController.h"
#import "UISlider+FlatUI.h"
#import "UIColor+FlatUI.h"
#import "UIFont+FlatUI.h"
#import "FUIButton.h"
#import "UIImage+FlatUI.h"

@interface FilterSearchViewController ()

//added UINavigationBar for hosting UINavigationButtonItems - modal view controller does not contain a Navigationbar

/*! @brief represents the navigation bar. */
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

/*! @brief represents the cancel bar button item. */
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;

/*! @brief represents the reset bar button item. */
@property (weak, nonatomic) IBOutlet UIBarButtonItem *resetButton;

/*! @brief represents the save filters button. */
@property (nonatomic)  FUIButton *saveFiltersButton;

/*! @brief represents the view for the services. */
@property (weak, nonatomic) IBOutlet UIView *servicesView;

/*! @brief represents the chosen category id. */
@property (nonatomic) int chosenCategory;

/*! @brief represents the nails title label. */
@property (weak, nonatomic) IBOutlet UILabel *nailsTitle;

/*! @brief represents the massage title label. */
@property (weak, nonatomic) IBOutlet UILabel *massageTitle;

/*! @brief represents the face title label. */
@property (weak, nonatomic) IBOutlet UILabel *faceTitle;

/*! @brief represents the body title label. */
@property (weak, nonatomic) IBOutlet UILabel *bodyTitle;

/*! @brief represents the hair title label. */
@property (weak, nonatomic) IBOutlet UILabel *hairTitle;

/*! @brief represents the hair removal title label. */
@property (weak, nonatomic) IBOutlet UILabel *hairRemoval;

/*! @brief represents the array of titles. */
@property (nonatomic) NSMutableArray * titlesArray;

/*! @brief represents the save button. */
- (IBAction)save:(id)sender;

/*! @brief represents the reset button. */
- (IBAction)reset:(id)sender;

/*! @brief represents the cancel button. */
- (IBAction)cancel:(id)sender;

/*! @brief represents the distance variable. */
@property (nonatomic) int distance;


/*! @brief represents the category button array. */
@property (nonatomic) NSMutableArray * categoryButtonArray;

/*! @brief represents an array of category images. */
@property (nonatomic) NSMutableArray * categoryImagesArray;

/*! @brief represents the view that holds the UISegmentedControl . */
@property (weak, nonatomic) IBOutlet UIView *salonSegmentHolder;

/*! @brief represents the UISegmentedControl for the salon type. */
@property (weak, nonatomic) IBOutlet UISegmentedControl *allInstantSegment;

/*! @brief the UISegmentedControl action for changing the salon type. */
- (IBAction)salonTypeChange:(id)sender;
@end

@implementation FilterSearchViewController

- (void)saveFilterButtonSetUp {
    self.saveFiltersButton = [[FUIButton alloc] init];;
    self.saveFiltersButton.frame = CGRectMake(10, self.view.frame.size.height-60, self.view.frame.size.width-20, 50);
    
    self.saveFiltersButton.buttonColor = kWhatSalonBlue;
    self.saveFiltersButton.shadowColor = kWhatSalonBlueShadow;
    self.saveFiltersButton.shadowHeight = 1.0f;
    self.saveFiltersButton.cornerRadius = 3.0f;
    
    [self.saveFiltersButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.saveFiltersButton setTitle:@"Apply Filter" forState:UIControlStateNormal];
    [self.saveFiltersButton addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveFiltersButton];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    [self saveFilterButtonSetUp];
    
    [WSCore transparentNavigationBar:self.navBar];
    
    self.instantBookingViewHolder.layer.cornerRadius=3.0f;
    self.instantBookingLabel.textColor = [UIColor lightGrayColor];
    
    self.servicesView.layer.cornerRadius=3.0;
    
    
    //index starts at 1 because server side hair category id starts at 1 etc
    
    
  /*
   view controller contains an image, title, and large button to represent the categories
   why all 3?
   It was difficult to set a button to contain a title and have its image above it.
   I therefore opted for an ImageView, UILabel, and a transparent button which covered the two.
   
   The tags are used to establish a relationship between button, label and image e.g all hair has tag 1
   
   Index starts at 1 because the server side hair category_id starts at 1 also
   
   */

    self.hairButton.tag=1;
    self.hairTitle.tag=1;
    self.hairImage.tag=1;
    
    self.waxButton.tag=2;
    self.hairRemoval.tag=2;
    self.hairRemoval.numberOfLines=0;
    self.hairRemovalImage.tag=2;
    
    self.nailsButton.tag=3;
    self.nailsTitle.tag=3;
    self.nailsImage.tag=3;
    
    self.massageTitle.tag=4;
    self.massageButton.tag=4;
    self.massageImage.tag=4;
   
    self.faceTitle.tag=5;
    self.faceButton.tag=5;
    self.faceImage.tag=5;

    
    self.bodyButton.tag=6;
    self.bodyTitle.tag=6;
    self.bodyImage.tag=6;
    
    
    self.categoryButtonArray = [NSMutableArray arrayWithObjects:self.hairButton,self.nailsButton,self.massageButton,self.waxButton,self.faceButton,self.bodyButton, nil];
    self.titlesArray = [NSMutableArray arrayWithObjects:self.hairRemoval,self.massageTitle,self.nailsTitle,self.faceTitle,self.bodyTitle,self.hairTitle, nil];
    self.categoryImagesArray = [NSMutableArray arrayWithObjects:self.hairImage,self.hairRemovalImage,self.nailsImage,self.massageImage,self.faceImage,self.bodyImage, nil];
    
    self.servicesLabel.textColor=[UIColor peterRiverColor];
    self.servicesLabel.font = [UIFont boldSystemFontOfSize:22.0f];
    
    
    //if browse category is set
    if (self.browseCategoryForFiltering!=0) {
        
        for (UIButton * btn in self.categoryButtonArray) {
            
            if (self.browseCategoryForFiltering == btn.tag) {
                
                [self categoryChange:btn];
            }
        }
    }
    else{
        
        for (UILabel * title in self.titlesArray) {
            title.textColor = kWhatSalonSubTextColor;
           
            
           
        }
        for (UIImageView * imgV in self.categoryImagesArray) {
            imgV.image = [imgV.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            imgV.tintColor = kWhatSalonSubTextColor;

        }

    }
    
    //creates flat image to add as background
    UIImage *normalBackgroundImage = [UIImage buttonImageWithColor:self.servicesView.backgroundColor
                                                      cornerRadius:kCornerRadius
                                                       shadowColor:kCellLinesColour
                                                      shadowInsets:UIEdgeInsetsMake(0, 0, 1.0, 0)];
    UIImageView * image = [[UIImageView alloc] initWithFrame:self.servicesView.frame];
    image.image=normalBackgroundImage;
    [self.view addSubview:image];
    self.servicesView.backgroundColor=[UIColor clearColor];
    [self.view bringSubviewToFront:self.servicesView];
    
    //line view used to add seperator between salon segment view and cateogrys
    UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, self.salonSegmentHolder.frame.size.width-30, 1)];
    lineView.backgroundColor = [UIColor cloudsColor];
    [self.salonSegmentHolder addSubview:lineView];
    
    if (self.showingInstant) {
        self.allInstantSegment.selectedSegmentIndex=1;
    }
    else{
        self.allInstantSegment.selectedSegmentIndex=0;
    }

}

//-(void)buttonTouch: (UITapGestureRecognizer*)sender{
//    UILabel * titleLabel = (UILabel *)sender.view;
//    UIButton * btn = [[UIButton alloc] init];
//    btn.tag = titleLabel.tag;
//    
//    [self categoryChange:btn];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)reset:(id)sender {

    
    //self.instantSwitch.on = self.isInstantBookOnly;
    //self.distance=0;
    self.chosenCategory=0;
    self.showingInstant=0;
    self.allInstantSegment.selectedSegmentIndex=0;
    UIButton * resetButton = [[UIButton alloc] init];
    resetButton.tag=0;
    
    
    [self categoryChange:resetButton];
    
    for (UILabel * title in self.titlesArray) {
        [title setTextColor:kWhatSalonSubTextColor];
    }
}


- (IBAction)save:(id)sender {
    
    if ([self.myDelegate respondsToSelector:@selector(filterSearchWithInstantBook:WithDistance:AndWithCategory:)]) {
        
        [self dismissViewControllerAnimated:YES completion:^{
           [self.myDelegate filterSearchWithInstantBook:self.showingInstant WithDistance:self.distance AndWithCategory:self.chosenCategory];
        }];

        
    }
     
    
}

/*
 changes image color
 */
- (IBAction)categoryChange:(id)sender {
    
    UIButton * btn = (UIButton *)sender;
   
    for (UIImageView * imgV in self.categoryImagesArray) {
        if (btn.tag == imgV.tag) {
           
            imgV.image = [imgV.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            imgV.tintColor = kWhatSalonBlue;
         
            self.chosenCategory = (int)btn.tag;
            
            for (UILabel * title in self.titlesArray) {
                if (btn.tag == title.tag) {
                    title.textColor = kWhatSalonBlue;
                }
                else{
                    title.textColor = kWhatSalonSubTextColor;
                    
                }
            }
        }
        else{
            
            imgV.image = [imgV.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            imgV.tintColor = kWhatSalonSubTextColor;
        }
    }
}
- (IBAction)salonTypeChange:(id)sender {
    UISegmentedControl * segment = (UISegmentedControl *)sender;
    
    self.showingInstant = segment.selectedSegmentIndex;
    //NSLog(@"Selected segment %ld Show instant %@",(long)segment.selectedSegmentIndex,StringFromBOOL( self.showingInstant));
}
@end
