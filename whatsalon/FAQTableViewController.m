//
//  FAQTableViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 21/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "FAQTableViewController.h"
#import "FAQItem.h"
#import "FAQDetailViewController.h"
#import "FAQTableViewCell.h"



@interface FAQTableViewController ()<NSURLSessionDataDelegate, UITableViewDataSource, UITableViewDelegate>

/*! @brief represents the dictionary for the JSON. */
@property (strong,nonatomic) NSDictionary * dataDictionary;



/*! @brief represents the array of FAQItems. */
@property (nonatomic,strong) NSMutableArray * faqItems;
- (IBAction)cancel:(id)sender;


@end

@implementation FAQTableViewController





- (void)parseAboutDataJson
{
    id message = [self.dataDictionary objectForKey:@"message"];
    if ([message respondsToSelector:@selector(objectAtIndex:)])
    {
        NSMutableArray* arrItems = [self.dataDictionary objectForKey:@"message"];
        
        [arrItems enumerateObjectsUsingBlock:^(NSMutableDictionary* obj, NSUInteger idx, BOOL *stop)
         {
           
             FAQItem * faqItem = [[FAQItem alloc] init];
             faqItem.title = [obj objectForKey:@"item_title"];
             faqItem.content = [obj objectForKey:@"item_text"];
             
             [self.faqItems addObject:faqItem];
             
         }];
        
    }
    //back to main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        //[WSCore dismissNetworkLoading];
        [WSCore dismissNetworkLoadingOnView:self.view];
    });
}


- (void)successHttpStatusCode:(NSData *)data
{
    NSError *jsonError;
    
    self.dataDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
    
    
    if (!jsonError) {
        
        if ([[self.dataDictionary objectForKey:@"success"] isEqualToString:@"true"]) {
            
            [self parseAboutDataJson];
            
            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Connection Failure" message:@"Connection Failure" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
                //[WSCore dismissNetworkLoading];
                [WSCore dismissNetworkLoadingOnView:self.view];
                
                
                return ;
            });
        }
        
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore flatNavBarOnView:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.faqItems = [[NSMutableArray alloc] init];
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kFAQ_URL]];
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url];
    
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (data) {
            
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
          
            
            if (httpResp.statusCode == 200) {
                
                [self successHttpStatusCode:data];
                
            }
            else{
                
                UIAlertView *serverAlert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:@"A server Error occurred." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [serverAlert show];
                
                
                [WSCore dismissNetworkLoadingOnView:self.view];
                
                return ;
            }
        }
        else{
            [WSCore errorAlert:error];
        }
    }];//end of session task
    
    
    //check network
    if ([WSCore isNetworkReachable]) {
        
        //[WSCore showNetworkLoading];
      
        [WSCore showNetworkLoadingOnView:self.view];
        
        [task resume];
    }
    else{
        
        [WSCore showNetworkErrorAlert];
    }
    
    [self registerCells];
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = footerView;
}

-(void) registerCells {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FAQTableViewCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"FAQTableViewCell"];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return self.faqItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    FAQTableViewCell *cell = (FAQTableViewCell *) [self.tableView dequeueReusableCellWithIdentifier:@"FAQTableViewCell"];

    FAQItem * faqItem =[self.faqItems objectAtIndex:indexPath.row];
    cell.titleLabel.text = faqItem.title;
    cell.arrowImageView.image = [UIImage imageNamed:@"arrowToBottom"];
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [self performSegueWithIdentifier:@"456" sender:self];
   
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"456"]) {
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        FAQDetailViewController * faqDetail = segue.destinationViewController;
        faqDetail.faqDetailItem = [self.faqItems objectAtIndex:indexPath.row];
    }
}
- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
