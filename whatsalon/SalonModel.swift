//
//  SalonModel.swift
//  whatsalon
//
//  Created by admin on 10/6/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import ObjectMapper

//Change to Salon(without model)
class SalonModel: Mappable {
    
    var salonAddress3: String?
    var salonAddress2: String?
    var salonAddress1: String?
    var source: String?
    var open: String?
    
    func mapping(map: Map) {
        salonAddress3 <-             map["salon_address_3"]
        salonAddress2 <-             map["salon_address_2"]
        salonAddress1 <-             map["salon_address_1"]
        source <-                    map["source"]
        open <-                      map["open"]


    }
    required init?(map: Map) {
    }
    
    init() {
    }
}
