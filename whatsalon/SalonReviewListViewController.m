//
//  SalonReviewListViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 18/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SalonReviewListViewController.h"
#import "SalonReviewTableViewCell.h"
#import "GCNetworkManager.h"
#import "UIView+AlertCompatibility.h"
#import "SalonReview.h"

@interface SalonReviewListViewController ()<GCNetworkManagerDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *numberOfReviews;
@property (weak, nonatomic) IBOutlet UITableView *reviewTablView;
@property (nonatomic) GCNetworkManager *manager;
@property (nonatomic) NSMutableArray * reviews;
- (IBAction)cancelButton:(id)sender;

@end

@implementation SalonReviewListViewController

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kUserReviewsCellHeight;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.reviews.count
    ;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SalonReviewTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ReviewCell" forIndexPath:indexPath];
    cell.listView=YES;
    SalonReview * salonReview = [self.reviews objectAtIndex:indexPath.row];
    [cell setUpReviewPageWithReview:salonReview AndWithNumberOfReviews:self.salonData.reviews];
    cell.backgroundView=nil;
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor clearColor];
    [WSCore addDarkBlurToView:self.view];
    
    self.numberOfReviews.textColor=[UIColor cloudsColor];
    
    [self.reviewTablView registerClass:[SalonReviewTableViewCell class] forCellReuseIdentifier:@"ReviewCell"];
    
    self.manager = [[GCNetworkManager alloc] init];
    self.manager.delegate=self;
    self.manager.parentView=self.view;
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSalon_Reviews_URL]];
    NSString * params = [NSString stringWithFormat:@"salon_id=%@",self.salonData.salon_id];
    [self.manager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
    self.reviewTablView.backgroundColor=[UIColor clearColor];
    self.reviewTablView.backgroundView=nil;
    
    self.reviewTablView.delegate=self;
    self.reviewTablView.dataSource=self;
    [WSCore addTopLine:self.reviewTablView :[UIColor cloudsColor] :0.5f];
    
    if ((int)self.salonData.reviews==1) {
        self.numberOfReviews.text = [NSString stringWithFormat:@"%d review",(int)self.salonData.reviews];
    }
    else{
        self.numberOfReviews.text = [NSString stringWithFormat:@"%d reviews",(int)self.salonData.reviews];
    }
    
    self.reviews = [NSMutableArray array];
    self.numberOfReviews.font = [UIFont boldSystemFontOfSize:17.0f];
    self.reviewTablView.hidden=YES;
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.reviewTablView.tableFooterView=footerView;
 
    self.reviewTablView.separatorStyle=UITableViewCellSeparatorStyleNone;
}

#pragma mark - GCNetworkManager
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{

    [UIView showSimpleAlertWithTitle:@"Oops" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Bookings dictionary %@",jsonDictionary);
    NSArray * salonsArray = jsonDictionary[@"message"];
    for (NSDictionary *sDict in salonsArray) {
        SalonReview *salonReview;
        if ([sDict[@"user_profile"] count]>0) {
          salonReview = [[SalonReview alloc] initWithReviewer:sDict[@"user_profile"][@"user_name"] AndWithLastName:sDict[@"user_profile"][@"user_last_name"]];
            salonReview.user_avatar_url=sDict[@"user_profile"][@"user_avatar"];

        }else{
            salonReview = [[SalonReview alloc] init];
        }
        
        salonReview.message = sDict[@"message"];
        salonReview.review_image=sDict[@"review_image"];
        salonReview.salon_rating=sDict[@"salon_rating"];
        salonReview.stylist_rating=sDict[@"stylist_rating"];
        salonReview.date_reviewed=sDict[@"date_reviewed"];
        
        [self.reviews addObject:salonReview];
    }
    
    [self.reviewTablView reloadData];
    self.reviewTablView.hidden=NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelButton:(id)sender {
    
    [self.manager cancelTask];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
