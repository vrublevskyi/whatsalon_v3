//
//  AboutMenuViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 22/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutMenuViewController : UIViewController


/*! 
 @brief determines if it is from the right side menu. 
 */
@property (nonatomic) BOOL isFromRightSideMenu;
@end
