//
//  SalonServiceCategory.h
//  whatsalon
//
//  Created by Graham Connolly on 15/10/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header SalonServiceCategory.h
  
 @brief This is the header file represents the SalonServiceCategory.h
   
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.1
 */

#import <Foundation/Foundation.h>

/*!
 @typedef ServiceType
  
 @brief  An enum representing the different service type.
  
 @discussion Represents the different service types.
  
 
 @field ServiceTypeHair         - represents the Hair Service category.
 
 @field ServiceTypeHairRemoval  - represents the Hair Removal category.
 
 @field ServiceTypeNails        - represents the nail category.
 
 @field ServiceTypeMassage      - represents the massage category.
 
 @field ServiceTypeFace         - represents the face category.
 
 @field ServiceTypeBody         - represents the body category.
 */
typedef enum ServiceType : NSUInteger{
    ServiceTypeHair =0,
    ServiceTypeHairRemoval,
    ServiceTypeNails,
    ServiceTypeMassage,
    ServiceTypeFace,
    ServiceTypeBody
} ServiceType;


@interface SalonServiceCategory : NSObject

/*! @brief represents the ServiceType enum. */
@property (nonatomic) ServiceType serviceType;

/*! @brief represnts the service image. */
@property (nonatomic) UIImage * serviceImage;


//@property (nonatomic) BOOL doesService;



/*! @brief returns the service type.
 
    @return ServiceType
 */
-(ServiceType) fetchServiceType;

/*! @brief gets the service image.
 
    @return UIImage
 
 */
-(UIImage *) fetchServiceImage;

/*! @brief creates an instancetype of the SalonServiceCategory.
 
    @param s_t ServiceType
 
    @return instancetype
 
 */
-(instancetype) initWithServiceType:(ServiceType)s_t;

/*! @brief creates an instancetype of the SalonServiceCategory.
 
    @param s_t ServiceType
 
    @return instancetype
 
 */
+(instancetype) serviceWithServiceType: (ServiceType) s_t;

@end
