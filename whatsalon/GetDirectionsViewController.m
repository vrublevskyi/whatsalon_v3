//
//  GetDirectionsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 28/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//


/*!
 @header GetDirectionsViewController.h
  
 @brief This is the header file for the GetDirectionsViewController

  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd.
 @version    -
 */

#import "GetDirectionsViewController.h"
#import "SalonAnnotation.h"
#import "SalonAnnotationView.h"




@interface GetDirectionsViewController ()<CLLocationManagerDelegate>

/*! @brief represents a MKMapItem */
@property (nonatomic,strong) MKMapItem * mapItem;

/*! @brief represents a CLLocation object. */
@property (nonatomic) CLLocation * location;

/*! @brief represents a CLLocationManager object. */
@property (nonatomic) CLLocationManager * locationManager;

/*! @brief represents the latitude of a salon. */
@property (nonatomic) double salonLat;

/*! @brief represents the longitude of a salon. */
@property (nonatomic) double salonLon;

/*! @brief represents the name of the salon. */
@property (nonatomic) NSString * salonName;
@end

@implementation GetDirectionsViewController

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    self.getDirectionsMap.mapType = MKMapTypeHybrid;
    self.getDirectionsMap.showsUserLocation = NO;
    self.getDirectionsMap.delegate = nil;
    [self.getDirectionsMap removeFromSuperview];
    self.getDirectionsMap = nil;;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    self.view.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.7];

    self.getDirectionsMap.showsUserLocation=YES;
    self.getDirectionsMap.showsPointsOfInterest=YES;

    if ([self.salonMapData isKindOfClass:[Salon class]]) {
        Salon * salon = (Salon *)self.salonMapData;
        self.salonLat=salon.salon_lat;
        self.salonLon = salon.salon_long;
        self.salonName = salon.salon_name;
    }
    else if([self.salonMapData isKindOfClass:[SalonTier4 class]]){
        SalonTier4 * salon =(SalonTier4 *)self.salonMapData;
        self.salonLat=[salon.locationLat doubleValue];
        self.salonLon=[salon.locationLon doubleValue];
        self.salonName = salon.salonName;
    }
    
    SalonAnnotation * salonAnnotation = [[SalonAnnotation alloc] init];
    CLLocationCoordinate2D newCoord;
    newCoord.latitude = self.salonLat;
    newCoord.longitude = self.salonLon;
    salonAnnotation.coordinate = newCoord;
    salonAnnotation.title = self.salonName;
    [self.getDirectionsMap addAnnotation:salonAnnotation];
    
    //MKPlacemark
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(newCoord.latitude , newCoord.longitude) addressDictionary:nil] ;
    self.mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    
    self.getDirectionsMap.showsUserLocation = YES;
    MKUserLocation *userLocation = self.getDirectionsMap.userLocation;
    MKCoordinateRegion region =
    MKCoordinateRegionMakeWithDistance(userLocation.coordinate,
                                       1000, 1000);
    [self.getDirectionsMap setRegion:region animated:YES];
    self.getDirectionsMap.delegate = self;
    [self.getDirectionsMap setCenterCoordinate:salonAnnotation.coordinate animated:YES];
    //[self getDirections];
    

    
    UITapGestureRecognizer * tapDetailsLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDetails)];
    [tapDetailsLabel setNumberOfTapsRequired:1];
    [self.detailsLabelView addGestureRecognizer:tapDetailsLabel];
    self.detailsLabelView.layer.cornerRadius = 9.0;
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70, 20, 60, 40)];
    cancelButton.backgroundColor=[UIColor blackColor];
    cancelButton.layer.cornerRadius=kCornerRadius;
    cancelButton.alpha=0.6;
    [cancelButton setTitle:@"Close" forState:UIControlStateNormal];
    cancelButton.tintColor=[UIColor whiteColor];
    
    [cancelButton addTarget:self action:@selector(closeMapView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:cancelButton];
    [self.view bringSubviewToFront:cancelButton];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    UILabel * directionsButtonLabel = [[UILabel alloc] initWithFrame:CGRectMake(100,self.view.frame.size.height-42, 120, 34)];
    directionsButtonLabel.text=@"Get Directions";
    directionsButtonLabel.font=[UIFont systemFontOfSize:13.0f];
    directionsButtonLabel.textColor=[UIColor whiteColor];
    directionsButtonLabel.textAlignment=NSTextAlignmentCenter;
    directionsButtonLabel.backgroundColor=[UIColor blackColor];
    directionsButtonLabel.layer.cornerRadius=3.0f;
    directionsButtonLabel.layer.masksToBounds=YES;
    directionsButtonLabel.alpha=0.7;
    [directionsButtonLabel addGestureRecognizer:tapDetailsLabel];
    directionsButtonLabel.userInteractionEnabled=YES;
    [self.view insertSubview:directionsButtonLabel aboveSubview:self.getDirectionsMap];
    [self.view bringSubviewToFront:directionsButtonLabel];
    directionsButtonLabel.center = CGPointMake(self.view.center.x, directionsButtonLabel.frame.origin.y);
}

-(void)closeMapView{
    if (self.isSplitTransition) {
       
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
       [self dismissViewControllerAnimated:YES completion:nil]; 
    }
    
}
#pragma mark Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation * location = [locations lastObject];
    
    if (location!=nil) {
        
        MKCoordinateRegion mapRegion;
        mapRegion.center = location.coordinate;
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(self.salonLat, self.salonLon);
        mapRegion.center=coord;
        mapRegion.span.latitudeDelta = 0.001;
        mapRegion.span.longitudeDelta = 0.001;
        
        self.getDirectionsMap.delegate = self;
        [self.getDirectionsMap setRegion:mapRegion animated: NO];
        
        [self.locationManager stopUpdatingLocation];
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            

            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps-x-callback://"]]) {
                
                if (IS_IOS_9_OR_LATER) {
                    [[UIApplication sharedApplication] openURL:
                     [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps-x-callback://?daddr=%f,%f&zoom=14&views=traffic&directionsmode=transit&x-success=whatsalon://?resume=true",self.salonLat,self.salonLon]]];
                }
                else{
                    [[UIApplication sharedApplication] openURL:
                     [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps-x-callback://?daddr=%f,%f&zoom=14&views=traffic&directionsmode=transit&x-success=whatsalon://?resume=true&x-source=WhatSalon",self.salonLat,self.salonLon]]];
                }
                
            } else {
                

                [[UIApplication sharedApplication] openURL:
                 [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",self.getDirectionsMap.userLocation.coordinate.latitude,self.getDirectionsMap.userLocation.coordinate.longitude, self.salonLat,self.salonLon]]];
            }
           }
    }
}
-(void)showDetails{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Directions" message:@"Opening the maps application will leave WhatSalon. Continue?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=1;
    
    [alert show];
    
    
    
}


-(void)dismissModalView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MKMapView delegate methods
- (MKAnnotationView *)mapView:(MKMapView *)mv viewForAnnotation:(id <MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[MKUserLocation class]]){
                return nil;
    }
    else{
    
        NSString *annotationIdentifier = @"PinViewAnnotation";
        
        SalonAnnotationView *pinView = (SalonAnnotationView *) [mv dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
        
        
        if (!pinView)
        {
            pinView = [[SalonAnnotationView alloc]
                       initWithAnnotation:annotation
                       reuseIdentifier:annotationIdentifier];
            pinView.salonImageType=kTier1SalonIcon;
            pinView.canShowCallout = YES;
            
            
        }
        else
        {
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
}


@end
