//
//  PhorestSalonServices.m
//  whatsalon
//
//  Created by Graham Connolly on 20/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "PhorestSalonServices.h"
#import "UIView+AlertCompatibility.h"

@implementation PhorestSalonServices

-(instancetype) initWithSalonID: (NSString *) s_id{
    
    self = [super init];
    if (self) {
        self.salon_id = s_id;
        [self fetchServicesListWithID:s_id];
    }
    return self;
}
+(instancetype) servicesListWithSalonID: (NSString *) s_id{
    
    return [[self alloc] initWithSalonID:s_id];
}

-(void)fetchServicesListWithID : (NSString *) s_id{
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Phorest_services_List_URL]];
    NSString * param = [NSString stringWithFormat:@"salon_id=%@",s_id];
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[param dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        if (error==nil) {
            
            if (dataDict!=nil) {
                
                switch (httpResp.statusCode) {
                    case kOKStatusCode:{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            NSLog(@"Services List: \n\n %@",dataDict);
                        });
                    }
                        break;
                        
                    default:
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [WSCore showServerErrorAlert];
                        });
                        break;
                }
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [WSCore showServerErrorAlert];
                });
            }
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView showSimpleAlertWithTitle:@"Error" message:[NSString stringWithFormat:@"Salon Services could not be downloaded. %@",[error localizedDescription] ] cancelButtonTitle:@"OK"];
            });
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [dataTask resume];
    }
    else{
        
        [WSCore showNetworkErrorAlert];
    }
    
}
@end
