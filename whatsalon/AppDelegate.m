//
//  AppDelegate.m
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "ServiceCategory.h"
#import "CategorySingleton.h"
#import "StyleCategoryListModel.h"
#import <SDWebImage/SDImageCache.h>
#import "BrowseViewController.h"
#import "User.h"
#import "UICKeyChainStore.h"
#import <Crashlytics/Crashlytics.h>
#import "AFHTTPRequestOperationManager.h"
#import "MyBookings.h"

#import "MenuViewController.h"
#import "MyBookingsViewController.h"
#import "RESViewController.h"
#import "RESideMenu/RESideMenu.h"
#import "RESideMenu.h"

#import <AudioToolbox/AudioServices.h>
#import "Harpy.h"
#import "UICKeyChainStore.h"
#import "GCFacebookHelper.h"
#import "SubmitReviewSalonDetailViewController.h"

#import <Crashlytics/Crashlytics.h>


@implementation AppDelegate



- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    
   UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    
    if ([shortcutItem.type isEqualToString:@"com.whatsalon.bookings"]) {
        tabBarController.selectedIndex=3;
    } else if([shortcutItem.type isEqualToString:@"com.whatsalon.search"]){
        tabBarController.selectedIndex=1;
    }
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}

/*! The read permissions that are required by the app are provided here.
allowLoginUI: This parameter accepts a YES or NO value. If YES, then it shows either the Facebook app (if installed) or Safari to enter credentials if needed, and then to authorize the app.
completionHandler: The completion handler block is called when the login process has finished. It returns three parameters: A session object, the session state value, and an error object in case it occures.
 */
-(void)openActiveSessionWithPermissions:(NSArray *)permissions allowLoginUI:(BOOL)allowLoginUI{
    NSLog(@"Active Session");
    /*
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:allowLoginUI
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                      
                                      // Create a NSDictionary object and set the parameter values.
                                      NSDictionary *sessionStateInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                                        session, @"session",
                                                                        [NSNumber numberWithInteger:status], @"state",
                                                                        error, @"error",
                                                                        nil];
                                      
                                      // Create a new notification, add the sessionStateInfo dictionary to it and post it.
                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionStateChangeNotification"
                                                                                          object:nil
                                                                                        userInfo:sessionStateInfo];
                                      
                                  }];
     
     */
}

- (void)harpyInit:(UIWindow *)window
{
    
   
    /*
     Harpy
     */
    // Set the App ID for your app
    [[Harpy sharedInstance] setAppID:@"670583378"];
    
    // Set the UIViewController that will present an instance of UIAlertController
    [[Harpy sharedInstance] setPresentingViewController:window.rootViewController];
    
    // (Optional) The tintColor for the alertController
    //[[Harpy sharedInstance] setAlertControllerTintColor:@"<#alert_controller_tint_color#>"];
    
    // (Optional) Set the App Name for your app
    [[Harpy sharedInstance] setAppName:@"WhatSalon"];
    
    /* (Optional) Set the Alert Type for your app
     By default, Harpy is configured to use HarpyAlertTypeOption */
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeOption];
    
    /* (Optional) If your application is not availabe in the U.S. App Store, you must specify the two-letter
     country code for the region in which your applicaiton is available. */
    [[Harpy sharedInstance] setCountryCode:@"IE"];
    
    /* (Optional) Overides system language to predefined language.
     Please use the HarpyLanguage constants defined inHarpy.h. */
    //[[Harpy sharedInstance] setForceLanguageLocalization<#HarpyLanguageConstant#>];
    
    //[Harpy sharedInstance].debugEnabled=YES;
    // Perform check for new version of your app
    
}


/*
 Revoke the facebook permissions
 
 */
- (void)revokeFacebookPermissions
{
    
    GCFacebookHelper * fbHelper = [[GCFacebookHelper alloc] init];
    [fbHelper revokePermissions];
    
}
/*
- (void)createDynamicShortcutItems {
    
    UIApplicationShortcutIcon *icon1 = [UIApplicationShortcutIcon iconWithTemplateImageName:@"what_salon_logo_icon"];
    UIApplicationShortcutIcon *icon2 = [UIApplicationShortcutIcon iconWithTemplateImageName:@"Search"];
    UIApplicationShortcutIcon *icon3 = [UIApplicationShortcutIcon iconWithTemplateImageName:@"rounded_heart_icon"];
    UIApplicationShortcutIcon *icon4 = [UIApplicationShortcutIcon iconWithTemplateImageName:@"three_o_clock_icon"];
    
    // create several (dynamic) shortcut items
    UIMutableApplicationShortcutItem *item1 = [[UIMutableApplicationShortcutItem alloc]initWithType:@"com.test.dynamic" localizedTitle:@"Discover" localizedSubtitle:nil icon:icon1 userInfo:nil];
    UIMutableApplicationShortcutItem *item2 = [[UIMutableApplicationShortcutItem alloc]initWithType:@"com.test.deep1" localizedTitle:@"Search" localizedSubtitle:nil icon:icon2 userInfo:nil];
    UIMutableApplicationShortcutItem *item3 = [[UIMutableApplicationShortcutItem alloc]initWithType:@"com.test.deep2" localizedTitle:@"Favourites" localizedSubtitle:nil icon:icon3 userInfo:nil];
    UIMutableApplicationShortcutItem *item4 = [[UIMutableApplicationShortcutItem alloc]initWithType:@"com.test.deep2" localizedTitle:@"My Bookings" localizedSubtitle:nil icon:icon4 userInfo:nil];

    
    // add all items to an array
    NSArray *items = @[item1, item2, item3,item4];
    
    // add this array to the potentially existing static UIApplicationShortcutItems
    NSArray *existingItems = [UIApplication sharedApplication].shortcutItems;
    NSArray *updatedItems = [existingItems arrayByAddingObjectsFromArray:items];
    [UIApplication sharedApplication].shortcutItems = updatedItems;
   

}
 */
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
   // [self createDynamicShortcutItems];
 
    self.isTab=YES;
    
    
    //Insert at top, but after all other third Party SDKS
    [Crashlytics startWithAPIKey:@"ac4937dc8d33c57ad0009f8049dd672f22c2eead"];
    
    
    [self checkForKey];
    
    [self harpyInit:_window];
    
    
    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"back_arrow"]];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"back_arrow"]];
  
    
    self.window.tintColor = kWhatSalonBlue;

    //if its first launch delete key chain
    //Clear keychain on first run in case of reinstallation
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"WSFirstRun"]) {
        // Delete values from keychain here
        
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [UICKeyChainStore removeAllItems];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"FirstRun" forKey:@"WSFirstRun"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    
  
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
forBarMetrics:UIBarMetricsDefault];
    
    
    if(self.isTab){
        [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:8]} forState:UIControlStateNormal];

        UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
        UITabBar *tabBar = tabBarController.tabBar;
    
        UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
        tabBarItem1.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0);
        
        UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
        tabBarItem2.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0);
        
        UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
        tabBarItem3.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0);
        
        UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
        tabBarItem4.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0);
        
        UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
        tabBarItem5.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0);
    }
    
    
    
       [self.window setTintColor:kWhatSalonBlue];
    
    // Handle launching from a notification
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        // Set icon badge number to zero
        application.applicationIconBadgeNumber = 0;
        [self handleLocationNotificationWithApplication:application AndNotification:locationNotification];
    }

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    //[[LocationManager sharedInstance] stop];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[User getInstance] markTwilioAsShown:NO];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)checkForKey
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
   
    if ([[User getInstance] isUserLoggedIn] && [[User getInstance] fetchKey].length==0) {
        //NSLog(@"User is logged in and doesnt not have the key, then log out");
        [[User getInstance] logout];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self checkForKey];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    
    
    if (![WSCore isNetworkReachable]){
        [WSCore showNetworkErrorAlert];
           }
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[User getInstance] updateProfile];
   
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[User getInstance] markTwilioAsShown:NO];
}


/*!
 handles the various local notifications that occur.
 The handling of the local notifications depends on the @c[userInfo[@"type"]].
 */
-(void)handleLocationNotificationWithApplication:(UIApplication *) application AndNotification: (UILocalNotification *) notification{
    
   
    
    NSDictionary* userInfo = [notification userInfo];
   
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    if ([userInfo[@"type"] isEqualToString:@"1"]) {
        
        
            
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:userInfo[@"title"] message:userInfo[@"body"] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag=111;
        [alert show];
        
      
        
    }
    else if ([userInfo[@"type"] isEqualToString:@"2"]){
        
        self.reviewSalonID = userInfo[@"salon_id"];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:userInfo[@"title"] message:userInfo[@"body"] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
         alert.tag=22;
         [alert show];
        
        
        
        }
    
    else{
        
        if (application.applicationState == UIApplicationStateActive) {
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:userInfo[@"title"] message:userInfo[@"body"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        else{
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:userInfo[@"title"] message:userInfo[@"body"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }

}
#pragma mark - Local Notification
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    
    [self handleLocationNotificationWithApplication: (UIApplication *) application AndNotification: (UILocalNotification *) notification];
    
    
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (alertView.tag==111) {
        if (buttonIndex != [alertView cancelButtonIndex]) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kShouldShowPreviousBookings];
             [[NSUserDefaults standardUserDefaults] synchronize];
            UITabBarController *tabb = (UITabBarController *)self.window.rootViewController;
            tabb.selectedIndex = 3;
        }
    }
    else if (alertView.tag==22){
        
        if (buttonIndex != [alertView cancelButtonIndex]) {
            
            //presents 'SubmitReviewSalonDetailViewController' for quick input of reviews.
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainTab"
                                                                     bundle: nil];
            SubmitReviewSalonDetailViewController * submitVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"submitReviewVC"];
            submitVC.modalPresentationStyle = UIModalPresentationCustom;
            Salon * salon = [[Salon alloc] initWithSalonID:self.reviewSalonID];
            submitVC.salonData = salon;
            CATransition* transition = [CATransition animation];
            
            transition.duration = 0.6;
            transition.type = kCATransitionFade;
            
            [[self.window.rootViewController navigationController].view.layer addAnimation:transition forKey:kCATransition];
            [self.window.rootViewController presentViewController:submitVC animated:NO completion:nil];

        }
        
    }
    else{
        if (buttonIndex == [alertView cancelButtonIndex]) {
            UITabBarController *tabb = (UITabBarController *)self.window.rootViewController;
            tabb.selectedIndex = 3;
        }
    }
   
}


@end
