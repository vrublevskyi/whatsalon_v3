//
//  EmptyBookingView.m
//  whatsalon
//
//  Created by admin on 10/2/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import "EmptyBookingView.h"
#import "whatsalon-Swift.h"

@implementation EmptyBookingView
@synthesize delegate;


-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void) customInit {
    [[NSBundle mainBundle] loadNibNamed:@"EmptyBookingView" owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = self.bounds;
    self.makeBookButton.clipsToBounds = true;
    self.makeBookButton.layer.cornerRadius = 2;
}

-(void)setMakeBookButton:(UIButton *)makeBookButton
{
    _makeBookButton = makeBookButton;
    _makeBookButton.clipsToBounds = true;
    _makeBookButton.layer.cornerRadius = 2;
    
    [Gradients addPinkToPurpleBorderWithView:_makeBookButton];
}

- (IBAction)makeBooking:(id)sender {
    [self.delegate onMakeBookingTapped:self];
}


@end
