//
//  SelectCategoryViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 05/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"
#import "FUIButton.h"
#import "SalonService.h"

@class SelectCategoryViewController;
@protocol SelectServiceDelegate <NSObject>
- (void) onServiceSelected: (SalonService *) service sender:(SelectCategoryViewController*) sender;
@end

@interface SelectCategoryViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic) Salon * salonData;
@property (nonatomic) NSMutableArray * servicesCategory;
@property (nonatomic) NSMutableArray * servicesArray;
@property (nonatomic, weak) id <SelectServiceDelegate> delegate;

@property (nonatomic) BOOL isService;
@end
