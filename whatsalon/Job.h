//
//  Job.h
//  whatsalon
//
//  Created by Graham Connolly on 05/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Job : NSObject

@property (nonatomic) NSString * address;
@property (nonatomic) NSString * appt_time;
@property (nonatomic) NSString * balance_description;
@property (nonatomic) NSString * confirm_ID;
@property (nonatomic) NSString * currency_sign;
@property (nonatomic) NSString * discount_percentage;
@property (nonatomic) NSString * discounted_price;
@property (nonatomic) NSString * full_price;
@property (nonatomic) NSString * image_url;
@property (nonatomic) NSString * online_payment_percentage;
@property (nonatomic) NSString * online_payment_price;
@property (nonatomic) int rating;
@property (nonatomic) float salon_lat;
@property (nonatomic) float salon_lon;
@property (nonatomic) NSString *salon_name;
@property (nonatomic) NSString *time;
@property (nonatomic) NSString * total_reviews;
@property (nonatomic) NSString * salon_description_full;
@property (nonatomic) NSString * salon_phone;
@property (nonatomic) NSString *proposed_time1;

//designated Init
-(instancetype)initWithConfirmID : (NSString *)c_id;
+(instancetype) jboWithConfirmID: (NSString *)c_id;

@end
