//
//  BrowseLoadingTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 20/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowseLoadingTableViewCell : UITableViewCell

@property (nonatomic) UIView * backgroundPlaceHolder;
@property (nonatomic) UIImageView * heartImage;
@property (nonatomic) UIImageView * letterBox;
@property (nonatomic) UIView * titleHolder;
@property (nonatomic) UIView * addressHolder;
@property (nonatomic) UIView * addressHolder2;
@property (nonatomic) UIView * kmHolder;

-(void)configureCell;
@end
