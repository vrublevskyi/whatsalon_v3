//
//  UITableView+ReloadTransition.m
//  whatsalon
//
//  Created by Graham Connolly on 25/07/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "UITableView+ReloadTransition.h"

@implementation UITableView (ReloadTransition)
- (void)reloadDataWithFade:(BOOL)animated {
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView transitionWithView:self
                          duration:0.5f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^(void) {
                            [self reloadData];
                        } completion:nil];
    });
    //[self reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    /*
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha=0.0;
    } completion:^(BOOL finished) {
        [self reloadData];
        [UIView animateWithDuration:0.2 animations:^{
            self.alpha=1.0;
        }];
    }];
    
    // Synchronize the implementation details with my changes so far.
   // [CATransaction flush];
    
    [self reloadData];
    if (animated) {
        CATransition *animation = [CATransition animation];
        [animation setDelegate:self.delegate]; // optional
        [animation setType:kCATransitionFade];
        [animation setDuration:0.3];
        [animation setTimingFunction:[CAMediaTimingFunction
                                      functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [animation setFillMode: kCAFillModeForwards];
        //@extended
        [[self layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
    }
     */
    
}

- (void)reloadDataAnimated:(BOOL)animated
{
    [self reloadData];
    
    if (animated) {
        
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionFade];
        [animation setSubtype:kCATransitionFromBottom];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [animation setFillMode:kCAFillModeBoth];
        [animation setDuration:.3];
        [[self layer] addAnimation:animation forKey:@"UITableViewReloadDataAnimationKey"];
        
    }
}
@end
