//
//  TabBrowseViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 30/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabBrowseViewController.h"
#import "Salon.h"
#import "WSCore.h"
#import "BrowseTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "TabBarController.h"
#import "ResultsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UITableView+ReloadTransition.h"
#import "User.h"
#import "MapViewController.h"
#import "NoSalonsMessageView.h"
#import "NoLocationMessageView.h"
#import "UIView+AlertCompatibility.h"
#import "CongratulationsScreenViewController.h"
#import "SalonIndividualDetailViewController.h"
#import "FilterSearchViewController.h"
#import "FUIButton.h"
#import "UIColor+FlatUI.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SearchSalonsTableViewController.h"
#import "WalkthroughViewController.h"
#import "TwilioVerifiyViewController.h"
#import "SettingsViewController.h"
#import "MyBookingsViewController.h"

#import "Booking.h"
#import "Harpy.h"
#import "SelectCategoryViewController.h"
#import "INTULocationManager.h"
#import "OpeningDay.h"
#import "BrowseLoadingTableViewCell.h"
#import "UIImage+Extras.h"

#import "TabNewSalonDetailViewController.h"//added for test

#import "UILabel+Boldify.h"

#import "SalonPreviewViewController.h"

#import "TabMakeBookingViewController.h"
#import "SalonReviewListViewController.h"
#import "SlideShowViewController.h"

#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>


#import "BrowseSalonTableViewCell.h"//new cell added
#import "whatsalon-Swift.h"

/*!
 * @brief FilterType - NSUInteger enums representing the filter types
 */

typedef enum FilterType : NSUInteger{
    FilterTypeAll = 0,
    FilterTypeInstantOnly,
    FilterTypeCallOnly,
} FilterType;

/*!
 * @brief kLoadinCellBrowseTag - const int random numeric value that is used to determine which is the loading cell
 */
const int kLoadingCellBrowseTag =1274;



@interface TabBrowseViewController ()<CLLocationManagerDelegate,UIViewControllerTransitioningDelegate,FilterDelegate,GCNetworkManagerDelegate,/*TabSalonIndividualDelegate,*/TabSalonDetailIndividualDelegate,BrowseDelegate,UIScrollViewDelegate,UIViewControllerPreviewingDelegate,SalonPreviewDelegate,BrowseSalonTableViewCellDelegate>

/*!
 * @brief allSalons - NSMutableArray that represents all the salons downloaded
 */

@property (nonatomic,strong) NSMutableArray * allSalons;

/*!
 * @brief dateFormatter - NSDateFormatter used with UIRefreshControl to display the last update
 */

@property (nonatomic) NSDateFormatter * dateFormatter;

@property (nonatomic) UIRefreshControl * refreshControl;

/*!
 * @brief location - CLLocation represents the users location
 */

@property (nonatomic) CLLocation * location;

/*!
 * @brief isNoLocationViewShowing - BOOL tells whether the no location view is showing or not
 */

@property (nonatomic,assign) BOOL isNoLocationViewShowing;

/*!
 * @brief isNoSalonsLocationViewShowing - BOOL tells whether no salons location view showing
 */
@property (weak, nonatomic) IBOutlet UIButton *filterButton;

@property (nonatomic,assign) BOOL isNoSalonsLocationViewShowing;

/*!
 * @brief isLoadingSalons - BOOL determines whether there is a request being made to the server. Insurance to prevent a double call to the server which shows duplicates
 */
@property (nonatomic) BOOL isLoadingSalons;

@property (nonatomic) GCNetworkManager * networkManager;


@property (nonatomic) BOOL showInstant;

/*!
 * @brief titleViewButton UIButton - header button that contains the title of what is being displayed. e.g All Salons, Hair Salons etc. Updated from the Filter Screen
 */

@property (nonatomic) UIButton * titleViewButton;

@property (nonatomic) BOOL isSearchForLocation;

@property (nonatomic) int selectedCategory;
@property (nonatomic) NSIndexPath * selectedPreviewIndexPath;
@end

@implementation TabBrowseViewController

/*
 filterNavigationBarButtonWithTitle..
 adds a uiview containing a button to the navigation bar 
 the button triggers a filter screen
 the buttons title is the chosen category to filter
 by default 'All Salons'
 
 */
-(void)filterNavigationBarButtonWithTitle: (NSString *) str{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 40)];
    view.backgroundColor = [UIColor clearColor];
    self.titleViewButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];

    CGFloat titleViewButtonWidth= 130;
    if (self.selectedCategory!=0) {
        titleViewButtonWidth = view.frame.size.width;
    }
    self.titleViewButton.frame = CGRectMake(0, 0, titleViewButtonWidth, view.frame.size.height);
    self.titleViewButton.backgroundColor = [UIColor clearColor];
    [self.titleViewButton setTitle:str forState:UIControlStateNormal];
    self.titleViewButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    
    if (!self.filterByDiscoveryCategory) {
        //NSLog(@"with down arrow");
        UIImage * image =[UIImage imageNamed:@"down_arrow_icon"];
        [self.titleViewButton setImage: [image imageByScalingProportionallyToSize:CGSizeMake(25,18)]forState:UIControlStateNormal];
        UIEdgeInsets titleEdge =UIEdgeInsetsMake(0, -self.titleViewButton.imageView.frame.size.width, 0, self.titleViewButton.imageView.frame.size.width);
        UIEdgeInsets imageEdge =  UIEdgeInsetsMake(0, self.titleViewButton.titleLabel.frame.size.width, 0, -self.titleViewButton.titleLabel.frame.size.width);
        self.titleViewButton.titleEdgeInsets =titleEdge;
        self.titleViewButton.imageEdgeInsets =imageEdge;
        [self.titleViewButton addTarget:self action:@selector(showFilterSearchViewController) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:self.titleViewButton];
        self.titleViewButton.center = CGPointMake(view.center.x+20, view.center.y);
    }
    else if(self.filterByDiscoveryCategory){
        
        //NSLog(@"With no down arrow");
        [view addSubview:self.titleViewButton];
        self.titleViewButton.userInteractionEnabled=NO;
    }
    
    self.navigationItem.titleView = view;
    
   
    self.filterBarButtonItem.hidden=YES;
}


/*
 didAddFavourite
 Updates the tableview
 */

-(void)didAddFavourite:(Salon *)salon{
    //NSLog(@"Add favouite");
    for (int i =0; i<self.allSalons.count; i++) {
        Salon * s = [self.allSalons objectAtIndex:i];
        
        if ([s.salon_id isEqualToString: salon.salon_id]) {
            s.is_favourite=YES;
            [self.allSalons replaceObjectAtIndex:i withObject:s];
            
            
            
            break;
        }
        
        
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });

    
}

/*
 didUnFavourite
 updates the tableView
 
 */

-(void)didUnFavourite:(Salon *)salon{
    //NSLog(@"Un favourite");
    for (int i =0; i<self.allSalons.count; i++) {
        Salon * s = [self.allSalons objectAtIndex:i];
        if ([s.salon_id isEqualToString:salon.salon_id]) {
            
            s.is_favourite=NO;
            [self.allSalons replaceObjectAtIndex:i withObject:s];
            
            
            break;
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        
    });

}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

/*
 checkIfLocationIsDenied
    used to check and handle the Location Permission state
 
 */

- (void)checkIfLocationIsDenied {
    
   // NSLog(@"Is searching for Location %@",StringFromBOOL(self.isSearchForLocation));
    if (!self.isSearchForLocation) {
        self.isSearchForLocation=YES;
        
      //  NSLog(@"Is searching for Location %@",StringFromBOOL(self.isSearchForLocation));
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                           timeout:10.0
                              delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                             block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                 
                                                 self.isSearchForLocation=NO;
                                                 
                                                 //NSLog(@"Is searching for Location %@",StringFromBOOL(self.isSearchForLocation));
                                                 
                                                 if (status == INTULocationStatusSuccess||status == INTULocationStatusTimedOut) {
                                                     // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                     // currentLocation contains the device's current location.
                                                     
                                                     //OR
                                                     
                                                     // Wasn't able to locate the user with the requested accuracy within the timeout interval.
                                                     // However, currentLocation contains the best location available (if any) as of right now,
                                                     // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
                                                     

                                                     
                                                     self.location = currentLocation;
                                                     
                                                     if ([[User getInstance] isUserLoggedIn]) {
                                                         [[User getInstance] updateUsersLocationWithLocation:currentLocation];
                                                         [WSCore updateUsersCityAndCountry:currentLocation];
                                                     }
                                                     
                                                     [self getLatest];
                                                     
                                                     
                                                     
                                                 }
                                                 else {
                                                     // An error occurred, more info is available by looking at the specific status returned.
                                                     
                                                     switch (status) {
                                                             
                                                         case INTULocationStatusServicesDenied:
                                                             
                                                             //NSLog(@"CUrrent System %f",[[[UIDevice currentDevice] systemVersion] floatValue]);
                                                             
                                                             if (IS_IOS_8_OR_LATER) {
                                                                 
                                                                 UIAlertController * alert=   [UIAlertController
                                                                                               alertControllerWithTitle:@"Location Error"
                                                                                               message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app"
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                                 
                                                                 UIAlertAction* ok = [UIAlertAction
                                                                                      actionWithTitle:@"Open Settings"
                                                                                      style:UIAlertActionStyleDefault
                                                                                      handler:^(UIAlertAction * action)
                                                                                      {
                                                                                          
                                                                                          if (&UIApplicationOpenSettingsURLString != NULL) {
                                                                                              NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                                              [[UIApplication sharedApplication] openURL:appSettings];
                                                                                          }
                                                                                          
                                                                                      }];
                                                                 UIAlertAction* cancel = [UIAlertAction
                                                                                          actionWithTitle:@"Cancel"
                                                                                          style:UIAlertActionStyleDestructive
                                                                                          handler:^(UIAlertAction * action)
                                                                                          {
                                                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                              
                                                                                          }];
                                                                 
                                                                 [alert addAction:cancel];
                                                                 [alert addAction:ok];
                                                                 
                                                                 [self presentViewController:alert animated:YES completion:nil];
                                                                 
                                                                 [self addNoLocationMessageView];
                                                             }
                                                             else{
                                                                 
                                                                 [UIView showSimpleAlertWithTitle:@"Location Error" message:@"You have denied WhatSalon access to location services. In order to use this app, please accept access. " cancelButtonTitle:@"OK"];
                                                             }
                                                             break;
                                                             
                                                             
                                                         case INTULocationStatusServicesRestricted:
                                                             
                                                             [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location services are restricted for WhatSalon. e.g (parental controls corporate policy, etc)" cancelButtonTitle:@"OK"];
                                                             break;
                                                             
                                                         case INTULocationStatusServicesDisabled:
                                                             
                                                             
                                                             [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app. Settings->Privacy->LocationServices" cancelButtonTitle:@"OK"];
                                                             
                                                             [self addNoLocationMessageView];
                                                             break;
                                                             
                                                         case INTULocationStatusError:
                                                             
                                                             
//                                                             [UIView showSimpleAlertWithTitle:@"Location Error" message:@"An error occurred while using the system location services." cancelButtonTitle:@"OK"];
                                                             break;
                                                         default:
                                                             break;
                                                     }
                                                     
                                                     
                                                 }
                                                 //NSLog(@"Status %ld",(long)status);
                                                 
                                             }];
        

    }
    
    
    

}

#pragma mark - NoLocationMessageView

/*
 addNoLocationMessageView
 
 adds a view that is shown when there is no location
 */
-(void)addNoLocationMessageView{
    
    self.mapButton.enabled=NO;
    self.titleViewButton.enabled=NO;
    UIView * noDataView = [[UIView alloc] initWithFrame:self.tableView.bounds];
    noDataView.backgroundColor=kBackgroundColor;
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    imageView.image = [UIImage imageNamed:kNo_Location_Placeholder_Image_Name];
    [noDataView addSubview:imageView];
    UIView * transparentView = [[UIView alloc] initWithFrame:self.view.frame];
    transparentView.backgroundColor = kGoldenTintForOverView;
    [imageView addSubview:transparentView];
    
    NoLocationMessageView * noLocationMessage = [[NoLocationMessageView alloc] init];
    noLocationMessage.view.backgroundColor=[UIColor clearColor];
    noLocationMessage.messageTitle.textColor = [UIColor whiteColor];
    noLocationMessage.messageText.textColor = [UIColor whiteColor];
    [noDataView addSubview:noLocationMessage];
    noLocationMessage.center=CGPointMake(noDataView.frame.size.width/2.0, noDataView.frame.size.height/2.0);
    [noLocationMessage.tryAgainButton addTarget:self action:@selector(checkIfLocationIsDenied) forControlEvents:UIControlEventTouchUpInside];
 

    self.tableView.hidden=NO;
    self.tableView.backgroundView=noDataView;
    self.isNoLocationViewShowing=YES;
}

/*
 removeNoLocationMessageVew
 removes the lcoation view from the tableview
 */
-(void)removeNoLocationMessageView{
    self.mapButton.enabled=YES;
    self.titleViewButton.enabled=YES;
    
    self.tableView.backgroundView=nil;
    self.isNoLocationViewShowing=NO;
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    [self.networkManager cancelTask];
    
    if (self.refreshControl.isRefreshing) {
        [self.refreshControl endRefreshing];
    }
}

#pragma mark - Application lifecycle methods

/*
 configureFooterView
 
    adds a footerView
 
    if the array 'allSalons' is greater than 0 add a label
 
    else if the array is equal to 0 add a label and a button
 
 */
- (void)configureFooterView{
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 160)];
    footerView.backgroundColor = [UIColor clearColor];
    UILabel * label =  [[UILabel alloc] initWithFrame:CGRectMake(15, 8, self.view.frame.size.width-30, 75)];
    label.numberOfLines=0;
    label.textAlignment=NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:15.0f];
    label.textColor = [UIColor lightGrayColor];
    [footerView addSubview:label];
    
    if (self.allSalons.count>0) {
        label.text=@"Can't find what you're looking for? \nTry searching the directory of salons near you.";
       
        
        UIButton * dirBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        dirBtn.frame = CGRectMake(0, 91, 140, 30);
        [dirBtn setTitle:@"View Directory" forState:UIControlStateNormal];
        [footerView addSubview:dirBtn];
        [dirBtn addTarget:self action:@selector(showDirectory) forControlEvents:UIControlEventTouchUpInside];
        dirBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        dirBtn.center = CGPointMake(footerView.center.x, dirBtn.center.y);
        
       
    }
    else{
        
        if ([[User getInstance] hasUsersCityAndCounty]) {
            NSString * city = [[User getInstance] fetchUsersCityAndCountry];
            label.text = [NSString stringWithFormat:@"We don’t have salon partners in %@ - check back soon.  In the meantime, you can search the directory for salons near you. ",city];
            
            [label boldSubstring:city];
        }
        else{
            label.text=@"We don’t have salon partners in your area yet - check back soon.  In the meantime, you can search the directory for salons near you. ";
        }
       
        UIButton * dirBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        dirBtn.frame = CGRectMake(0, 91, 140, 30);
        [dirBtn setTitle:@"View Directory" forState:UIControlStateNormal];
        [footerView addSubview:dirBtn];
        [dirBtn addTarget:self action:@selector(showDirectory) forControlEvents:UIControlEventTouchUpInside];
        dirBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        dirBtn.center = CGPointMake(footerView.center.x, dirBtn.center.y);


    }
    
   
    self.tableView.tableFooterView=footerView;
    
    
    [WSCore addTopLine:footerView :[UIColor lightGrayColor] :0.5 WithYvalue:footerView.frame.size.height-20];
  
}


-(void)showDirectory{
    [self performSegueWithIdentifier:@"directorySegue" sender:self];
}


//hides the footerview
-(void)hideFooterView{
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.tableView.tableFooterView=footerView;
    
}

- (void)filterButtonSetUp
{
}

-(void)setSearchCategoryType{
    //used to prefilter based on discovery selection
    //disable filter button
    //hide down arrow
    
    NSString * title;
    if (self.filterByDiscoveryCategory) {
        // update selected category - self.selectedCategory
        //NSLog(@"Discovery object id type %d",self.discoveryObjectIdType);
        switch (self.discoveryObjectIdType) {
            case 11:
                self.selectedCategory=1;
                title = @"Hair";
                break;
                
            case 22:
                self.selectedCategory=2;
                title = @"Hair Removal";
                break;
            case 33:
                self.selectedCategory = 3;
                title = @"Nails";
                break;
            case 44:
                self.selectedCategory = 4;
                title = @"Massage";
                break;
            case 55:
                self.selectedCategory = 5;
                title = @"Face Treatments";
                break;
            case  66:
                self.selectedCategory = 6;
                title = @"Body Treatments";
                break;
            
                
            default:
                self.selectedCategory=0;
                title = @"Salons Nearby";
                break;
        }
    }
    else{
        title = @"Salons Nearby";
        self.selectedCategory=0;
    }
    
    
    [self filterNavigationBarButtonWithTitle:title];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self setSearchCategoryType];
    

    [[Harpy sharedInstance] checkVersion];
    [WSCore statusBarColor:StatusBarColorBlack];
  
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor silverColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatest)
                  forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
    
    [self tablesSetUp];//the set up of the tables
    
    
    
    self.allSalons = [NSMutableArray array];
    self.browsePageCurrentPage=1;
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.parentView =self.view;
    self.networkManager.delegate=self;

    
    [self checkIfLocationIsDenied];
    
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self filterButtonSetUp];
    
    self.buttonView.backgroundColor=[UIColor clearColor];
    
    
    [self.mapButton setImage:[self.mapButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.mapButton.tintColor = kWhatSalonBlue;
    
    self.tableView.hidden=YES;
    
    
    
    UIImage * filterBarButtonImage = self.filterBarButtonItem.imageView.image;
    [self.filterBarButtonItem setImage:[filterBarButtonImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.filterBarButtonItem.tintColor = self.view.tintColor;
    

    [self registerFor3dTouch];
    
    /*
    // Objective-C
    [[Crashlytics sharedInstance] setDebugMode:YES];
    [Fabric with:@[[Crashlytics class]]];
    [CrashlyticsKit crash];
     */
    
            [self.tableView registerNib:[UINib nibWithNibName:@"BrowseSalonTableViewCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"BrowseSalonTableViewCell"];
    
}

/*
 cancels a network task if one is in progress
 
 shows FilterSearchViewController
 */
-(void)showFilterSearchViewController{
   
    
    [self.networkManager cancelTask];
    FilterSearchViewController * filter = [self.storyboard instantiateViewControllerWithIdentifier:@"filterVC"];
    filter.modalPresentationStyle = UIModalPresentationCurrentContext;
    filter.myDelegate=self;
    filter.browseCategoryForFiltering=self.selectedCategory;
    filter.showingInstant=self.showInstant;
    [self presentViewController:filter animated:YES completion:^{
        if (self.allSalons.count>0) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:0 animated:NO];
        }
        
    }];
}



-(void)hideRefreshIfVisible{
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
    
}


#pragma mark - NSNotification methods

- (void)refreshTableView:(NSNotification *)notif {
    [self.tableView reloadDataWithFade:YES];
   
}


#pragma mark - TabSalonDetailIndividualDelegate
/*
 //update the didFavouriteSalon called from tabSalonDetail.
 loops through the array and update the object
 */
-(void)didFavouriteSalonOnDetailView:(Salon *)salon{
    
    for (int i =0; i<self.allSalons.count; i++) {
        Salon * s = [self.allSalons objectAtIndex:i];
        if ([s.salon_id isEqualToString: salon.salon_id]) {
            
            s.is_favourite=YES;
            [self.allSalons replaceObjectAtIndex:i withObject:salon];
            
            
            break;
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        
    });
}

/*
 update didUnFavouriteSalon
 loops through the array and update the object
 
 */
-(void)didUnFavouriteSalonOnDetailView:(Salon *)salon{
    
    for (int i =0; i<self.allSalons.count; i++) {
        Salon * s = [self.allSalons objectAtIndex:i];
        if ([s.salon_id isEqualToString: salon.salon_id]) {
            
            s.is_favourite=NO;
            [self.allSalons replaceObjectAtIndex:i withObject:salon];
            
            
            break;
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        
    });
    
}

-(void)refreshSalonObjectsWithUpdatedSalonObject:(Salon *)salon{
   // NSLog(@"Refresh all salon objects");
    for (int i =0; i<self.allSalons.count; i++) {
        Salon * s = [self.allSalons objectAtIndex:i];
        if ([s.salon_id isEqualToString: salon.salon_id]) {
            
            s = salon;
            [self.allSalons replaceObjectAtIndex:i withObject:salon];
            
            
            break;
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        
    });

}


//#pragma mark - TabSalonDetailDelegate methods

/*
 //update the didFavouriteSalon called from tabSalonDetail.
 loops through the array and update the object
 */
/*
-(void)didFavouriteSalon:(Salon *)salon{
    
    for (int i =0; i<self.allSalons.count; i++) {
        Salon * s = [self.allSalons objectAtIndex:i];
        if ([s.salon_id isEqualToString: salon.salon_id]) {
            
            s.is_favourite=YES;
            [self.allSalons replaceObjectAtIndex:i withObject:salon];
            
            
            break;
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        
    });
}
 */

/*
 update didUnFavouriteSalon
 loops through the array and update the object
 
 */
/*
-(void)didUnFavouriteSalon:(Salon *)salon{
    
    for (int i =0; i<self.allSalons.count; i++) {
        Salon * s = [self.allSalons objectAtIndex:i];
        if ([s.salon_id isEqualToString: salon.salon_id]) {
            
            s.is_favourite=NO;
            [self.allSalons replaceObjectAtIndex:i withObject:salon];
            
            
            break;
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        
    });

}
*/

/*
 viewWillAppear
 
 Handles the navigationbar appearance
 
 */
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self navigationBarSetUp];
    
}



/*
 resetPageAndCall
 
 resets the tableView to its default state and makes a call to the server to get the latest salons
 */
- (void)resetPageAndCall {
    self.browsePageCurrentPage=1;
    self.totalBrowsePageCount=0;
    [self.allSalons removeAllObjects];
    [self setSalonURLAndCall];
}


/*
 resets the page
 */
-(void)getLatest{

    [self resetPageAndCall];
    
}


#pragma mark - Set up methods




- (void)tablesSetUp
{
    //Near me & fav table
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.allowsSelectionDuringEditing = YES;
    
    [[self tableView] registerClass:[BrowseTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    
    
}


-(void)navigationBarSetUp{
    
    //show navigation bar - hidden in TabBarController
    if (self.navigationController.navigationBar.hidden ==YES) {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
    
 
    [WSCore flatNavBarOnView:self];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationController.navigationBarHidden=NO;
}


#pragma mark - UITableView datasource & delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    

    //if current page is less than total page AND salon array is not 0 return salon array +1
    if (self.browsePageCurrentPage<self.totalBrowsePageCount && self.allSalons.count!=0) {
                
        return self.allSalons.count+1;
    }
            
            
    return self.allSalons.count;

}

- (BrowseSalonTableViewCell *)salonCellForIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {

    BrowseSalonTableViewCell *cell = (BrowseSalonTableViewCell *) [self.tableView dequeueReusableCellWithIdentifier:@"BrowseSalonTableViewCell"];

    cell.myDelegate=self;
    Salon * salon = [self.allSalons objectAtIndex:indexPath.row];
    
    if (indexPath.row==0) {
        cell.isNearest=YES;
        
    }
    else{
        cell.isNearest=NO;
    }
    [cell setUpCellWithSalon:salon];
    
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = nil;
    
    if(indexPath.row <self.allSalons.count){
                
        cell=[self salonCellForIndexPath:indexPath tableView:tableView];
        
    }
    
    else{
        cell=[self loadingCell];
        
    }
    
    return cell;
}

-(void)didClickOnMapPin:(Salon* )salon {
    MapViewController*viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"mapVC"];
    viewController.salonArray = self.allSalons;
    viewController.selelctedSalon = salon;
    viewController.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:viewController animated:YES];
    self.navigationController.hidesBottomBarWhenPushed=NO;
}

-(void)didClickIntantBookFor:(Salon *)salon {
    [WSCore setBookingType:BookingTypeBrowse];
    
    TabMakeBookingViewController * bookVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tabMakeBookingVC"];
    bookVC.salonData = salon;
    bookVC.isFromFavourite = salon.is_favourite;
    bookVC.unwindBackToSearchFilter = NO;
    
    [self.navigationController pushViewController:bookVC animated:YES];
}

/*
 Configures the loading cell
 adds tag to loading cell for comparision
 */
-(UITableViewCell *)loadingCell{
    
    
    BrowseLoadingTableViewCell * cell = [[BrowseLoadingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    [cell configureCell];
    cell.tag=kLoadingCellBrowseTag;
    return cell;
}

/*
 willDisplayCEll
 if cell tag is loading tag (kLoadinCellBrowseTag)
    hide the footerView
    if page is not currently searching for salons
        increment the browsePageCurrentPage
        make call after 1.5 second delay (might look into reducing this)
 
 update: removed delay and added '[self setSalonURLAndCall]'
 
 */
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (cell.tag==kLoadingCellBrowseTag) {
        
        
        [self hideFooterView];
        
            if (!self.isLoadingSalons) {
                self.isLoadingSalons=YES;//added
                self.browsePageCurrentPage++;
                //[self performSelector:@selector(setSalonURLAndCall) withObject:nil afterDelay:1.5f];
                [self setSalonURLAndCall];
                
            }
        }
    else{
        //update footer height
//        [self configureFooterView];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    return CellHeight;
}

/*
 didSelectRowAtIndexPath
 set the booking type browse
 go to detail page
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
  [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        [WSCore setBookingType:BookingTypeBrowse];
        //push to salon detail
       // TabSalonDetailViewController * detailSalon = [self.storyboard instantiateViewControllerWithIdentifier:@"salonDetailVC"];;
        TabNewSalonDetailViewController * detailSalon = [self.storyboard instantiateViewControllerWithIdentifier:@"newSalonDetailVC"];
        detailSalon.salonData = [self.allSalons objectAtIndex:indexPath.row];
        detailSalon.myDelegate=self;
    
    
        [self.navigationController pushViewController:detailSalon animated:YES];
    
    
}



#pragma mark - Actions

/*
 MapButton
    if not iPhone4 then perform the flip animation
 
        else
 
        perform segue
 */
- (IBAction)mapButton:(id)sender {
    
    
    
    if (!IS_iPHONE4) {
        MapViewController*viewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"mapVC"];
        viewController.salonArray = self.allSalons;
    
        
        [UIView beginAnimations:@"View Flip" context:nil];
        [UIView setAnimationDuration:0.80];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        
        [UIView setAnimationTransition:
         UIViewAnimationTransitionFlipFromRight
                               forView:self.navigationController.view cache:NO];
        viewController.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:viewController animated:YES];
        self.navigationController.hidesBottomBarWhenPushed=NO;
        [UIView commitAnimations];
        
    }else{
        
        MapViewController*viewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"mapVC"];
        viewController.salonArray = self.allSalons;
             viewController.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:viewController animated:YES];
         self.navigationController.hidesBottomBarWhenPushed=NO;
    }
    
}


/*
setSalonURLAndCall
 
updates the salon url and make request
 
params contains the url
 
 if it is not self.showInstant
    then request for tier_1, tier_2, and tier_3
 
 else
    request only tier_1
 
 sends page number
 
 if the hasDiscoveryLatLong (DiscoveryPage is the page before this page)
    then append self.discoveryLat, self.discoveryLon
 
 else send the users lat and long
 
 send the user id if the user is logged in
 
 if the category id is not 0 then send the category id also
 
 make request
 */
-(void)setSalonURLAndCall{
    
    
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSearch_Salons_URL]];
    NSString * params = [NSString string];
    
    if (!self.showInstant) {
        params=[NSString stringWithFormat:@"tier_1=1"];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_2=1"]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_3=1"]];
    }
    else{
        params=[NSString stringWithFormat:@"tier_1=1"];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_2=0"]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_3=0"]];
        
    }

    params = [params stringByAppendingString:[NSString stringWithFormat:@"&page=%ld",(long)self.browsePageCurrentPage]];
    
    
    if (self.hasDiscoveryLatLong) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%@&longitude=%@",self.discoveryLat,self.discoveryLon]];
    }
    else if(self.location.coordinate.latitude!=0 && self.location.coordinate.longitude!=0){
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%f&longitude=%f",self.location.coordinate.latitude,self.location.coordinate.longitude]];
    }
    
    /*
    CLLocation * testLoc = [[User getInstance] fetchFarAwayLocation];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%@&longitude=%@",testLoc.coordinate.latitude,testLoc.coordinate.longitude]];
     */
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%@",kSearchRadius]];

    
    if ([[User getInstance] isUserLoggedIn]) {
        params =[params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken]]];
    }
    
    if (self.selectedCategory!=0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&filter_category_id=%d",self.selectedCategory]];
    }

    params = [params stringByAppendingString:@"&client_version=3"];
    //NSLog(@"1Params %@",params);
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}




#pragma mark - GCNetworkDelegate
/*
 if request fails
 show message alertView
 show tableView
 */
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    //[UIView showSimpleAlertWithTitle:jsonDictionary[@"message"] message:@"" cancelButtonTitle:@"OK"];
    [self configureFooterView];
    self.tableView.hidden=NO;
   
}

/*
 If successful
 set isLoadingSalon (BOOL) to NO
 show tableview
 parse the array
 reload the table view
 */
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    self.isLoadingSalons=NO;
    self.tableView.hidden=NO;
    
    [self.tableView reloadDataWithFade:YES];
   
    NSMutableArray *allSalonsArray = [NSMutableArray array];
    NSArray * salonsArray = jsonDictionary[@"message"][@"results"];
    for (NSDictionary *sDict in salonsArray) {
        
        Salon *salon = [Salon salonWithID:[sDict[@"salon_id"] stringValue] ];
        
        [salon updateCurrenecyWithJsonDictionary:sDict];
       
        [salon updateSalonDetailsWithJsonDictionary:sDict];
        
        [allSalonsArray addObject:salon];
    }
    
    
    if (allSalonsArray.count==0) {
        
        //[self addNoDataMessage];
    
    }
    else{
    
            
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            
        [self.allSalons addObjectsFromArray:allSalonsArray];
        self.totalBrowsePageCount=[jsonDictionary[@"message"][@"total_pages"] intValue];
            
        
        if (self.isNoLocationViewShowing) {
            self.isNoLocationViewShowing=NO;
            [self removeNoLocationMessageView];
        }
        //self.isNoSalonsLocationViewShowing=NO;
    }
    
    if (self.refreshControl) {
        [self.refreshControl endRefreshing];
    }

}

#pragma mark - FilterDelegate

-(void)filterSearchWithInstantBook : (BOOL) instant WithDistance: (int) distance AndWithCategory:(int)cat{
    
    
    self.selectedCategory=cat;
    
    self.showInstant = instant;
    switch (cat) {
        case 0:
            [self filterNavigationBarButtonWithTitle:@"Salons Nearby"];
            break;
        case 1:
            [self filterNavigationBarButtonWithTitle:@"Hair"];
            break;
        case 2:
            [self filterNavigationBarButtonWithTitle:@"Hair Removal"];
            break;
        case 3:
            [self filterNavigationBarButtonWithTitle:@"Nails"];
            break;
        case 4:
            [self filterNavigationBarButtonWithTitle:@"Massage"];
            break;
        case 5:
            [self filterNavigationBarButtonWithTitle:@"Face"];
            break;
        case 6:
            [self filterNavigationBarButtonWithTitle:@"Body"];
            break;
            
        default:
            [self filterNavigationBarButtonWithTitle:@"All Salons"];
            break;
    }
    [self resetPageAndCall];
    
}

#pragma mark - UIViewControllerTransitioningDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return [[DismissDetailTransition alloc] init];
}

#pragma mark - UNWIND Segue
/*
 Unwind back to this view
 then reset the booking type
 */
- (IBAction)unwindToTabBrowse:(UIStoryboardSegue *)unwindSegue
{

    //NSLog(@"Unwind to tab browse");
    [WSCore resetBookingType];
}

- (IBAction)filterBarButtonAction:(id)sender {
    
    [self showFilterSearchViewController];
}

#pragma - 3D Touch
-(void)registerFor3dTouch{
    if ([self.traitCollection
         respondsToSelector:@selector(forceTouchCapability)] &&
        (self.traitCollection.forceTouchCapability ==
         UIForceTouchCapabilityAvailable))
    {
        [self registerForPreviewingWithDelegate:self sourceView:self.view];
    }
}

- (UIViewController *)previewingContext:
(id<UIViewControllerPreviewing>)previewingContext
              viewControllerForLocation:(CGPoint)location {
    
    
    
    CGPoint convertedLocation = [self.view convertPoint:location toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView
                              indexPathForRowAtPoint:convertedLocation ];
    self.selectedPreviewIndexPath=indexPath;
    SalonPreviewViewController *salonPreview = [self.storyboard instantiateViewControllerWithIdentifier:@"salonPreviewVC"];
    salonPreview.delegate=self;
    Salon * salon = [self.allSalons objectAtIndex:indexPath.row];
    if (salon) {
        BrowseTableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (cell) {
            CGRect focusFrame = cell.frame;
            focusFrame.origin.y +=kNavBarAndStatusBarHeight;
            focusFrame =CGRectOffset(focusFrame, -self.tableView.contentOffset.x, -self.tableView.contentOffset.y);
            previewingContext.sourceRect=focusFrame;
            salonPreview.salonData=salon;
            return salonPreview;

        }
    }
    
    return nil;
}



- (void)previewingContext:
(id<UIViewControllerPreviewing>)previewingContext
     commitViewController:(UIViewController *)viewControllerToCommit {
    
    
    TabNewSalonDetailViewController * detailSalon = [self.storyboard instantiateViewControllerWithIdentifier:@"newSalonDetailVC"];
    detailSalon.salonData = [self.allSalons objectAtIndex:self.selectedPreviewIndexPath.row];
    detailSalon.myDelegate=self;
    [self.navigationController pushViewController:detailSalon animated:NO];
   
}

#pragma mark - SalonPreviewDelegate 
/*
 if salon tier is 3
 then only allow the user to make a call
 else goes to the TabMakeBookingViewController
 */
-(void)salonPreviewViewController:(SalonPreviewViewController *)preview didSelectMakeBookingOrCallActionItem:(UIPreviewAction *)action WithSalon:(Salon *)salon{
    
    if (salon.salon_tier==3.0) {
        
        
        if (salon.salon_landline>0) {
            
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
                NSString *telno = [NSString stringWithFormat:@"tel://%@",salon.salon_landline];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telno]];
            }
        }
        else{
            [UIView showSimpleAlertWithTitle:@"No number provided by Salon" message:@"" cancelButtonTitle:@"OK"];
        }

    }
    else{
        [WSCore setBookingType:BookingTypeBrowse];
        TabMakeBookingViewController * bookVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tabMakeBookingVC"];
        bookVC.salonData = salon;
        [self.navigationController pushViewController:bookVC animated:YES];
    }
}

/*
 Shows the SalonReviewListViewController
 */
-(void)salonPreviewViewController:(SalonPreviewViewController *)preview didSelectReadReviewsActionItem:(UIPreviewAction *)action WithSalon:(Salon *)salon{
    
    SalonReviewListViewController * salonReview = [self.storyboard instantiateViewControllerWithIdentifier:@"reviewList"];
    salonReview.salonData=salon;
    salonReview.transitioningDelegate=self;
    salonReview.modalPresentationStyle = UIModalPresentationCustom;
   // salonReview.isList=YES;
    [self presentViewController:salonReview animated:YES completion:nil];
}

/*
 Shows the slide show 
 */
-(void)salonPreviewViewController:(SalonPreviewViewController *)preview didSelectSeeGalleryActionItem:(UIPreviewAction *)action WithSalon:(Salon *)salon{
  
    SlideShowViewController *slideShow = [self.storyboard instantiateViewControllerWithIdentifier:@"SlideShowVC"];
    slideShow.transitioningDelegate=self;
    slideShow.modalPresentationStyle = UIModalPresentationCustom;
    slideShow.salonData=salon;
    slideShow.startIndex=0;
   
    [self presentViewController:slideShow animated:YES completion:nil];
}

@end
