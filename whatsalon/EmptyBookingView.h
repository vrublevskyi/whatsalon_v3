//
//  EmptyBookingView.h
//  whatsalon
//
//  Created by admin on 10/2/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@class EmptyBookingView;
@protocol EmptyBookingViewDelegate <NSObject>
- (void) onMakeBookingTapped: (EmptyBookingView *) sender;
@end

@interface EmptyBookingView : UIView
@property (weak, nonatomic) IBOutlet UIButton *makeBookButton;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (nonatomic, weak) id <EmptyBookingViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
