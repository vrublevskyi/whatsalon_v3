//
//  UISegmentedControl+setSegments.m
//  whatsalon
//
//  Created by Graham Connolly on 18/07/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "UISegmentedControl+setSegments.h"

@implementation UISegmentedControl (setSegments)

- (void)setSegments:(NSArray *)segments
{
    int i=0;
    while(self.numberOfSegments > 0)
    {
        [self removeSegmentAtIndex:0 animated:NO];
    }
    
    
    for (NSString *segment in segments)
    {
        if (i<=4) {
            [self insertSegmentWithTitle:segment atIndex:self.numberOfSegments animated:NO];
            i++;
        }
        
    }
}
@end
