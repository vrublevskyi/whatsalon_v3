//
//  CurrencyItem.h
//  whatsalon
//
//  Created by Graham Connolly on 19/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyItem : NSObject

@property (nonatomic) NSString *conversionRate;
@property (nonatomic) NSString *currencyName;
@property (nonatomic) NSString *currencyId;
@property (nonatomic) NSString * symbolUtf8;

-(void)updatedCurrencySymbol: (NSString *) currency;
@end
