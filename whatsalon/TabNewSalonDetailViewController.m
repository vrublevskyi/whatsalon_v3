//
//  TabNewSalonDetailViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 02/09/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabNewSalonDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TabMakeBookingViewController.h"//tab make booking
#import "GetDirectionsViewController.h"//tab
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "User.h"
#import "UIView+AlertCompatibility.h"
#import "SlideShowViewController.h"//tab
#import "BiographyTableViewCell.h"

#define kMapCache @"WSMapCache"

#import "GCPopInTransition.h"
#import "GCPopOutTransition.h"
#import "GCImagePresentTransition.h"
#import "GCImageDismissTransition.h"

#import "ServicesIconTableViewCell.h"
#import "DirectionsMapTableViewCell.h"

#define kStaticMap @"AIzaSyD0RrghF_SmwrHUfHZ_EcTBNrvpMCQs0Nk"

#import "GCSplitPresentTransition.h"
#import "GCSplitDismissTransition.h"
#import <QuartzCore/QuartzCore.h>
#import "OpeningDay.h"
#import "SalonReviewTableViewCell.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SalonReviewListViewController.h"//tab
#import "ConfirmViewController.h"//removed
#import "WSCoachMarksView.h"
#import "NSString+Icons.h"
#import "UIImage+Extras.h"
#import "FUIAlertView.h"
#import "SubmitReviewSalonDetailViewController.h"
#import "TabFavouritesViewController.h"
#import "whatsalon-Swift.h"

@interface TabNewSalonDetailViewController ()<UITableViewDataSource,UITableViewDelegate,UIViewControllerTransitioningDelegate,SlideShowDelegate,SalonReviewDelegate,WSCoachMarksViewDelegate,GCNetworkManagerDelegate,ReviewSalonDelegate>

/*! @brief represents the number of pages for the slide show. */
@property (weak, nonatomic) IBOutlet UILabel *noOfPagesLabel;

/*! @brief represents the scroll view for hosting the salon images. */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

/*! @brief represents the salon title label. */
@property (weak, nonatomic) IBOutlet UILabel *salonTitle;

/*! @brief represents the view that holds salon content.

 @discussion holder view contains the scrollView, no of pages label, salon title, star rating, no of reviews, fav button.
 */
@property (weak, nonatomic) IBOutlet UIView *holderView;
@property (weak, nonatomic) IBOutlet UIButton *bookButton;

/*!
 @brief represents the UIImageView that shows the number of stars of the salon.
 */
@property (weak, nonatomic) IBOutlet UIImageView *starRating;

/*!
 @brief represents the UILabel that shows the number of reviews that the salon has.
 */
@property (weak, nonatomic) IBOutlet UILabel *noOfReviews;

/*!
 @brief used for favouriting a salon.
 */
@property (weak, nonatomic)  UIButton *favButton;

/*!
 @brief an array of UIViews for the number of pages in the slide show.
 */
@property (nonatomic, strong) NSMutableArray *pageViews;

/*!
 @brief an NSString that contains the address of the salon.
 */
@property (nonatomic) NSString * fullAddress;

/*!
 @brief determines which page is currently visible
 */
- (void)loadVisiblePages;
/*!
 @brief loads in the pages.
 @param page the page to load in
 */
- (void)loadPage:(NSInteger)page;

/*!
 @brief removes the pages that are no longer needed.
 @param page the current page.
 */
- (void)purgePage:(NSInteger)page;

/*!
 @brief the number of pages in the slide show.
 */
@property (nonatomic) NSInteger noOfPages;

/*!
 @brief represents an array of salon images.
 */
@property (nonatomic,strong) NSMutableArray * salonImages;

/*!
 @brief determines if the salon is a users favourite.
 */
@property (nonatomic,assign) BOOL isFav;


/*!
 @brief Bool which determines whether more of the biography should be seen.
 */
@property (nonatomic,assign) BOOL showMoreBiography;

/*!
 @brief represents the footer view.
 */
@property (weak, nonatomic) IBOutlet UIView *footerView;

/*!
 @brief represents the MKMapShanpshotOptions.
 */
@property (nonatomic,weak) MKMapSnapshotOptions *options;

/*!
 @brief represents MkMapSnapshotter.
 */
@property (nonatomic,weak) MKMapSnapshotter *snapshotter;

/*!
 @brief the current page showing in the slide show.
 */
@property (nonatomic) NSInteger currentPage;

/*!
 @brief represents the width of a scrollView that is used to update the content offset.
 */
@property (nonatomic) CGFloat w;

/*!
 @brief represents a timer for scrolling the scroll view.
 */
@property (nonatomic) NSTimer * scrollTimer;

/*!
 @brief determines if the direction transition is to be performed.
 */
@property (nonatomic) BOOL isDirectsionsTransition;

/*!
 @brief determines if the slide show transition is to be performed.
 */
@property (nonatomic) BOOL isSlideShowTranition;

/*!
 @brief represents the image of the map
 */
@property (nonatomic) UIImage * mapImage;

/*!
 @brief determines if the show reviews transition should be performed.
 */
@property (nonatomic) BOOL showReviews;

/*!
 @brief determines if the show category list transition should be permored.
 */
@property (nonatomic) BOOL showCategoryList;

/*!
 @brief represents the network manager for making requests to favourite/unfavourite a salon.
 */
@property (nonatomic) GCNetworkManager * favNetworkManager;

/*!
 @brief represents the network manaager for making request to record a request to make a phone call.
 */
@property (nonatomic) GCNetworkManager * callManager;

/*!
 @brief represents the network manager for making a request to record an impression on the page.
 */
@property (nonatomic) GCNetworkManager * impressionManager;

/*!
 @brief determines if this salon has categories assigned.
 */
@property (nonatomic) BOOL hasCategories;

/*!
 @brief represents the view that is shown while a request to record.
 */
@property (nonatomic) UIView * phonePlaceholderForLoading;

/*!
 @brief determines whether SubmitReviewVC transition is taking place.
 */
@property (nonatomic) BOOL showSubmitReviewVC;
@end

@implementation TabNewSalonDetailViewController





- (void)callNowButtonSetUp {


    [self.bookButton setTitle:@"" forState:UIControlStateNormal];

    UILabel * callLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, self.bookButton.frame.size.width-75, self.bookButton.frame.size.height-10)];
    callLabel.text = @"Instant Book not available\nCall to book your appointment";
    callLabel.textColor = [UIColor whiteColor];
    callLabel.numberOfLines=2;
    callLabel.textAlignment=NSTextAlignmentCenter;
    callLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.0f];
    [WSCore addRightLineToView:callLabel withWidth:0.5 AndColor:[UIColor whiteColor]];

    UIView * phoneHolderView = [[UIView alloc] initWithFrame:CGRectMake(self.bookButton.frame.size.width-75, 2, 35, 30)];
    phoneHolderView.backgroundColor = [UIColor clearColor];
    [self.bookButton addSubview:phoneHolderView];
    UIImageView * phoneImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    phoneImage.contentMode = UIViewContentModeScaleAspectFit;
    phoneImage.image = [UIImage imageNamed:@"telephone_filled_icon"];
    phoneImage.image = [phoneImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    phoneImage.tintColor = [UIColor whiteColor];
    phoneImage.clipsToBounds=YES;
    phoneHolderView.clipsToBounds=YES;
    [phoneHolderView addSubview:phoneImage];
    phoneImage.center = CGPointMake(phoneHolderView.frame.size.width/2.0f, phoneHolderView.frame.size.height/2.0f);
    phoneHolderView.userInteractionEnabled=NO;
    phoneImage.userInteractionEnabled=NO;

    [Gradients  addBlueToPurpleHorizontalLayerWithView:callLabel];
    [Gradients  addPinkToPurpleHorizontalLayerWithView:phoneHolderView ];

    [self.bookButton addSubview:callLabel];
}

-(void)updateNumberOfStars{

    self.starRating.image = [WSCore getStarRatingImage:(int)self.salonData.ratings];


    if ((int)self.salonData.reviews==1) {
        self.noOfReviews.text =[NSString stringWithFormat:@"%d review",(int)self.salonData.reviews];


    }else{
        self.noOfReviews.text =[NSString stringWithFormat:@"%d reviews",(int)self.salonData.reviews];
    }

    if ((int)self.salonData.reviews==0) {
        self.starRating.image = [self.starRating.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.starRating setTintColor:[UIColor lightGrayColor]];
        NSLog(@"No stars");
    }else{
        self.starRating.image = [self.starRating.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.starRating setTintColor:kYelowStar];
    }

}
- (void)viewDidLoad {
    [super viewDidLoad];


    [WSCore addTopLine:self.footerView :kCellLinesColour :0.5];

    //adjust details
    self.salonTitle.text = self.salonData.salon_name;
    self.salonTitle.adjustsFontSizeToFitWidth=YES;
    self.fullAddress = [self.salonData fetchFullAddress];


    if ([WSCore isDummyMode]) {
        // self.salonImages =[[NSMutableArray alloc] initWithArray: [WSCore getDummySalonGallery]];
    }
    else{

        //if there are no images have just show the default images
        if ([self.salonData fetchSlideShowGallery].count ==0) {
            [self.salonImages removeAllObjects];
            self.salonImages =self.salonData.placeHolderImageGallery;

            /*
             UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Images" message:[NSString stringWithFormat:@"Unable to download images for %@. Please try again later.",self.salonData.salon_name] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];
             */

        }else{
            self.salonImages = [[NSMutableArray alloc] initWithArray:[self.salonData fetchSlideShowGallery]];

            [self.salonImages removeLastObject];

        }



    }
    [self navigationLikeButton];
    [self updateNumberOfStars];

    self.holderView.backgroundColor=[UIColor whiteColor];
    self.holderView.userInteractionEnabled=YES;




    NSInteger pageCount = self.salonImages.count;

    self.noOfPages=pageCount;



    // 3
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }

    self.scrollView.delegate=self;

    self.noOfPagesLabel.text = [NSString stringWithFormat:@"%d/%ld",1,(long)self.noOfPages];

    self.tableView.delegate=self;
    self.tableView.dataSource=self;


    self.bookButton.layer.cornerRadius=3.0f;
    self.bookButton.layer.masksToBounds=YES;
    self.bookButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.bookButton.layer.borderWidth = 2;
    self.bookButton.backgroundColor = kWhatSalonBlue;

    /*
     handles the bottom button apearance.
     */
    if(self.salonData.salon_tier==3.0){



        [self callNowButtonSetUp];


    }
    else{
        [self.bookButton setTitle:@"  See Availability" forState:UIControlStateNormal];
        UIImage * bolt = [[UIImage imageNamed:@"instant_booking_filled_discovery"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.bookButton setImage:[bolt imageByScalingProportionallyToSize:CGSizeMake(15,15)] forState:UIControlStateNormal];
        self.bookButton.tintColor = [UIColor sunflowerColor];
        [Gradients  addPinkToPurpleHorizontalLayerWithView:_bookButton ];


    }



    [self.bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal
     ];
    [self.bookButton addTarget:self action:@selector(book) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.bookButton];
    [WSCore floatingView:self.bookButton];
    [self.view bringSubviewToFront:self.bookButton];

    UITapGestureRecognizer * scrollTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toSlideShow)];
    scrollTap.numberOfTapsRequired=1;
    [self.scrollView addGestureRecognizer:scrollTap];

    self.scrollView.userInteractionEnabled=YES;
    self.holderView.backgroundColor=kViewColor;

    self.view.backgroundColor=kViewColor;


    self.footerView.backgroundColor=kViewColor;


    //MKSnapshotter
    NSString * mapName = [NSString stringWithFormat:@"map%f%f",self.salonData.salon_lat,self.salonData.salon_long];


    /*
     used to take the screen shot for the map image
     */
    if (IS_IOS_8_OR_LATER) {
        if (![self checkIfMapExistsAtKey:mapName]) {

            MKMapSnapshotOptions * snapOptions= [[MKMapSnapshotOptions alloc] init];
            self.options=snapOptions;
            CLLocation * salonLocation = [[CLLocation alloc] initWithLatitude:self.salonData.salon_lat longitude:self.salonData.salon_long];
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(salonLocation.coordinate, 300, 300);
            self.options.region = region;
            self.options.size = self.view.frame.size;
            self.options.scale = [[UIScreen mainScreen] scale];

            MKMapSnapshotter * mapSnapShot = [[MKMapSnapshotter alloc] initWithOptions:self.options];
            self.snapshotter =mapSnapShot;
            [self.snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
                if (error) {
                    // NSLog(@"[Error] %@", error);
                    return;
                }

                UIImage *image = snapshot.image;
                self.mapImage = image;
                NSData *data = UIImagePNGRepresentation(image);

                [self saveMapDataToCache:data WithKey:mapName];
            }];
        }else{
            UIImage * dataImage = [UIImage imageWithData:[self fetchFileAtKey:mapName]];
            self.mapImage= dataImage;
        }
    }

    //for automatic scrolling on the slide show
    self.scrollTimer =[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];

    [self setUpOpeningHoursSectionAppearance];


    [self.tableView registerClass:[ServicesIconTableViewCell class] forCellReuseIdentifier:@"iconCell"];

    [self.tableView registerClass:[DirectionsMapTableViewCell class] forCellReuseIdentifier:@"mapCell"];

    [self.tableView registerClass:[SalonReviewTableViewCell class] forCellReuseIdentifier:@"reviewCell"];


    self.noOfPagesLabel.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.4];
    self.noOfPagesLabel.layer.cornerRadius=6.0;
    self.noOfPagesLabel.layer.masksToBounds=YES;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    self.showCategoryList=NO;

    self.favNetworkManager = [[GCNetworkManager alloc] init];
    self.favNetworkManager.delegate=self;
    self.callManager = [[GCNetworkManager alloc] init];
    self.callManager.delegate=self;
    self.callManager.shouldNotShowMessages=YES;
    self.impressionManager = [[GCNetworkManager alloc] init];
    self.impressionManager.delegate =self;
    self.impressionManager.shouldNotShowMessages=YES;

    [self recordVisitImpression];


    int numberOfCategories=0;
    for(NSString* key in self.salonData.salonCategories) {
        BOOL bValue = [[self.salonData.salonCategories objectForKey:key] boolValue];
        //  NSLog(@"%@ = %@",key,StringFromBOOL(bValue));
        if (bValue==YES) {
            numberOfCategories++;
        }
    }


    if (numberOfCategories==0) {
        self.hasCategories=NO;
    }
    else{
        self.hasCategories=YES;
    }

    [self.view layoutIfNeeded];
    for (int i = 0; i < self.salonImages.count; ++i) {
        [self loadPage:i];
    }
}


-(void)setUpOpeningHoursSectionAppearance{
    //opening hours
    if (self.salonData.openingHours.count==0) {
        UILabel* monday = [[UILabel alloc] initWithFrame:CGRectMake(8, 37, 240, 20)];
        monday.text = @"No Opening hours provided.";
        monday.font=[UIFont systemFontOfSize:14.0f];
        monday.textColor = [UIColor lightGrayColor];
        [self.footerView addSubview:monday];
        self.footerView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 120);
    }
    else{

        UILabel* monday = [[UILabel alloc] initWithFrame:CGRectMake(8, 37, 170, 20)];
        monday.text = @"Monday";
        monday.font=[UIFont systemFontOfSize:14.0f];
        monday.textColor = [UIColor lightGrayColor];
        [self.footerView addSubview:monday];

        UILabel* tuesday = [[UILabel alloc] initWithFrame:CGRectMake(8, 60, 170, 20)];
        tuesday.text = @"Tuesday";
        tuesday.font=[UIFont systemFontOfSize:14.0f];
        tuesday.textColor = [UIColor lightGrayColor];
        [self.footerView addSubview:tuesday];

        UILabel* weds = [[UILabel alloc] initWithFrame:CGRectMake(8, 83, 170, 20)];
        weds.text = @"Wednesday";
        weds.textColor = [UIColor lightGrayColor];
        weds.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:weds];

        UILabel* thurs = [[UILabel alloc] initWithFrame:CGRectMake(8, 106, 170, 20)];
        thurs.text = @"Thursday";
        thurs.textColor = [UIColor lightGrayColor];
        thurs.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:thurs];

        UILabel* fri = [[UILabel alloc] initWithFrame:CGRectMake(8, 129, 170, 20)];
        fri.text = @"Friday";
        fri.textColor = [UIColor lightGrayColor];
        fri.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:fri];

        UILabel* sat = [[UILabel alloc] initWithFrame:CGRectMake(8, 152, 170, 20)];
        sat.text = @"Saturday";
        sat.textColor = [UIColor lightGrayColor];
        sat.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:sat];

        UILabel* sun = [[UILabel alloc] initWithFrame:CGRectMake(8, 175, 170, 20)];
        sun.text = @"Sunday";
        sun.textColor = [UIColor lightGrayColor];
        sun.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:sun];

        UILabel * chosenLabel;
        for (OpeningDay*day in self.salonData.openingHours) {

            switch ([day.dayOfWeek integerValue]) {
                case 1:
                    sun.text=day.dayName;
                    chosenLabel=sun;
                    break;
                case 2:
                    monday.text=day.dayName;
                    chosenLabel=monday;
                    break;
                case 3:
                    tuesday.text=day.dayName;
                    chosenLabel=tuesday;
                    break;
                case 4:
                    weds.text=day.dayName;
                    chosenLabel=weds;
                    break;
                case 5:
                    thurs.text=day.dayName;
                    chosenLabel=thurs;
                    break;
                case 6:
                    fri.text=day.dayName;
                    chosenLabel=fri;
                    break;
                case 7:
                    sat.text=day.dayName;
                    chosenLabel=sat;
                    break;
                default:
                    break;
            }


            UILabel * time = [[UILabel alloc] initWithFrame:CGRectMake(chosenLabel.frame.origin.x-10, chosenLabel.frame.origin.y, self.view.frame.size.width-10, 18)];
            time.font=[UIFont systemFontOfSize:14.0f];
            if (day.isOpened) {
                time.text = [NSString stringWithFormat:@"%@ - %@  ",[WSCore getShortTimeFromDate:day.startTime WithFormat:@"HH:mm"],[WSCore getShortTimeFromDate:day.endTime WithFormat:@"HH:mm"]];
            }
            else{
                time.text = @"Closed  ";
            }

            time.textColor=[UIColor lightGrayColor];
            time.textAlignment=NSTextAlignmentRight;
            [self.footerView addSubview:time];


        }

    }
}
/*
 recordVisitImpression
 records when users visit this page
 uses the user id if the user is logged in
 */
-(void)recordVisitImpression{

    //if (self.salonData.salon_tier==3) {
    NSString * params = [NSString stringWithFormat:@"salon_id=%@",self.salonData.salon_id];
    if ([[User getInstance] isUserLoggedIn]) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance]fetchAccessToken ]]];
    }

    if (self.salonData.salon_tier) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier=%d", (int)self.salonData.salon_tier ]];
    }


    [self.impressionManager loadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kTier3_Impressions_URL] ] withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
    //}
}




- (void)onTimer {


    NSInteger numberOfPages = self.salonImages.count - 1;
    if (self.currentPage==numberOfPages) {

        NSLog(@"End Timer");
        [self.scrollTimer invalidate];
        return;
    }

    // Updates the variable h, adding 100 (put your own value here!)
    self.w += CGRectGetWidth(self.scrollView.frame);

    //This makes the scrollView scroll to the desired position
    [self.scrollView setContentOffset:CGPointMake(self.w, 0) animated:YES];

    //[self loadVisiblePages];
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{

    [self.scrollTimer invalidate];

}



-(void)goToMap: (UITableViewCell *)cell{


    self.isDirectsionsTransition=YES;

    GetDirectionsViewController *destinationViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
    destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
    destinationViewController.transitioningDelegate = self;
    destinationViewController.salonMapData = self.salonData;
    [self.scrollTimer invalidate];
    [self presentViewController:destinationViewController animated:YES completion:nil];


}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self releaseMKMapSnapshotMem];
    [self.scrollTimer invalidate];
    [self.impressionManager destroy];
}
-(void)releaseMKMapSnapshotMem{
    self.snapshotter=nil;
    self.options=nil;


}

//MKMapSnapShotter
-(void)saveMapDataToCache: (NSData *) data WithKey: (NSString *) key{

    //check if the cache exits, if not create it
    if ([self createCacheIfItDoesNotExist]) {
        NSString *path;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        path = [[paths objectAtIndex:0] stringByAppendingPathComponent:kMapCache];
        path = [path stringByAppendingPathComponent:key];

        [[NSFileManager defaultManager] createFileAtPath:path
                                                contents:data
                                              attributes:nil];
    }
    //An error occurred when creating the caching

}

-(BOOL)createCacheIfItDoesNotExist{
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:kMapCache];
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])    //Does directory already exist?
    {
        if (![[NSFileManager defaultManager] createDirectoryAtPath:path
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            // NSLog(@"Create directory error: %@", error);
            return NO;
        }

        return YES;
    }

    return YES;
}

-(BOOL)checkIfMapExistsAtKey: (NSString *) key{

    NSString *path2;
    NSArray *path2s = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path2 = [[path2s objectAtIndex:0] stringByAppendingPathComponent:kMapCache];
    path2 = [path2 stringByAppendingPathComponent:key];

    if ([[NSFileManager defaultManager] fileExistsAtPath:path2])
    {
        //File exists
        NSData *file1 = [[NSData alloc] initWithContentsOfFile:path2];
        if (file1)
        {

            return YES;
        }
    }

    return NO;

}

-(NSData *)fetchFileAtKey: (NSString *)key{


    NSString *path2;
    NSArray *path2s = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path2 = [[path2s objectAtIndex:0] stringByAppendingPathComponent:kMapCache];
    path2 = [path2 stringByAppendingPathComponent:key];

    if ([[NSFileManager defaultManager] fileExistsAtPath:path2])
    {
        //File exists
        NSData *file1 = [[NSData alloc] initWithContentsOfFile:path2];
        if (file1)
        {

            return file1;
        }
    }

    return nil;

}

-(void)toSlideShow{

    self.isSlideShowTranition=YES;
    [self.scrollTimer invalidate];
    SlideShowViewController *slideShow = [self.storyboard instantiateViewControllerWithIdentifier:@"SlideShowVC"];
    if (IS_iPHONE5_or_Above) {
        slideShow.modalPresentationStyle=UIModalPresentationCustom;
        slideShow.transitioningDelegate=self;
    }
    else{
        slideShow.modalPresentationStyle=UIModalPresentationCurrentContext;
    }

    if ([WSCore isDummyMode]) {
        slideShow.dummyImageArray=self.salonImages;
    }
    slideShow.salonData=self.salonData;
    slideShow.startIndex=self.currentPage;
    slideShow.myDelegate=self;
    [self presentViewController:slideShow animated:YES completion:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    if ([segue.identifier isEqualToString:@"makeBookingSegue"]) {

        TabMakeBookingViewController * bookVC = (TabMakeBookingViewController *)segue.destinationViewController;
        bookVC.salonData = self.salonData;
        bookVC.isFromFavourite=self.isFromFavourite;
        //NSLog(@"Is from fav %d",self.isFromFavourite);
        bookVC.unwindBackToSearchFilter=self.unwindBackToSearchFilter;
        self.hidesBottomBarWhenPushed=YES;


    }

    else if ([segue.identifier isEqualToString:@"toMap"]) {

        GetDirectionsViewController *destinationViewController =
        (GetDirectionsViewController *)segue.destinationViewController;
        destinationViewController.salonMapData = self.salonData;
        destinationViewController.isSplitTransition=YES;


    }

    [self.scrollTimer invalidate];

}

-(void) navigationLikeButton
{

    UIButton *favouriteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    favouriteButton.frame = CGRectMake(0, 0, 45, 48);
    [favouriteButton addTarget:self action:@selector(markAsFavourite) forControlEvents:UIControlEventTouchUpInside];
    [[favouriteButton imageView] setContentMode: UIViewContentModeScaleAspectFit];

    favouriteButton.showsTouchWhenHighlighted = YES;

    UIImage *favImage = [UIImage imageNamed:@"unselectedFavLogo"];
    [favouriteButton setImage:favImage forState:UIControlStateNormal];
    if (self.salonData.is_favourite){
        UIImage *favImage = [UIImage imageNamed:@"selectedFavLogo"];
        [favouriteButton setImage:favImage forState:UIControlStateNormal];
    } else {
        favouriteButton.tintColor = UIColor.grayColor;
    }

    favouriteButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);

    UIBarButtonItem *favouriteBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:favouriteButton];


    self.navigationItem.rightBarButtonItem = favouriteBarButtonItem;
}

-(void)book{
    [self.scrollTimer invalidate];
    if (self.salonData.salon_tier==3.0) {


        [self recordCallImpression];
    }
    else{
        [self performSegueWithIdentifier:@"makeBookingSegue" sender:self];
    }



}
- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.salonImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }


    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {

        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;


        UIImageView * newPageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"empty_cell"]];

        if ([WSCore isDummyMode]) {
            newPageView.image = [self.salonImages objectAtIndex:page];
        }
        else{
            [newPageView sd_setImageWithURL:[self.salonImages objectAtIndex:page] placeholderImage:[UIImage imageNamed:[self.salonImages objectAtIndex:page]]];
        }

        newPageView.contentMode = UIViewContentModeScaleAspectFill;
        newPageView.frame = frame;
        newPageView.clipsToBounds = YES;
        [self.scrollView addSubview:newPageView];

        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.salonImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }


    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)loadVisiblePages {

    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    self.currentPage = page;
    self.noOfPagesLabel.text = [NSString stringWithFormat:@"%d/%ld",page + 1,(long)self.noOfPages];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView==self.scrollView) {
        [self loadVisiblePages];
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.title = self.salonData.salon_name;
    [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore flatNavBarOnView:self];

    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];


    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }


    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.salonImages.count, pagesScrollViewSize.height);

    self.tableView.delegate=self;
    self.tableView.dataSource=self;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {


    NSString *bioCell = @"bioCell";

    UITableViewCell *cell;

    if (indexPath.row==0) {
        cell = [tableView dequeueReusableCellWithIdentifier:bioCell forIndexPath:indexPath];

        NSArray *viewsToRemove = [cell.contentView subviews];
        for (UIView *v in viewsToRemove) {
            [v removeFromSuperview];
        }



        UILabel * descriptionLabel;
        if (descriptionLabel==nil) {


            NSInteger noOfLines=0;
            CGFloat readMoreLabelY = 0.0f;
            NSString * moreText;


            if (self.showMoreBiography) {

                noOfLines=0;
                readMoreLabelY=185;
                moreText=@"Less";
            }
            else{


                noOfLines=3;
                readMoreLabelY=115;
                moreText=@"Read More";
            }

            descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, cell.frame.size.width-20, 60)];
            if (self.salonData.salon_description.length==0) {
                descriptionLabel.text = @"   No description available";
                descriptionLabel.textColor=[UIColor lightGrayColor];
            }else{

                descriptionLabel.text =self.salonData.salon_description;
            }

            descriptionLabel.font = [UIFont systemFontOfSize:15.0f];
            NSDictionary *attributes = @{NSFontAttributeName: descriptionLabel.font};

            CGRect rect = [descriptionLabel.text boundingRectWithSize:CGSizeMake(cell.frame.size.width-20, 170)
                                                              options:NSStringDrawingUsesLineFragmentOrigin
                                                           attributes:attributes
                                                              context:nil];


            if (self.showMoreBiography) {


                CGRect currentLabelFrame = descriptionLabel.frame;

                currentLabelFrame.size.height = rect.size.height;

                descriptionLabel.frame = currentLabelFrame;
                descriptionLabel.adjustsFontSizeToFitWidth=YES;
            }


            //shows gradient
            if (!self.showMoreBiography) {
                CAGradientLayer *l = [CAGradientLayer layer];
                l.frame = descriptionLabel.bounds;
                l.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor clearColor].CGColor];
                l.startPoint = CGPointMake(1.f, .1f);
                l.endPoint = CGPointMake(1.0f, 1.0f);
                descriptionLabel.layer.mask = l;
                descriptionLabel.adjustsFontSizeToFitWidth=NO;

            }

            UILabel * moreLabel = [[UILabel alloc] initWithFrame:CGRectMake(105, readMoreLabelY, 110, 40)];
            moreLabel.text = moreText;
            moreLabel.font = [UIFont systemFontOfSize:14.0f];
            moreLabel.textColor=kWhatSalonBlue;
            moreLabel.textAlignment=NSTextAlignmentCenter;
            moreLabel.layer.borderColor=kWhatSalonBlue.CGColor;
            moreLabel.layer.borderWidth=1.0f;
            moreLabel.layer.cornerRadius=3.0f;
            [cell.contentView addSubview:moreLabel];


            descriptionLabel.numberOfLines=noOfLines;
            [cell.contentView addSubview:descriptionLabel];
        }

    }

    if(indexPath.row==1){

        ServicesIconTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"iconCell" forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.clipsToBounds=YES;
        cell.backgroundColor=kViewColor;
        [cell setUpCellWithSalonObject:self.salonData];
        return cell;
    }

    if (indexPath.row==2) {
        DirectionsMapTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"mapCell" forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.clipsToBounds=YES;
        cell.backgroundColor=kViewColor;
        if ([self.salonData fetchFullAddress].length>=30) {
            cell.directionsLabel.frame = CGRectMake(cell.directionsLabel.frame.origin.x, cell.directionsLabel.frame.origin.y, cell.directionsLabel.frame.size.width, 40);
            cell.directionsLabel.numberOfLines=0;
        }
        cell.directionsLabel.text=[self.salonData fetchFullAddress];
        cell.mapImageView.image = self.mapImage;


        cell.backgroundColor=kViewColor;
        return cell;

    }

    if (indexPath.row==3) {
        SalonReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reviewCell" forIndexPath:indexPath];
        cell.delegate=self;

        [cell setUpReviewPageWithReview:self.salonData.salonReview AndWithNumberOfReviews:(int)self.salonData.reviews];

        cell.backgroundColor=kViewColor;
        return cell;
    }


    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.clipsToBounds=YES;
    cell.backgroundColor=kViewColor;
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{


    if (indexPath.row==0) {
        if (self.salonData.salon_description.length==0) {
            return 60;
        }
        if (self.showMoreBiography) {
            return 250;
        }
        return 170;
    }
    else if(indexPath.row==1){
        if (!self.hasCategories) {
            return 0;
        }
        return 80;
    }
    else if (indexPath.row==2){
        return 90;
    }
    else if(indexPath.row==3){

        if ((int)self.salonData.reviews>1) {

            return 220;
        }

        return 180;


    }
    return self.tableView.rowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{


    if (indexPath.row==0) {
        if (self.salonData.salon_description.length!=0) {

            if (self.showMoreBiography) {

                self.showMoreBiography=NO;
            } else{

                self.showMoreBiography=YES;
            }


            NSArray * rowArray = [[NSArray alloc] initWithObjects:indexPath, nil];
            [tableView reloadRowsAtIndexPaths:rowArray withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
    if (indexPath.row==1) {

        //only do call to book if is tier 1
        if ((int)self.salonData.salon_tier==1) {
            [self book];
        }



    }
    if (indexPath.row==2) {

        self.isDirectsionsTransition=YES;
        GetDirectionsViewController *destinationViewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
        destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
        destinationViewController.transitioningDelegate = self;
        destinationViewController.salonMapData = self.salonData;
        [self.scrollTimer invalidate];
        [self presentViewController:destinationViewController animated:YES completion:nil];

    }

}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{

    if (self.showReviews || self.showCategoryList || self.showSubmitReviewVC) {

        self.bookButton.hidden=YES;

        return [[PresentDetailTransition alloc] init];

    } else if (self.isSlideShowTranition) {

        self.favButton.alpha=0.0;
        self.noOfPagesLabel.alpha=0.0;

        if ([WSCore isDummyMode]) {
            return [[GCImagePresentTransition alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.scrollView.frame.size.height) AndImage:self.salonImages[self.currentPage]];
        }
        return [[GCImagePresentTransition alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.scrollView.frame.size.height) AndImagePath:self.salonImages[self.currentPage]];
    }
    else if(self.isDirectsionsTransition){

        return [[GCPopInTransition alloc] init];

    }


    return nil;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{

    if (self.showReviews || self.showCategoryList || self.showSubmitReviewVC) {

        self.bookButton.hidden=NO;
        self.showReviews=NO;
        self.showCategoryList=NO;
        self.showSubmitReviewVC=NO;

        return [[DismissDetailTransition alloc] init];

    } else if (self.isSlideShowTranition) {

        self.isSlideShowTranition=NO;

        [UIView animateWithDuration:0.4f delay:0.6 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.favButton.alpha=1.0f;
            self.noOfPagesLabel.alpha=1.0f;
        } completion:nil];

        if ([WSCore isDummyMode]) {
            return [[GCImageDismissTransition alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.scrollView.frame.size.height) AndImage:self.salonImages[self.currentPage]];
        }

        return [[GCImageDismissTransition alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.scrollView.frame.size.height) AndImagePath:self.salonImages[self.currentPage]];

    }
    else if(self.isDirectsionsTransition){

        self.isDirectsionsTransition=NO;

        //sets the size for the transitions
        CGRect cellRect = [self.tableView rectForFooterInSection:0];
        cellRect = CGRectOffset(cellRect, -self.tableView.contentOffset.x, -self.tableView.contentOffset.y+ cellRect.size.height);

        return [[DismissDetailTransition alloc] init];

    }

    return nil;



}




#pragma mark - GCNetwork Delegate
-(void)manager:(GCNetworkManager *)manager handleServerFailureWithError:(NSError *)error{

    if (!self.callManager) {

        [WSCore showServerErrorAlert];

    }
    else if(self.callManager){

        [self callSalon];

    }


}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{

    if (manager==self.callManager) {
        //NSLog(@"\n\n **** Success %@ ***** \n\n",jsonDictionary);

        [self callSalon];
    } else if (manager == self.favNetworkManager) {
        self.salonData.is_favourite = YES;
        [self navigationLikeButton];
    }
}
- (void)callSalon {

    [self removePhonePlaceholder];

    if (self.salonData.salon_landline>0) {

        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
            NSString *telno = [NSString stringWithFormat:@"tel://%@",self.salonData.salon_landline];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telno]];
        }
    }
    else{
        [UIView showSimpleAlertWithTitle:@"No number provided by Salon" message:@"" cancelButtonTitle:@"OK"];
    }

}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{

    if (manager==self.callManager) {
        //NSLog(@"\n\n **** Fail %@ ***** \n\n",jsonDictionary);
        [self callSalon];
    }
}


/*

 RemovePhonePlaceholder
 remove phonePlaceholderForLoading from superview
 enable the bookbutton after it was originaly disabled
 cancel networkTask


 */
-(void)removePhonePlaceholder{
    [self.phonePlaceholderForLoading removeFromSuperview];
    self.bookButton.enabled=YES;
    [self.callManager cancelTask];
}

/*
 createPhoneholderForLoading
 creates phonePlaceholderForLoading
 sets background
 adds to subview
 brings subview to front inorder to cover other assets

 Creates a label containing some details
 sets text color to white

 creates cancel button
 adds actions

 Adds activity indicator
 start animatiing

 */
- (void)createPhoneholderForLoading {

    self.phonePlaceholderForLoading = [[UIView alloc] initWithFrame:self.view.frame];
    ;
    self.phonePlaceholderForLoading.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.6];
    [self.view addSubview:self.phonePlaceholderForLoading];
    [self.view bringSubviewToFront:self.phonePlaceholderForLoading];

    UILabel * fetchingNumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    fetchingNumber.text = @"Fetching Details. Please Wait ...";
    fetchingNumber.textColor = [UIColor cloudsColor];
    fetchingNumber.textAlignment=NSTextAlignmentCenter;
    [self.phonePlaceholderForLoading addSubview:fetchingNumber];
    fetchingNumber.center = CGPointMake(fetchingNumber.center.x, self.view.center.y);

    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [cancelButton setImage:kCancelButtonImage forState:UIControlStateNormal];
    UIImage *cancelButtonImage = [kCancelButtonImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cancelButton setImage:cancelButtonImage forState:UIControlStateNormal];
    [cancelButton setTintColor:[UIColor cloudsColor]];
    [cancelButton addTarget:self action:@selector(removePhonePlaceholder) forControlEvents:UIControlEventTouchUpInside];
    [self.phonePlaceholderForLoading addSubview:cancelButton];

    cancelButton.center = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height-40);


    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.color = [UIColor cloudsColor];
    activityIndicator.center = CGPointMake(self.view.center.x, self.view.center.y-40);
    [self.phonePlaceholderForLoading addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

/*
 makeCallToSalon
 call createPhoneholderForLoading

 create params for url
 calls callManager loadWithURL
 sets loading dialog to No because we dont want a spinner on view


 */
- (void)makeCallToSalon {

    [self createPhoneholderForLoading];

    NSString * params = [NSString stringWithFormat:@"salon_id=%@",self.salonData.salon_id];
    if ([[User getInstance] isUserLoggedIn]) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken]]];
    }
    // NSLog(@"params %@",params);
    [self.callManager loadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kTier3_Call_URL]] withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];

}

/*
 recordCallImpression
 shows FUIAlertView
 if positive action
 make call to salon

 */
-(void)recordCallImpression{


    FUIAlertView *alertView = [[FUIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Call %@?",self.salonData.salon_name]
                                                          message:[NSString stringWithFormat:@"You are about to call %@. Do you wish to continue?",self.salonData.salon_name]
                                                         delegate:nil cancelButtonTitle:@"Cancel"
                                                otherButtonTitles: @"Continue",nil];
    alertView.alertViewStyle=FUIAlertViewStyleDefault;
    alertView.titleLabel.textColor = [UIColor cloudsColor];
    //alertView.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    alertView.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    alertView.messageLabel.textColor = [UIColor cloudsColor];
    alertView.messageLabel.font = [UIFont systemFontOfSize:14.0];
    //alertView.messageLabel.font = [UIFont flatFontOfSize:14];
    alertView.backgroundOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    alertView.alertContainer.backgroundColor = kWhatSalonBlue;
    alertView.defaultButtonColor = [UIColor cloudsColor];
    alertView.defaultButtonShadowColor = [UIColor asbestosColor];
    alertView.defaultButtonShadowHeight=kShadowHeight;
    //alertView.defaultButtonFont = [UIFont boldFlatFontOfSize:16];
    alertView.defaultButtonTitleColor = [UIColor asbestosColor];
    alertView.onOkAction=^{

        self.bookButton.enabled=NO;


        [self makeCallToSalon];

    };

    [alertView show];




}

/*
 markAsFavorite
 if user isnt logged in then return
 else
 if favourite
 make network request to unfavourite / disable fav button
 else
 make network request / disable fav button

 */
-(void)markAsFavourite{

    if (![[User getInstance] isUserLoggedIn]) {
        [WSCore showLoginSignUpAlert];
        return;
    }

    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonData.salon_id]];
    [self.favNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];

    [self.tabBarController setSelectedIndex:1];

    //MARK: - TODO
    //show fav if added to fav list else reload cell
}


#pragma mark - SlideShowDelegate

-(void)getLastPage:(NSInteger)lastPage{

    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    // Start point - Set initial content offset of scrollview
    self.scrollView.contentOffset = CGPointMake(pagesScrollViewSize.width * lastPage, self.scrollView.contentOffset.y);
}

#pragma mark - SalonReviewDelegate
-(void)didSelectRowToShowAll{
    //[UIView showSimpleAlertWithTitle:@"SHOW LIST" message:@"" cancelButtonTitle:@"OK"];
    self.showReviews=YES;
    SalonReviewListViewController * salonReview = [self.storyboard instantiateViewControllerWithIdentifier:@"reviewList"];
    salonReview.salonData=self.salonData;
    salonReview.transitioningDelegate=self;
    salonReview.modalPresentationStyle = UIModalPresentationCustom;
    //salonReview.isList=YES;
    [self presentViewController:salonReview animated:YES completion:nil];


}

#pragma mark - UNWIND Segue
- (IBAction)unwindToSalonIndividualFromLastMin:(UIStoryboardSegue *)unwindSegue
{
    [self.scrollTimer invalidate];
    self.scrollTimer=nil;
    [self performSegueWithIdentifier:@"unwindFromSalonIndividualToLastMinute" sender:self];

}

/*
 trying to solve below crash:
 -[UIScrollView(UIScrollViewInternal) _delegateScrollViewAnimationEnded] + 52
 setting delegate to nil
 */
-(void)dealloc{
    self.scrollView.delegate=nil;
}


-(void)didSelectAddYourReview{

    self.showSubmitReviewVC=YES;
    SubmitReviewSalonDetailViewController * submitVC = [self.storyboard instantiateViewControllerWithIdentifier:@"submitReviewVC"];
    submitVC.delegate =self;
    submitVC.transitioningDelegate=self;
    submitVC.modalPresentationStyle = UIModalPresentationCustom;
    submitVC.salonData = self.salonData;
    [self presentViewController:submitVC animated:YES completion:nil];
}

#pragma ReviewDelegate
-(void)reviewsDelegate:(SubmitReviewSalonDetailViewController *)submitReviewSalonViewController didSubmitReviewWithSalonData:(Salon *)salon{

    if (salon!=nil) {

        self.salonData = salon;
        self.noOfReviews.text = [NSString stringWithFormat:@"%f reviews",salon.reviews];

        [self updateNumberOfStars];

        //update recent review cell


        // update salon array on browse screen
        if ([self.myDelegate respondsToSelector:@selector(refreshSalonObjectsWithUpdatedSalonObject:)]) {
            [self.myDelegate refreshSalonObjectsWithUpdatedSalonObject:salon];
        }

        [self.tableView reloadData];
    }

}
@end

