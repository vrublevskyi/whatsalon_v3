//
//  LastMinuteLocationViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 13/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "LastMinuteLocationViewController.h"
#import "GCNetworkManager.h"
#import "LastMinuteLocationItem.h"


@interface LastMinuteLocationViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,GCNetworkManagerDelegate>
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic) NSTimer * myTimer;
@property (nonatomic) BOOL isFiltered;

@property (nonatomic) NSMutableArray * locationItems;
@property (weak, nonatomic) IBOutlet UIView *searchBarHolder;

@property (nonatomic) GCNetworkManager * manager;

@end

@implementation LastMinuteLocationViewController

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [WSCore statusBarColor:StatusBarColorBlack];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [WSCore statusBarColor:StatusBarColorWhite];
}
- (void)updateNavBarAttributes {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor whiteColor],NSForegroundColorAttributeName,
                                nil];
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    self.title=@"Change Location";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    self.searchBar.delegate=self;
    
    self.manager = [[GCNetworkManager alloc] init];
    self.manager.delegate=self;
    self.manager.parentView = self.view;
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView=footerView;
    
    [self setUpSearchBar:self.searchBar];
    self.view.backgroundColor=kBackgroundColor;
    [WSCore transparentNavigationBarOnView:self];
    
    //update the nav bar attributes e.g title and button
    [self updateNavBarAttributes];
    
    [self setUpLocationItemsArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSourceMethod
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([self.delegate respondsToSelector:@selector(didPickerLocation:)]) {
      
        [ self.delegate didPickerLocation:[self.locationItems objectAtIndex:indexPath.row ]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.locationItems.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    LastMinuteLocationItem * loc = [self.locationItems objectAtIndex:indexPath.row];
    
    //change color of cell if its 'Current Location'
    if (loc.isCurrentLocation && indexPath.row==0) {
        
        
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 20, 20)];
        imgView.backgroundColor=[UIColor clearColor];
        [imgView.layer setCornerRadius:8.0f];
        [imgView.layer setMasksToBounds:YES];
        [imgView setImage:[UIImage imageNamed:@"location"]];
        imgView.image = [imgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [imgView setTintColor:kWhatSalonBlue];
        [cell.contentView addSubview:imgView];
        imgView.center=CGPointMake(imgView.center.x, cell
                                   .center.y);
        cell.imageView.image = [UIImage new];
        cell.textLabel.textColor=kWhatSalonBlue;
        cell.textLabel.text = [NSString stringWithFormat:@"%@",loc.salon_address3];
        
        
        
    }//standard cell
    else{
        
        cell.imageView.image=nil;
        cell.textLabel.textColor=[UIColor blackColor];
        cell.textLabel.text = [NSString stringWithFormat:@"%@, %@",loc.salon_address3,loc.county_name];
    }
    
    return cell;
}

#pragma mark UISearchBarDelegate
#pragma mark - UISearchBar delegate and datasource methods


-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    return YES;
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    //if text is 0 or less than 3
    //reset boolean, timer and activity indicator
    if (searchBar.text ==0 ||searchBar.text.length<3) {
        
        self.isFiltered=NO;
        if (self.myTimer)
        {
            if ([self.myTimer isValid])
            {
                [self.myTimer invalidate];
            }
            self.myTimer=nil;
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
        }
        
    }
    //if text is greater than or equal to 3
    //set up timer
    //start activity indicator
    //set interval
    else if (searchBar.text.length>=3) {
        NSLog(@"Text Did Change & perfom search");
        
        
        
        if (self.myTimer)
        {
            if ([self.myTimer isValid])
            {
                //[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [self.myTimer invalidate];
            }
            self.myTimer=nil;
        }
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(performSearchWithSearchString:) userInfo:searchText repeats:NO];
        
    }
}

//perform the search with the text
-(void)performSearchWithSearchString: (NSString *) text{
    //if the timer is valid
    //assign the user info from timer to the text
    if (self.myTimer) {
        if ([self.myTimer isValid])
        {
            text = self.myTimer.userInfo;
        }
        
    }
    
    //else the text is that of the search bar
    else{
        text=self.searchBar.text;
    }
    
    
        
    [self searchForAddress:text];
        
    
}

//perform search using text
- (void)searchForAddress:(NSString *)text{
    
    NSLog(@"Search for address");
    [self.manager loadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Salons_By_Location_URL]] withParams:[NSString stringWithFormat:@"location=%@",text] WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
    
}

//hide keyboard if search bar button is clicked
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.view endEditing:YES];

}


#pragma mark - GCNetwork Delegate
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
     NSLog(@"Failure %@ \n",jsonDictionary);
}

//set up the locationItems array
//initialise to 0
//create users objet if it hasUsersLocation is true
//add current object to array if available

- (void)setUpLocationItemsArray {
    
    
    self.locationItems = [NSMutableArray array];
    
    if (self.hasUsersLocation) {
        if (CLLocationCoordinate2DIsValid(self.usersLocation.coordinate)) {
            NSLog(@"Coordinate valid");
            LastMinuteLocationItem *currentLocation = [[LastMinuteLocationItem alloc] init];
            currentLocation.salon_lat=[NSString stringWithFormat:@"%f",self.usersLocation.coordinate.latitude];
            currentLocation.salon_lon=[NSString stringWithFormat:@"%f",self.usersLocation.coordinate.longitude];
            currentLocation.salon_address3=@"Current Location";
            currentLocation.isCurrentLocation=YES;
            [self.locationItems addObject:currentLocation];
            
            
        } else {
           //coordinate is not valid
        }
        
    }
    else{
       //no users location
    }
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    [self setUpLocationItemsArray];
    
    
    for (id key in jsonDictionary[@"message"]) {
        
        LastMinuteLocationItem * locItem = [[LastMinuteLocationItem alloc]init];
        
        if (key[@"city_lat"]!=[NSNull null]) {
            locItem.city_lat = key[@"city_lat"];
        }
        if (key[@"city_lon"]!=[NSNull null]) {
            locItem.city_lon = key[@"city_lon"];
            
        }
        if (key[@"city_name"]!=[NSNull null]) {
            locItem.city_name = key[@"city_name"];
            
        }
        if (key[@"country_name"]!=[NSNull null]) {
            locItem.country_name = key[@"country_name"];
            
        }
        if (key[@"county_id"]!=[NSNull null]) {
            locItem.county_id = key[@"county_id"];
            
        }
        if (key[@"county_latitude"]!=[NSNull null]) {
            locItem.county_latitude = key[@"county_latitude"];
            
        }
        if (key[@"county_longitude"]!=[NSNull null]) {
            locItem.county_longitude = key[@"county_longitude"];
            
        }
        if (key[@"county_name"]!=[NSNull null]) {
            locItem.county_name = key[@"county_name"];
            
        }
        if (key[@"google_success"]!=[NSNull null]) {
            locItem.google_success = [key[@"google_success"] boolValue];
            
        }
        if (key[@"salon_address_1"]!=[NSNull null]) {
            locItem.salon_address1 = key[@"salon_address_1"];
            
        }
        if (key[@"salon_address_2"]!=[NSNull null]) {
            locItem.salon_address2 = key[@"salon_address_2"];
            
        }
        if (key[@"salon_address_3"]!=[NSNull null]) {
            locItem.salon_address3 = key[@"salon_address_3"];
            
        }
        if (key[@"salon_lat"]!=[NSNull null]) {
            locItem.salon_lat = key[@"salon_lat"];
            
        }
        if (key[@"salon_lon"]!=[NSNull null]) {
            locItem.salon_lon = key[@"salon_lon"];
            
        }
        
       // if (locItem.google_success==YES) {
            [self.locationItems addObject:locItem];
       // }
    }
    
   
    [self.tableView reloadData];
    NSLog(@"Success %@ \n",jsonDictionary);
}

//search bar set up
-(void)setUpSearchBar:(UISearchBar *)searchBar{
    

    searchBar.backgroundColor=[UIColor clearColor];
    
    // set Search Bar texfield corder radius
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    txfSearchField.layer.cornerRadius = 3.0f;
    searchBar.backgroundImage=[UIImage new];
    [self.searchBarHolder bringSubviewToFront:searchBar];
    
    //add gray line to bottome of searchbarholder
    UIView * grayLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.searchBarHolder.frame.size.height-0.5, self.searchBarHolder.frame.size.width, 0.5)];
    grayLine.backgroundColor=[UIColor lightGrayColor];
    [self.searchBarHolder addSubview:grayLine];
    [self.searchBarHolder bringSubviewToFront:grayLine];
    
    self.searchBarHolder.backgroundColor=kWhatSalonBlue;
    
        
}


@end
