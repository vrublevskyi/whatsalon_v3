//
//  TabCongratulationsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"
#import "Booking.h"

@interface TabCongratulationsViewController : UIViewController

@property (nonatomic) double chargedPrice;

/*! @brief represents the remaining price. */
@property (nonatomic) double remainingPrice;

/*! @brief represents the salon object. */
@property (nonatomic, strong) Salon * salonData;

/*! @brief represents the booking object that contains the booking details. */
@property (nonatomic, strong) Booking * bookingObj;

/*! @brief represents the date of the appointment. */
@property (nonatomic, strong) NSDate * chosenDate;


@end
