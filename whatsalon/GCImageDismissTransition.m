//
//  GCImageDismissTransition.m
//  transition
//
//  Created by Graham Connolly on 18/02/2015.
//  Copyright (c) graham connolly All rights reserved.
//

#import "GCImageDismissTransition.h"
#import "UIImageViewModeScaleAspect.h"
#import <SDWebImage/UIImageView+WebCache.h>


@implementation GCImageDismissTransition 


-(instancetype)initWithFrame: (CGRect) frame AndImagePath: (NSString *) imagePath{
    self = [super init];
    if (self) {
        self.endImageFrame =frame;
        self.imageUrl=imagePath;
    }
    return self;
}

-(instancetype)initWithFrame: (CGRect) frame AndImage: (UIImage *) image{
    
    self = [super init];
    if (self) {
        self.endImageFrame =frame;
        self.dummyImage=image;
    }
    return self;
}
-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
   
    UIViewController * detail = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    NSArray *viewsToRemove = [detail.view subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    detail.view.backgroundColor=[UIColor clearColor];
    
    UIImageViewModeScaleAspect *myImage = [[UIImageViewModeScaleAspect alloc]initWithFrame:detail.view.frame];
    myImage.contentMode = UIViewContentModeScaleAspectFit; // Add the first contentMode
    UIImageView * image = [[UIImageView alloc] init];
    if ([WSCore isDummyMode]) {
        image.image = self.dummyImage;
    }else{
        [image sd_setImageWithURL:[NSURL URLWithString: self.imageUrl ]];
    }
   
    myImage.image = image.image;
    [myImage initToScaleAspectFillToFrame:self.endImageFrame];
    [detail.view addSubview:myImage];
    
    [UIView animateWithDuration:0.4f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         
                         [myImage animaticToScaleAspectFill];
                         
                        
                     } completion:^(BOOL finished) {
                         [myImage animateFinishToScaleAspectFill];
                         
                         
                         [detail.view removeFromSuperview];
                         [transitionContext completeTransition:YES];
                     }];
    
    

}

-(NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    return 0.4;
}
@end
