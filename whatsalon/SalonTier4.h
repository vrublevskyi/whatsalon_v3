//
//  SalonTier4.h
//  whatsalon
//
//  Created by Graham Connolly on 15/09/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SalonTier4 : NSObject

/*! @brief represents the country code. */
@property (nonatomic) NSString * countryCode;

/*! @brief represents the distance. */
@property (nonatomic) NSString * distance;

/*! @brief represents the url to the image. */
@property (nonatomic) NSString * imageUrlLink;

/*! @brief represents the location latitude. */
@property (nonatomic) NSString * locationLat;

/*! @brief represents the location longitude. */
@property (nonatomic) NSString * locationLon;

/*! @brief represents the post code of the salon. */
@property (nonatomic) NSString * postCode;

/*! @brief represents the address of the salon. */
@property (nonatomic) NSString * salonAddress;

/*! @brief represents the name of the salon. */
@property (nonatomic) NSString * salonName;

/*! @brief represents the country of the salon. */
@property (nonatomic) NSString * salonCounty;

/*! @brief represents the phone number of the salon. */
@property (nonatomic) NSString * salonPhone;

/*! @brief creates an instancetype of SalonTier4
 
    @param salon_name - represents the name of the salon.
 
    @return instancetype
 
 */
-(instancetype) initWithSalonName: (NSString *) salon_name;

/*! @brief creates an instancetype of the SalonTier4.
 
    @param salonName - represents the name of the salon.
 
    @return instancetype
 
 */
+ (instancetype) salonWithSalonName: (NSString *) salonName;


/*! @brief returns the full of the salon.
    @return NSString */

-(NSString *)getFullAddress;
@end
