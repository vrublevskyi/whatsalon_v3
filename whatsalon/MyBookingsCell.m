//
//  MyBookingsCell.m
//  whatsalon
//
//  Created by admin on 10/2/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import "MyBookingsCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MyBookingsCell()
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *servicePriceLabel;

@end

@interface MyBookingsCell ()

@end

@implementation MyBookingsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setBooking:(MyBookings *)booking {
    _booking = booking;
    [self.topImageView sd_setImageWithURL:[NSURL URLWithString:booking.bookingSalon.salon_image]];
    self.serviceNameLabel.text = booking.servceName;
    self.serviceTimeLabel.text = booking.bookingStartTime;
    self.servicePriceLabel.text = [NSString stringWithFormat:@"$%@", booking.bookingTotal];
    self.serviceAddressLabel.text = [NSString stringWithFormat:@"with %@ in %@", booking.stylistName, booking.bookingSalon.salon_address_1];
}

@end
