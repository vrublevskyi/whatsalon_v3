//
//  FriendlyService.h
//  whatsalon
//
//  Created by Graham Connolly on 26/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header FriendlyService.h
  
 @brief This is the header file for FriendlyServices.h
  
 @remark represents WhatSalons friendly category list. Was used in LatMinuteSearchViewController.h
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    -
 */
#import <Foundation/Foundation.h>

@interface FriendlyService : NSObject

/*! @brief represents the friendly category. */
@property (nonatomic) NSString * friendly_category;

/*! @brief represents the friendly name. */
@property (nonatomic) NSString * friendly_name;

/*! @brief represents the friendly service id. */
@property (nonatomic) NSString * friendly_service_id;

/*! @brief represents the friendly category title. */
@property (nonatomic) NSString * friendly_category_title;
@end
