//
//  constants.h
//  whatsalon
//
//  Created by Graham Connolly on 25/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "WSCore.h"
#ifndef whatsalon_constants_h
#define whatsalon_constants_h

#define kVersionNumber [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

//#define CURRENCY_SYMBOL [[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol]
#define CURRENCY_SYMBOL [[User getInstance] fetchCurrencySymbol ]
#define StringFromBOOL(b) ((b) ? @"YES" : @"NO")

#define kGoogle_Geocode_API_Key @"AIzaSyD0RrghF_SmwrHUfHZ_EcTBNrvpMCQs0Nk"
#define kButtonHeight 44
#define CellHeight 230

#define kAccessTokenKey @"accessToken"

#define kImageDataKey @"imageData"
#define kGenderKey @"gender"
#define kEmailKey @"user_email"
#define kSurnameKey @"surname"
#define kFirstNameKey @"user_name"
#define kMale @"male"
#define kFemale @"female"
#define kPhoneKey @"phone"
#define kUserBalanceKey @"user_balance"
#define kCategories @"categories"
#define kLastLong @"long"
#define kLastLat @"lat"
#define kCreditCardFirstNameKey @"card_first_name"
#define kCreditCardLastNameKey @"card_last_name"
#define kCreditCardNumberKey @"card_number"
#define kCreditCardTypeKey @"card_type"
#define kCreditCardExpKey @"card_expiry"
#define kCreditCardPaymentRef @"card_payment_ref"
#define kCatDictionaryKey @"catDictionary"
#define kStyleListKey @"styleList"
#define kShouldShowPreviousBookings @"showPrevious"
#define IS_IOS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_IOS_9_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define kTier1SalonIcon @"blueSalonImage"
#define kTier2SalonIcon @"salon_green"
#define kTier3SalonIcon @"salon_red"
#define kCloseSalonIcon @"closed_salon"
#define kImageURLKey @"imageURL"

#define kOKStatusCode 200

#define kProfile_image @"no_avatar"
#define kDefault_menu @"blue_menu_default"
#define kMenu_wallpaper @"wallpaper2"


#define kRefreshMenuNotification @"refreshMenu"

#define kIsLiveEnvironment [WSCore isLiveEnvironment]
#define kTestAPI_URL @"https://api.whatsalon.com/"

#define kSuccessIndex @"success"
#define kMessageIndex @"message"

#define kFAQ_URL @"faqs"
#define kContent_URL @"get_pages_content"
#define kContent_pageID_cancelPolicy @"17"
#define kContent_pageID_WhatCities @"68"
#define kSearchRadius @"60"
#define kWorldCircumference @"20000"

/*!
 @brief tab notifications that are triggered to refresh their views
 */
//refresh tab screens- notification names
#define kTab_0_Notification @"tab_0_notification"
#define kTab_1_Notification @"tab_1_notification"
#define kTab_2_Notification @"tab_2_notification"
#define kTab_3_Notification @"tab_3_notification"

/*!
 @brief a list of URLS being used in the app
 */
#define kLogin_URL @"login_user"
#define kUpdate_User_URL @"update_user"
#define kChange_Password_URL @"change_password"
#define kGet_User_Balance_URL @"get_user_balance"
#define kRedeem_Code_URL @"redeem_code"
#define kSalon_Categories_URL @"categories"
#define kValidate_Twilio_URL @"validate_twilio"
#define kResend_Twilio_URL @"reset_validation_code"
#define kSearch_Salons_URL @"search_salons"
#define kMark_favourite_URL @"mark_favourite"
#define kSalon_image_gallery_URL @"salon_image_gallery"
#define kPayment_add_card_URL @"payment/add_card"
#define kPayment_delete_card_URL @"payment/delete_card"
#define kPayment_authorize_card_URL @"payment/auth"
#define kPayment_check_user_needs_card_URL @"payment/check_need_card"
#define kResetPassword_URL @"reset_password"
#define kBook_now_Tier2_URL @"book_now"
#define kBooking_answers_Tier2_URL @"booking_answers"
#define kRegister_User_v2_URL @"register_user_v2"
#define kInspire_User_URL @"inspire_me"
#define kMark_Style_Favourite_URL @"mark_style_favourite"
#define kStyle_Category_URL @"style_category_list"
#define kCancel_Tier2_Search_URL @"cancel_search"
#define kPayment_Auth_URL @"payment/auth"
#define kBooking_History_URL @"user_appointments/history"
#define kRating_URL @"rating"
#define kUpload_to_inspire_me_URL @"upload_to_inspire_me"
#define kCountdown_timer_URL @"seconds_to_launch_date"
#define kUse_Invite_URL @"use_invitation_code"
#define kDelete_Booking_URL @"delete_previous_booking"
#define kGet_Phorest_services_List_URL @"get_phorest_services"
#define kGet_Phorest_Categories_URL @"get_phorest_categories"
#define kGet_APP_version_URL @"get_app_version"
#define kGet_Phorest_Staff_URL @"get_phorest_staff"
#define kGet_Phorest_Service_Available_Times @"phorest_service_available_times"
#define kBook_Phorest_Service_URL @"book_phorest_service"
#define kPay_Phorest_Booking_URL @"payment/pay_phorest_booking"
#define kPhorest_Staff_URL @"get_phorest_staff"
#define kFeed_Back_URL @"receive_feedback"
#define kHas_Show_Twilio @"has_shown_twilio"
#define kIs_Twilio_verified @"is_twilio_verfied"
#define kTwilio_Reg_URL @"initiate_twilio"
#define kSalon_Reviews_URL @"salon_reviews"
#define kUser_Inbox_URL @"user_inbox"
#define kValidate_Secure_Twilio_URL @"validate_twilio_secure"
#define kGet_Services_Names_URL @"get_names_categories"
#define kLast_Minute_URL @"last_minute"
#define kGet_Experiences_URL @"get_experiences"
#define kCancel_Appointment_URL @"cancel_appointment"
#define kGet_Salons_By_Location_URL @"get_salons_by_location"
#define kGet_Names_Categories_Location_URL @"get_names_categories_location"
#define kGet_Discovery_Objects_URL @"get_discovery_objects"
#define kFavourite_Experience_URL @"favourite_experience"
#define kTier3_Impressions_URL @"tier3_impression"
#define kTier3_Call_URL @"tier3_call"
#define kGet_Profile_Status_URL @"profile_status"
#define kSearch_Tier_4_URL @"search_tier4"
#define kGet_Promo_Discounted_Price_URL @"get_discounted_price"
#define kPay_For_Booking_URL @"pay_service_v2"
#define kBook_Isalon_Service_URL @"book_isalon_service"


/*!
 @brief image constants
 
 */
#define kNo_Avatar_Image_String @"no_photo_avatar"
#define kPlace_Holder_Cell_Image @"empty_cell"
#define kHeart_Icon_Image @"rounded_heart_icon"
#define kPlace_Holder_Image [UIImage coloredImage_resizeableImageWithColor:[UIColor silverColor]]

//overlays
#define kIntructionalOverlay_LastMinute @"LastMinuteOverlay"

#define kWhatSalonBlue [UIColor colorWithRed:0.0/255.0f green:168.0/255.0f blue:233/255.0f alpha:1.0]
#define kYelowStar [UIColor colorWithRed:193.0/255.0 green:150.0/255.0 blue:38/255.0f alpha:1.0]
#define kCongratsWordsColor [UIColor colorWithRed:159.0/255.0 green:90.0/255.0 blue:165/255.0f alpha:1.0]

#define kGoldenTintForOverView [UIColor colorWithRed:51.0/255.0 green:34.0/255.0 blue:0/255.0f alpha:0.3]
#define kWhatSalonBlueShadow [UIColor belizeHoleColor]
#define kBackgroundColor [UIColor cloudsColor]
#define kViewColor [UIColor whiteColor]
#define kWhatSalonSubTextColor [UIColor lightGrayColor]
#define kCancelButtonImage [UIImage imageNamed:@"icon_x_cancel"]
#define kStatusBarHeight [UIApplication sharedApplication].statusBarFrame.size.height

#define kShadowHeight 1.0
#define kCornerRadius 3.0
#define kNavBarAndStatusBarHeight self.navigationController.navigationBar.frame.size.height+ [[UIApplication sharedApplication] statusBarFrame].size.height

//device sizes
#define IS_iPHONE5 ( [WSCore screenSize].height == 568 )
#define IS_iPHONE5_or_Above ( [WSCore screenSize].height >= 568 )
#define IS_iPHONE4 ( [WSCore screenSize].height == 480 )


#define kWhatSalonWebsite @"https://www.whatsalon.com"


#define kKeyBoardHeight 216
#define kUserReviewsCellHeight 180

//colours
#define kFacebookBlue [UIColor colorWithRed:59.0/255.0 green:89.0/255.0 blue:152.0/255.0 alpha:1.0]
#define kTwitterBlue [UIColor colorWithRed:85.0/255.0 green:172.0/255.0 blue:238.0/255.0 alpha:1.0]
#define kWhatsAppDarkGreen [UIColor colorWithRed:52.0/255.0 green:175.0/255.0 blue:35.0/255.0 alpha:1.0]
#define kWhatsAppLightGreen [UIColor colorWithRed:100.0/255.0 green:212.0/255.0 blue:72.0/255.0 alpha:1.0]


#define kDefaultTintColor [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]
#define kGroupedTableGray [UIColor silverColor] 
#define kCellLinesColour [UIColor silverColor]
#define kPtsToPxlsHeight ([[UIScreen mainScreen ] bounds].size.height * 2)
#define kPtsToPxlsWidth ([[UIScreen mainScreen ] bounds].size.width * 2)

#define DiscoveryCellHeight ([WSCore screenSize].height - 64)/1.65f
#define DiscoveryCellHeight2 ([WSCore screenSize].height - 64)/2.5f
#define DiscoveryCellHeight3 ([WSCore screenSize].height - 64)/2.2f


#define ACCEPTABLE_EXPIRY_DATE_CHARECTERS @"1234567890"


//placeholders
#define kMy_Bookings_placeholder_Image_Name @"my_bookings_placeholder.jpg"
#define kSearch_placeholder_Image_Name @"search_placeholder.png"
#define kMy_Favourites_placeholder_Image_Name @"my_favourites_placeholder"
#define kNo_Location_Placeholder_Image_Name @"my_bookings_placeholder.jpg"



//constants
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_6_or_above (IS_IPHONE && SCREEN_MAX_LENGTH >= 667.0)

/*!
 @brief tags used in UITableViewCells
 
 @remark can be updated to use custom cells by registering new cells in the Controller, or create custom tableview cells in interface builder.
 */
#define kTwoThirdsCellHeight 380
//menu
#define kMenuCellLabel 100
#define kMenuCellImage 101



//my bookings
#define kImageV 201
#define kServiceLabel 202
#define kByWhoLabel 203
#define kRatingImg 204
#define kDateLabel 205

//Salon Review
#define kSalonStarRating 206
#define kStyleStarRating 207

//inspire me main view
#define kInspireImageV 301
#define kInspireSalon 302


//my fav inspirations
#define kFavInspireImageV 401
#define kFavInspireSalon 402

//Salon Detail
#define kCollectionView 601
#define kTimeLabel 603
#define kPriceLabel 604

//salon done (trace screen)
#define kSalonNameCell 701
#define kAddressCell 702
#define kServiceLabelCell 703
#define kStylistLabelCell 704
#define kDateLabelCell 705

#define kNavTitleCell 707
#define kFullPriceCell 708
#define kAtSalonLabel 709
#define kDiscountLabel 710

//reviews table
#define kReviewServiceName 801
#define kReviewDate 802
#define kReviewUser 803
#define kStarRating 804
#define kAddPhoto 805
#define kShowYourLookText 806
#endif
