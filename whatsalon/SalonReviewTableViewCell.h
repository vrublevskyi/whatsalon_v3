//
//  SalonReviewTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 18/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header SalonReviewTableViewCell.h
  
 @brief This is the header file for the SalonReviewTableViewCell.h
  
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.5
 */
#import <UIKit/UIKit.h>
#import "SalonReview.h"


/*!
  @protocol SalonReviewDelegate
  
  @brief The SalonReviewDelegate protocol.
  
   This protocol notifies the delegate to perform an action.
 
 */
@protocol SalonReviewDelegate <NSObject>

@optional

/*! @brief an action that is triggered when the show all button is pressed. */
-(void)didSelectRowToShowAll;

/*! @brief an action when the add review action is selected. */
-(void)didSelectAddYourReview;

@end
@interface SalonReviewTableViewCell : UITableViewCell

/*! @brief represents the SalonReviewDelegate object. */
@property (nonatomic) id<SalonReviewDelegate> delegate;

/*! @brief represents the number of reviews label. */
@property (nonatomic) UILabel * numberOfReviewsLabel;

/*! @brief represents the message label. */
@property (nonatomic) UILabel *messageLabel;

/*! @brief represents the reviewers name. */
@property (nonatomic) UILabel *reviewerName;

/*! @brief represents the image rating image. */
@property (nonatomic) UIImageView *imageRatingImage;

/*! @brief represents the date label. */
@property (nonatomic) UILabel *dateLabel;

/*! @brief represent the name label. */
@property (nonatomic) UILabel *nameLabel;

/*! @brief represents the view all button. */
@property (nonatomic) UIButton * viewAllButton;

/*! @brief represents the users image. */
@property (nonatomic) UIImageView * userImage;

/*! @brief represents the SalonReview object. */
@property (nonatomic) SalonReview *salonReview;

/*! @brief determines if the cell should be modified if its appearing with all reviews. */
@property (nonatomic) BOOL listView;

/*! @brief represents the write your review label. */
@property (nonatomic) UILabel * writeYourReviewLabel;

/*! @brief represents the write review image. */
@property (nonatomic) UIImageView * writeReviewImage;

/*! @brief represents the add review button. */
@property (nonatomic) UIButton * addReviewButton;


/*! @brief handles the customisation of the review cell. 
 
    @param review the SalonReview object.
 
    @param numberOfReviews the number of reviews.
 */
-(void)setUpReviewPageWithReview : (SalonReview *) review AndWithNumberOfReviews: (int) numberOfReviews;

@end
