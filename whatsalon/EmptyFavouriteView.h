//
//  EmptyFavouriteView.h
//  whatsalon
//
//  Created by admin on 10/2/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class EmptyFavouriteView;
@protocol EmptyFavouriteViewDelegate <NSObject>
- (void) onStardDiscoveringTapped: (EmptyFavouriteView *) sender;
@end

@interface EmptyFavouriteView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIButton *startDiscoveringButton;
@property (nonatomic, weak) id <EmptyFavouriteViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
