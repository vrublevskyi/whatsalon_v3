//
//  AllSalonsModel.h
//  whatsalon
//
//  Created by Graham Connolly on 27/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Salon.h"
#import "JGProgressHUD.h"

@protocol AllSalonsDelegate <NSObject>
@optional
-(void)reloadSalonDataWithSalons: (NSMutableArray *) salons AndWithPageNumber:(NSInteger) pageNo;


@end
@interface AllSalonsModel : NSObject
@property (nonatomic,assign) id<AllSalonsDelegate> myDelegate;
@property (nonatomic) NSString * loginKey;
@property (nonatomic) NSString * searchTerm;
@property (nonatomic) NSMutableArray * arryResults;
@property (nonatomic) int totalNumberOfPages;
@property (nonatomic) NSDictionary * salonsOnPageDictionary;
@property (nonatomic)  JGProgressHUD *salonHUD;
@property (nonatomic) UIView * parentView;
@property (nonatomic) NSURLSessionDataTask * task;

-(void)load: (NSURL *)url withParams: (NSString *)params AndWithSpinner:(BOOL) spinner;

-(NSMutableArray*)fetchAllSalons;

-(void)removeAllSalonObjectsFromArray;
-(int)fetchTotalNumberOfPages;

-(void)stopNetworkTask;

@end
