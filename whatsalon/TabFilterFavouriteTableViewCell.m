//
//  TabFilterFavouriteTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 18/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabFilterFavouriteTableViewCell.h"

@implementation TabFilterFavouriteTableViewCell

- (void)awakeFromNib {
    // Initialization code
}



-(void)setUpCellWithItem:(FavouriteFilterItem *)item{
    if (self.tintSub==nil) {
        self.tintSub = [[UIView alloc] initWithFrame:self.filterBackgroundImage.frame];
        self.tintSub.backgroundColor = kGoldenTintForOverView;
        [self.filterBackgroundImage addSubview:self.tintSub];
    }
  
    self.filterBackgroundImage.image = [UIImage imageNamed:item.imageName];
    self.noOfFavouritesLabel.text = [NSString stringWithFormat:@"%@ Favourites",item.number];
    self.titleLabel.text = [NSString stringWithFormat:@"Favourite %@",item.type];
    
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont boldSystemFontOfSize:26.0f];
    self.noOfFavouritesLabel.backgroundColor = kWhatSalonBlue;
    self.noOfFavouritesLabel.layer.cornerRadius=kCornerRadius;
    self.noOfFavouritesLabel.layer.masksToBounds=YES;
    
    if (item.fav_id==0) {
        if ([[[User getInstance] fetchStringNumberOfFavouriteSalons] isEqualToString:@"0"]) {
             self.noOfFavouritesLabel.text = @"Start Discovering";
        }
        else{
            
            NSString * label ;
            if ([[[User getInstance] fetchStringNumberOfFavouriteSalons ] isEqualToString:@"1"]) {
                label =[NSString stringWithFormat:@"%@ Favourite",[[User getInstance] fetchStringNumberOfFavouriteSalons ]];
            }
            else{
                label = [NSString stringWithFormat:@"%@ Favourites",[[User getInstance] fetchStringNumberOfFavouriteSalons ]];
                
                }
            self.noOfFavouritesLabel.text = label;
        }
        
      
    }
    else{
        
        if ([[[User getInstance] fetchStringNumberOfFavouriteExperiences] isEqualToString:@"0"]) {
            self.noOfFavouritesLabel.text = @"Start Reading";
        }
        else{
            NSString * label ;
            if ([[[User getInstance] fetchStringNumberOfFavouriteExperiences] isEqualToString:@"1"]) {
                label =[NSString stringWithFormat:@"%@ Favourite",[[User getInstance] fetchStringNumberOfFavouriteExperiences]];
            }
            else{
                label = [NSString stringWithFormat:@"%@ Favourites",[[User getInstance] fetchStringNumberOfFavouriteExperiences]];
                
            }
            self.noOfFavouritesLabel.text = label;

        }
       
        

    }
    
    self.noOfFavouritesLabel.adjustsFontSizeToFitWidth=YES;
    /*
    self.noOfFavouritesLabel.alpha=0.0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.noOfFavouritesLabel.alpha=1.0;
    }];
     */
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  
    [super setSelected:selected animated:animated];
    
    if (selected) {
        self.noOfFavouritesLabel.backgroundColor = kWhatSalonBlue;
        self.tintSub.backgroundColor = kGoldenTintForOverView;
    }
    

}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
   
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted){
        self.noOfFavouritesLabel.backgroundColor = kWhatSalonBlue;
        self.tintSub.backgroundColor = kGoldenTintForOverView;
    }
}
@end
