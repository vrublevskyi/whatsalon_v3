//
//  TwilioVerifiyViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 18/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TwilioVerifiyViewController.h"
#import "UIView+AlertCompatibility.h"
#import "GCNetworkManager.h"
#import "User.h"

@interface TwilioVerifiyViewController()<GCNetworkManagerDelegate,UIPickerViewDataSource,UIPickerViewDelegate>


/*! @brief represents the country codes array. */
@property (nonatomic,strong) NSMutableArray * countryCodes;

/*! @brief represents the an array of countries. */
@property (nonatomic,strong) NSMutableArray * countries;

/*! @brief represents GCNetworkManager for making network request . */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief represents GCNetworkManager for making requests. */
@property (nonatomic) GCNetworkManager * verifyTwilioManager;

/*! @brief determines if the verify text has been sent. */
@property (nonatomic) BOOL verifyTextSent;

/*! @brief represents the areaCodeButton. */
@property (weak, nonatomic) IBOutlet FUIButton *areaCodeButton;

/*! @brief represents the view for hosting the area code. */
@property (nonatomic) UIView * viewForAreaCode;

/*! @brief represents the cover view that covers the other UI Elements when the viewForAreaCode is present. */
@property (nonatomic) UIView * coverView;

/*! @brief represents the UIPickerView for the country codes. */
@property (nonatomic) UIPickerView * countryCodePickerView;

/*! @brief represents the NSDictionary for country codes. */
@property (nonatomic) NSDictionary * countryCodesDictionary;

/*! @brief represents the selected index for remember the index for the selected phone number. */
@property (nonatomic) NSInteger selectedIndex;

/*! @brief represents the phone number. */
@property (nonatomic) NSString *phoneNumber;

/*! @brief action for changing the read code. */
- (IBAction)changeAreaCode:(id)sender;

@end
@implementation TwilioVerifiyViewController

#pragma mark - UIView lifecycle
-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    [WSCore statusBarColor:StatusBarColorBlack];
    
    //initialize the countryCodes and countries array

    self.countryCodes = [[NSMutableArray alloc] initWithObjects:@"+353",@"+44",@"+1", nil];
    self.countries = [[NSMutableArray alloc] initWithObjects:@"Ireland",@"United Kingdom",@"United States", nil];
    self.selectedIndex = [self.countryCodes indexOfObject: [NSString stringWithFormat:@"+%@",[WSCore getUsersCountryCode]]];
    
    //UIElements set up
    //set navigation bar to invisible
    [WSCore setInvisibleNavBar:self.navBar];
    [self.submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    self.submitButton.layer.cornerRadius=3.0;
    self.submitButton.buttonColor = kWhatSalonBlue;
    self.submitButton.shadowColor = kWhatSalonBlueShadow;
    self.submitButton.shadowHeight = 1.0f;
    self.submitButton.cornerRadius = 3.0f;
    [self.submitButton setTintColor:[UIColor whiteColor]];
    
    //set the area code title to the selectedIndex
    [self.areaCodeButton setTitle:self.countryCodes[self.selectedIndex] forState:UIControlStateNormal];
    self.areaCodeButton.buttonColor = [UIColor whiteColor];
    self.areaCodeButton.shadowColor=[UIColor silverColor];
    self.areaCodeButton.shadowHeight=1.0f;
    self.areaCodeButton.cornerRadius=3.0f;
    self.areaCodeButton.layer.cornerRadius=3.0;
    [self.areaCodeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    
    //mobileNumberTextfield
    self.mobileNumberTextField.backgroundColor = [UIColor clearColor];
    self.mobileNumberTextField.edgeInsets = UIEdgeInsetsMake(4.0f, 15.0f, 4.0f, 15.0f);
    self.mobileNumberTextField.textFieldColor = [UIColor whiteColor];
    self.mobileNumberTextField.borderColor = [UIColor clearColor];
    self.mobileNumberTextField.borderWidth = 1.0f;
    self.mobileNumberTextField.cornerRadius = 3.0f;
    self.mobileNumberTextField.placeholder = @"Phone number";
    self.mobileNumberTextField.keyboardType = UIKeyboardTypePhonePad;
    
    //verificationCode textfield
    self.verificationCode.backgroundColor = [UIColor clearColor];
    self.verificationCode.edgeInsets = UIEdgeInsetsMake(4.0f, 15.0f, 4.0f, 15.0f);
    self.verificationCode.textFieldColor = [UIColor whiteColor];
    self.verificationCode.borderColor = [UIColor clearColor];
    self.verificationCode.borderWidth = 1.0f;
    self.verificationCode.cornerRadius = 3.0f;
    self.verificationCode.keyboardType = UIKeyboardTypePhonePad;
    
    
    //network manager
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    //verifyTwilioManager
    self.verifyTwilioManager = [[GCNetworkManager alloc] init];
    self.verifyTwilioManager.delegate =self;
    self.verifyTwilioManager.parentView = self.view;
    
  
    
    //hide a number of elements
    self.verificationCode.alpha=0.0;
    self.verificationCode.hidden=YES;
    self.submitButton.alpha=0.0;
    self.submitButton.hidden=YES;
    
    //init country code picker
    self.countryCodePickerView = [[UIPickerView alloc] init];
    
   
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.networkManager cancelTask];
    [self.verifyTwilioManager cancelTask];
}

#pragma mark - IBActions
- (IBAction)cancel:(id)sender {
    
    
  
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.delegate respondsToSelector:@selector(twilioCanceled)]) {
            
            [self.delegate twilioCanceled];
        }
    }];
}


//if verified then submit
- (IBAction)submit:(id)sender {
    
    if (self.verifyTextSent==YES) {
        
        if (self.verificationCode.text.length==0) {
           [UIView showSimpleAlertWithTitle:@"Oops" message:@"Verification Code cannot be empty." cancelButtonTitle:@"OK"];
        }
        else{
            //verify twilio
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kValidate_Twilio_URL]]
            ;
            NSString * params = [NSString stringWithFormat:@"phone_number=%@%@",self.areaCodeButton.titleLabel.text,self.mobileNumberTextField.text];
            self.phoneNumber = [NSString stringWithFormat:@"%@%@",self.areaCodeButton.titleLabel.text,self.mobileNumberTextField.text];
            params=[params stringByAppendingString:[NSString stringWithFormat:@"&verification_string=%@",self.verificationCode.text]];
            params = [params stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
            
            [self.verifyTwilioManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
        }
    }else{
       
        NSLog(@"Error");
        
    }
}




//initate twilio
- (IBAction)verify:(id)sender {
    
    //mobile number needs to be greater than or less than 6
    if (self.mobileNumberTextField.text.length<=6) {
        
        [UIView showSimpleAlertWithTitle:@"Oops" message:@"Phone number cannot be empty." cancelButtonTitle:@"OK"];
        
    }
    else{
        
        //verify account with twilio
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kTwilio_Reg_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&phone_number=%@%@",self.areaCodeButton.titleLabel.text, self.mobileNumberTextField.text]];
        
        //replace occurents of + with %2B to make it web safe
        params = [params stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
        
    }

}

//change the users area code for mobile number
- (IBAction)changeAreaCode:(id)sender {
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, [WSCore screenSize].height-320, [WSCore screenSize].width, 320)];
    
    
    UILabel * timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(view.frame)/2-100, 11, 200, 21)];
    timeLabel.text = @"Country Codes";
    timeLabel.adjustsFontSizeToFitWidth=YES;
    timeLabel.font = [UIFont systemFontOfSize: 17.0f];;
    timeLabel.textColor = [UIColor blackColor];
    timeLabel.textAlignment=NSTextAlignmentCenter;
    [view insertSubview:timeLabel atIndex:1];
    
    
    //cancel
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 2, 80, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelCountryCodesForm) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [view insertSubview:cancelButton atIndex:1];
    
    UIButton * doneButton = [[UIButton alloc] initWithFrame:CGRectMake(240, 2, 80, 40)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneCountryCodesForm) forControlEvents:UIControlEventTouchUpInside];
    doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    doneButton.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    [view insertSubview:doneButton atIndex:1];
    
    
    [WSCore addTopLine:view :[UIColor lightGrayColor] :0.5f];
    
    self.countryCodePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, view.frame.size.width, 300)];
    self.countryCodePickerView.delegate=self;
    self.countryCodePickerView.dataSource = self;
    [self.countryCodePickerView selectRow:self.selectedIndex inComponent:0 animated:NO];
    [view addSubview:self.countryCodePickerView];
    self.viewForAreaCode = view;
    
    UIView * coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-self.viewForAreaCode.frame.size.height)];
    coverView.alpha=0.0;
    coverView.backgroundColor=[UIColor blackColor];
    self.coverView=coverView;
    [self.view addSubview: self.coverView];
    [self.view addSubview:self.viewForAreaCode];
    
    [self popInViewAnimationWithView:self.viewForAreaCode andWithCoverView:self.coverView];
    
}


#pragma mark - GCNetworkManager
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    [self.view endEditing:YES];
    
    
    if (manager==self.networkManager) {
        //animation
        
        self.mobileNumberTextField.enabled=NO;
        self.areaCodeButton.enabled=NO;
        //self.verifyButton.enabled=NO;
        [self.verifyButton setTitle:@"Resend" forState:UIControlStateNormal];
        self.verifyButton.titleLabel.adjustsFontSizeToFitWidth=YES;
        self.verificationCode.hidden=NO;
        self.submitButton.hidden=NO;
        [UIView animateWithDuration:1.5 animations:^{
         
            
            self.verificationCode.alpha=1.0;
            self.submitButton.alpha=1.0;
            
        } completion:^(BOOL finished) {
            
            self.verifyTextSent=YES;
        }];
       
        
        
    }
    else if(manager==self.verifyTwilioManager){
        //verify Twilio
        
        [[User getInstance] updateTwilioVerified:YES];
        if ([jsonDictionary[@"message"][@"secret_key"] length]>0) {
            [[User getInstance] updateKey:jsonDictionary[@"message"][@"secret_key"] ];
            
        }
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Verified" message:@"Thank you! Your phone number is now verified" delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        [alert show];
        
        double delayInSeconds = 1.0;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alert dismissWithClickedButtonIndex:0 animated:YES];
            
            [[User getInstance] updatePhoneNumber:self.phoneNumber];
            
            [self dismissViewControllerAnimated:YES completion:^{
            if ([self.delegate respondsToSelector:@selector(twilioWasVerified)]) {
                [self.delegate twilioWasVerified];
            }
            }];
            
        });
        
  
    }
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    [self.view endEditing:YES];
    
     NSLog(@"Response %@",jsonDictionary);
    
    if (manager==self.networkManager) {
        //animation
        [UIView showSimpleAlertWithTitle:@"Verify Error" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
        
    }
    else if(manager==self.verifyTwilioManager){
        //verify Twilio
        [UIView showSimpleAlertWithTitle:@"Oops" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
        
    }
}


#pragma  mark - area code picker view actions
//remove cviews after selecting done
-(void)destroyPickerContainerView{
    
    [self.countryCodePickerView removeFromSuperview];
    [self.coverView removeFromSuperview];
    self.viewForAreaCode=nil;
    self.coverView=nil;
}


-(void)cancelCountryCodesForm{
    
    [self popOutViewAnimationWithView:self.viewForAreaCode andWithCoverView:self.coverView];
}

-(void)doneCountryCodesForm{
    
    self.selectedIndex = [self.countryCodePickerView selectedRowInComponent:0];
    [self.areaCodeButton setTitle:[self.countryCodes objectAtIndex:self.selectedIndex] forState:UIControlStateNormal];
    [self popOutViewAnimationWithView:self.viewForAreaCode andWithCoverView:self.coverView];
}

-(void)popOutViewAnimationWithView: (UIView *) container andWithCoverView: (UIView *) blockView{
    // animate it to the identity transform (100% scale)
    

    
    [UIView animateWithDuration:0.1 animations:^{
        blockView.alpha=0.0f;
    } completion:^(BOOL finished) {
        
        container.transform = CGAffineTransformIdentity;
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            container.transform = CGAffineTransformMakeScale(0.01, 0.01);
        } completion:^(BOOL finished) {
            
            
            
            [self destroyPickerContainerView];
            
        }];
        
    }];
}
-(void)popInViewAnimationWithView: (UIView *) container andWithCoverView: (UIView *)blockView{
    

    
    container.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        container.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        
        [UIView animateWithDuration:0.1 animations:^{
            blockView.alpha=0.5;
        }];
        
    }];
}

#pragma mark - Picker
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.countryCodes.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    
    return [NSString stringWithFormat:@"%@ %@",self.countries[row],self.countryCodes[row]];
}



@end
