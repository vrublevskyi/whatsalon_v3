//
//  BrowseTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 15/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "BrowseTableViewCell.h"
#import "BrowseViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "User.h"
#import "UIImage+ColoredImage.h"
#import "UIColor+FlatUI.h"
#import "UIView+AlertCompatibility.h"
#import "SalonIndividualDetailViewController.h"
#import "SalonAvailableTime.h"
#import "LastMinuteItem.h"
#import "ConfirmViewController.h"
#import "UILabel+Boldify.h"


@implementation BrowseTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       // self.backgroundColor = [UIColor blueColor];
        self.clipsToBounds = YES;
        self.salonName = [[UILabel alloc] initWithFrame:CGRectMake(8, 170, 300, 20)];
        self.salonName.text = @"Test";
        self.salonName.textColor = [UIColor whiteColor];
        self.salonName.font = [UIFont boldSystemFontOfSize:16.0];
        self.salonName.adjustsFontSizeToFitWidth=YES;
        
        if (!IS_IOS_8_OR_LATER) {
           self.salonImage = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, self.bounds.size.width, CellHeight)];
        }
        else{
        
            self.salonImage = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, self.bounds.size.width, CGRectGetHeight(self.bounds))];
        }
        self.salonImage.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.salonImage.contentMode = UIViewContentModeScaleAspectFill;

        self.overlayLabel  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.contentView.bounds), CGRectGetHeight(self.contentView.bounds))];
        self.overlayLabel.contentMode = UIViewContentModeScaleToFill;
        self.overlayLabel.image = [UIImage imageNamed:@"overlayGradient"];
        
        
        self.overlayLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
      
        
        self.overlayLabel.contentMode = UIViewContentModeScaleToFill;
        self.overlayLabel.alpha = 0.6;
        self.overlayLabel.hidden=YES;
        
        self.addressLabel = [[UILabel alloc] init];;
        self.addressLabel.text = @"";
        self.addressLabel.textColor = [UIColor whiteColor];
        self.addressLabel.font = [UIFont systemFontOfSize:14.0];
        self.addressLabel.adjustsFontSizeToFitWidth=NO;
        self.addressLabel.numberOfLines = 0;
        self.addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        
        self.favLabel = [[UIImageView alloc] initWithFrame:CGRectMake(286, 10, 24, 23)];
        self.selectFavLabel = [[UILabel alloc] initWithFrame:CGRectMake(270, 0, 60, 60)];
        self.selectFavLabel.backgroundColor=[UIColor clearColor];
        self.selectFavLabel.userInteractionEnabled=YES;
        
        
        [self insertSubview:self.salonImage atIndex:0];
        [self insertSubview:self.overlayLabel atIndex:1];
       
        
        //[self insertSubview:self.labelOffer atIndex:3];
        [self insertSubview:self.favLabel atIndex:3];
        [self.contentView addSubview:self.selectFavLabel];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        UITapGestureRecognizer * tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
        tapPress.numberOfTapsRequired=1;
        tapPress.delegate=self;
        tapPress.delaysTouchesBegan=YES;
        tapPress.cancelsTouchesInView=YES;
        [self.selectFavLabel addGestureRecognizer:tapPress];
        
        self.whiteLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, DiscoveryCellHeight-70, self.frame.size.width, 70)];
        //self.whiteLabel.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
        self.bubble = [[UIImageView alloc] initWithFrame:self.whiteLabel.frame] ;
        //self.bubble.image =[UIImage imageNamed:@"top_bubble_gray_glow.png"];
        self.bubble.image =[UIImage imageNamed:@"letter_box_2"];
        self.bubble.image = [self.bubble.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.bubble setTintColor:[UIColor whiteColor]];
       // self.bubble.frame =CGRectMake(0, 0, 320, 55);
         //self.bubble.frame =CGRectMake(0, 0, 320, 65);
       // self.bubble.frame =CGRectMake(-20, 0, 325, 60);//for glow
        self.bubble.contentMode = UIViewContentModeScaleToFill;
        [self.whiteLabel addSubview:self.bubble];
        [self insertSubview:self.whiteLabel atIndex:2];
        
        
        self.addedShadow=NO;
        
        self.mapHolder= [[UILabel alloc] init];
        [self.contentView addSubview:self.mapHolder];
        
        self.ratingsHolder = [[UILabel alloc ]init];
        [self.contentView addSubview:self.ratingsHolder];
        
        self.networkManager = [[GCNetworkManager alloc] init];
        self.networkManager.delegate=self;
    
        
        self.labelMiles = [[UILabel alloc] initWithFrame:CGRectMake(self.bubble.frame.size.width-80, self.bubble.frame.size.height-30, 70, 20)];
        self.labelMiles.text = @"0.5mls";
        self.labelMiles.textColor = [UIColor whiteColor];
        self.labelMiles.textAlignment = NSTextAlignmentRight;
        self.labelMiles.textColor = kWhatSalonBlue;
        self.labelMiles.font = [UIFont systemFontOfSize:14.0];
        self.labelMiles.adjustsFontSizeToFitWidth=YES;
        self.labelMiles.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.bubble addSubview:self.labelMiles];
        /*
        UIImageView * insta = [[UIImageView alloc] initWithFrame:CGRectMake(275, 230-60-45, 30, 30)];
        insta.image = [UIImage imageNamed:@"insta_icon"];
        [self.contentView addSubview:insta];
         */
        
        //[self.bubble addSubview: self.addressLabel];
        [self.contentView addSubview:self.addressLabel];
       // [self.bubble addSubview: self.salonName];
        [self.contentView addSubview:self.salonName];

        //self.instantBookImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.bubble.frame.size.width-25, 20, 20, 20)];
        self.instantBookImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.bubble.frame.size.width-25, 20, 20, 20)];
        self.instantBookImage.image = [UIImage imageNamed:@"instant_booking_filled_discovery"];
        self.instantBookImage.image = [self.instantBookImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.instantBookImage.tintColor = [UIColor sunflowerColor];
        [self.whiteLabel addSubview:self.instantBookImage];
        
        
        
    }
    return self;
}

-(void)showNetworkIndicator{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
}
-(void)dismissNetworkIndicator{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

#pragma mark - GCNetwork Delegate
- (void)favouriteButtonState {
    if (self.isFav) {
        
        if ([self.myDelegate respondsToSelector:@selector(didAddFavourite:)]) {
            [self.myDelegate didUnFavourite:self.salon];
        }
        else{
            [self postNotificationWithSalonRemoveFavourite: self.salonID];
        }
        
        self.isFav=NO;
        self.favLabel.image = [UIImage imageNamed:@"White_Heart_With_Gray_Solid"];
       
        
    }
    else{
        self.isFav=YES;
        self.favLabel.image = [UIImage imageNamed:@"fav_heart.png"];
        
     
        self.favLabel.image = [self.favLabel.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.favLabel.tintColor = [UIColor alizarinColor];
  
        if ([self.myDelegate respondsToSelector:@selector(didAddFavourite:)]) {
            [self.myDelegate didAddFavourite:self.salon];
        }
        else{
             [self postNotificationWithSalonIsFavourite:self.salonID];
        }
       
        
        
    }
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    
    
    if ([jsonDictionary[@"message"] isEqualToString:@"This user already favourited this salon"]) {
        [self favouriteButtonState];

    }
    [self enableFavLabel];
    
    
}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
   
    [self favouriteButtonState];
    
    self.favLabel.contentMode = UIViewContentModeScaleToFill;
    [self enableFavLabel];
    
}
-(void)manager:(GCNetworkManager *)manager handleServerFailureWithError:(NSError *)error{
    
    [self enableFavLabel];
    [WSCore showServerErrorAlert];
}
- (void)disableFavLabel {
    self.selectFavLabel.userInteractionEnabled=NO;
    self.favLabel.alpha=0.5;
}
-(void)enableFavLabel{
    self.selectFavLabel.userInteractionEnabled=YES;
    self.favLabel.alpha=1.0;
}

-(void)tapPress: (UIGestureRecognizer*) hold{
    
    NSString * accessToken =[[User getInstance] fetchAccessToken];
    
    if (accessToken==nil) {
        [WSCore showLoginSignUpAlert];
        
        return;
    }
    
    if (self.isFav) {
    
        NSLog(@"unmark");
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonID]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&unmark=true"]];
        
        if ([[User getInstance] fetchKey].length!=0) {
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
        }
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        [self disableFavLabel];
        
    }
    else{
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonID]];
        
        if ([[User getInstance] fetchKey].length!=0) {
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
        }
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        
        [self disableFavLabel];
        
    }
    
    
}

-(void)showLikeCompletion{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Added to favourites!" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    
    double delayInSeconds = 1.0;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });
    
}

-(void)setCellImageWithURL: (NSString*) url{
    
    if ([WSCore isDummyMode]) {
        self.salonImage.image=[UIImage imageNamed:url];
    }
    else{
        if (url.length <1) {
            self.salonImage.image = kPlace_Holder_Image;
            self.salonImage.contentMode=UIViewContentModeScaleAspectFill;
            
            
        }
        else{
            [self.salonImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:kPlace_Holder_Image];
            
            
        }
    }
    
    
}

- (void)postNotificationWithSalonIsFavourite:(NSString *)salonId //post notification method and logic
{
    NSString *notificationName = @"salonIsAddedToFavourites";
    NSString *key = @"Salon_id";
    NSLog(@"Salon ID %@",salonId);
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:salonId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

- (void)postNotificationWithSalonRemoveFavourite:(NSString *)salonId //post notification method and logic
{
    NSString *notificationName = @"salonIsNotFavourite";
    NSString *key = @"Salon_id";
    NSLog(@"Salon ID %@",salonId);
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:salonId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}


-(void)dealloc{
    
    [self removeFromSuperview];
   
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setUpCellWithSalon: (id) s{
   
    LastMinuteItem *last;
    if([s isKindOfClass:[Salon class]]){
        self.salon = (Salon *)s;
    }
    else if([s isKindOfClass:[LastMinuteItem class]]){
    
        last = (LastMinuteItem *)s;
        self.lastMin = last;
      
        self.salon = last.lastMinuteSalon;
        NSLog(@"last min salon %@ start time %@",last.lastMinuteSalon.salon_name,last.startTime);
    }
    
    if (!IS_IOS_8_OR_LATER) {
        self.contentView.frame = CGRectMake(0, 0, self.bounds.size.width, CellHeight);
    }
    
    self.salonName.text = self.salon.salon_name;
    self.salonID =self.salon.salon_id;
    self.address1 = self.salon.salon_address_1;
    self.address2 = self.salon.salon_address_2;
    self.address3 = self.salon.salon_address_3;
    self.city_name = self.salon.city_name;
    
    
    [self setCellImageWithURL:self.salon.salon_image];
    
    
    self.addressLabel.text = @"";
    if (![self.salon.salon_address_1 isEqual:[NSNull null]]) {
        if (self.salon.salon_address_1.length!=0)
            self.addressLabel.text = [self.addressLabel.text stringByAppendingString:[NSString stringWithFormat:@"%@",self.salon.salon_address_1]];
    }
    if (![self.salon.salon_address_2 isEqual:[NSNull null]] && ![self.salon.salon_address_1 isEqualToString:self.salon.salon_address_2]) {
        if (self.salon.salon_address_2.length!=0)
            self.addressLabel.text = [self.addressLabel.text stringByAppendingString:[NSString stringWithFormat:@", %@",self.salon.salon_address_2]];
    }
    if (![self.salon.salon_address_3 isEqual:[NSNull null]] && ![self.salon.salon_address_2 isEqualToString:self.salon.salon_address_3]) {
        if (self.salon.salon_address_3.length!=0)
            self.addressLabel.text = [self.addressLabel.text stringByAppendingString:[NSString stringWithFormat:@", %@",self.salon.salon_address_3]];
    }
    if (![self.salon.city_name isEqual:[NSNull null]]) {
        if (self.salon.city_name.length!=0 &&![self.salon.city_name isEqualToString:self.salon.salon_address_3])
            self.addressLabel.text = [self.addressLabel.text stringByAppendingString:[NSString stringWithFormat:@", %@",self.salon.city_name]];
    }
    
    self.whiteLabel.frame = CGRectMake(0, CellHeight-75, CGRectGetWidth(self.contentView.bounds)+10, 75);//for glow
    self.bubble.frame =CGRectMake(-5, 0, CGRectGetWidth(self.contentView.bounds)+10, 75);//for glow
    if (self.addressLabel.text.length >33) {
        
        
        self.addressLabel.frame = CGRectMake(8, 188, 210, 38);
        self.salonName.textColor = [UIColor darkGrayColor];
        self.addressLabel.font = [UIFont systemFontOfSize:14.0f];
        self.addressLabel.textColor = [UIColor grayColor];
        self.salonImage.frame =CGRectMake(0, 0, self.contentView.bounds.size.width,CellHeight-50);
        
        
        
    }else{
        
       
        self.addressLabel.frame = CGRectMake(8, 188, 320, 16);
        self.salonName.textColor = [UIColor darkGrayColor];
        self.addressLabel.textColor = [UIColor grayColor];
        self.addressLabel.font = [UIFont systemFontOfSize:14.0f];
        self.salonImage.frame =CGRectMake(0, 0, self.contentView.bounds.size.width, CellHeight-50);
        
        
    }
    
    
    UILabel * seperator = [[UILabel alloc] initWithFrame:CGRectMake(0, self.contentView.frame.size.height-1.0,self.contentView.frame.size.width,1.0)];
    seperator.backgroundColor = [UIColor darkGrayColor];
    [self.contentView addSubview:seperator];
    
    
    
    
    
    if (self.salon.is_favourite) {
        self.favLabel.image = [UIImage imageNamed:@"fav_heart"];
        self.isFav = YES;
        
        
        self.favLabel.image = [self.favLabel.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.favLabel.tintColor = [UIColor alizarinColor];
    
    }
    else{
        
        self.favLabel.image = [UIImage imageNamed:@"White_Heart_With_Gray_Solid"];
        
        
        self.isFav=NO;
    }
    
    if (self.isDiscovery) {
        self.favLabel.hidden=YES;
        self.selectFavLabel.hidden=YES;
        UIView * discoveryButton = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 54)];
        discoveryButton.backgroundColor=kWhatSalonBlue;
        [self.contentView addSubview:discoveryButton];
        discoveryButton.center = CGPointMake(self.frame.size.width-10-(discoveryButton.frame.size.width/2), (self.frame.size.height-self.bubble.frame.size.height)-(discoveryButton.frame.size.height/2)-5);
        discoveryButton.layer.cornerRadius=kCornerRadius;

        UILabel *priceLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, discoveryButton.frame.size.width, discoveryButton.frame.size.height)];
        [discoveryButton addSubview:priceLabel];
        priceLabel.text=@"Book Now";
        priceLabel.textColor=[UIColor cloudsColor];
        priceLabel.textAlignment=NSTextAlignmentCenter;
        
        UITapGestureRecognizer *makeBookingTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(makeBooking)];
        makeBookingTap.numberOfTapsRequired=1;
        makeBookingTap.delegate=self;
        makeBookingTap.delaysTouchesBegan=YES;
        makeBookingTap.cancelsTouchesInView=YES;
        [discoveryButton addGestureRecognizer:makeBookingTap];
       
        self.mapHolder.frame =CGRectMake(self.frame.size.width-54, 15, 44, 44);
        self.mapHolder.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.2];
        self.mapHolder.layer.cornerRadius=self.mapHolder.frame.size.width/2.0f;
        self.mapHolder.layer.masksToBounds=YES;
        
        UIImageView * mapImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-54, 15, 30, 30)];
        mapImageView.image= [UIImage imageNamed:@"Map"];
        mapImageView.image = [mapImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        mapImageView.tintColor=[UIColor whiteColor];
        [self.contentView addSubview:self.mapHolder];
        [self.contentView addSubview:mapImageView];
        mapImageView.center = self.mapHolder.center;
        
        
        self.ratingsHolder.frame = CGRectMake(10,self.frame.size.height-(self.bubble.frame.size.height+35),100,30) ;
        self.ratingsHolder.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.2];
      
        UIImageView *stars = [[UIImageView alloc] initWithFrame:self.ratingsHolder.frame];
        NSLog(@"salon rating %f",self.salon.ratings);
        stars.image=[WSCore getStarRatingImage:(int)self.salon.ratings];
        stars.contentMode=UIViewContentModeScaleAspectFit;
        if (self.salon.ratings>0) {
            stars.tintColor=[UIColor whiteColor];
        }
        else{
            stars.tintColor=[UIColor lightGrayColor];
        }
        
        self.ratingsHolder.layer.cornerRadius=kCornerRadius;
        self.ratingsHolder.layer.masksToBounds=YES;
        [self.contentView addSubview:stars];
        
        self.instantBookImage.hidden=YES;
       CGRect frame = CGRectMake(self.frame.size.width-180, 10, 180, self.bubble.frame.size.height);
        self.labelMiles.frame=frame;
        
        UITapGestureRecognizer *mapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMap)];
        mapGesture.numberOfTapsRequired=1;
        mapGesture.delegate=self;
        mapGesture.delaysTouchesBegan=YES;
        mapGesture.cancelsTouchesInView=YES;
        [self.mapHolder addGestureRecognizer:mapGesture];
        self.mapHolder.userInteractionEnabled=YES;
        mapImageView.userInteractionEnabled=NO;
        
        UITapGestureRecognizer *ratingGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showRatings)];
        ratingGesture.numberOfTapsRequired=1;
        ratingGesture.delegate=self;
        ratingGesture.delaysTouchesBegan=YES;
        ratingGesture.cancelsTouchesInView=YES;
        [self.ratingsHolder addGestureRecognizer:ratingGesture];
        self.ratingsHolder.userInteractionEnabled=YES;
        stars.userInteractionEnabled=NO;
       
        NSString * time =[last getSimpleDateISO8601];
        self.labelMiles.text = [NSString stringWithFormat:@"%@%@\n %@",CURRENCY_SYMBOL,last.service_price,time];
        
        self.labelMiles.numberOfLines=0;
        self.labelMiles.textAlignment=NSTextAlignmentRight;
        self.labelMiles.font = [UIFont systemFontOfSize: 18.0f];
        [self.labelMiles boldSubstring:time];
       
        
        if (!self.addedShadow) {
            
            [WSCore floatingView:discoveryButton];
           
            self.addedShadow=YES;
        }
        
       
        
    }
    else{
        
        //Not discovery
        if (self.salon.salon_tier==1.0) {
            self.instantBookImage.hidden=NO;
        }
        else{
            self.instantBookImage.hidden=YES;
        }
        
        NSLog(@"Index path %ld",(long)self.cellIndexPathRow);
        if (self.isNearest) {
            self.labelMiles.text = @"Nearest";
            self.labelMiles.font = [UIFont boldSystemFontOfSize:14.0f];
            
  
        }
        else{
            self.labelMiles.text = [NSString stringWithFormat:@"%.1f km", self.salon.distance ] ;
            self.labelMiles.font = [UIFont systemFontOfSize:14.0f];
        }

    }
    

}

-(void)showRatings{
    if (self.salon.ratings>0) {
        NSLog(@"Show ratings");
        if ([self.myDelegate respondsToSelector:@selector(showReviewsWithLastMinuteItem:)]) {
            [self.myDelegate showReviewsWithLastMinuteItem:self.lastMin];
        }
    }
    
}
-(void)showMap{
    NSLog(@"Show map");
    if ([self.myDelegate respondsToSelector:@selector(showMapWithLastMinuteItem:)]) {
        [self.myDelegate showMapWithLastMinuteItem:self.lastMin];
    }
}
-(void)makeBooking{
    
  
    UIAlertView * makeBooking = [[UIAlertView alloc] initWithTitle:@"Make Booking" message:@"You are about to make a booking, continue?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    makeBooking.tag=1111;
    [makeBooking show];
    
    
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1111) {
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            
            
            if ([self.myDelegate respondsToSelector:@selector(bookNowActionTriggeredWithLastMinuteItem:)]) {
                [self.myDelegate bookNowActionTriggeredWithLastMinuteItem:self.lastMin];
            
           
            
            }
        }
    }
}

@end
