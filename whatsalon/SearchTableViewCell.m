//
//  SearchTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 09/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SearchTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UILabel+Boldify.h"

@implementation SearchTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.clipsToBounds=YES;
        self.salonImage = [[UIImageView alloc] initWithFrame: CGRectMake(2, 2, 86, 79)];
        self.salonImage.contentMode = UIViewContentModeScaleAspectFill;
        self.salonImage.layer.masksToBounds=YES;
        self.salonImage.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.salonImage.layer.borderWidth=0.5;
        self.salonImage.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.salonImage.layer.masksToBounds=YES;
        self.salonImage.clipsToBounds=YES;
        self.salonImage.layer.cornerRadius=kCornerRadius;
        
        [self.contentView insertSubview:self.salonImage atIndex:0];
        
        self.salonName = [[UILabel alloc] initWithFrame:CGRectMake(102, 20, 165, 21)];
        self.salonName.textColor = [UIColor blackColor];
        self.salonName.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
        self.salonName.adjustsFontSizeToFitWidth=YES;
        
        [self.contentView addSubview:self.salonName];
        
        self.salonAddress = [[UILabel alloc] initWithFrame:CGRectMake(102, 39, 185, 21)];
        self.salonAddress.textColor = [UIColor blackColor];
        self.salonAddress.adjustsFontSizeToFitWidth=YES;
        self.salonAddress.numberOfLines=0;
        self.salonAddress.font = [UIFont systemFontOfSize:13.0];
        self.salonAddress.adjustsFontSizeToFitWidth=NO;
        self.salonAddress.numberOfLines = 0;
        self.salonAddress.lineBreakMode = NSLineBreakByWordWrapping;
        
        [self.contentView addSubview:self.salonAddress];
        
        self.selectionStyle = UITableViewCellSelectionStyleDefault;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        self.contentView.layer.masksToBounds=YES;
        
    
    }
    return self;
}

-(void)setCellImageWithURL: (NSString *)url{
    
    if (url.length<1) {
        self.salonImage.image = [UIImage imageNamed:@"empty_cell"];
        self.salonImage.contentMode=UIViewContentModeScaleAspectFill;
    }
    else{
        // Here we use the new provided setImageWithURL: method to load the web image
        [self.salonImage  sd_setImageWithURL:[NSURL URLWithString:url]
                            placeholderImage:[UIImage imageNamed:@"empty_cell"]];
    }
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUpCellWithSalon:(Salon *)salon{
    
    self.salonAddress.text = @"";
    
    CGRect frame =self.salonImage.frame;
    frame.size.height=self.frame.size.height-4;
    self.salonImage.frame=frame;
    
    
    //sets up the address label
    if (salon.salon_address_1.length!=0) {
        self.salonAddress.text = [self.salonAddress.text stringByAppendingString:[NSString stringWithFormat:@"%@",salon.salon_address_1]];
    }
    if (salon.salon_address_2.length!=0) {
        self.salonAddress.text = [self.salonAddress.text stringByAppendingString:[NSString stringWithFormat:@", %@",salon.salon_address_2]];
    }
    if (salon.salon_address_3.length!=0) {
        self.salonAddress.text = [self.salonAddress.text stringByAppendingString:[NSString stringWithFormat:@", %@",salon.salon_address_3]];
    }
    if (salon.city_name.length!=0 ) {
        self.salonAddress.text = [self.salonAddress.text stringByAppendingString:[NSString stringWithFormat:@", %@",salon.city_name]];
        
    }
    
    if (self.salonAddress.text.length>30) {
        self.salonAddress.frame = CGRectMake(102, 32, 175, 41);
        self.salonName.frame=CGRectMake(102, 13, 165, 21);
    }
    else{
        self.salonName.frame = CGRectMake(102, 20, 165, 21);
        self.salonAddress.frame=CGRectMake(102, 39, 175, 21);
        
    }
    
    //sets up the image
    [self setCellImageWithURL:salon.salon_image];
    
    
    self.salonName.text = salon.salon_name;
    
    //adds a sperator for aesthetic reaspons
    UIView * seperator = [[UIView alloc] initWithFrame:CGRectMake(0,self.frame.size.height-0.5, self.frame.size.width, 0.5)];
    seperator.backgroundColor=[UIColor lightGrayColor];
    [self.contentView addSubview:seperator];
    
    //bold search term
    if (self.searchTerm.length>0) {
        NSRange salonNameRangeValue = [self.salonName.text rangeOfString:self.searchTerm options:NSCaseInsensitiveSearch];
        
        if (salonNameRangeValue.length > 0){
            
            NSLog(@"Contains search term");
            [self.salonName boldSubstring:self.searchTerm];
            
        }
        
        //bold search term
        NSRange salonAddressRangeValue = [self.salonAddress.text rangeOfString:self.searchTerm options:NSCaseInsensitiveSearch];
        
        if (salonAddressRangeValue.length > 0){
            
            NSLog(@"contains search term");
            [self.salonAddress boldSubstring:self.searchTerm];
            
        }

    }
    
    
    
}

@end
