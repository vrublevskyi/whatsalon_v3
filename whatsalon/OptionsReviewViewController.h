//
//  OptionsReviewViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyBookings.h"
#import "Salon.h"

@protocol BookingsReviewDelegate <NSObject>

@optional
-(void)bookAgainWithSalon: (Salon*)salon;

-(void)canceledAppointment: (MyBookings *)booking;

@end
@interface OptionsReviewViewController : UIViewController

@property (nonatomic,assign) id<BookingsReviewDelegate> delegate;
@property (nonatomic) MyBookings * bookingData;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
- (IBAction)buttonAction1:(id)sender;
- (IBAction)buttonAction3:(id)sender;

- (IBAction)buttonAction2:(id)sender;
@end
