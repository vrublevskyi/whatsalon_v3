//
//  SalonStylistPickerData.m
//  whatsalon
//
//  Created by Graham Connolly on 16/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonStylistPickerData.h"

@implementation SalonStylistPickerData
/*
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self loadStyles];
    }
    return self;
}
*/
- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        [self loadStyles];
    }
    return self;
}


-(void)loadStyles{
    
    self.arrayOfStyles = [[NSMutableArray alloc] initWithObjects:@"Any Stylist",@"Fiona Stiles", @"Scott Barnes",@"Pat McGrath",@"Jackie Gomez", nil];
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.arrayOfStyles.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [self.arrayOfStyles objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    NSString * selectedRow = [self.arrayOfStyles objectAtIndex:row];
    NSLog(@"Selected a Stylist");
    if([self.myDelegate respondsToSelector:@selector(didSelectStylist:)])
    {
        [self.myDelegate didSelectStylist:selectedRow];
    }
   
    
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString * title;
    
    title = [self.arrayOfStyles objectAtIndex:row];
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
