//
//  MKMapView-Additions.m
//  vevoke
//
//  Created by Alexandru Chis on 11/24/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MKMapView-Additions.h"

#define MERCATOR_OFFSET 268435456
#define MERCATOR_RADIUS 85445659.44705395


@implementation MKMapView (MKMapView_Additions)


+ (double)distanceFromStartCoordinate:(CLLocationCoordinate2D)startCoordinate
					  toEndCoordinate:(CLLocationCoordinate2D)endCoordinate {
    
	/* calculate distance from last locaiton */
	int earthRadius = 6371; /* in kilometers */
	
	double latitudeDelta = (endCoordinate.latitude - startCoordinate.latitude) * M_PI / 180.0;
	double longitudeDelta = (endCoordinate.longitude - startCoordinate.longitude) * M_PI / 180.0;
	double a = sin(latitudeDelta / 2) * sin(latitudeDelta / 2) +	cos(startCoordinate.latitude * M_PI / 180.0) *
    cos(endCoordinate.latitude * M_PI / 180.0) *
    sin(longitudeDelta / 2) *
    sin(longitudeDelta / 2);
	double c = 2 * atan2(sqrt(a), sqrt(1 - a));
	
	double distance = earthRadius * c;
	
	return distance;
}


+(CLLocationCoordinate2D)translateCoord:(CLLocationCoordinate2D)coord MetersLat:(double)metersLat MetersLong:(double)metersLong{
    
    CLLocationCoordinate2D tempCoord;
    
    MKCoordinateRegion tempRegion = MKCoordinateRegionMakeWithDistance(coord, metersLat, metersLong);
    MKCoordinateSpan tempSpan = tempRegion.span;
    
    tempCoord.latitude = coord.latitude + tempSpan.latitudeDelta;
    tempCoord.longitude = coord.longitude + tempSpan.longitudeDelta;
    
    return tempCoord;
    
}

+ (MKCoordinateRegion)regionForAnnotations:(NSArray *)annotations {
	CLLocationDegrees maxLatitude = -90.0;
	CLLocationDegrees minLatitude = 90.0;
	CLLocationDegrees maxLongitude = -180.0;
	CLLocationDegrees minLongitude = 180.0;
	
	for (id<MKAnnotation>anno in annotations) {
		CLLocationDegrees thisLatitude = anno.coordinate.latitude;
		CLLocationDegrees thisLongitude = anno.coordinate.longitude;
		
		if (thisLatitude > maxLatitude) maxLatitude = thisLatitude;
		if (thisLatitude < minLatitude) minLatitude = thisLatitude;
		if (thisLongitude > maxLongitude) maxLongitude = thisLongitude;
		if (thisLongitude < minLongitude) minLongitude = thisLongitude;
	}
	
	MKCoordinateRegion myRegion;
	
	myRegion.center.latitude = (maxLatitude + minLatitude) / 2.0;
	myRegion.center.longitude = (maxLongitude + minLongitude) / 2.0;
	myRegion.span.latitudeDelta = (maxLatitude - minLatitude) + 0.005;
	myRegion.span.longitudeDelta = (maxLongitude - minLongitude) + 0.005;
	
	if (myRegion.span.latitudeDelta == 0.0) myRegion.span.latitudeDelta = 0.05;
	if (myRegion.span.longitudeDelta == 0.0) myRegion.span.longitudeDelta = 0.05;
	
	return myRegion;
}

+ (MKCoordinateRegion)regionArroundAnnotationWithYOffset:(id<MKAnnotation>) annotation andOffset:(float) offset {
    double miles = 0.992; //1000 metres
    double scalingFactor = ABS( (cos(2 * M_PI * annotation.coordinate.latitude / 360.0) ));
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = miles/69.0;
    span.longitudeDelta = miles/(scalingFactor * 69.0);
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = annotation.coordinate;
    
    MKMapPoint regionCenter = MKMapPointForCoordinate(annotation.coordinate);
    regionCenter.y += offset; //we move everything down because of the top black bar
    region.center = MKCoordinateForMapPoint(regionCenter);
    
    return region;
}


+ (MKCoordinateRegion)regionArroundGPSPosition:(CLLocationCoordinate2D)coordinate  andOffset:(float) offset andCurrentRegion:(MKCoordinateRegion) currentRegion {
    
    MKCoordinateRegion regionNew;
    regionNew.span = currentRegion.span;
    regionNew.center = coordinate;
    
    MKMapPoint regionCenter = MKMapPointForCoordinate(coordinate);
    regionCenter.y += offset; //we move everything down because of the top black bar
    regionNew.center = MKCoordinateForMapPoint(regionCenter);
    
    return regionNew;
}


+ (MKCoordinateRegion)regionArroundAnnotation:(MKMapView*) annotation {
    double miles = 0.992; //1000 metres
    double scalingFactor = ABS( (cos(2 * M_PI * annotation.centerCoordinate.latitude / 360.0) ));
    
    MKCoordinateSpan span;
    
    span.latitudeDelta = miles/69.0;
    span.longitudeDelta = miles/(scalingFactor * 69.0);
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = annotation.centerCoordinate;
    
	return region;
}

//===================================


- (double)longitudeToPixelSpaceX:(double)longitude
{
    return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * M_PI / 180.0);
}

- (double)latitudeToPixelSpaceY:(double)latitude
{
    return round(MERCATOR_OFFSET - MERCATOR_RADIUS * logf((1 + sinf(latitude * M_PI / 180.0)) / (1 - sinf(latitude * M_PI / 180.0))) / 2.0);
}

- (double)pixelSpaceXToLongitude:(double)pixelX
{
    return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / M_PI;
}

- (double)pixelSpaceYToLatitude:(double)pixelY
{
    return (M_PI / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / M_PI;
}

#pragma mark -
#pragma mark Helper methods

- (MKCoordinateSpan)coordinateSpanWithMapView:(MKMapView *)mapView
                             centerCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                 andZoomLevel:(NSUInteger)zoomLevel
{
    // convert center coordiate to pixel space
    double centerPixelX = [self longitudeToPixelSpaceX:centerCoordinate.longitude];
    double centerPixelY = [self latitudeToPixelSpaceY:centerCoordinate.latitude];
    
    // determine the scale value from the zoom level
    NSInteger zoomExponent = 20 - zoomLevel;
    double zoomScale = pow(2, zoomExponent);
    
    // scale the map’s size in pixel space
    CGSize mapSizeInPixels = mapView.bounds.size;
    double scaledMapWidth = mapSizeInPixels.width * zoomScale;
    double scaledMapHeight = mapSizeInPixels.height * zoomScale;
    
    // figure out the position of the top-left pixel
    double topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
    double topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
    
    // find delta between left and right longitudes
    CLLocationDegrees minLng = [self pixelSpaceXToLongitude:topLeftPixelX];
    CLLocationDegrees maxLng = [self pixelSpaceXToLongitude:topLeftPixelX + scaledMapWidth];
    CLLocationDegrees longitudeDelta = maxLng - minLng;
    
    // find delta between top and bottom latitudes
    CLLocationDegrees minLat = [self pixelSpaceYToLatitude:topLeftPixelY];
    CLLocationDegrees maxLat = [self pixelSpaceYToLatitude:topLeftPixelY + scaledMapHeight];
    CLLocationDegrees latitudeDelta = -1 * (maxLat - minLat);
    
    // create and return the lat/lng span
    MKCoordinateSpan span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
    return span;
}

#pragma mark -
#pragma mark Public methods

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated
{
    // clamp large numbers to 28
    zoomLevel = MIN(zoomLevel, 28);
    
    // use the zoom level to compute the region
    MKCoordinateSpan span = [self coordinateSpanWithMapView:self centerCoordinate:centerCoordinate andZoomLevel:zoomLevel];
    MKCoordinateRegion region = MKCoordinateRegionMake(centerCoordinate, span);
    
    // set the region like normal
    [self setRegion:region animated:animated];
}


@end
