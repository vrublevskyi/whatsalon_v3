//
//  CongratulationsScreenViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 04/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"
#import "Booking.h"

@interface CongratulationsScreenViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UILabel *appointmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *subAppointmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *thanksLabel;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (nonatomic) double chargedPrice;
@property (nonatomic) double remainingPrice;
@property (nonatomic) Salon * salonData;
@property (nonatomic) Booking * bookingObj;
@property (nonatomic) NSDate * chosenDate;
@property (nonatomic) BOOL isFromLastMinPopToSalonIndividual;
@property (nonatomic) BOOL unwindBackToSearchFilter;


@property (nonatomic) BOOL isFromLastMinute;
- (IBAction)directions:(id)sender;
- (IBAction)setAReminder:(id)sender;
- (IBAction)saveReceipt:(id)sender;
- (IBAction)cancel:(id)sender;

@property (nonatomic) BOOL isFromFavourite;

@end
