//
//  MCCamFocus.m
//  ManiCam
//
//  Created by Graham Connolly on 01/10/2015.
//  Copyright © 2015 WhatSalon. All rights reserved.
//

#import "MCCamFocusSquare.h"
const float squareLength = 80.0f;

@implementation MCCamFocusSquare

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setBackgroundColor:[UIColor clearColor]];
        [self.layer setBorderWidth:2.0];
        [self.layer setCornerRadius:4.0];
        //self.layer.cornerRadius = self.layer.frame.size.width/2.0f;
        [self.layer setBorderColor:[UIColor whiteColor].CGColor];
        
        CABasicAnimation* selectionAnimation = [CABasicAnimation
                                                animationWithKeyPath:@"borderColor"];
        selectionAnimation.toValue = (id)[UIColor pomegranateColor].CGColor;
        selectionAnimation.repeatCount = 8;
        [self.layer addAnimation:selectionAnimation
                          forKey:@"selectionAnimation"];
        
    }
    return self;
}

@end
