//
//  LastMinuteSearchViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 19/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LastMinuteSearchViewController : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchHolderHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchHolderVerticalSpaceY;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpace;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;
@property (weak, nonatomic) IBOutlet UIButton *todayButton;
@property (weak, nonatomic) IBOutlet UIButton *tomorrow;
- (IBAction)selectedDay:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;

@property (nonatomic) BOOL hasUsersLocation;
- (IBAction)changeLocation:(id)sender;

@end
