//
//  TabSearchSalonsTableViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 28/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabSearchSalonsTableViewController.h"
#import "UITableView+ReloadTransition.h"
#import "User.h"
#import "Salon.h"
#import "OpeningDay.h"
#import "SearchTableViewCell.h"
#import "LocationObject.h"
#import "UIView+AlertCompatibility.h"
#import "TabNewSalonDetailViewController.h"
#import "TabDirectoryViewController.h"
#import "GCGoogleAutoComplete.h"//added
#import "GCGoogleAutoCompleteObject.h"
#import "INTULocationManager.h"


/*! @brief represents the bacground tint for search. */
#define kBackgroundTintForSearch2 [UIColor colorWithWhite:1.0 alpha:0.9]

/*! @brief represents the constant for Current Location. */
NSString * CURRENT_LOCATION_TEXT =@"Current Location";

@interface TabSearchSalonsTableViewController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,GCNetworkManagerDelegate>

//search
/*! @brief represents the location search bar. */
@property (weak, nonatomic) IBOutlet UISearchBar *locationSearchBar;

/*! @brief determines if the salons have been filtered. */
@property (nonatomic) BOOL isFilteredSalons;

/*! @brief represents the search string. */
@property (nonatomic) NSString * searchString;

/*! @brief determines if the current view controller is beign presented modally. */
@property (nonatomic) BOOL isModal;

/*! @brief represents the filtered Salons. */

/*! @brief represents the network manager for making network requests. */

/*! @brief timer used to prevent multiple searches at once. */
@property (nonatomic) NSTimer * myTimer;

/*! @brief represents the NSMutableArray for the location results. */
@property (nonatomic) NSMutableArray * locationArray;

/*! @brief determines if a location search has taken place. */
@property (nonatomic) BOOL isLocationSearch;

/*! @brief represents the location object. */
@property (nonatomic) CLLocation *location;

/*! @brief represents the powered by Google image view. */
@property (nonatomic) UIImageView * poweredByGoogle;

/*! @brief determines if the directory footer is showing. */
@property (nonatomic) BOOL isDirectoryFooterShowing;
@end

@implementation TabSearchSalonsTableViewController

/*
 viewDidLayoutSubviews
 
 if is iphone 4 sets the searchSuggestionsTableView
 
 
 */
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (IS_iPHONE4) {
    
        CGRect newHeight = CGRectMake(0, self.searchBarHolder.frame.size.height+kStatusBarHeight, self.view.frame.size.height, self.view.frame.size.height-self.searchBarHolder.frame.size.height);
        self.searchSuggestionsTableView.frame = newHeight;
    }

}


/*
 resetTableViewAndArrays
 
 reset location array
 reset suggestionStrings
 reset filteredStrings
 reload searchSuggestionsTableView
 
 
 */
-(void)resetTableViewAndArrays{
    
    if (self.locationArray.count>0) {
        [self.locationArray removeAllObjects];
    }
       if (self.filteredSalons.count>0) {
        [self.filteredSalons removeAllObjects];
    }
  
    self.searchSuggestionsTableView.tableFooterView=nil;
    
    [self.searchSuggestionsTableView reloadData];
    
    
}


/*
 handleLocationSearchSuccessResult
 
 handles the parsing of the returned JSON data (jsonDictionary: NSDictionary)
 sets isFiltered to NO
 reloads the searchSuggestionsTableView
 
 */
- (void)handleLocationSearchSuccessResult:(NSDictionary *)jsonDictionary {
    
    NSArray *locationArray = [[jsonDictionary valueForKey:@"results"] valueForKey:@"formatted_address"];
    NSArray *latitudeLongitude = [[jsonDictionary valueForKey:@"results"] valueForKey:@"geometry"];
    
    int total = (int)locationArray.count;
   //NSLog(@"locationArray count: %lu", (unsigned long)locationArray.count);
    
    for (int i = 0; i < total; i++)
    {
       
        LocationObject *address= [[LocationObject alloc] init];
        
        
        
        address.fullAddress = [locationArray objectAtIndex:i];
        address.lat=[latitudeLongitude objectAtIndex:i][@"location"][@"lat"];
        address.lon=[latitudeLongitude objectAtIndex:i][@"location"][@"lng"];
        
        
                
        [self.locationArray addObject:address];
    }
    
    self.isFilteredSalons=NO;
    [self.searchSuggestionsTableView reloadData];
}

/*
 handleSalonSearchSuccessResult
 
 handles the parsing of the salon search results (jsonDictionary: NSDictionary)
 
 sets isFiltered to YES
 
 seachSuggestionsTableView - reload tableview
 
 */
- (void)handleSalonSearchSuccessResult:(NSDictionary *)jsonDictionary {
    self.filteredSalons = [NSMutableArray array];
    
    NSDictionary *dataDict = jsonDictionary;
    NSArray * salonsArray = dataDict[@"message"][@"results"];
    for (NSDictionary *sDict in salonsArray) {
        
        Salon *salon = [Salon salonWithID:[sDict[@"salon_id"] stringValue] ];
        if ([sDict[@"salon_latest_review"]count]!=0) {
            
            salon.hasReview=YES;
            SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:sDict[@"salon_latest_review"][@"user_name"] AndWithLastName:sDict[@"salon_latest_review"][@"user_last_name"]];
            //NSLog(@"first name %@",sDict[@"salon_latest_review"][@"user_name"]);
            salonReview.message = sDict[@"salon_latest_review"][@"message"];
            salonReview.review_image=sDict[@"salon_latest_review"][@"review_image"];
            salonReview.salon_rating=sDict[@"salon_latest_review"][@"salon_rating"];
            salonReview.stylist_rating=sDict[@"salon_latest_review"][@"stylist_rating"];
            salonReview.date_reviewed=sDict[@"salon_latest_review"][@"date_reviewed"];
            salonReview.user_avatar_url=sDict[@"salon_latest_review"][@"user_avatar"];
            salon.salonReview=salonReview;
            
        }
        if (sDict[@"salon_name"] !=[NSNull null]) {
            salon.salon_name=sDict[@"salon_name"];
        }
        if (sDict[@"salon_description"] !=[NSNull null]) {
            salon.salon_description = sDict[@"salon_description"];
        }
        if (sDict[@"salon_phone"] != [NSNull null]) {
            salon.salon_phone = sDict[@"salon_phone"];
        }
        if (sDict[@"salon_lat"] !=[NSNull null]) {
            salon.salon_lat = [sDict[@"salon_lat"] doubleValue];
        }
        if (sDict[@"salon_lon"] != [NSNull null]) {
            salon.salon_long = [sDict[@"salon_lon"] doubleValue];
        }
        if (sDict[@"salon_address_1"] != [NSNull null]) {
            salon.salon_address_1 = sDict[@"salon_address_1"];
        }
        if (sDict[@"salon_address_2"] != [NSNull null]) {
            salon.salon_address_2 = sDict[@"salon_address_2"];
        }
        if (sDict[@"salon_address_3"] != [NSNull null]) {
            salon.salon_address_3 = sDict[@"salon_address_3"];
        }
        if (sDict[@"city_name"] != [NSNull null]) {
            salon.city_name = sDict[@"city_name"];
        }
        if (sDict[@"county_name"]) {
            salon.country_name =sDict[@"county_name"];
        }
        if (sDict[@"country_name"] != [NSNull null]) {
            salon.country_name = sDict[@"country_name"];
        }
        if (sDict[@"salon_landline"] != [NSNull null]) {
            
            salon.salon_landline = sDict[@"salon_landline"];
        }
        if (sDict[@"salon_website"] !=[NSNull null]) {
            salon.salon_website = sDict[@"salon_website"];
        }
        if (sDict[@"salon_image"] != [NSNull null]) {
            salon.salon_image = sDict[@"salon_image"];
        }
        if (sDict[@"salon_type"] != [NSNull null]) {
            salon.salon_type = sDict[@"salon_type"];
        }
        if (sDict[@"rating"] != [NSNull null]) {
            salon.ratings = [sDict[@"rating"] doubleValue];
        }
        if (sDict[@"reviews"] != [NSNull null]) {
            salon.reviews = [sDict[@"reviews"] doubleValue];
        }
        if (sDict[@"salon_tier"] != [NSNull null]) {
            salon.salon_tier = [sDict[@"salon_tier"] doubleValue];
        }
        if (sDict[@"is_favourite"] !=[NSNull null]) {
            salon.is_favourite = [sDict[@"is_favourite"] boolValue];
        }
        if (sDict[@"distance"] !=[NSNull null]) {
            salon.distance = [sDict[@"distance"] doubleValue];
        }
        if (sDict[@"salon_description"] !=[NSNull null]) {
            salon.salon_description= sDict[@"salon_description"];
        }
        if (sDict[@"salon_images"]!=[NSNull null]) {
            
            NSArray *messageArray = sDict[@"salon_images"];
            
            for (NSDictionary * dataDict in messageArray) {
                
                NSString * imagePath = [dataDict objectForKey:@"image_path"];
                
                
                [salon.slideShowGallery addObject:imagePath];
            }
        }
        
        if (sDict[@"rating"]!=[NSNull null]) {
            salon.ratings = [sDict[@"rating"] doubleValue];
        }
        if (sDict[@"reviews"]!=[NSNull null]) {
            salon.reviews = [sDict[@"reviews"] doubleValue];
        }
        
        if ([sDict[@"salon_categories"] count]!=0) {
            salon.salonCategories =  @{
                                       @"Hair" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair"] boolValue]],
                                       @"Nails" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Nails"]boolValue]],
                                       @"Face" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Face"] boolValue]],
                                       @"Body" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Body"] boolValue]],
                                       @"Massage" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Massage"] boolValue]],
                                       @"Hair Removal" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair Removal"] boolValue]],
                                       };
        }
        
      
        
        if ([sDict[@"salon_opening_hours"] count]!=0) {
            
            NSArray * openingHours =sDict[@"salon_opening_hours"];
            for (NSDictionary* day in openingHours) {
         
                OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
                
                [salon.openingHours addObject:openingDay];
            }
            
            
        }
        
        /*
         Used to determine source type which refers to Phorest, iSalon etc
         */
        if (sDict[@"source_id"] != [NSNull null]) {
            salon.source_id = sDict[@"source_id"];
        }

        
        
        [self.filteredSalons addObject:salon];
        
        
    }
    
   
    self.isFilteredSalons=YES;//added
    [self.searchSuggestionsTableView reloadData];
    
    
}

#pragma mark - GCNetwork Manager
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    //NSLog(@"jsonDictionary %@",jsonDictionary);
    
    if (self.isLocationSearch) {
        self.isFilteredSalons=NO;
        [self handleLocationSearchSuccessResult:jsonDictionary];
        
        
    }
    else{
        
        [self handleSalonSearchSuccessResult:jsonDictionary];
    }
    
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    
    [self resetTableViewAndArrays];
    
    //reset tables and remove all objects
    //NSLog(@"Error jsonDictionary %@",jsonDictionary);
    if (!self.isLocationSearch) {
        
        [UIView showSimpleAlertWithTitle:@"No Salons in your area" message:@"" cancelButtonTitle:@"OK"];
        [self configureFooterView];
       
    }
}

#pragma mark - UIView LifeCycle

/*
 viewDidDisappears
 
 cancels networkManager
 
 
 */
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
   // [self.networkManager cancelTask];
    //self.networkManager.delegate=nil;//set to nil so it no longer responds to delegate - must test ****&&&**&&&@*&@&!*&*
    
    [self.networkManager destroy];
}

/*
 viewDidLoad
 
 sets upSearchBar for salonsSearchBar +locationSearchBar
 sets the delegate for the tableview
 
 dismiss keyboard on drag of tableview
 
 if searchTerm is greater than 0 in length - set the salonsSearchBar.text to the search bar
 
 sets up the networkController
 
 performs searchByCurrentLocation
 
 sets the locationSearchBar to CURRENT_LOCATION_TEXT
 
 if wasSearchPressed then make the salon search bar become first responder
 
 if catString and catId is not empty then set the searchbar to the catString
        performSearchWithSearchString
 
 sets the backgroundImageView
 
 sets the style of the search bar
 
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpSearchBar:self.salonsSearchBar];
    [self setUpSearchBar:self.locationSearchBar];
    
    self.searchSuggestionsTableView.delegate =self;
    self.searchSuggestionsTableView.dataSource=self;
    
    //search bar
    self.salonsSearchBar.delegate=self;
    
    
    self.searchSuggestionsTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    
    if ([self isBeingPresented]) {
        self.view.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.4];
        self.searchSuggestionsTableView.backgroundColor=[UIColor clearColor];
        self.isModal=YES;
    }
    
    //self.suggestedStrings = [[NSMutableArray alloc] initWithArray:[WSCore searchSuggestions]];
    
    if (self.searchTerm.length!=0) {
        self.salonsSearchBar.text=self.searchTerm;
    }
    
    self.networkManager =[[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    [self.searchSuggestionsTableView registerClass:[SearchTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    //self.searchSuggestionsTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    
    [self searchByCurrentLocation];
    
    self.locationSearchBar.text=CURRENT_LOCATION_TEXT;
    
    
    
    if (self.wasSearchPressed) {
        [self.salonsSearchBar becomeFirstResponder];
    }
    if (self.catString.length>0 && [self.catID intValue]!=0) {
        self.salonsSearchBar.text = self.catString;
        [self performSearchWithSearchString:@""];
    }
    
    CGRect frame = self.view.frame;
    frame.origin.y = kStatusBarHeight;
    frame.size.height= frame.size.height- kStatusBarHeight;
    UIImageView * backgroundImageView = [[UIImageView alloc] initWithFrame:frame];
    backgroundImageView.image = [UIImage imageNamed:kSearch_placeholder_Image_Name];
    
    UIView * trans = [[UIView alloc] initWithFrame:self.view.frame];
    trans.backgroundColor = kGoldenTintForOverView;
    [backgroundImageView addSubview:trans];
    [self.view insertSubview:backgroundImageView atIndex:0];

    
    self.searchBarHolder.backgroundColor = kBackgroundTintForSearch2;
    self.salonsSearchBar.backgroundColor = [UIColor clearColor];
    self.locationSearchBar.backgroundColor = [UIColor clearColor];
    self.searchSuggestionsTableView.backgroundColor = [UIColor clearColor];

    self.locationSearchBar.searchBarStyle=UISearchBarStyleMinimal;
    self.salonsSearchBar.searchBarStyle=UISearchBarStyleMinimal;
    
}

/*
 locationArrayResetAndInit
 
 sets locationArray to new Array
 add one onbject, LocationObject that is currentLocation
 
 */
- (void)locationArrayResetAndInit {
    
    self.locationArray = [NSMutableArray array];
    LocationObject *currentLocation = [[LocationObject alloc] init];
   
    if ([[User getInstance] fetchUsersLocation]!=nil) {
        currentLocation.lat=[NSString stringWithFormat:@"%f",[[User getInstance] fetchUsersLocation].coordinate.latitude];
        currentLocation.lon=[NSString stringWithFormat:@"%f",[[User getInstance] fetchUsersLocation].coordinate.longitude];
    }
    
    currentLocation.fullAddress=CURRENT_LOCATION_TEXT;
    currentLocation.isCurrentLocation=YES;
    
    [self.locationArray addObject:currentLocation];
    
}

/*
 removeFooterView
 
 */
-(void)removeFooterView{
    self.searchSuggestionsTableView.tableFooterView=nil;
}


/*
 addFooterView
 
 creates a footerView
 adds poweredByGoogle imageView
 adds footerView to tableview
 */
- (void)addFooterView {
    //location search
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    footerView.backgroundColor=[UIColor clearColor];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"powered-by-google-on-non-white"]];
    imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    imageView.frame = CGRectMake(110,10,110,20);
    [footerView addSubview:imageView];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.center=CGPointMake(footerView.center.x, imageView.frame.origin.y);
    imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.poweredByGoogle = imageView;
    self.poweredByGoogle.hidden=YES;
    
    
    self.searchSuggestionsTableView.tableFooterView=footerView;
    
    self.isDirectoryFooterShowing=NO;
}

/*
 removeBackgroundView
 
 sets the searchSuggestionsTableView background view to nil
 
 */
-(void)removeBackgroundView{
    self.searchSuggestionsTableView.backgroundView =nil;
}


-(void)addBackgroundView{
    
    UIView * backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    backgroundView.backgroundColor = kBackgroundTintForSearch2;
    
    self.searchSuggestionsTableView.backgroundView = backgroundView;
    
}

/*
 searchByCurrentLocation
 
 ** does a small bit more **
 
 adds footerView
 
 adds Cancel UIBarButtonItem
 updates title 
 
 adds Search UIBarButtonItem
 
 setUpSearchBar to update the apperance of locationSearchBar
 */
-(void)searchByCurrentLocation{
    
    [self addFooterView];
    
    //v2
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                kWhatSalonBlue,NSForegroundColorAttributeName,
                                nil];
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
    self.title=@"Search";
    

  
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(searchButtonPressed)];
    NSDictionary *attributes2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                 kWhatSalonBlue,NSForegroundColorAttributeName,
                                 nil];
    self.navigationItem.rightBarButtonItem = submitButton;
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:attributes2 forState:UIControlStateNormal];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self setUpSearchBar:self.locationSearchBar];
    
    
    //iterate through subviews
    for (UIView *subView in self.locationSearchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                //updates the imageview tint
                UIImageView *iconView = (id)searchBarTextField.leftView;
                iconView.image = [iconView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                iconView.tintColor = kWhatSalonBlue;
                //set font color here
                searchBarTextField.textColor = kWhatSalonBlue;
                searchBarTextField.font=[UIFont boldSystemFontOfSize:14.0f];
                searchBarTextField.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.8];
                
                break;
            }
        }
    }
    
    
    [self locationArrayResetAndInit];
    
    
    self.salonsSearchBar.showsCancelButton=NO;
    
    //adds extra white space after search salons
    self.salonsSearchBar.placeholder=@"Search Salons                                        ";
    
    self.locationSearchBar.delegate=self;
    
    //init cllocation with users location
    if ([[User getInstance] fetchUsersLocation]!=nil) {
        self.location = [[CLLocation alloc]initWithLatitude:[[User getInstance] fetchUsersLocation].coordinate.latitude
                                                  longitude:[[User getInstance] fetchUsersLocation].coordinate.longitude];
    }
    
    
}


/*
 setUpSearchBar
 
 updates appearance of searchbar
 
 
 */
-(void)setUpSearchBar:(UISearchBar *)searchBar{
    
    
    
    searchBar.backgroundColor=[UIColor clearColor];
    
    // Search Bar Cancel Button Color
    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:self.view.tintColor];
    
    // set Search Bar texfield corder radius
    UITextField *txfSearchField = [searchBar valueForKey:@"_searchField"];
    txfSearchField.layer.cornerRadius = 3.0f;
    
    searchBar.backgroundImage=[UIImage new];
    [self.searchBarHolder bringSubviewToFront:searchBar];
    
    
    UIView * grayLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.searchBarHolder.frame.size.height-0.5, self.searchBarHolder.frame.size.width, 0.5)];
    grayLine.backgroundColor=[UIColor lightGrayColor];
    [self.searchBarHolder addSubview:grayLine];
    [self.searchBarHolder bringSubviewToFront:grayLine];
    
    if (searchBar==self.locationSearchBar) {
        [self.locationSearchBar setImage:[UIImage imageNamed:@"map_pin_filled"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
        
    }
    
}

/*
 cancel
 
 handles cancel action
 */
-(void)cancel{
    
    //[self.networkManager cancelTask];
    //self.networkManager.delegate=nil;
    //[self.networkManager destroy];
    
     self.tabBarController.tabBar.hidden=NO;
    if([self.delegate respondsToSelector:@selector(didDismissSearchSalonsTable)]){
        [self.delegate didDismissSearchSalonsTable];
    }
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
   
}


/*
 searchForLocation
 
 performs google gecode search for a location that is typed in the locationSearchBar
 
 
 */
- (void)searchForLocation {
    
    if (self.isDirectoryFooterShowing) {
        [self addFooterView];//default footerview
    }
    [self locationArrayResetAndInit];

    
    //perform search
    //check if error or has failed
    //iterate over results
        //convert CGGoogleAutoCompleteObject to LocationObject - name, id
        //add to array
    //reload content - look at success message from networkloader
    
    GCGoogleAutoComplete * placesClient = [[GCGoogleAutoComplete alloc] init];
    [placesClient autoCompleteWithString:self.locationSearchBar.text callBack:^(NSArray *results, NSError *error, BOOL hasFailed, NSString * googleStatusCode) {
        
        
        /*
         If failure has occured then no location from Google was recieve
            reset locationSearchbar to CURRENT_LOCATION_TEXT
         
            if a googleStatusCode is returned and does not read OK
                check googleStatusCode
                show error
            else the failure is not determend
                show alert
         
         else
            got a location from Google
            iterate through results
            add objects
            set isFileteredSalons to NO as its the Locations objects that have been filtered
         
         reload table view
         hide network indicator
         
         
         */
        if (hasFailed==YES) {
            
            NSLog(@"Failure has occured");
           
            
            self.locationSearchBar.text = CURRENT_LOCATION_TEXT;
            
            if (googleStatusCode.length>0 && ![googleStatusCode isEqualToString:@"OK"]) {
                
                NSString * status;
                if ([googleStatusCode isEqualToString:@"ZERO_RESULTS"]) {
                    //indicates that the search was successful but returned no results. This may occur if the search was passed a bounds in a remote location.
                    status=@"No Results found";
                }
                else if ([googleStatusCode isEqualToString:@"OVER_QUERY_LIMIT"])
                {
                    //indicates that you are over your quota.
                    status=@"Error Code 1";
                }
                else if ([googleStatusCode isEqualToString:@"REQUEST_DENIED"]){
                    //indicates that your request was denied, generally because of lack of an invalid key parameter.
                    status=@"Error Code 2";
                }
                else if ([googleStatusCode isEqualToString:@"INVALID_REQUEST"]){
                    //generally indicates that the input parameter is missing.
                    status = @"Error Code 3";
                }
                
                
                [UIView showSimpleAlertWithTitle:@"Google Search" message: [NSString stringWithFormat:@"A Google Search Error occurred. Reason: %@",status] cancelButtonTitle:@"OK"];
                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                    
                
            }
            else{
                
                NSLog(@"Failure not determined");
                [UIView showSimpleAlertWithTitle:@"Google Search" message:@"A Google Search Error occurred." cancelButtonTitle:@"OK"];
            }
            

        }
        else{
            
            for (GCGoogleAutoCompleteObject * autoCompleteObject in results) {
                
                //NSLog(@"obj name %@ place id %@", autoCompleteObject.name, autoCompleteObject.placeID);
                LocationObject * loc = [[LocationObject alloc] init];
                loc.fullAddress = autoCompleteObject.name;
                loc.place_id = autoCompleteObject.placeID;
                loc.title = autoCompleteObject.title;
                loc.subtitle = autoCompleteObject.subtitle;
                [self.locationArray addObject:loc];
                
            }
            
            self.isFilteredSalons=NO;
            
        }
        
        [self.searchSuggestionsTableView reloadData];
        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

        
       
    }];

}

/*
 searchButtonPressed
 
 action when the search button is pressed
 
 */
-(void)searchButtonPressed{
}

/*
 searchForSalon
 
 params: text takes in string
 
 creates url 
    sets 15 per page
    handles free text typing of face,body etc
        if the search string is equal to Body then it uses the corresponding catID
    appends parameters
    sends user id if user is logged in
 
 starts network request
 
 */
- (void)searchForSalon:(NSString *)name location:(CLLocation*) loc  {
    
    if (self.isDirectoryFooterShowing) {
        [self addFooterView];//default footer view
    }
    
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSearch_Salons_URL]];
    NSString * params = [NSString stringWithFormat:@"per_page=15"];
  
    
    NSString * categoryId = @"";
    
    
    
    /*
     Updated to handle free text typing of face, body etc
     
     */
    if ( name && [name caseInsensitiveCompare:@"Body"] == NSOrderedSame) {
        categoryId = @"6";
        NSLog(@"Is body");
    }
    else if (name && [name caseInsensitiveCompare:@"Face"] == NSOrderedSame) {
        categoryId = @"5";
        
    }else if (name && [name caseInsensitiveCompare:@"Hair"] == NSOrderedSame) {
        categoryId = @"1";
        
    }
    else if (name && [name caseInsensitiveCompare:@"Hair Removal"] == NSOrderedSame) {
       categoryId = @"2";
        
    }
    else if (name && [name caseInsensitiveCompare:@"Massage"] == NSOrderedSame) {
       categoryId = @"4";
        
    }
    else if (name && [name caseInsensitiveCompare:@"Nails"] == NSOrderedSame) {
        categoryId = @"3";
        
    }
    else{
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&search_string=%@",@""]];
    }
    
    if (categoryId.length>0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&filter_category_id=%@",categoryId]];
    }
    
    
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%f&longitude=%f",loc.coordinate.latitude,loc.coordinate.longitude]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%@",kWorldCircumference]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&tier_1=1"]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_2=1"]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_3=1"]];
    
    if ([[User getInstance] isUserLoggedIn]) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken ]]];
    }
    
   params = [params stringByAppendingString:@"&client_version=3"];
  
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
}

/*
 performSearchWithSearchString
 param: text NSString
 
    if myTimer is not nil   
        if the timer is valid, set the text to myTimer userInfo
    else    
        set text as the salonsSearchBarText
 
 if isLocationSearch
    searchForLocation
 
 else
    searchForSalon
 
 
 */

-(void)performSearchWithSearchString: (NSString *) text{
    
    if (self.myTimer) {
        if ([self.myTimer isValid])
        {
            text = self.myTimer.userInfo;
        }
        
    }
    else{
        text=self.salonsSearchBar.text;
    }
    
    if (self.isLocationSearch) {
        
        [self searchForLocation];
        
        
    }
    else{
        
        
    }
}

/*
 viewWillAppear
 
 sets transparent navigaiton bar
 sets the status bar color to black
 
 set the navigation bar tint color, barTintCOlr, and titleTextAttributes
 ensures the navigation bar is no hidden
 
 hides the tab bar
 
 */
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [WSCore transparentNavigationBarOnView:self];
    [WSCore statusBarColor:StatusBarColorBlack];
    
    self.navigationController.navigationBar.barTintColor = kWhatSalonBlue;
    self.navigationController.navigationBar.tintColor = kWhatSalonBlue;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonBlue}];
    
    self.navigationController.navigationBarHidden=NO;

    self.tabBarController.tabBar.hidden=YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

#pragma mark - UISearchBar
/*
 searchBarShouldBeginEditing
 
 the searchBar that is being edited
 
 if locationSearchBar
    addBackgroundView
    set poweredByGoogle imageview to visible
    sets isFiltered to NO
    reloads the tableview
 
 else
    remove the background view
 
    if the salonSearchBar text is equal to one of the categories then set the seachBar text to empty
 
    hide the poweredByGoogle imageView
    set isLocationSearch to NO
    sets isFiltered to YES
    if locationSearchBar text is empty
        set the location search bar text to current location text
        reset the location array
    reload tableview
    
 
 
 
 */
- (BOOL)searchBarShouldBeginEditing:(UISearchBar*)searchBar {
    
    if (self.isDirectoryFooterShowing) {
        [self addFooterView];//default footerview
    }
    
    if (searchBar==self.locationSearchBar) {
        
        //ensures at least current location object is present in the array
        if (self.locationArray.count==0) {
            [self locationArrayResetAndInit];
        }
        
        [self addBackgroundView];
        self.poweredByGoogle.hidden=NO;
        self.isLocationSearch=YES;
        self.isFilteredSalons=NO;//added
        [self.searchSuggestionsTableView reloadData];
        
    }
    else{
        
        [self removeBackgroundView];
        
        if ([self.salonsSearchBar.text isEqualToString:@"Body"]||[self.salonsSearchBar.text isEqualToString:@"Face"]||[self.salonsSearchBar.text isEqualToString:@"Hair Removal"]||[self.salonsSearchBar.text isEqualToString:@"Hair"]|| [self.salonsSearchBar.text isEqualToString:@"Massage"]||[self.salonsSearchBar.text isEqualToString:@"Nails"] ) {
            searchBar.text=@"";
            self.catID=nil;
            
        }
        
        self.poweredByGoogle.hidden=YES;
        
        self.isLocationSearch=NO;
        self.isFilteredSalons=YES;//added
        
        if (self.locationSearchBar.text.length==0) {
           
            self.locationSearchBar.text = CURRENT_LOCATION_TEXT;
            [self locationArrayResetAndInit];
        }
        [self.searchSuggestionsTableView reloadData];
    }
    return YES;
}

#pragma mark - UISearchBar delegate and datasource methods

/*
    shouldChangeTextInRange
    if locationSearchBar
        if the location search bar text is equal to the CURRENT_LOCATION_TEXT constant
            then set the locationSearchBar text to empty
 
 */

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    if (searchBar==self.locationSearchBar) {
        if ([self.locationSearchBar.text isEqualToString:CURRENT_LOCATION_TEXT]) {
            self.locationSearchBar.text=@"";
        }
    }
    
    
    return YES;
}

/*
 textDidChange
 
 if the searchBar text is, or less than 3, then isFiltered is set to NO
    if myTimer is not nil
        if the timer is valid, then invalidate it
        set myTimer to nil
        hides networkActivityIndicator
 
 if search bar text is greater than 3   
 
    if it is not a location search (isLocationSearch) then set isFiltered to YES
    
    if myTimer is not nil
        if the timer is valid, then invalidate it
        set the timer to nil
 
    make networkActivityIndicatorVisible
    set myTimer to schedule the timer after 1 second and then perfomrSearchWithSearchString
 
 
 
 */
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
   
    if (searchBar.text ==0 ||searchBar.text.length<3) {
        
        self.isFilteredSalons=NO;
        if (self.myTimer)
        {
            if ([self.myTimer isValid])
            {
                [self.myTimer invalidate];
            }
            self.myTimer=nil;
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
        }
        
    }else if (searchBar.text.length>=3) {
        
        
        if (!self.isLocationSearch) {
            self.isFilteredSalons=YES;
        }
        
        
        if (self.myTimer)
        {
            if ([self.myTimer isValid])
            {
                
                [self.myTimer invalidate];
            }
            self.myTimer=nil;
        }
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(performSearchWithSearchString:) userInfo:searchText repeats:NO];
        
        
        
    }
}

/*
 searchBarSearchButtonClick 
    endEditing
    
    performSearchWithSearchString
 */

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.view endEditing:YES];
   
    [self performSearchWithSearchString:searchBar.text];
    
}

/*
 searchBarCancelButton
 reset the delegate and datasrouce of searchSuggestionsTableView
 sets the salonsSearchBar delegate to self ** I am unsure of why this is in place **
 
 pop view controller with custom transition
 */
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    self.searchSuggestionsTableView.delegate=nil;
    self.searchSuggestionsTableView.dataSource=nil;
    self.salonsSearchBar.delegate=self;
    
    if([self isBeingPresented] ||self.isModal){
       
        [self dismissViewControllerAnimated:YES completion:^{
            self.isModal=NO;
        }];
    }
    else{
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.type = kCATransitionFade;
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController popViewControllerAnimated:NO];
        
    }
    
    
}
#pragma mark - Table view delegate and data source
/*
 searchCell
 searchCell appearance
 */
- (UITableViewCell *)searchCell:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [self.searchSuggestionsTableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = [self.filteredSalons objectAtIndex:indexPath.row];
    
    
    return cell;
}


- (void)requestAndSetUsersLocation {
    //************ Get Current Location **************
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             
                                             
                                             
                                             if (status == INTULocationStatusSuccess||status == INTULocationStatusTimedOut) {
                                                 /*
                                                 // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                 // currentLocation contains the device's current location.
                                                 
                                                 //OR
                                                 
                                                 // Wasn't able to locate the user with the requested accuracy within the timeout interval.
                                                 // However, currentLocation contains the best location available (if any) as of right now,
                                                 // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
                                                 */
                                                 
                                                 
                                                 self.location = currentLocation;
                                                 
                                                 if ([[User getInstance] isUserLoggedIn]) {
                                                     [[User getInstance] updateUsersLocationWithLocation:currentLocation];
                                                     [WSCore updateUsersCityAndCountry:currentLocation];
                                                 }
                                                 else{
                                                     
                                                     [[User getInstance] updateGuestUsersLocation:currentLocation];
                                                 }

                                                
                                                 NSLog(@"Now Search");
                                                 [self.salonsSearchBar becomeFirstResponder];
                                                 self.isLocationSearch=NO;
                                                 
                                                 
                                                 
                                             }
                                             else {
                                                 // An error occurred, more info is available by looking at the specific status returned.
                                                 
                                                 switch (status) {
                                                         
                                                     case INTULocationStatusServicesDenied:
                                                         
                                                        
                                                         
                                                         if (IS_IOS_8_OR_LATER) {
                                                             
                                                             UIAlertController * alert=   [UIAlertController
                                                                                           alertControllerWithTitle:@"Location Error"
                                                                                           message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app"
                                                                                           preferredStyle:UIAlertControllerStyleAlert];
                                                             
                                                             UIAlertAction* ok = [UIAlertAction
                                                                                  actionWithTitle:@"Open Settings"
                                                                                  style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action)
                                                                                  {
                                                                                      
                                                                                      if (&UIApplicationOpenSettingsURLString != NULL) {
                                                                                          NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                                          [[UIApplication sharedApplication] openURL:appSettings];
                                                                                      }
                                                                                      
                                                                                  }];
                                                             UIAlertAction* cancel = [UIAlertAction
                                                                                      actionWithTitle:@"Cancel"
                                                                                      style:UIAlertActionStyleDestructive
                                                                                      handler:^(UIAlertAction * action)
                                                                                      {
                                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                          
                                                                                      }];
                                                             
                                                             [alert addAction:cancel];
                                                             [alert addAction:ok];
                                                             
                                                             [self presentViewController:alert animated:YES completion:nil];
                                                             
                                                             
                                                         }
                                                         else{
                                                             
                                                             [UIView showSimpleAlertWithTitle:@"Location Error" message:@"You have denied WhatSalon access to location services. In order to use this app, please accept access. " cancelButtonTitle:@"OK"];
                                                         }
                                                         break;
                                                         
                                                         
                                                     case INTULocationStatusServicesRestricted:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location services are restricted for WhatSalon. e.g (parental controls corporate policy, etc)" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                     case INTULocationStatusServicesDisabled:
                                                         
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app. Settings->Privacy->LocationServices" cancelButtonTitle:@"OK"];
                                                         
                                                         
                                                         break;
                                                         
                                                     case INTULocationStatusError:
                                                         
                                                         
//                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"An error occurred while using the system location services." cancelButtonTitle:@"OK"];
                                                         break;
                                                     default:
                                                         break;
                                                 }
                                                 
                                                 
                                             }
                                             
                                         }];
}

/*
 didSelectLocationCell  
    if filteredSalons is not empty
        remove all objects
 
    reload
 
 if the lcoation array is greater than 0
    fetchLocationObject
    set the locationSearchBar text to the location objects fullAddress
    allocate self.location
    make salonsSearchBar become firstResponder
    set isLocationSearch to NO as location search is no finished
    search for salon using the salonsSearchBar text
 
 */
- (void)didSelectLocationCell:(NSIndexPath *)indexPath {
    
    //to reset filtered salons based to prep for new search
    if (self.filteredSalons.count>0) {
        [self.filteredSalons removeAllObjects];
    }
    
    [self.searchSuggestionsTableView reloadData];
    
    if (self.locationArray.count>0) {
        

        //get location lat and long
        /*
         if current location use lat and long and assign to self.location
         else 
            perform search to get details
                in call back method set self.location to location
         */
        LocationObject * loc = [self.locationArray objectAtIndex:indexPath.row];
        if (loc.isCurrentLocation) {
            
            //users location
  
            [self requestAndSetUsersLocation];
        
           
        }
        else{
            
            GCGoogleAutoComplete * googlePlacesClient = [[GCGoogleAutoComplete alloc] init];
            
            [googlePlacesClient getPlacesDetailsWithID:loc.place_id callBack:^(NSDictionary * jsonDict, NSError *error, BOOL hasFailed) {
               
                
                
                loc.lat = jsonDict[@"result"][@"geometry"][@"location"][@"lat"];
                loc.lon = jsonDict[@"result"][@"geometry"][@"location"][@"lng"];
                self.location = [[CLLocation alloc] initWithLatitude:[jsonDict[@"result"][@"geometry"][@"location"][@"lat"] doubleValue] longitude:[jsonDict[@"result"][@"geometry"][@"location"][@"lng"] doubleValue]];
                
          
                [self.salonsSearchBar becomeFirstResponder];
                self.isLocationSearch=NO;
                
            }];
            
        }
        
        self.locationSearchBar.text = loc.fullAddress;
        
        
    }
}


/*
 didSelectSalonCell
    destination when the cell is selected
 */
- (void)didSelectSalonCell:(NSIndexPath *)indexPath {
   
    
    TabNewSalonDetailViewController * salonVc = [self.storyboard instantiateViewControllerWithIdentifier:@"newSalonDetailVC"];//new viewcontroller instead of tableviewcontroller
    if (self.filteredSalons.count>0) {
        
        if ([[self.filteredSalons objectAtIndex:indexPath.row] isKindOfClass:[Salon class]]) {
            salonVc.salonData = [self.filteredSalons objectAtIndex:indexPath.row];
            salonVc.unwindBackToSearchFilter=YES;
            salonVc.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:salonVc animated:YES];
            
        }
    }
    
}

/*
 didSelectRowAtIndexPath    
    isLocationSearch
        then call didSelectLocationCell
    and
        the call didSelectSalonCell
 
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (self.isLocationSearch) {
        
        
        [self didSelectLocationCell:indexPath];
        

        
    }
    else{
        
        [self didSelectSalonCell:indexPath];
    }
    
    
}

/*
 locationCellSetUp
 
    get index of selected cell
 
 if the LocationObject is the isCurrentLocation
    set the current location cell style with blue tint and image
 else
    standard cell
 
 set the textLabel to the locationObjects fullAddress
 
 */
- (UITableViewCell *)locationCellSetUp:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    UITableViewCell * cell;
    
    
    LocationObject * loc = [self.locationArray objectAtIndex:indexPath.row];
    
    
    
    if (loc.isCurrentLocation && indexPath.row==0) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"CurrentLocationCell" forIndexPath:indexPath];
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 20, 20)];
        imgView.backgroundColor=[UIColor clearColor];
        [imgView.layer setCornerRadius:8.0f];
        [imgView.layer setMasksToBounds:YES];
        [imgView setImage:[UIImage imageNamed:@"location"]];
        imgView.image = [imgView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [imgView setTintColor:kWhatSalonBlue];
        [cell.contentView addSubview:imgView];
        imgView.center=CGPointMake(imgView.center.x, cell.contentView.center.y);
        cell.imageView.image = [UIImage new];
        cell.textLabel.textColor=kWhatSalonBlue;
        cell.textLabel.text=loc.fullAddress;

        
        
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"LocationCell" forIndexPath:indexPath];
        cell.textLabel.text = loc.title;
        cell.detailTextLabel.text = loc.subtitle;
        cell.imageView.image=nil;
        cell.textLabel.textColor=[UIColor blackColor];
        
       
        
        
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor=[UIColor clearColor];
    cell.textLabel.numberOfLines=0;
    
    cell.backgroundView.backgroundColor=[UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
       return cell;
}


/*
 cellForRowAtIndexPath
 
if isLocationSearch
    set up the location cell
    return location cell
 
 
 if isFilteredSalons is equal to yes    
    then grab salon from filteredSalons
    setUpTheCell
 
 sets the backgroundColor tint
 
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isLocationSearch) {
        
        
        UITableViewCell *cell;
        cell = [self locationCellSetUp:indexPath tableView:tableView];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.backgroundView=nil;
        return cell;
    }
    
    
    
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (self.isFilteredSalons == YES) {
        
        Salon * salon = nil;
        salon = [self.filteredSalons objectAtIndex:indexPath.row];
   
        cell.searchTerm = self.salonsSearchBar.text;
        [cell setUpCellWithSalon:salon];
        
    }
    
    cell.backgroundColor = kBackgroundTintForSearch2;
    cell.backgroundView=nil;
    return cell;
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

/*
 numberOfRowsInSection
 
 if isLocationSearch
    return the size of the location array
 
 else
    if isFilteredSalons
        return the filteredSalons size
 
 
 
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.isLocationSearch) {
        
        return self.locationArray.count;
    }
    else{
        
        if (self.isFilteredSalons) {
            return self.filteredSalons.count;
        }
    }
    
    return 0;
    
}

/*
 heightForRowAtIndexPath
    if isFilteredSalons
        then it is a Salon search - so make the cell 100 in height
 
    else a standard cell height is ok
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(self.isFilteredSalons){
        return 100.0f;
        
    }
    return 55;
}

/*
 configureFooterView
 
 creates a footerver view if there are no results for what the user is looking for
 */
- (void)configureFooterView{
    
   
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 160)];
    footerView.backgroundColor = [UIColor clearColor];
    footerView.backgroundColor = kBackgroundTintForSearch2;
    UILabel * label =  [[UILabel alloc] initWithFrame:CGRectMake(15, 8, self.view.frame.size.width-30, 75)];
    label.numberOfLines=0;
    label.textAlignment=NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:15.0f];
    label.textColor = [UIColor lightGrayColor];
    [footerView addSubview:label];
    
    label.text=@"Can't find what you're looking for? \nTry searching the directory of salons near you.";
        
        
    UIButton * dirBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    dirBtn.frame = CGRectMake(0, 91, 140, 30);
    [dirBtn setTitle:@"View Directory" forState:UIControlStateNormal];
    [footerView addSubview:dirBtn];
    [dirBtn addTarget:self action:@selector(showDirectoryView) forControlEvents:UIControlEventTouchUpInside];
    dirBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    dirBtn.center = CGPointMake(footerView.center.x, dirBtn.center.y);
    
    
    self.searchSuggestionsTableView.tableFooterView=footerView;
    
    
    [WSCore addTopLine:footerView :[UIColor lightGrayColor] :0.5 WithYvalue:footerView.frame.size.height];
    
     self.isDirectoryFooterShowing=YES;
}
/*
 showDirectoryView
 
 goes to the directory view
 
 */
-(void)showDirectoryView{
    TabDirectoryViewController * dir = [self.storyboard instantiateViewControllerWithIdentifier:@"dirVC"];
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:dir];
    [self presentViewController:nav animated:YES completion:nil];
}

@end
