//
//  SalonAvailableTime.m
//  whatsalon
//
//  Created by Graham Connolly on 01/12/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonAvailableTime.h"

@implementation SalonAvailableTime


-(NSString *) formattedDate : (NSString *) date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * tempDate = [dateFormatter dateFromString: date];
    
    [dateFormatter setDateFormat:@"EE MMM,dd"];
    return [dateFormatter stringFromDate:tempDate];
    

}

-(NSString *) getTimeFromDate : (NSString *) date{
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
   
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    

    NSDate * tempDate = [dateFormatter dateFromString: date];
    
    [dateFormatter setDateFormat:@"h:mm a"];
    
    
    return [dateFormatter stringFromDate:tempDate];
}
@end
