//
//  CallOutConfirmationScreenViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 10/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CallOutConfirmationScreenViewController.h"
#import "FUIButton.h"
#import "CalloutCongratulationsPageViewController.h"


//confirmedBookingCalloutVC
@interface CallOutConfirmationScreenViewController ()
@property (weak, nonatomic) IBOutlet UIView *addressHolderView;
@property (weak, nonatomic) IBOutlet UILabel *callOutAddress;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UIView *bookingInfoHolderView;
@property (weak, nonatomic) IBOutlet UIView *priceHolderView;
@property (weak, nonatomic) IBOutlet UILabel *service;
@property (weak, nonatomic) IBOutlet UILabel *therapist;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *noOfHeads;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *bookingPrice;
@property (weak, nonatomic) IBOutlet UILabel *serviceDetail;
@property (weak, nonatomic) IBOutlet UILabel *therapistName;
@property (weak, nonatomic) IBOutlet UILabel *bookingDate;
@property (nonatomic) UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *bookingNumber;
@property (nonatomic) FUIButton * confirmButton;

@property(nonatomic) NSArray * xArray;
@property (nonatomic) NSArray *yArray;

@property(nonatomic) NSInteger count;
@property (nonatomic,assign) BOOL hasStarted;
@property (nonatomic,assign) BOOL isFinished;
@property (nonatomic,assign) BOOL isLogoTraced;



@end

@implementation CallOutConfirmationScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:self.backgroundImageView];
    [self.view sendSubviewToBack:self.backgroundImageView];
    if (IS_IOS_8_OR_LATER) {
        self.backgroundImageView.image=[UIImage imageNamed:@"salon1.jpg"];
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = self.backgroundImageView.bounds;
        
        [self.backgroundImageView addSubview:visualEffectView];
    }
    else{
        UIView * blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        blackView.backgroundColor=[UIColor blackColor];
        blackView.alpha=0.9;
        [self.backgroundImageView insertSubview:blackView atIndex:0];
        self.backgroundImageView.image =[UIImage imageNamed:@"salon1.jpg"];
        
    }
    
    self.addressHolderView.backgroundColor=[UIColor clearColor];
    self.bookingInfoHolderView.backgroundColor=[UIColor clearColor];
    self.priceHolderView.backgroundColor=[UIColor clearColor];
    
    
    self.callOutAddress.textColor=[UIColor lightGrayColor];
    self.address.textColor = [UIColor whiteColor];
    self.date.textColor = [UIColor lightGrayColor];
    self.bookingDate.textColor = [UIColor whiteColor];
    self.service.textColor = [UIColor lightGrayColor];
    self.serviceDetail.textColor=[UIColor whiteColor];
    self.therapist.textColor=[UIColor lightGrayColor];
    self.therapistName.textColor=[UIColor whiteColor];
    self.noOfHeads.textColor = [UIColor lightGrayColor];
    self.bookingNumber.textColor=[UIColor whiteColor];
    self.price.textColor=[UIColor lightGrayColor];
    self.bookingPrice.textColor= [UIColor whiteColor];
    
    [WSCore addTopLine:self.addressHolderView :[UIColor whiteColor] :0.5];
    [WSCore addTopLine:self.bookingInfoHolderView :[UIColor whiteColor] :0.5];
    [WSCore addTopLine:self.priceHolderView :[UIColor whiteColor] :0.5];
    
    [WSCore addBottomLine:self.priceHolderView :[UIColor whiteColor]];
    
    self.title=@"Confirm Booking";

    self.instructionsLabel.textColor=[UIColor lightGrayColor];
    
    self.confirmButton = [[FUIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-44, self.view.frame.size.width, 44)];
    self.confirmButton.buttonColor = kWhatSalonBlue;
    self.confirmButton.shadowColor = kWhatSalonBlueShadow;
    self.confirmButton.shadowHeight = 3.0f;
    self.confirmButton.cornerRadius = kCornerRadius;
    [self.confirmButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.confirmButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];
    [self.confirmButton setTitle:@"Confirm and Pay €60" forState:UIControlStateNormal];
    [self.confirmButton addTarget:self action:@selector(goToCalloutConfirm) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.confirmButton];
    
    if (IS_iPHONE4) {
        self.traceImageView.hidden=YES;
        self.tutorialImageView.hidden=YES;
        self.confirmButton.hidden=NO;
        self.confirmButton.alpha=1.0;
        self.instructionsLabel.hidden=YES;
    }
    else{
        self.confirmButton.hidden=YES;
        self.confirmButton.alpha=0.0;
        
        
        self.tutorialImageView.alpha=0.8;
        self.traceImageView.userInteractionEnabled=YES;
        
        self.tutorialImageView.animationImages = [NSArray arrayWithObjects:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0001" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0002" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0003" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0004" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0005" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0006" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0007" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0008" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0009" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0010" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0011" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0022" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0023" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0024" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0025" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0026" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0027" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0028" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0029" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0030" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0031" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0032" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0033" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0034" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0035" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0036" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0037" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0038" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0039" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0040" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0041" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0042" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0043" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0044" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0045" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0046" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0047" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0048" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0049" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0050" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0051" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0052" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0053" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0054" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0055" ofType:@"png"]],nil];
        
        
        
        
        
        
        
        self.tutorialImageView.animationDuration=1.5f;
        self.tutorialImageView.animationRepeatCount=1;
        
        self.xArray = @[@253.0f,
                        @258.0f,@258.0f,@262.0f,@277.0f,@266.0f,@266.0f,@265.0f,
                        @264.0f,@260.0f,@255.0f,@250.0f,@242.0f,@224.0f,@210.0f,@188.0f,
                        @180.0f,@172.0f,@156.0f,@144.0f,@134.0f,@116.0f,@98.0f,@80.0f,@67.0f,
                        @65.0f,@62.0f,@62.0f,@63.0f,@63.0f,@67.0f,
                        @71.0f,@74.0f,@79.0f,@90.0f,@97.0f,@104.0f,@118.0f,@126.0f,
                        @139.0f,@149.0f,@158.0f,@171.0f,@177.0f,@188.0f,@191.0f,@199.0f,
                        @204.0f,@206.0f,@207.0f,@205.0f,@204.0f,@200.0f,@194.0f,
                        @184.0f,@174.0f,@161.0f,@147.0f,@136.0f,@128.0f,@118.0f,@103.0f];
        
        self.yArray = @[@139.0f,@127.0f,@119.0f,@107.0f,@102.0f,@94.0f,@83.0f,@74.0f,@67.0f,
                        @63.0f,@53.0f,@45.0f,@37.0f,@29.0f,@23.0f,@17.0f,@17.0f,@18.0f,@20.0f,@24.0f,@21.0f,
                        @31.0f,@45.0f,@58.0f,@71.0f,@83.0f,@97.0f,
                        @114.0f,@130.0f,@138.0f,@147.0f,@164.0f,
                        @172.0f,@180.0f,@198.0f,@208.0f,@214.0f,@230.0f,@239.0f,@248.0f,@255.0f,
                        @266.0f,@276.0f,@284.0f,@296.0f,@299.0f,@310.0f,@324.0f,@336.0f,
                        @344.0f,@354.0f,@364.0f,@375.0f,@385.0f,
                        @386.0f,@396.0f,@396.0f,@396.0f,@395.0f,@389.0f,@383.0f,@366.0f];
        
        self.count = 0;
        
        self.isFinished=NO;
        
        [self performSelector:@selector(animateTutorial) withObject:self afterDelay:1.5f];
        
        [self performSelector:@selector(animateTutorial) withObject:self afterDelay:6.5f];
        
    }

    self.address.numberOfLines=0;
    
}

-(void)goToCalloutConfirm{
    
    CalloutCongratulationsPageViewController * cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"confirmedBookingCalloutVC"];
    [self.navigationController pushViewController:cvc animated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [super touchesMoved:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self.traceImageView];
    
    self.hasStarted=YES;
    CGFloat xFloat = currentPoint.x;
    CGFloat yFloat = currentPoint.y;
    
    
    if (self.isFinished==NO) {
        if (_count<=_xArray.count-1) {
            if ((xFloat <=[[_xArray objectAtIndex:_count] floatValue]/3.0f + 30.0f && xFloat >= [[_xArray objectAtIndex:_count] floatValue]/3.0f - 30.0f) && (yFloat <= [[_yArray objectAtIndex:_count] floatValue]/3.0f + 30.0f && yFloat >= [[_yArray objectAtIndex:_count] floatValue]/3.0f - 30.0f)) {
                
                _count ++;
                NSString *fileLocation = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"wsbutton_%ld",(long)_count] ofType:@"png"];
                UIImage* yourImage = [[UIImage alloc] initWithContentsOfFile:fileLocation] ;
                self.traceImageView.image = yourImage;
                while (_count>=56&& _count<=_xArray.count) {
                    NSString *fileLocation = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"wsbutton_%ld",(long)_count] ofType:@"png"];
                    UIImage* yourImage = [[UIImage alloc] initWithContentsOfFile:fileLocation] ;
                    self.traceImageView.image = yourImage;
                    _count++;
                }
            }
            
        }else{
            self.isFinished=YES;
            
            //[self.confirmButton setTitle:[NSString stringWithFormat:@"Confirm & Pay €%.2lf Deposit",[self.bookingObj.chosenPrice doubleValue]/100*10] forState:UIControlStateNormal];
            
            if (IS_iPHONE5_or_Above) {
                
                [self.traceImageView setNeedsUpdateConstraints];
                //[self.tutorialImageView setNeedsUpdateConstraints];
                self.tutorialImageView.hidden=YES;
                [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    
                    
                    self.traceImageView.frame = CGRectMake(self.traceImageView.frame.origin.x, self.traceImageView.frame.origin.y - 40, self.traceImageView.frame.size.width, self.traceImageView.frame.size.height);
                   // self.bottomTutorialImage.constant=+50;
                   self.bottomImageView.constant=+50;
                   // [self.tutorialImageView layoutIfNeeded];
                    [self.traceImageView layoutIfNeeded];
                    
                    self.instructionsLabel.alpha=0;
                    
                    self.isLogoTraced =YES;
                    self.confirmButton.hidden=NO;
                    self.confirmButton.alpha=1.0;
                    
                } completion:^(BOOL finished) {
                    if (self.isFinished==NO) {
                        self.confirmButton.hidden=YES;
                    }
                }];
                
                
                
            }
            
            
            
        }
    }
    
    // }
    
}


-(void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.traceImageView];
    UIView* v = [self.view hitTest:location withEvent:nil];
    
    if (v)
        [self animateTutorial];
    
}
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.traceImageView];
    UIView* v = [self.view hitTest:location withEvent:nil];
    
    if (v)
        [self animateTutorial];
    
}

-(void)animateTutorial{
    if (!self.hasStarted) {
        [self.tutorialImageView startAnimating];
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore changeNavigationTitleColor:[UIColor whiteColor] OnViewController:self];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    [WSCore transparentNavigationBarOnView:self];
    
    
    [WSCore statusBarColor:StatusBarColorWhite];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    if ([self isMovingFromParentViewController])
    {
        [WSCore changeNavigationTitleColor:[UIColor blackColor] OnViewController:self];
        [WSCore statusBarColor:StatusBarColorBlack];
    }
   
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
