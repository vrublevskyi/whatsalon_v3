//
//  TabBarMainScreenViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 16/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabBarMainScreenViewController.h"
#import "RightSideMenuController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"

@interface TabBarMainScreenViewController ()<UITabBarControllerDelegate,UIViewControllerTransitioningDelegate>

@end

@implementation TabBarMainScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tabBarController.delegate=self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tabBarController:(UITabBarController *)theTabBarController didSelectViewController:(UIViewController *)viewController {
    NSUInteger indexOfTab = [theTabBarController.viewControllers indexOfObject:viewController];
    NSLog(@"Tab index = %lu", (unsigned long)indexOfTab);
    
    if (indexOfTab == 4) {
        
    }
}

- (BOOL) tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    NSUInteger indexOfTab = [tabBarController.viewControllers indexOfObject:viewController];
    NSLog(@"Tab index = %lu", (unsigned long)indexOfTab);
//
//    if (indexOfTab == 4) {
//        RightSideMenuController *m = [self.storyboard instantiateViewControllerWithIdentifier:@"MVC"];
//        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:m];
//        navCon.transitioningDelegate=self;
//        navCon.modalPresentationStyle = UIModalPresentationCustom;
//        [self.tabBarController presentViewController:navCon animated:YES completion:nil];
//        return NO;
//    }
    
    return YES;
}

#pragma mark - UIViewControllerTransitioningDelegate




-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return [[DismissDetailTransition alloc] init];
}


@end
