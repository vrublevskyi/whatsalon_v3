//
//  ExperienceItem.h
//  whatsalon
//
//  Created by Graham Connolly on 08/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExperienceItem : NSObject

@property (nonatomic)  NSString *title;
@property (nonatomic) NSString *link;
@property (nonatomic) NSString *expImageURL;
@property (nonatomic) NSString *expDescription;
@property (nonatomic) double timestamp;
@property (nonatomic) NSString * experience_id;
@property (nonatomic) NSString * favourite_dispaly;
@property (nonatomic) NSString * experienceDateToDisplay;
@property (nonatomic) int favourites;
@property (nonatomic) BOOL is_user_favourite;
@end
