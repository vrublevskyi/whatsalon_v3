//
//  MViewController.m
//  TabbarTest
//
//  Created by Graham Connolly on 28/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "RightSideMenuController.h"
#import <UIKit/UIKit.h>
#import "User.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "InboxViewController.h"
#import "AboutMenuViewController.h"
#import "EditProfileViewController.h"
#import "PolicyDetailViewController.h"
#import "TabSettingsViewController.h"
#import "TabLoginOrSignUpViewController.h"
#import "MenuSideHeaderView.h"
#import "MenuSideCell.h"

@interface RightSideMenuController ()<EditProfileDelegate,SettingsDelegate,LoginDelegate, UITableViewDelegate, UITableViewDataSource>
/*! 
 @brief represents the first button in the menu
 */
@property (weak, nonatomic) IBOutlet UITableView *tableView;
/*! 
 @brief represents the Menu
 */
@property (weak, nonatomic) IBOutlet UIView *menuView;

/*! 
 @brief represents the header view within the menu view.
 */
@property (weak, nonatomic) IBOutlet MenuSideHeaderView *headerView;


/*! 
 @brief represents the image added to the bottom button
 */
@property (nonatomic) UIImageView * bottomImage;

@property (nonatomic) NSMutableArray * buttonsTitles;
@property (nonatomic) NSMutableArray * buttonsImages;

/*! 
 @brief represents the profile Image of the user
 */

/*! @brief represents the background of the bottomButton.
    
    @discussion the bottomView is used for the background color because the UIButton has left aligned text, but the spacing it not great enough. The bottomView gives the impression of the background image
 */

/*! 
 @brief represents the image used to represent settings. Embedded in a UIButton to properly center it.
 */
@property (nonatomic) UIImageView * gearImage ;

/*! @brief represents the image used to represent inbox. Embedded in a UIButton to properly center it.
 */
@property (nonatomic) UIImageView * emailImage;

/*! @brief represents the button for inbox. */
@property (nonatomic) UIButton * inboxButton;

/*! @brief represents the button for settings. */
@property (nonatomic) UIButton * settingsButton;


@end

@implementation RightSideMenuController


///updates profile Image
- (void)updateProfileImage {
    if ([[User getInstance] isUserLoggedIn]){
        [self.headerView.profileImageView sd_setImageWithURL:[[User getInstance] fetchImageURL ] placeholderImage:[UIImage imageNamed:kProfile_image]];
        self.headerView.userNameLabel.text = [[User getInstance]  fetchFirstName];
    }else{
        self.headerView.profileImageView.image = [UIImage imageNamed:kProfile_image];
        self.headerView.userNameLabel.text = nil;
    }
}


///manages button1,button2,button3 state, as well as settings buttons
- (void)setUpButtonState {
    self.buttonsTitles = [[NSMutableArray alloc] initWithObjects:@"Settings", @"Wallet", @"FAQ", @"Help", nil];
    self.buttonsImages = [[NSMutableArray alloc] initWithObjects:@"settingsMenu", @"wallet_icon", @"faqMenu", @"helpMenu", nil];
    
    if (![[User getInstance] isUserLoggedIn]) {
        [_buttonsTitles removeObjectAtIndex:1];
        [_buttonsImages removeObjectAtIndex:1];
    }
    [self.tableView reloadData];
}

-(void)updateGearIconColor{
    if ([[User getInstance] isTwilioVerified] || ![[User getInstance] isUserLoggedIn]) {
        self.gearImage.tintColor = [UIColor grayColor];
    }
    else{
        self.gearImage.tintColor = [UIColor redColor];
    }

}

/**manages the bottom buttons setup (login/signup etc)
 Also manages the bottomImage which is placed in the bottom button.
 */
- (void)bottomButtonSetUp {
    if (![[User getInstance] isUserLoggedIn]) {
        
        [self.loginBtn setTitle:@"Log In or Sign Up" forState:UIControlStateNormal];
        [self.loginBtn addTarget:self action:@selector(loginVC) forControlEvents:UIControlEventTouchUpInside];
        [self.loginBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.loginBtn.backgroundColor=[UIColor clearColor];
        self.loginBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        [self.bottomImage removeFromSuperview];
        UIImageView * loginImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20,20)];
        loginImage.image = [[UIImage imageNamed:@"login_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        loginImage.tintColor=[UIColor grayColor];
        self.bottomImage = loginImage;
        [self.loginBtn addSubview:self.bottomImage];
        self.bottomImage.center = CGPointMake(self.loginBtn.frame.size.width-50, self.loginBtn.frame.size.height/2.0f);
        self.loginBtn.hidden=NO;

    }
    else{
        self.loginBtn.hidden=YES;
        
    }
}

-(void) registerCells {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MenuSideCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"MenuSideCell"];
}


- (void)inboxAndSettingsButtonSetUp {
    
    self.gearImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    self.gearImage.image = [[UIImage imageNamed:@"gear_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.gearImage.tintColor=[UIColor grayColor];
    [self.settingsButton addSubview:self.gearImage];
    self.gearImage.center = CGPointMake(self.settingsButton.frame.size.width/2.0, self.settingsButton.frame.size.height/2.0f);
    [self.settingsButton setTitle:@"" forState:UIControlStateNormal];
    self.settingsButton.layer.masksToBounds=YES;
    [self.settingsButton addTarget:self action:@selector(goToSettings) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.emailImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    self.emailImage.image = [[UIImage imageNamed:@"email_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.emailImage.tintColor=[UIColor grayColor];
    [self.inboxButton addSubview:self.emailImage];
    self.emailImage.center = CGPointMake(self.inboxButton.frame.size.width/2.0, self.inboxButton.frame.size.height/2.0f);
    [self.inboxButton setTitle:@"" forState:UIControlStateNormal];
    self.inboxButton.layer.masksToBounds=YES;
    [self.inboxButton addTarget:self action:@selector(goToInbox) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)bottomButtonInit {
   
    if (IS_IOS_8_OR_LATER) {
        if (IS_iPHONE4) {
            self.loginBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, self.view.frame.size.height-64, self.visibleFrame.size.width-10, 44)];
        }else{
            self.loginBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, self.menuView.frame.size.height-44, self.visibleFrame.size.width-10, 44)];
        }
        
    }
    else{
       self.loginBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, self.view.frame.size.height-64, self.visibleFrame.size.width-10, 44)];
    }
    
    self.loginBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.loginBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.menuView addSubview:self.loginBtn];
    self.loginBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.loginBtn addTarget:self action:@selector(loginVC) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [WSCore logTabType];
    [self footerView];
   
    self.headerView.backgroundColor = kBackgroundColor;
    self.view.backgroundColor=[UIColor clearColor];

    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, 5, self.menuView.frame.size.height)];
    self.menuView.layer.masksToBounds = NO;
    self.menuView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.menuView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.menuView.layer.shadowOpacity = 0.5f;
    self.menuView.layer.shadowPath = shadowPath.CGPath;
    
   
     NSLog(@"header frame before %@",NSStringFromCGRect(self.headerView.frame));
    CGRect visibleHeaderFrame = self.headerView.frame;
    visibleHeaderFrame.size.width=self.view.frame.size.width-self.view.frame.size.width/3.0f;
    self.headerView.frame=visibleHeaderFrame;
    NSLog(@"header frame after %@",NSStringFromCGRect(self.headerView.frame));
    
    self.inboxButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height-50, visibleHeaderFrame.size.width/2.0f, 50)];
    self.inboxButton.backgroundColor = [UIColor whiteColor];

    
    self.settingsButton = [[UIButton alloc] initWithFrame:CGRectMake(visibleHeaderFrame.size.width/2.0, self.headerView.frame.size.height-50, visibleHeaderFrame.size.width/2.0f, 50)];
    [self.settingsButton setTitle:@"Settings" forState:UIControlStateNormal];
    self.settingsButton.backgroundColor = [UIColor whiteColor];

    [self updateProfileImage];

//    [self inboxAndSettingsButtonSetUp];

    self.visibleFrame = visibleHeaderFrame;
//
//    [self bottomButtonInit];
//    
//    [self bottomButtonSetUp];
//    
    [self setUpButtonState];

    [self registerCells];

    UITapGestureRecognizer * tappedImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToProfile)];
    tappedImage.numberOfTapsRequired=1;
    [self.headerView.profileImageView addGestureRecognizer:tappedImage];
    
    UISwipeGestureRecognizer * dismissSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    dismissSwipe.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:dismissSwipe];
    
   
    [WSCore setLoginType:LoginTypeSideMenu];
    
}


/**
 loginVC - manages navigation to the loginSignUp pages if the user is logged in.
 */
-(void)loginVC{
    
    if (![[User getInstance] isUserLoggedIn]) {
        [self performSegueWithIdentifier:@"loginSignUpVC" sender:self];

    }
}

-(void)updateGearColor{
    
    if ([[User getInstance] isUserLoggedIn]) {
        [self updateGearIconColor];
    }
    
}

/**
 goToSettings -presents the status bar before performing presentSettings
 performs presentSettings after 0.3 second delay - this gives the status bar time to appear before being pushed
 */
-(void)goToSettings{
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    
    [self presentSettings];

}

///performs a segue 'loggedInSettingsVC'
-(void)presentSettings{
    [self performSegueWithIdentifier:@"loggedInSettingsVC" sender:self];
}

/**
 goToHelp - presents the status bar before performing pushToHelp
 performs presentSettings after 0.3 second delay - this gives the status bar time to appear before being pushed
 */
-(void)goToHelp{
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    
    
   
    [self pushToHelp];
}

///performs the segue 'helpVC'
-(void)pushToHelp{
    [self performSegueWithIdentifier:@"helpVC" sender:self];
}

/**
 goToProfile - presents the status bar before performing goToHelp
 */
-(void)goToProfile{
    if([[User getInstance] isUserLoggedIn]){
        [[UIApplication sharedApplication] setStatusBarHidden:NO
                                                withAnimation:UIStatusBarAnimationFade];
        
        
        [self performSelector:@selector(showProfile) withObject:nil afterDelay:0.3];
    }
    
    
}

/**
 show - presents EditProfileViewController
 set EditProfileViewController delegate to self
 set isFromRightMenu bool to yes on EditProfileViewController
  - this determines what should and should not be perform in EditProfileViewController
 presents EditProfileViewController and sets statusBarColor to black.
 */
-(void)showProfile{
    EditProfileViewController * editProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileController"];
    editProfile.myDelegate=self;
    editProfile.isFromRightMenu=YES;
    [self presentViewController:editProfile animated:YES completion:^{
        [WSCore statusBarColor:StatusBarColorBlack];
    }];

}

/**
 goToWallet - presents the status bar before performing pushToWallet
 pushToWallet is performed after a 0.3 second delay  - this gives the status bar time to appear
 
 */
-(void)goToWallet{
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    
    
    [self performSelector:@selector(pushToWallet) withObject:nil afterDelay:0.3];
}

///performs the segue 'walletVC'
-(void)pushToWallet{
    [self performSegueWithIdentifier:@"walletVC" sender:self];
}

/**
 goToFAQ - presents the status bar before performing pushToFAQ
 pushToFAQ is performed after a 0.3 delay - this gives the status bar time to appear
 */
-(void)goToFAQ{
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    
    
    [self performSelector:@selector(pushToFAQ) withObject:nil afterDelay:0.3];
}

///perfomrs the segue 'aboutVC'
-(void)pushToFAQ{
  
    
    [self performSegueWithIdentifier:@"aboutVC" sender:self];

}


///prepareForSegue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"aboutVC"]) {
        UINavigationController * nav = (UINavigationController *)segue.destinationViewController;
        AboutMenuViewController * about = (AboutMenuViewController *)[nav.viewControllers objectAtIndex:0];
        about.isFromRightSideMenu=YES;
    }
    else if ([segue.identifier isEqualToString:@"loggedInSettingsVC"]){
        UINavigationController * nav = (UINavigationController *)segue.destinationViewController;
        TabSettingsViewController * loggedIn = (TabSettingsViewController *)[nav.viewControllers objectAtIndex:0];
        loggedIn.delegate=self;
    }
    else if([segue.identifier isEqualToString:@"loginSignUpVC"]){
       
        TabLoginOrSignUpViewController * tab = (TabLoginOrSignUpViewController *)segue.destinationViewController;
        tab.delegate=self;
        [WSCore setLoginType:LoginTypeSideMenu];
    }
}

#pragma mark - LoginDelegate
/**
 Login Delegate
 refreshes the button1,2,3 and settings button states
 updates the profile iamge
 rereshes the bottomButtonSetUp
 
 */
-(void)userDidLoginWithFacebook{
    NSLog(@"User did login with Facebook");
    
    [self setUpButtonState];
    [self updateProfileImage];
    [self bottomButtonSetUp];
    [self updateBackgroundViewController];
    
    
   
}
#pragma mark - SettingsDelegate
/**
 SettingsDelegate
 refreshes the button1,2,3 and settings button states
 updates the profile image
 refresehs the bottomButtonSetUp
 
 */
-(void)didLogOut{
    //log out
    
    NSLog(@"Did Log out");
    [self setUpButtonState];
    
    [self updateProfileImage];
    
    [self bottomButtonSetUp];
    
    
    [self updateBackgroundViewController];
}

-(void)updateBackgroundViewController{
    
    [WSCore logTabType];
    
    NSLog(@"\n\n Post message to update the background view \n\n");
    switch ([WSCore fetchTabType]) {
        case TabTypeFavourites:
            [[NSNotificationCenter defaultCenter] postNotificationName:kTab_2_Notification object:self];
            break;
            
        case TabTypeMyBookings:
            [[NSNotificationCenter defaultCenter] postNotificationName:kTab_3_Notification object:self];
            break;
            
        default:
            break;
    }
    

}

/**
 goToInbox - presents the status bar before pushingToInbox
 performs pushToInbox after a 0.3 delay - this gives the status bar animation enough time to finish
 */
-(void)goToInbox{
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    

    [self performSelector:@selector(pushToInbox) withObject:nil afterDelay:0.3];
   
}

///performs 'inboxVC' segue
-(void)pushToInbox{
   
    [self performSegueWithIdentifier:@"inboxVC" sender:self];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore flatNavBarOnView:self];

    [self.navigationController.navigationBar setTitleTextAttributes:
    @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}

/**
 viewDidAppear - performs 'updateProfileImage
 */
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self updateProfileImage];
    
}

/**
 dismiss
 dismisses the menu via animation
 shows the status bar
 adjusts the xAlignmentConstraint
 perform animation on menuView
 */

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -Edit profile delegate method

/**
 updateMenuProfile - EditProfileDelegate method
 updates the profile image
 */
-(void)updateMenuProfile{
    
    [self updateProfileImage];
}


#pragma mark - Unwind Segue
/**
 unwinToRightMenu - unwind segue 
 used to refresh content
 calls setUpButtonState
 calls updateProfileImage
 calls updateProfileIMage
 */
- (IBAction)unwindToRightMenu:(UIStoryboardSegue *)unwindSegue
{
  
    
    [self setUpButtonState];
    
    [self updateProfileImage];
    
    [self bottomButtonSetUp];
    
    [self updateBackgroundViewController];
}

#pragma mark - Tableview
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
            [self goToSettings];
            break;
        case 1:
            if (![[User getInstance] isUserLoggedIn]) {
                [self goToHelp];
                break;
            }
            [self pushToWallet];
            break;
        case 2:
           if  (![[User getInstance] isUserLoggedIn]) {
               [self goToFAQ];
               break;
            }
            [self goToHelp];
            break;
        case 3:
            [self goToFAQ];
            break;
        default:
            break;
    }
}

- (void)footerView
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kButtonHeight+120)];//adds 120 because inital starting height of table view is 'self.tableView.frame.size.height+100
    footerView.backgroundColor=[UIColor whiteColor];
    self.tableView.tableFooterView=footerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuSideCell *cell = (MenuSideCell *) [self.tableView dequeueReusableCellWithIdentifier:@"MenuSideCell"];
    NSString *title = self.buttonsTitles[indexPath.row];
    NSString *imageName = self.buttonsImages[indexPath.row];
    
    cell.menuLogoImageView.tintColor = [UIColor grayColor];
    cell.menuLogoImageView.image = [UIImage imageNamed:imageName];
    cell.menuNameLabel.text = title;
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.buttonsTitles.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

@end
