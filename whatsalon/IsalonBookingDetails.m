//
//  Isalon_booking_details.m
//  whatsalon
//
//  Created by Graham Connolly on 21/12/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//

#import "IsalonBookingDetails.h"

@implementation IsalonBookingDetails


-(NSString *) fetchBookingDescription{
    
    return [NSString stringWithFormat:@"iSalon Booking Details \n isalon_service_id=%@ \n salon_id=%@ \n staff_id=%@ \n start_time=%@",self.iSalon_service_id,self.salon_id,self.staff_id,self.start_time ];
}
@end
