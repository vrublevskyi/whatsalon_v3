//
//  MViewController.h
//  TabbarTest
//
//  Created by Graham Connolly on 28/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RightSideMenuController : UIViewController

/*!
 @brief loginBtn Button that represents a login/Button. The button is situated at the bottome of the menu.
 */

@property (nonatomic) UIButton* loginBtn;

/*!
 
 @brief visibleFrame represents the CGRect of the visible portion of the Menu. This is used to determine how to place UIElements evenly on the screen.
 */

@property (nonatomic) CGRect visibleFrame;
@end
