//
//  NoSalonsMessageView.m
//  whatsalon
//
//  Created by Graham Connolly on 02/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "NoSalonsMessageView.h"

@implementation NoSalonsMessageView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        //1. load .xib
        [[NSBundle mainBundle] loadNibNamed:@"NoSalonsMessageView" owner:self options:nil];
        
        //2 adjust bounds
        self.bounds = self.view.bounds;
        
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.mainText.adjustsFontSizeToFitWidth=YES;
        self.mainText.textColor=kCellLinesColour;
        self.smallText.textColor=kCellLinesColour;
        self.smallText.adjustsFontSizeToFitWidth=YES;
        self.titleText.textColor=kCellLinesColour;
        
        UIImage * templateImage = [[UIImage imageNamed:@"sad_face" ] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.imageView.image = templateImage;
        self.imageView.tintColor=kWhatSalonBlue;
        //3 add as subview
        [self addSubview:self.view];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //load the interface file from .xib
        [[NSBundle mainBundle] loadNibNamed:@"NoSalonsMessageView" owner:self options:nil];
        
        //Add as a subview
        [self addSubview:self.view];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
