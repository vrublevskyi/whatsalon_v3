//
//  TimeCollectionViewCell.h
//  Horizontal TableView
//
//  Created by Graham Connolly on 12/12/2014.
//  Copyright (c) 2014 Graham Connolly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeCollectionViewCell : UICollectionViewCell

@property (nonatomic) UILabel * priceLabel;
@property (nonatomic) UILabel * timeLabel;
@property (nonatomic) UILabel * midLabel;

@end
