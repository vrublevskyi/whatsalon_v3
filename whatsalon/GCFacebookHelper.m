//
//  GCFacebookHelper.m
//  FacebookiOS9Test
//
//  Created by Graham Connolly on 17/09/2015.
//  Copyright © 2015 GrahamConnolly. All rights reserved.
//

#import "GCFacebookHelper.h"


@implementation GCFacebookHelper

-(void)revokePermissions{
    
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/permissions"
                                       parameters:nil
                                       HTTPMethod:@"DELETE"]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         
         if (!error) {
             NSLog(@"Permissions revoked");
             dispatch_async(dispatch_get_main_queue(), ^{
                 if ([self.delegate respondsToSelector:@selector(permissionsRevoked)]) {
                     [self.delegate permissionsRevoked];
                 }
             });
         }
         else{
             NSLog(@"An error occurred");
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 if ([self.delegate respondsToSelector:@selector(revokeErrorOccured:)]) {
                     [self.delegate revokeErrorOccured:error];
                 }
             });
         }
     }];
    
}
- (id)init {
    self = [super init];
    if (self) {
        [self initHelper];
    }
    return self;
}

-(void)initHelper{
    self.loginManager = [[FBSDKLoginManager alloc] init];

}
-(void)loginFacebookUserWithPermissions: (NSArray *) permissions FromViewController: (UIViewController *) parentController{
    
    
    [self.loginManager logInWithReadPermissions:permissions fromViewController:parentController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"Result");
            NSLog(@"Process error\n Title: %@ \n description: %@",error.userInfo[ FBSDKErrorLocalizedTitleKey],error.userInfo[ FBSDKErrorLocalizedDescriptionKey]);
            
#warning add facebook error in next update to show the user
            
            if ([self.delegate respondsToSelector:@selector(errorOccurredWhenLoggingIn:)]) {
                [self.delegate errorOccurredWhenLoggingIn:error];
            }
            
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
            
            if ([self.delegate respondsToSelector:@selector(userDidCancelLogin)]) {
                [self.delegate userDidCancelLogin];
            }
        } else {
            NSLog(@"Logged in");
            
            if ([self.delegate respondsToSelector:@selector(userDidLogin)]) {
                [self.delegate userDidLogin];
            }
        }
        
    }];
}

-(void)logout{
    NSLog(@"GCHelper logout");
    if ([FBSDKAccessToken currentAccessToken]) {
        NSLog(@"logged out user");
        [self.loginManager logOut];
        
        
    }
}
@end
