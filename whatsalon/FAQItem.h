//
//  FAQItem.h
//  whatsalon
//
//  Created by Graham Connolly on 21/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAQItem : NSObject

/*! @brief represents the title. */
@property (nonatomic,strong) NSString * title;

/*! @brief represents the FAQ content. */
@property (nonatomic,strong) NSString *content;

@end
