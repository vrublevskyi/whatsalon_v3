//
//  ServiceTimeCell.h
//  whatsalon
//
//  Created by admin on 10/1/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ServiceTimeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkMarkImageView;
@property ( nonatomic) BOOL  stateSelected;


@end

NS_ASSUME_NONNULL_END
