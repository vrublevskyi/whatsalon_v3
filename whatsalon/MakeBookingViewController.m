//
//  BookUIViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 06/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "MakeBookingViewController.h"
#import "SalonBookingModel.h"
#import "Salon.h"
#import "UIView+AlertCompatibility.h"
#import "SalonService.h"
#import "Booking.h"
#import "PhorestStaffMember.h"
#import "SalonAvailableTime.h"
#import "CKCalendarView.h"
#import "User.h"
#import "LoginSignUpViewController.h"
#import "LinkCCViewController.h"
#import "ConfirmViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TwilioVerifiyViewController.h"
#import "UIImage+ColoredImage.h"
#import "NSNumber+PrimitiveComparison.h"
#import "SelectCategoryViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SelectServicesViewController.h"
#import "SelectStylistViewController.h"
#import "SelectTimeViewController.h"

@interface MakeBookingViewController ()<ServicesDelegate,UIPickerViewDataSource,UIPickerViewDelegate,CKCalendarDelegate,TwilioDelegate,LoginSignUpDelegate,UIViewControllerTransitioningDelegate,SelectStylistVCDelegate,SelectTimeDelegate>

@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UILabel *stylistHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceHeaderLabel;

@property (weak, nonatomic) IBOutlet UIView *datePickerContainer;
@property (nonatomic) UIColor * pickerButtonColor;

typedef NS_ENUM(int16_t, PreferedDay){
    PreferredDayToday =0,
    PreferredDayTomorrow =1,
    PreferredDayLater =2
    
};


@property (weak, nonatomic) IBOutlet UIButton *timeButton;
@property (weak, nonatomic) IBOutlet UIButton *serviceButton;
@property (weak, nonatomic) IBOutlet UIButton *stylistButton;
- (IBAction)pickDay:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *todayButton;
@property (weak, nonatomic) IBOutlet UIButton *laterButton;
@property (weak, nonatomic) IBOutlet UIButton *tomorrowButton;
@property (nonatomic) NSString * appointmentTime;


@property (nonatomic) UIButton * bookButton;


//booking model
@property (nonatomic) SalonBookingModel * salonBookingModel;

//services
@property (nonatomic) NSMutableArray * arrayOfServices;
@property (nonatomic,assign) BOOL isArrayOfServicesDownloaded;
@property (nonatomic) UIView * pickerContainer;
@property (nonatomic) UIView * coverView;
@property (nonatomic) UIPickerView * servicesPicker;
@property (nonatomic,assign) BOOL hasPickedService;
@property (nonatomic) NSInteger servicePickerIndex;
@property (nonatomic) Booking * bookObj;
@property (nonatomic) PreferedDay PreferredDay;

//stylist
@property (nonatomic,strong) NSMutableArray * workingStaffArray;
@property (nonatomic,assign) BOOL isArrayOfStylistDownloaded;
@property (nonatomic) NSMutableArray * allStaffArray;
@property (nonatomic) UIPickerView * stylistPicker;
@property (nonatomic) NSInteger stylistPickerIndex;


//time
@property (nonatomic) UIPickerView * timePicker;
@property (nonatomic) NSMutableArray * timesArray;
@property (nonatomic) NSInteger timePickerIndex;
@property (nonatomic,assign) BOOL isTimeFormFilled;


//later view
@property(nonatomic, strong) CKCalendarView *calendar;
@property(nonatomic, strong) NSDate *minimumDate;

@property (nonatomic) NSInteger oldTag;
@property (nonatomic) NSInteger actualTag;
@property (nonatomic) NSDate * selectedDate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic) NSString * timeDetail;

@property (nonatomic) CGFloat pickerHeight;

@property (nonatomic) NSDate * chosenDate;

@property (nonatomic) BOOL unwindSegueExecuted;

@property (nonatomic) UIButton * doneLaterFormButton;

@property (nonatomic) NSDate * calendarSelectedDate;

@end

@implementation MakeBookingViewController

#pragma mark - SelectTimeDelegate

-(void)didSelectTime:(SalonAvailableTime *)time{
    
    
    if (self.timesArray.count>0) {
        
        //SalonAvailableTime * time = time;
        
        if (self.PreferredDay == PreferredDayToday || self.PreferredDay == PreferredDayTomorrow){
            [self.timeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
            [self.timeButton setTitle:[time getTimeFromDate:time.start_time] forState:UIControlStateNormal];
            self.bookObj.chosenTime=[time getTimeFromDate:time.start_time];
            self.appointmentTime=self.bookObj.chosenTime;
            
        }
        else if(self.PreferredDay == PreferredDayLater){
            
            
            [self.timeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
            self.appointmentTime=[time getTimeFromDate:time.start_time];
            self.timeDetail =[NSString stringWithFormat:@"%@ - %@",[self laterFormattedDate:self.selectedDate],[time getTimeFromDate:time.start_time ]];
            [self.timeButton setTitle:self.timeDetail forState:UIControlStateNormal];
            self.bookObj.chosenTime=self.timeDetail;
            NSLog(@"Later");
            
        }
        self.bookObj.room_id = time.room_id;
        self.bookObj.slot_id = time.slot_id;
        self.bookObj.staff_id= time.staff_id;
        self.bookObj.internal_start_datetime = time.internal_start_datetime;
        self.bookObj.internal_end_datetime = time.internal_end_datetime;
        self.bookObj.endTime = time.end_time;
        
        //the time form is filled
        self.isTimeFormFilled=YES;
    
    }
    
        NSLog(@"*1 can make booking without credit card %d",[self hasMoneyToMakeBooking]);
        [self currentButtonState];

}
#pragma mark - SelectStylistDelegate
-(void)didSelectStylist:(PhorestStaffMember *)stylist{
    
    self.bookButton.hidden=YES;
    [self.timeButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.timeButton setTitle:@"Select Time" forState:UIControlStateNormal];
    self.isTimeFormFilled=NO;
    
    PhorestStaffMember * staffMember = stylist;
    NSLog(@"Staff member first name %@",staffMember.staff_last_name);
    if (staffMember.isWorking) {
        [self.timesArray removeAllObjects];
        
        if (!staffMember.isAnyStylist) {
            
            NSLog(@"Staff member times %@",staffMember.times);
            
            [self.timesArray addObjectsFromArray:staffMember.times];
            NSLog(@"Times array %lu",(unsigned long)self.timesArray.count);
            self.bookObj.chosenStylist = [NSString stringWithFormat:@"%@ %@",staffMember.staff_first_name,staffMember.staff_last_name];
            self.bookObj.staff_id = staffMember.staff_id;
            
        }
        else{
            
            self.bookObj.chosenStylist = @"Any Stylist";
            self.bookObj.staff_id=@"";
            
            
            //creates an array of all available times rather than times specific for one stylist
            [self createAllTimesArray];
        }
        
        [self.stylistButton setTitle:self.bookObj.chosenStylist forState:UIControlStateNormal];
        [self.stylistButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        //[self cancelStylistForm];
        
        
    }
    else{
        
        [UIView showSimpleAlertWithTitle:@"Oh Oh" message:[NSString stringWithFormat:@"%@ %@ is unavailble today. Please try another stylist.",staffMember.staff_first_name,staffMember.staff_last_name] cancelButtonTitle:@"OK"];
    }

}
- (IBAction)unwindToMakeBookingWithSelectedService:(UIStoryboardSegue *)unwindSegue
{
    SelectServicesViewController * unwind = [unwindSegue sourceViewController];
    NSLog(@"Unwind %@",unwind.selectedService.service_name);
    [self defaultValues];
    
    
    if (![WSCore isDummyMode]) {
        SalonService * pServ = unwind.selectedService;
        NSLog(@"Phorest Service %d %@",pServ.isHeader,pServ.service_name);
        if (!pServ.isHeader) {
            
            [self.serviceButton setTitle:[NSString stringWithFormat:@"%@ - %@%.2f ",pServ.service_name,CURRENCY_SYMBOL,[pServ.price doubleValue]] forState:UIControlStateNormal];
            [self.serviceButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
            
            //add to booking object
            self.bookObj.service_id=pServ.service_id;
            self.bookObj.chosenPrice=pServ.price;
            self.bookObj.chosenService=pServ.service_name;
            self.hasPickedService=YES;
            
        }
    }
    else{
        
        /*
        [self.serviceButton setTitle:pSer forState:UIControlStateNormal];
        [self.serviceButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        self.servicePickerIndex=(int)row;
        
        self.bookObj.chosenPrice=@"35.00";
        self.bookObj.chosentService=[self.arrayOfServices objectAtIndex:row];
        self.hasPickedService=YES;
        
        [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
        */
    }

}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    
    
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    
        return [[DismissDetailTransition alloc] init];

    
}


#pragma mark - hasMoneyToMakeBooking
/*
 Determines if ths user has balance to make a booking or needs to add a card
 */
-(BOOL)hasMoneyToMakeBooking{
    
    double usersBalanceDouble = [[[User getInstance] fetchUsersBalance] doubleValue] ;     NSDecimalNumber* usersBalance = [[NSDecimalNumber alloc] initWithDouble:usersBalanceDouble]; //create an NSDecimalNumber using your previously extracted double
    
    double chosenPrice = [self.bookObj.chosenPrice doubleValue];
    double bookingPrice = (chosenPrice/100)*10;
  
    NSDecimalNumber* bookingPriceDM= [[NSDecimalNumber alloc] initWithDouble:bookingPrice];
    
    NSLog(@"booking price DM %@",bookingPriceDM);
    NSLog(@"has credit card %d",[[User getInstance] hasCreditCard]);
    NSLog(@"users balance %@ is greater than the booking price %@ %d",usersBalance,bookingPriceDM,[usersBalance compare:bookingPriceDM]==NSOrderedDescending);
    NSLog(@"users balance %@ is NOT greater than the booking price %@ %d",usersBalance,bookingPriceDM,[usersBalance compare:bookingPriceDM]==NSOrderedAscending);
    NSLog(@"users balance %@ is the same as booking price %@ %d",usersBalance,bookingPriceDM,[usersBalance compare:bookingPriceDM]==NSOrderedSame);
    
    
    if ([[User getInstance] hasCreditCard] ||[usersBalance compare:bookingPriceDM]==NSOrderedDescending || [bookingPriceDM compare:usersBalance]==NSOrderedSame) {
        
        return YES;
    }
    return NO;
    
}





#pragma mark - View lifecycle
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (self.unwindSegueExecuted) {
        self.unwindSegueExecuted=NO;
        if (![[User getInstance] isTwilioVerified]) {
        
            TwilioVerifiyViewController * twilioVC = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyVC"];
            twilioVC.delegate=self;
            [self presentViewController:twilioVC animated:YES completion:nil];
            
        }
        else if(![self hasMoneyToMakeBooking]){
         
            LinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
            linkCC.modalPresentationStyle = UIModalPresentationCurrentContext;
            //linkCC.isOnlyAddingCreditCard=YES;
            linkCC.onlyAddCard=YES;
            [self presentViewController:linkCC animated:YES completion:nil];
        }

    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    [self.salonBookingModel cancelBookingModel];

}
- (void)backgroundImageInit {
    
    UIImageView *imageView = [[UIImageView alloc ]init];
    imageView.frame =self.view.frame;
    
    if ([self.salonData fetchSlideShowGallery].count>0) {
        [imageView sd_setImageWithURL:[[self.salonData fetchSlideShowGallery] objectAtIndex:0]];
    }
    else{
        imageView.image = [UIImage imageNamed:kProfile_image];
    }
    UIView * blackView = [[UIView alloc] initWithFrame:imageView.frame];
    blackView.backgroundColor=[UIColor blackColor];
    blackView.alpha=0.7;
    [imageView addSubview:blackView];
    [self.view insertSubview:imageView belowSubview:self.scrollView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.isMovingToParentViewController) {
        [self dateButtonSetUp];
        [self buttonSetUp];
        
    }
    [self currentButtonState];
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    NSLog(@"***** Can make booking without card %d ****",[self hasMoneyToMakeBooking]);
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound categories:nil]];
    }
    [self backgroundImageInit];
    [WSCore statusBarColor:StatusBarColorWhite];


    if ([WSCore isDummyMode]) {
        /*
        self.arrayOfServices = [[NSMutableArray alloc] initWithArray:[WSCore getDummyServices]];
        self.timesArray = [[NSMutableArray alloc] initWithArray:[WSCore getDummyTimes]];
        self.workingStaffArray = [[NSMutableArray alloc] initWithArray:[WSCore getDummyStylist]];
        self.bookObj = [Booking bookingWithSalonID:self.salonData];
*/
    }
    else{
        self.salonBookingModel = [[SalonBookingModel alloc] init];
        self.salonBookingModel.view = self.view;
        self.salonBookingModel.myDelegate=self;
        
        //services
        [self.salonBookingModel fetchServicesListWithID:self.salonData.salon_id];
        self.arrayOfServices = [NSMutableArray array];
        
        self.bookObj = [Booking bookingWithSalonID:self.salonData];
        //working array
        self.workingStaffArray = [NSMutableArray array];
        //all staff
        [self.salonBookingModel fetchStaff:self.salonData.salon_id];
        self.allStaffArray = [NSMutableArray array];
        
        //times array
        self.timesArray = [NSMutableArray array];

    }
    
    self.servicesPicker = [[UIPickerView alloc] init];

    self.PreferredDay=PreferredDayToday;
    //stylist picker
    self.stylistPicker = [[UIPickerView alloc] init];
    self.oldTag= -1;
    self.actualTag=0;
    
   

    self.footerView.backgroundColor=[UIColor clearColor];
    
    [WSCore transparentNavigationBarOnView:self];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.title=self.salonData.salon_name;
    
    
    self.pickerButtonColor=[UIColor whiteColor];
    self.pickerHeight=300.0f;
    if ([WSCore isDummyMode]) {
        self.servicePickerIndex=0;
    }else{
        self.servicePickerIndex=1;
    }
  
}





#pragma mark - Button init
- (void)buttonSetUp
{

    self.bookButton.layer.cornerRadius=3.0f;
    self.bookButton.layer.masksToBounds=YES;

    
    [self.view bringSubviewToFront:self.bookButton];

    if (!IS_iPHONE5_or_Above) {
        
        self.serviceButton.layer.cornerRadius=3.0f;
        self.serviceButton.backgroundColor=[UIColor clearColor];
        self.serviceButton.layer.borderColor=[UIColor whiteColor].CGColor;
        self.serviceButton.layer.borderWidth=1.0;
        
        self.stylistButton.layer.cornerRadius=3.0f;
        self.stylistButton.backgroundColor=[UIColor clearColor];
        self.stylistButton.layer.borderColor=[UIColor whiteColor].CGColor;
        self.stylistButton.layer.borderWidth=1.0;
        
        self.timeButton.layer.cornerRadius=3.0f;
        self.timeButton.backgroundColor=[UIColor clearColor];
        self.timeButton.layer.borderColor=[UIColor whiteColor].CGColor;
        self.timeButton.layer.borderWidth=1.0;
        
        self.dateHeaderLabel.textColor=[UIColor whiteColor];
        self.stylistHeaderLabel.textColor=[UIColor whiteColor];
        self.timeHeaderLabel.textColor=[UIColor whiteColor];
        self.serviceHeaderLabel.textColor=[UIColor whiteColor];
        
        self.todayButton.backgroundColor=[UIColor clearColor];
        self.tomorrowButton.backgroundColor=[UIColor clearColor];
        self.laterButton.backgroundColor=[UIColor clearColor];
        
        self.datePickerContainer.layer.cornerRadius=3.0f;
        self.datePickerContainer.backgroundColor=[UIColor clearColor];
        self.datePickerContainer.layer.borderColor=[UIColor colorWithWhite:1.0 alpha:0.7].CGColor;
        self.datePickerContainer.layer.borderWidth=1.0;
    }
    else{
        
        self.serviceButton.alpha=0.7;
        self.serviceButton.layer.cornerRadius=3.0f;
        self.serviceButton.backgroundColor=[UIColor clearColor];
        self.serviceButton.layer.borderColor=[UIColor colorWithWhite:1.0 alpha:0.7].CGColor;
        self.serviceButton.layer.borderWidth=1.0;
        
        self.stylistButton.alpha=0.7;
        self.stylistButton.layer.cornerRadius=3.0f;
        self.stylistButton.backgroundColor=[UIColor clearColor];
        self.stylistButton.layer.borderColor=[UIColor colorWithWhite:1.0 alpha:0.7].CGColor;
        self.stylistButton.layer.borderWidth=1.0;
        
        
        self.timeButton.alpha=0.7;
        self.timeButton.layer.cornerRadius=3.0f;
        self.timeButton.backgroundColor=[UIColor clearColor];
        self.timeButton.layer.borderColor=[UIColor colorWithWhite:1.0 alpha:0.7].CGColor;
        self.timeButton.layer.borderWidth=1.0;
        
        
        self.todayButton.alpha=0.7;
        self.tomorrowButton.alpha=0.7;
        self.laterButton.alpha=0.7;
        
        self.dateHeaderLabel.alpha=0.7;
        self.dateHeaderLabel.textColor=[UIColor whiteColor];
        
        self.stylistHeaderLabel.alpha=0.7;
        self.stylistHeaderLabel.textColor=[UIColor whiteColor];
        
        self.timeHeaderLabel.alpha=0.7;
        self.timeHeaderLabel.textColor=[UIColor whiteColor];
        
        self.serviceHeaderLabel.alpha=0.7;
        self.serviceHeaderLabel.textColor=[UIColor whiteColor];
        
        self.todayButton.backgroundColor=[UIColor clearColor];
        self.tomorrowButton.backgroundColor=[UIColor clearColor];
        self.laterButton.backgroundColor=[UIColor clearColor];
        
        self.datePickerContainer.layer.cornerRadius=3.0f;
        self.datePickerContainer.backgroundColor=[UIColor clearColor];
        self.datePickerContainer.layer.borderColor=[UIColor colorWithWhite:1.0 alpha:0.7].CGColor;
        self.datePickerContainer.layer.borderWidth=1.0;

    }
}






#pragma mark - Twilio Delegate
-(void)twilioWasVerified{
    
    if (![[User getInstance] hasCreditCard]) {
        
        LinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
        linkCC.modalPresentationStyle = UIModalPresentationCurrentContext;
        linkCC.onlyAddCard=YES;
        [self presentViewController:linkCC animated:YES completion:nil];

    }
    else{
       
        [self currentButtonState];
    }
    
}






#pragma mark - Current title for button
-(void)currentButtonState{
    
    if (self.isTimeFormFilled) {
        self.bookButton.hidden=NO;
        self.bookButton.userInteractionEnabled=YES;
        [self.bookButton setTitle:[[User getInstance] buttonTitleStateWithServicePrice:self.bookObj.chosenPrice] forState:UIControlStateNormal];
        self.bookButton.backgroundColor=kWhatSalonBlue;
        [self.bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else{
        self.bookButton.hidden=YES;
        self.bookButton.userInteractionEnabled=NO;
    }
    
}





#pragma mark - check for credentials
-(void)didLoginWithFacebook{
    
    //facebook logged in successfully
    //check if twilio is present
    if (![[User getInstance] isTwilioVerified]) {
     
        TwilioVerifiyViewController * twilioVC = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyVC"];
        twilioVC.delegate=self;
        [self presentViewController:twilioVC animated:YES completion:nil];

    }
    else if(![[User getInstance] hasCreditCard]){

        LinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
        linkCC.modalPresentationStyle = UIModalPresentationCurrentContext;
        linkCC.onlyAddCard=YES;
        [self presentViewController:linkCC animated:YES completion:nil];
    }
    else{
        //[self changeButtonToBookNow];
        [self currentButtonState];
    }
}




#pragma mark - Destination view controller 
/*
 Determines which viwe controller to go to
 */
- (void)destinationViewController {

    
    /*
     Check if the user is not logger in
     */
    if (![[User getInstance] isUserLoggedIn]) {
        
        //Go to login controller
        LoginSignUpViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignupVC"];
        login.modalPresentationStyle=UIModalPresentationCurrentContext;
        login.delegate=self;
        [self presentViewController:login animated:YES completion:nil];
        
    }
    
    else if(![[User getInstance] isTwilioVerified]){
     
        
        TwilioVerifiyViewController * twilioVC = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyVC"];
        twilioVC.delegate=self;
        [self presentViewController:twilioVC animated:YES completion:nil];
    }
    
    /*
     Check if the user has a credit card assigned
     */
    else if(![self hasMoneyToMakeBooking]){
        NSLog(@"Show link credit card");
        //show credit card
        LinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
        linkCC.modalPresentationStyle = UIModalPresentationCurrentContext;
        linkCC.onlyAddCard=YES;
        [self presentViewController:linkCC animated:YES completion:nil];
        
        
    }
    /*
     If the user is logged in, and has a credit card attached to their account, then everything is ok
     */
    else{
        
        if (![WSCore isDummyMode]) {
            [self.salonBookingModel bookPhorestServiceWithUserID:[[User getInstance] fetchAccessToken] WithSalonID:self.salonData.salon_id WithSlotID:self.bookObj.slot_id WIthServiceID:self.bookObj.service_id WithRoomID:self.bookObj.room_id WithInternalStartDateTime:self.bookObj.internal_start_datetime WithInternalEndDateTime:self.bookObj.internal_end_datetime AndStaff_id:self.bookObj.staff_id];
        }
        else{
     
            
            [WSCore showNetworkLoadingOnView:self.view];
            [self performSelector:@selector(goToBookingScreenInDummyMode) withObject:nil afterDelay:1.5];

        }
        
    }
}

-(void)goToBookingScreenInDummyMode{
    NSLog(@"Dummy");
    [WSCore dismissNetworkLoadingOnView:self.view];
    [self performSegueWithIdentifier:@"confirmBookingB" sender:self];
}

- (IBAction)bookNow:(id)sender {
    [self book];
}
#pragma mark - Booking action (Main Button) ***
-(void)book{
    
    if ([[User getInstance] isUserLoggedIn]) {
        
        if (![[User getInstance] isTwilioVerified]) {
            [self destinationViewController];
        }
        else if (!self.hasPickedService && !self.isTimeFormFilled && !self.isArrayOfStylistDownloaded) {
            
            [UIView showSimpleAlertWithTitle:@"Oh Oh" message:@"Please fill out the entire form." cancelButtonTitle:@"OK"];
        }
        else{
            
            [self destinationViewController];
        }

    }
    else{
        
        [self destinationViewController];
    }
   
    
}

#pragma mark - Custom button methods
-(void)selectedButton: (UIButton *)btn{
   
    [btn setTitleColor:[UIColor colorWithRed:0.0/255.0f green:168.0/255.0f blue:233/255.0f alpha:1.0] forState:UIControlStateNormal];
}

-(void)deselectButton: (UIButton *)btn{
    btn.layer.borderColor=[UIColor clearColor].CGColor;
    btn.layer.borderWidth=0.0f;
    [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
}
-(void)addLineToBottonOfButton: (UIButton *) button{
    UIView * bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, button.frame.size.height-0.5, button.frame.size.width, 0.5f)];
    bottomLine.alpha=0.8;
    bottomLine.backgroundColor=[UIColor lightGrayColor];
    
    [button addSubview:bottomLine];
    
}




#pragma mark - UIButton custom actions - imitates UISegmentedControl
- (IBAction)pickDay:(id)sender {
    
    UIButton * btn = (UIButton *)sender;
    NSLog(@"Btn tag %ld",(long)btn.tag);
    self.oldTag=self.actualTag;
    self.actualTag=btn.tag;
    
   
    switch (btn.tag) {
        case 0:
            self.PreferredDay = PreferredDayToday;
            [self selectedButton:btn];
            [self deselectButton:self.tomorrowButton];
            [self deselectButton:self.laterButton];
          
            break;
        case 1:
             self.PreferredDay = PreferredDayTomorrow;
            [self selectedButton:btn];
            [self deselectButton:self.todayButton];
            [self deselectButton:self.laterButton];
           
            break;
        case 2:
            self.PreferredDay = PreferredDayLater;
            [self selectedButton:btn];
            [self deselectButton:self.todayButton];
            [self deselectButton:self.tomorrowButton];
       
            
            //create later view
            [self createLaterView];
            break;
            
        default:
            
             self.PreferredDay = PreferredDayToday;
            [self selectedButton:btn];
            [self deselectButton:self.tomorrowButton];
            [self deselectButton:self.laterButton];
            
            break;
    }

    //handles rest of values
    [self defaultValues];
}

-(void)createLaterView{
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, [WSCore screenSize].height-320, [WSCore screenSize].width, 320)];
    self.pickerContainer= view;
    
    [self addDarkBlurToPickerView: self.pickerContainer];
    //Center label
    UILabel * servicesNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.pickerContainer.frame)/2-75, 11, 150, 21)];
    servicesNameLabel.text = @"Pick a Date";
    servicesNameLabel.adjustsFontSizeToFitWidth=YES;
    servicesNameLabel.font = [UIFont systemFontOfSize: 17.0];
    servicesNameLabel.textColor =self.pickerButtonColor;
    servicesNameLabel.textAlignment=NSTextAlignmentCenter;
    [self.pickerContainer insertSubview:servicesNameLabel atIndex:1];
    
    //cancel
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 2, 80, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:self.pickerButtonColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelLaterForm) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.pickerContainer insertSubview:cancelButton atIndex:1];
    
    //button
    UIButton * doneButton = [[UIButton alloc] initWithFrame:CGRectMake(235, 2, 80, 40)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:self.pickerButtonColor forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneLaterForm) forControlEvents:UIControlEventTouchUpInside];
    doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.doneLaterFormButton=doneButton;
    
    if (self.selectedDate==nil) {
        self.doneLaterFormButton.hidden=YES;
    }
    
    [self.pickerContainer insertSubview:self.doneLaterFormButton atIndex:1];
    
    
    [self createCalendar];
    
    [WSCore addTopLine:self.pickerContainer :[UIColor lightGrayColor] :0.5f];
    [self.view addSubview:self.pickerContainer];
    
    UIView * coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-self.pickerContainer.frame.size.height)];
    coverView.alpha=0.0;
    coverView.backgroundColor=[UIColor blackColor];
    self.coverView=coverView;
    [self.view addSubview: self.coverView];
    
    [self popInViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
    
}

-(void)addDarkBlurToPickerView: (UIView *)view{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = view.bounds;
        view.backgroundColor=[UIColor clearColor];
        [view addSubview:visualEffectView];
        
    }
    
    else{
        
        view.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.9];
        
    }

}
-(void)cancelLaterForm{
    
   /*
    If selected date is nil set it back to the today button
    selected date is nil if no date has been selected.
    */
    if (self.selectedDate==nil) {
        [self pickDay:self.todayButton];
    }
    
    [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];

}



-(void)doneLaterForm{
    
    /*
     only assign selected date if the user selected a date from the calendar
     */
    self.selectedDate = self.chosenDate;
    
    
    [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
   
    
    [self.laterButton setTitle:[NSString stringWithFormat:@"Later \n%@",[self laterFormattedDate:self.selectedDate]] forState:UIControlStateNormal];
    [self.laterButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    
    [self defaultValues];

}

#pragma mark - Select Service Methods

- (void)createServiceView {
    

    
    SelectCategoryViewController * s = [self.storyboard instantiateViewControllerWithIdentifier:@"selectCategoryVC"];
    
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:s];
    s.modalPresentationStyle = UIModalPresentationCustom;
    
    s.transitioningDelegate = self;
    s.salonData = self.salonData;
    s.servicesArray = self.arrayOfServices;
    //now present this navigation controller modally
    [self presentViewController:navigationController
                       animated:YES
                     completion:^{
                         
                     }];
  
}

-(void)popOutViewAnimationWithView: (UIView *) container andWithCoverView: (UIView *) blockView{
    // animate it to the identity transform (100% scale)

    
    [UIView animateWithDuration:0.1 animations:^{
        blockView.alpha=0.0f;
    } completion:^(BOOL finished) {
        
        container.transform = CGAffineTransformIdentity;
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            container.transform = CGAffineTransformMakeScale(0.01, 0.01);
        } completion:^(BOOL finished) {
        
                [self destroyPickerContainerView];
        
        }];

    }];
   }
-(void)popInViewAnimationWithView: (UIView *) container andWithCoverView: (UIView *)blockView{
    
    self.bookButton.hidden=YES;
    
    container.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        container.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        
        [UIView animateWithDuration:0.1 animations:^{
            blockView.alpha=0.5;
        }];
        
    }];
}
- (IBAction)selectService:(id)sender {
    
    
    if ([WSCore isDummyMode]) {
        
        [self createServiceView];
    }
    else{
        if (!self.isArrayOfServicesDownloaded) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView showSimpleAlertWithTitle:@"Please Wait" message:@"Please wait, the list of available services are still downloading" cancelButtonTitle:@"OK"];
            });
            
        }
        else{
            
            [self createServiceView];
            
            
        }

    }
    
}
-(void)cancelServicesForm{
    
    
    [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
}





//used to get the staff
- (void)downloadAvailableStaffForService {
    [self.salonBookingModel fetchStaffAndTimesWithSalonID:self.salonData.salon_id WithServiceID:self.bookObj.service_id WithStaffID:@"" WithStartDatetime:[NSString stringWithFormat:@"%@ 00:00:00",[self getChosenDay]] WithEndDateTime:[NSString stringWithFormat:@"%@ 23:59:59",[self getChosenDay]]];
}




-(void)doneServicesForm{
    

    /*
     Successful handling of picking the service
     
     */
    [self defaultValues];

    NSInteger row = [self.servicesPicker selectedRowInComponent:0];
    
    if (![WSCore isDummyMode]) {
        SalonService * pServ = [self.arrayOfServices objectAtIndex:row];
        
        if (!pServ.isHeader) {
            
            [self.serviceButton setTitle:pServ.service_name forState:UIControlStateNormal];
            [self.serviceButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
            self.servicePickerIndex=(int)row;
            
            
            
            //add to booking object
            self.bookObj.service_id=pServ.service_id;
            self.bookObj.chosenPrice=pServ.price;
            self.bookObj.chosenService=pServ.service_name;
            self.hasPickedService=YES;
            
            [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
        }
    }
    else{
        
        [self.serviceButton setTitle:[self.arrayOfServices objectAtIndex:row] forState:UIControlStateNormal];
        [self.serviceButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        self.servicePickerIndex=(int)row;
        
        self.bookObj.chosenPrice=@"35.00";
        self.bookObj.chosenService=[self.arrayOfServices objectAtIndex:row];
        self.hasPickedService=YES;
        
        [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];

    }
    
    
    
    
}



#pragma mark - ServicesDelegate methods

-(void)didGetServices:(NSMutableArray *)services{
    self.isArrayOfServicesDownloaded=YES;
    [self.arrayOfServices removeAllObjects];
    [self.arrayOfServices addObjectsFromArray:services];
    
    //[self.arrayOfServices sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
   /*
#warning remove - only used for testing category picker.
    CategoryPickerViewController * cat = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryPickerVC"];
    cat.fullServicesList=self.arrayOfServices;
    [self presentViewController:cat animated:YES completion:nil];
    */
}

#pragma mark - Services Delegate - didGetStaff
//working staff
-(void)didGetStaff:(NSMutableArray *)staff{
    
    self.isArrayOfStylistDownloaded=YES;
    [self.workingStaffArray removeAllObjects];
    
    //any stylist staff object
    PhorestStaffMember * staffM = [[PhorestStaffMember alloc] init];
    staffM.isAnyStylist=YES;
    [self.workingStaffArray addObject:staffM];
    
    //add staff
    [self.workingStaffArray addObjectsFromArray:staff];
    
    [self createAllTimesArray];
    [self sortStaff];
   
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [WSCore dismissNetworkLoadingOnView:self.view];
        
//added
        if(self.workingStaffArray.count==0){
           
        [UIView showSimpleAlertWithTitle:@"Bummer!" message:@"There are no stylists available for that service today. Please try another service, or try another day." cancelButtonTitle:@"OK"];
            
        }
        else{
            
            [self createStylistView];
            
        }

    });
    
    
}

//all the staff
-(void)didGetAllStaff:(NSMutableArray *)allStaff{
  
    [self.allStaffArray addObjectsFromArray:allStaff];
    
}

-(void)failureAppointmentWithDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"** \n Booking failure %@ \n **",jsonDictionary);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView showSimpleAlertWithTitle:@"Unable to book" message:[NSString stringWithFormat:@"%@", jsonDictionary[@"message"]] cancelButtonTitle:@"OK"];
    });
    
}

-(void)successfullAppointmentWithDictionary:(NSDictionary *)jsonDictionary{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"** \n Booking success %@ \n **",jsonDictionary);
        NSString * ref = jsonDictionary[@"message"][@"appointment_reference_number"];
        self.bookObj.appointment_reference_number=ref;
        
        //pushes to a new view control, values are also pushed, please see prepare for segue
        [self performSegueWithIdentifier:@"confirmBookingB" sender:self];
    });
    
}
/*
-(void)didReserveAppointment:(BOOL)success WithRefNumber:(NSString *)ref{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [WSCore dismissNetworkLoadingOnView:self.view];
        
        if (!success) {
           
            [UIView showSimpleAlertWithTitle:@"Unable to book" message:[NSString stringWithFormat:@"%@", ref]cancelButtonTitle:@"OK"];
            
            
            
            
        }
        else{
            
            self.bookObj.appointment_reference_number=ref;
            
            //pushes to a new view control, values are also pushed, please see prepare for segue
            [self performSegueWithIdentifier:@"confirmBookingB" sender:self];
            
        }
        
    });
}

*/
#pragma mark - prepareForSegue
/*
 Notifies the view controller that a segue is about to be performed
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"confirmBookingB"]) {
        
        /*
         Go to confirm page
         create an instance of ConfirmViewController
         assign confirmViewControllers salonData instance our salonData instance
         assign confirmViewController bookingObj instance our bookObj instance
         */
        ConfirmViewController * confirmVC = (ConfirmViewController *)segue.destinationViewController;
        confirmVC.salonData = self.salonData;
        confirmVC.bookingObj=self.bookObj;
        confirmVC.isFromFavourite=self.isFromFavourite;
        confirmVC.unwindBackToSearchFilter=self.unwindBackToSearchFilter;
        NSLog(@"Is From Favourite %d",self.isFromFavourite);
        
        NSLog(@"Booking time %@",self.appointmentTime);
        NSArray *timeSegments = [self.appointmentTime componentsSeparatedByString:@":"];
        NSLog(@"%@",timeSegments);
        
        //get pm/am
        NSArray * minsAmPmSegments = [timeSegments[1] componentsSeparatedByString:@" "];
        NSLog(@"Min %@",minsAmPmSegments[0]);
        NSLog(@"AM/PM %@",minsAmPmSegments[1]);
        
        NSDate *currentDate = self.chosenDate;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate]; // Get necessary date components
        
        [components month]; //gives you month
        [components day]; //gives you day
        [components year]; // gives you year
        
        NSString* firstString = [timeSegments objectAtIndex:0];
        NSLog(@"\n\nFirst String (HOUR) %@",firstString);
        NSString* secondString = [minsAmPmSegments objectAtIndex:0];
        NSLog(@"Second string (MIN) %@",secondString);
        NSLog(@"AM OR PM %@\n\n\n", minsAmPmSegments[1]);
        if ([[minsAmPmSegments[1] lowercaseString ]isEqualToString:@"pm"]) {
            firstString = [NSString stringWithFormat:@"%d",[firstString intValue] + 12];
            NSLog(@"\n\nFirst String (HOUR) in 24 format %@",firstString);
        }
        
        NSDate * fullDate = [WSCore dateWithYear:[components year] month:[components month] day:[components day] WithHour:[firstString intValue] AndMin:[secondString intValue]];
        NSLog(@"Full Date %@",fullDate);
        confirmVC.chosenDate=fullDate;
        
        /*
         Check which day is selected
         */
        switch (self.PreferredDay) {
                
                /*
                 If today is selected then
                 Assign chosenDay a string containing Today and the chosen Time
                 */
            case PreferredDayToday:
                self.bookObj.chosenDay=[NSString stringWithFormat:@"Today, %@",self.bookObj.chosenTime];
                break;
                
                /*
                 If tomorrow is selected as the preferred day
                 Assign chosenday a string containing Tomorrow plus the chosenTime
                 */
            case PreferredDayTomorrow:
                self.bookObj.chosenDay=[NSString stringWithFormat:@"Tomorrow, %@",self.bookObj.chosenTime];
                
                break;
                
                /*
                 If later is selected as the preferred day
                 Assign chosenDay the self.dateDetailString which contains the Date and Time
                 
                 */
            case PreferredDayLater:
                self.bookObj.chosenDay=[NSString stringWithFormat:@"%@",self.timeDetail];
                break;
                
            default:
                self.bookObj.chosenDay=[NSString stringWithFormat:@"Today, %@",self.bookObj.chosenTime];
                break;
        }
        
        
        
    }
}

#pragma mark - Times array
/*
 Creates an array of all times
 
 */
-(void)createAllTimesArray{
    
    for (int i=0; i<self.workingStaffArray.count; i++) {
        
        PhorestStaffMember * staffM = self.workingStaffArray[i];
        if (!staffM.isAnyStylist)
            NSLog(@"Staff name %@",staffM.staff_first_name);
            [self.timesArray addObjectsFromArray:staffM.times];
    }
    
    //sort the array of times by there start time
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"start_time"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [self.timesArray sortedArrayUsingDescriptors:sortDescriptors];
    
    //remove all previous times
    [self.timesArray removeAllObjects];
    [self.timesArray addObjectsFromArray:sortedArray];
    
    
    
    
}

#pragma mark - Sort the staff array
-(void)sortStaff{
    
    NSMutableArray * fullArray = [NSMutableArray array];
    NSMutableArray * idArray = [NSMutableArray array];
    //NSMutableArray * noDuplicatesArray = [NSMutableArray array];
    NSMutableSet *staffSet = [NSMutableSet new];
    
    for (int i =1; i<self.workingStaffArray.count; i++) {
        PhorestStaffMember * workingStaffMember = [[PhorestStaffMember alloc] init];
        workingStaffMember = [self.workingStaffArray objectAtIndex:i];
        
        for (int j=0; j<self.allStaffArray.count; j++) {
            PhorestStaffMember * allStaffMember =[[PhorestStaffMember alloc] init];
            
            allStaffMember=[self.allStaffArray objectAtIndex:j];
            
            
            if (![workingStaffMember isEqual:allStaffMember]) {
                NSLog(@"working id %d != %d all Staff id add",[workingStaffMember.staff_id intValue],[allStaffMember.staff_id intValue]);
                allStaffMember.isWorking=NO;
                
                [idArray addObject:allStaffMember.staff_id];
                [staffSet addObject:allStaffMember];
           
                
            }
            
        }
    }
    
    [fullArray addObjectsFromArray:[staffSet allObjects]];
    
    
    [fullArray sortUsingDescriptors:[NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"staff_first_name" ascending:YES selector:@selector(caseInsensitiveCompare:)], nil]];
    
    
    for (int i=1; i<self.workingStaffArray.count; i++) {
        PhorestStaffMember * workingMember = [self.workingStaffArray objectAtIndex:i];
        
        for (int j=0; j<fullArray.count; j++) {
            PhorestStaffMember * staff = [fullArray objectAtIndex:j];
            
            if ([workingMember isEqual:staff]) {
                [fullArray removeObject:staff];
            }
            
        }
        
    }
    

    [self.workingStaffArray addObjectsFromArray:fullArray];
    
    /*
     Removes Any stylist - if issue remove this
     */
    NSMutableArray * workingArray = [NSMutableArray array];
    for (PhorestStaffMember*staff in self.workingStaffArray) {
        if (staff.isWorking) {
            [workingArray addObject:staff];
        }
        
    }
    
    if (workingArray.count==2) {
    
        for (PhorestStaffMember * staff in workingArray) {
            if (staff.isAnyStylist) {
               
                [self.workingStaffArray removeObject:staff];
            }
        }
    }
    /*
     End of removes any stylist
     */
    
    
}


#pragma mark - Get Chosen day
-(NSString *)getChosenDay{
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init] ;
    [deltaComps setDay:1];
    NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
    NSDate * today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    switch (self.PreferredDay) {
        case PreferredDayToday:
            
            NSLog(@"Date %@",[dateFormatter stringFromDate:today]);
            self.chosenDate=today;
            return [dateFormatter stringFromDate:today];
            
            break;
        case PreferredDayTomorrow:
            
            NSLog(@"Date %@",[dateFormatter stringFromDate:tomorrow]);
            self.chosenDate=tomorrow;
            return [dateFormatter stringFromDate:tomorrow];
            break;
        case PreferredDayLater:
            
            self.chosenDate=self.selectedDate;
            NSLog(@"Selected Date %@",[dateFormatter stringFromDate:self.selectedDate]);
            return [dateFormatter stringFromDate:self.selectedDate];
            break;
            
        default:
            
            return [dateFormatter stringFromDate:today];
            break;
    }
    
    
    return [dateFormatter stringFromDate:today];
}


#pragma mark - Select stylist methods
- (void)createStylistView {
    
    /*
     go to new view
     */
    
    SelectStylistViewController * stylistView = [self.storyboard instantiateViewControllerWithIdentifier:@"selectStylistVC"];
    stylistView.modalPresentationStyle = UIModalPresentationCustom;
//    stylistView.workers=self.workingStaffArray;
    stylistView.transitioningDelegate = self;
    stylistView.delegate=self;
//    stylistView.salonObj = self.salonData;
   // s.servicesArray = self.arrayOfServices;
    [self presentViewController:stylistView animated:YES completion:nil];
    
    /*
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, [WSCore screenSize].height-320, [WSCore screenSize].width, 320)];
    [self addDarkBlurToPickerView:view];
    UILabel * stylistNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(view.frame)/2-75, 11, 150, 21)];
    stylistNameLabel.text = @"Stylist";
    stylistNameLabel.adjustsFontSizeToFitWidth=YES;
    stylistNameLabel.font = [UIFont systemFontOfSize:17.0];
    stylistNameLabel.textColor = self.pickerButtonColor;
    stylistNameLabel.textAlignment=NSTextAlignmentCenter;
    [view insertSubview:stylistNameLabel atIndex:1];
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 2, 80, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:self.pickerButtonColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelStylistForm) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [view insertSubview:cancelButton atIndex:1];
    
    UIButton * doneButton = [[UIButton alloc] initWithFrame:CGRectMake(240, 2, 80, 40)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:self.pickerButtonColor forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneStylistForm) forControlEvents:UIControlEventTouchUpInside];
    doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    doneButton.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    [view insertSubview:doneButton atIndex:1];
    //view.backgroundColor = kGroupedTableGray;
    
    
    self.pickerContainer = view;
    self.stylistPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, view.frame.size.width, self.pickerHeight)];
    self.stylistPicker.delegate=self;
    self.stylistPicker.dataSource = self;
    [self.stylistPicker selectRow:self.stylistPickerIndex inComponent:0 animated:NO];

    
    [self.pickerContainer addSubview:self.stylistPicker];
    
    [WSCore addTopLine:self.pickerContainer :[UIColor lightGrayColor] :0.5f];
    
    [self.view addSubview:self.pickerContainer];
    
    UIView * coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-self.pickerContainer.frame.size.height)];
    coverView.alpha=0.5;
    coverView.backgroundColor=[UIColor blackColor];
    self.coverView=coverView;
    [self.view addSubview: self.coverView];
    
    [self popInViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
     */
}

- (IBAction)selectStylist:(id)sender {
    
    if (self.isArrayOfStylistDownloaded==NO && self.hasPickedService==NO) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please pick a service in order to get the available stylists." cancelButtonTitle:@"OK"];
        });
    }else{
        
        [self downloadAvailableStaffForService];
    }

}

-(void)cancelStylistForm{
    
    
    [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
}

-(void)doneStylistForm{
    
    
   
    
    NSInteger row = [self.stylistPicker selectedRowInComponent:0];
    NSLog(@"WOrking array %lu",(unsigned long)self.workingStaffArray.count);
    PhorestStaffMember * staffMember = [self.workingStaffArray objectAtIndex:row];
    
    /*
     Available workers are selectable
     
     */
    if (![WSCore isDummyMode]) {
        
        if (staffMember.isWorking) {
            [self.timesArray removeAllObjects];
            
            if (!staffMember.isAnyStylist) {
                
                NSLog(@"Staff member times %@",staffMember.times);
                
                [self.timesArray addObjectsFromArray:staffMember.times];
                NSLog(@"Times array %lu",(unsigned long)self.timesArray.count);
                self.bookObj.chosenStylist = [NSString stringWithFormat:@"%@ %@",staffMember.staff_first_name,staffMember.staff_last_name];
                self.bookObj.staff_id = staffMember.staff_id;
                
            }
            else{
                
                self.bookObj.chosenStylist = @"Any Stylist";
                self.bookObj.staff_id=@"";
                
                
                //creates an array of all available times rather than times specific for one stylist
                [self createAllTimesArray];
            }
            
            [self.stylistButton setTitle:self.bookObj.chosenStylist forState:UIControlStateNormal];
            [self.stylistButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
            [self cancelStylistForm];
            self.stylistPickerIndex=(int)row;
            
            
        }
        else{
            
            [UIView showSimpleAlertWithTitle:@"Oh Oh" message:[NSString stringWithFormat:@"%@ %@ is unavailble today. Please try another stylist.",staffMember.staff_first_name,staffMember.staff_last_name] cancelButtonTitle:@"OK"];
        }

    }
    else{
        
        self.bookObj.chosenStylist = [self.workingStaffArray objectAtIndex:row];;
        self.bookObj.staff_id=@"";

        [self.stylistButton setTitle:[self.workingStaffArray objectAtIndex:row] forState:UIControlStateNormal];
        [self.stylistButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        [self cancelStylistForm];
        self.stylistPickerIndex=(int)row;
        
       
    }
}


#pragma mark - Select Time
- (void)createTimeView {
    
    SelectTimeViewController * s = [self.storyboard instantiateViewControllerWithIdentifier:@"selectCategoryVC"];
    
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:s];
    s.modalPresentationStyle = UIModalPresentationCustom;
    
    s.timesArray=self.timesArray;
    s.delegate=self;
    //now present this navigation controller modally
    [self presentViewController:navigationController
                       animated:YES
                     completion:^{
                         
                     }];
}

- (IBAction)selectTime:(id)sender {
    
    if ([WSCore isDummyMode]) {
       
        [self createTimeView];
    }
    else{
        if (self.isArrayOfStylistDownloaded==NO) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please pick a service in order to get the available times." cancelButtonTitle:@"OK"];
            });
        }else if(self.workingStaffArray.count==0){
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView showSimpleAlertWithTitle:@"Bummer!" message:@"There are no stylists available for that service today. Please try another service, or try another day." cancelButtonTitle:@"OK"];
            });
        }else{
            
            
            [self createTimeView];
            
        }

    }
    
}


-(void)destroyPickerContainerView{
    [self.pickerContainer removeFromSuperview];
    [self.coverView removeFromSuperview];
    self.pickerContainer=nil;
    self.coverView=nil;
}
-(void)cancelTimesForm{
    [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
}

-(void)doneTimesForm{
    
    NSInteger row = [self.timePicker selectedRowInComponent:0];
    if ([WSCore isDummyMode]) {
        
        [self.timeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        [self.timeButton setTitle:[self.timesArray objectAtIndex:row] forState:UIControlStateNormal];
        self.bookObj.chosenTime=[self.timesArray objectAtIndex:row];
        
        //reference to the chosen index
        self.timePickerIndex=(int)row;
        
        
        
        //the time form is filled
        self.isTimeFormFilled=YES;
        
        
        [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
    }
    else{
        
        if (self.timesArray.count>0) {
            
            SalonAvailableTime * time = [self.timesArray objectAtIndex:row];
            
            if (self.PreferredDay == PreferredDayToday || self.PreferredDay == PreferredDayTomorrow){
                [self.timeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
                [self.timeButton setTitle:[time getTimeFromDate:time.start_time] forState:UIControlStateNormal];
                self.bookObj.chosenTime=[time getTimeFromDate:time.start_time];
                self.appointmentTime=self.bookObj.chosenTime;
                
            }
            else if(self.PreferredDay == PreferredDayLater){
                
               
                [self.timeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
                self.appointmentTime=[time getTimeFromDate:time.start_time];
                self.timeDetail =[NSString stringWithFormat:@"%@ - %@",[self laterFormattedDate:self.selectedDate],[time getTimeFromDate:time.start_time ]];
                [self.timeButton setTitle:self.timeDetail forState:UIControlStateNormal];
                self.bookObj.chosenTime=self.timeDetail;
                NSLog(@"Later");
                
            }
            self.bookObj.room_id = time.room_id;
            self.bookObj.slot_id = time.slot_id;
            self.bookObj.staff_id= time.staff_id;
            self.bookObj.internal_start_datetime = time.internal_start_datetime;
            self.bookObj.internal_end_datetime = time.internal_end_datetime;
            self.bookObj.endTime = time.end_time;
            
            //reference to the chosen index
            self.timePickerIndex=(int)row;
            
            
            
            //the time form is filled
            self.isTimeFormFilled=YES;
            
            
            [self popOutViewAnimationWithView:self.pickerContainer andWithCoverView:self.coverView];
            
        }
 
    }
  
    NSLog(@"*1 can make booking without credit card %d",[self hasMoneyToMakeBooking]);
    [self currentButtonState];
}
#pragma mark - picker
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView==self.servicesPicker) {
        return self.arrayOfServices.count;
    }
    if (pickerView==self.timePicker) {
        return self.timesArray.count;
    }
    return self.workingStaffArray.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines=0;
    
    if (pickerView==self.servicesPicker) {
        
        if ([WSCore isDummyMode]) {
            CGRect frame =CGRectMake(15, 0, pickerView.frame.size.width, 44);
            label.frame=frame;
            label.text = [NSString stringWithFormat:@"-%@",[self.arrayOfServices objectAtIndex:row]];
            label.font =[UIFont systemFontOfSize:16.0f];
            label.textColor=[UIColor blackColor];
          
        }else{
            SalonService * pServ = [self.arrayOfServices objectAtIndex:row];
            if (pServ.isHeader==YES) {
                pServ = [self.arrayOfServices objectAtIndex:row];
                CGRect frame =CGRectMake(15, 0, pickerView.frame.size.width-15, 44);
                label.frame=frame;
                label.text = [NSString stringWithFormat:@"%@",pServ.category_name];
                label.font = [UIFont boldSystemFontOfSize:18.0f];
                label.textColor=[UIColor blackColor];
                
            }
            else if (pServ.isHeader==NO){
                CGRect frame =CGRectMake(25, 0, pickerView.frame.size.width-25, 44);
                label.frame=frame;
                label.text = [NSString stringWithFormat:@"-%@ €%@",pServ.service_name, pServ.price];
                if ([label.text length]>30) {
                    label.font =[UIFont systemFontOfSize:14.0f];
                }
                label.font = [UIFont systemFontOfSize:16.0f];
                label.textColor=[UIColor blackColor];
            }

        }
        
        
    }
    else if (pickerView==self.stylistPicker){
        if (![WSCore isDummyMode]) {
            PhorestStaffMember * staffMember = [self.workingStaffArray objectAtIndex:row];
            
            if (!staffMember.isAnyStylist) {
                label.text = [NSString stringWithFormat:@"     -%@ %@",staffMember.staff_first_name,staffMember.staff_last_name];
                
            }
            else{
                label.text =@"     -Any Stylist";
                
            }
            
            label.adjustsFontSizeToFitWidth=YES;
           
            if (staffMember.isWorking) {
                label.alpha=1;
                 label.font = [UIFont systemFontOfSize:18];
            
            }
            else{
                
                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:label.text];
                [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                        value:@2
                                        range:NSMakeRange(0, [attributeString length])];
                
                label.attributedText = attributeString;
                 
                 label.alpha=0.2;
                 label.font = [UIFont systemFontOfSize:15];
                
            }

        }
        else{
            label.text = [NSString stringWithFormat:@"     -%@",[self.workingStaffArray objectAtIndex:row ]];
        }
        
        
    }else if(pickerView==self.timePicker){
        
        if (![WSCore isDummyMode]) {
            SalonAvailableTime * times = [self.timesArray objectAtIndex:row];
            label.text = [NSString stringWithFormat:@"%@",[times getTimeFromDate:times.start_time]];
          
        }
        else{
            label.text = [NSString stringWithFormat:@"%@",[self.timesArray objectAtIndex:row]];
        }
        
        label.adjustsFontSizeToFitWidth=YES;
        label.font = [UIFont systemFontOfSize:22];
        
        label.textAlignment=NSTextAlignmentCenter;
        
    }

    label.textColor=[UIColor whiteColor];
    return label;
}





#pragma mark - Default values
/*
 Returns values to their original state/
 */
-(void)defaultValues{
    
    self.bookButton.hidden=YES;

/*
    [self.serviceButton setTitle:@"Select Service" forState:UIControlStateNormal];//removed for testing
    [self.serviceButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];//removed for testing
    self.hasPickedService=NO; //removed for testing
 */
    [self.timeButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.timeButton setTitle:@"Select Time" forState:UIControlStateNormal];
    
    [self.stylistButton setTitle:@"Select Stylist" forState:UIControlStateNormal];
    [self.stylistButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
  
    self.isTimeFormFilled=NO;
    self.isArrayOfStylistDownloaded=NO;
    self.calendar=nil;
    
    //indexes
    if ([WSCore isDummyMode]) {
        self.servicePickerIndex=0;
    }else{
//removed for testing
        //self.servicePickerIndex=1; removed for testing
    }
    self.timePickerIndex=0;
    self.stylistPickerIndex=0;
    
}

- (void)dateButtonSetUp {
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init] ;
    [deltaComps setDay:1];
    NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
    NSDate * today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd"];
    
    [self.todayButton setTitleColor:[UIColor lightGrayColor]forState:UIControlStateNormal];
    [self.tomorrowButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.laterButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    
    [self.todayButton setTitle:[NSString stringWithFormat:@"Today \n%@%@",[dateFormatter stringFromDate:today],[WSCore daySuffixForDate:today] ] forState:UIControlStateNormal];
    [self.tomorrowButton setTitle:[NSString stringWithFormat:@"Tomorrow \n%@%@",[dateFormatter stringFromDate:tomorrow],[WSCore daySuffixForDate:tomorrow]] forState:UIControlStateNormal];
    [self.laterButton setTitle:@"Later \n-" forState:UIControlStateNormal];
    
    self.todayButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.todayButton.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    self.todayButton.titleLabel.textAlignment=NSTextAlignmentCenter;
    
    
    self.tomorrowButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.tomorrowButton.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    self.tomorrowButton.titleLabel.textAlignment=NSTextAlignmentCenter;
    
    self.laterButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.laterButton.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    self.laterButton.titleLabel.textAlignment=NSTextAlignmentCenter;
    
    UIView * seperatorLine1 = [[UIView alloc] initWithFrame:CGRectMake(0, 5, 1.0f, self.tomorrowButton.frame.size.height-10)];
    seperatorLine1.alpha=0.4;
    seperatorLine1.backgroundColor=[UIColor darkGrayColor];
    
    [self.tomorrowButton addSubview:seperatorLine1];
    
    UIView * seperatorLine2 = [[UIView alloc] initWithFrame:CGRectMake(self.tomorrowButton.frame.size.width-1.0f, 5, 1.0f, self.tomorrowButton.frame.size.height-10)];
    seperatorLine2.alpha=0.4;
    seperatorLine2.backgroundColor=[UIColor darkGrayColor];
    
    [self.tomorrowButton addSubview:seperatorLine2];
    
    
    self.todayButton.tag=0;
    self.tomorrowButton.tag=1;
    self.laterButton.tag=2;
    [self selectedButton:self.todayButton];
    
    [self addLineToBottonOfButton:self.todayButton];
    [self addLineToBottonOfButton:self.tomorrowButton];
    [self addLineToBottonOfButton:self.laterButton];
    
    [self addLineToBottonOfButton:self.serviceButton];
    [self addLineToBottonOfButton:self.stylistButton];
    [self addLineToBottonOfButton:self.timeButton];
    
    
}


-(void)createCalendar{
    if (self.calendar==nil) {
        self.calendar = [[CKCalendarView alloc] init];
        self.calendar.delegate=self;
        self.calendar.titleColor=self.pickerButtonColor;
        self.calendar.frame = CGRectMake(10, 40, 300, 300);
        //self.calendar.center = self.view.center;
        self.calendar.backgroundColor=[UIColor clearColor];
        if (self.selectedDate!=nil) {
            [self.calendar selectDate:self.selectedDate makeVisible:YES];
        }
    }
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = 1;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    
    self.minimumDate= nextDate;
    
    self.calendar.onlyShowCurrentMonth = NO;
    
    [self.calendar setInnerBorderColor:[UIColor whiteColor]];
    
    [self.pickerContainer addSubview:self.calendar];
    
}






#pragma mark - CKCalendar Delegate
- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date {
    
    //self.selectedDate = date;
    /*
     the date is chosen from the calendar - this is used later when done is selected on the later form
     */
    self.chosenDate=date;
    
    self.doneLaterFormButton.hidden=NO;
    NSLog(@"User picked %@",date);
}
- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
    return YES;
}

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date {
    
    if ([date compare: self.minimumDate] != NSOrderedDescending) {
        dateItem.textColor = [UIColor lightGrayColor];
        dateItem.backgroundColor = [UIColor clearColor];
    }
    
}
- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return [date compare: self.minimumDate] == NSOrderedDescending;
}
- (void)calendar:(CKCalendarView *)calendar didChangeToMonth:(NSDate *)date {
    NSLog(@"Hurray, the user changed months!");
}
- (BOOL)dateIsDisabled:(NSDate *)date {
    
    if ([date compare: self.minimumDate] != NSOrderedDescending) {
        return YES;
    }
    return NO;
}

-(NSString *)laterFormattedDate: (NSDate *) date{
    
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init] ;
    [deltaComps setDay:1];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM"];
    
    return [dateFormatter stringFromDate:date];
}






#pragma mark - UNWIND Segue
- (IBAction)unwindToBookUI:(UIStoryboardSegue *)unwindSegue
{
    
    UIViewController* sourceViewController = unwindSegue.sourceViewController;
    
    if ([sourceViewController isKindOfClass:[LinkCCViewController class]])
    {
        self.unwindSegueExecuted=NO;
    }else{
        self.unwindSegueExecuted=YES;
    }

}

@end
