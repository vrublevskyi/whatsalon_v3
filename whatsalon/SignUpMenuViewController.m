//
//  SignUpMenuViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 19/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SignUpMenuViewController.h"
#import "RegisterViewController.h"
#import "LoginPageViewController.h"
//#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"


@interface SignUpMenuViewController ()
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) AppDelegate *appDelegate;

-(void)handleFBSessionStateChangeWithNotification:(NSNotification *)notification;
@end

@implementation SignUpMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!self.isSignUp) {
          self.descriptionLabel.text = @"Quickly log in with:";  
    }

    [WSCore addDarkBlurAsSubviewToView:self.backgroundImage];
    
    self.facebookBtn.layer.cornerRadius=3.0f;
    self.emailBtn.layer.cornerRadius=3.0f;
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toNormalReg)];
    [self.emailBtn addGestureRecognizer:tap];
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    //we force our class to observe for the notification specified by the given name, and when it arrives we will call the handleFBSessionStateChangeWithNotification: method.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFBSessionStateChangeWithNotification:) name:@"SessionStateChangeNotification" object:nil];
}

-(void)toNormalReg{
    
    if (self.isSignUp) {
        RegisterViewController * reg =[self.storyboard instantiateViewControllerWithIdentifier:@"registerVC"];
        [self presentViewController:reg animated:YES completion:nil];
    }
    else{
        LoginPageViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
        [self presentViewController:login animated:YES completion:nil];
    }
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)facebookSignUp:(id)sender {
    
    /*
     The current session, also named active session, is accessed just like this:
     Using the state property of the active session, we determine if there is an open session or not.
     */
    
    /*
    if ([FBSession activeSession].state != FBSessionStateOpen &&
        [FBSession activeSession].state != FBSessionStateOpenTokenExtended) {
        
        //The openActiveSessionWithPersmissions:allowLoginUI public method of the AppDelegate is first called, which invokes in turn the openActiveSessionWithReadPermissions:allowLoginUI:completionHandler of the FBSession class. Once the whole login process is over, we inform the ViewController using a notification, where we'll handle the various session states in a while.
        [self.appDelegate openActiveSessionWithPermissions:@[@"public_profile",@"email"] allowLoginUI:YES];
        
        
    }
    */
    //facebook sign in
}
- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//here we'll handle the session states we are interested in.
//FBSessionStateOpen: When a session is open
//FBSessionStateClosed: When a session is closed
//FBSessionStateClosedLoginFailed: When the user    CANCELS   the login/authorization process

//1.From the notification paramater object, we'll extract the dictionary.
//2. If no error occurred, then if the session is open, we'll amke a call to Facebook Graph API to get all the info we want. If the session is closed or failed, notify the UI
//3 If an error occurred, we'll output the error description and perform any UI-related tasks.
-(void)handleFBSessionStateChangeWithNotification:(NSNotification *)notification{
    
    
    //Get the session, state and error values from the notifications userInfo dictionary
    //NSDictionary * userInfo = [notification userInfo];
    
    /*
    FBSessionState sessionState = [[userInfo objectForKey:@"state"] integerValue];
    NSError * error = [userInfo objectForKey:@"error"];
    
    //handle the session state
    //Usually the only interesting states are the opened session, the closed session and the failed login.
    if (!error) {
        
        //In case that there's not any error, then check if the session is opened or closed
        
        if (sessionState==FBSessionStateOpen) {
            
            //The session is    OPEN    . Get the uesr information and update the UI
            
            [FBRequestConnection startWithGraphPath:@"me" parameters:@{@"fields":@"first_name, last_name, picture.type(normal), email, gender, birthday, age_range"} HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                
                if (!error) {
                    
                    NSLog(@"Result %@",result);
                    
                    NSLog(@"Session token is still alive, so log in automatically");
                    
                    //register user
                    
                    NSLog(@"______AccessToken______ %@",[[FBSession activeSession] accessTokenData]);
                    // Close an existing session.
                    //[[FBSession activeSession] closeAndClearTokenInformation];
                    
                    

                }
                else{
                    
                    //A graph error occurred
                    NSLog(@"A graph error occurred %@", [error localizedDescription]);
                }
            }];
            
            
            
            NSLog(@"You are logged in");
            
            
            
        }
        else if (sessionState==FBSessionStateClosed || sessionState == FBSessionStateClosedLoginFailed){
            
            // A session was closed or the login was failed or canceled. Update the UI Accordingly
            NSLog(@"You are NOT logged in");
        }
    }
    else{
        
        //Facebook error occurred
        NSLog(@"Facebook error has occurred %@",error);
    }
     */
}
@end
