//
//  PartnerAccessViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 29/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PartnerAccessViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
