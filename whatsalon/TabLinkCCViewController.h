//
//  TabLinkCCViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 07/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header TabLinkCCViewController.h
  
 @brief This is the header file for adding a credit card.
  
 
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.3
 */

#import <UIKit/UIKit.h>
#import "FUIButton.h"


/*!
     @protocol LinkCCDelegate
  
     @brief The LinkCCDelegate protocol
  
     It's a protocol to notify the delegate to update its appearance.
 */

@protocol LinkCCDelegate <NSObject>

@optional
/*! @brief performed when a card is added. */
-(void)didAddCard;

/*! @brief is performed when the skip card action is triggered. */
-(void)didSkipCard;

@end

@interface TabLinkCCViewController : UIViewController

/*! @brief represents the LinkCCDelegate. */
@property (nonatomic,assign) id<LinkCCDelegate> delegate;

/*! @brief represents the view for holding the credit card number. */
@property (weak, nonatomic) IBOutlet UIView *viewForCCNumber;

/*! @brief represents the view for the first name. */
@property (weak, nonatomic) IBOutlet UIView *viewForFirstName;

/*! @brief represents the view for expiration. */
@property (weak, nonatomic) IBOutlet UIView *viewForExpiration;

/*! @brief represents the navigation bar view. */
@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;

/*! @brief represents the navigation bar. */
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;

/*! @brief represents the cancel bar button. */
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelBarButton;

/*! @brief represents the textfield for the card number. */
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextField;

/*! @brief represents the textfield for the users first name. */
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;

/*! @brief represents the textfield for the users last name. */
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;

/*! @brief represents the textfield for the cvv textfield. */
@property (weak, nonatomic) IBOutlet UITextField *cVVTextField;

/*! @brief represents the button for linking the card to the users account. */
@property (weak, nonatomic) IBOutlet FUIButton *linkButton;

/*! @brief represents if a credit card is only being added. */
@property (nonatomic,assign) BOOL onlyAddCard;

//last min
/*! @brief determines if the credit card being added from Last Minute process. */
//@property (nonatomic) BOOL isFromLastMin;

/*! @brief represents the cancel action. */
- (IBAction)cancel:(id)sender;

/*! @brief represents the linking card action. */
- (IBAction)linkCCCard:(id)sender;

/*! @brief represents the skip card action. */
- (IBAction)skipCCEntry:(id)sender;

/*! @brief determines if the UIViewCotnroller is being presented from settings. */
//@property (nonatomic) BOOL isFromSettings;

/*! @brief determines if the UIViewController is being presented from a local sign up screen. */
//@property (nonatomic) BOOL isFromLocalSignUpBookingScreen;
@end
