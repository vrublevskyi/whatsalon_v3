//
//  TabMyBookingsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabMyBookingsViewController.h"
#import "WSCore.h"
#import "SalonReviewViewController.h"//tab
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SocialShareViewController.h"//tab
#import "BookingTableViewCell.h"
#import "User.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NoBookingMessageView.h"
#import "UIView+AlertCompatibility.h"
#import "GCNetworkManager.h"
#import "BrowseViewController.h"
#import "TabCongratulationsViewController.h"
#import "GetDirectionsViewController.h"
#import "TabReviewOptionsViewController.h"
#import "UILabel+Boldify.h"
#import "UITableView+ReloadTransition.h"
#import "OpeningDay.h"
#import "LoginView.h"
#import "TabLoginOrSignUpViewController.h"
#import "TabNewSalonDetailViewController.h"
#import "MyBookingsCell.h"
#import "EmptyBookingView.h"
#import "whatsalon-Swift.h"
#import "UIScrollView+EmptyDataSet.h"

@interface TabMyBookingsViewController ()<UIViewControllerTransitioningDelegate,GCNetworkManagerDelegate,BookingsReviewDelegate,LoginDelegate,EmptyBookingViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>


/*! @brief represent the NSMutableArray of booking objects. */
@property (nonatomic,strong) NSMutableArray * bookingsArray;

/*! @brief determines if a review has been submitted. */
@property (nonatomic,assign) BOOL hasSubmittedReview;

/*! @brief represents the refresh control. */
@property (nonatomic) UIRefreshControl * refreshControl;

/*! @brief represents the network manager for making network requests. */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief represents the network manager for deleting. */
@property (nonatomic) GCNetworkManager * deleteEntryNetworkManager;


/*! @brief represents the upcoming bookings section. */
@property (weak, nonatomic) IBOutlet UIView *buttonsContainerView;
@property (weak, nonatomic) IBOutlet UIButton *upcoming;

/*! @brief represents the previous booking section. */
@property (weak, nonatomic) IBOutlet UIButton *previous;

/*! @brief represents an array of buttons. */
@property (nonatomic) NSMutableArray * buttons;


/*! @brief determines if upcoming bookings section is showing. */
@property (nonatomic) BOOL isUpcomingBookings;

/*! @brief represents the upcoming selected view. */
@property (nonatomic) UIView *upcomingSelectedView;

/*! @brief represents the upcoming bookings array. */
@property (nonatomic) NSMutableArray * upcomingBookingsArray;

/*! @brief represents the previous bookings array. */
@property (nonatomic) NSMutableArray * previousBookingsArray;

/*! @brief determines if it is the views first load or not. */
@property (nonatomic) BOOL isFirstLoad;

/*! @brief determines if login view is showing. */
@property (nonatomic) BOOL loginViewIsShowing;

/*! @brief represents the login view place holder. */
@property (nonatomic) IBOutlet UIView *loginViewPlaceHolder;

/*! @brief determines whether to relaod the view or not. */
@property (nonatomic) BOOL reloadView;

/*! @brief determines whether to show previous bookings or not. */
@property (nonatomic) BOOL showPrevious;

@end

@implementation TabMyBookingsViewController

#pragma mark - UIViewController LifeCycle


-(void)notificationSetUp{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshBookingScreen) name:kTab_3_Notification object:nil];
}
-(void)refreshBookingScreen{
    
    if (self.upcomingBookingsArray.count>0 || self.previousBookingsArray.count>0) {
        if (self.upcomingBookingsArray.count>0) {
            [self.upcomingBookingsArray removeAllObjects];
        }
        if (self.previousBookingsArray.count>0) {
            [self.previousBookingsArray removeAllObjects];
        }
        
        
        [self.bookingTableView reloadData];
    }
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationSlide];

    
    [self removeNoBookingsView];
    if ([[User getInstance] isUserLoggedIn]) {
        //[self userIsNotLoggedInView];
        [self removeUserIsNotLoggedInView];
        [self reloadBookings];
    
    }
    else{
          [self userIsNotLoggedInView];
    }
   

}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    
    UIView * view = [[UIView alloc] initWithFrame:self.bookingTableView.frame];
    view.backgroundColor=[UIColor clearColor];
    
    EmptyBookingView * emptyView = [[EmptyBookingView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    emptyView.delegate = self;
    
    return emptyView;
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    _buttonsContainerView.layer.masksToBounds = true;
    _buttonsContainerView.layer.borderWidth = 2;
    _buttonsContainerView.layer.cornerRadius = 4;
    [Gradients addLightBlueToBlueWithPurpleBorderWithView:_buttonsContainerView];

    self.bookingTableView.emptyDataSetSource = self;
    self.bookingTableView.emptyDataSetDelegate = self;
    
    // A little trick for removing the cell separators
    self.bookingTableView.tableFooterView = [UIView new];
}
/*
 Ensure that the statusBar is black
 */


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
        [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore flatNavBarOnView:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
    self.navigationItem.title = @"Bookings";
    
    if ([[User getInstance] isUserLoggedIn]) {
        if (self.reloadView) {
            
            
            
            self.reloadView=NO;
            self.isUpcomingBookings=YES;
            [self refreshBookingScreen];
        }

        else if ( self.isFirstLoad) {
           
            [self fetchPreviousBookings];
        }

    }
     
    
}



-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.networkManager cancelTask];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    [self.deleteEntryNetworkManager cancelTask];
}


/*
 Set up on viewDidLoad
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self notificationSetUp];
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView = self.view;
    
    self.deleteEntryNetworkManager = [[GCNetworkManager alloc] init];
    self.deleteEntryNetworkManager.delegate =self;
    self.deleteEntryNetworkManager.parentView=self.view;
    
    [WSCore returnNavBar:self.navigationController];
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bookingTableView.frame.size.width, 100)];
    self.bookingTableView.tableFooterView=footerView;
    self.bookingTableView.delegate = self;
    self.bookingTableView.dataSource = self;
    //self.bookingsArray = [[NSMutableArray alloc] init];
    self.upcomingBookingsArray = [NSMutableArray array];
    self.previousBookingsArray = [NSMutableArray array];
    

    if ([[User getInstance] isUserLoggedIn]) {
         [self fetchPreviousBookings];
    }
   
    
    self.hasSubmittedReview=NO;//default ****
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.bookingTableView addSubview:self.refreshControl];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatest)
                  forControlEvents:UIControlEventValueChanged];
    
//    if (self.navigationController.navigationBar.tintColor ==[UIColor whiteColor]) {
//        [WSCore nonTransparentNavigationBarOnView:self];
//        self.navigationController.navigationBar.tintColor=self.view.tintColor;
//    }
//
    [self.bookingTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.upcoming.tag=0;
    self.previous.tag=1;
    self.buttons =[[NSMutableArray alloc] initWithObjects:self.upcoming,self.previous, nil];
    
    [self.upcoming setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    [self.previous setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    self.upcoming.titleLabel.font = [UIFont systemFontOfSize:10.0f];
    self.previous.titleLabel.font = [UIFont systemFontOfSize: 10.0f];

    self.showPrevious = [[NSUserDefaults standardUserDefaults] boolForKey:kShouldShowPreviousBookings];
    
    if (self.showPrevious) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShouldShowPreviousBookings];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self selectedButton:self.previous];
    }else{
         [self selectedButton:self.upcoming];
    }
   
    
    [self.upcoming addTarget:self action:@selector(selectedButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.previous addTarget:self action:@selector(selectedButton:) forControlEvents:UIControlEventTouchUpInside];
    
  
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeLeft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeRight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
    
    
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    self.loginViewPlaceHolder.backgroundColor = [UIColor clearColor];
    [self registerCells];
}

-(void)swipe: (UISwipeGestureRecognizer *)swipe{
    
    
    if (swipe.direction==UISwipeGestureRecognizerDirectionLeft) {
        [self selectedButton:self.previous];
    }else if(swipe.direction==UISwipeGestureRecognizerDirectionRight){
        [self selectedButton:self.upcoming];
    }
}


-(void) registerCells {
    
    [self.bookingTableView registerNib:[UINib nibWithNibName:@"MyBookingsCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"MyBookingsCell"];
}

-(void)selectedButton: (UIButton *) button{
    
    if (button.tag==0) {
        self.isUpcomingBookings=YES;
        self.previous.backgroundColor = [UIColor whiteColor];
        [self.previous setTitleColor:kWhatSalonSubTextColor forState:UIControlStateNormal];
        [self.upcoming setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

        [Gradients addBookingUpcommingButtonLayerWithView:_upcoming];

    }else{
        self.isUpcomingBookings=NO;
        self.upcoming.backgroundColor = [UIColor whiteColor];
        [self.upcoming setTitleColor:kWhatSalonSubTextColor forState:UIControlStateNormal];
        [self.previous setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [Gradients addPreviousUpcommingButtonLayerWithView:_previous];

    }

    [self.bookingTableView reloadDataAnimated:YES];
}


-(void)userIsNotLoggedInView{
    
    self.loginViewIsShowing=YES;
    self.loginViewPlaceHolder.hidden=NO;
    self.bookingTableView.hidden=YES;
    self.upcoming.hidden=YES;
    self.previous.hidden=YES;
    self.buttonsContainerView.hidden = true;
    
    UIImageView * backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.loginViewPlaceHolder.frame.size.width, self.loginViewPlaceHolder.frame.size.height)];
    backgroundImage.image = [UIImage imageNamed:kMy_Bookings_placeholder_Image_Name];
    backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    UIView * blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.loginViewPlaceHolder.frame.size.width, self.view.frame.size.height)];
    blackView.backgroundColor = kGoldenTintForOverView;
    [backgroundImage addSubview:blackView];
    self.loginViewPlaceHolder.layer.masksToBounds
    =YES;
    [self.loginViewPlaceHolder addSubview:backgroundImage];
    
    LoginView * noMessage = [[LoginView alloc] init];
    noMessage.view.backgroundColor = [UIColor clearColor];
    [self.loginViewPlaceHolder addSubview:noMessage];
  
    noMessage.center=backgroundImage.center;
    
   
    noMessage.titleLabel.text = @"MY BOOKINGS";
    noMessage.titleLabel.font = [UIFont boldSystemFontOfSize:26.0f];
    [noMessage.loginButton addTarget:self action:@selector(showLoginViewForMyBookings) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)userDidLoginWithFacebook{
   
    self.reloadView=YES;
   
}
-(void)showLoginViewForMyBookings{
    //Go to login controller

    TabLoginOrSignUpViewController * tab = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignUpVC"];
    tab.delegate=self;
    [WSCore setLoginType:LoginTypeMyBookings];
    [self presentViewController:tab animated:YES completion:nil];
}
-(void)removeUserIsNotLoggedInView{
  
    self.upcoming.hidden=NO;
    self.previous.hidden=NO;
    self.loginViewPlaceHolder.hidden=YES;
    self.loginViewIsShowing=NO;
    self.bookingTableView.hidden=NO;
    self.buttonsContainerView.hidden = false;
    
    
}
/*
 Adds a view to the background view of the tableview when there are no bookings.
 
 */

- (IBAction)unwindToTabMyBookings:(UIStoryboardSegue *)unwindSegue{
   
    self.reloadView=YES;
    [WSCore resetLoginType];
    [self selectedButton:self.upcoming];
}
//- (void)addNoBookingsView {
//
//
//    UIView * view = [[UIView alloc] initWithFrame:self.bookingTableView.frame];
//    view.backgroundColor=[UIColor clearColor];
//
//    EmptyBookingView * emptyView = [[EmptyBookingView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
//    emptyView.delegate = self;
//    [self.bookingTableView addSubview:emptyView];
//}



/*
 Triggered by the NoBookingsViews when the imageView is pressed
 */
-(void)toBrowseTab{
 
    [WSCore setBookingType:BookingTypeBrowse];
    [self.tabBarController setSelectedIndex:0];
    
}



/*
 Removes the No bookings view
 
 */
- (void)removeNoBookingsView {
    self.bookingTableView.backgroundView=nil;
}

#pragma mark - GCNetworkManagerDelegate
/*
 Handles the success response from GCNetworkManagerDelegate
 */
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
  
    if (manager==self.networkManager) {
       
        NSArray * bookingsArray = jsonDictionary[@"message"][@"results"];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        
        for (NSDictionary *bDict in bookingsArray) {
            
            MyBookings * booking = [MyBookings bookingWithID:[NSString stringWithFormat:@"%@",bDict[@"booking_id"]] ];
            
            booking.bookingCreatedData = bDict[@"booking_created_date"];
            booking.bookingDeposit = bDict[@"booking_deposit"];
            booking.bookingEndTime = bDict[@"booking_end_time"];
            booking.bookingPaid = [bDict[@"booking_piad"] boolValue];
            
            booking.bookingPaidDate = bDict[@"booking_paid_date"];
            booking.bookingStartTime = bDict[@"booking_start_time"];
            booking.bookingTotal = bDict[@"booking_total"];
            
            if (bDict[@"staff_first_name"]!=[NSNull null]) {
                
                booking.stylistName=[NSString stringWithFormat:@"%@ %@",bDict[@"staff_first_name"],bDict[@"staff_last_name"]];
            }
            //set up salon object
            
            booking.bookingSalon.salon_id = [NSString stringWithFormat:@"%@",bDict[@"salon_id"] ];
            if (bDict[@"rating"] != [NSNull null]) {
                booking.bookingSalon.ratings = [bDict[@"salon_details"][@"rating"] doubleValue];
            }
            
            booking.bookingSalon.salon_name=bDict[@"salon_name"];
            booking.bookingSalon.salon_image=bDict[@"salon_details"][@"salon_image"];
            booking.bookingSalon.salon_description=bDict[@"salon_details"][@"salon_description"];
            booking.bookingSalon.salon_landline=bDict[@"salon_details"][@"salon_landline"];
            booking.bookingSalon.salon_address_1=bDict[@"salon_details"][@"salon_address_1"];
            booking.bookingSalon.salon_address_2=bDict[@"salon_details"][@"salon_address_2"];
            booking.bookingSalon.salon_address_3=bDict[@"salon_details"][@"salon_address_3"];
            booking.bookingSalon.is_favourite = [bDict[@"salon_details"][@"is_favourite"] boolValue];
            booking.active = [bDict[@"active"] boolValue];
            booking.reviewOccurred = [bDict[@"review_occurred"] boolValue];
            booking.bookingOccurred = [bDict[@"booking_occurred"] boolValue] ;
            booking.isCancelled = [bDict[@"is_cancelled"] boolValue];
            
            //currency
      
            if (bDict[@"salon_details"][@"salon_currency"][@"currency_name"] !=[NSNull null] && bDict[@"salon_details"][@"salon_currency"][@"currency_id"] !=[NSNull null] && bDict[@"salon_details"][@"salon_currency"][@"symbol_utf8"] !=[NSNull null]) {
                
                [booking.bookingSalon updatedCurrencyName:bDict[@"salon_details"][@"salon_currency"][@"currency_name"] WithCurrencyId:bDict[@"salon_details"][@"salon_currency"][@"currency_id"] AndWithSymbolUtf8:bDict[@"salon_details"][@"salon_currency"][@"symbol_utf8"]];
            }

            
            if ([bDict[@"salon_details"][@"salon_latest_review"]count]!=0) {
                
                booking.bookingSalon.hasReview=YES;
                SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:bDict[@"salon_details"][@"salon_latest_review"][@"user_name"] AndWithLastName:bDict[@"salon_latest_review"][@"user_last_name"]];
                salonReview.message = bDict[@"salon_details"][@"salon_latest_review"][@"message"];
                salonReview.review_image=bDict[@"salon_details"][@"salon_latest_review"][@"review_image"];
                salonReview.salon_rating=bDict[@"salon_details"][@"salon_latest_review"][@"salon_rating"];
                salonReview.stylist_rating=bDict[@"salon_details"][@"salon_latest_review"][@"stylist_rating"];
                salonReview.date_reviewed=bDict[@"salon_details"][@"salon_latest_review"][@"date_reviewed"];
                salonReview.user_avatar_url=bDict[@"salon_details"][@"salon_latest_review"][@"user_avatar"];
                booking.bookingSalon.salonReview=salonReview;
                
            }
            
            if (bDict[@"salon_details"][@"salon_images"]!=[NSNull null]) {
                
                for (NSDictionary *imageDict in bDict[@"salon_details"][@"salon_images"]) {
                    NSString * url = imageDict[@"image_path"];
                    
                    [booking.bookingSalon.slideShowGallery addObject:url];
                    
                }
            }
            
            
            
            booking.bookingSalon.salon_lat=[bDict[@"salon_details"][@"salon_lat"] doubleValue];
            booking.bookingSalon.salon_long=[bDict[@"salon_details"][@"salon_lon"] doubleValue];
            
            
            booking.staffID=[NSString stringWithFormat:@"%@",bDict[@"staff_id"]];
            if (bDict[@"service_name"]!=[NSNull null]) {
                booking.servceName = bDict[@"service_name"];
                
            }
            
            
            if (bDict[@"rating"]!=[NSNull null]) {
                booking.bookingSalon.ratings = [bDict[@"rating"] doubleValue];
            }
            if (bDict[@"reviews"]!=[NSNull null]) {
                booking.bookingSalon.reviews = [bDict[@"reviews"] doubleValue];
            }
            
            if ([bDict[@"salon_details"][@"salon_categories"] count]!=0) {
                booking.bookingSalon.salonCategories =  @{
                                                          @"Hair" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Hair"] boolValue]],
                                                          @"Nails" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Nails"]boolValue]],
                                                          @"Face" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Face"] boolValue]],
                                                          @"Body" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Body"] boolValue]],
                                                          @"Massage" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Massage"] boolValue]],
                                                          @"Hair Removal" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Hair Removal"] boolValue]],
                                                          };
            }
            
            
            if ([bDict[@"salon_details"][@"salon_opening_hours"] count]!=0) {
         
                NSArray * openingHours =bDict[@"salon_details"][@"salon_opening_hours"];
                
                for (NSDictionary* day in openingHours) {
                   
                    
                    OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
                    
                    [booking.bookingSalon.openingHours addObject:openingDay];
                    
                }
                
            }
            

            //NSDate *date = [dateFormat dateFromString:booking.bookingEndTime];
            //date =[date dateByAddingTimeInterval:60*30];

            //if ([date timeIntervalSinceNow] < 0.0) {
            if(booking.bookingOccurred  ){
                // Date has passed
                
               // NSLog(@"Previous Booking object %@",bDict);
                [self.previousBookingsArray addObject:booking];
                
            }
           
            else{
                [self.upcomingBookingsArray addObject:booking];
            }
            
           
        }
        
        [self.bookingTableView reloadData];
        self.isFirstLoad=YES;
        
        self.hasSubmittedReview=NO;
        
    }else if(manager==self.deleteEntryNetworkManager){
        
        
    }
    
}

/*
 Handles the failure message
 */
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    self.bookingTableView.hidden=NO;
    
    if (manager==self.networkManager) {
        [WSCore showServerErrorAlert];
    }
    else if(manager==self.deleteEntryNetworkManager){
        [UIView showSimpleAlertWithTitle:@"Oh Oh!" message: jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
    }
    

}

/*
 Fetch the Booking History
 */
-(void)fetchPreviousBookings{
    
  
    self.previousBookingsArray = [NSMutableArray array];
    self.upcomingBookingsArray = [NSMutableArray array];
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kBooking_History_URL]];
    
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    if ([[User getInstance] fetchKey].length>0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
    }
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}


- (void) onMakeBookingTapped: (EmptyBookingView *) sender {
    [self toBrowseTab];
}
/*
 Refreshes the TableView
 */
-(void)getLatest{
    //[self.bookingsArray removeAllObjects];
    [self.upcomingBookingsArray removeAllObjects];
    [self.previousBookingsArray removeAllObjects];
    [self fetchPreviousBookings];
    
    [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}


/*
 Reloads the tableview and handles the refreshControl
 
 */
- (void)reloadData
{
    
    
    [self.bookingTableView reloadData];
    
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}







#pragma mark - UITableView datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.isUpcomingBookings) {
        if (self.upcomingBookingsArray.count==0) {
       
            
                if (![[User getInstance] isUserLoggedIn]) {
                   
                    [self userIsNotLoggedInView];
                }else{
                  
                    if (self.loginViewIsShowing) {
                        [self removeUserIsNotLoggedInView];
                    }
                }
            
            return 0;
        }
        
        if (![[User getInstance] isUserLoggedIn]) {
            [self userIsNotLoggedInView];
        }
        else{
            if (self.loginViewIsShowing) {
                [self removeUserIsNotLoggedInView];
            }
             [self removeNoBookingsView];
        }
       
        
        return self.upcomingBookingsArray.count;
    }
    else{
        //previous
        if (self.previousBookingsArray.count==0) {
            if (![[User getInstance] isUserLoggedIn]) {
                [self userIsNotLoggedInView];
            }else{
                if (self.loginViewIsShowing) {
                    [self removeUserIsNotLoggedInView];
                }
            }
            
            return 0;
        }
        
        
        if (![[User getInstance] isUserLoggedIn]) {
            [self removeUserIsNotLoggedInView];
        }
        else{
            if (self.loginViewIsShowing) {
                [self removeUserIsNotLoggedInView];
            }
            
            [self removeNoBookingsView];
        }
        
        return self.previousBookingsArray.count;
    }
    
    return 0;
 
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
     MyBookingsCell *cell = (MyBookingsCell *) [self.bookingTableView dequeueReusableCellWithIdentifier:@"MyBookingsCell"];
    MyBookings *booking;
    if (self.isUpcomingBookings) {
        booking = self.upcomingBookingsArray[indexPath.row];
    } else {
        booking = self.previousBookingsArray[indexPath.row];
    }
    cell.booking = booking;
    
//    BookingTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    
//    MyBookings * bookings;
//    if (self.isUpcomingBookings) {
//        cell.isPrevious=NO;
//        bookings = [self.upcomingBookingsArray objectAtIndex:indexPath.row];
//    }
//    else{
//        cell.isPrevious=YES;
//        bookings = [self.previousBookingsArray objectAtIndex:indexPath.row];
//    }
//    cell.salon=bookings.bookingSalon;
//
//    [cell updateCellWithBooking:bookings];
//    if (bookings.isCancelled) {
//        cell.userInteractionEnabled=NO;
//    }
//    else{
//        cell.userInteractionEnabled=YES;
//    }
    
    return cell;
}

/*
 Handles the selection of the tableview
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    MyBookings * booking;
    if (self.isUpcomingBookings) {
        booking = [self.upcomingBookingsArray objectAtIndex:indexPath.row];
    }
    else{
        booking = [self.previousBookingsArray objectAtIndex:indexPath.row];
    }
    
    if (booking.isCancelled) {
        
    }

    if (!booking.isCancelled) {
        TabReviewOptionsViewController *destinationViewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"optionsVC"];
        destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
        destinationViewController.transitioningDelegate = self;
        destinationViewController.bookingData = booking;
        destinationViewController.delegate=self;
        
        [self presentViewController:destinationViewController animated:YES completion:^{
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }];
        
    }    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 150 ;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}


#pragma mark - Review Delegate
-(void)reloadBookings{
    self.hasSubmittedReview=YES;
    //[self.bookingsArray removeAllObjects];
    [self.upcomingBookingsArray removeAllObjects];
    [self.previousBookingsArray removeAllObjects];
    [self fetchPreviousBookings];
}

#pragma mark - UITableView Delegate
/*
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 //delete cells
 [self deleteBookingAtIndexPathRow:indexPath.row];
 [self.bookingsArray removeObjectAtIndex:indexPath.row];
 
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 
 if (self.bookingsArray.count==0) {
 if (self.bookingTableView.backgroundView==nil) {
 [self addNoBookingsView];
 
 }
 }
 else if(self.bookingTableView.backgroundView!=nil){
 [self removeNoBookingsView];
 }
 
 }
 
 }
 
 -(void)deleteBookingAtIndexPathRow: (NSInteger) row{
 NSLog(@"DELETE");
 
 
 MyBookings * booking = [self.bookingsArray objectAtIndex:row];
 
 
 NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kDelete_Booking_URL]];
 NSString * params = [NSString stringWithFormat:@"confirm_id=%@",booking.bookingId];
 params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken]]];
 
 [self.deleteEntryNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
 
 
 }
 */

/*
 Unwind segue - called when user makes a booking via the my bookings sections
 */
/*
- (IBAction)unwindToTabMyBookings:(UIStoryboardSegue *)unwindSegue
{
    UIViewController* sourceViewController = unwindSegue.sourceViewController;
    
    if ([sourceViewController isKindOfClass:[TabCongratulationsViewController class]])
    {
        [WSCore statusBarColor:StatusBarColorBlack];
        
        [WSCore nonTransparentNavigationBarOnView:self];
    }
}
*/

#pragma mark - BookingsReviewDelegate Methods
-(void)bookAgainWithSalon:(Salon *)salon{
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    TabNewSalonDetailViewController * sVC = [self.storyboard instantiateViewControllerWithIdentifier:@"newSalonDetailVC"];//new viewcontroller instead of tableviewcontroller
    salon.isReBooking=YES;
    sVC.salonData = salon;
    [WSCore setBookingType:BookingTypeMybookings];
    sVC.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:sVC animated:YES];
    self.hidesBottomBarWhenPushed=NO;
    
}

-(void)canceledAppointment:(MyBookings *)booking {
    
    
    for (int i=0 ; i<self.upcomingBookingsArray.count; i++) {
        
        MyBookings * b = [self.upcomingBookingsArray objectAtIndex:i];
        if ([b.bookingId isEqualToString:booking
             .bookingId]) {
            
            b.isCancelled=YES;
            [self.upcomingBookingsArray replaceObjectAtIndex:i withObject:b];
        }
        
    }
    [self.bookingTableView reloadData];
    
    
}

-(void)addedReview{
    
    [WSCore statusBarColor:StatusBarColorBlack];
    [self fetchPreviousBookings];
}


@end

