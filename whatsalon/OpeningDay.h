//
//  OpeningDay.h
//  whatsalon
//
//  Created by Graham Connolly on 13/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpeningDay : NSObject<NSCoding>


/*! @brief represents the day of the week. */
@property (nonatomic) NSString *dayOfWeek;

/*! @brief determines if the salon is open. */
@property (nonatomic) BOOL isOpened;

/*! @brief represents the start time of the salon on this day. */
@property (nonatomic) NSString *startTime;

/*! @brief represents the end time of the salon on this day. */
@property (nonatomic) NSString * endTime;

/*! @brief represents the day name. */
@property (nonatomic) NSString * dayName;

/*! @brief creates an instancetype of OpeningDay
 
    @param day_of_week - represents the day of the week
 
    @param startTime - represents the start time.

    @param endTime - represents the end time. 
 
    @param day_name - represents the name of the day.
 
    @param isOpened - determines if it is open today. 
 */
-(instancetype) initWithDayOfWeek :(NSString *)day_of_week WithStartTime: (NSString *)startTime WithEndTime: (NSString *) endTime WithDayName: (NSString *) day_name AndIsOpene: (BOOL) isOpened;

/*! @brief creates an instancetype of OpeningDay
 
    @param day_of_week - represents the day of the week
 
    @param startTime - represents the start time.
 
    @param endTime - represents the end time.
 
    @param day_name - represents the name of the day.
 
    @param isOpened - determines if it open today.
 */

+(instancetype) openingDayWithDayOfWeek:(NSString *)day_of_week WithStartTime:(NSString *)startTime WithEndTime:(NSString *)endTime WithDayName:(NSString *)day_name AndIsOpene:(BOOL)isOpened;

@end
