//
//  TabConfirmViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header TabConfirmViewController.h
  
 @brief This is the header file for the TabConfirmViewController.h
  
 This is the file used to confirm the booking.
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    -
 */
#import <UIKit/UIKit.h>

static NSMutableArray *bookingArray;

@interface TabConfirmViewController : UIViewController

/*! @brief represents the view for holding the salon information. */
@property (weak, nonatomic) IBOutlet UIView *viewForSalonInfo;

/*! @brief represents the view for holdering the salon stylist. */
@property (weak, nonatomic) IBOutlet UIView *viewForSalonStylist;

/*! @brief represents the view for the price. */
@property (weak, nonatomic) IBOutlet UIView *viewForPrice;

/*! @brief represents the navigation bar. */
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UIImageView *salonImageVIew;

/*! @brief represents the cancel bar button item. */
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelBarButtonItem;

/*! @brief represents the cancel action. */
- (IBAction)cancel:(id)sender;

/*! @brief represents the salon label. */
@property (weak, nonatomic) IBOutlet UILabel *salonLabel;

/*! @brief represents the salon name label. */
@property (weak, nonatomic) IBOutlet UILabel *salonNameLabel;

/*! @brief represents the address label. */
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

/*! @brief represents the address name label. */
@property (weak, nonatomic) IBOutlet UILabel *addressNameLabel;

/*! @brief represents the service label. */
@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;

/*! @brief represents the service name label. */
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;

/*! @brief represents the stylist label. */
@property (weak, nonatomic) IBOutlet UILabel *stylistLabel;

/*! @brief represents the stylist name label. */
@property (weak, nonatomic) IBOutlet UILabel *stylistNameLabel;

/*! @brief represents the date label. */
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

/*! @brief represents the date selected for the appointment label. */
@property (weak, nonatomic) IBOutlet UILabel *dateSelectedLabel;

/*! @brief represent the full price amount label. */
@property (weak, nonatomic) IBOutlet UILabel *fullPriceAmountLabel;

/*! @brief represent the price that needs to be paid at the salon. */
@property (weak, nonatomic) IBOutlet UILabel *priceAtSalonLabel;

/*! @brief represents the discount amount label. */
@property (weak, nonatomic) IBOutlet UILabel *discountAmountLabel;

/*! @brief represents label for the instructions to trace. */
@property (weak, nonatomic) IBOutlet UILabel *instructionsToTraceLabel;

/*! @brief represents the confirmation button. */
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

/*! @brief represents the Booking object. */
@property (nonatomic) Booking * bookingObj;

/*! @brief the chosen service for the appointment. */
@property (nonatomic) NSString * chosenService;

/*! @brief the chosen price for the appointment. */
@property (nonatomic) NSString * chosenPrice;

/*! @brief the chosen stylist for the appoinment. */
@property (nonatomic) NSString * chosenStylist;

/*! @brief the chosen time for the appointment. */
@property (nonatomic) NSString * chosenTime;

/*! @brief the chosen day for the appointment. */
@property (nonatomic) NSString * chosenDay;

/*! @brief the chosen date for the appointment. */
@property (nonatomic) NSDate * chosenDate;

/*! @brief the tutorial trace image. */
@property (weak, nonatomic) IBOutlet UIImageView *traceTutorialImageView;

/*! @brief the trace image view. */
@property (weak, nonatomic) IBOutlet UIImageView *traceImageView;

/*! @brief determines if a booking is being made through the search filter screen. */
//@property (nonatomic) BOOL unwindBackToSearchFilter;

/*! @brief the confirmation action button. */
- (IBAction)confirm:(id)sender;

/*! @brief the Salon booking object. */
@property (weak,nonatomic) Salon * salonData;

/*! @brief determines if a booking is being made from favourites. */
//@property (nonatomic) BOOL isFromFavourite;

/*! @brief determines if a booking is being made from the last minute. */
//@property (nonatomic) BOOL isFromLastMinute;

/*! @brief determines if this is coming from salon individual screen. */
//@property (nonatomic) BOOL isFromLastMinPopToSalonIndividual;

@end

