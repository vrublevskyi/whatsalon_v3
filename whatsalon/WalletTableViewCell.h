//
//  WalletTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 25/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h" 
#import "User.h"

@protocol WalletTableViewDelegate <NSObject>

@optional
-(void)didTriggerActionWithType: (BOOL) type;

@end
@interface WalletTableViewCell : UITableViewCell

@property (nonatomic) id<WalletTableViewDelegate> delegate;
@property (nonatomic) UILabel * cellTitleLabel;
@property (nonatomic) UILabel * cellSubTitleLabel;
@property (nonatomic) FUIButton * button;
@property (nonatomic) BOOL isShareCell;


-(void)setUpCellIsCodeCell: (BOOL) yn;
@end
