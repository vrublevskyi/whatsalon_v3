//
//  ExperiencesTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 25/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExperienceItem.h"
#import "GCNetworkManager.h"

@protocol ExperienceCellDelegate <NSObject>

@optional
-(void)didUnFavouriteBlogPostWithID: (NSString *)b_id;;
-(void)didFavouriteBlogPostWithID: (NSString *)b_id;

@end
@interface ExperiencesTableViewCell : UITableViewCell<GCNetworkManagerDelegate>

@property (nonatomic) id<ExperienceCellDelegate> myDelegate;
@property (nonatomic) UIView *holderView;
@property (nonatomic) UIImageView* headerImageView;
@property (nonatomic) UILabel * titleLabel;
@property (nonatomic) UILabel * previewLabel;
@property (nonatomic) UIView * bottomView;
@property (nonatomic) BOOL hasShadow;
@property (nonatomic) ExperienceItem *expItem;
@property (nonatomic)  UIImageView * heartImage;
@property (nonatomic) UILabel * numberOfHearts;
@property (nonatomic) GCNetworkManager * networkManager;
@property (nonatomic) BOOL isFav;

@property (nonatomic) UIView * tapView;
-(void)setUpExperiencesCellWithExperienceItem: (ExperienceItem *) exp;
@end
