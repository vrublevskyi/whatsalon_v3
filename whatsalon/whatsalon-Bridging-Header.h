//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import "TabDiscoveryViewController.h"
#import "TabSearchSalonsTableViewController.h"
#import "GCNetworkManager.h"
#import "Salon.h"
#import "TabNewSalonDetailViewController.h"
