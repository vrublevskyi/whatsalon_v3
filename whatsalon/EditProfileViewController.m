//
//  EditProfileViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 28/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "EditProfileViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "ProgressHUD.h"
#import "RESideMenu/RESideMenu.h"
#import "SettingsViewController.h"
#import "User.h"
#import "GCNetworkManager.h"
#import "UIView+AlertCompatibility.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AFHTTPRequestOperationManager.h"
#import <AVFoundation/AVFoundation.h>

//GCCameraViewController
#import "GCCameraViewController.h"

@interface EditProfileViewController () <UIActionSheetDelegate,GCNetworkManagerDelegate,UITextFieldDelegate,GCCameraDelegate>
/*!
 @brief represent an NSString for holding the firstName
 */
@property (nonatomic) NSString * firstName;

/*!
 @brief represents an NSString for holding the surname
 */
@property (nonatomic) NSString * surname;

/*!
 @brief represents an NSString for holding the gender
 */
@property (nonatomic) NSString * gender;

/*!
 @brief represents a GCNetworkManager instance for handling network calls
 */
@property (nonatomic) GCNetworkManager * editProfileNetworkManager;


@end

@implementation EditProfileViewController



#pragma mark - Views life cycle methods

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (IS_iPHONE4) {
        self.cameraAvatar.hidden=YES;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (IS_iPHONE4) {
        self.cameraAvatar.hidden=NO;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //instantiate GCNetworkManager
    self.editProfileNetworkManager = [[GCNetworkManager alloc] init];
    self.editProfileNetworkManager.delegate=self;
    self.editProfileNetworkManager.parentView = self.view;
    
    //set up navigation bar
    [self navigationBarSetUp];
    
    [WSCore addTopLine:self.viewForFirstName :[UIColor lightGrayColor] :0.5];
    [WSCore addBottomIndentedLine: self.viewForFirstName: kCellLinesColour];
    [WSCore addBottomIndentedLine:self.viewForSecondName: kCellLinesColour];
    [WSCore addBottomLine:self.viewForGender : kCellLinesColour];
    [WSCore addBottomLine:self.viewForNavigationBar: kCellLinesColour];

    self.genderSegmentedControl.tintColor=kWhatSalonBlue;
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:16.0f] } forState:UIControlStateSelected];
    
    self.firstName = [[NSUserDefaults standardUserDefaults] objectForKey:kFirstNameKey];
    self.surname = [[NSUserDefaults standardUserDefaults] objectForKey:kSurnameKey];
    self.gender = [[NSUserDefaults standardUserDefaults] objectForKey:kGenderKey] ;
    self.firstNameTextfield.text = self.firstName;
    self.surNameTextfield.text = self.surname;

    if ([self.gender caseInsensitiveCompare: kMale] == NSOrderedSame &&self.gender!=nil) {
       
        self.genderSegmentedControl.selectedSegmentIndex=0;
    }
    else if([self.gender caseInsensitiveCompare:kFemale] == NSOrderedSame &&self.gender!=nil){
        self.genderSegmentedControl.selectedSegmentIndex=1;
    }
    else{
        self.genderSegmentedControl.selectedSegmentIndex=UISegmentedControlNoSegment;
    }
    
    
    self.cameraAvatar.contentMode=UIViewContentModeScaleAspectFill;
    
    [self setProfileImage];
    
    self.firstNameTextfield.delegate = self;
    self.surNameTextfield.delegate=self;
}

- (void)setProfileImage {
    /*
     If the accessToken is not nil then attempt to recieve the imageData associated witht the users accessToken
     
     */
    if ([[User getInstance] isUserLoggedIn]) {
        
       
        //check if users image url is available
        if([[User getInstance] fetchImageURL]){
            NSLog(@"Use Image url %@",[[User getInstance] fetchImageURL]);
            
            [self.cameraAvatar sd_setImageWithURL:[[User getInstance] fetchImageURL ]
                                 placeholderImage:self.cameraAvatar.image ];
        }
        
        
    }
    self.cameraAvatar.layer.cornerRadius = roundf(self.cameraAvatar.frame.size.width/2.0);
    self.cameraAvatar.layer.masksToBounds = YES;
    self.cameraAvatar.contentMode=UIViewContentModeScaleAspectFill;
    self.cameraAvatar.layer.borderColor=[UIColor whiteColor].CGColor;
    self.cameraAvatar.layer.borderWidth=3.0f;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self setProfileImage];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    //add tap gesture to image avatar for selecting image
    UITapGestureRecognizer * tapPic = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCameraOptions:)];
    tapPic.numberOfTapsRequired=1;
    tapPic.delegate=self;
    self.cameraAvatar.userInteractionEnabled = YES;
    [self.cameraAvatar addGestureRecognizer:tapPic];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        self.imagePicker = nil;
    }
}


#pragma mark - camera methods

/*!
 * @description displays an action sheet containing camera options
 */
-(void)showCameraOptions: (UIGestureRecognizer *)tap{
    
    [self.view endEditing:YES];
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    NSLog(@"Status %ld",(long)status);
    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            // Will get here on both iOS 7 & 8 even though camera permissions weren't required
            // until iOS 8. So for iOS 7 permission will always be granted.
            if (granted) {
                // Permission has been granted. Use dispatch_async for any UI updating
                // code because this block may be executed in a thread.
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self actionSheetDialog];
                });
            } else {
                // Permission has been denied.
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    switch (status) {
                        case 1:
                            [UIView showSimpleAlertWithTitle:@"Permission Denied" message:@"Use of the camera has been restricted by the user. Please go to settings to allow WhatSalon to use your camera." cancelButtonTitle:@"OK"];
                            break;
                        case 2:
                            [UIView showSimpleAlertWithTitle:@"Permission Denied" message:@"Use of the camera has been denied by the user. Please go to settings to allow WhatSalon to use your camera." cancelButtonTitle:@"OK"];
                            break;
                        default:
                            [UIView showSimpleAlertWithTitle:@"Permission Denied" message:@"Use of the camera has been restricted by the user. Please go to settings to allow WhatSalon to use your camera." cancelButtonTitle:@"OK"];
                            break;
                    }
                    
                });
            }
        }];
    } else {
        // We are on iOS <= 6. Just do what we need to do.
        
        [self actionSheetDialog];
    }
    
    
}
-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    //reposition self.cameraAvatar based on screen size
    if (IS_iPHONE4) {
        self.cameraAvatar.frame = CGRectMake(120, 85, 80, 80) ;
        self.cameraAvatar.layer.cornerRadius = roundf(self.cameraAvatar.frame.size.width/2.0);
        self.cameraAvatar.layer.masksToBounds = YES;
        self.cameraAvatar.contentMode=UIViewContentModeScaleAspectFill;
        
    }
}



#pragma mark - camera methods


-(void)actionSheetDialog{
    UIActionSheet * actionSheet = [[UIActionSheet alloc]initWithTitle:@"Camera" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Picture",@"Choose Existing", nil];
    [actionSheet showInView:self.view];

}

//take picture uses simpleCam for better memory mananger
- (void)takePicture {
   
   
    GCCameraViewController * gcCamera = [[GCCameraViewController alloc] init];
    gcCamera.delegate=self;
    [self presentViewController:gcCamera animated:YES completion:nil];
    
}

#pragma mark - GCCamera Delegate
-(void)cameraView:(GCCameraViewController *)cameraView didFinishWithImage:(UIImage *)image{

    
        
    //if capturing photo is not available
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        [self choosePhoto];
        
    }
    else{
        
        if (image) {
            
            // simple cam finished with image
            // use the image's layer to mask the image into a circle
            self.cameraAvatar.layer.cornerRadius = roundf(self.cameraAvatar.frame.size.width/2.0);
            self.cameraAvatar.layer.masksToBounds = YES;
            self.cameraAvatar.contentMode=UIViewContentModeScaleAspectFill;
            self.cameraAvatar.image=image;
            
            //save the image to photos library
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
            // NSData *imageData = UIImageJPEGRepresentation(image,1);
            //[[User getInstance] updateUserImagePath:imageData];
            
            if (image) {
                [self saveImage:image];
                
                
            }
            
            
        }
        else {
            
            // simple cam finished w/o image
            [UIView showSimpleAlertWithTitle:@"Oops" message:@"The camera was unable to take the picture. Please try again later." cancelButtonTitle:@"OK"];
            
        }
        
                
    }

    
}


- (void)choosePhoto {
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing=NO;//set to NO - can cause memory issues
    
    //choose from photo library
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}



#pragma mark - UIActionSheet delegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex != [actionSheet cancelButtonIndex]) {
        
        if (buttonIndex==0) {
            [self takePicture];
        }
        else if(buttonIndex ==1){
            [self choosePhoto];
        }
    }
}



#pragma mark - custom methods
/*
 creates a tranparent navigation bar.
 Custom fonts
 cancelBarBUtton
 doneBarButton
 
 */
-(void)navigationBarSetUp{
    //make navigation bar completely transparent
    self.viewForNavigationBar.backgroundColor=[UIColor clearColor];
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
    
    //change font style of navigation bar title
    [self.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont systemFontOfSize:18],
      NSFontAttributeName, nil]];
    
    [self.cancelBarButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont  systemFontOfSize:18], NSFontAttributeName, nil] forState:UIControlStateNormal];
    [self.doneBarButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize: 18], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
}




#pragma mark - Image Picker Controller delegate
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.cameraAvatar.image = image;
    self.cameraAvatar.contentMode=UIViewContentModeScaleAspectFill;
    
    // use the image's layer to mask the image into a circle
    self.cameraAvatar.layer.cornerRadius = roundf(self.cameraAvatar.frame.size.width/2.0);
    self.cameraAvatar.layer.masksToBounds = YES;
    
    if (self.imagePicker.sourceType == UIImagePickerControllerSourceTypeCamera){
        
        //save the image to photos library
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    
    image=[WSCore resizeImage:image];
    //
    //NSData *imageData = UIImageJPEGRepresentation(image,1);
    
   // [[User getInstance] updateUserImagePath:imageData];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        [self saveImage:image];
        //blur menu image
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"BlurMenuImage" object:nil userInfo:nil];
    }];
}


-(void)saveImage: (UIImage *) image{

    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    NSString *encodedString = [imageData base64EncodedStringWithOptions:0];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters;
    if ([[User getInstance] fetchKey].length!=0) {
        parameters = @{@"user_id": [[User getInstance] fetchAccessToken],@"image":encodedString,@"secret_key":[[User getInstance] fetchKey]};
    }
    else{
        parameters = @{@"user_id": [[User getInstance] fetchAccessToken],@"image":encodedString};
    }
     NSLog(@"Parameters %@",parameters);
    [WSCore showNetworkLoadingOnView:self.view];
    
    [manager POST:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,@"update_user_avatar"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"%@", string);
        [WSCore dismissNetworkLoadingOnView:self.view];
       
        
        NSString * url = [NSString stringWithFormat:@"%@",responseObject[@"message"]];
        [[User getInstance] setImageURL:url];
        [self setProfileImage];
        
        
        //save the image to photos library
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        //save url
        //[self dismissViewControllerAnimated:YES completion:nil];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [WSCore dismissNetworkLoadingOnView:self.view];
        [UIView showSimpleAlertWithTitle:@"Oh Oh!" message:@"An error has occurred when changing you profile image. Please try again later"cancelButtonTitle:@"OK"];
        NSLog(@"Error: %@", error);
    }];

}
#pragma mark - IBActions
- (IBAction)genderChange:(UISegmentedControl *)sender {
    
    if(sender.selectedSegmentIndex==0){
    
        self.gender = kMale;
       
    }
    else if(sender.selectedSegmentIndex == 1){
    
        self.gender = kFemale;
    }
}

- (IBAction)takePicture:(id)sender {
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing=NO;//set to NO -can cause memory issues
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
       
    }else{
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    
    self.imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    [self presentViewController:self.imagePicker animated:YES completion:nil];

}
- (IBAction)chooseExistingImage:(id)sender {
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing=NO;//set to NO - can cause memory issues
    
    //choose from photo library
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}


- (IBAction)cancelEditProfile:(id)sender {
    
    if (![[User getInstance] hasFirstNameAndLastName]) {
        
        if (!IS_IOS_8_OR_LATER) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:@"In order to make a booking, a salon needs to know your profile information." delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
            alert.tag=333;
            [alert show];
        }
        else{
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Are you sure?"
                                                  message:@"In order to make a booking, a salon needs to know your profile information."
                                                  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:@"No"
                                           style:UIAlertActionStyleCancel
                                           handler:nil];
            
            UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [alertController addAction:yesAction];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }

    }
    else{
        //has first name
        [self dismissViewControllerAnimated:YES completion:^{
            
            if (self.isFromRightMenu) {
                [WSCore statusBarColor:StatusBarColorBlack];
            }
            /*
             If coming from Menu then change the status bar color
             */
           else if (self.isFromMenu) {
                
                [WSCore statusBarColor:StatusBarColorWhite];
            }
            
        }];
    }
    
    
}


- (IBAction)done:(id)sender {
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kUpdate_User_URL]];
    
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&name=%@",self.firstNameTextfield.text]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_last_name=%@",self.surNameTextfield.text]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&gender=%@",self.gender]];
    if ([[User getInstance] fetchKey].length!=0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
    }
    
    [self.editProfileNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
    
    
}

-(void)showSavedMessage{
    
    if (!IS_IOS_8_OR_LATER) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Saved" message:@"Your profile has been updated." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag=1177;
        [alert show];
    }
    else{
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Saved"
                                              message:@"Your profile has been updated."
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           if (self.isFromMenu||self.isFromRightMenu) {
                                               [self.myDelegate updateMenuProfile];
                                               [self dismissViewControllerAnimated:YES completion:^{
                                                   if (self.isFromMenu) {
                                                       [WSCore statusBarColor:StatusBarColorWhite];
                                                   }
                                                   
                                               }];
                                           }
                                           else{
                                              [self dismissViewControllerAnimated:YES completion:nil];
                                           }
                                          
                                       }];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


#pragma mark - UIAlertView delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag ==202) {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } else if (alertView.tag==1177) {
        if (buttonIndex==[alertView cancelButtonIndex]) {
            if (self.isFromMenu||self.isFromRightMenu) {
                [self.myDelegate updateMenuProfile];
                [self dismissViewControllerAnimated:YES completion:^{
                    [WSCore statusBarColor:StatusBarColorWhite];
                }];
            }else{
               [self dismissViewControllerAnimated:YES completion:nil]; 
            }
            
        }
    }
    else if(alertView.tag==333){
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
   
}



#pragma mark - GCNetworkManager Delegate Methods
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[NSUserDefaults standardUserDefaults] setObject:self.firstNameTextfield.text forKey:kFirstNameKey];
        [[NSUserDefaults standardUserDefaults] setObject:self.surNameTextfield.text forKey:kSurnameKey];
        [[NSUserDefaults standardUserDefaults] setObject:self.gender forKey:kGenderKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [WSCore dismissNetworkLoadingOnView:self.view];
        
        [self showSavedMessage];
    });
    
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView showSimpleAlertWithTitle:@"Server Error" message:@"The submission of the form was unsuccessful." cancelButtonTitle:@"OK"];
        
    });
    
}

@end
