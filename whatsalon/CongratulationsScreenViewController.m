//
//  CongratulationsScreenViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 04/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "CongratulationsScreenViewController.h"
#import "WSCore.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "GetDirectionsViewController.h"
#import <UIImage+ImageEffects.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "UILabel+Boldify.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SocialShareViewController.h"
#import "User.h"

@interface CongratulationsScreenViewController ()<UIViewControllerTransitioningDelegate>
@property (nonatomic,strong) EKEventStore * eventStore;
@property BOOL eventStoreAccessGranted;
@property (weak, nonatomic) IBOutlet UIView *reminderView;
- (IBAction)cancelReminder:(id)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *reminderDatePicker;
- (IBAction)setReminder:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelReminder;
@property (weak, nonatomic) IBOutlet UIButton *setReminderButton;
@property (weak, nonatomic) IBOutlet UIButton *directionButton;
@property (weak, nonatomic) IBOutlet UIButton *addReminderButton;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeButton;
@property (nonatomic) NSString * number;
- (IBAction)share:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *directionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *addReminderLabel;
@property (weak, nonatomic) IBOutlet UILabel *callLabel;

@end

@implementation CongratulationsScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    self.view.backgroundColor=[UIColor blackColor];
    if ([WSCore isDummyMode]) {
      //  self.bgImage.image = [[WSCore getDummySalonGallery] objectAtIndex:0];
    }
    else{
        if ([self.salonData fetchSlideShowGallery].count>0) {
            [self.bgImage
             sd_setImageWithURL:[[self.salonData fetchSlideShowGallery
                                         ] objectAtIndex:0]];
            
        }
    }
    
    /*
    if ((IS_IOS_8_OR_LATER && [self.salonData fetchSlideShowGallery].count !=0) || (IS_IOS_8_OR_LATER && [WSCore isDummyMode])) {
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = self.bgImage.bounds;
        [self.bgImage insertSubview:visualEffectView atIndex:0];
    }
    else{
        
        [WSCore addDarkBlurToView:self.viewContainer];
    }
     */

    self.viewContainer.alpha=0.7;
    self.viewContainer.backgroundColor=[UIColor blackColor];
    /*
    UIView * blackView = [[UIView alloc] initWithFrame:self.viewContainer.frame];
    blackView.backgroundColor=[UIColor blackColor
                               ];
    blackView.alpha=0.7;
    [self.bgImage addSubview:blackView];
   */
    
    [self navigationBarSetUp];
    
    self.remainingPrice = [self.bookingObj.chosenPrice doubleValue] /100*90;
    self.chargedPrice = [self.bookingObj.chosenPrice doubleValue] /100*10;
    
    NSString * service = [WSCore vowelCheckString:self.bookingObj.chosenService];
    /*
    service = [service stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([service.lowercaseString hasPrefix:@"a"] ||[service.lowercaseString hasPrefix:@"e"] || [service.lowercaseString hasPrefix:@"i"] ||[service.lowercaseString hasPrefix:@"o"]){
        service = [NSString stringWithFormat:@"an %@",service];
    }
    else{
        service = [NSString stringWithFormat:@"a %@",service];
    }
    */
    self.appointmentLabel.text = [NSString stringWithFormat: @"Appointment booked & confirmed for %@ with %@ at %@, %@",service,self.bookingObj.chosenStylist,self.salonData.salon_name,self.bookingObj.chosenDay];
    NSLog(@"Chosen Stylist %@",self.bookingObj.chosenStylist);
    
    /*
     
     
    
    [WSCore setLocalNotificationWithAlertBody:[NSString stringWithFormat:@"%@ Appointment with %@ at %@, %@",self.bookingObj.chosentService,self.bookingObj.chosenStylist,self.salonData.salon_name,self.bookingObj.chosenTime] AndWithBookingDate:self.chosenDate];
    
     
     */
    
    
   //NSString *testDate = @"2015-05-14 14:58:50";
    NSLog(@" End time congratulations screen %@",self.bookingObj.endTime);
    
    
    //[WSCore setReviewReminderWithSalon:self.salonData WithBookingObj:self.bookingObj AndWithChosenDate:[WSCore getDateFromStringWithFormat:@"yyyy-MM-dd HH:mm:ss" WithDateString:self.bookingObj.endTime]];
    
   // [WSCore setReviewReminderWithSalon:self.salonData WithBookingObj:self.bookingObj AndWithChosenDate:[WSCore getDateFromStringWithFormat:@"yyyy-MM-dd HH:mm:ss" WithDateString:testDate]];
    
    

    if (![WSCore stringIsEmpty:service]) {
       [self.appointmentLabel boldSubstring:service];
    }
    
    
    if (![WSCore stringIsEmpty:self.bookingObj.chosenStylist]) {
        [self.appointmentLabel boldSubstring:self.bookingObj.chosenStylist];
    }
    
    
    if (![WSCore stringIsEmpty:self.salonData.salon_name]) {
       [self.appointmentLabel boldSubstring:self.salonData.salon_name];
    }
    

    if (![WSCore stringIsEmpty:self.bookingObj.chosenDay]) {
      [self.appointmentLabel boldSubstring:self.bookingObj.chosenDay];
    }
    
    self.appointmentLabel.adjustsFontSizeToFitWidth=YES;
   
    NSString * chargePrStr = [NSString stringWithFormat:@"%@%.2lf",CURRENCY_SYMBOL,self.chargedPrice];
    NSString * remainingPrStr = [NSString stringWithFormat:@"%@%.2lf",CURRENCY_SYMBOL,self.remainingPrice];
    
    
    self.subAppointmentLabel.text = [NSString stringWithFormat:@"%@ has been charged on your card and the remaining %@ can be paid on arrival at this Salon.",chargePrStr,remainingPrStr];
    
    [self.subAppointmentLabel boldSubstring:chargePrStr];
    [self.subAppointmentLabel boldSubstring:remainingPrStr];
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: self.subAppointmentLabel.attributedText];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:kWhatSalonBlue
                 range:NSMakeRange(0, chargePrStr.length)];
    [self.subAppointmentLabel setAttributedText: text];
    
    self.subAppointmentLabel.adjustsFontSizeToFitWidth=YES;
    
    self.reminderView.hidden=YES;
    self.reminderView.layer.cornerRadius=3.0;
    self.reminderView.layer.masksToBounds=YES;
    self.reminderView.backgroundColor = [UIColor lightGrayColor];
    
    [WSCore addRightLineToView:self.cancelReminder withWidth:0.5];
    
   
    UIImage *image = [[UIImage imageNamed:@"receipt"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.callButton setImage:image forState:UIControlStateNormal];
    self.callButton.tintColor = kWhatSalonBlue;
    self.callButton.layer.borderColor=kWhatSalonBlue.CGColor;
    self.callButton.layer.borderWidth=1.0;
    self.callButton.layer.cornerRadius=self.callButton.frame.size.width/2.0f;
    self.callButton.layer.masksToBounds=YES;
    
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    self.navigationItem.rightBarButtonItem = cancelButton;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor whiteColor],NSForegroundColorAttributeName,
                                nil];
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:attributes forState:UIControlStateNormal];

    
}

-(void)navigationBarSetUp{
    //make navigation bar completely transparent
    [self.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
    self.navigationBar.backgroundColor = [UIColor clearColor];
    [WSCore addBottomLine:self.navigationBar :[UIColor whiteColor]];
    
    //change font style of navigation bar title
    [self.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont systemFontOfSize: 20.0f],
      NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil]];

    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
}

- (IBAction)directions:(id)sender {

    GetDirectionsViewController *destinationViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
    destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
    destinationViewController.transitioningDelegate = self;
    destinationViewController.salonMapData = self.salonData;
    
    [self presentViewController:destinationViewController animated:YES completion:NULL];
}

- (IBAction)setAReminder:(id)sender {
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(10, 161, self.view.frame.size.width-10, 223)];
    view.backgroundColor = [UIColor lightGrayColor];
    view.layer.cornerRadius=kCornerRadius;
    view.clipsToBounds=YES;
    
    
    UIDatePicker * dp = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 162)];
    dp.datePickerMode=UIDatePickerModeDateAndTime;
    dp.minimumDate=[NSDate date];
    //NSLog(@"%@",self.chosenDate);
    //dp.date=self.chosenDate;
    NSDate * startDateForReminder = [WSCore oneHourBeforeUsersApointmentDate:self.chosenDate];
    dp.date=startDateForReminder;
    NSLog(@"Start date for reminder %@",startDateForReminder);
    // dp.date =
    self.reminderDatePicker = dp;
    [view addSubview:dp];
    
    
    UIButton * reminderButton = [UIButton buttonWithType:UIButtonTypeSystem];
    reminderButton.backgroundColor = kWhatSalonBlue;
    [reminderButton addTarget:self action:@selector(setReminder:) forControlEvents:UIControlEventTouchUpInside];
    [reminderButton setTitle:@"Set Reminder" forState:UIControlStateNormal];
    [reminderButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    reminderButton.frame = CGRectMake(view.frame.size.width/2, view.frame.size.height-44, view.frame.size.width/2, 44);
    self.setReminderButton=reminderButton;
    [view addSubview:self.setReminderButton];
    
    UIButton * cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    cancelButton.backgroundColor = kWhatSalonBlue;
    [cancelButton addTarget:self action:@selector(cancelReminder:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(0, view.frame.size.height-44, view.frame.size.width/2, 44);
    [WSCore addRightLineToView:cancelButton withWidth:0.5f];
    self.cancelReminder = cancelButton;
    [view addSubview:self.cancelReminder];
    
    self.reminderView = view;
    
    [self.view addSubview:self.reminderView];
    self.reminderView.center=self.view.center;

    
    [self disableControls];
}

-(void)createReminder
{
    EKReminder *reminder = [EKReminder
                            reminderWithEventStore:self.eventStore];
    
    reminder.title = [NSString stringWithFormat:@"Appointment booked for a %@ with %@ at %@, %@",self.bookingObj.chosenService,self.bookingObj.chosenStylist,self.salonData.salon_name,self.bookingObj.chosenTime];// @"Appointment booked for an Eye Makeup with Scott Barnes at Blushington Salon, Today at 02.30pm";
    
    reminder.calendar = [_eventStore defaultCalendarForNewReminders];
    
    NSDate *date = [self.reminderDatePicker date];
    NSLog(@"Reminder date %@",date);
    EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:date];
    
    [reminder addAlarm:alarm];
    
    NSError *error = nil;
    
    [_eventStore saveReminder:reminder commit:YES error:&error];
    
    if (error){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@",[error localizedDescription]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            alert.tag=1;
            NSLog(@"error = %@", error);
        });
        
    }
    else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Reminder" message:@"Reminder set" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
            NSLog(@"%@",[self.reminderDatePicker date]);
            [self enableControls];
        });
        
    }
    
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        if (buttonIndex ==[alertView cancelButtonIndex]) {
            [self enableControls];
        }
    }
    else if(alertView.tag==2){
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.number]]];
        }
      
    }
}

- (IBAction)saveReceipt:(id)sender {
    
    /*
    self.number=@"";
    self.number  = self.salonData.salon_phone;
    if (self.number.length==0) {
        self.number=self.salonData.salon_landline;
    }
    
    if (self.number.length==0) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"No number" message:@"The salon has not provided a contact number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Call" message:[NSString stringWithFormat:@"Are you sure you want to call %@?",self.number] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=2;
    [alert show];
     */
    [self takeScreenShotForReceipt];
   
}
- (IBAction)cancel:(id)sender {
    
   
    NSLog(@"** \nunwind to search filter %@ \n is rebooking %@ \n is from last min and pop to salon individual %@ \n is from fav %@ \n is from last mine %@ \n\n **",StringFromBOOL(self.unwindBackToSearchFilter),StringFromBOOL(self.salonData.isReBooking),StringFromBOOL(self.isFromLastMinPopToSalonIndividual),StringFromBOOL(self.isFromFavourite),StringFromBOOL(self.isFromLastMinute));
    
    if (self.unwindBackToSearchFilter) {
        [self performSegueWithIdentifier:@"unwindToSearchFilterScreen" sender:self];

    }
    else if (self.salonData.isReBooking) {
        NSLog(@"unwindToMyBookings");
        [self performSegueWithIdentifier:@"unwindToMyBookings" sender:self];
        
    }
    else if(self.isFromLastMinPopToSalonIndividual){
        
        
         [self performSegueWithIdentifier:@"unwindToDiscoveryFromLastMinute" sender:self];
        //[self performSegueWithIdentifier:@"unwindToSalonIndividualFromLastMin" sender:self];
    }
    else if(self.isFromFavourite){
        NSLog(@"Unwind to favs");
        [self performSegueWithIdentifier:@"unwindToFavs" sender:self];
    }
    else if(self.isFromLastMinute){
        NSLog(@"Last minute");
        [self performSegueWithIdentifier:@"unwindToDiscoveryFromLastMinute" sender:self];
    }else{
        NSLog(@"Go back to Browse");
        [self performSegueWithIdentifier:@"unwindToBrowse" sender:self];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"unwindToBrowse"]) {
        
        self.salonData = nil;
        self.bgImage=nil;
        self.appointmentLabel=nil;
        self.subAppointmentLabel=nil;
        self.thanksLabel=nil;
        self.navigationBar=nil;
        self.viewContainer=nil;
    }
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}

- (IBAction)setReminder:(id)sender {
   
    if (self.eventStore == nil)
    {
        self.eventStore = [[EKEventStore alloc]init];
        
        [self.eventStore requestAccessToEntityType:EKEntityTypeReminder completion:^(BOOL granted, NSError *error) {
            
            if (!granted){
              
                [WSCore dismissNetworkLoadingOnView:self.view];
            }
            
            if (_eventStore!=nil) {
           
                    [self createReminder];

            }
        }];
    }
    
    

}


-(void)disableControls{
   // self.reminderView.hidden=NO;
    self.closeButton.enabled=NO;
    self.directionButton.userInteractionEnabled=NO;
    self.directionButton.alpha=0.8;
    self.callButton.userInteractionEnabled=NO;
    self.callButton.alpha=0.8;
    self.addReminderButton.userInteractionEnabled=NO;
    self.addReminderButton.alpha=0.8;

    
}

-(void)enableControls{
    self.reminderView.hidden=YES;
    self.closeButton.enabled=YES;
    self.directionButton.userInteractionEnabled=YES;
    self.directionButton.alpha=1.0;
    self.callButton.userInteractionEnabled=YES;
    self.callButton.alpha=1.0;
    self.addReminderButton.userInteractionEnabled=YES;
    self.addReminderButton.alpha=1.0;

}
- (IBAction)cancelReminder:(id)sender {
    [self enableControls];
}


- (void)takeScreenShotForReceipt {
    /*
     SocialShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"socialVC"];
     shareVC.modalPresentationStyle=UIModalPresentationCustom;
     shareVC.transitioningDelegate=self;
     shareVC.messageToShare= [NSString stringWithFormat: @"I just booked an appointment for an %@ in %@ using WhatSalon.",self.bookingObj.chosentService,self.salonData.salon_name];
     [self presentViewController:shareVC animated:YES completion:nil];
     */
    self.navigationBar.hidden=YES;
    self.directionButton.hidden=YES;
    self.addReminderButton.hidden=YES;
    self.callButton.hidden=YES;
    self.callLabel.hidden=YES;
    self.directionsLabel.hidden=YES;
    self.addReminderLabel.hidden=YES;
    
    UILabel * nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, 64)];
    nameLabel.text = [NSString stringWithFormat:@"Booking Reference for \n %@ %@",[[User getInstance] fetchFirstName],[[User getInstance] fetchLastName] ];
    nameLabel.textColor = [UIColor cloudsColor];
    nameLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    nameLabel.textAlignment=NSTextAlignmentCenter;
    nameLabel.numberOfLines=0;
    [WSCore addBottomLine:nameLabel :[UIColor cloudsColor]];
    [self.view addSubview:nameLabel];
    [WSCore saveViewAsImageWithView:self.view];
    self.navigationBar.hidden=NO;
    
    self.directionButton.hidden=NO;
    self.addReminderButton.hidden=NO;
    self.callButton.hidden=NO;
    self.callLabel.hidden=NO;
    self.directionsLabel.hidden=NO;
    self.addReminderLabel.hidden=NO;
    [nameLabel removeFromSuperview];
    nameLabel=nil;
}

- (IBAction)share:(id)sender {
    
    [self takeScreenShotForReceipt];
}
@end
