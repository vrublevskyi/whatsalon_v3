//
//  FilterSearchViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 03/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header FilterSearchViewController.h
  
 @brief This is the header file for the FilterSearchViewController.
  
 This file contains the methods and properties for the file that handles the filtering of the 'TabBrowseViewController.h'
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.3
 */
#import <UIKit/UIKit.h>
#import "FUISwitch.h"

/*!
     @protocol FilterDelegate
  
     @brief The FitlerDelegate protocol
  
     The protocol is used here to handle the options selected on the UIViewController.
 */
@protocol FilterDelegate <NSObject>

@optional
/*!
 @brief returns the results of the filtering screen.
 
 @param instant - BOOL determines whether instant book option is selected.
 
 @param distance - int determines the user selected distance.
 
 @param cat - the category id chosen
 */

-(void)filterSearchWithInstantBook : (BOOL) instant WithDistance: (int) distance AndWithCategory: (int) cat;

@end

@interface FilterSearchViewController : UIViewController

/*! @brief represents the hair button. */
@property (weak, nonatomic) IBOutlet UIButton *hairButton;

/*! @brief represents the nails button. */
@property (weak, nonatomic) IBOutlet UIButton *nailsButton;

/*! @brief represents the massage button. */
@property (weak, nonatomic) IBOutlet UIButton *massageButton;

/*! @brief represents the wax/hair removal button. */
@property (weak, nonatomic) IBOutlet UIButton *waxButton;

/*! @brief represents the face button. */
@property (weak, nonatomic) IBOutlet UIButton *faceButton;

/*! @brief represents the body button. */
@property (weak, nonatomic) IBOutlet UIButton *bodyButton;


/*! @brief the category id represented as a int */
@property (nonatomic) int browseCategoryForFiltering;

/*!
 @brief kmLabel - UILabel for hosting kilometres
 */
@property (weak, nonatomic) IBOutlet UILabel *kmLabel;

/*!
 @brief represents the FilterDelegate object.
 */
@property (nonatomic,assign) id<FilterDelegate>  myDelegate;


//labels and holders views
/*!
 @brief represents the instantBookingLabel.
 */
@property (weak, nonatomic) IBOutlet UILabel *instantBookingLabel;

/*!
 @brief represents the intantBookingViewHolder. 
 */
@property (weak, nonatomic) IBOutlet UIView *instantBookingViewHolder;

/*!
 @brief represents the services label.
 */
@property (weak, nonatomic) IBOutlet UILabel *servicesLabel;

/*!
 @brief represents the distance label.
 */
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

//large icons
/*! @brief represents the hair image view */
@property (weak, nonatomic) IBOutlet UIImageView *hairImage;

/*! @brief represents the hair removal image view. */
@property (weak, nonatomic) IBOutlet UIImageView *hairRemovalImage;

/*! @brief represents the nails image view. */
@property (weak, nonatomic) IBOutlet UIImageView *nailsImage;

/*! @brief represents the massage image view. */
@property (weak, nonatomic) IBOutlet UIImageView *massageImage;

/*! @brief represents the face image view. */
@property (weak, nonatomic) IBOutlet UIImageView *faceImage;

/*! @brief represents the body image view. */
@property (weak, nonatomic) IBOutlet UIImageView *bodyImage;


/*!
 @brief showingInstant - a boolean value to determine whether to show Instant booking salons or not.
 */

@property (nonatomic) BOOL showingInstant;

/*!
@brief category change - IBAction - handles category change.
 
@param sender - (id)
 */
- (IBAction)categoryChange:(id)sender;
@end
