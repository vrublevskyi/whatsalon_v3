//
//  GetDirectionsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 28/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//


/*!
 @header GetDirectionsViewController.h
  
 @brief This is the header file for the GetDirectionsViewController.h
  
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.3
 */
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Salon.h"
#import "SalonTier4.h"


@interface GetDirectionsViewController : UIViewController <MKMapViewDelegate>

/*! @brief represents the MKMapView. */
@property (weak, nonatomic) IBOutlet MKMapView *getDirectionsMap;

/*! @brief represents the salonMapData. */
@property (weak,nonatomic) id salonMapData;

/*! @brief represents the details label UIView. */
@property (strong, nonatomic) UIView *detailsLabelView;

/*! @brief determines if a splitTransition is taking place. */
@property (nonatomic) BOOL isSplitTransition;



@end
