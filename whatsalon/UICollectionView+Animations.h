//
//  UICollectionView+Animations.h
//  whatsalon
//
//  Created by Graham Connolly on 28/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (Animations)

-(void)fadeInReload;

-(void)fadeOut;

-(void)fadeOutFadeInReload;
@end
