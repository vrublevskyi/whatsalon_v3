//
//  TermsAndConditionsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 20/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "WhatSalonDotComViewController.h"

@interface WhatSalonDotComViewController ()

@end

@implementation WhatSalonDotComViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    
    NSURL * url = [NSURL URLWithString:kWhatSalonWebsite];
    NSLog(@"url String %@",self.urlString);
    NSURLRequest * requestURL = [NSURLRequest requestWithURL:url];
    self.webView.delegate = self;
    [self.webView loadRequest:requestURL];

    self.webView.backgroundColor = kBackgroundColor;
    
   self.title=self.pageTitle;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [WSCore flatNavBarOnView:self];
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    NSLog(@"start");
    [self.activityIndicator startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"finish");
    [self.activityIndicator stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    /*
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:[NSString stringWithFormat:@"%@ %@",@"A connection Error occurred.",[error localizedFailureReason]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [alert show];
     */
}

- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)rq
{
    NSLog(@"start");
    [self.activityIndicator startAnimating];
    return YES;
}

- (void)webViewDidFinishLoading:(UIWebView *)wv
{
    NSLog(@"finish");
    [self.activityIndicator stopAnimating];
}


@end
