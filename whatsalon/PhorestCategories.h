//
//  PhorestCategories.h
//  whatsalon
//
//  Created by Graham Connolly on 20/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhorestCategories : NSObject

@property (nonatomic) NSMutableArray * completeArray;


+(instancetype)getInstance;

-(NSMutableArray *) fetchCategories;

-(void)downloadCategories;
@end
