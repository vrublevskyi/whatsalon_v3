//
//  InspireMeViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "InspireMeViewController.h"
#import "InspireDetailViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "InspireCollectionViewCell.h"
#import "BrowseViewController.h"
#import "SettingsViewController.h"
#import "InspireImage.h"
#import "UISegmentedControl+setSegments.h"
#import "User.h"
#import "StyleCategoryListModel.h"
#import "StyleObject.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "NoInspireMessageView.h"
#import "GCStackedGridLayout.h"////
#import "UICollectionView+Animations.h"
#import "GCNetworkManager.h"

#define kColCellTag 1121212
#define LOADING_CELL_IDENTIFIER @"LoadingItemCell"

@interface InspireMeViewController ()<UISearchBarDelegate,UISearchDisplayDelegate,UIViewControllerTransitioningDelegate,GCNetworkManagerDelegate>
@property (nonatomic,strong) NSMutableArray * cellSizes;



@property (nonatomic) NSInteger selectedPhotoIndex;
@property (nonatomic,strong) UISearchBar * searchbar;

@property (nonatomic) NSString * gender;
@property (nonatomic,assign) BOOL isMale;



@property (nonatomic) NSMutableArray * inspireObjects;
@property (nonatomic) int style_id;
@property (nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) int currentPage;
@property (nonatomic) int totalNumberOfPages;
@property (nonatomic) NSMutableArray * styleList;
@property (nonatomic) NSMutableArray * styleIdsArray;
@property (nonatomic) NSMutableArray * styleNamesArray;
@property (nonatomic) BOOL isSearching;

@property (nonatomic) GCStackedGridLayout * stackedLayout;/////
@property (nonatomic) GCNetworkManager * networkManager;
@end

@implementation InspireMeViewController



#pragma mark - StackedGridLayout Delegate
- (NSInteger)collectionView:(UICollectionView*)cv layout:(UICollectionViewLayout*)cvl
   numberOfColumnsInSection:(NSInteger)section {
    return 2;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)cv layout:(UICollectionViewLayout*)cvl
   itemInsetsForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemWithWidth:(CGFloat)width atIndexPath:(NSIndexPath *)indexPath{
    
    
    
    NSArray *mElements = [NSArray arrayWithObjects:
                          [NSValue valueWithCGSize:CGSizeMake(500.0, 880.0)],
                          [NSValue valueWithCGSize:
                           CGSizeMake(600.0, 900.0)],

                          [NSValue valueWithCGSize:CGSizeMake(546.0, 1032.0)],
                          [NSValue valueWithCGSize:CGSizeMake(300.0, 700.0)],
                          [NSValue valueWithCGSize:CGSizeMake(300.0, 800.0)],
                          
                          nil];
    CGSize size = size = [[mElements objectAtIndex:indexPath.row % [mElements count]] CGSizeValue];
    
    CGSize picSize =  size.width>0.0f ? size : CGSizeMake(100.0f, 100.0f);
    
    picSize.height += 35.0f; picSize.width += 35.0f;
    CGSize retval = CGSizeMake(width, picSize.height * width / picSize.width);
    
    return retval;
}


#pragma mark - Inspire Message
-(void)addNoInspireView{
    
    UIView * noDataView = [[UIView alloc] initWithFrame:self.collectionView.bounds];
    noDataView.backgroundColor=kGroupedTableGray;
    
    NoInspireMessageView * noInspireMessage = [[NoInspireMessageView alloc] init];
    noInspireMessage.view.backgroundColor=[UIColor clearColor];
    [noDataView addSubview:noInspireMessage];
    
    noInspireMessage.center=CGPointMake(noDataView.frame.size.width/2.0, noDataView.frame.size.height/2.0);
    
    
    self.collectionView.backgroundView=noDataView;
   
}

-(void)removeNoInspireView{
    
    self.collectionView.backgroundView = nil;
}
#pragma mark LoginOrSignUp button
- (UIButton *)getInspiredLoginOrSignUp:(UIView *)blockView
{
    
    UIButton * loginSignUpButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    loginSignUpButton.frame =CGRectMake(0, 0, 200, 44);
    [loginSignUpButton setTitle:@"Get Inspired!" forState:UIControlStateNormal];
    [loginSignUpButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    loginSignUpButton.titleLabel.font = [UIFont systemFontOfSize: 16.0f];
    [blockView addSubview:loginSignUpButton];
    
    [loginSignUpButton setCenter:CGPointMake(blockView.frame.size.width/2, (blockView.frame.size.height/2)- loginSignUpButton.frame.size.height/2)];
    
    [loginSignUpButton addTarget:self action:@selector(loginSignUpAction) forControlEvents:UIControlEventTouchUpInside];
    loginSignUpButton.layer.cornerRadius=6.0;
    loginSignUpButton.layer.borderColor=[UIColor darkGrayColor].CGColor;
    
    UILabel * textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 220, 45)];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey ]==nil) {
        textLabel.text = @"Help us to Inspire you!\n Login or Sign Up to edit your Profile.";
    }
    else if (self.gender==nil || self.gender.length==0 || [self.gender isEqualToString:@"N/A"]){
        textLabel.text = @"Help us to Inspire you!\n But first, you must edit your Profile.";
    }
    
    textLabel.font = [UIFont systemFontOfSize:15.0f];
    textLabel.numberOfLines=3;
    textLabel.adjustsFontSizeToFitWidth=YES;
    textLabel.textColor=[UIColor darkGrayColor];
    textLabel.textAlignment=NSTextAlignmentCenter;
    
    [blockView addSubview:textLabel];
    [textLabel setCenter:CGPointMake(blockView.frame.size.width / 2, (blockView.frame.size.height / 2)+(loginSignUpButton.frame.size.height+20))];

    return loginSignUpButton;
}

-(void)loginSignUpAction{
    
    SettingsViewController * settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
    self.tabBarController.tabBar.hidden=YES;
    self.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:settingsViewController animated:YES];
    
}


#pragma mark - Views Lifecycle methods
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    
    NSString * accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey];
    self.gender = [[NSUserDefaults standardUserDefaults] objectForKey:kGenderKey];
  
    
    if (accessToken==nil|| self.gender == nil || self.gender.length==0 ||[self.gender isEqualToString:@"N/A"]) {
        self.searchbar.userInteractionEnabled=NO;
        self.searchbar.alpha=0.8;
        self.segmentedControl.userInteractionEnabled=NO;
        self.segmentedControl.alpha=0.7;
        
        UIView * blockView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + self.navBarView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height- self.navBarView.frame.size.height)];
        blockView.backgroundColor = kGroupedTableGray;
        [self.view addSubview:blockView];
        
        
        UIButton *loginSignUpButton;
        loginSignUpButton = [self getInspiredLoginOrSignUp:blockView];
        loginSignUpButton.layer.borderWidth=0.5;
        
        return;
        
    }
    else{
        [self resetPage];
        [self fetchInspireObjects];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate =self;
    self.networkManager.parentView=self.view;
    
    self.collectionView.alpha=0.0;
    self.collectionView.backgroundColor=kGroupedTableGray;
    NSString * string = [[NSUserDefaults standardUserDefaults] objectForKey:kGenderKey];
    
    if ([string isEqualToString:kMale]) {
        self.isMale=YES;
       self.style_id=[[StyleCategoryListModel getInstance] fetchMaleStartPage];
    }
    else{
        self.isMale=NO;
        self.style_id=[[StyleCategoryListModel getInstance] fetchFemalStartPage];
    }
    
    
    self.currentPage=1;
    self.totalNumberOfPages=0;
    
    if (self.isMale) {
        self.styleList = [[NSMutableArray alloc] initWithArray:[[StyleCategoryListModel getInstance] fetchMaleStylesList]];
        
    }
    else{
        self.styleList = [[NSMutableArray alloc] initWithArray:[[StyleCategoryListModel getInstance] fetchFemaleStylesList]];
        
    }
   
    self.styleIdsArray = [[NSMutableArray alloc] init];
    self.styleNamesArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<self.styleList.count; i++) {
        StyleObject * style = [self.styleList objectAtIndex:i];
        
        NSString *styleName = style.styleTypeName;
        styleName = [styleName stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[styleName substringToIndex:1] uppercaseString]];
        [self.styleNamesArray addObject:styleName];
        [self.styleIdsArray addObject:style.styleTypeId];
    }
    
    [self.segmentedControl setSegments:self.styleNamesArray];
    [self.segmentedControl setSelectedSegmentIndex:0];
    
    [self navigationBarSetUp];
	
    //show navigation bar - hidden in TabBarController
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
   
    self.searchbar = [[UISearchBar alloc] init];
    self.navigationItem.titleView = self.searchbar;
   [self.searchbar setSearchFieldBackgroundImage:[UIImage imageNamed:@"searchbarBckground"] forState:UIControlStateNormal];
    self.searchbar.delegate=self;
    self.searchbar.placeholder = @"Inspire me";
    
    [self addBlurToView];
    
    //UICollection View
    self.collectionView.delegate = self;
    self.collectionView.dataSource =self;
    
    UITapGestureRecognizer * tapToDismissKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tapToDismissKeyboard.numberOfTapsRequired=1;
    [self.overlayBlocView addGestureRecognizer:tapToDismissKeyboard];
    
    [self.collectionView registerClass:[InspireCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:LOADING_CELL_IDENTIFIER];

    self.inspireObjects = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateInspireRemoveFavourite:)
                                                 name:@"inspireNotFav"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateInspireRemoveFavouriteCell:)
                                                 name:@"inspireNotFavCell"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateInspireAsFavourite:)
                                                 name:@"inspireIsFav"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scrollToIndex:)
                                                 name:@"scrollToIndex"
                                               object:nil];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.searchbar setImage:[UIImage imageNamed:@"Multiply"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
    [self.searchbar setImage:[UIImage imageNamed:@"Multiply"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateHighlighted];

    self.isSearching=NO;
    
    
    self.stackedLayout = [[GCStackedGridLayout alloc] init];
    self.stackedLayout.headerHeight=0.0f;
    self.collectionView.collectionViewLayout=self.stackedLayout;
    
}

-(void)startRefresh{
    
    
    if (self.refreshControl) {
        
        [self.refreshControl endRefreshing];
        [self resetPage];
        [self fetchInspireObjects];
    }

}

-(void)resetPage{
    [self.inspireObjects removeAllObjects];
    [self.collectionView fadeOutFadeInReload];
    self.currentPage=1;
    self.totalNumberOfPages=0;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [self resetPage];
}
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"inspireNotFav" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"inspireNotFavCell" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"inspireIsFav" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"scrollToIndex" object:nil];
}

#pragma mark - NSNotification Methods

-(void)scrollToIndex:(NSNotification *) notification{
    NSString * key = @"index";
    NSDictionary * dictionary = [notification userInfo];
    NSInteger rowIndex = [[dictionary valueForKey:key] integerValue];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
}

-(void)updateInspireRemoveFavourite:(NSNotification *)notification {
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    
    for (int i =0; i<self.inspireObjects.count; i++) {
        InspireImage * insp = [self.inspireObjects objectAtIndex:i];
        if ([insp.style_id isEqualToString: stringValueToUse]) {
            
            insp.is_favourite=NO;
            [self.inspireObjects replaceObjectAtIndex:i withObject:insp];
            
            //[self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.collectionView.numberOfSections)]];
            //[self.collectionView reloadData];
            //[self.collectionView fadeInReload];
            [self.collectionView fadeOutFadeInReload];
            return;
        }
        
    }
    
}

-(void)updateInspireRemoveFavouriteCell:(NSNotification *)notification {
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    
    for (int i =0; i<self.inspireObjects.count; i++) {
        InspireImage * insp = [self.inspireObjects objectAtIndex:i];
        if ([insp.style_id isEqualToString: stringValueToUse]) {
            
            insp.is_favourite=NO;
            [self.inspireObjects replaceObjectAtIndex:i withObject:insp];
            
            return;
        }
        
    }
    
}


-(void)updateInspireAsFavourite:(NSNotification *)notification {
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    for (int i =0; i<self.inspireObjects.count; i++) {
        InspireImage * insp = [self.inspireObjects objectAtIndex:i];
        
        if ([insp.style_id isEqualToString: stringValueToUse]) {
            insp.is_favourite=YES;
            [self.inspireObjects replaceObjectAtIndex:i withObject:insp];
            
            //[self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.collectionView.numberOfSections)]];
            //[self.collectionView reloadData];
            //[self.collectionView fadeInReload];
            [self.collectionView fadeOutFadeInReload];
            return;
        }
        
        
    }
    
}

#pragma mark - GCNetworkManager
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    [WSCore showServerErrorAlert];
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    self.totalNumberOfPages = [jsonDictionary[@"message"][@"total_pages"] intValue];
    NSArray * inspireObjects = jsonDictionary[@"message"][@"results"];
    for (NSDictionary *iDict in inspireObjects) {
        
        
        InspireImage *inspire = [InspireImage inspireWithStyle_id:[iDict[@"style_id"] stringValue]];
        
        inspire.is_favourite =[iDict[@"is_favourite"] boolValue];
        inspire.salon_name = iDict[@"salon"][@"salon_name"];
        inspire.style_gender =iDict[@"style_gender"];
        inspire.image_name = iDict[@"style_image"];
        inspire.style_type_id=iDict[@"style_type_id"];
        inspire.style_type_name=iDict[@"style_type_name"];
        inspire.stylist_name = iDict[@"stylist_name"];
        inspire.isBookable= [iDict[@"is_bookable"] boolValue];
        
        //salon obj
        inspire.salon_id = iDict[@"salon"][@"salon_id"];
        inspire.salon_des=iDict[@"salon"][@"salon_description"];
        inspire.salon_phone=iDict[@"salon"][@"salon_phone"];
        inspire.salon_lat=iDict[@"salon"][@"salon_lat"];
        inspire.salon_lon=iDict[@"salon"][@"salon_lon"];
        inspire.salon_address_1 = iDict[@"salon"][@"salon_address_1"];
        inspire.salon_address_2=iDict[@"salon"][@"salon_address_2"];
        inspire.salon_address_3=iDict[@"salon"][@"salon_address_3"];
        inspire.salon_city_name=iDict[@"salon"][@"city_name"];
        inspire.salon_country_name=iDict[@"salon"][@"country_name"];
        inspire.salon_landline=iDict[@"salon"][@"salon_landline"];
        inspire.salon_image_url=iDict[@"salon"][@"salon_image"];
        inspire.salon_tier=iDict[@"salon"][@"salon_tier"];
        inspire.salon_rating=iDict[@"salon"][@"rating"];
        inspire.salon_review=iDict[@"salon"][@"reviews"];
        
        
        [self.inspireObjects addObject:inspire];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        
        if (self.inspireObjects.count==0) {
            [self.collectionView fadeOutFadeInReload];
            if (!self.isSearching) {
               [self addNoInspireView];
            }else{
                [self.collectionView reloadData];
            }
            
        }
        else{
            
            [self removeNoInspireView];
            if (!self.isSearching) {
                [self.collectionView fadeOutFadeInReload];
            }else{
                [self.collectionView reloadData];
            }
            
        }
        
    });
    
}
#pragma mark - fetch inspire object
-(void)fetchInspireObjects{
   
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kInspire_User_URL]];
    NSString * accessToken =[[User getInstance] fetchAccessToken];
    
    NSString * params =[NSString stringWithFormat:@"style_type_id=%d",self.style_id ];
    if (accessToken!=nil) {
        params =[params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",accessToken]];
    }
    
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&page=%d",self.currentPage]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&search_string=%@",self.searchbar.text]];
    
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:!self.isSearching];
    /*
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.f];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];

    
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        
        if (!error) {
            if (data && dataDict!=nil) {
                
                switch (httpResp.statusCode) {
                    case kOKStatusCode:{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                                
                                self.totalNumberOfPages = [dataDict[@"message"][@"total_pages"] intValue];
                                NSArray * inspireObjects = dataDict[@"message"][@"results"];
                                for (NSDictionary *iDict in inspireObjects) {
                                    
                                    
                                    InspireImage *inspire = [InspireImage inspireWithStyle_id:[iDict[@"style_id"] stringValue]];
                                    
                                    inspire.is_favourite =[iDict[@"is_favourite"] boolValue];
                                    inspire.salon_name = iDict[@"salon"][@"salon_name"];
                                    inspire.style_gender =iDict[@"style_gender"];
                                    inspire.image_name = iDict[@"style_image"];
                                    inspire.style_type_id=iDict[@"style_type_id"];
                                    inspire.style_type_name=iDict[@"style_type_name"];
                                    inspire.stylist_name = iDict[@"stylist_name"];
                                    inspire.isBookable= [iDict[@"is_bookable"] boolValue];
                                    
                                    //salon obj
                                    inspire.salon_id = iDict[@"salon"][@"salon_id"];
                                    inspire.salon_des=iDict[@"salon"][@"salon_description"];
                                    inspire.salon_phone=iDict[@"salon"][@"salon_phone"];
                                    inspire.salon_lat=iDict[@"salon"][@"salon_lat"];
                                    inspire.salon_lon=iDict[@"salon"][@"salon_lon"];
                                    inspire.salon_address_1 = iDict[@"salon"][@"salon_address_1"];
                                    inspire.salon_address_2=iDict[@"salon"][@"salon_address_2"];
                                    inspire.salon_address_3=iDict[@"salon"][@"salon_address_3"];
                                    inspire.salon_city_name=iDict[@"salon"][@"city_name"];
                                    inspire.salon_country_name=iDict[@"salon"][@"country_name"];
                                    inspire.salon_landline=iDict[@"salon"][@"salon_landline"];
                                    inspire.salon_image_url=iDict[@"salon"][@"salon_image"];
                                    inspire.salon_tier=iDict[@"salon"][@"salon_tier"];
                                    inspire.salon_rating=iDict[@"salon"][@"rating"];
                                    inspire.salon_review=iDict[@"salon"][@"reviews"];
                                    
                                   
                                    [self.inspireObjects addObject:inspire];
                                }
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    //[WSCore dismissNetworkLoading];
                                    [WSCore dismissNetworkLoadingOnView:self.view];
                                    
                                    if (self.inspireObjects.count==0) {
                                        [self addNoInspireView];
                                    }
                                    else{
                                        
                                        [self removeNoInspireView];
                                        [self.collectionView fadeOutFadeInReload];
                                    }
                                });

                                
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    
//                                    [WSCore dismissNetworkLoading];
//                                    
//                                    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.collectionView.numberOfSections)]];
//                                    [self.collectionView reloadData];
//                                    [self.collectionView fadeInReload];
//                                    [self.collectionView fadeOutFadeInReload];
//                                });
                                
                            }
                            else{
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    if (!self.isSearching)
                                        //[WSCore dismissNetworkLoading];
                                        [WSCore dismissNetworkLoadingOnView:self.view];
                                    
                                    if (error) {
                                        //[WSCore dismissNetworkLoading];
                                        [WSCore dismissNetworkLoadingOnView:self.view];
                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@", [error localizedDescription] ]message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                        [alert show];
                                        return ;
                                    }
                                    
                                    [WSCore showServerErrorAlert];
                                });
                                
                                
                            }
                        });
                    }
                        break;
                        
                    default:
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (!self.isSearching)
                                //[WSCore dismissNetworkLoading];
                                [WSCore dismissNetworkLoadingOnView:self.view];
                            
                            [WSCore showServerErrorAlert];
                        });
                        
                        break;
                }
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[WSCore dismissNetworkLoading];
                    [WSCore dismissNetworkLoadingOnView:self.view];
                    [WSCore showServerErrorAlert];
                });
                
            }
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[WSCore dismissNetworkLoading];
                [WSCore dismissNetworkLoadingOnView:self.view];
                [WSCore errorAlert:error];
            });
        }
        
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        if (!self.isSearching)
           // [WSCore showNetworkLoading];
            [WSCore showNetworkLoadingOnView:self.view];
        
        
        [dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
    
    */
}



-(void)dismissKeyboard{
    NSLog(@"Tap tap");
    [self.searchbar resignFirstResponder];
    self.overlayBlocView.hidden=YES;
    self.segmentedControl.enabled=YES;
}

#pragma mark - Set up Methods
-(void)navigationBarSetUp{
    
    
    //make navigation bar completely transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
}

-(void) addBlurToView{
    /*
    self.navBarView.backgroundColor = [UIColor clearColor];
    UIToolbar *bgToolbar = [[UIToolbar alloc] initWithFrame:self.navBarView.frame];
    bgToolbar.barStyle = UIBarStyleDefault;
    //bgToolbar.alpha=0.9f;
    [self.navBarView.superview insertSubview:bgToolbar belowSubview:self.navBarView];
    */
    UIView *blurredView = [[UIView alloc] initWithFrame:self.navBarView.frame];
    [WSCore blurredViewForNav:blurredView below:self.navBarView];
}




#pragma mark - UICollectionView datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    if (self.currentPage<self.totalNumberOfPages && self.inspireObjects.count!=0
        ) {
        return self.inspireObjects.count+1;
    }
    return self.inspireObjects.count;
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10; // This is the minimum inter item spacing, can be more
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (IS_iPHONE4) {
        
        return CGSizeMake(140, 220);
    }
    
    return CGSizeMake(140, 240);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell1=nil;
    
    if (indexPath.item <self.inspireObjects.count) {
        
        return [self itemCellForIndexPath:indexPath];
    }
    else{
        //loading
        self.currentPage++;
  
        [self fetchInspireObjects];
        return [self loadingCellForIndexPath:indexPath];
    }
    
    return cell1;
}


- (UICollectionViewCell *)itemCellForIndexPath:(NSIndexPath *)indexPath {
    
    InspireCollectionViewCell * cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.clipsToBounds = YES;
    
    InspireImage * inspire=nil;
    inspire =[self.inspireObjects objectAtIndex:indexPath.row];
    cell.infoLabel.text = [NSString stringWithFormat:@"Created by %@ at %@.",inspire.stylist_name,inspire.salon_name];
    [cell setCellImageWithURL:inspire.image_name];
    cell.styleID=inspire.style_id;
    cell.isFromFavScreen=NO;
    cell.isBookable=inspire.isBookable;
    
    /*
    if(inspire.isBookable){
        cell.bgImageView.frame = CGRectMake(0, 0, 140, cell.bounds.size.height - cell.book.bounds.size.height);
        cell.book.hidden = NO;
        cell.infoLabel.frame=CGRectMake(6, cell.bgImageView.bounds.size.height - 38, 129, 38);
    }
    else{
        cell.bgImageView.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
        cell.book.hidden = YES;
        cell.infoLabel.frame=CGRectMake(6, cell.bgImageView.bounds.size.height - 38, 129, 38);
        
    }
    
    [cell layoutIfNeeded];
    */
    if (inspire.is_favourite) {
        cell.favImageView.image = [UIImage imageNamed:@"White_Heart_Solid"];
        cell.isFav = YES;
    }
    else{
        
        //cell.favImageView.image = [UIImage imageNamed:@"White_Heart"];
        cell.favImageView.image = [UIImage imageNamed:@"White_Heart_With_Gray_Solid"];
        cell.isFav=NO;
    }
    
    cell.bgImageView.contentMode=UIViewContentModeScaleAspectFill;
    
    return cell;
}

- (UICollectionViewCell *)loadingCellForIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = (UICollectionViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:LOADING_CELL_IDENTIFIER forIndexPath:indexPath];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]
                                                  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = CGRectMake((cell.frame.size.width/2)-20, (cell.frame.size.height/2)-20, 40, 40);
    cell.backgroundColor=[UIColor lightGrayColor];
    cell.layer.cornerRadius=3.0;
    cell.layer.borderColor=[UIColor blackColor].CGColor;
    cell.layer.masksToBounds=YES;
    cell.layer.borderWidth=0.5;
    
    [cell.contentView addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    return cell;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.isSearching) {
        [self resetSearchBar];
    }
    [self.collectionView setNeedsDisplay];
}

#pragma mark - UICollectionView Delegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    [self resetSearchBar];
    
    InspireDetailViewController *viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"inspireDetail"];
    NSString *imageName;
    viewController.imagesToDisplay=self.inspireObjects;
    
    self.listIndex = indexPath.row;
    viewController.modalPresentationStyle = UIModalPresentationCustom;
    viewController.transitioningDelegate = self;
    viewController.imageName =imageName;
    viewController.listIndex=self.listIndex;
    viewController.isFromFavPage=NO;
    
    [self presentViewController:viewController animated:YES completion:NULL];
    
        
    
}

#pragma mark - IBActions

- (IBAction)changeInspireListings:(id)sender {
    
    if (self.segmentedControl.selectedSegmentIndex ==0) {
        
        self.style_id=[[self.styleIdsArray objectAtIndex:0] intValue];
        [self.inspireObjects removeAllObjects];
        //[self.collectionView reloadData];
        //[self.collectionView fadeOut];
        [self.collectionView fadeOutFadeInReload];
        [self fetchInspireObjects];
     
    }
     else if (self.segmentedControl.selectedSegmentIndex ==1) {
        
         self.style_id=[[self.styleIdsArray objectAtIndex:1] intValue];
         [self.inspireObjects removeAllObjects];
         //[self.collectionView reloadData];
         //[self.collectionView fadeOut];
         [self.collectionView fadeOutFadeInReload];
         [self fetchInspireObjects];
         
    }
    else if (self.segmentedControl.selectedSegmentIndex ==2) {
       
        self.style_id=[[self.styleIdsArray objectAtIndex:2] intValue];
        [self.inspireObjects removeAllObjects];
        //[self.collectionView reloadData];
        //[self.collectionView fadeOut];
        [self.collectionView fadeOutFadeInReload];
        [self fetchInspireObjects];
       
    }
    else if (self.segmentedControl.selectedSegmentIndex ==3) {
       
        self.style_id=[[self.styleIdsArray objectAtIndex:3] intValue];
        [self.inspireObjects removeAllObjects];
        //[self.collectionView reloadData];
       // [self.collectionView fadeOut];
        [self.collectionView fadeOutFadeInReload];
        [self fetchInspireObjects];
       
    }
    else{
        
        self.style_id=[[self.styleIdsArray objectAtIndex:0] intValue];
        [self.inspireObjects removeAllObjects];
        //[self.collectionView fadeOut];
        //[self.collectionView reloadData];
        [self.collectionView fadeOutFadeInReload];
        [self fetchInspireObjects];
    }
    
    [self.collectionView setContentOffset:CGPointMake(-self.collectionView.contentInset.left, -self.collectionView.contentInset.top) animated:YES];
}

- (IBAction)showMenu:(id)sender {
   
    [self.sideMenuViewController presentMenuViewController];
    //[self.sideMenuViewController presentLeftMenuViewController];
}



#pragma mark - UISearchBar datasource and delegate methods

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    self.searchbar.showsCancelButton=YES;
    self.segmentedControl.enabled=NO;
    //self.searchbar.showsBookmarkButton=YES;
    self.isSearching=YES;
   
}

-(void)resetSearchBar{
 
    [self.searchbar resignFirstResponder];
    self.searchbar.showsBookmarkButton=NO;
    self.segmentedControl.enabled=YES;
    [self.view endEditing:YES];
    self.isSearching=NO;
}
- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar{
    [self resetSearchBar];
    
    if (searchBar.text.length ==0) {
        [self resetPage];
        [self fetchInspireObjects];
    }
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.overlayBlocView.hidden=YES;
    
   
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    self.searchbar.showsCancelButton=NO;
    [self resetSearchBar];
    
    if (searchBar.text.length ==0) {
        [self resetPage];
        [self fetchInspireObjects];
    }
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.overlayBlocView.hidden=YES;
    self.segmentedControl.enabled=YES;
    self.isSearching=NO;
    
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([searchText length]==0) {
        [self.inspireObjects removeAllObjects];
        self.currentPage=1;
        self.totalNumberOfPages=0;
        //[self.collectionView reloadData];
        [self.collectionView fadeOutFadeInReload];
    }
    else{
        if (self.inspireObjects.count!=0) {
            [self.inspireObjects removeAllObjects];
        }
        
        [self fetchInspireObjects];
    }
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}





@end
