//
//  SalonAnnotation.h
//  whatsalon
//
//  Created by Graham Connolly on 15/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Salon.h"

/*!
 @header SalonAnnotation.h
  
 @brief This is the header file for the Salon Annotation.
  
 
  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd.
 @version    -
 */
@interface SalonAnnotation : NSObject<MKAnnotation>

/*! @brief represents the coordiante object. */
@property (nonatomic,assign) CLLocationCoordinate2D coordinate;

/*! @brief represents the title of the annotation. */
@property (nonatomic,copy) NSString * title;

/*! @brief represents the subtitle of the annotation. */
@property (nonatomic,copy) NSString * subtitle;

/*! @brief represents the Salon Object. */
@property (nonatomic,retain) Salon * salon;

/*! @brief creates an instancetype of SalonAnnotation.
    @return instancetype */
-(instancetype)init;

/*! @brief creates an instancetype of SalonAnnotation based on a coordinate.
    @return instancetype */
-(instancetype)initWithCoordinate:(CLLocationCoordinate2D)inCoord;

/*! @brief sets up the properties based on the Salon object. */
-(void) setUpAnnotationWithSalon: (Salon *)salon;
@end
