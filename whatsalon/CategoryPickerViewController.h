//
//  CategoryPickerViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 04/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceCategory.h"


@interface CategoryPickerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSMutableArray * fullServicesList;

@end
