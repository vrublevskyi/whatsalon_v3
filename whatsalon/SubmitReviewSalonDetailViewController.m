//
//  SubmitReviewSalonDetailViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 22/01/2016.
//  Copyright © 2016 What Applications Ltd. All rights reserved.
//

#import "SubmitReviewSalonDetailViewController.h"
#import <EDStarRating/EDStarRating.h>
#import "GCNetworkManager.h"
#import "UIView+AlertCompatibility.h"

@interface SubmitReviewSalonDetailViewController ()<EDStarRatingProtocol,UITextViewDelegate,GCNetworkManagerDelegate>
@property (weak, nonatomic) IBOutlet EDStarRating *salonRating;
@property (weak, nonatomic) IBOutlet UIView *submitReviewHolder;
@property (weak, nonatomic) IBOutlet UITextView *textReview;

@property (weak, nonatomic) IBOutlet UIButton *submitReviewButton;

@property (nonatomic) int starRating;

@property (nonatomic) CGRect holderStartFrame;

- (IBAction)submitReview:(id)sender;
@property (nonatomic) GCNetworkManager * manager;
@end

@implementation SubmitReviewSalonDetailViewController

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
     NSLog(@"Success json %@",jsonDictionary);
    [self.view endEditing:YES];
    self.salonData.ratings = [jsonDictionary[@"message"][@"rating"] doubleValue];
    self.salonData.reviews = [jsonDictionary[@"message"][@"review_num"] doubleValue];
    
    if ([jsonDictionary[@"message"][@"salon_latest_review"]count]!=0) {
        NSLog(@"salon details %@",jsonDictionary[@"message"][@"salon_latest_review"]);
        [self.salonData updateSalonReviewWithJsonDictionary:jsonDictionary[@"message"]];
        
        /*
        self.salonData.hasReview=YES;
        SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:jsonDictionary[@"message"][@"salon_latest_review"][@"user_name"] AndWithLastName:jsonDictionary[@"message"][@"salon_latest_review"][@"user_last_name"]];
        NSLog(@"first name %@",jsonDictionary[@"message"][@"salon_latest_review"][@"user_name"]);
        salonReview.message = jsonDictionary[@"message"][@"salon_latest_review"][@"message"];
        salonReview.review_image=jsonDictionary[@"message"][@"salon_latest_review"][@"review_image"];
        salonReview.salon_rating=jsonDictionary[@"message"][@"salon_latest_review"][@"salon_rating"];
        salonReview.stylist_rating=jsonDictionary[@"message"][@"salon_latest_review"][@"stylist_rating"];
        salonReview.date_reviewed=jsonDictionary[@"message"][@"salon_latest_review"][@"date_reviewed"];
        salonReview.user_avatar_url=jsonDictionary[@"message"][@"salon_latest_review"][@"user_avatar"];
        self.salonData.salonReview=salonReview;
        */
        
    }

    if ([self.delegate respondsToSelector:@selector(reviewsDelegate:didSubmitReviewWithSalonData:)]) {
        [self.delegate reviewsDelegate:self didSubmitReviewWithSalonData:self.salonData];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
   
}

-(void)manager:(GCNetworkManager *)manager handleServerFailureWithError:(NSError *)error{
    
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    [UIView showSimpleAlertWithTitle:@"Review Error" message:@"A review could not be submitted. Please try again later." cancelButtonTitle:@"OK"];
    
    
}
#pragma mark - EDStarRating methods
-(void)starsSelectionChanged:(EDStarRating *)control rating:(float)rating
{
    NSString *ratingString = [NSString stringWithFormat:@"%d", (int)rating];
    NSLog(@"Rating %@",ratingString);
    self.starRating = (int)rating;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.salonRating.starImage = [UIImage imageNamed:@"emptyStar"];
    self.salonRating.starHighlightedImage = [UIImage imageNamed:@"blueStar"];
    self.salonRating.maxRating = 5.0;
    self.salonRating.delegate = self;
    self.salonRating.horizontalMargin = 0;
    self.salonRating.editable=YES;
    self.salonRating.displayMode=EDStarRatingDisplayFull;
    self.salonRating.rating= 0;
    [self starsSelectionChanged:self.salonRating rating:0];
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.7];
    
    self.submitReviewHolder.layer.cornerRadius=kCornerRadius;
    self.textReview.text=@"";
    self.submitReviewHolder.layer.masksToBounds=YES;
    
    self.submitReviewButton.backgroundColor=kWhatSalonBlue;
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2.0f)-30, self.view.frame.size.height-80, 60, 60)];
    [cancelButton setImage:kCancelButtonImage forState:UIControlStateNormal];
    [self.view addSubview:cancelButton];
    [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    
    self.textReview.delegate=self;
    
    self.textReview.text = @"Describe your experience";
    self.textReview.textColor=kWhatSalonSubTextColor;
    self.textReview.autocorrectionType = UITextAutocorrectionTypeNo;
    self.holderStartFrame = self.submitReviewHolder.frame;
    
    self.manager = [[GCNetworkManager alloc] init];
    self.manager.delegate = self;
    self.manager.parentView = self.view;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}
-(void)cancel{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.text = @"";
    textView.textColor=[UIColor blackColor];
    [self animateTextView: self.submitReviewHolder up:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSString * textLength = textView.text;
    if (textLength.length == 0) {
        textView.text = @"Describe your experience";
        textView.textColor=kWhatSalonSubTextColor;
    }

    [self animateTextView:self.submitReviewHolder up:NO];
}

- (void) animateTextView:(BOOL) up
{
    const int movementDistance =100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement= movement = (up ? -movementDistance : movementDistance);
    NSLog(@"%d",movement);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.submitReviewHolder.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(void)animateTextView:(UIView*)view up:(BOOL)up
{
    const int movementDistance = -80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


#pragma mark - UITextView delegate methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        
        return NO;
    }
    
    return YES;
}



- (IBAction)submitReview:(id)sender {
    if (self.textReview.text.length<20) {
        [UIView showSimpleAlertWithTitle:@"Review too short" message:@"" cancelButtonTitle:@"OK"];
        
        return;
    }
    
    if (self.starRating ==0) {
        self.starRating = 1;
    }
    NSString * url = [NSString stringWithFormat:@"%@%@",kTestAPI_URL,@"salon_review_v2"];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonData.salon_id]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&description=%@",self.textReview.text]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_rating=%d",self.starRating]];
    
    
    NSLog(@"Params %@",params);
    [self.manager loadWithURL:[NSURL URLWithString:url] withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}
@end
