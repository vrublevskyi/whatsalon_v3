//
//  TabConfirmViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabConfirmViewController.h"
#import "UIImage+ImageEffects.h"
#import "TabCongratulationsViewController.h"
#import "SalonBookingModel.h"
#import "UIView+AlertCompatibility.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "UITextField+PaddingText.h"
#import "GCNetworkManager.h"
#import "whatsalon-Swift.h"

#define kApplyButtonApplyState 0
#define kApplyButtonRemoveState 1



@interface TabConfirmViewController ()<ServicesDelegate,UITextFieldDelegate,GCNetworkManagerDelegate>


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tutorialTraceBottom;//
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *traceBottom;


@property (nonatomic,assign) BOOL isLogoTraced;
@property (nonatomic,assign) float logoToTraceOriginY;
@property(nonatomic) NSArray * xArray;
@property (nonatomic) NSArray *yArray;

@property(nonatomic) NSInteger count;
@property (nonatomic,assign) BOOL hasStarted;
@property (nonatomic,assign) BOOL isFinished;

@property (nonatomic) SalonBookingModel * salonBookingModel;

//promo code
@property (weak, nonatomic) IBOutlet UIView *promoCodeView;
@property (weak, nonatomic) IBOutlet UILabel *promoCodeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starsImageView;
@property (weak, nonatomic) IBOutlet UILabel *feedbackCountLabel;



//promo textfield
@property (weak, nonatomic) IBOutlet UITextField *promoTextFied;
@property (weak, nonatomic) IBOutlet UIButton *promoApplyBtn;
- (IBAction)promoApplyRemove:(id)sender;

@property (nonatomic) NSDate *firstTimeStamp;
@property (nonatomic) NSDate *revisitTimeStamp;

@property (nonatomic) GCNetworkManager * promoCodeNetworkManager;


@end

@implementation TabConfirmViewController






- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.firstTimeStamp = [NSDate date];
    
    //disable screen lock
    [UIApplication sharedApplication].idleTimerDisabled=YES;
    
    [self navigationBarSetUp];
    
    if ([self.salonData fetchSlideShowGallery].count!=0) {
        [self.salonImageVIew sd_setImageWithURL:[[self.salonData fetchSlideShowGallery
                                                       ] objectAtIndex:0]];
    }
    
    self.confirmButton.layer.cornerRadius = 2;
    

    //self.fullPriceAmountLabel.text = [NSString stringWithFormat:@"%@%@",CURRENCY_SYMBOL,self.bookingObj.chosenPrice];
    self.fullPriceAmountLabel.text = [NSString stringWithFormat:@"%@%d",self.salonData.currencySymbolUtf8,30];
    
    self.fullPriceAmountLabel.adjustsFontSizeToFitWidth=YES;
    double discount = [self.bookingObj.chosenPrice doubleValue];
    discount = discount/100*90;
    
    //self.priceAtSalonLabel.text = [NSString stringWithFormat:@"%@%.2lf",CURRENCY_SYMBOL,discount];
    //self.discountAmountLabel.text = [NSString stringWithFormat:@"%@%.2lf",CURRENCY_SYMBOL,[self.bookingObj.chosenPrice doubleValue]/100*10];
    
    self.priceAtSalonLabel.text = [NSString stringWithFormat:@"%@%.2lf",self.salonData.currencySymbolUtf8,discount];
    self.discountAmountLabel.text = [NSString stringWithFormat:@"%@%.2lf",self.salonData.currencySymbolUtf8,[self.bookingObj.chosenPrice doubleValue]/100*10];
    

    self.dateSelectedLabel.text = self.bookingObj.chosenDay;
    
    [WSCore addBottomLine:self.viewForPrice :[UIColor whiteColor]];
    [WSCore addBottomLine:self.viewForSalonInfo :[UIColor whiteColor]];
    [WSCore addBottomLine:self.viewForSalonStylist :[UIColor whiteColor]];
    [WSCore addBottomLine:self.promoCodeView :[UIColor whiteColor]];
    
    self.viewForPrice.backgroundColor = [UIColor clearColor];
    self.viewForSalonStylist.backgroundColor = [UIColor clearColor];
    self.viewForSalonInfo.backgroundColor = [UIColor clearColor];
    self.promoCodeView.backgroundColor = [UIColor clearColor];
   
    //self.discountAmountLabel.textColor = [UIColor whiteColor];
    
    self.logoToTraceOriginY = 406;
    
    
    
    UILabel * traceLabel = [[UILabel alloc] initWithFrame:CGRectMake(51, 346, 218, 47)];
    traceLabel.textAlignment=NSTextAlignmentCenter;
    traceLabel.text = @"If everything is OK, trace the logo to reveal the Confirm & Book button";
    traceLabel.font=[UIFont systemFontOfSize:13.0f];
    traceLabel.textColor = [UIColor whiteColor];
    traceLabel.numberOfLines=2;
    traceLabel.adjustsFontSizeToFitWidth=YES;

    UIImageView * trace = [[UIImageView alloc] initWithFrame:CGRectMake(108, 406, 105, 141)];
    trace.contentMode=UIViewContentModeScaleToFill;
    trace.image = [UIImage imageNamed:@"wsbutton"];

    
    UIImageView * traceT = [[UIImageView alloc] initWithFrame:CGRectMake(108, 406, 105, 141)];
    traceT.contentMode=UIViewContentModeScaleToFill;
    traceT.image = [UIImage imageNamed:@"tutorial0001"];
    
    if (IS_iPHONE4) {
        self.traceImageView.hidden=YES;
        self.traceTutorialImageView.hidden=YES;

        self.instructionsToTraceLabel.hidden=YES;
        
    }
    else{

        self.traceTutorialImageView.alpha=0.8;
        self.traceImageView.userInteractionEnabled=YES;
        
        self.traceTutorialImageView.animationImages = [NSArray arrayWithObjects:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0001" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0002" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0003" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0004" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0005" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0006" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0007" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0008" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0009" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0010" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0011" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0022" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0023" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0024" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0025" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0026" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0027" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0028" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0029" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0030" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0031" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0032" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0033" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0034" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0035" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0036" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0037" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0038" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0039" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0040" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0041" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0042" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0043" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0044" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0045" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0046" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0047" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0048" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0049" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0050" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0051" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0052" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0053" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0054" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0055" ofType:@"png"]],nil];
        
        
        
        
        
        
        
        self.traceTutorialImageView.animationDuration=1.5f;
        self.traceTutorialImageView.animationRepeatCount=1;
        
        self.xArray = @[@253.0f,
                        @258.0f,@258.0f,@262.0f,@277.0f,@266.0f,@266.0f,@265.0f,
                        @264.0f,@260.0f,@255.0f,@250.0f,@242.0f,@224.0f,@210.0f,@188.0f,
                        @180.0f,@172.0f,@156.0f,@144.0f,@134.0f,@116.0f,@98.0f,@80.0f,@67.0f,
                        @65.0f,@62.0f,@62.0f,@63.0f,@63.0f,@67.0f,
                        @71.0f,@74.0f,@79.0f,@90.0f,@97.0f,@104.0f,@118.0f,@126.0f,
                        @139.0f,@149.0f,@158.0f,@171.0f,@177.0f,@188.0f,@191.0f,@199.0f,
                        @204.0f,@206.0f,@207.0f,@205.0f,@204.0f,@200.0f,@194.0f,
                        @184.0f,@174.0f,@161.0f,@147.0f,@136.0f,@128.0f,@118.0f,@103.0f];
        
        self.yArray = @[@139.0f,@127.0f,@119.0f,@107.0f,@102.0f,@94.0f,@83.0f,@74.0f,@67.0f,
                        @63.0f,@53.0f,@45.0f,@37.0f,@29.0f,@23.0f,@17.0f,@17.0f,@18.0f,@20.0f,@24.0f,@21.0f,
                        @31.0f,@45.0f,@58.0f,@71.0f,@83.0f,@97.0f,
                        @114.0f,@130.0f,@138.0f,@147.0f,@164.0f,
                        @172.0f,@180.0f,@198.0f,@208.0f,@214.0f,@230.0f,@239.0f,@248.0f,@255.0f,
                        @266.0f,@276.0f,@284.0f,@296.0f,@299.0f,@310.0f,@324.0f,@336.0f,
                        @344.0f,@354.0f,@364.0f,@375.0f,@385.0f,
                        @386.0f,@396.0f,@396.0f,@396.0f,@395.0f,@389.0f,@383.0f,@366.0f];
        
        self.count = 0;
        
        self.isFinished=NO;
        
        [self performSelector:@selector(animateTutorial) withObject:self afterDelay:1.5f];
        
        [self performSelector:@selector(animateTutorial) withObject:self afterDelay:6.5f];
        
        //MOCKED
//        self.salonNameLabel.text = @"Fringe";
//        self.salonNameLabel.adjustsFontSizeToFitWidth=YES;
//        self.addressNameLabel.text = @"Main St, Coork City";
//        self.addressNameLabel.adjustsFontSizeToFitWidth=YES;
//        self.serviceNameLabel.text = @"Hair";
//        self.serviceNameLabel.adjustsFontSizeToFitWidth=YES;
//        self.stylistNameLabel.text = @"Regina Stalone";
//        self.stylistNameLabel.adjustsFontSizeToFitWidth=YES;
//        self.bookingObj.chosenPrice = @"30";
        
        
        self.dateSelectedLabel.text = self.bookingObj.chosenTime;
        self.priceAtSalonLabel.text = [NSString stringWithFormat:@"%@%.2d",CURRENCY_SYMBOL,27];
        self.discountAmountLabel.text = [NSString stringWithFormat:@"%@%.2lf",CURRENCY_SYMBOL,[self.bookingObj.chosenPrice doubleValue]/100*10];
        
        //check ==== updateNumberOfStars ===== for mocked data
        [self updateNumberOfStars];

        
        
         self.salonNameLabel.text = self.salonData.salon_name;
         self.salonNameLabel.adjustsFontSizeToFitWidth=YES;
         self.addressNameLabel.text = [self.salonData fetchFullAddress];
         self.addressNameLabel.adjustsFontSizeToFitWidth=YES;
         self.serviceNameLabel.text = self.bookingObj.chosenService;
         self.serviceNameLabel.adjustsFontSizeToFitWidth=YES;
         self.stylistNameLabel.text = self.bookingObj.chosenStylist;
         self.stylistNameLabel.adjustsFontSizeToFitWidth=YES;
        self.addressNameLabel.text = self.salonData.salon_address_1;
        
    }
    
    
    self.salonBookingModel = [[SalonBookingModel alloc] init];
    self.salonBookingModel.view = self.view;
    self.salonBookingModel.myDelegate=self;
    
   
    [self.confirmButton setTitle:[NSString stringWithFormat:@"Confirm & Pay %@%.2lf Deposit",self.salonData.currencySymbolUtf8,[self.bookingObj.chosenPrice doubleValue]/100*10] forState:UIControlStateNormal];
    [Gradients addPinkToPurpleHorizontalLayerWithView:self.confirmButton];

    self.promoApplyBtn.layer.cornerRadius=kCornerRadius;

    self.promoTextFied.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    ;
    self.promoTextFied.returnKeyType=UIReturnKeySend;
    self.promoTextFied.keyboardAppearance=UIKeyboardAppearanceDark;
    self.promoTextFied.delegate=self;
    self.promoTextFied.autocorrectionType = UITextAutocorrectionTypeNo;
    
    
    self.promoCodeNetworkManager = [[GCNetworkManager alloc] init];
    self.promoCodeNetworkManager.delegate=self;
    self.promoCodeNetworkManager.parentView=self.view;
    
    self.promoApplyBtn.tag = kApplyButtonApplyState;
    
    [self updatePayNowPriceList:self.bookingObj.chosenPrice ToOriginalPrice:YES];
    
}
-(void)updatePayNowPriceList: (NSString *) payNow ToOriginalPrice: (BOOL) yn{
    
    if (yn == YES) {
        
        [self.confirmButton setTitle:[NSString stringWithFormat:@"Confirm & Pay %@%.2lf Deposit",self.salonData.currencySymbolUtf8,[payNow doubleValue]/100*10] forState:UIControlStateNormal];
        self.discountAmountLabel.text = [NSString stringWithFormat:@"%@%.2lf",self.salonData.currencySymbolUtf8,[payNow doubleValue]/100*10];
    }
    else{
        
        
        [self.confirmButton setTitle:[NSString stringWithFormat:@"Confirm & Pay %@%.2lf Deposit",self.salonData.currencySymbolUtf8,[payNow doubleValue]] forState:UIControlStateNormal];
        self.discountAmountLabel.text = [NSString stringWithFormat:@"%@%.2lf",self.salonData.currencySymbolUtf8,[payNow doubleValue]];
        
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField== self.promoTextFied) {
        [textField resignFirstResponder];
    }
    
    return NO;
}
-(void)animateTutorial{
    
    
    if (!self.hasStarted) {
        
    
        
        [self.traceTutorialImageView startAnimating];
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    [self.view endEditing:YES];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [super touchesMoved:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self.traceImageView];
    
    self.hasStarted=YES;
    CGFloat xFloat = currentPoint.x;
    CGFloat yFloat = currentPoint.y;
    
    
    if (self.isFinished==NO) {
        
        if (_count<=_xArray.count-1) {
            
            if ((xFloat <=[[_xArray objectAtIndex:_count] floatValue]/3.0f + 30.0f && xFloat >= [[_xArray objectAtIndex:_count] floatValue]/3.0f - 30.0f) && (yFloat <= [[_yArray objectAtIndex:_count] floatValue]/3.0f + 30.0f && yFloat >= [[_yArray objectAtIndex:_count] floatValue]/3.0f - 30.0f)) {
                
                _count ++;
                NSString *fileLocation = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"wsbutton_%ld",(long)_count] ofType:@"png"];
                UIImage* yourImage = [[UIImage alloc] initWithContentsOfFile:fileLocation] ;
                self.traceImageView.image = yourImage;
                while (_count>=56&& _count<=_xArray.count) {
                    NSString *fileLocation = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"wsbutton_%ld",(long)_count] ofType:@"png"];
                    UIImage* yourImage = [[UIImage alloc] initWithContentsOfFile:fileLocation] ;
                    self.traceImageView.image = yourImage;
                    _count++;
                }
            }
            
        }else{
            self.isFinished=YES;
            
            
            if (IS_iPHONE5_or_Above) {
                
                [self.traceImageView setNeedsUpdateConstraints];
                [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.traceTutorialImageView.hidden=YES;
                    
                    self.traceImageView.frame = CGRectMake(self.traceImageView.frame.origin.x, self.logoToTraceOriginY - 40, self.traceImageView.frame.size.width, self.traceImageView.frame.size.height);
                    self.traceBottom.constant=+50;
                    [self.traceTutorialImageView layoutIfNeeded];
                    [self.traceImageView layoutIfNeeded];
                    
                    self.instructionsToTraceLabel.alpha=0;
                    
                    self.isLogoTraced =YES;
  
                    
                } completion:^(BOOL finished) {
                   
                }];
                
                
                
            }
            
            
            
        }
    }
    
    
}


-(void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.traceImageView];
    UIView* v = [self.view hitTest:location withEvent:nil];
    
    if (v)
        [self animateTutorial];
    
}
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.traceImageView];
    UIView* v = [self.view hitTest:location withEvent:nil];
    
    if (v)
        [self animateTutorial];
    
}



#pragma mark - UIGestureRecognizer Delegate Methods



-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
}


-(void)navigationBarSetUp{
    //make navigation bar completely transparent
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonSubTextColor}];
    self.navigationController.navigationBar.tintColor = kWhatSalonSubTextColor;
    
    self.title = @"Confim Booking";
}


- (IBAction)cancel:(id)sender {
    
    NSString * alertMessage;
    
    if ([self.salonData fetchSalonType] == SalonTypePhorest) {
        alertMessage =@"This time is reserved for you for 5 minutes. Canceling will prevent you from booking this time until the 5 minutes has passed. Do you wish to cancel this booking?";
    }
    else if([self.salonData fetchSalonType] == SalonTypeIsalon || [self.salonData fetchSalonType] == SalonTypePremier){
        alertMessage = @"You might lose this time if you choose to cancel.";
    }
    
    
    UIAlertView * cancelAlert = [[UIAlertView alloc] initWithTitle:@"Cancel Enquiry?" message:alertMessage delegate:self cancelButtonTitle: @"No" otherButtonTitles:@"Yes, I want to cancel", nil];
    cancelAlert.tag=1;
    [cancelAlert show];
    
   
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    else if (alertView.tag == 1001){
        if (buttonIndex==[alertView cancelButtonIndex]) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}


- (IBAction)confirm:(id)sender {
    
    [self performSelector:@selector(goToCongratsScreenInDummyMode) withObject:nil afterDelay:1.5];
    return;
    
    
    self.revisitTimeStamp =  [NSDate date];
    NSTimeInterval distanceBetweenDates = [self.revisitTimeStamp timeIntervalSinceDate:self.firstTimeStamp];
    if (distanceBetweenDates>180.0 && [self.salonData fetchSalonType] == SalonTypeIsalon) {
        
        UIAlertView * bookingExpiredAlert = [[UIAlertView alloc] initWithTitle:@"Timed Out" message:@"Your booking has timed out due to inactivity. Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        bookingExpiredAlert.tag = 1001;
        [bookingExpiredAlert show];
        return;
    }
    
    self.confirmButton.userInteractionEnabled=NO;
    if ([WSCore isDummyMode]) {
        
        [WSCore showNetworkLoadingOnView:self.view];
        [self performSelector:@selector(goToCongratsScreenInDummyMode) withObject:nil afterDelay:1.5];
        
    }else{
       
        if ([self.salonData fetchSalonType] == SalonTypePhorest) {
            NSLog(@"Phorest");
            [self.salonBookingModel phorestPaymentWithApptRef:self.bookingObj.appointment_reference_number AndWithPromoCode:self.promoTextFied.text];
        }
        else if([self.salonData fetchSalonType] == SalonTypeIsalon){
            NSLog(@"ISalon");
            [self.salonBookingModel iSalonPaymentWithBookingObject:self.bookingObj AndWithPromoCode:self.promoTextFied.text];
        }
        else if([self.salonData fetchSalonType] == SalonTypePremier){
            
            [self.salonBookingModel premierBookServiceWithBookingObject:self.bookingObj withPromoCode:self.promoTextFied.text];
        }

        
    }
    
    
}




#pragma mark - iSalon Booking methods
/*!
 iSalonSuccessfullBookingWithMessage
 * @discussion if a successful booking is placed then this method is called. Delegate method ServicesDelegate. Performs the successfullBookingAnimation Methods
 * @param message An NSString which contains the successful string messgae
 */
-(void)iSalonSuccessfulBookingWithMessage:(NSString *)message{
    //NSLog(@"SUCCESS: \n %@",message);
    
    [self successfullBookingAnimation];
}
/*!
 iSalonFailureBookingWithMessage
 * @discussion if an attempt to make a booking is unsuccessful then this method is called. Delegate method ServicesDelegate. Performs the unsuccessfullBookingAnimation method.
 * @param message An NSString which contains the successful string message
 */
-(void)iSalonFailureBookingWithMessage:(NSString *)message{
    //NSLog(@"FAILURE: \n %@",message);
    
    [self unsuccessfullBookingAnimation];
    
    [UIView showSimpleAlertWithTitle:message message:@"" cancelButtonTitle:@"OK"];
}


-(void)premierSuccessfulBookingWithMessage:(NSString *)message{
    //NSLog(@"Premier %@",message);
    [self successfullBookingAnimation];
}

-(void)premierFailureBookingWithMessage:(NSString *)message{
    //NSLog(@"Premier failure %@",message);
    [UIView showSimpleAlertWithTitle:message message:@"" cancelButtonTitle:@"OK"];
    [self unsuccessfullBookingAnimation];
}
/*!
 goToCongratsScreenInDummyMode
 !!!: LEGACY
 * @brief - early code used in early iteration to display dummy information
 */


-(void)goToCongratsScreenInDummyMode{
    
    [WSCore dismissNetworkLoadingOnView:self.view];
    TabCongratulationsViewController * s = [self.storyboard instantiateViewControllerWithIdentifier:@"congratsScreen"];

    s.salonData= self.salonData;
    s.bookingObj=self.bookingObj;
    s.chosenDate=self.chosenDate;
    //now present this navigation controller modally
    [self.navigationController pushViewController:s animated:YES];

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        bookingArray = [NSMutableArray new];
    });

    [bookingArray addObject:self.bookingObj];
}



/*!
 successfullBookingAnimation
 * @brief The screen animation that is performed following a succesful booking
 */
-(void)successfullBookingAnimation{
    [WSCore getUsersBalance];
    
    self.instructionsToTraceLabel.alpha=0.0;
    
    [UIView animateWithDuration:1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        //move back down
        
        self.confirmButton.alpha=0.0;
        self.traceImageView.frame = CGRectMake(self.traceImageView.frame.origin.x, self.logoToTraceOriginY-20, self.traceImageView.frame.size.width, self.traceImageView.frame.size.height);
        self.traceTutorialImageView.frame=self.traceImageView.frame;
        self.instructionsToTraceLabel.frame = CGRectMake(self.instructionsToTraceLabel.frame.origin.x, self.instructionsToTraceLabel.frame.origin.y, self.instructionsToTraceLabel.frame.size.width, 35);
        self.traceImageView.image = [UIImage imageNamed:@"wsbutton"];
        self.traceImageView.alpha=0.0;
        self.traceTutorialImageView.alpha=0.0;
        self.isLogoTraced =NO;
        
        
    } completion:^(BOOL finished) {
        NSLog(@"Go to confirmation screen");
        [self performSegueWithIdentifier:@"confirmation" sender:self];
    }];

}

/*!
 successfullBookingAnimation
 * @brief The screen animation that is performed following a succesful booking
 */
-(void)unsuccessfullBookingAnimation{
    self.confirmButton.userInteractionEnabled=YES;
    self.confirmButton.hidden=NO;
}

-(void)updateNumberOfStars{
    
//    self.starsImageView.image = [WSCore getStarRatingImage:(int)self.salonData.ratings];
    self.starsImageView.image = [WSCore getStarRatingImage:4];

    
    if ((int)self.salonData.reviews==1) {
        self.feedbackCountLabel.text =[NSString stringWithFormat:@"(%d)",(int)self.salonData.reviews];
        
        
    }else{
//        self.feedbackCountLabel.text =[NSString stringWithFormat:@"(%d)",(int)self.salonData.reviews];
        self.feedbackCountLabel.text =[NSString stringWithFormat:@"(%d)",88];

    }
    
    if ((int)self.salonData.reviews==0) {
        self.starsImageView.image = [self.starsImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.starsImageView setTintColor:[UIColor lightGrayColor]];
        NSLog(@"No stars");
    }else{
        self.starsImageView.image = [self.starsImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.starsImageView setTintColor:kYelowStar];
    }
    
}



#pragma mark - Services Delegate
/*!
 phorestBookingResult:WithMessage:
 * @discussion called when a booking is attempted with Phorest Client. Handles both the successful and unsuccessful bookings. Delegate method from ServicesDelegate
 * @param success BOOL to be used to determine if a booking was successful or not successful
 * @param message NSString containing a description of the booking result
 */
-(void)phorestBookingResult:(BOOL)success WithMessage:(NSString *)message{
    
    //NSLog(@"message %@",message);
    if (success) {
        
        [self successfullBookingAnimation];
        
    }
    else{
        
        [self unsuccessfullBookingAnimation];
        
        [UIView showSimpleAlertWithTitle:@"A booking could not be confirmed." message:[NSString stringWithFormat:@"%@",message] cancelButtonTitle:@"OK"];
    }
}


-(void)phorestSuccessfulBookingWithMessage: (NSString *) message{
    [self successfullBookingAnimation];
}
-(void)phorestFailureBookingWithMessage: (NSString *) message{
    [self unsuccessfullBookingAnimation];
    
    [UIView showSimpleAlertWithTitle:@"A booking could not be confirmed." message:[NSString stringWithFormat:@"%@",message] cancelButtonTitle:@"OK"];
}


- (IBAction)promoApplyRemove:(id)sender {
    
    [self.promoTextFied endEditing:YES];
    
    if (self.promoApplyBtn.tag==kApplyButtonApplyState) {
        if (self.promoTextFied.text>0) {
            
            //make request
            [self makePromoCodeRequestWithCouponName:self.promoTextFied.text];
        }
        else{
            [UIView showSimpleAlertWithTitle:@"Please add a promo code" message:@"" cancelButtonTitle:@"OK"];
        }
    }
    else{
        //self.promoApplyBtn.tag=kApplyButtonApplyState;
        [UIView showSimpleAlertWithTitle:@"Discount Removed" message:@"" cancelButtonTitle:@"OK"];
        [self resetScreenValue];
    }
    
}
-(void)makePromoCodeRequestWithCouponName: (NSString *) coupon{
    
    NSString * params = [NSString stringWithFormat:@"coupon_name=%@",coupon];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&service_id=%@",self.bookingObj.service_id]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken]]];
    
    
    if (self.salonData.salonType == SalonTypeIsalon) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&staff_id=%@",self.bookingObj.staff_id]];//for isalon promo code the backend requires the WhatSalon Staff ID for that stylist, not the iSalon Staff ID
    }
 
    [self.promoCodeNetworkManager loadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Promo_Discounted_Price_URL]] withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
    
}

-(void)updatePromoButtonAppearance{
    
    if (self.promoApplyBtn.tag==kApplyButtonApplyState) {
        [self.promoApplyBtn setTitle:@"Apply" forState:UIControlStateNormal];
        [self.promoApplyBtn setImage:nil forState:UIControlStateNormal];
        self.promoApplyBtn.tintColor = [UIColor whiteColor];
        self.promoTextFied.userInteractionEnabled=YES;
        self.promoTextFied.enabled=YES;
    }
    else{
        [self.promoApplyBtn setTitle:@"" forState:UIControlStateNormal];
        UIImage *image = [kCancelButtonImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.promoApplyBtn setImage:image forState:UIControlStateNormal];
        self.promoApplyBtn.tintColor = [UIColor whiteColor];
        self.promoTextFied.userInteractionEnabled=NO;
        self.promoTextFied.enabled=NO;
    }
}
#pragma mark - GCNetworkManager
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    self.promoApplyBtn.tag=kApplyButtonRemoveState;
    [self updatePromoButtonAppearance];
    
    //update details
    //original_price
    //discount_price
    //new_price
    [self updatePayNowPriceList:jsonDictionary[@"message"][@"price_due_now"] ToOriginalPrice:NO];
    
    if (jsonDictionary[@"message"][@"human_message"]) {
        
        [UIView showSimpleAlertWithTitle:jsonDictionary[@"message"][@"human_message"] message:@"" cancelButtonTitle:@"OK"];
    }
}
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    
    [UIView showSimpleAlertWithTitle:jsonDictionary[@"message"] message:@"" cancelButtonTitle:@"OK"];
    self.promoTextFied.text=@"";
}
-(void)resetScreenValue{
    //reset values
    self.promoTextFied.text=@"";
    self.promoApplyBtn.tag=kApplyButtonApplyState;
    [self updatePromoButtonAppearance];
    [self updatePayNowPriceList:self.bookingObj.chosenPrice ToOriginalPrice:YES];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"Did begin editing");
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"DidEndEditing");
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    NSLog(@"Animated view");
    
    if (IS_iPHONE4) {
        const int movementDistance = -130; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? movementDistance : -movementDistance);
        
        [UIView beginAnimations: @"animateTextField" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];

    }
    
}

@end
