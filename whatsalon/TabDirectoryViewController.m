//
//  TabDirectoryViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 21/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabDirectoryViewController.h"
#import "TabDirectorySalonTableViewCell.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "TabDirectoryDetailViewController.h"
#import "GCNetworkManager.h"
#import "SalonTier4.h"
#import "INTULocationManager.h"
#import "UIView+AlertCompatibility.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ColoredImage.h"

/*! @brief represents a random number for the salon tier loading cell tag. */
const int kSalonTier4LoadingCellTag = 37889;

@interface TabDirectoryViewController ()<UITableViewDataSource,UITableViewDelegate,UIViewControllerTransitioningDelegate,GCNetworkManagerDelegate,UISearchBarDelegate>

/*! @brief represents the network manager for making requests to the server. */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief represents the current page of results. */
@property (nonatomic) NSInteger  currentPage;

/*! @brief represents the total pages of results. */
@property (nonatomic) NSInteger totalPages;

/*! @brief represents the array of salons. */
@property (nonatomic) NSMutableArray * salonsArray;

/*! @brief represents the search timer to prevent multiple searches at one time. */
@property (nonatomic) NSTimer * searchTimer;

/*! @brief represents the current location object. */
@property (nonatomic) CLLocation * currentLocation;

/*! @brief represents the cancel action. */
- (IBAction)cancel:(id)sender;


@end

@implementation TabDirectoryViewController

- (void)checkIfLocationIsDenied {
    
  
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess || status == INTULocationStatusTimedOut) {
                                                 // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                 // currentLocation contains the device's current location.
                                                 NSLog(@"Current Location %f %f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
                                                 
                                                 if ([[User getInstance] isUserLoggedIn]) {
                                                     [[User getInstance] updateUsersLocationWithLocation:currentLocation];
                                                     
                                                     [WSCore updateUsersCityAndCountry:currentLocation];
                                                 }
                                                 else{
                                                     self.currentLocation = currentLocation;
                                                 }
                                                 
                                                 if (self.networkManager == nil) {
                                                     self.networkManager = [[GCNetworkManager alloc] init];
                                                     self.networkManager.delegate=self;
                                                     self.networkManager.parentView=self.view;
                                                 }
                                                
                                                 
                                                 [self fetchSalons];
                                                 
                                                 
                                             }
                                             
                                             else {
                                                 // An error occurred, more info is available by looking at the specific status returned.
                                                 
                                                 switch (status) {
                                                     case INTULocationStatusServicesDenied:
                                                         
                                                         if (IS_IOS_8_OR_LATER) {
                                                             
                                                             UIAlertController * alert=   [UIAlertController
                                                                                           alertControllerWithTitle:@"Location Error"
                                                                                           message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app"
                                                                                           preferredStyle:UIAlertControllerStyleAlert];
                                                             
                                                             UIAlertAction* ok = [UIAlertAction
                                                                                  actionWithTitle:@"Open Settings"
                                                                                  style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action)
                                                                                  {
                                                                                      
                                                                                      if (&UIApplicationOpenSettingsURLString != NULL) {
                                                                                          NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                                          [[UIApplication sharedApplication] openURL:appSettings];
                                                                                      }
                                                                                      
                                                                                  }];
                                                             UIAlertAction* cancel = [UIAlertAction
                                                                                      actionWithTitle:@"Cancel"
                                                                                      style:UIAlertActionStyleDestructive
                                                                                      handler:^(UIAlertAction * action)
                                                                                      {
                                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                          
                                                                                      }];
                                                             
                                                             [alert addAction:cancel];
                                                             [alert addAction:ok];
                                                             
                                                             [self presentViewController:alert animated:YES completion:nil];
                                                         }
                                                         else{
                                                             
                                                             [UIView showSimpleAlertWithTitle:@"Location Error" message:@"You have denied WhatSalon access to location services. In order to use this app, please accept access. " cancelButtonTitle:@"OK"];
                                                         }
                                                         break;
                                                         
                                                         
                                                         
                                                     case INTULocationStatusServicesRestricted:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location services are restricted for WhatSalon. e.g (parental controls corporate policy, etc)" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                     case INTULocationStatusServicesDisabled:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                     case INTULocationStatusError:
                                                         
//                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"An error occurred while using the system location services." cancelButtonTitle:@"OK"];
                                                         break;
                                                     default:
                                                         break;
                                                 }
                                                 
                                             }
                                             NSLog(@"Status %ld",(long)status);
                                             
                                         }];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self checkIfLocationIsDenied];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.networkManager cancelTask];
}

- (IBAction)resetSearch:(id)sender {
    
    
    self.searchBar.text = @"";
    [self killTimerIfValid];
    //reset values
    [self defaultValues];
    //reset table
    [self resetTable];
    //perform search
    [self fetchSalons];
    
    [self.searchBar resignFirstResponder];
}
- (void)killTimerIfValid {
    if (self.searchTimer) {
        if ([self.searchTimer isValid]) {
            [self.searchTimer invalidate];
            
        }
        
        self.searchTimer=nil;
    }
}

/*---------------------------*/
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    
    if (searchBar.text.length==0 || searchBar.text.length<3) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        [self killTimerIfValid];

    }
    else if (searchBar.text.length>=3) {
        NSLog(@"Load filters %@",searchBar.text);
        
        //reset, reload and call
        
        [self defaultValues];
        [self resetTable];
        //[self fetchSearchStringSalons:searchBar.text];
        
        //check if timer is nil or valid
        //if not valid, set nil or invalidate
        //create nstimer and perform search
        
        [self killTimerIfValid];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(fetchSearchStringSalons:) userInfo:searchBar.text repeats:NO];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
}
/*------------------------*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TabDirectoryDetailViewController *destinationViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"directoryDetailVC"];
    destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
    destinationViewController.transitioningDelegate = self;
    destinationViewController.salon=[self.salonsArray objectAtIndex:indexPath.row];
    [self presentViewController:destinationViewController animated:YES completion:nil];
  
}
- (void)fetchSalons {
    NSURL * tier4url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSearch_Tier_4_URL]];
    
    NSString * params = [NSString string];
    
    if ([[User getInstance] isUserLoggedIn]) {
        CLLocation * usersLocation = [[User getInstance] fetchUsersLocation];
                params = [params stringByAppendingString:[NSString stringWithFormat:@"lat=%f",usersLocation.coordinate.latitude]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&lon=%f",usersLocation.coordinate.longitude]];
        
    }
    else{
        params = [params stringByAppendingString:[NSString stringWithFormat:@"lat=%f",self.currentLocation.coordinate.latitude]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&lon=%f",self.currentLocation.coordinate.longitude]];
    }
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%d",300]];//300 km default in order to get any results at all
    
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&page_num=%ld",(long)self.currentPage]];
    
   
    [self.networkManager loadWithURL:tier4url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
}

-(void)fetchSearchStringSalons: (NSString *) searchString{
    
    if (self.searchTimer) {
        searchString = self.searchTimer.userInfo;
    }
    NSURL * tier4url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSearch_Tier_4_URL]];
    
    NSString * params = [NSString string];
  
    
    if ([[User getInstance] isUserLoggedIn]) {
        CLLocation * usersLocation = [[User getInstance] fetchUsersLocation];
        
        params = [params stringByAppendingString:[NSString stringWithFormat:@"lat=%f",usersLocation.coordinate.latitude]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&lon=%f",usersLocation.coordinate.longitude]];
        
    }
    else{
        params = [params stringByAppendingString:[NSString stringWithFormat:@"lat=%f",self.currentLocation.coordinate.latitude]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&lon=%f",self.currentLocation.coordinate.longitude]];
    }
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%@",kWorldCircumference]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&search_string=%@",searchString]];
    
    

  
    [self.networkManager loadWithURL:tier4url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];

}

-(void)resetTable{
    if(self.salonsArray.count>0){
        [self.salonsArray removeAllObjects];
    }
    
    [self.tableView reloadData];
}
- (void)defaultValues {
    // Do any additional setup after loading the view.
    self.currentPage = 1;
    self.salonsArray = [NSMutableArray array];
    self.totalPages=0;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self defaultValues];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView=footerView;
    
    self.view.backgroundColor = [UIColor cloudsColor];
    
    self.tableView.backgroundColor=[UIColor cloudsColor];
    self.tableView.backgroundView=nil;
    
    [WSCore flatNavBarOnView:self];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorColor = kBackgroundColor;
    
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    UIView * searchBackgroundView = [[UIView alloc] initWithFrame:self.searchBar.frame];
    searchBackgroundView.backgroundColor = [UIColor whiteColor];
    [self.view insertSubview:searchBackgroundView belowSubview:self.searchBar];
    
    
    
    self.searchBar.delegate=self;
  
    
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    self.searchBar.returnKeyType=UIReturnKeyDone;

}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
   
    [self defaultValues];
    [self.tableView reloadData];
    
    //footerView
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 80)];
    UILabel * viewLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.tableView.frame.size.width-20, footerView.frame.size.height-20)];
    viewLabel.text = jsonDictionary[@"message"];
    viewLabel.numberOfLines=0;
    viewLabel.textAlignment=NSTextAlignmentCenter;
    viewLabel.textColor = [UIColor lightGrayColor];
    [footerView addSubview:viewLabel];
    viewLabel.font = [UIFont systemFontOfSize:15.0f];
    self.tableView.tableFooterView = footerView;
    
    [WSCore addTopLine:footerView :[UIColor lightGrayColor] :0.5 WithYvalue:footerView.frame.size.height];    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (cell.tag == kSalonTier4LoadingCellTag) {
        self.currentPage++;
        [self fetchSalons];
    }
}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    if (self.tableView.tableFooterView!=nil) {
        self.tableView.tableFooterView=nil;
    }
   
    if (jsonDictionary[@"message"][@"current_page"] != [NSNull null]) {
        NSLog(@"Current page %@",jsonDictionary[@"current_page"]);
        
        self.currentPage = [jsonDictionary[@"message"][@"current_page"] intValue];
        self.totalPages = [jsonDictionary[@"message"][@"total_pages"] intValue];
    }
   
    NSArray * salonsArray = jsonDictionary[@"message"][@"results"];
    for (NSDictionary * sDict in salonsArray) {
        NSLog(@"sDict count %lu",(unsigned long)sDict.count);
        if (sDict[@"salon_name"] !=[NSNull null]) {
            SalonTier4 * s4 = [SalonTier4 salonWithSalonName:sDict[@"salon_name"]];
            
            if (sDict[@"country_code"] !=[NSNull null]) {
                s4.distance = [NSString stringWithFormat:@"%@",sDict[@"country_code"]];
            }
            
            if (sDict[@"distance"] !=[NSNull null]) {
                s4.distance = sDict[@"distance"];
            }
            
            if (sDict[@"image_link"] !=[NSNull null]) {
                s4.imageUrlLink = sDict[@"image_link"];
            }
            
            if (sDict[@"location_lat"] !=[NSNull null]) {
                s4.locationLat = sDict[@"location_lat"];
            }
            
            if (sDict[@"location_lon"] !=[NSNull null]) {
                s4.locationLon = sDict[@"location_lon"];
            }
            
            if (sDict[@"postcode_zip"] != [NSNull null]){
                
                s4.postCode = sDict[@"postcode_zip"];
            }
            
            if (sDict[@"salon_address"] !=[NSNull null]) {
                s4.salonAddress = sDict[@"salon_address"];
            }
            if (sDict[@"salon_county"] != [NSNull null]) {
                s4.salonCounty = sDict[@"salon_county"];
            }
            if (sDict[@"salon_name"] !=[NSNull null]) {
                s4.salonName = sDict[@"salon_name"];
            }
            if (sDict[@"salon_phone"] !=[NSNull null]) {
                s4.salonPhone = sDict[@"salon_phone"];
            }
            
            NSLog(@"Salon name %@",s4.salonName);
            
            [self.salonsArray addObject:s4];
            
            
        }
        
      
        
       
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    
    if (self.currentPage<self.totalPages) {
        if(self.salonsArray.count>0){
            
           return self.salonsArray.count+1;
        }
        
    }
    return self.salonsArray.count;

    
}
- (TabDirectorySalonTableViewCell *)SalonCellForIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    
   
    TabDirectorySalonTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    SalonTier4 * salon;
    salon = [self.salonsArray objectAtIndex:indexPath.row];
    
    [cell configureCell:salon];
    [cell.salonImage sd_setImageWithURL:[NSURL URLWithString:salon.imageUrlLink] placeholderImage:kPlace_Holder_Image];
    cell.salonImage.contentMode = UIViewContentModeScaleAspectFit;
   
    return cell;
}

-(UITableViewCell *)loadingCell{
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    cell.tag = kSalonTier4LoadingCellTag;
    
    return cell;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row<self.salonsArray.count) {
        return [self SalonCellForIndexPath:indexPath tableView:tableView];
    }
    else{
        return  [self loadingCell];
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    

        return [[PresentDetailTransition alloc] init];
  
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
 
        return [[DismissDetailTransition alloc] init];
    
}

@end
