//
//  BrowseTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 15/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"
#import "LastMinuteItem.h"
#import "GCNetworkManager.h"


@protocol BrowseDelegate <NSObject>

@optional
-(void)bookNowActionTriggeredWithLastMinuteItem: (LastMinuteItem *)l;
-(void)showMapWithLastMinuteItem: (LastMinuteItem *) ml;
-(void)showReviewsWithLastMinuteItem: (LastMinuteItem *)rl;
-(void)didAddFavourite:(Salon *)salon;
-(void)didUnFavourite: (Salon *)salon;
@end


@interface BrowseTableViewCell : UITableViewCell<GCNetworkManagerDelegate>
@property (nonatomic,strong) UILabel * salonName;
@property (nonatomic,strong) UIImageView * salonImage;
@property (nonatomic,strong) UIImageView * overlayLabel;
@property (nonatomic,strong) UILabel * addressLabel;
@property (nonatomic,strong) UILabel * labelMiles;
@property (nonatomic,strong) UILabel * labelOffer;
@property (nonatomic,strong) UIImageView * favLabel;
@property (nonatomic,strong) UILabel* selectFavLabel;
@property (nonatomic) NSString * salonID;
@property (nonatomic) NSString * address1;
@property (nonatomic) NSString * address2;
@property (nonatomic) NSString * address3;
@property (nonatomic) NSString * city_name;
@property (nonatomic,assign) BOOL isFav;
@property (nonatomic) UILabel * whiteLabel;
@property (nonatomic) UIImageView * bubble;
@property (nonatomic) BOOL isDiscovery;
@property (nonatomic) UIViewController * parent;
@property (nonatomic) BOOL addedShadow;

@property (nonatomic) UILabel * mapHolder;
@property (nonatomic) UILabel * ratingsHolder;


@property (nonatomic) id<BrowseDelegate> myDelegate;
@property (nonatomic) Salon * salon;
@property (nonatomic) Booking *bookingObj;
@property (nonatomic) LastMinuteItem * lastMin;

@property (nonatomic) GCNetworkManager *networkManager;


//for tab
@property (nonatomic) BOOL isTab;

-(void)setCellImageWithURL: (NSString*) url;

-(void)setUpCellWithSalon: (id) s;

@property (nonatomic) UIImageView * instantBookImage;
@property (nonatomic) NSInteger cellIndexPathRow;
@property (nonatomic) BOOL isNearest;
@end
