//
//  SalonBookingModel.m
//  whatsalon
//
//  Created by Graham Connolly on 27/11/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonBookingModel.h"
#import "UIView+AlertCompatibility.h"
#import "SalonService.h"
#import "PhorestStaffMember.h"
#import "SalonAvailableTime.h"


#import "User.h"

NSString * hairKey = @"Hair";
NSString * hairRemovalKey = @"Hair Removal";
NSString * nailsKey = @"Nails";
NSString * massageKey = @"Massage";
NSString * faceKey = @"Face";
NSString * bodyKey = @"Body";


/*
 Steps to integrate salon
 - create new RequestType
 - use '-(void)makeBookingRequestWithURL: (NSURL *)url WithParameters: (NSString *) params AndWithRequestType: (RequestType) requestType' and create a Services Delegate method that is implemented in TabMakeBookingViewController, TabConfirmViewController, TabCongratulationsViewController. See delegate examples for the other integrations seen in the the above classes.
 - each integration on the backend should follow the same pattern and display the same data in the same form
 
 */
typedef enum : NSUInteger {
    RequestTypePhorestBooking =0,
    RequestTypePremierBooking,
    RequestTypeIsalonBookingPayment,
    RequestTypePremierClientCreate,
    RequestTypePhorestPayment
} RequestType;


@implementation SalonBookingModel
- (void)sortServicesArray:(NSMutableArray *)array {
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"service_name" ascending:YES  selector:@selector(caseInsensitiveCompare:)];
    [array sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
   
}

-(NSMutableArray *)fetchServicesListWithID : (NSString *) s_id{
    
    //s_id=[NSString stringWithFormat:@"%d",kDummySalonID];
    NSMutableArray * mArray = [NSMutableArray array];
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Phorest_services_List_URL]];
    NSString * param = [NSString stringWithFormat:@"salon_id=%@",s_id];
    param = [param stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken]]];
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[param dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        if (error==nil) {
            
            if (dataDict!=nil) {
                
                switch (httpResp.statusCode) {
                    case kOKStatusCode:{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //NSLog(@"Service list %@",dataDict);
                           
                            if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                                
                                
                                NSMutableArray * hairArray = [[NSMutableArray alloc] init];
                                SalonService * headerServ = [[SalonService alloc] init];
                                headerServ.category_name = hairKey;
                                headerServ.isHeader=YES;
                                [hairArray addObject:headerServ];
                                
                                NSMutableArray * hairRemovalArray = [[NSMutableArray alloc] init];
                                SalonService * headerRemovalServ = [[SalonService alloc] init];
                                headerRemovalServ.category_name = hairRemovalKey;
                                headerRemovalServ.isHeader=YES;
                                [hairRemovalArray addObject:headerRemovalServ];
                                
                                NSMutableArray * nailsArray = [[NSMutableArray alloc] init];
                                SalonService * nailsServ = [[SalonService alloc] init];
                                nailsServ.category_name = nailsKey;
                                nailsServ.isHeader=YES;
                                [nailsArray addObject:nailsServ];
                                
                                NSMutableArray * massageArray = [[NSMutableArray alloc] init];
                                SalonService * massageServ = [[SalonService alloc] init];
                                massageServ.category_name = massageKey;
                                massageServ.isHeader=YES;
                                [massageArray addObject:massageServ];
                                
                                NSMutableArray * faceArray = [[NSMutableArray alloc] init];
                                SalonService * faceServ = [[SalonService alloc] init];
                                faceServ.category_name = faceKey;
                                faceServ.isHeader=YES;
                                [faceArray addObject:faceServ];
                                
                                NSMutableArray * bodyArray = [[NSMutableArray alloc] init];
                                SalonService * bodyServ = [[SalonService alloc] init];
                                bodyServ.category_name = bodyKey;
                                bodyServ.isHeader=YES;
                                [bodyArray addObject:bodyServ];
                                
                                for (id key in dataDict[@"message"]) {
                                    SalonService * pService = [[SalonService alloc] init];
                                    
                                    pService.category_name = key[@"category_name"];
                                    pService.service_name=key[@"service_name"];
                                    pService.service_id=[NSString stringWithFormat:@"%@",key[@"service_id"]];
                                    pService.price=[NSString stringWithFormat:@"%@",key[@"service_price"]];
                                    
                                    if ([[pService.category_name lowercaseString] isEqualToString:[hairKey lowercaseString]]) {
                                        [hairArray addObject:pService];
                                    }else  if ([[pService.category_name lowercaseString] isEqualToString:[hairRemovalKey lowercaseString]]) {
                                        [hairRemovalArray addObject:pService];
                                    }else  if ([[pService.category_name lowercaseString] isEqualToString:[nailsKey lowercaseString]]) {
                                        [nailsArray addObject:pService];
                                    }
                                    else  if ([[pService.category_name lowercaseString] isEqualToString:[massageKey lowercaseString]]) {
                                        [massageArray addObject:pService];
                                    }else  if ([[pService.category_name lowercaseString] isEqualToString:[faceKey lowercaseString]]) {
                                        [faceArray addObject:pService];
                                    }
                                    else  if ([[pService.category_name lowercaseString] isEqualToString:[bodyKey lowercaseString]]) {
                                        [bodyArray addObject:pService];
                                    }
                                    //[mArray addObject:pService];
                                }
                                
                                if (hairArray.count>1) {
                                    
                                    
                                    //[self sortServicesArray:hairArray];
                                    [mArray addObjectsFromArray:hairArray];
                                }
                                if (hairRemovalArray.count>1) {
                                    //[self sortServicesArray:hairRemovalArray];
                                    [mArray addObjectsFromArray:hairRemovalArray];
                                }
                                if (nailsArray.count>1) {
                                    //[self sortServicesArray:nailsArray];
                                    [mArray addObjectsFromArray:nailsArray];
                                }
                                if(massageArray.count>1){
                                    //[self sortServicesArray:massageArray];
                                    [mArray addObjectsFromArray:massageArray];
                                }
                                if (faceArray.count>1) {
                                   // [self sortServicesArray:faceArray];
                                    [mArray addObjectsFromArray:faceArray];
                                }
                                if (bodyArray.count>1) {
                                    
                                   // [self sortServicesArray:bodyArray];
                                    [mArray addObjectsFromArray:bodyArray];
                                }
                                
                                
                               
                            
                                
                                
                                if ([self.myDelegate respondsToSelector:@selector(didGetServices:)]) {
                                    
                                      [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                   
                                    [self.myDelegate didGetServices:mArray];
                                }
                                
                            }else if ([dataDict[kSuccessIndex] isEqualToString:@"false"]){
                                
                                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                [WSCore showServerErrorAlert];
                            }
                        });
                    }
                        break;
                        
                    default:
                        dispatch_async(dispatch_get_main_queue(), ^{
                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                            [WSCore showServerErrorAlert];
                        });
                        break;
                }
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                      [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                    [WSCore showServerErrorAlert];
                });
            }
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                [UIView showSimpleAlertWithTitle:@"Error" message:[NSString stringWithFormat:@"Salon Services could not be downloaded. %@",[error localizedDescription] ] cancelButtonTitle:@"OK"];
            });
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        [self.dataTask resume];
    }
    else{
        
        [WSCore showNetworkErrorAlert];
    }
    
    return mArray;
}

-(void)fetchStaffWithSalonID:(NSString *)s_id{
    //s_id=[NSString stringWithFormat:@"%@",kDummySalonIDForStaff];
    NSMutableArray * staffArray = [NSMutableArray array];
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Phorest_Staff_URL]];
    NSString * param = [NSString stringWithFormat:@"salon_id=%@",s_id];
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[param dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
   self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
            NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    
                    //NSLog(@"Response: %@",dataDict);
                    if (dataDict!=nil && error==nil) {
                        
                        if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                        
                            for (id key in dataDict[@"message"]) {
                               
                                PhorestStaffMember * staff = [[PhorestStaffMember alloc] init];
                                staff.staff_first_name = [NSString stringWithFormat:@"%@",key[@"staff_first_name"]];
                                staff.staff_last_name = [NSString stringWithFormat:@"%@",key[@"staff_last_name"]];
                                staff.staff_id = [NSString stringWithFormat:@"%@",key[@"staff_id"]];
                               
                                [staffArray addObject:staff];
                            }
                            
                            /*
                            
                            if ([self.myDelegate respondsToSelector:@selector(didGetStaff:)]) {
                                [self.myDelegate didGetStaff:staffArray];
                            }
                             */
                            
                        }
                    }
                    break;
                    
                default:
                    break;
            }
       
      
    }];
    
    if([WSCore isNetworkReachable]){
        [self.dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
    
    
    
}

- (void)sortStaffArray:(NSMutableArray *)array {
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"staff_first_name" ascending:YES  selector:@selector(caseInsensitiveCompare:)];
    [array sortUsingDescriptors:[NSArray arrayWithObject:sorter]];
}

/*
 refactor name to more general name
 */
-(void)fetchStaffAndTimesWithSalonID:(NSString *) s_id WithServiceID: (NSString *) service_id WithStaffID: (NSString *) staff_id WithStartDatetime: (NSString *) start_datetime WithEndDateTime: (NSString *) end_datetime{
    
   
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Phorest_Service_Available_Times]];
    NSString * params =[NSString stringWithFormat:@"salon_id=%@",s_id];
    params =[params stringByAppendingString:[NSString stringWithFormat:@"&service_id=%@",service_id
                                             ]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&start_datetime=%@",start_datetime]];
   
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&end_datetime=%@",end_datetime]];
   
    
    
    NSMutableURLRequest * req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [req setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [req setHTTPMethod:@"POST"];
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    
    self.dataTask = [session dataTaskWithRequest:req completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        
        if (data!=nil) {
            NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            if (dataDict !=nil && error ==nil) {
                
                //NSLog(@"Phorest staff and time \n\n\n%@ \n\n\n\n",dataDict);
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                        
                        if([dataDict[@"success"] isEqualToString:@"true"]){
                            
                            NSMutableArray * mArray = [NSMutableArray array];
                            
                            NSMutableArray * idArray = [NSMutableArray array];
                            
                            for (id key in dataDict[@"message"]) {
                                
                                PhorestStaffMember * staff = [[PhorestStaffMember alloc] init];
                                if ([key[@"staff"] isKindOfClass:[NSDictionary class]] && key[@"staff"][@"staff_id"]){
                                    
                                    //NSLog(@"\n\n KEY \n\n %@ \n\n",key);
                                    staff.staff_id = key[@"staff"][@"staff_id"];
                                    staff.staff_first_name=key[@"staff"][@"staff_first_name"];
                                    staff.staff_last_name=key[@"staff"][@"staff_last_name"];
                                    staff.staff_id = key[@"staff"][@"staff_id"];
                                    if (key[@"service_price"] !=[NSNull null]) {
                                        staff.staff_service_price = key[@"service_price"];
                                    }
                                    
                                    if (key[@"isalon_booking_details"] != [NSNull null]) {
                                        if (key[@"isalon_booking_details"][@"isalon_service_id"] != [NSNull null]) {
                                            staff.isalon_booking_details.iSalon_service_id = key[@"isalon_booking_details"][@"isalon_service_id"];
                                        }
                                        
                                        if (key[@"isalon_booking_details"][@"salon_id"] != [NSNull null]) {
                                            staff.isalon_booking_details.salon_id = key[@"isalon_booking_details"][@"salon_id"];
                                        }
                                        
                                        if (key[@"isalon_booking_details"][@"staff_id"] != [NSNull null]) {
                                            staff.isalon_booking_details.staff_id = key[@"isalon_booking_details"][@"staff_id"];
                                        }
                                        
                                        if (key[@"isalon_booking_details"][@"start_time"] != [NSNull null]) {
                                            staff.isalon_booking_details.start_time = key[@"isalon_booking_details"][@"start_time"];
                                        }
                                        
                                        if (key[@"isalon_booking_details"][@"isalon_service_id"] != [NSNull null]) {
                                            staff.isalon_booking_details.iSalon_service_id = key[@"isalon_booking_details"][@"isalon_service_id"];
                                        }
                                        
                                    }
                                    
                                    //NSLog(@"%@",[staff.isalon_booking_details fetchBookingDescription]);
                                    
                                    NSString * searchID =staff.staff_id;
                                    if (![idArray containsObject:searchID]) {
                                        [idArray addObject:searchID];
                                        
                                        [mArray addObject:staff];
                                    }
                                    
                                    
                                    if ([idArray containsObject:searchID]) {
                                        NSInteger row = [idArray indexOfObject:searchID];
                                        
                                        PhorestStaffMember * staff2 = [mArray objectAtIndex:row];
                                        SalonAvailableTime * avTime = [[SalonAvailableTime alloc] init];
                                        avTime.slot_id = key[@"slot_id"];
                                        avTime.start_time=key[@"start_datetime"];
                                        avTime.end_time=key[@"end_datetime"];
                                        avTime.staff_id=key[@"staff"][@"staff_id"];
                                        avTime.internal_start_datetime=key[@"internal_start_datetime"];
                                        avTime.internal_end_datetime=key[@"internal_end_datetime"];
                                        avTime.end_time =key[@"end_datetime"];
                                        avTime.room_id=key[@"room_id"];
                                        NSLog(@"Patch test %@", key[@"patch_test_required"]);
                                        avTime.patch_test_required = [key[@"patch_test_required"] boolValue];
                                        NSLog(@"av time patch test required %@",StringFromBOOL(avTime.patch_test_required));
                                        NSLog(@"END time %@",avTime.end_time);
                                        
                                        
                                        [staff2.times addObject:avTime];
                                        
                                        
                                        [mArray replaceObjectAtIndex:row withObject:staff2];
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                            }
                            
                            
                            if ([self.myDelegate respondsToSelector:@selector(didGetStaff:)]) {
                                NSLog(@"MArray %lu ",(unsigned long)mArray.count);
                                
                                [self sortStaffArray:mArray];
                                [self.myDelegate didGetStaff:mArray];
                            }
                        }
                        else{
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [WSCore dismissNetworkLoadingOnView:self.view];
                                //[WSCore dismissNetworkLoading];
                                [UIView showSimpleAlertWithTitle:@"Oh Oh" message:[NSString stringWithFormat:@"%@.",dataDict[@"message"]] cancelButtonTitle:@"OK"];
                            });
                            
                        }
                        
                        break;
                        
                    default:
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [WSCore dismissNetworkLoadingOnView:self.view];
                            //[WSCore dismissNetworkLoading];
                            if (error.code != NSURLErrorCancelled) {
                                NSLog(@"Show server error");
                                [WSCore showServerErrorAlert];
                            }
                            else{
                                NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                            }
                            
                        });
                        break;
                }
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [WSCore dismissNetworkLoadingOnView:self.view];
                    if (error.code != NSURLErrorCancelled) {
                        NSLog(@"Show server error");
                        [WSCore showServerErrorAlert];
                    }
                    else{
                        NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                    }
                });
            }
            
            
        }//data is nil
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [WSCore dismissNetworkLoadingOnView:self.view];
                [UIView showSimpleAlertWithTitle:@"Houston, we have a server issue" message:@"A server issue occurred when dealing with the data. Please try again shortly." cancelButtonTitle:@"OK"];//data parameter is nil
            });
        }
        
        
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        [WSCore showNetworkLoadingOnView:self.view];
        //[WSCore showNetworkLoading];
        [self.dataTask resume];
       
    }
    else{
        [WSCore showNetworkErrorAlert];
    }

    
    
}



-(void)bookPhorestServiceWithUserID: (NSString *) user_id WithSalonID: (NSString *) s_id WithSlotID: (NSString *)slot_id WIthServiceID: (NSString *)service_id WithRoomID: (NSString *)room_id WithInternalStartDateTime: (NSString *) startTime WithInternalEndDateTime: (NSString *) endTime AndStaff_id: (NSString *) staff_id{
   // s_id=[NSString stringWithFormat:@"%d",KPhorestSalonID];
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kBook_Phorest_Service_URL]];
    NSString * param = [NSString stringWithFormat:@"user_id=%@",user_id];//1
    param = [param stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",s_id]];//2
    param =[param stringByAppendingString:[NSString stringWithFormat:@"&slot_id=%@",slot_id]];//3
    param =[param stringByAppendingString:[NSString stringWithFormat:@"&room_id=%@",room_id]];//4
    
    param=[param stringByAppendingString:[NSString stringWithFormat:@"&internal_start_datetime=%@",startTime]];//5
    param=[param stringByAppendingString:[NSString stringWithFormat:@"&internal_end_datetime=%@",endTime]];//6
     param=[param stringByAppendingString:[NSString stringWithFormat:@"&staff_id=%@",staff_id]];//7
     param=[param stringByAppendingString:[NSString stringWithFormat:@"&service_id=%@",service_id]];//8
    
    if ([[User getInstance] fetchKey].length!=0) {
        param = [param stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
    }
     
    [self makeBookingRequestWithURL:url WithParameters:param AndWithRequestType:RequestTypePhorestBooking];
    
    /*
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[param  dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        
       
        if (!error && dataDict!=nil) {
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    
                    NSLog(@"Booking message: %@",dataDict[@"message"]);
                    
                    if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                        
                
                        if ([self.myDelegate respondsToSelector:@selector(successfullAppointmentWithDictionary:)]) {
                            [self.myDelegate successfullAppointmentWithDictionary:dataDict];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [WSCore dismissNetworkLoadingOnView:self.view];

                            });
                            
                        }
                    }
                    else{
                   
                        if ([self.myDelegate respondsToSelector:@selector(failureAppointmentWithDictionary:)]) {
                            [self.myDelegate failureAppointmentWithDictionary:dataDict];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [WSCore dismissNetworkLoadingOnView:self.view];
                            });
                        }
                    }
                    
                    
                    
                    break;
                    
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        [WSCore showServerErrorAlert];
                    });
                    break;
            }
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
                if (error.code != NSURLErrorCancelled) {
                    NSLog(@"Show server error");
                    [WSCore showServerErrorAlert];
                }
                else{
                    NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                }

            });
        }
        
        
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        [WSCore showNetworkLoadingOnView:self.view WithTitle:@"Booking..."];
        [self.dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
    */
    
}


-(void) phorestPaymentWithApptRef: (NSString *) appt_ref{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kPay_Phorest_Booking_URL]];
    NSString * params = [NSString stringWithFormat:@"appointment_reference_number=%@",appt_ref];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    //phorestPaymentWithApptRef is deprecated so bottom method wont be tested. 
    [self makeBookingRequestWithURL:url WithParameters:params AndWithRequestType:RequestTypePhorestPayment];
   
    /*
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        NSLog(@"Http resp %ld",(long)httpResp.statusCode);
      
        if (!error && dataDict!=nil) {
            NSLog(@"Booking Data Dict %@",dataDict);
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
            });
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    
                    
                    
                    if ([self.myDelegate respondsToSelector:@selector(phorestBookingResult:WithMessage:)]) {
                        
                        NSString * success = dataDict[kSuccessIndex];
                        NSString * message = dataDict[kMessageIndex];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.myDelegate phorestBookingResult:[success boolValue] WithMessage:message];
                        });
                        
                        
                    }
                    break;
                    
                default:
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        [WSCore showServerErrorAlert];
                    });
                    break;
            }
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
                if (error.code != NSURLErrorCancelled) {
                    NSLog(@"Show server error");
                    [WSCore showServerErrorAlert];
                }
                else{
                    NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                }

            });
        }
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        [WSCore showNetworkLoadingOnView:self.view];
        [self.dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
     */
}

-(void)fetchStaff:(NSString *)s_id{
   
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kPhorest_Staff_URL]];
    NSString * param = [NSString stringWithFormat:@"salon_id=%@",s_id];
    
    NSMutableURLRequest * req = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [req setHTTPBody:[param dataUsingEncoding:NSUTF8StringEncoding]];
    [req setHTTPMethod:@"POST"];
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:req completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (data!=nil) {
            
            NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *) response;
            
            
            if (error==nil ||dataDict==nil) {
                
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                        //NSLog(@"***** \n\n All Staff:\n %@ \n\n *****",dataDict);
                        
                        if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                            
                            NSMutableArray * staffArray = [NSMutableArray array];
                            if ([dataDict[@"message"] isKindOfClass:[NSArray class]] ) {
                                for (id key in dataDict[@"message"]) {
                                    PhorestStaffMember * staff = [[PhorestStaffMember alloc] init];
                                    staff.staff_id = key[@"staff_id"];
                                    staff.staff_first_name = key[@"staff_first_name"];
                                    staff.staff_last_name = key[@"staff_last_name"];
                                    
                                    if (dataDict[@"service_price"] != [NSNull null]) {
                                        staff.staff_service_price = dataDict[@"service_price"];
                                    }
                                    
                                    
                                    [staffArray addObject:staff];
                                    
                                }
                                
                                if ([self.myDelegate respondsToSelector:@selector(didGetAllStaff:)]) {
                                    
                                    [self sortStaffArray:staffArray];
                                    [self.myDelegate didGetAllStaff:staffArray];
                                }
                                
                            }else if([dataDict[@"message"] isKindOfClass:[NSObject class]]){
                                
                                NSLog(@"One Staff memeber %@",dataDict[@"message"]);
                                
                                
                                PhorestStaffMember * staff = [[PhorestStaffMember alloc] init];
                                staff.staff_id = dataDict[@"message"][@"staff_id"];
                                staff.staff_first_name =dataDict[@"message"][@"staff_first_name"];
                                staff.staff_last_name = dataDict[@"message"][@"staff_last_name"];
                                
                                
                                [staffArray addObject:staff];
                                
                                
                            }
                            else{
                                NSLog(@"Not array");
                            }
                            
                            
                        }
                        else{
                            
                        }
                        break;
                        
                    default:
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            NSLog(@"Server Error ");
                        
                        });
                        break;
                }
                
            }
            else{
                
                NSLog(@"Error");
            }

            
        }
        else{
            
            //no data = data is nil
            
            
        }
            }];
    
    if ([WSCore isNetworkReachable]) {
        
        [self.dataTask resume];
    }
}

-(void)cancelBookingModel{
    
    [WSCore dismissNetworkLoadingOnView:self.view];
    [self.dataTask cancel];
    
    
}



#pragma mark - PayPhorestBookingWithCoupon

-(void)makeBookingRequestWithURL: (NSURL *)url WithParameters: (NSString *) params AndWithRequestType: (RequestType) requestType{
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        
        
        if (!error && dataDict!=nil) {
            
            
            if (httpResp.statusCode==kOKStatusCode) {
                        
               // NSLog(@"Booking Info: %@",dataDict);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        
                        if ([[dataDict[kSuccessIndex] lowercaseString] isEqualToString:@"true"]) {
                            
                            switch (requestType) {
                                case RequestTypePremierBooking:
                                    
                                    if ([self.myDelegate respondsToSelector:@selector(premierSuccessfulBookingWithMessage:)]) {
                                        
                                        [self.myDelegate premierSuccessfulBookingWithMessage:dataDict[@"message"]];
                                        
                                    }
                                    
                                    break;
                                    
                                case RequestTypePremierClientCreate:
                                    
                                    if ([self.myDelegate respondsToSelector:@selector(createPremierClientSuccessfulWithMessage:)]) {
                                        
                                        [self.myDelegate createPremierClientSuccessfulWithMessage:dataDict[@"message"]];
                                        
                                    }

                                    break;
                                    
                                case RequestTypeIsalonBookingPayment:
                                    
                                    if ([self.myDelegate respondsToSelector:@selector(iSalonSuccessfulBookingWithMessage:)]) {
                                        
                                        [self.myDelegate iSalonSuccessfulBookingWithMessage:dataDict[@"message"]];
                                        
                                    }

                                    break;
                                    
                                case RequestTypePhorestPayment:
                                    if ([self.myDelegate respondsToSelector:@selector(phorestSuccessfulBookingWithMessage:)]) {
                                        
                                        [self.myDelegate phorestSuccessfulBookingWithMessage:dataDict[@"message"]];
                                    }
                                    
                                    break;
                                    
                                case RequestTypePhorestBooking:
                                    if ([self.myDelegate respondsToSelector:@selector(successfullAppointmentWithDictionary:)]) {
                                        [self.myDelegate successfullAppointmentWithDictionary:dataDict];
                                      
                                    }

                                    
                                    break;
                                    
                                default:
                                    NSLog(@"Unassigned Request");
                                    break;
                            }
                           
                            
                            
                        }
                        else{
                            
                            //success is failure coming from server
                            switch (requestType) {
                                case RequestTypePremierBooking:
                                    
                                    
                                    if([self.myDelegate respondsToSelector:@selector(premierFailureBookingWithMessage:)]){
                                        
                                        [self.myDelegate premierFailureBookingWithMessage:dataDict[@"message"]];
                                    }
                                    
                                    
                                    break;
                                    
                                    case RequestTypePremierClientCreate:
                                    
                                        if([self.myDelegate respondsToSelector:@selector(createPremierClientFailureWithMessage:)]){
                                        
                                            [self.myDelegate createPremierClientFailureWithMessage:dataDict[@"message"]];
                                    }

                                    
                                    break;
                                    
                                    case RequestTypeIsalonBookingPayment:
                                    
                                    if([self.myDelegate respondsToSelector:@selector(iSalonFailureBookingWithMessage:)]){
                                        
                                        [self.myDelegate iSalonFailureBookingWithMessage:dataDict[@"message"]];
                                    }
                                    
                                    break;
                                    
                                    case RequestTypePhorestPayment:
                                    
                                    if ([self.myDelegate respondsToSelector:@selector(phorestFailureBookingWithMessage:)]) {
                                        [self.myDelegate phorestFailureBookingWithMessage:dataDict[@"message"]];
                                    }
                                    
                                    break;
                                    
                                    case RequestTypePhorestBooking:
                                    
                                    if ([self.myDelegate respondsToSelector:@selector(failureAppointmentWithDictionary:)]) {
                                        [self.myDelegate failureAppointmentWithDictionary:dataDict];
                                    }

                                    
                                    break;
                                    
                                default:
                                    
                                    NSLog(@"Unassigned Request");
                                    break;
                            }
                            
                            
                        }
                    });
                }
            
            
            else {
                //not status 200 from server
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        [WSCore showServerErrorAlert];
                    });
            }
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
                if (error.code != NSURLErrorCancelled) {
                    
                    NSLog(@"Show server error");
                    
                    [WSCore showServerErrorAlert];
                }
                else{
                    NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                }
                
            });
            
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        
        if (RequestTypePhorestBooking) {
            [WSCore showNetworkLoadingOnView:self.view WithTitle:@"Booking..."];

        }
        else{
            [WSCore showNetworkLoadingOnView:self.view];
        }
        
        [self.dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }

}
-(void)premierBookServiceWithBookingObject: (Booking *) bookingObject withPromoCode: (NSString *) coupon{
    
    
  
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,@"book_premier_service"]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance]fetchKey ]]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",bookingObject.bookingSalon.salon_id]];
    
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",bookingObject.bookingSalon.salon_id]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&service_id=%@",bookingObject.service_id]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&staff_id=%@",bookingObject.staff_id]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&internal_start_datetime=%@",bookingObject.internal_start_datetime]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&internal_end_datetime=%@",bookingObject.internal_end_datetime]];
    
    if (coupon.length>0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&coupon_name=%@",coupon]];
    }
    
  
    [self makeBookingRequestWithURL:url WithParameters:params AndWithRequestType:RequestTypePremierBooking];
    
    //folded logic below as new method is being used above
    /*
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        
        NSLog(@"\n\n ****** \n\n Premier Confirm Booking \n %@ \n\n **********",dataDict);
        
        if (!error && dataDict!=nil) {
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        
                        if ([[dataDict[kSuccessIndex] lowercaseString] isEqualToString:@"true"]) {
                            
                            
                            if ([self.myDelegate respondsToSelector:@selector(premierSuccessfulBookingWithMessage:)]) {
                                
                                [self.myDelegate premierSuccessfulBookingWithMessage:dataDict[@"message"]];
                                
                            }
                            
                            
                        }
                        else{
                            
                            if([self.myDelegate respondsToSelector:@selector(premierFailureBookingWithMessage:)]){
                                
                                [self.myDelegate premierFailureBookingWithMessage:dataDict[@"message"]];
                            }
                            
                        }
                    });
                }
                    break;
                    
                default:
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        [WSCore showServerErrorAlert];
                    });
                    
                    break;
            }
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
                if (error.code != NSURLErrorCancelled) {
                    
                    NSLog(@"Show server error");
                    
                    [WSCore showServerErrorAlert];
                }
                else{
                    NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                }
                
            });
            
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [WSCore showNetworkLoadingOnView:self.view];
        [self.dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
*/
    
}
-(void)premierCreateClientWithBookingObject: (Booking *) bookingObject {
    
    
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,@"reserve_premier_booking"]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance]fetchKey ]]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",bookingObject.bookingSalon.salon_id]];
    
    
    [self makeBookingRequestWithURL:url WithParameters:params AndWithRequestType:RequestTypePremierClientCreate];
    
    //folded logic below as new method is being used
    /*
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        
        NSLog(@"\n\n ****** \n\n Create Premier client \n %@ \n\n **********",dataDict);
        
        if (!error && dataDict!=nil) {
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        
                        if ([[dataDict[kSuccessIndex] lowercaseString] isEqualToString:@"true"]) {
                            
                            
                            if ([self.myDelegate respondsToSelector:@selector(createPremierClientSuccessfulWithMessage:)]) {
                                
                                [self.myDelegate createPremierClientSuccessfulWithMessage:dataDict[@"message"]];
                                
                            }
                            
                            
                        }
                        else{
                            
                            if([self.myDelegate respondsToSelector:@selector(createPremierClientFailureWithMessage:)]){
                                
                                [self.myDelegate createPremierClientFailureWithMessage:dataDict[@"message"]];
                            }
                            
                        }
                    });
                }
                    break;
                    
                default:
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        [WSCore showServerErrorAlert];
                    });
                    
                    break;
            }
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
                if (error.code != NSURLErrorCancelled) {
                    
                    NSLog(@"Show server error");
                    
                    [WSCore showServerErrorAlert];
                }
                else{
                    NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                }
                
            });
            
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [WSCore showNetworkLoadingOnView:self.view];
        [self.dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }

    */
}
/*
 iSalonPaymentWithBookingObject: (Booking *) bookingObject AndWithPromoCode: (NSString *) coupong
 
 * @discussion attempts to make an iSalon booking. If successfull 'iSalonSuccessfullBookingWithMessage' delegate method is called, if unscussful 'iSalonFailureBookingWithMessage' is called. If network request fails, it is handled accordingly with a server alert.
 * @param bookingObject A Booking that contains information about the booking
 * @param coupon An NSString which represents a coupong/discount
  */
-(void)iSalonPaymentWithBookingObject: (Booking *) bookingObject AndWithPromoCode: (NSString *) coupon{
    
    
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kBook_Isalon_Service_URL]];
    NSString * params = [NSString stringWithFormat:@"customer_id=%@",[[User getInstance] fetchAccessToken ]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&internal_start_datetime=%@",bookingObject.internal_start_datetime]];
    
    //isalon staff id
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&isalon_service_id=%@",bookingObject.isalonBookingDetails.iSalon_service_id]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&isalon_staff_id=%@",bookingObject.isalonBookingDetails.staff_id]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&isalon_salon_id=%@",bookingObject.isalonBookingDetails.salon_id]];
    
    //isalon salon id
    if (coupon.length>0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&coupon_name=%@",coupon]];
    }
    
    
    [self makeBookingRequestWithURL:url WithParameters:params AndWithRequestType:RequestTypeIsalonBookingPayment];
    
    //uncommented below to use the new method above
    /*
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
       
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        
        NSLog(@"JSON response \n %@",dataDict);
        
        if (!error && dataDict!=nil) {
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                         [WSCore dismissNetworkLoadingOnView:self.view];
                        
                        if ([[dataDict[kSuccessIndex] lowercaseString] isEqualToString:@"true"]) {
                            
                            
                            if ([self.myDelegate respondsToSelector:@selector(iSalonSuccessfulBookingWithMessage:)]) {
                                
                                [self.myDelegate iSalonSuccessfulBookingWithMessage:dataDict[@"message"]];
                                
                            }
                           
                            
                        }
                        else{
                            
                            if([self.myDelegate respondsToSelector:@selector(iSalonFailureBookingWithMessage:)]){
                                
                                [self.myDelegate iSalonFailureBookingWithMessage:dataDict[@"message"]];
                            }

                        }
                    });
                }
                    break;
                    
                default:
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        [WSCore showServerErrorAlert];
                    });
                    
                    break;
            }
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
                if (error.code != NSURLErrorCancelled) {
                    
                    NSLog(@"Show server error");
                    
                    [WSCore showServerErrorAlert];
                }
                else{
                    NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                }
                
            });
        
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [WSCore showNetworkLoadingOnView:self.view];
        [self.dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
*/
}

/*!
 * @discussion Makes Phorest payment for an appointment
 * @param appt_ref - An NSString representing the appointment reference number
 * @param coupon - An NSString representing a coupon code for discounts
 */
-(void) phorestPaymentWithApptRef: (NSString *) appt_ref AndWithPromoCode:(NSString *) coupon{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kPay_For_Booking_URL]];
    NSString * params = [NSString stringWithFormat:@"appointment_reference_number=%@",appt_ref];
    if (coupon.length>0) {
      params = [params stringByAppendingString:[NSString stringWithFormat:@"&coupon_name=%@",coupon]];
    }
    
    [self makeBookingRequestWithURL:url WithParameters:params AndWithRequestType:RequestTypePhorestPayment];
    
    //uncommented code below to use the method above
    /*
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
       
        
        if (!error && dataDict!=nil) {
            
           
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
            });
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    
                    
                    
                    if ([self.myDelegate respondsToSelector:@selector(phorestBookingResult:WithMessage:)]) {
                        
                        NSString * success = dataDict[kSuccessIndex];
                        NSString * message = dataDict[kMessageIndex];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.myDelegate phorestBookingResult:[success boolValue] WithMessage:message];
                        });
                        
                        
                    }
                    break;
                    
                default:
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        [WSCore showServerErrorAlert];
                    });
                    break;
            }
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoadingOnView:self.view];
                if (error.code != NSURLErrorCancelled) {
                    
                   
                    
                    [WSCore showServerErrorAlert];
                }
                else{
                    
                    NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                }
                
            });
        }
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        [WSCore showNetworkLoadingOnView:self.view];
        [self.dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
     */
}
@end
