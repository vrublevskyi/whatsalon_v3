//
//  BiographyTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 13/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "BiographyTableViewCell.h"

@implementation BiographyTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.bioTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.contentView.frame.size.width-10, 21)];
        self.bioTitle.text = @"Biography";
        self.bioTitle.font = [UIFont systemFontOfSize:17.0f];
        [self.contentView addSubview:self.bioTitle];
        
        self.descriptionLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.descriptionLabel];
    }
    
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
