//
//  UIView.swift
//  whatsalon
//
//  Created by admin on 10/10/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import Foundation

extension UIView {
    
    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func toImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
