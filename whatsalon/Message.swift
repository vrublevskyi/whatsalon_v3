//
//  Message.swift
//  whatsalon
//
//  Created by admin on 10/9/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import ObjectMapper

//Change to Salon(without model)
class Message: Mappable {
    
    var messageId:          Int?
    var userImageURLString: String?
    var salonName:          String?
    var status:             Bool?
    var time:               String?

    //delete
    var image:              UIImage?
    
    init(id:Int, url:String, name:String, image: UIImage, time: String, status:Bool) {
        self.messageId = id
        self.userImageURLString = url
        self.salonName = name
        self.status = status
        self.time = time
        self.image = image
    }
    
    required init(map: Map) {
        self.mapping(map: map)
    }
    

    func mapping(map: Map) { }
}

extension Message {
    
    static func mocked() -> [Message] {
        return [
            Message(id: 1, url: "", name: "Fringe", image: UIImage(named: "salonMessIcon")!, time:"12:00", status: true),
            Message(id: 2, url: "", name: "Fringe", image: UIImage(named: "salonMessIcon")!, time:"14:30", status: true),
            Message(id: 3, url: "", name: "Yesterday", image: UIImage(named: "whatSalonMessIcon")!, time:"15:45", status: false),
            Message(id: 4, url: "", name: "Salon Name",  image: UIImage(named: "salonMessIcon")!, time:"October 12", status: false),
            Message(id: 5, url: "", name: "Road", image: UIImage(named: "salonMessIcon")!, time:"23:10",status: false)

        ]
    }
}
