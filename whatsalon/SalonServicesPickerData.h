//
//  SalonServicesPickerData.h
//  whatsalon
//
//  Created by Graham Connolly on 16/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ServicesPickerDelegate <NSObject>

-(void)didSelectService:(NSString *) service;

@end

@interface SalonServicesPickerData : NSObject <UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic) NSMutableArray * arrayOfServices;
@property (nonatomic,assign) id<ServicesPickerDelegate> myDelegate;

-(void)loadServices;
@end
