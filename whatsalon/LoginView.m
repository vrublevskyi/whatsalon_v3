//
//  LoginView.m
//  whatsalon
//
//  Created by Graham Connolly on 07/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "LoginView.h"


@implementation LoginView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        //1. load .xib
        [[NSBundle mainBundle] loadNibNamed:@"LoginView" owner:self options:nil];
        
        //2 adjust bounds
        self.bounds = self.view.bounds;
        self.loginButton.backgroundColor=[UIColor clearColor];
        self.loginButton.buttonColor = kWhatSalonBlue;
        self.loginButton.shadowColor = kWhatSalonBlueShadow;
        self.loginButton.shadowHeight = 1.0f;
        self.loginButton.cornerRadius = 3.0f;
        
        self.descriptionLabel.textColor = [UIColor whiteColor];
                //3 add as subview
        [self addSubview:self.view];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //load the interface file from .xib
        [[NSBundle mainBundle] loadNibNamed:@"LoginView" owner:self options:nil];
        
        //Add as a subview
        [self addSubview:self.view];
    }
    return self;
}

@end
