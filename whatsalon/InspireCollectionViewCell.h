//
//  InspireCollectionViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 16/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InspireCollectionViewCell : UICollectionViewCell <UIGestureRecognizerDelegate>
@property (nonatomic) UIImageView * bgImageView;
@property (nonatomic) UIImageView * overlayImageView;
@property (nonatomic) UIImageView * favImageView;
@property (nonatomic) UILabel * infoLabel;
@property (nonatomic) UILabel * book;
@property (nonatomic,assign) BOOL isBookable;
@property (nonatomic) NSString * styleID;
@property (nonatomic,assign) BOOL isFav;
@property (nonatomic,assign) BOOL isFromFavScreen;


-(void)setCellImageWithURL: (NSString*) url;
@end
