//
//  CreditCardDetailsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 01/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "CreditCardDetailsViewController.h"
#import "UITextField+PaddingText.h"
#import "User.h"
#import "PaddedLabel.h"
#import "NSString+Validations.h"
#import "UIView+AlertCompatibility.h"
#import "GCNetworkManager.h"
#import "FUIButton.h"
#import "whatsalon-Swift.h"

@interface CreditCardDetailsViewController ()<UIPickerViewDataSource,UIPickerViewDelegate, UITextFieldDelegate,GCNetworkManagerDelegate>

/*! @brief represents the Credit Card text field. */
@property (weak, nonatomic) IBOutlet UITextField *ccTextField;

/*! @brief represents the first name text field. */
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;

/*! @brief represents the last name text field. */
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;

/*! @brief represents the card type label. */
@property (weak, nonatomic) IBOutlet UILabel *cardTypeLabel;

/*! @brief represents the CVV text field. */
@property (weak, nonatomic) IBOutlet UITextField *cvvTextField;

/*! @brief represents the view for hosting the card information. */
@property (weak, nonatomic) IBOutlet UIView *viewForCard;

/*! @brief represents the array of card types.*/
@property (nonatomic) NSArray * cardTypes;

/*! @brief represents the picker view for choosing the card. */
@property (nonatomic) UIPickerView * cardPicker;

/*! @brief represents the view for holding the picker view. */
@property (nonatomic,weak) UIView * cardTypeView;

/*! @brief represents the button for removing the card. */
@property (weak, nonatomic) IBOutlet FUIButton *removeCardButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

/*! @brief determines if the card is picked or not. */
@property (nonatomic,assign) BOOL isCardPicked;


/*! @brief represents the save action for saving the card details. */
- (IBAction)save:(id)sender;

/*! @brief represents the scan card button. */
@property (strong, nonatomic) IBOutlet UIButton *scanCard;

/*! @brief represents the scan card action. */
- (IBAction)scanCard:(id)sender;

/*! @brief represents the network manager for making an add card network request. */
@property (nonatomic) GCNetworkManager * addCardNetworkManager;

/*! @brief represents the network manager for making a remove card network request. */
@property (nonatomic) GCNetworkManager * removeCardNetworkManager;

/*! @brief represents the view for hiding other UIElements. */
@property (nonatomic) UIView * blackView;

/*! @brief determines if the back space/slash button is pressed.*/
@property (nonatomic,assign) BOOL isBackSapcePressed;

/*! @brief represents the month label. */
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;

/*! @brief determines if the expiry picker is showing. */
@property (nonatomic) BOOL isExpiryPicker;

/*! @brief represents the NSMutableArray for years. */
@property (nonatomic) NSMutableArray *years;

/*! @brief represents the NSMutableArray for months. */
@property (nonatomic) NSMutableArray *months;

/*! @brief represents the view for holding expiry. */
@property (nonatomic) UIView * expiryView;

/*! @brief represents the selected row for month. */
@property (nonatomic) NSInteger monthRow;

/*! @brief represents the selected row for year. */
@property (nonatomic) NSInteger yearRow;

/*! @brief represents the lock image view. */
@property (weak, nonatomic) IBOutlet UIImageView *lockImage;

@end

@implementation CreditCardDetailsViewController

-(void)addRightBarAndScanButtons{
//    UIBarButtonItem * cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleBordered target:self action:@selector(save:)];
//    self.navigationItem.rightBarButtonItem=cancelButton;
//    self.scanCard.hidden=YES;//had to disable cardio as it did not support iOS9 bitcode
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.addCardNetworkManager = [[GCNetworkManager alloc] init];
    self.addCardNetworkManager.delegate =self;
    self.addCardNetworkManager.parentView = self.view;
    
    self.removeCardNetworkManager = [[GCNetworkManager alloc] init];
    self.removeCardNetworkManager.delegate =self;
    self.removeCardNetworkManager.parentView = self.view;
    
    self.removeCardButton.hidden=YES;
//    self.scanCard.hidden=YES;
    
    self.monthLabel.userInteractionEnabled=YES;
    self.monthLabel.textColor = [UIColor lightGrayColor];

    if ([[User getInstance] hasCreditCard]) {
       
        NSLog(@"Has credit card");
        self.ccTextField.text = [[User getInstance] fetchCreditCardNo];
        self.firstNameTextField.text = [[User getInstance] fetchCreditCardFirstName];
        self.lastNameTextField.text = [[User getInstance] fetchCreditCardLastName];
        
        if ([[User getInstance] fetchCreditCardExp].length==4) {
            NSMutableString *mu = [NSMutableString stringWithString:[[User getInstance] fetchCreditCardExp]];
            [mu insertString:@"/" atIndex:2];
            [mu insertString:@"20" atIndex:3];
            self.monthLabel.text = [NSString stringWithFormat:@"  %@",mu];
        }else{
           self.monthLabel.text = [NSString stringWithFormat:@"  %@",[[User getInstance] fetchCreditCardExp]];
            
        }
        self.monthLabel.textColor=[UIColor blackColor];
        self.cardTypeLabel.text = [NSString stringWithFormat:@"  %@",[[User getInstance] fetchCreditCardType]];
        self.cvvTextField.text=@"***";
        self.cardTypeLabel.textColor = [UIColor blackColor];
        self.removeCardButton.hidden=NO;
//        self.scanCard.hidden=YES;
        self.navigationItem.rightBarButtonItem=nil;
       
        
    }
    else{
        [self addRightBarAndScanButtons];
        
    }
   
    [WSCore addBottomLine:self.ccTextField :kCellLinesColour];
    [WSCore addRightLineToView:self.firstNameTextField withWidth:0.5];
    [WSCore addBottomLine:self.firstNameTextField :kCellLinesColour];
    [WSCore addBottomLine:self.lastNameTextField :kCellLinesColour];
    [WSCore addBottomLine:self.cvvTextField :kCellLinesColour];
    [WSCore addBottomLine:self.cardTypeLabel :kCellLinesColour];
    
    [self.ccTextField setLeftPadding:10];
    [self.firstNameTextField setLeftPadding:10];
    [self.lastNameTextField setLeftPadding:10];
    [self.cvvTextField setLeftPadding:10];
    
    
    self.cardTypes =[[NSArray alloc] initWithArray:[WSCore getSupportedCardsArray]];
    self.cardPicker.delegate=self;
    self.cardPicker.dataSource=self;
    
    self.cardTypeLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cardType:)];
    tap.numberOfTapsRequired=1;
    [self.cardTypeLabel addGestureRecognizer:tap];

    
    /*
    self.removeCardButton.layer.cornerRadius=3.0;
    self.removeCardButton.layer.masksToBounds=YES;
    self.removeCardButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.removeCardButton.layer.borderWidth=1.0;
    */
    self.removeCardButton.buttonColor = kWhatSalonBlue;
    self.removeCardButton.shadowColor = kWhatSalonBlueShadow;
    self.removeCardButton.shadowHeight = kShadowHeight;
    self.removeCardButton.cornerRadius = kCornerRadius;
    [self.removeCardButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.removeCardButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];
    
    //textfields
    self.ccTextField.delegate=self;
    self.ccTextField.tag=0;
    self.ccTextField.returnKeyType=UIReturnKeyNext;
    self.firstNameTextField.delegate=self;
    self.firstNameTextField.tag=1;
    self.firstNameTextField.returnKeyType=UIReturnKeyNext;
    self.lastNameTextField.delegate=self;
    self.lastNameTextField.tag=2;
    self.lastNameTextField.returnKeyType=UIReturnKeyNext;
    self.cvvTextField.delegate=self;
    self.cvvTextField.tag=4;
    self.cvvTextField.returnKeyType=UIReturnKeyDone;
    
    self.isCardPicked=NO;
   

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleDidBecomeActive)
                                                 name: UIApplicationDidBecomeActiveNotification
                                               object: nil];
    

    self.ccTextField.keyboardType=UIKeyboardTypePhonePad;
    self.firstNameTextField.keyboardType=UIKeyboardTypeAlphabet;
    self.firstNameTextField.autocapitalizationType=UITextAutocapitalizationTypeSentences;
    self.lastNameTextField.autocapitalizationType=UITextAutocapitalizationTypeSentences;
    self.lastNameTextField.autocorrectionType=UITextAutocorrectionTypeNo;
    self.firstNameTextField.autocorrectionType=UITextAutocorrectionTypeNo;
    self.firstNameTextField.keyboardType=UIKeyboardTypeAlphabet;
    self.cvvTextField.keyboardType=UIKeyboardTypePhonePad;
    
    self.scanCard.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.scanCard.layer.borderWidth = 2;

    
    UITapGestureRecognizer * selectMonthGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectMonth)];
    selectMonthGesture.numberOfTapsRequired=1;
    [self.monthLabel addGestureRecognizer:selectMonthGesture];
    
    self.years = [[NSMutableArray alloc] initWithArray:[WSCore getYearsArrayWithNumberOfYears:5]];
    self.months =[[NSMutableArray alloc] initWithArray:[WSCore getMonthsArray]];
    self.monthLabel.backgroundColor=[UIColor whiteColor];
    
    self.yearRow=0;
    self.monthRow=0;
    
    [WSCore addBottomLine:self.monthLabel :kCellLinesColour];
    
    if (IS_iPHONE4) {
        self.lockImage.hidden=YES;
    }
    [Gradients addPinkToPurpleHorizontalLayerWithView:_saveButton];

}

-(void)selectMonth{

    self.isExpiryPicker=YES;

    [self.view endEditing:YES];
    
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-260, self.view.frame.size.width, 260)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(254, 0, 66, 44)];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneExpiryView) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 66, 44)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelExpiryView) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancelButton];
    
    self.expiryView = view;
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 160)];
    picker.delegate=self;
    picker.dataSource=self;
    [picker selectRow:self.monthRow inComponent:0 animated:NO];
    [picker selectRow:self.yearRow inComponent:1 animated:NO];
    self.cardPicker =picker;
    [self.expiryView addSubview:self.cardPicker];
    
    [self.view addSubview:self.expiryView];
    

    
    self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.navigationController.navigationBar.frame), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-CGRectGetHeight(self.expiryView.bounds) - CGRectGetHeight(self.navigationController.navigationBar.frame))];
    self.blackView.backgroundColor=[UIColor lightGrayColor];
    self.blackView.alpha=0.7;
    [self.view addSubview:self.blackView];

}

-(void)doneExpiryView{
    
    
    self.monthLabel.text = [NSString stringWithFormat:@"  %@/%@",[self.months objectAtIndex:self.monthRow],[self.years objectAtIndex:self.yearRow]];
    self.monthLabel.textColor = [UIColor blackColor];
    
    self.isExpiryPicker=NO;
    self.blackView.hidden=YES;
    [self.blackView removeFromSuperview];
    self.blackView=nil;
    self.expiryView.hidden=YES;
    [self.expiryView removeFromSuperview];
    [self.cardPicker removeFromSuperview];
    self.expiryView=nil;
    
    
}

-(void)cancelExpiryView{
    
    self.isExpiryPicker=NO;
    self.expiryView.hidden=YES;
    [self.expiryView removeFromSuperview];
    [self.cardPicker removeFromSuperview];
    
    
    self.blackView.hidden=YES;
    [self.blackView removeFromSuperview];
    self.blackView=nil;
    self.expiryView=nil;
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
    if (theTextField.text.length==2 && !self.isBackSapcePressed) {
        theTextField.text = [theTextField.text stringByAppendingString:@"/"];
    }
    
    NSString *string = theTextField.text;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"/" options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:string options:0 range:NSMakeRange(0, [string length])];
    NSLog(@"Found %lu",(unsigned long)numberOfMatches);
    if (numberOfMatches>1) {
        NSRange lastComma = [theTextField.text rangeOfString:@"/" options:NSBackwardsSearch];
        
        if(lastComma.location != NSNotFound) {
            theTextField.text = [theTextField.text stringByReplacingCharactersInRange:lastComma
                                               withString: @""];
        }
    }

   
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)handleDidBecomeActive{
    
    
    self.cvvTextField.hidden=NO;
    self.ccTextField.hidden=NO;;
    self.monthLabel.hidden=NO;
}

-(void)handleEnteredBackground{
    
    
    self.cvvTextField.hidden=YES;
    self.ccTextField.hidden=YES;
    self.monthLabel.hidden=YES;
}
-(void)handleSingleTap:(UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
}
    
#pragma mark - UITextfield delegate methods

-(void)textFieldDidEndEditing:(UITextField *)textField{
        
        [self resignFirstResponder];
        
        
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    UIView *view = [self.view viewWithTag:textField.tag + 1];
    if (!view)
        [textField resignFirstResponder];
    else
        [view becomeFirstResponder];
    return YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // [CardIOUtilities preload];
}

- (IBAction)cardType:(id)sender {
    
    [self.view endEditing:YES];
    
    self.isCardPicked=NO;
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-260, self.view.frame.size.width, 260)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(254, 0, 66, 44)];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneCardType) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 66, 44)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelCardType) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancelButton];
    
    self.cardTypeView = view;
    UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, 50, 320, 180)];
    picker.delegate=self;
    picker.dataSource=self;
    self.cardPicker =picker;
    [self.cardTypeView addSubview:self.cardPicker];
    
    [self.view addSubview:self.cardTypeView];
    
    self.cardTypeLabel.text = [NSString stringWithFormat:@"   %@",[self.cardTypes objectAtIndex:0]];
    self.cardTypeLabel.textColor = [UIColor blackColor];
    
    self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.navigationController.navigationBar.frame), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-CGRectGetHeight(self.cardTypeView.bounds) - CGRectGetHeight(self.navigationController.navigationBar.frame))];
    self.blackView.backgroundColor=[UIColor lightGrayColor];
    self.blackView.alpha=0.7;
    [self.view addSubview:self.blackView];
}

-(void)doneCardType{
    
    self.blackView.hidden=YES;
    [self.blackView removeFromSuperview];
    self.blackView=nil;
    self.isCardPicked=YES;
    self.cardTypeView.hidden=YES;
    [self.cardTypeView removeFromSuperview];
    [self.cardPicker removeFromSuperview];
    
    
}

-(void)cancelCardType{
    
    self.isCardPicked=NO;
    self.cardTypeView.hidden=YES;
    [self.cardTypeView removeFromSuperview];
    [self.cardPicker removeFromSuperview];
    
    self.cardTypeLabel.text = @"Card Type";
    self.cardTypeLabel.textColor = [UIColor lightGrayColor];
    
    self.blackView.hidden=YES;
    [self.blackView removeFromSuperview];
    self.blackView=nil;
}


#pragma mark - UIPickerView Datasource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    if (self.isExpiryPicker) {
       
        return 2;
    }
   
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (self.isExpiryPicker) {
        
        if (component == 0) {
            return self.months.count;
        }
        else {
            return self.years.count;
        }
    }
    
    return self.cardTypes.count;
}
#pragma mark - UIPickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (self.isExpiryPicker) {
       
        if (component == 0) {
            return [self.months objectAtIndex:row];
        }
        else {
          
            return  [NSString stringWithFormat:@"%@",[self.years objectAtIndex:row]];
        }
    }
   
    return [self.cardTypes objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (self.isExpiryPicker) {
        
        self.yearRow = [pickerView selectedRowInComponent:1];
        self.monthRow= [pickerView selectedRowInComponent:0];
        
    }
    else{
        self.cardTypeLabel.text=[NSString stringWithFormat:@"   %@",[self.cardTypes objectAtIndex:row]];
        self.cardTypeLabel.textColor = [UIColor blackColor];

    }
}

- (IBAction)removeCard:(id)sender {
    
    
    [self.view endEditing:YES];
    if ([[User getInstance] fetchCreditCardPaymentRef] !=nil){
        NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kPayment_delete_card_URL]];
    NSString * params = [NSString stringWithFormat:@"payment_ref=%@",[[User getInstance] fetchCreditCardPaymentRef]];
        
        [self.removeCardNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
        
    }
    
    self.monthLabel.textColor = [UIColor lightGrayColor];
     
}
- (IBAction)masterCardTapper:(id)sender {
    self.cardTypeLabel.text =@"MasterCard";

}
- (IBAction)visaTapped:(id)sender {
    self.cardTypeLabel.text =@"VISA";

}

- (IBAction)save:(id)sender {
   
    NSString*exp =[self.monthLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"exp %@",exp);
    [self.view endEditing:YES];
    if (self.ccTextField.text.length==0 || self.firstNameTextField.text.length==0 || self.lastNameTextField.text.length==0 || self.isCardPicked==NO) {
        
        [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please fill out the form." cancelButtonTitle:@"OK"];
       
        
        return;
    }
    
    else if(![NSString isNumber:self.ccTextField.text]){
        
        [UIView showSimpleAlertWithTitle:@"Card must be numeric" message:@"" cancelButtonTitle:@"OK"];
        
        return;
    }
    else if(![NSString isNumber:self.cvvTextField.text]){
        
        [UIView showSimpleAlertWithTitle:@"CVV must be numeric" message:@"" cancelButtonTitle:@"OK"];
       
        return;
    }
    else if(![NSString validateStringIsExpiryDate:exp ]){
        [UIView showSimpleAlertWithTitle:@"Incorrect Format" message:@"The expiry date must follow MM/YYYY" cancelButtonTitle:@"OK"];
                return;
    }
    else if(![NSString isValidFirstName:self.firstNameTextField.text]){
        
        [UIView showSimpleAlertWithTitle:@"First Name Error" message:@"Your first name must be composed of standard english letters. No special characters."cancelButtonTitle:@"OK"];
       
        return;
    }
    else if(![NSString isValidSurname:self.lastNameTextField.text]){
        
        [UIView showSimpleAlertWithTitle:@"Last Name Error" message:@"Your last name must be composed of standard english letters. No special characters" cancelButtonTitle:@"OK"];
        
        
        return;
    }

    [self udpateCreditCardDetails];
    
}

-(void)udpateCreditCardDetails{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kPayment_add_card_URL]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&carddetails=%@",[self carddetailsJSON]]];
    [self.addCardNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}

-(NSString *)carddetailsJSON{
    NSString * cardString = self.cardTypeLabel.text;
    NSString *newCardString = [cardString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Card type %@",newCardString);
    NSString * cardNumber =[self.ccTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    cardNumber = [self.ccTextField.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSLog(@"MM %@",self.monthLabel.text);
    NSString *exp = [self.monthLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString * str = [NSString stringWithFormat:@"[{\"cardnumber\":\"%@\"},{\"cardname\":\"%@ %@\"},{\"cardtype\":\"%@\"},{\"expdate\":\"%@\"},{\"cvn\":\"%@\"}]",cardNumber,self.firstNameTextField.text,self.lastNameTextField.text,newCardString,exp,self.cvvTextField.text];
    
  
     
    
    
    
    return str;
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        if (buttonIndex== [alertView cancelButtonIndex]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if(alertView.tag==2){
        if (buttonIndex==[alertView cancelButtonIndex]) {
            self.ccTextField.text=@"";
            self.cvvTextField.text=@"";
            self.firstNameTextField.text=@"";
            self.lastNameTextField.text=@"";
            self.monthLabel.text = @"  MM/YYYY";
            self.cardTypeLabel.text = @"   Card Type";
            self.cardTypeLabel.textColor=kCellLinesColour;
            self.removeCardButton.hidden=YES;
            [self addRightBarAndScanButtons];
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(didAddCard)]) {
        [self.delegate didAddCard];
    }
}

- (IBAction)scanCard:(id)sender {
/*
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    [self presentViewController:scanViewController animated:YES completion:nil];
 */
}
/*
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController {

    // Handle user cancellation here...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    // The full card number is available as info.cardNumber, but don't log that!
   
    self.ccTextField.text = @"";
    self.ccTextField.text = info.cardNumber;
    
    if (self.cvvTextField.text ==nil && info.cvv !=nil)
        self.cvvTextField.text = info.cvv;
    
    if (info.expiryMonth !=0 && info.expiryYear !=0) {
       
        self.monthLabel.text = [NSString stringWithFormat:@"  %02lu/%lu",(unsigned long)info.expiryMonth,(unsigned long)info.expiryYear];
        self.monthLabel.textColor=[UIColor blackColor];
    }
    
    if (info.cvv.length !=0) {
        self.cvvTextField.text = info.cvv;
    }
    
    for (NSString * n in self.cardTypes) {
        
        if ([n.lowercaseString isEqualToString:[CardIOCreditCardInfo displayStringForCardType:info.cardType usingLanguageOrLocale: [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2]].lowercaseString]) {
            
            
            
            self.cardTypeLabel.text = [NSString stringWithFormat:@"   %@",n];
            self.cardTypeLabel.textColor = [UIColor blackColor];
            self.isCardPicked=YES;
        }
    }
    // Use the card info...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}
*/
#pragma mark - GCNetworkDelegate
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    if (manager==self.addCardNetworkManager) {
        NSLog(@"Add card %@",jsonDictionary);
        [[User getInstance] updateCreditCardDetailsWithFirstName:jsonDictionary[@"message"][@"card_first_name"] withLastName:jsonDictionary[@"message"][@"card_last_name"] withCardNumber:jsonDictionary[@"message"][@"card_number"] withCardType:jsonDictionary[@"message"][@"card_type"] withExp:jsonDictionary[@"message"][@"expiry"] withPaymentRef:jsonDictionary[@"message"][@"payment_ref"] ];
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Card Added" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        alert.tag=1;
    }else if(manager==self.removeCardNetworkManager){
        
        [[User getInstance] removeCreditCardInfo];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Card Removed" message:jsonDictionary[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        alert.tag=2;
    }
    
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    if (manager==self.addCardNetworkManager) {
        [UIView showSimpleAlertWithTitle:@"Oops" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
    }else if(manager==self.removeCardNetworkManager){
        [WSCore showServerErrorAlert];

    }
}

@end
