//
//  MyBookings.m
//  whatsalon
//
//  Created by Graham Connolly on 25/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "MyBookings.h"
#import <objc/runtime.h>

@implementation MyBookings


-(instancetype) initWithBookingID:(NSString *)book_id{
    self = [super init];
    if (self) {
        self.bookingId=book_id;
        self.bookingSalon =[[Salon alloc] initWithSalonID:@""];
    }
    
    return self;
}

+(instancetype)bookingWithID:(NSString *)book_id{
    return [[self alloc] initWithBookingID:book_id];
}

- (NSDictionary *)dictionaryRepresentation {
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
            [dictionary setObject:value forKey:key];
    }
    
    free(properties);
    
    return dictionary;
}
@end
