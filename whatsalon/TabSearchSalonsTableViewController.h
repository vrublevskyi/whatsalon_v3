//
//  TabSearchSalonsTableViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 28/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header TabSearchSalonsTableViewController.h
  
 @brief This is the header file for TabSearchSalonsTableViewController.h
  
 @remark - It uses Google Places API. Ensure that it is kept up to date.
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    -
 */
#import <UIKit/UIKit.h>
#import "GCNetworkManager.h"
#import "LocationObject.h"
#import "GCGoogleAutoComplete.h"//added
#import "GCGoogleAutoCompleteObject.h"
#import "INTULocationManager.h"
/*!
     @protocol SearchDelegate
  
     @brief The SearchDelegate protocol
  
     It's a protocol used to notify the delegate that a change has occurred. 
 */
@protocol SearchDelegate <NSObject>

@optional

/*! @brief this action is called when the salons table view is dismissed.
 
 */
-(void)didDismissSearchSalonsTable;

@end

@interface TabSearchSalonsTableViewController : UIViewController

/*! @brief represents the SearchDelegate object. */
@property (nonatomic,assign) id<SearchDelegate> delegate;

/*! @brief represents the UISearchBar. */
@property (weak, nonatomic) IBOutlet UISearchBar *salonsSearchBar;

/*!
  @brief searchBarHolder UIView for holding searchbar
 */
@property (weak, nonatomic) IBOutlet UIView *searchBarHolder;

/*!
 @brief searchSuggestionsTableView contains a list of suggestions based on the search term beign typed in the search bar
 */
@property (weak, nonatomic) IBOutlet UITableView *searchSuggestionsTableView;

/*! @brief represents the search term. */
@property (nonatomic) NSString * searchTerm;

/*! @brief determines whether the search button was pressed in the previous view controller. */
@property (nonatomic) BOOL wasSearchPressed;//whether the search button has been pressed in the previous UIViewController
@property (nonatomic) NSMutableArray * filteredSalons;

/*! @brief represents the category string. */
@property (nonatomic) NSString * catString;
    @property (nonatomic) GCNetworkManager * networkManager;

/*! @brief represents the category id. */
@property (nonatomic) NSString * catID;
- (void)searchForSalon:(NSString *)name location:(CLLocation*) loc ;
- (void)handleSalonSearchSuccessResult:(NSDictionary *)jsonDictionary;
@end
