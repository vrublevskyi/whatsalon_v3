//
//  GCImagePresentTransition.m
//  transition
//
//  Created by Graham Connolly on 18/02/2015.
//  Copyright (c) graham connolly All rights reserved.
//

#import "GCImagePresentTransition.h"
#import "UIImageViewModeScaleAspect.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation GCImagePresentTransition

-(instancetype)initWithFrame: (CGRect) frame AndImagePath: (NSString *) imagePath{
    self = [super init];
    if (self) {
        self.startImageFrame =frame;
        self.imageUrl=imagePath;
    }
    return self;
}

-(instancetype)initWithFrame: (CGRect) frame AndImage: (UIImage *) image{
    
    self = [super init];
    if (self) {
        self.startImageFrame =frame;
        self.dummyImage=image;
    }
    return self;
}
-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    UIViewController * detail = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    //The view that acts as the superview for the views involved in the transition.
    UIView * containerView = [transitionContext containerView];
    detail.view.frame=containerView.bounds;
    detail.view.alpha=0.0;
    [containerView addSubview:detail.view];
    
    
    UIImageViewModeScaleAspect *myImage = [[UIImageViewModeScaleAspect alloc]initWithFrame:self.startImageFrame];
    myImage.contentMode = UIViewContentModeScaleAspectFill; // Add the first contentMode
    UIImageView * image = [[UIImageView alloc] init];
    if ([WSCore isDummyMode]) {
        NSLog(@"image url %@",self.imageUrl);
        image.image=self.dummyImage;
    }else{
        [image sd_setImageWithURL:[NSURL URLWithString: self.imageUrl ]];

    }
    myImage.image = image.image;
    [myImage initToScaleAspectFitToFrame:containerView.frame];
     [containerView addSubview:myImage];
    
    [UIView animateWithDuration:0.4f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         //
                         // Others Animation
                         //
                         [myImage animaticToScaleAspectFit];
                         //
                         // Others Animation
                         //
                     } completion:^(BOOL finished) {
                         [myImage animateFinishToScaleAspectFit];
                         
                         [UIView animateWithDuration:0.3 animations:^{
                             detail.view.alpha = 1.0;
                         } completion:^(BOOL finished) {
                             
                             [myImage removeFromSuperview];
                             [transitionContext completeTransition:YES];
                         }];
                     }];
   
    
    
}

-(NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    return 0.7f;
}
@end
