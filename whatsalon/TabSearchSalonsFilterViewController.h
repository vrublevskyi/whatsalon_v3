//
//  TabSearchSalonsFilterViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 28/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header TabSearchSalonsFilterViewController.h
  
 @brief This is the header file for TabSearchSalonsFilterViewController.h
  

  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    -
 */
#import <UIKit/UIKit.h>

@interface TabSearchSalonsFilterViewController : UIViewController

/*! @brief represents the tableview. */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/*! @brief represents the view for holding the search bar. */
@property (weak, nonatomic) IBOutlet UIView *searchHolder;

/*! @brief represents the label for holding the search text. */
@property (weak, nonatomic) IBOutlet UILabel *searchText;

/*! @brief represent the search image view. */
@property (weak, nonatomic) IBOutlet UIImageView *searchImage;

/*! @brief represents the view for the navigation bar. */
@property (weak, nonatomic) IBOutlet UIView *navBarView;

@end
