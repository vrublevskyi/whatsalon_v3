//
//  TabFavouriteFilterViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 18/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabFavouriteFilterViewController.h"
#import "TabFilterFavouriteTableViewCell.h"
#import "FavouriteFilterItem.h"
#import "ExperiencesViewController.h"
#import "TabFavouritesViewController.h"
#import "LoginView.h"
#import "TabLoginOrSignUpViewController.h"
#import "User.h"
#import "BrowseSalonTableViewCell.h"

@interface TabFavouriteFilterViewController ()<UITableViewDataSource,UITableViewDelegate,LoginDelegate,GCNetworkManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSMutableArray * favItems;
@property (nonatomic) BOOL loginViewIsShowing;
@property (nonatomic) IBOutlet UIView *loginViewPlaceHolder;

@property (nonatomic) GCNetworkManager * profileManager;

@property (nonatomic) BOOL isFetchingProfile;



@end

@implementation TabFavouriteFilterViewController

/*
 viewDidDisappear
 cancel profileManager if the view disappears
 */
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.profileManager cancelTask];
}


/*
 updateProfile
 if user is logged in
    create GCNetwork request
 
 */
-(void)updateProfile{
    
    
    if ([[User getInstance] isUserLoggedIn]) {
        NSLog(@"Update profile");
        UIViewController *current = self.tabBarController.selectedViewController; NSArray *className = [current childViewControllers];
        NSLog(@"className %@",[className objectAtIndex:0]);
        if (self.profileManager ==nil) {
            self.profileManager = [[GCNetworkManager alloc] init];
            self.profileManager.delegate=self;
            self.profileManager.parentView=self.view;
            self.profileManager.parentViewController=self;
            self.profileManager.tagDescription=@"Favourite Call";
            NSLog(@"Profile manager init");
        }
        
        NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Profile_Status_URL]];
        [self.profileManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        
    }
    
}

/*
 handleFailureMessage
 if profile status returns false then log out the user. this is useful if the secret keys are incorrect, or variables.
 */
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
  
    [[User getInstance] logout];
}

-(void)manager:(GCNetworkManager *)manager handleServerFailureWithError:(NSError *)error{
    //NSLog(@"**********\n\n\nServer failure %@ \n\n\n ******",error.debugDescription);
}

/*
 If the request is successful
    update the users profile
        -update the balance
        -update the currency ibhect
            + conversion rate
            + currency name
            + currency id
            + symbol
        - update card details
        - update whether the user has a card or not
        - update is_banned
        - needs logout
        - update the number of experiences favourites
        - update the number of salon favourites
        - update the users phone number
        - update the key
        - update twilio
        - update the avatar
 
 If needs_logout is true then log out the user
 
 Then reload the tableview - this will update the number of favourites field
 */
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    if (jsonDictionary[@"message"][@"balance"] !=[NSNull null]) {
        [[User getInstance] updateUsersBalance:jsonDictionary[@"message"][@"balance"]];
    }
    
    
    if ([jsonDictionary[@"message"][@"currency_object"] count] !=0) {
        
        CurrencyItem * currency = [[CurrencyItem alloc] init];
        if (jsonDictionary[@"message"][@"currency_object"][@"conversion_rate_vs_euro"] !=[NSNull null]) {
            currency.conversionRate = jsonDictionary[@"message"][@"currency_object"][@"conversion_rate_vs_euro"];
            
        }
        if (jsonDictionary[@"message"][@"currency_object"][@"currency_name"] !=[NSNull null]) {
            currency.currencyName = jsonDictionary[@"message"][@"currency_object"][@"currency_name"];
            
        }
        if (jsonDictionary[@"message"][@"currency_object"][@"id"] !=[NSNull null]) {
            currency.currencyId = jsonDictionary[@"message"][@"currency_object"][@"id"];
            
        }
        if (jsonDictionary[@"message"][@"currency_object"][@"symbol_utf8"] !=[NSNull null]) {
          
            [ currency updatedCurrencySymbol:[NSString stringWithFormat:@"%@", jsonDictionary[@"message"][@"currency_object"][@"symbol_utf8"] ]];
            // currency.symbolUtf8 = [NSString stringWithFormat:@"%@", jsonDictionary[@"message"][@"currency_object"][@"symbol_utf8"] ];
            
            
        }
        
        
        [[User getInstance] updateUsersCurrency:currency];
       
        
    }
    if ([jsonDictionary[@"message"][@"card_details"] count]!=0) {
        if (jsonDictionary[@"message"][@"card_details"][@"card_first_name"]) {
            
            [[User getInstance] updateCreditCardFirstName:jsonDictionary[@"message"][@"card_details"][@"card_first_name"]];
        }
        if (jsonDictionary[@"message"][@"card_details"][@"card_last_name"]!=[NSNull null]) {
            
            [[User getInstance] updateCreditCardLastName:jsonDictionary[@"message"][@"card_details"][@"card_last_name"]];
        }
        if (jsonDictionary[@"message"][@"card_details"][@"card_number"]!=[NSNull null]) {
            
            [[User getInstance] updateCreditCardNo:jsonDictionary[@"message"][@"card_details"][@"card_number"]];
        }
        
        if (jsonDictionary[@"message"][@"card_details"][@"card_type"]!=[NSNull null]) {
            
            [[User getInstance] updateCreditCardType:jsonDictionary[@"message"][@"card_details"][@"card_type"]];
        }
        if (jsonDictionary[@"message"][@"card_details"][@"expiry"]!=[NSNull null]) {
            
            [[User getInstance] updateCreditCardExp:jsonDictionary[@"message"][@"card_details"][@"expiry"]];
        }
        if (jsonDictionary[@"message"][@"card_details"][@"payment_ref"]!=[NSNull null]) {
            
            [[User getInstance] updateCreditCardPaymentRef:[NSString stringWithFormat:@"%@",jsonDictionary[@"message"][@"card_details"][@"payment_ref"]]];
            
        }
    }else if ([[User getInstance] hasCreditCard]){
        [[User getInstance] removeCreditCardInfo];
    }
    
    if (jsonDictionary[@"message"][@"has_card"] !=[NSNull null]) {
        [[User getInstance] updateDoesUserHaveCard:jsonDictionary[@"message"][@"has_card"]
         ];
    }
    
    if (jsonDictionary[@"message"][@"is_banned"] !=[NSNull null]) {
        [[User getInstance] updateIsUserBanned:jsonDictionary[@"message"][@"is_banned"] ];
    }
    
    if (jsonDictionary[@"message"][@"needs_logout"] !=[NSNull null]) {
        [[User getInstance] updateUserNeedsToLogOut:jsonDictionary[@"message"][@"needs_logout"]];
    }
    
    if (jsonDictionary[@"message"][@"no_of_exp_fav"] !=[NSNull null]) {
       
        [[User getInstance] updateNumberOfExperiencesFavourite:jsonDictionary[@"message"][@"no_of_exp_fav"] ];
    }
    
    if (jsonDictionary[@"message"][@"no_of_salons_fav"] !=[NSNull null]) {
         //NSLog(@"No of favourites %@",jsonDictionary[@"message"][@"no_of_exp_fav"] );
        [[User getInstance] updateNumberOfSalonFavourites:jsonDictionary[@"message"][@"no_of_salons_fav"]];
    }
    if (jsonDictionary[@"message"][@"phone_number"] !=[NSNull null]) {
        [[User getInstance] updatePhoneNumber:jsonDictionary[@"message"][@"phone_number"]];
    }
    if (jsonDictionary[@"message"][@"secret_key"]!=[NSNull null]) {
        [[User getInstance] updateKey:jsonDictionary[@"message"][@"secret_key"]];
    }
    if (jsonDictionary[@"message"][@"twilio_verified"] !=[NSNull null]) {
        [[User getInstance] updateTwilioVerified:[jsonDictionary[@"message"][@"twilio_verified"] boolValue]];
    }
    if (jsonDictionary[@"message"][@"user_avatar"] !=[NSNull null]) {
        
        [[User getInstance] setImageURL:jsonDictionary[@"message"][@"user_avatar"]];
    }
    
    //check if user should be logged out
    if ([[User getInstance] needsToLogOut]==YES) {
     
        [[User getInstance] logout];
        
        [self userIsNotLoggedInView];
    }
    
    [self.tableView reloadData];
    
}


/*
 viewDidLayoutSubviews
    updated the loginViewPlaceHolder frame
 
 */
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    CGRect frame = self.loginViewPlaceHolder.frame;
    frame.origin.y =kStatusBarHeight;
    frame.size.height = self.view.frame.size.height-kStatusBarHeight;
    self.loginViewPlaceHolder.frame=frame;
    
    
    self.tableView.frame = frame;
}

/*
 remove the is user not logged in view
    shows tableview
    hides loginViewPlaceHolder
    set loginViewIsShowing to NO
 */
-(void)removeUserIsNotLoggedInView{
   
    
    self.tableView.hidden=NO;
    self.loginViewPlaceHolder.hidden=YES;
    self.loginViewIsShowing=NO;
   
    
    
    
}


/*
 userIsNotLoggedInVIew
 if the user is not logged in then
 show the loginView
 
 add background image to loginViewPlaceHodler
 add loginVIew to loginViewPlaceHolder
 
 */
-(void)userIsNotLoggedInView{
   
    self.loginViewIsShowing=YES;
    self.tableView.hidden=YES;
    self.loginViewPlaceHolder.hidden=NO;
    
    UIImageView * backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.loginViewPlaceHolder.frame.size.width, self.view.frame.size.height)];
    backgroundImage.image = [UIImage imageNamed:kMy_Favourites_placeholder_Image_Name];
    backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    UIView * blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    blackView.backgroundColor = kGoldenTintForOverView;
    [backgroundImage addSubview:blackView];
    [self.loginViewPlaceHolder addSubview:backgroundImage];
    self.loginViewPlaceHolder.layer.masksToBounds=YES;
    
    LoginView * noMessage = [[LoginView alloc] init];
    noMessage.view.backgroundColor = [UIColor clearColor];
    [self.loginViewPlaceHolder addSubview:noMessage];
    //noMessage.center = CGPointMake(CGRectGetWidth(self.loginViewPlaceHolder.bounds)/2, (CGRectGetHeight(self.loginViewPlaceHolder.bounds)/2)+60.0f);
    noMessage.center = backgroundImage.center;
    
    noMessage.titleLabel.text = @"MY FAVOURITES";
    noMessage.titleLabel.font = [UIFont boldSystemFontOfSize:26.0f];
    noMessage.descriptionLabel.text = @"No Favourites yet \n\n Browse all your favourite salon destinations. Heart a Salon and we'll store them here for you.";
    noMessage.descriptionLabel.font = [UIFont systemFontOfSize:16.0f];
    [noMessage.loginButton addTarget:self action:@selector(showLoginView) forControlEvents:UIControlEventTouchUpInside];
    
  
}


-(void)userDidLoginWithFacebook{
    
    NSLog(@"User did sign up with facebook");
    
    
}

/*
 showLoginView
 present TabLoginOrSignUpViewController
 */
-(void)showLoginView{
    //Go to login controller
    
    TabLoginOrSignUpViewController * tab = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignUpVC"];
    tab.delegate=self;
    [WSCore setLoginType:LoginTypeFavourites];
    [self presentViewController:tab animated:YES completion:nil];
}

//////

-(void)notificationSetUp{
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewState) name:kTab_2_Notification object:nil];
}


/*
 viewState
 if the user is not logged in, show userIsNOtLoggedInView
    else
        remove userIsNotLoggedInView
 */
-(void)viewState{
  
    if (![[User getInstance] isUserLoggedIn]) {
        [self userIsNotLoggedInView];
    }
    else{
        
        [self updateProfile];
        [self removeUserIsNotLoggedInView];
    }
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.navigationController.navigationBarHidden=YES;
    self.tabBarController.tabBar.hidden=NO;
    
    
}

/*
 viewDidApepar
    calls viewState
    updates profile
 */
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self viewState];
    [self updateProfile];
}


/*
 viewDidLoad
 
    set up notification
    tableview delegate and datasource
    updates viewState
 
    adds footerview
    
    add FavouriteFilterItems (Salons adn experiences)
 
 */

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self notificationSetUp];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    [self viewState];
    [self.tableView registerClass:[TabFilterFavouriteTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView=footerView;
    self.view.backgroundColor=kBackgroundColor;
    
   
    
    FavouriteFilterItem * salonsFav = [[FavouriteFilterItem alloc] init];
    salonsFav.type=@"Salons";
    salonsFav.number=@"2";
    salonsFav.imageName=@"no_discover_fav.png";
    salonsFav.fav_id = 0;
    
    FavouriteFilterItem * expsFav = [[FavouriteFilterItem alloc]init];
    expsFav.type=@"Experiences";
    expsFav.imageName = @"experience_fav.jpg";
    expsFav.number=@"3";
    expsFav.fav_id = 1;
    
    self.favItems = [NSMutableArray arrayWithObjects:salonsFav,expsFav, nil];
    
    self.view.backgroundColor=kBackgroundColor;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TabFilterFavouriteTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    FavouriteFilterItem * item = [self.favItems objectAtIndex:indexPath.row];
   
    [cell setUpCellWithItem:item];
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.favItems.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 270;
}


/*
 didSelectRowAtIndexPath
    if indexPath.row == 0   
 
    if number of fav salons ==0
        go to tab 0
 
    else
        perform favSalonsVC
 
    else if indexPath.row ==1
        if number of exp salons == 0
            go to tab 0
 
        else
            present experiences
 
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        if ([[[User getInstance] fetchStringNumberOfFavouriteSalons] isEqualToString:@"0"]) {
            self.tabBarController.selectedIndex = 0;
        }
        else{
           [self performSegueWithIdentifier:@"favSalonsVC" sender:self];
        }
        
    }
    else if (indexPath.row==1) {
        
        if ([[[User getInstance] fetchStringNumberOfFavouriteExperiences] isEqualToString:@"0"]) {
            self.tabBarController.selectedIndex = 0;
        }
        else{
            ExperiencesViewController * exp = [self.storyboard instantiateViewControllerWithIdentifier:@"expVC"];
            exp.hidesBottomBarWhenPushed=YES;
            exp.showOnlyFavourites=YES;
            [self.navigationController pushViewController:exp animated:YES];
            self.hidesBottomBarWhenPushed=NO;
        }
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"favSalonsVC"]) {
        TabFavouritesViewController * fav = (TabFavouritesViewController *)segue.destinationViewController;
        fav.isFromFavFilter=YES;
        fav.hidesBottomBarWhenPushed=YES;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 When an unwind is performed
        resetLoginType  
        set statusBar color to black
 */
- (IBAction)unwindToTabFavourites:(UIStoryboardSegue *)unwindSegue
{
    NSLog(@"unwind to tab favourites");
    [WSCore resetLoginType];
    [WSCore statusBarColor:StatusBarColorBlack];
}


@end
