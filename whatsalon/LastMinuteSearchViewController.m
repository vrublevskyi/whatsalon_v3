//
//  LastMinuteSearchViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 19/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "LastMinuteSearchViewController.h"
#import "FUIButton.h"
#import "FUISegmentedControl.h"
#import "BrowseTableViewCell.h"
#import "UITableView+ReloadTransition.h"
#import "LastMinuteSearchViewController.h"
#import "SalonIndividualDetailViewController.h"
#import "GCNetworkManager.h"
#import "UIView+AlertCompatibility.h"
#import "FriendlyService.h"
#import "SelectCategoryViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SelectServicesViewController.h"
#import "User.h"
#import "INTULocationManager/INTULocationManager.h"
#import "LastMinuteItem.h"
#import "SalonReview.h"
#import "OpeningDay.h"
#import "ConfirmViewController.h"
#import "Booking.h"
#import "SalonBookingModel.h"
#import "LastMintuteMapViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SalonReviewListViewController.h"
#import "UILabel+Boldify.h"
#import "LastMinuteLocationViewController.h"
#import "WSCoachMarksView.h"
#import "TabLinkCCViewController.h"



@interface LastMinuteSearchViewController ()<UITableViewDataSource,UITableViewDelegate,UIViewControllerTransitioningDelegate,GCNetworkManagerDelegate,
BrowseDelegate,ServicesDelegate,UIViewControllerTransitioningDelegate,LinkCCDelegate,LastMinuteLocation,WSCoachMarksViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *searchHolder;
@property (weak, nonatomic) IBOutlet UIButton *selectService;
- (IBAction)selectService:(id)sender;
- (IBAction)changeLocation:(id)sender;

@property (weak, nonatomic) IBOutlet FUIButton *searchButton;
- (IBAction)search:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) UIButton * showFilter;
@property (nonatomic) NSMutableArray * lastMinuteSalons;
@property (nonatomic) CGPoint lastContentOffset;

@property (nonatomic) UIView * timesHolder;
@property (nonatomic) CGRect timeFrame;
@property (nonatomic) CGRect tableFrame;

@property (nonatomic) UIButton * allButton;
@property (nonatomic) UIButton * nineToTwelveButton;
@property (nonatomic) UIButton * twelveToThreeButton;
@property (nonatomic) UIButton * threeToSixButton;
@property (nonatomic) UIButton * sixToNineButton;

@property (nonatomic) NSMutableArray * buttonArray;
@property (nonatomic) CGFloat buttonWidth;
@property (nonatomic) UIView * seperatorView;
@property (nonatomic) NSMutableArray * dayButtons;


@property (nonatomic) BOOL isDummy;

@property (nonatomic) GCNetworkManager *networkManager;
@property (nonatomic) GCNetworkManager * lastMinuteManager;
@property (nonatomic) BOOL hasDownloadedServices;
@property (nonatomic) NSMutableArray * friendlyServices;

@property (nonatomic) NSString * serviceIdToSearch;

@property (nonatomic) CLLocation * locationToSearch;

@property (nonatomic) long long chosenDaySeconds;
@property (nonatomic) long long endDaySeconds;

@property (nonatomic) NSMutableArray * nineToTwelveArray;
@property (nonatomic) NSMutableArray * twelveToThreeArray;
@property (nonatomic) NSMutableArray * threeToSixArray;
@property (nonatomic) NSMutableArray * sixToNineArray;
@property (nonatomic) NSMutableArray * numberOfSectionsArray;
@property (nonatomic) NSMutableArray * sectionTitlesArray;

@property (nonatomic)  BOOL timeButtonsSetUp;

@property (nonatomic)  UIView * black;

@property (nonatomic) NSString * chosenDay;

@property (nonatomic) NSDate *chosenDate;
@property (nonatomic) NSDate *todayDate;
@property (nonatomic) NSDate *tomorrowDate;

@property (nonatomic) BOOL isUnwinded;

@property (nonatomic) Booking * bookingObj;
@property (nonatomic) LastMinuteItem *selectedLastMin;
//booking model
@property (nonatomic) SalonBookingModel * salonBookingModel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionText;

@property (nonatomic) WSCoachMarksView *coachMarksView;
@property (nonatomic) UIView * loadingView;
@property (nonatomic) UILabel * askingSalons;
@property (nonatomic) UILabel * checkingForTimes;
@property (nonatomic) UILabel * done;

@end

@implementation LastMinuteSearchViewController


-(void)showLoadingScreen{
    self.loadingView = [[UIView alloc] initWithFrame:self.navigationController.view.frame];
    self.loadingView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.8];
    //self.loadingView.alpha=0.6;
    [self.view addSubview:self.loadingView];
    
    self.rightBarButton.enabled=NO;
    self.navigationItem.hidesBackButton=YES;
    self.checkingForTimes = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, 30)];
    self.checkingForTimes.font = [UIFont boldSystemFontOfSize:18.0f];
    self.checkingForTimes.text = @"Checking for Times";
    self.checkingForTimes.textAlignment=NSTextAlignmentCenter;
    self.checkingForTimes.text = self.checkingForTimes.text.uppercaseString;
    [self.loadingView addSubview:self.checkingForTimes];
    self.checkingForTimes.textColor=[UIColor carrotColor];
    self.checkingForTimes.center = self.loadingView.center;
    
    self.askingSalons =[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, 30)];
    self.askingSalons.font = [UIFont boldSystemFontOfSize:18.0f];
    self.askingSalons.text = @"Asking Salons";
    self.askingSalons.textAlignment=NSTextAlignmentCenter;
    self.askingSalons.text = self.askingSalons.text.uppercaseString;
    [self.loadingView addSubview:self.askingSalons];
    self.askingSalons.textColor=[UIColor alizarinColor];
    self.askingSalons.center = CGPointMake(self.view.center.x, self.view.center.y-25);
    
    
    self.done =[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, 30)];
    self.done.font = [UIFont boldSystemFontOfSize:18.0f];
    self.done.text = @"nearly there";
    self.done.textAlignment=NSTextAlignmentCenter;
    self.done.text = self.done.text.uppercaseString;
    [self.loadingView addSubview:self.done];
    self.done.textColor=[UIColor emerlandColor];
    self.done.center = CGPointMake(self.view.center.x, self.view.center.y+25);
    
    self.askingSalons.alpha=0;
    self.checkingForTimes.alpha=0;
    self.done.alpha=0;
    
    [UIView animateWithDuration:0.8 animations:^{
        self.askingSalons.alpha=1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.8 delay:1.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.checkingForTimes.alpha=1.0;
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.8 delay:0.7 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.done.alpha=1.0;
            } completion:^(BOOL finished) {
                
            }];

        }];
    }];
    
    UILabel * descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-40, 70)];
    descriptionLabel.text=@"Requesting last minute appointments may take a few minutes. \n Please wait.";
    descriptionLabel.textAlignment=NSTextAlignmentCenter;
    [self.loadingView addSubview:descriptionLabel];
    descriptionLabel.center = CGPointMake(self.loadingView.center.x, self.loadingView.frame.size.height-80);
    descriptionLabel.numberOfLines=0;
    descriptionLabel.font = [UIFont systemFontOfSize:14.0f];
    descriptionLabel.textColor = [UIColor silverColor];
    
}

-(void)dismissLoadingScreen{
    self.loadingView.hidden=YES;
    [self.loadingView removeFromSuperview];
    
    self.rightBarButton.enabled=YES;
    self.navigationItem.hidesBackButton=NO;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.networkManager cancelTask];
    [self.lastMinuteManager cancelTask];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    //adjusts the frame of the tableView
    CGRect frame = CGRectMake(0, kNavBarAndStatusBarHeight+self.showFilter.frame.size.height+5+self.timesHolder.frame.size.height+10, self.tableView.frame.size.width, self.view.frame.size.height-kNavBarAndStatusBarHeight-self.showFilter.frame.size.height);
    self.tableFrame=frame;
    self.tableView.frame=frame;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    //returns the Nav bar to the default style when the view is dismissed
    [WSCore setDefaultNavBar:self];
    [self.coachMarksView stop];
   // [self.coachMarksView skipCoach];
}

- (void)tableViewSetUp {
    
    //tableview set up
    self.tableView.backgroundColor=[UIColor clearColor];
    self.tableView.backgroundView=nil;
    self.tableView.delegate = self;
    self.tableView.dataSource=self;
    [self.tableView registerClass:[BrowseTableViewCell class] forCellReuseIdentifier:@"Cell"];
}

- (void)searchHolderButtonSetUp {
    
   
    
    //search button set up
    self.searchButton.buttonColor = kWhatSalonBlue;
    self.searchButton.shadowColor = kWhatSalonBlueShadow;
    self.searchButton.shadowHeight = 1.5f;
    self.searchButton.cornerRadius = 3.0f;
    [self.searchButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal
     ];
    
    //select service set up
    self.selectService.layer.cornerRadius=kCornerRadius;
    self.selectService.layer.borderWidth=1.0;
    self.selectService.layer.borderColor=kWhatSalonBlue.CGColor;
    [self.selectService setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    
    //search button
    self.searchButton.buttonColor = kWhatSalonBlue;
    self.searchButton.shadowColor = kWhatSalonBlueShadow;
    self.searchButton.shadowHeight = 1.5f;
    self.searchButton.cornerRadius = 3.0f;
    [self.searchButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal
     ];
    

}

//the set up and styling of the 'Show Filter' button
- (void)filterButtonStylingAndSetUp {
    
    self.showFilter = [[UIButton alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 64)];
    [self.showFilter setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    [self.showFilter setTitle:@"Show Filter" forState:UIControlStateNormal];
    self.showFilter.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    self.showFilter.backgroundColor=[UIColor colorWithWhite:1.0 alpha:1.0];
    
    UIImageView * iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"expand_arrow"]];
    CGRect frame = CGRectMake(0, 0, 15, 15);
    iv.frame =frame;
    iv.image = [iv.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    iv.tintColor=kWhatSalonBlue;
    [self.showFilter addSubview:iv];
    iv.center = CGPointMake(self.showFilter.titleLabel.center.x+50, self.showFilter.titleLabel.center.y+2);
    [self.view addSubview:self.showFilter];
    
    self.showFilter.hidden=YES;
    self.showFilter.alpha=0;
    
    [self.showFilter addTarget:self action:@selector(showFilterMenu) forControlEvents:UIControlEventTouchUpInside];
    

}

//sets up the navigation bar title
- (void)navigationBarTitleSetUp {
    self.dayLabel.text=@"Today";
    self.dayLabel.textColor=kWhatSalonBlueShadow;
    self.serviceLabel.text=@"-";
    self.serviceLabel.textColor=kWhatSalonBlue;
    
    [WSCore transparentNavigationBarOnView:self];
    UIView * navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kNavBarAndStatusBarHeight)];
    navView.backgroundColor=[UIColor colorWithWhite:1.0 alpha:1.0];
    [self.view addSubview:navView];
    self.automaticallyAdjustsScrollViewInsets=NO;
}

- (void)getServices {
    // services are not downloaded try again
    if (!self.hasDownloadedServices&&self.locationToSearch.coordinate.longitude!=0) {
        self.networkManager =[[GCNetworkManager alloc] init];
        self.networkManager.parentView=self.view;
        self.networkManager.delegate=self;
        //[self.networkManager loadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Services_Names_URL]] withParams:@"" WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        NSString * params = [NSString stringWithFormat:@"latitude=%f&longitude=%f",self.locationToSearch.coordinate.latitude,self.locationToSearch.coordinate.longitude];
        NSLog(@"Params %@",params);
        [self.networkManager loadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Names_Categories_Location_URL]
                                          ] withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
    }
    else{
        NSLog(@"No location");
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    /*
    BOOL coachMarksShown = [[NSUserDefaults standardUserDefaults] boolForKey:kIntructionalOverlay_LastMinute];
    if (coachMarksShown == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIntructionalOverlay_LastMinute];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Show coach marks
        [self.coachMarksView start];
        
        // Or show coach marks after a second delay
        // [coachMarksView performSelector:@selector(start) withObject:nil afterDelay:1.0f];
    }
     */
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [self navigationBarTitleSetUp];
    
    [self getServices];
    
    
    if (!self.isUnwinded) {
        self.dayLabel.text=self.chosenDay;
        self.serviceLabel.text = self.selectService.titleLabel.text;
    }
    
}

//sets up the Today and Tomorrow buttons which immitate a UISegment View
//reason: allows better customisablility if dealing with buttons over segments
- (void)todayTomorrowButtonSetUp {
    self.todayButton.backgroundColor=[UIColor clearColor];
    self.tomorrow.tag=1;
    self.tomorrow.layer.borderColor=kWhatSalonBlue.CGColor;
    self.tomorrow.layer.borderWidth=1.0;
    self.todayButton.layer.borderColor=kWhatSalonBlue.CGColor;
    self.todayButton.layer.borderWidth=1.0;
    self.tomorrow.backgroundColor=[UIColor clearColor];
    self.todayButton.tag=0;
    [self.tomorrow setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.todayButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.todayButton addTarget:self action:@selector(selectedDay:) forControlEvents:UIControlEventTouchUpInside];
    [self.tomorrow addTarget:self action:@selector(selectedDay:) forControlEvents:UIControlEventTouchUpInside];
    self.dayButtons =[NSMutableArray arrayWithObjects:self.todayButton,self.tomorrow, nil];
    [self selectedDay:self.todayButton];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
    footerView.backgroundColor=[UIColor clearColor];
    self.tableView.tableFooterView=footerView;
}

//sets up the view that holds the time buttons and their appearance
- (void)timesSelectorSetUp {
    self.timesHolder = [[UIView alloc] initWithFrame:CGRectMake(0, self.showFilter.frame.size.height+10+kNavBarAndStatusBarHeight, self.view.frame.size.width, self.showFilter.frame.size.height-10)];
    self.timesHolder.alpha=0.0;
    [self.view insertSubview:self.timesHolder aboveSubview:self.showFilter];
    UIView * background = [[UIView alloc] initWithFrame:CGRectMake(0, 2, self.timesHolder.frame.size.width, self.timesHolder.frame.size.height-4)];
    background.backgroundColor = self.showFilter.backgroundColor;
    [self.timesHolder addSubview:background];
    
    self.timeFrame = self.timesHolder.frame;
   
    self.buttonWidth = (self.timesHolder.frame.size.width-10)/5;//5 buttons
    
    self.allButton = [[UIButton alloc] init];
    [self.allButton setTitle:@"All" forState:UIControlStateNormal];
    [self.allButton addTarget:self action:@selector(timeButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    self.nineToTwelveButton = [[UIButton alloc] init];
    [self.nineToTwelveButton setTitle:@"9 - 12" forState:UIControlStateNormal];
    [self.nineToTwelveButton addTarget:self action:@selector(timeButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    self.twelveToThreeButton = [[UIButton alloc] init];
    [self.twelveToThreeButton setTitle:@"12 - 3" forState:UIControlStateNormal];
    [self.twelveToThreeButton addTarget:self action:@selector(timeButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    self.threeToSixButton= [[UIButton alloc] init];
    [self.threeToSixButton setTitle:@"3 - 6" forState:UIControlStateNormal];
    [self.threeToSixButton addTarget:self action:@selector(timeButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    self.sixToNineButton = [[UIButton alloc] init];
    [self.sixToNineButton setTitle:@"6 - 9" forState:UIControlStateNormal];
    [self.sixToNineButton addTarget:self action:@selector(timeButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    self.buttonArray = [NSMutableArray arrayWithObjects:self.allButton,self.nineToTwelveButton,self.twelveToThreeButton,self.threeToSixButton,self.sixToNineButton, nil];
    CGFloat x = 5;
    CGFloat y=0;
    CGFloat h = self.timesHolder.frame.size.height;
    int i =0;
    for (UIButton * btn in self.buttonArray) {
        CGRect frame = CGRectMake(x, y, self.buttonWidth, h);
        btn.frame=frame;
        btn.tag=i;
        [btn setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        
        if (btn.tag!=4) {
            UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(x+self.buttonWidth, (btn.frame.size.height/2)-(btn.frame.size.height/2)/2, 0.3, btn.frame.size.height/2)];
            seperatorView.backgroundColor=kWhatSalonSubTextColor;
            [self.timesHolder addSubview:seperatorView];
            
        }
        
        [self.timesHolder addSubview:btn];
        x=x+self.buttonWidth;
        i++;
    }
    
    [self updateTimeButtons:self.allButton];
}

- (void)lastMinuteManagerSetUp {
    
    
    self.lastMinuteManager = [[GCNetworkManager alloc] init];
    self.lastMinuteManager.parentView = self.view;
    self.lastMinuteManager.delegate=self;
    
    
}

- (void)locationPermissionQuery {
    // Do any additional setup after loading the view.
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             NSLog(@"Status %ld",(long)status);
                                             if (status == INTULocationStatusSuccess|| status == INTULocationStatusTimedOut) {
                                                 // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                 // currentLocation contains the device's current location.
                                                 NSLog(@"Success");
                                                 self.locationToSearch = [[CLLocation alloc] initWithLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude];
                                                 self.hasUsersLocation=YES;
                                                 self.descriptionText.text=@"Find last minute appointments closest to your current location.";
                                                 [self.descriptionText boldSubstring:@"current location."];
                                                 [self getServices];
                                             }
                                           
                                             else {
                                                 // An error occurred, more info is available by looking at the specific status returned.
                                                 self.descriptionText.text=@"Choose a location to search for last minute appointments.";
                                                 [self.descriptionText boldSubstring:@"Choose"];
                                                 
                                                 switch (status) {
                                                     case INTULocationStatusServicesDenied:
                                                         
                                                         
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"You have denied WhatSalon access to location services. In order to use this app, please accept access. \n Searching Salons will default to Cork City, Ireland" cancelButtonTitle:@"OK"];
                                                         break;
                                                     case INTULocationStatusServicesRestricted:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location services are restricted for WhatSalon. e.g (parental controls corporate policy, etc). \n Searching Salons will default to Cork City, Ireland" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                     case INTULocationStatusServicesDisabled:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app. \n Searching Salons will default to Cork City, Ireland" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                     case INTULocationStatusError:
                                                         
//                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"An error occurred while using the system location services." cancelButtonTitle:@"OK"];
                                                         break;
                                                     default:
                                                         break;
                                                 }
                                                 
                                             }
                                             
                                             
                                         }];
}

- (void)updateButtonAppearanceBasedOnTodaysTime {
    
    int hour = (int)[WSCore getCurrentHour] ;
    if (hour>=12) {
        [self disableButton:self.nineToTwelveButton];
    }
    if(hour>=15){
        [self disableButton:self.twelveToThreeButton];
    }
    if (hour>=18){
        [self disableButton:self.threeToSixButton];
        
    }
    if(hour>=21){
        [self disableButton:self.sixToNineButton];
        
    }
}

- (void)defaultSetUp {
    self.lastMinuteSalons = [NSMutableArray array];
    [self updateButtonAppearanceBasedOnTodaysTime];
    self.chosenDay = @"Today";
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init] ;
    [deltaComps setDay:1];
    NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
    NSDate * today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    self.chosenDate = today;
    self.todayDate=today;
    self.tomorrowDate=tomorrow;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.navigationController.navigationBar.hidden ==YES) {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
    
   
    
    
    [self locationPermissionQuery];
    
    /*
    UIImageView * backGroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    backGroundImage.image = [UIImage imageNamed:@"background_candy_stripe"];
    [self.view insertSubview:backGroundImage atIndex:0];
    */
    [WSCore floatingView:self.searchHolder WithAlpha:0.4];
    
    //self.searchHolder.backgroundColor = [UIColor colorWithRed:236.0f/255.0f green:240.0f/255.0f blue:241.0f/255.0f alpha:0.8];
    self.searchHolder.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = kBackgroundColor;
    

    [self tableViewSetUp];
    
    [self todayTomorrowButtonSetUp];

    self.view.backgroundColor=kBackgroundColor;
    
    [self searchHolderButtonSetUp];

   
    [WSCore addBottomLine:self.searchHolder :kCellLinesColour];

    
    [self filterButtonStylingAndSetUp];
 
    [self.view bringSubviewToFront:self.searchHolder];
    [self.view insertSubview:self.tableView belowSubview:self.searchHolder];
    [self.view bringSubviewToFront:self.showFilter];
   
    [self navigationBarTitleSetUp];
    
    [self timesSelectorSetUp];
    
    [self lastMinuteManagerSetUp];
   
    [self defaultSetUp];
    
    self.salonBookingModel = [[SalonBookingModel alloc] init];
    self.salonBookingModel.view = self.view;
    self.salonBookingModel.myDelegate=self;
    
    self.descriptionText.numberOfLines=0;
    self.descriptionText.textColor=kWhatSalonSubTextColor;
    self.descriptionText.textAlignment=NSTextAlignmentCenter;
    [self.descriptionText boldSubstring:@"current location"];
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{self.descriptionText.frame.origin.x-5,self.descriptionText.frame.origin.y+kNavBarAndStatusBarHeight-5},{self.descriptionText.frame.size.width+10,self.descriptionText.frame.size.height+10}}],
                                @"caption": self.descriptionText.text
                                                                /*@"shape": @"circle"*/
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{self.view.frame.size.width-55.0f,kStatusBarHeight},{45,45}}],
                                @"caption": @"Change Your Search Location"
                               // @"shape": @"circle"
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{self.todayButton.frame.origin.x-10,self.todayButton.frame.origin.y+kNavBarAndStatusBarHeight-5},{self.todayButton.frame.size.width+self.tomorrow.frame.size.width+20,self.todayButton.frame.size.height+10}}],
                                @"caption": @"Pick a Day"
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{self.selectService.frame.origin.x-5,self.selectService.frame.origin.y+kNavBarAndStatusBarHeight-5},{self.selectService.frame.size.width+10,self.selectService.frame.size.height+10}}],
                                @"caption": @"Select A Service"
                                },
                            
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{self.searchButton.frame.origin.x-5,self.searchButton.frame.origin.y+kNavBarAndStatusBarHeight-5},{self.searchButton.frame.size.width+10,self.searchButton.frame.size.height+10}}],
                                @"caption": @"Search For Appointments"
                                },
                            ];
    
   self.coachMarksView = [[WSCoachMarksView alloc] initWithFrame:self.navigationController.view.bounds coachMarks:coachMarks];
    [self.navigationController.view addSubview:self.coachMarksView];
    self.coachMarksView.animationDuration = 0.5f;
    self.coachMarksView.lblColor = [UIColor whiteColor];
    self.coachMarksView.delegate=self;
#warning remove when userdefaults are added
    [self.coachMarksView start];
    
    self.rightBarButton.enabled=NO;
    self.navigationItem.hidesBackButton=YES;
   
    
}


#pragma Coach Mark delegate
- (void)coachMarksViewDidCleanup:(WSCoachMarksView*)coachMarksView{
    self.rightBarButton.enabled=YES;
    //self.navigationController.navigationItem.leftBarButtonItem.enabled=YES;
    self.navigationItem.hidesBackButton=NO;
}

-(void)disableButton: (UIButton *)btn{
    btn.enabled=NO;
    [btn setTitleColor:kWhatSalonSubTextColor forState:UIControlStateNormal];
}
#pragma mark - GCNetwork Manager
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
   // NSLog(@"Success %@",jsonDictionary);
     [self dismissLoadingScreen];
    
    if (manager == self.lastMinuteManager) {
        
       
        if ([jsonDictionary[@"message"] isKindOfClass:[NSString class]]) {
            [UIView showSimpleAlertWithTitle:jsonDictionary[@"message"] message:@"" cancelButtonTitle:@"OK"];
        }else if([jsonDictionary[@"message"] isKindOfClass:[NSArray class]]){
            
            self.nineToTwelveArray = [NSMutableArray array];
            self.twelveToThreeArray = [NSMutableArray array];
            self.threeToSixArray = [NSMutableArray array];
            self.sixToNineArray = [NSMutableArray array];
            self.sectionTitlesArray = [NSMutableArray array];
            self.numberOfSectionsArray = [NSMutableArray array];
            
            self.sectionTitlesArray = [NSMutableArray array];
            for (id key in jsonDictionary[@"message"]) {
                NSLog(@"key %@",key[@"startTime"]);
                LastMinuteItem * lastMinute = [[LastMinuteItem alloc] init];
                lastMinute.branchServiceRef = key[@"branchServiceRef"];
                lastMinute.branch_identity = key[@"branch_identity"];
                lastMinute.endTime =key[@"endTime"];
                lastMinute.startTime=key[@"startTime"];
                lastMinute.service_id=key[@"service_id"];
                lastMinute.service_identity=key[@"service_identity"];
                lastMinute.slot_id = key[@"slot_id"];
                lastMinute.staffRef =key[@"staffRef"];
                lastMinute.staffRequest = key[@"staffRequest"];
                lastMinute.staff_phorest_id = key[@"staff_phorest_id"];
                lastMinute.timestamp_tag = key[@"timestamp_tag"];
                 NSLog(@"service price %@",key[@"service_price"]);
                lastMinute.service_price = key[@"service_price"];
                NSLog(@"lastmin service price %@",lastMinute.service_price);
                NSLog(@"Service name %@",key[@"service_name"]);
                lastMinute.service_name = key[@"service_name"];
                NSLog(@"lastMintue service name %@",lastMinute.service_name);
                lastMinute.lastMinuteSalon = [Salon salonWithID:key[@"salon_id"]];
                lastMinute.internal_start_time = key[@"internal_start_datetime"];
                lastMinute.internal_end_time = key[@"internal_end_datetime"];
               
                
                
                if ([key[@"salon_latest_review"]count]!=0) {
                    
                    lastMinute.lastMinuteSalon.hasReview=YES;
                    SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:key[@"salon_latest_review"][@"user_name"] AndWithLastName:key[@"salon_latest_review"][@"user_last_name"]];
                    NSLog(@"first name %@",key[@"salon_latest_review"][@"user_name"]);
                    salonReview.message = key[@"salon_latest_review"][@"message"];
                    salonReview.review_image=key[@"salon_latest_review"][@"review_image"];
                    salonReview.salon_rating=key[@"salon_latest_review"][@"salon_rating"];
                    salonReview.stylist_rating=key[@"salon_latest_review"][@"stylist_rating"];
                    salonReview.date_reviewed=key[@"salon_latest_review"][@"date_reviewed"];
                    salonReview.user_avatar_url=key[@"salon_latest_review"][@"user_avatar"];
                    lastMinute.lastMinuteSalon.salonReview=salonReview;
                    
                }
                if (key[@"staff_details"]!=[NSNull null] && [key[@"staff_details"] count]>0) {
                    lastMinute.staff_id = key[@"staff_details"][@"staff_id"];
                }
                if (key[@"salon_name"] !=[NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_name=key[@"salon_name"];
                }
                if (key[@"salon_description"] !=[NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_description = key[@"salon_description"];
                }
                if (key[@"salon_phone"] != [NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_phone = key[@"salon_phone"];
                }
                if (key[@"salon_lat"] !=[NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_lat = [key[@"salon_lat"] doubleValue];
                }
                if (key[@"salon_lon"] != [NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_long = [key[@"salon_lon"] doubleValue];
                }
                if (key[@"salon_address_1"] != [NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_address_1 = key[@"salon_address_1"];
                }
                if (key[@"salon_address_2"] != [NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_address_2 = key[@"salon_address_2"];
                }
                if (key[@"salon_address_3"] != [NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_address_3 = key[@"salon_address_3"];
                }
                if (key[@"city_name"] != [NSNull null]) {
                    lastMinute.lastMinuteSalon.city_name = key[@"city_name"];
                }
                if (key[@"county_name"]) {
                    lastMinute.lastMinuteSalon.country_name =key[@"county_name"];
                }
                if (key[@"salon_image"] != [NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_image = key[@"salon_image"];
                }
                
                if (key[@"rating"] != [NSNull null]) {
                    NSLog(@"ratings key %@",key[@"rating"]);
                    NSLog(@"ratings double %f",[key[@"rating"] doubleValue]);
                    lastMinute.lastMinuteSalon.ratings = [key[@"rating"] doubleValue];
                    NSLog(@"Last min ratings %f",lastMinute.lastMinuteSalon.ratings);
                }
                
                if (key[@"is_favourite"] != [NSNull null]) {
                    lastMinute.lastMinuteSalon.is_favourite = [key[@"is_favourite"] boolValue];
                }
                
                
                if (key[@"distance"] !=[NSNull null]) {
                    lastMinute.lastMinuteSalon.distance = [key[@"distance"] doubleValue];
                }
                if (key[@"salon_description"] !=[NSNull null]) {
                    lastMinute.lastMinuteSalon.salon_description= key[@"salon_description"];
                }
                if (key[@"salon_images"]!=[NSNull null]) {
                    
                    NSArray *messageArray = key[@"salon_images"];
                    
                    for (NSDictionary * dataDict in messageArray) {
                        
                        NSString * imagePath = [dataDict objectForKey:@"image_path"];
                        
                        
                        [lastMinute.lastMinuteSalon.slideShowGallery addObject:imagePath];
                    }
                }
                
                if (key[@"room_id"]!=[NSNull null]) {
                    lastMinute.room_id=key[@"room_id"];
                }
                if (key[@"reviews"]!=[NSNull null]) {
                    lastMinute.lastMinuteSalon.reviews = [key[@"reviews"] doubleValue];
                }
                
                if (key[@"start_datetime"]!=[NSNull null]) {
                    lastMinute.start_datetime = key[@"start_datetime"];
                }
                if (key[@"end_datetime"]!=[NSNull null]) {
                    lastMinute.end_datetime=key[@"end_datetime"];
                    
                }
                if ([key[@"salon_categories"] count]!=0) {
                    lastMinute.lastMinuteSalon.salonCategories =  @{
                                                                    @"Hair" : [NSNumber numberWithBool:[key[@"salon_categories"][@"Hair"] boolValue]],
                                                                    @"Nails" : [NSNumber numberWithBool:[key[@"salon_categories"][@"Nails"]boolValue]],
                                                                    @"Face" : [NSNumber numberWithBool:[key[@"salon_categories"][@"Face"] boolValue]],
                                                                    @"Body" : [NSNumber numberWithBool:[key[@"salon_categories"][@"Body"] boolValue]],
                                                                    @"Massage" : [NSNumber numberWithBool:[key[@"salon_categories"][@"Massage"] boolValue]],
                                                                    @"Hair Removal" : [NSNumber numberWithBool:[key[@"salon_categories"][@"Hair Removal"] boolValue]],
                                                                    };
                }
                
                NSLog(@"%@ categories %@",lastMinute.lastMinuteSalon.salon_name,lastMinute.lastMinuteSalon.salonCategories);
                
                if ([key[@"salon_opening_hours"] count]!=0) {
                    
                    NSArray * openingHours =key[@"salon_opening_hours"];
                    for (NSDictionary* day in openingHours) {
                        NSLog(@"%@ day %@",lastMinute.lastMinuteSalon.salon_name, day);
                        OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
                        
                        [lastMinute.lastMinuteSalon.openingHours addObject:openingDay];
                    }
                    
                    switch ([lastMinute.timestamp_tag intValue]) {
                        case 0:
                            [self.nineToTwelveArray addObject:lastMinute];
                            break;
                            
                            
                        case 1:
                            [self.twelveToThreeArray addObject:lastMinute];
                            break;
                            
                        case 2:
                            [self.threeToSixArray addObject:lastMinute];
                            break;
                        case 3:
                            [self.sixToNineArray addObject:lastMinute];
                            break;
                            
                        default:
                            break;
                    }
                    
                }
               
            }
            
           
            self.numberOfSectionsArray = [NSMutableArray array];
            if (self.nineToTwelveArray.count!=0) {
                [self.numberOfSectionsArray addObject:self.nineToTwelveArray];
                            }
            if (self.twelveToThreeArray.count!=0) {
                [self.numberOfSectionsArray addObject:self.twelveToThreeArray];
                             }
            if (self.threeToSixArray.count!=0) {
                [self.numberOfSectionsArray addObject:self.threeToSixArray];
                
                
            }
            if (self.sixToNineArray.count!=0) {
                [self.numberOfSectionsArray addObject:self.sixToNineArray];
                
                
                
            }
            
            [self.sectionTitlesArray addObject: @"9 a.m - 12 p.m"];
            [self.sectionTitlesArray addObject: @"12 p.m - 3 p.m"];
            [self.sectionTitlesArray addObject: @"3 p.m - 6 p.m"];
            [self.sectionTitlesArray addObject: @"6 p.m - 9 p.m"];
            
            
            [self hideSearchHolderAndShowTable];
        }
        
     
        
    
        
    }else{
     
       
        self.hasDownloadedServices=YES;
        
        self.friendlyServices = [NSMutableArray array];
        if ([jsonDictionary[@"message"] isKindOfClass:[NSArray class]]) {
            for (id key in jsonDictionary[@"message"]){
                
                FriendlyService * friendlyService = [[FriendlyService alloc] init];
                friendlyService.friendly_category=key[@"friendly_category"];
                friendlyService.friendly_name=key[@"friendly_name"];
                friendlyService.friendly_service_id=key[@"friendly_service_id"] ;
                friendlyService.friendly_category_title=key[@"category_title"];
                
                if ([WSCore stringIsEmpty:friendlyService.friendly_category_title]) {
                    
                    
                    switch ([friendlyService.friendly_service_id intValue]) {
                        case 1:
                            friendlyService.friendly_category_title=@"Hair";
                            break;
                        case 2:
                            friendlyService.friendly_category_title=@"Hair Removal";
                            break;
                            
                        case 3:
                            friendlyService.friendly_category_title=@"Nails";
                            break;
                            
                        case 4:
                            friendlyService.friendly_category_title=@"Massage";
                            break;
                            
                        case 5:
                            friendlyService.friendly_category_title=@"Face";
                            break;
                            
                        case 6:
                            friendlyService.friendly_category_title=@"Body";
                            break;
                            
                        default:
                            break;
                    }
                    
                }
                
                [self.friendlyServices addObject:friendlyService];
              
            }
        }
      
    }
    
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Fail %@",jsonDictionary);
    [self dismissLoadingScreen];
    [WSCore showServerErrorAlert];
}

- (void)updateTimeButtons:(UIButton *)btn {
    for (UIButton * button  in self.buttonArray) {
        if (button.tag==btn.tag) {
            button.layer.cornerRadius=kCornerRadius;
            button.layer.borderColor=kWhatSalonBlue.CGColor;
            button.layer.borderWidth=2.0;
        }
        else{
            button.layer.borderWidth=0;
        }
    }
}

//handles the selection of the days (Today, Tomorrow buttons)
-(void)timeButtonSelected:(UIButton *) btn{
    
   
    switch (btn.tag) {
            
        case 0:
            if (self.nineToTwelveArray.count==0 && self.twelveToThreeArray.count==0 && self.threeToSixArray.count==0 && self.sixToNineArray.count==0) {
                [UIView showSimpleAlertWithTitle:@"No Times Available" message:@"There are no times available for the selected time. Please try again." cancelButtonTitle:@"OK"];
            }
            else{
                [self scrollToSection:btn.tag];
                 [self updateTimeButtons:btn];
            }
            
            break;
        case 1:
            if (self.nineToTwelveArray.count==0) {
                [UIView showSimpleAlertWithTitle:@"No Times Available" message:@"There are no times available for the selected time. Please try again." cancelButtonTitle:@"OK"];
            }
            else{
                [self scrollToSection:btn.tag];
                [self updateTimeButtons:btn];
            }
            break;
        case 2:
            if (self.twelveToThreeArray.count==0) {
                [UIView showSimpleAlertWithTitle:@"No Times Available" message:@"There are no times available for the selected time. Please try again." cancelButtonTitle:@"OK"];
            }else{
                [self scrollToSection:btn.tag];
                [self updateTimeButtons:btn];
            }
            break;
        case 3:
            if (self.threeToSixArray.count==0) {
                [UIView showSimpleAlertWithTitle:@"No Times Available" message:@"There are no times available for the selected time. Please try again." cancelButtonTitle:@"OK"];
            }else{
                [self scrollToSection:btn.tag];
                [self updateTimeButtons:btn];
            }
            break;
        case 4:
            if (self.sixToNineArray.count==0) {
                [UIView showSimpleAlertWithTitle:@"No Times Available" message:@"There are no times available for the selected time. Please try again." cancelButtonTitle:@"OK"];
            }else{
                [self scrollToSection:btn.tag];
                [self updateTimeButtons:btn];
            }
            break;
            
            
        default:
            break;
    }

   
    
    
    
}

//scrolls to the section
-(void)scrollToSection: (NSInteger) btnTag{
    if (btnTag==0) {
        //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        //[self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        self.tableView.contentOffset = CGPointMake(0, 0 - self.tableView.contentInset.top);

    }
    else{
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:btnTag-1];
        [self.tableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
    }
   
}

#pragma mark - UITableView Datasource and Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

//customise the header view
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 48)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(45, 0, 200, view.frame.size.height)];
    [label setFont:[UIFont systemFontOfSize:16.0f]];
    UIImageView * timerImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
    timerImage.image = [UIImage imageNamed:@"timing_icon"];
    timerImage.image = [timerImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    timerImage.tintColor=kWhatSalonSubTextColor;
    [view addSubview:timerImage];
    timerImage.center =CGPointMake(20, view.center.y);

    NSString *string =[NSString stringWithFormat:@"%@",self.sectionTitlesArray[section]];
    [label setText:string];
    label.textColor=kWhatSalonSubTextColor;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]];
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            if (self.nineToTwelveArray.count==0) {
                return 0;
            }
            break;
        case 1:
            if (self.twelveToThreeArray.count==0) {
                return 0;
            }
            break;
            
        case 2:
            if (self.threeToSixArray.count==0) {
                return 0;
            }
            break;
            
        case 3:
            if (self.sixToNineArray.count==0) {
                return 0;
            }

            break;
            
        default:
            break;
    }
    
    
    

    return 48;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    switch (section) {
        case 0:{
            
        
            
            return self.nineToTwelveArray.count;
        }
            break;
        case 1:{
            
            
            return self.twelveToThreeArray.count;
        }
            break;
        case 2:{
            
            
            return self.threeToSixArray.count;
        
        }
            break;
        case 3:{
            
            
            return self.sixToNineArray.count;
        
        }
            break;



            
        default:
            return 0;
            break;
    }
    return 0;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    BrowseTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.isDiscovery=YES;
    cell.parent=self;
    cell.myDelegate=self;
    switch (indexPath.section) {
        case 0:
             [cell setUpCellWithSalon:self.nineToTwelveArray[indexPath.row]];
            break;
        case 1:
            [cell setUpCellWithSalon:self.twelveToThreeArray[indexPath.row]];
            break;
        case 2:
            [cell setUpCellWithSalon:self.threeToSixArray[indexPath.row]];
            break;
        case 3:
            [cell setUpCellWithSalon:self.sixToNineArray[indexPath.row]];
            break;

            
        default:
            break;
    }
    
   
    
    
    return cell;
    
}


#pragma mark - LastMintueDelegate

-(void)bookNowActionTriggeredWithLastMinuteItem:(LastMinuteItem *)l{
    NSLog(@"Book now");
    self.selectedLastMin = l;
    self.bookingObj= [Booking bookingWithSalonID:self.selectedLastMin.lastMinuteSalon.salon_id];
    
    [WSCore setBookingType:BookingTypeLastMinute];
    [self.salonBookingModel bookPhorestServiceWithUserID:[[User getInstance] fetchAccessToken] WithSalonID:self.selectedLastMin.lastMinuteSalon.salon_id WithSlotID:self.selectedLastMin.slot_id WIthServiceID:self.selectedLastMin.service_id WithRoomID:self.selectedLastMin.room_id WithInternalStartDateTime:self.selectedLastMin.internal_start_time WithInternalEndDateTime:self.selectedLastMin.internal_end_time AndStaff_id:self.selectedLastMin.staff_id];
   
    
    
}

//shows the reviews when its clicked upon
-(void)showReviewsWithLastMinuteItem:(LastMinuteItem *)rl{
    NSLog(@"Show salon reviews");
    SalonReviewListViewController * salonReview = [self.storyboard instantiateViewControllerWithIdentifier:@"reviewList"];
    salonReview.salonData=rl.lastMinuteSalon;
    salonReview.transitioningDelegate=self;
    salonReview.modalPresentationStyle = UIModalPresentationCustom;
    //salonReview.isList=YES;
    [self presentViewController:salonReview animated:YES completion:nil];
}

//shows the map when clicked upon
-(void)showMapWithLastMinuteItem:(LastMinuteItem *)ml{
    
    LastMintuteMapViewController * mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"lastMinMapVC"];
    mapVC.lastMin=ml;
    mapVC.modalPresentationStyle = UIModalPresentationCustom;
    
    mapVC.transitioningDelegate = self;
    [self presentViewController:mapVC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//handles the select service call
- (IBAction)selectService:(id)sender {
    
    if (self.locationToSearch.coordinate.longitude!=0) {
        NSLog(@"Show list of services");
        if (!self.hasDownloadedServices) {
            [UIView showSimpleAlertWithTitle:@"Please wait" message:@"Service list is downloading, please wait" cancelButtonTitle:@"OK"];
        }
        else{
            
            SelectCategoryViewController * s = [self.storyboard instantiateViewControllerWithIdentifier:@"selectCategoryVC"];
            s.modalPresentationStyle = UIModalPresentationCustom;
            
            s.transitioningDelegate = self;

            s.servicesArray= self.friendlyServices;
            [self presentViewController:s animated:YES completion:nil];
        }

    }
    else{
        [UIView showSimpleAlertWithTitle:@"Choose Location" message:@"No location selected. Please select a location." cancelButtonTitle:@"OK"];
    }
    
}

//searches for results
- (void)searchForResults {
    if (![WSCore stringIsEmpty:self.serviceIdToSearch] &&[[User getInstance] isUserLoggedIn] && [[User getInstance] isTwilioVerified] && [[User getInstance] hasCreditCard]) {
        NSString * params = [NSString stringWithFormat:@"service_id=%@",self.serviceIdToSearch];
        
        
        if ([[User getInstance]isUserLoggedIn ]) {
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken]]];
        }
        
        if (CLLocationCoordinate2DIsValid(self.locationToSearch.coordinate)) {
            NSLog(@"Coordinate valid");
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%f&longitude=%f",self.locationToSearch.coordinate.latitude,self.locationToSearch.coordinate.longitude]];
        } else {
            NSLog(@"Coordinate invalid");
        }
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&start_datetime=%lld",self.chosenDaySeconds]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&end_datetime=%lld",self.endDaySeconds]];
        self.tableView.hidden=YES;

        NSLog(@"Params %@",params);
        [self.lastMinuteManager loadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kLast_Minute_URL]] withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        [self showLoadingScreen];
       // [self.lastMinuteManager progressLoaderWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kLast_Minute_URL]] withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    }
    else{
        if (![[User getInstance] isUserLoggedIn]) {
            [UIView showSimpleAlertWithTitle:@"Bummer!" message:@"Please log in to use this service." cancelButtonTitle:@"OK"];
            
        }
        else if(![[User getInstance] hasCreditCard]){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Credit Card" message:@"In order to use the 'Last Minute' search you must have a credit card attached to your account. Do you wish to add a card? " delegate:self cancelButtonTitle:@"No thanks" otherButtonTitles:@"Add Card", nil];
            alert.tag=1111;
            [alert show];
        }
        else if (![[User getInstance] isTwilioVerified]){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Verify Phone Number" message:@"In order to use the 'Last Minute' search you must verify your phone number." delegate:self cancelButtonTitle:@"No thanks" otherButtonTitles:@"Verify Number", nil];
            alert.tag=1112;
            [alert show];
        }
        else{
            [UIView showSimpleAlertWithTitle:@"Bummer!" message:@"Please select a service" cancelButtonTitle:@"OK"];
        }
        
    }
}


- (IBAction)search:(id)sender {
    
    [self searchForResults];
   

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1111) {
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            
            //go to the credit card screen
            /*
            LinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
            linkCC.modalPresentationStyle = UIModalPresentationCurrentContext;
            linkCC.isFromLastMin=YES;
            linkCC.delegate=self;
            [self presentViewController:linkCC animated:YES completion:nil];
            */
            TabLinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
            linkCC.delegate=self;
            [self presentViewController:linkCC animated:YES completion:nil];
        }
    }
}

-(void)didAddCard{
    NSLog(@"did add card");
}
-(void)didSkipCard{
    NSLog(@"Skipped");
}

//animation when table view is shown
//hide right bar button
//update search holder
//hide the black overlay if present
//perform animation to hide searchHolder
-(void)hideSearchHolderAndShowTable{
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.verticalSpace.constant -=self.searchHolder.frame.size.height+kNavBarAndStatusBarHeight;
    
    [self.searchHolder setNeedsUpdateConstraints];
    if (self.black) {
        self.black.hidden=YES;
    }
    //[WSCore transparentNavigationBarOnView:self];
    [UIView animateWithDuration:1
                     animations:^{
                         
                         [self.searchHolder layoutIfNeeded];
                         
                     } completion:^(BOOL finished) {
                         
                         //does not hide holder for times
                         self.timesHolder.hidden=NO;
                         //does not show filter button
                         self.showFilter.hidden=NO;
                         
                         
                         [UIView animateWithDuration:0.5
                          
                                          animations:^{
                                              
                                              //alpha is currently 0 set it to 1
                                              self.timesHolder.alpha=1.0;
                                              self.showFilter.alpha=1.0;
                                              
                                          } completion:^(BOOL finished) {
                                              //dont hide the table view
                                              self.tableView.hidden=NO;
                                              
                                              [self.tableView reloadDataAnimated:YES];
                                              
                                          }];
                     }];

}
//
-(void)showFilterMenu{
    
    
    [self showSearchHolder];
}

//re-hides the search holder once clicked after downloading results
-(void)cancelSearchHolder{

    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.verticalSpace.constant -=self.searchHolder.frame.size.height+kNavBarAndStatusBarHeight;
    
    [self.searchHolder setNeedsUpdateConstraints];
    if (self.black) {
        self.black.hidden=YES;
    }
    
    [UIView animateWithDuration:1
                     animations:^{
                         
                         [self.searchHolder layoutIfNeeded];
                         
                     } completion:^(BOOL finished) {
                         
                         //does not hide holder for times
                         self.timesHolder.hidden=NO;
                         //does not show filter button
                         self.showFilter.hidden=NO;
                         
                         
                         [UIView animateWithDuration:0.5
                          
                                          animations:^{
                                              
                                              //alpha is currently 0 set it to 1
                                              self.timesHolder.alpha=1.0;
                                              self.showFilter.alpha=1.0;
                                              
                                          } completion:^(BOOL finished) {
                                              //dont hide the table view
                                              self.tableView.hidden=NO;
                                              
                                              [self.tableView reloadDataAnimated:YES];
                                              
                                          }];
                     }];
    

}

//show search holder
-(void)showSearchHolder{
    
     self.navigationItem.rightBarButtonItem.enabled = YES;
    [UIView animateWithDuration:0.5
     
                     animations:^{
                         
                         //hides these buttons
                         self.timesHolder.alpha=0.0;
                         self.showFilter.alpha=0.0;
                         
                         
                     } completion:^(BOOL finished) {
                         
                         self.timesHolder.hidden=YES;
                         self.showFilter.hidden=YES;
                         
                         //adjust the vertical constant
                         self.verticalSpace.constant =0;
                         [self.searchHolder setNeedsUpdateConstraints];
                         
                         
                         
                         [UIView animateWithDuration:1
                                          animations:^{
                                              
                                              [self.searchHolder layoutIfNeeded];
                                              
                                          } completion:^(BOOL finished) {
                                              
                                              
                                              
                                              self.black = [[UIView alloc] initWithFrame:CGRectMake(0, kNavBarAndStatusBarHeight+self.searchHolder.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-kNavBarAndStatusBarHeight-self.searchHolder.frame.size.height)];
                                              self.black.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.7
                                                                          ];
                                              [self.view addSubview:self.black];
                                              [self.view bringSubviewToFront:self.black];
                                              UIButton* cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
                                              [cancelButton setImage:kCancelButtonImage forState:UIControlStateNormal];
                                              [cancelButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
                                              //[cancelButton setTitle:@"Hellow" forState:UIControlStateNormal];
                                              [self.black addSubview:cancelButton];
                                              cancelButton.center = CGPointMake(self.black.center.x, self.black.frame.size.height-cancelButton.frame.size.height-20);
                                              [cancelButton addTarget:self action:@selector(cancelSearchHolder) forControlEvents:UIControlEventTouchUpInside];
                                          }];
                     }];
    

}

//determines when the tableview was scrolled
-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint currentOffset = scrollView.contentOffset;
     //tableview is scrolled down
    if (currentOffset.y > self.lastContentOffset.y)
    {
       
        //if the showFilter button is visible i.e alpha is greater than 0
        if (self.showFilter.alpha==1.0) {
            
            [UIView animateWithDuration:1 animations:^{
                
                //hide show filter button
                self.showFilter.alpha=0.0;
                
                //adjust times Holders frmae
                self.timesHolder.frame=CGRectMake(self.showFilter.frame.origin.x, self.showFilter.frame.origin.y, self.view.frame.size.width, self.timesHolder.frame.size.height);
                //adjusts the tableview frame
                self.tableView.frame=CGRectMake(0, self.timesHolder.frame.size.height+5+kNavBarAndStatusBarHeight, self.tableView.frame.size.width, self.view.frame.size.height);
            }];
        
        }
        
        
    }
    
    
    //determines if it is scrolled back to the top
    if (self.showFilter.alpha==0.0 && currentOffset.y==0) {
        [UIView animateWithDuration:0.3 animations:^{
            //show filter button
            self.showFilter.alpha=1.0;
            
            //adjust frames
            self.timesHolder.frame=self.timeFrame;
            self.tableView.frame=self.tableFrame;
        }];
    }

    self.lastContentOffset = currentOffset;
}

//gets unix timestamp for now
- (void)nowTimeUnixTimeStamp {

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *timeStamp = [dateFormatter stringFromDate:[NSDate date]];
    NSDate *curdate = [dateFormatter dateFromString:timeStamp];
    int unix_timestamp =  [curdate timeIntervalSince1970];
    NSDate* referenceDate = [NSDate dateWithTimeIntervalSince1970: 0];
    NSTimeZone* timeZone = [NSTimeZone systemTimeZone];
    int offset = (int)[timeZone secondsFromGMTForDate: referenceDate];
    int currentTimestamp = unix_timestamp + offset;
    
    self.chosenDaySeconds =currentTimestamp;
}

//gets the unix timestamp of today
- (void)endOfTodayUnixTimeStamp {
    
    NSDate *date = [NSDate date];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = [[NSCalendar currentCalendar] ordinalityOfUnit:(NSCalendarUnitDay) inUnit:(NSCalendarUnitEra) forDate:date];
    components.day += 1;
    NSDate *dayEnd = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc]init];
    dateFormatter2.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    [dateFormatter2 setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *laterTimeStamp = [dateFormatter2 stringFromDate:dayEnd];
    NSDate *laterDate = [dateFormatter2 dateFromString:laterTimeStamp];
    int unix_timestamp_later =  [laterDate timeIntervalSince1970];
    NSDate* referenceDateLater = [NSDate dateWithTimeIntervalSince1970: 0];
    NSTimeZone* timeZoneLater = [NSTimeZone systemTimeZone];
    int offsetLater = (int)[timeZoneLater secondsFromGMTForDate: referenceDateLater];
    int currentTimestampLater = unix_timestamp_later + offsetLater;
    self.endDaySeconds = currentTimestampLater;
  
}

//gets tomorrow start time
- (void)tomorrowStartUnixTimestamp:(NSDateFormatter **)dateFormatter2_p timeZoneLater_p:(NSTimeZone **)timeZoneLater_p currentTimestampLater_p:(int *)currentTimestampLater_p tomorrowEnd_p:(NSDate **)tomorrowEnd_p {
    NSDate *date = [NSDate date];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = [[NSCalendar currentCalendar] ordinalityOfUnit:(NSCalendarUnitDay) inUnit:(NSCalendarUnitEra) forDate:date];
    components.day += 1;
    NSDate *tomorrowStart = [[NSCalendar currentCalendar] dateFromComponents:components];
    components.day+=1;
    *tomorrowEnd_p = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    *dateFormatter2_p = [[NSDateFormatter alloc]init];
    (*dateFormatter2_p).dateFormat = @"yyyy-MM-dd HH:mm:ss";
    [*dateFormatter2_p setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *laterTimeStamp = [*dateFormatter2_p stringFromDate:tomorrowStart];
    NSDate *laterDate = [*dateFormatter2_p dateFromString:laterTimeStamp];
    int unix_timestamp_later =  [laterDate timeIntervalSince1970];
    NSDate* referenceDateLater = [NSDate dateWithTimeIntervalSince1970: 0];
    *timeZoneLater_p = [NSTimeZone systemTimeZone];
    int offsetLater = (int)[*timeZoneLater_p secondsFromGMTForDate: referenceDateLater];
    *currentTimestampLater_p = unix_timestamp_later + offsetLater;
    self.chosenDaySeconds = *currentTimestampLater_p;
}


//gets tomorrow end datetime
- (int)tomorrowEndUnixTimestamp:(NSDate *)tomorrowEnd dateFormatter2:(NSDateFormatter *)dateFormatter2 timeZoneLater:(NSTimeZone *)timeZoneLater {
    //tomorrow end
    NSString *laterTimeStampEnd = [dateFormatter2 stringFromDate:tomorrowEnd];
    NSDate *laterDateEnd = [dateFormatter2 dateFromString:laterTimeStampEnd];
    int unix_timestamp_later_end =  [laterDateEnd timeIntervalSince1970];
    NSDate* referenceDateLaterEnd = [NSDate dateWithTimeIntervalSince1970: 0];
    int offsetLaterEnd = (int)[timeZoneLater secondsFromGMTForDate: referenceDateLaterEnd];
    int currentTimestampLaterEnd= unix_timestamp_later_end + offsetLaterEnd;
    self.endDaySeconds = currentTimestampLaterEnd;
    return currentTimestampLaterEnd;
}

//determine what happens when tomorrow is selected
- (void)tomorrowIsSelected {
    self.dayLabel.text=@"Tomorrow";
    self.chosenDate=self.tomorrowDate;
    self.chosenDay =self.dayLabel.text;
    UIButton * button = [[UIButton alloc] init];
    button.tag=0;
    [self updateTimeButtons:button];
    
    NSDate *tomorrowEnd;
    NSDateFormatter *dateFormatter2;
    NSTimeZone *timeZoneLater;
    int currentTimestampLater;
    
    [self tomorrowStartUnixTimestamp:&dateFormatter2 timeZoneLater_p:&timeZoneLater currentTimestampLater_p:&currentTimestampLater tomorrowEnd_p:&tomorrowEnd];
    
    
    int currentTimestampLaterEnd;
    currentTimestampLaterEnd = [self tomorrowEndUnixTimestamp:tomorrowEnd dateFormatter2:dateFormatter2 timeZoneLater:timeZoneLater];
   
}

//determine what happens when today is selected
- (void)todayIsSelected {
    self.dayLabel.text=@"Today";
    self.chosenDay = self.dayLabel.text;
    self.chosenDate = self.todayDate;
    [self nowTimeUnixTimeStamp];
    
    [self endOfTodayUnixTimeStamp];
}

//the actions when Today Tomorrow buttons are pressed
- (void)selectedDay:(UIButton *)btn {
    
    
    for (UIButton * button in self.dayButtons) {
        if (btn.tag==button.tag) {
           
            //update button to have selected look
            button.backgroundColor=kWhatSalonBlue;
            [button setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
        }
        else{
            //update button to have a deselected look
            button.backgroundColor=[UIColor clearColor];
            [button setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        }
    }
    
    if (btn.tag==0) {
        [self todayIsSelected];
        [self updateButtonAppearanceBasedOnTodaysTime];
    }
    else{
        [self tomorrowIsSelected];
        [self.nineToTwelveButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        self.nineToTwelveButton.enabled=YES;
        [self.twelveToThreeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        self.twelveToThreeButton.enabled=YES;
        [self.threeToSixButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        self.threeToSixButton.enabled=YES;
        [self.sixToNineButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        self.sixToNineButton.enabled=YES;
        
    }
    
   
   
}
//change location button
- (IBAction)changeLocation:(id)sender {
    [self performSegueWithIdentifier:@"lastMinLocationVC" sender:self];
    
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return [[DismissDetailTransition alloc] init];
}


#pragma mark - UNWIND Segue
- (IBAction)unwindToLastMinute:(UIStoryboardSegue *)unwindSegue
{
   
    //get the resutls back from selectservicesviewcontroller
    SelectServicesViewController * unwind = [unwindSegue sourceViewController];
   [self.selectService setTitle:unwind.friendlySelectedService.friendly_name forState:UIControlStateNormal];
    self.serviceLabel.text = unwind.friendlySelectedService.friendly_name;
    self.serviceIdToSearch = unwind.friendlySelectedService.friendly_service_id;
}



//wrong name - should be unwindToLastMinute
- (IBAction)unwindToDiscoveryFromLastMinute:(UIStoryboardSegue *)unwindSegue
{
     self.navigationItem.rightBarButtonItem.enabled = YES;
    self.serviceIdToSearch=@"";
    self.isUnwinded=YES;
    self.tableView.hidden=YES;
    [self.tableView setContentOffset:CGPointZero animated:YES];
    self.black.hidden=YES;
    [self todayTomorrowButtonSetUp];
    [self defaultSetUp];
    [self.selectService setTitle:@"Select A Service" forState:UIControlStateNormal];
  
    
    [UIView animateWithDuration:0.5
     
                     animations:^{
                         
                         //hides these buttons
                         self.timesHolder.alpha=0.0;
                         self.showFilter.alpha=0.0;
                         
                         
                     } completion:^(BOOL finished) {
                         
                         self.timesHolder.hidden=YES;
                         self.showFilter.hidden=YES;
                         
                         //adjust the vertical constant
                         self.verticalSpace.constant =0;
                         [self.searchHolder setNeedsUpdateConstraints];
                         
                         
                         
                         [UIView animateWithDuration:1
                                          animations:^{
                                              
                                              [self.searchHolder layoutIfNeeded];
                                              
                                          } completion:nil];
                      }];
    
     
}

-(void)failureAppointmentWithDictionary:(NSDictionary *)jsonDictionary{
   
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView showSimpleAlertWithTitle:@"Unable to book" message:[NSString stringWithFormat:@"%@", jsonDictionary[@"message"]] cancelButtonTitle:@"OK"];
    });
    
}

-(void)successfullAppointmentWithDictionary:(NSDictionary *)jsonDictionary{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString * ref = jsonDictionary[@"message"][@"appointment_reference_number"];
        self.bookingObj.appointment_reference_number=ref;
        
        Salon * salon=self.selectedLastMin.lastMinuteSalon;
        
        ConfirmViewController * confirmViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"confirmVC"];
        confirmViewController.salonData = salon;
        confirmViewController.isFromLastMinute=YES;
        
        
        NSString * day = self.chosenDay;
        day  = [day stringByAppendingString:[NSString stringWithFormat:@", %@",[self.selectedLastMin getSimpleDateISO8601]]];
        self.bookingObj.chosenDay = day;
        self.bookingObj.chosenStylist = @"Any stylist";
        self.bookingObj.staff_id = self.selectedLastMin.staff_phorest_id;
        self.bookingObj.chosenTime = [self.selectedLastMin getSimpleDateISO8601]
        ;
        self.bookingObj.slot_id = self.selectedLastMin.slot_id;
        self.bookingObj.internal_start_datetime = self.selectedLastMin.startTime;
        self.bookingObj.internal_end_datetime = self.selectedLastMin.endTime;
        self.bookingObj.endTime = self.selectedLastMin.endTime;
        self.bookingObj.chosenService = self.selectService.titleLabel.text;
        self.bookingObj.chosenPrice = [NSString stringWithFormat:@"%.2f",[self.selectedLastMin.service_price doubleValue]];
        confirmViewController.bookingObj=self.bookingObj;
        
        
 
        NSArray *timeSegments = [self.bookingObj.chosenTime componentsSeparatedByString:@":"];
       
        
        //get pm/am
        NSArray * minsAmPmSegments = [timeSegments[1] componentsSeparatedByString:@" "];
        NSLog(@"Min %@",minsAmPmSegments[0]);
        NSLog(@"AM/PM %@",minsAmPmSegments[1]);
        
        NSDate *currentDate = self.chosenDate;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate]; // Get necessary date components
        
        [components month]; //gives you month
        [components day]; //gives you day
        [components year]; // gives you year
        
        NSString* firstString = [timeSegments objectAtIndex:0];
        NSLog(@"\n\nFirst String (HOUR) %@",firstString);
        NSString* secondString = [minsAmPmSegments objectAtIndex:0];
        NSLog(@"Second string (MIN) %@",secondString);
        NSLog(@"AM OR PM %@\n\n\n", minsAmPmSegments[1]);
        if ([[minsAmPmSegments[1] lowercaseString ]isEqualToString:@"pm"]) {
            firstString = [NSString stringWithFormat:@"%d",[firstString intValue] + 12];
            NSLog(@"\n\nFirst String (HOUR) in 24 format %@",firstString);
        }
        
        NSDate * fullDate = [WSCore dateWithYear:[components year] month:[components month] day:[components day] WithHour:[firstString intValue] AndMin:[secondString intValue]];
        NSLog(@"Full Date %@",fullDate);
        confirmViewController.chosenDate=fullDate;
        
        [self presentViewController:confirmViewController animated:YES completion:nil];
        
    });
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"lastMinLocationVC"]) {
        UINavigationController* locNav = (UINavigationController *)segue.destinationViewController;
        LastMinuteLocationViewController * loc = (LastMinuteLocationViewController *) [
        locNav.viewControllers objectAtIndex:0];
        loc.delegate=self;
        NSLog(@"Has users location %d",self.hasUsersLocation);
        loc.hasUsersLocation=self.hasUsersLocation;
    }
}

#pragma mark - LastMinuteLocationDelegate
-(void)didPickerLocation:(LastMinuteLocationItem*)lastMinItem{
    
    NSLog(@"Is current location %d",lastMinItem.isCurrentLocation);
    if (lastMinItem.isCurrentLocation) {
        self.descriptionText.text = [NSString stringWithFormat:@"Find last minute appointments closest to your  %@.",lastMinItem.salon_address3];
        [self.descriptionText boldSubstring:lastMinItem.salon_address3];
    }
    else{
        NSLog(@"%@",[NSString stringWithFormat:@"Find last minute appointments closest to  %@, %@.",lastMinItem.salon_address3,lastMinItem.county_name]);
        self.descriptionText.text = [NSString stringWithFormat:@"Find last minute appointments closest to  %@, %@.",lastMinItem.salon_address3,lastMinItem.county_name];
        [self.descriptionText boldSubstring:[NSString stringWithFormat:@"%@, %@.",lastMinItem.salon_address3,lastMinItem.county_name]];
    }

    
    CLLocation *searchLocation = [[CLLocation alloc] initWithLatitude:[lastMinItem.salon_lat floatValue] longitude:[lastMinItem.salon_lon floatValue]];
    NSLog(@"Search location %@",searchLocation);
    NSLog(@"Lcat %@",lastMinItem.salon_lat);
    self.locationToSearch = searchLocation;

    
}
@end
