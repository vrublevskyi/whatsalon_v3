//
//  SearchResultsVC.swift
//  whatsalon
//
//  Created by admin on 10/5/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit

@objc protocol SearchResultsVCDelegate: class {
    func searchedSalonSelected(_ salon: Salon, on controller: SearchResultsVC)
}
class SearchResultsVC: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var searchView: SearchView! {
        didSet {
            searchView.delegate = self
            searchView.searchIsActive = true
            searchView.searchedlocations = location
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(cellType: SearchResultCell.self)
        }
    }
    
    //MARK: - Properties
    @objc var salons: [Salon]?
    private var filteredSalons: [Salon]?
    @objc var searchedCategory: String?
    @objc var location: String?
    @objc weak var delegate: SearchResultsVCDelegate?
    
    private var filteringIsActive = false
    
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .lightGray


    }

}

extension SearchResultsVC: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteringIsActive {
            return filteredSalons?.count ?? 0
        }
        return salons?.count ?? 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var salon = salons?[indexPath.row]
        if filteringIsActive {
            salon = filteredSalons?[indexPath.row]
        }
        let cell: SearchResultCell = tableView.dequeueReusableCell(for: indexPath)
        cell.address = salon?.salon_address_1
        cell.city = salon?.city_name
        cell.name = salon?.salon_name
        cell.imageURL = salon?.salon_image
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         var salon = salons?[indexPath.row]
        if filteringIsActive  {
            salon = filteredSalons?[indexPath.row]
        }
        
        guard let selectedSalon = salon else {
            return
        }
        self.delegate?.searchedSalonSelected(selectedSalon, on: self)
    }

}

extension SearchResultsVC: SearchViewDelegate {
    func searchByTextTapped(_ text: String, on controller: SearchView) {
        filteringIsActive = false
        if text.count > 0 {
            filteringIsActive = true
            filteredSalons = salons?.filter{($0.salon_name.uppercased().contains(text.uppercased()))}
        }
        tableView.reloadData()
    }
    
    func currentLocationTapped(on controller: SearchView) {
    
    }
    
    func locationTapped(on controller: SearchView) {
        
    }
}
