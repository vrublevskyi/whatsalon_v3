//
//  SalonInfoViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 08/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"

@interface SalonInfoViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *pricesTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedController;
@property (weak, nonatomic) IBOutlet UIView *bioView;
@property (weak, nonatomic) IBOutlet UIView *selectionHeaderView;

@property (nonatomic) NSMutableArray * prices;
@property (nonatomic) Salon * salonInfoData;
- (IBAction)segmentChangValue:(id)sender;

@end
