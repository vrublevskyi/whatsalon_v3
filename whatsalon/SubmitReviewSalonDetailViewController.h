//
//  SubmitReviewSalonDetailViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 22/01/2016.
//  Copyright © 2016 What Applications Ltd. All rights reserved.
//


/*!
 @header SubmitReviewSalonDetailViewController.h
  
 @brief This is the header file for SubmitReviewSalonDetailViewController.h
  
  
 @author Graham Connolly
 @copyright  2016 What Applications Ltd.
 @version    1.0.2
 */
#import <UIKit/UIKit.h>
#import "Salon.h"


@class SubmitReviewSalonDetailViewController;

/*!
     @protocol ReviewSalonDelegate
  
     @brief The ReviewSsalonDelegate protocol
  
     Its the protocol for notifying the delegate that a review has been submited. 
 */
@protocol ReviewSalonDelegate <NSObject>


/*! @brief notifies the delegate that a review has been submitted. */
-(void)reviewsDelegate: (SubmitReviewSalonDetailViewController *) submitReviewSalonViewController didSubmitReviewWithSalonData : (Salon *) salon;

@end
@interface SubmitReviewSalonDetailViewController : UIViewController

/*! @brief represents a Salon object. */
@property (nonatomic) Salon * salonData;

/*! @brief represents a ReviewSalonDelegate object. */
@property (nonatomic) id<ReviewSalonDelegate> delegate;
@end
