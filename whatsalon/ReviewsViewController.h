//
//  ReviewsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 04/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *reviewTableView;
@property (weak,nonatomic) UILabel * reviewServiceLabel;
@property (weak,nonatomic) UILabel * reviewDateLabel;
@property (weak,nonatomic) UILabel * reviewUserLabel;
@property (weak, nonatomic) IBOutlet UIView *tapView;//the view to tap to dismiss
@property (weak,nonatomic) UIImageView * starRating;
@end
