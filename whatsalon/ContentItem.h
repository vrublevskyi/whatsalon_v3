//
//  ContentItem.h
//  whatsalon
//
//  Created by Graham Connolly on 22/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentItem : NSObject

/*! @brief represents the page id. */
@property (nonatomic) NSString * pageId;

/*! @brief represents the page title. */
@property (nonatomic) NSString * padeTitle;

/*! @brief represents the page description. */
@property (nonatomic) NSString * pageDescription;

@end
