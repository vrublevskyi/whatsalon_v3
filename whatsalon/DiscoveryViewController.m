//
//  DiscoveryViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 12/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "DiscoveryViewController.h"
#import "DiscoveryImageTableViewCell.h"
#import "DiscoveryServicesTableViewCell.h"
#import "SalonTableViewCell.h"
#import "RESideMenu.h"
#import "DiscoveryTableViewCell.h"
#import "NSString+Validations.h"
#import "Harpy.h"
#import "WalkthroughViewController.h"
#import "SettingsViewController.h"
#import "DiscoveryItem.h"
#import "FavouriteViewController.h"
#import "SearchSalonsTableViewController.h"
#import "BrowseViewController.h"
//#import <Crashlytics/Crashlytics.h>
#import "INTULocationManager.h"
#import "UIView+AlertCompatibility.h"
#import "LastMinuteSearchViewController.h"
#import "User.h"
#import "SearchSalonsFilterViewController.h"
#import "GCNetworkManager.h"

@interface DiscoveryViewController ()<UITableViewDataSource,UITableViewDelegate,WalkthroughDelegate,GCNetworkManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) UISearchBar * searchBar;
@property (weak, nonatomic) IBOutlet UIView *searchBarView;

@property (nonatomic) NSMutableArray * discoveryItems;
- (IBAction)menuButton:(id)sender;
- (IBAction)searchButton:(id)sender;
- (IBAction)toggleSearch:(id)sender;

@property (nonatomic) UIScrollView * scroller;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
- (IBAction)search:(id)sender;

@property (nonatomic) GCNetworkManager *manager;

@end

NSString * kNearbyURL=@"http://sms.whatsalon.com/salon_images/15/City_Wax_6.jpg";
NSString *kCorkURL=@"http://sms.whatsalon.com/salon_images/16/All_Dolled_Up_1.JPG";
NSString *kDublinURL=@"http://sms.whatsalon.com/salon_images/25/Simply_Elegant_2.JPG";
NSString *kFavURL=@"http://sms.whatsalon.com/salon_images/16/All_Dolled_Up_6.JPG";
@implementation DiscoveryViewController

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [WSCore transparentNavigationBarOnView:self];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    /*
    [WSCore statusBarColor:kBlack];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.tintColor=self.view.tintColor;
     */
    
    [WSCore setDefaultNavBar:self];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"User id %@",[[User getInstance] fetchAccessToken]);
    //removes back button title
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess) {
                                                 // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                 // currentLocation contains the device's current location.
                                                 NSLog(@"Current Location %f %f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
                                             }
                                             else if (status == INTULocationStatusTimedOut) {
                                                 // Wasn't able to locate the user with the requested accuracy within the timeout interval.
                                                 // However, currentLocation contains the best location available (if any) as of right now,
                                                 // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
                                                 
                                             }
                                             else {
                                                 // An error occurred, more info is available by looking at the specific status returned.
                                                 
                                                 switch (status) {
                                                     case INTULocationStatusServicesDenied:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"You have denied WhatSalon access to location services. In order to use this app, please accept access. " cancelButtonTitle:@"OK"];
                                                         break;
                                                    case INTULocationStatusServicesRestricted:
                                                         
                                                          [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location services are restricted for WhatSalon. e.g (parental controls corporate policy, etc)" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                    case INTULocationStatusServicesDisabled:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                    case INTULocationStatusError:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"An error occurred while using the system location services." cancelButtonTitle:@"OK"];
                                                         break;
                                                     default:
                                                         break;
                                                 }
                                                 
                                             }
                                             NSLog(@"Status %ld",(long)status);
                                             
                                         }];
    
    
    //show navigation bar - hidden in TabBarController
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    //ensure navigation bar is visible - it was made invisible in `BrowseViewController`
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    [[Harpy sharedInstance] checkVersion];
    [WSCore statusBarColor:StatusBarColorBlack];
    
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
//    [self.tableView registerClass:[DiscoveryTableViewCell class] forCellReuseIdentifier:@"Cell"];

    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView setSeparatorColor:[UIColor whiteColor]];
    [self.navigationController setNavigationBarHidden:NO];
    
    self.tableView.backgroundColor=[UIColor clearColor];
    
    
    UIImage *image = self.searchButton.imageView.image;
    [self.searchButton setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.searchButton setTintColor:[self.view tintColor]];
    
    
    self.searchButton.userInteractionEnabled=NO;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToSearch)];
    tap.numberOfTapsRequired=1;
    [self.searchBarView addGestureRecognizer:tap];
    self.searchBarView.userInteractionEnabled=YES;
    
    self.title=@"Discover";
    
    if ([WSCore showWalkthrough]) {
        [self performSelector:@selector(showWalkthroughView
                                        ) withObject:self afterDelay:0.5];
    }
    else{
        //perform discovery
        self.manager = [[GCNetworkManager alloc] init];
        self.manager.delegate=self;
        self.manager.parentView=self.view;
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Discovery_Objects_URL]];
        [self.manager loadWithURL:url WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    }
    
     
}

#pragma mark - GCNetworkManager delegate
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"FAil %@",jsonDictionary);
}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Success %@",jsonDictionary);
    self.discoveryItems = [NSMutableArray array];
    for (id key in jsonDictionary[@"message"]) {
        
        DiscoveryItem *disc = [[DiscoveryItem alloc] init];
        
        if (key[@"title"] !=[NSNull null]) {
            disc.title = key[@"title"];
        }
        if (key[@"image_url"]!=[NSNull null]) {
            disc.imageURL = key[@"image_url"];
        }
        if (key[@"longitude"]!=[NSNull null]) {
            disc.lon =  [NSString stringWithFormat:@"%@",key[@"longitude"] ];
        }
        if (key[@"latitude"]!=[NSNull null]) {
            disc.lat = [ NSString stringWithFormat:@"%@", key[@"latitude"] ];
        }
        if (key[@"sort_id"]!=[NSNull null]) {
            disc.sort_id = [ NSString stringWithFormat:@"%@", key[@"sort_id"] ];
        }
        if (key[@"type"]!=[NSNull null]) {
            disc.type = key[@"type"];
        }
        if (key[@"subtitle"]!=[NSNull null]) {
            disc.subtitle = key[@"subtitle"];
        }
        
        [self.discoveryItems addObject:disc];
    }
    [self.tableView reloadData];
    
    
}

-(void)showWalkthroughView{
    if ([WSCore showWalkthrough]) {
        
        WalkthroughViewController * wVC = [self.storyboard instantiateViewControllerWithIdentifier:@"walkthroughVC"];
        wVC.myDelegate=self;
        [self presentViewController:wVC animated:YES completion:nil];
        
        [WSCore walkthroughHasBeenShown];
        
    }

}
-(void)goToSearch{
    
    SearchSalonsFilterViewController * searchFiler = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchFilterVC"];
    //SearchSalonsTableViewController * searchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVC"];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
     /*
    [self.navigationController pushViewController:searchVC animated:NO];
     */
    [self.navigationController pushViewController:searchFiler animated:NO];
}
#pragma walkthrough delegate
-(void)goToLoginSignUp{
    
    SettingsViewController * settings = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.0;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:settings animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate & Datasoure
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // UITableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
 
    DiscoveryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
            [cell setUpDiscoveryCell:[self.discoveryItems objectAtIndex:indexPath.row] AndIsSalonsNearMe:YES];
            
            break;
            
        default:
            
            [cell setUpDiscoveryCell:[self.discoveryItems objectAtIndex:indexPath.row] AndIsSalonsNearMe:NO];
            break;
    }
    
        return cell;

    
    return nil;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.discoveryItems.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
  
    return 270;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DiscoveryItem * disc = [self.discoveryItems objectAtIndex:indexPath.row];
    NSLog(@"\n\n Title: %@ \n lat %@ \n lon %@ \n\n",disc.title,disc.lat,disc.lon );
    
    if ([disc.type intValue]== 1) {
        FavouriteViewController *fvc =[self.storyboard instantiateViewControllerWithIdentifier:@"favVC"];
        [self.navigationController pushViewController:fvc animated:YES];
    
    }
    else if([disc.type intValue]==3){
        NSLog(@"Last Minute");
        [self performSegueWithIdentifier:@"searchDiscovery" sender:self];
    }else if([disc.type intValue] ==4){
        [self performSegueWithIdentifier:@"experiencesSegue" sender:self];
        
    }
    else{
       [self performSegueWithIdentifier:@"discoveryToBrowse" sender:self]; 
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"discoveryToBrowse"]) {
        
        
        NSIndexPath * selectedIndexPath = [self.tableView indexPathForSelectedRow];
        DiscoveryItem * disc = [self.discoveryItems objectAtIndex:selectedIndexPath.row];
        BrowseViewController * browse = segue.destinationViewController;
        browse.browseTitle=disc.subtitle;
        if  ([disc.type intValue]==2) {
            
            
            if ([disc.lat intValue]!=0 && [disc.lon intValue] !=0) {
                browse.hasDiscoveryLatLong=YES;
                browse.discoveryLat = disc.lat;
                browse.discoveryLon = disc.lon;
               
            }
            //DiscoveryItem * disc = [self.discoveryItems objectAtIndex:selectedIndexPath.row];
            //pass in the lat and lon
            
        }
        
        
        
    }
    
}
- (IBAction)menuButton:(id)sender {
    
    [self.sideMenuViewController presentMenuViewController];

}

- (IBAction)searchButton:(id)sender {
}

- (IBAction)toggleSearch:(id)sender {
    
      

}

- (IBAction)search:(id)sender {
}


@end
