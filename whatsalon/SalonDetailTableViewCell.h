//
//  SalonDetailTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 01/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalonDetailTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel * label;
@property (nonatomic,strong) UILabel * detailLabel;

@end
