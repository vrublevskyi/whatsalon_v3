//
//  SalonPreviewViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 14/10/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header SalonPreviewViewController.h
  
 @brief This is the header file for the SalonPreviewViewController.
  
 The SalonPreviewViewController is the view that is launched through 3D touch (Peek & pop) when selecting the salon cell.
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.5
 */

#import <UIKit/UIKit.h>
#import "Salon.h"



@class SalonPreviewViewController;
/*!
     @protocol SalonPreviewDelegate
  
     @brief The SalonPreviewDelegate protocol
  
     It's a protocol used on the controller that implementes the 3D touch (peek & pop) when selecting the salon cell */
@protocol  SalonPreviewDelegate <NSObject>

@optional

/*! @brief handles the action of selecting the booking/call action item on the preview controller.
 
    @param preview SalonPreviewViewController
 
    @param action UIPreviewAction
 
    @param salon Salon
 */
-(void)salonPreviewViewController: (SalonPreviewViewController *) preview didSelectMakeBookingOrCallActionItem: (UIPreviewAction *) action WithSalon:(Salon *)salon;

/*!
 @brief handles the actions of selecting the see gallery action item on the preview view controller.
 
 @param preview SalonPreviewViewController
 
 @param action UIPreviewAction
 
 @param salon Salon
 */
-(void)salonPreviewViewController: (SalonPreviewViewController *) preview didSelectSeeGalleryActionItem: (UIPreviewAction *) action WithSalon: (Salon *)salon;

/*!
 @brief handles the actions of selecting the read reviews action item on the preview view controller.
 
 @param preview SalonPreviewViewController
 
 @param action UIPreviewAction
 
 @param salon Salon
 */
-(void)salonPreviewViewController: (SalonPreviewViewController *) preview didSelectReadReviewsActionItem: (UIPreviewAction *) action WithSalon: (Salon *) salon;

@end
@interface SalonPreviewViewController : UIViewController

/*! @brief the salon object */
@property (nonatomic) Salon * salonData;

/*! @brief SalonPreviewDelegate object */
@property (nonatomic) id <SalonPreviewDelegate> delegate;
@end
