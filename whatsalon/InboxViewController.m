//
//  InboxViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 26/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "InboxViewController.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "RESideMenu.h"
#import "Inbox.h"
#import "InboxTableViewCell.h"
#import "InboxContentViewController.h"


@interface InboxViewController ()<UITableViewDataSource,UITableViewDelegate,GCNetworkManagerDelegate>

/*! @brief represents the tableview. */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/*! @brief represents the inbox array. */
@property (nonatomic) NSMutableArray * inboxListArray;

/*! @brief represents the network manager for making network requests. */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief shows the menu. */
- (IBAction)showMenu:(id)sender;

/*! @brief cancels the view. */
- (IBAction)cancel:(id)sender;

@end

@implementation InboxViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore nonTransparentNavigationBarOnView:self];
    [WSCore changeNavigationTitleColor:[UIColor blackColor] OnViewController:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.inboxListArray = [NSMutableArray array];
    self.title=@"Inbox";
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.parentView=self.view;
    self.networkManager.delegate=self;
    
    /*
     //the network based inbox
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kUser_Inbox_URL]];
    NSString * params =[NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    */
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    [self.tableView registerClass:[InboxTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    self.tableView.backgroundView=nil;
    self.tableView.backgroundColor=[UIColor clearColor];
    
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    footerView.backgroundColor=[UIColor clearColor];
    self.tableView.tableFooterView=footerView;
    
    
    
    
    Inbox * firstEmail = [[Inbox alloc] initWithInboxID:@"0"];
    firstEmail.content = @"Thanks for downloading WhatSalon!  We will send notifications, reminders and anything else we think you would need to this inbox.";
    firstEmail.from = @"WhatSalon";
    firstEmail.recieved = @"";
    firstEmail.subject = @"You’ve found the new WhatSalon Inbox!";
    firstEmail.to = @"";
    firstEmail.imageName = @"inbox_image.jpg";
 
    [self.inboxListArray addObject:firstEmail];
    [self.tableView reloadData];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GCNetworkManagerDelegate and Datasource
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    
    NSArray * inboxArray = jsonDictionary[@"message"];
    for (NSDictionary *iDict in inboxArray) {
       
        Inbox * inbox = [Inbox inboxWithInboxID:iDict[@"inbox_id"]];
        inbox.content = iDict[@"content"];
        inbox.from = iDict[@"from"];
        inbox.recieved = iDict[@"recieved"];
        inbox.subject = iDict[@"subject"];
        inbox.to = iDict[@"to"];
        
        
        [self.inboxListArray addObject:inbox];
        
    }
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate & Datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InboxTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Inbox * inbox = [self.inboxListArray objectAtIndex:indexPath.row];
    [cell setUpCellWithInbox:inbox];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kTwoThirdsCellHeight;
   
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.inboxListArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Inbox * item = [self.inboxListArray objectAtIndex:indexPath.row];
    
    InboxContentViewController *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"inboxDetailVC"];
//    detailVC.inboxItem=item;
    [self.navigationController pushViewController:detailVC animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showMenu:(id)sender {
    
    [self.sideMenuViewController presentMenuViewController];
}

/*
 Cancel button used for right menu/tab storyboard
 */
- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
