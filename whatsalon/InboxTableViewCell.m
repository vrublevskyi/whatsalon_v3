//
//  InboxTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 26/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "InboxTableViewCell.h"

@implementation InboxTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.titleLabel = [[UILabel alloc] init];
        self.contentLabel = [[UILabel alloc] init];
        self.contentView.backgroundColor=[UIColor clearColor];
        self.backgroundColor=[UIColor clearColor];
        
        self.contentView.layer.masksToBounds=YES;
        self.layer.masksToBounds=YES;
        self.contentView.layer.cornerRadius=kCornerRadius;
        
               }
    return self;
}

-(void)setUpCellWithInbox: (Inbox *) inbox{
    
    //content holder
    UIView * contentViewHolder = [[UIView alloc] initWithFrame:CGRectMake(10, 20, self.frame.size.width-20, self.frame.size.height-25)];
    contentViewHolder.layer.cornerRadius=kCornerRadius;
    contentViewHolder.layer.masksToBounds=YES;
    contentViewHolder.layer.borderColor=[UIColor silverColor].CGColor;
    contentViewHolder.layer.borderWidth=0.5f;
    contentViewHolder.backgroundColor=[UIColor clearColor];
    [self.contentView addSubview:contentViewHolder];
   
    
    //image holder
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, contentViewHolder.frame.size.width, 180)];
    imageView.image=[UIImage imageNamed:inbox.imageName];
    [contentViewHolder addSubview:imageView];
    imageView.layer.masksToBounds=YES;
    imageView.contentMode=UIViewContentModeScaleAspectFill;
    imageView.layer.cornerRadius=kCornerRadius;
    
    //background view to hold header view and
    UIView * backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, imageView.frame.size.height, contentViewHolder.frame.size.width, contentViewHolder.frame.size.height-imageView.frame.size.height-2)];
    backgroundView.backgroundColor = [UIColor cloudsColor];
    [contentViewHolder addSubview:backgroundView];
    backgroundView.layer.cornerRadius=kCornerRadius;
    backgroundView.layer.masksToBounds=YES;
    
    UIView * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, backgroundView.frame.size.width, 60)];
    headerView.backgroundColor=[UIColor silverColor];
    [backgroundView addSubview:headerView];
    
    self.titleLabel.frame = CGRectMake(10, 10, headerView.frame.size.width-20, 50);
    self.titleLabel.numberOfLines=0;
    self.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    inbox.subject =[inbox.subject stringByReplacingOccurrencesOfString:@" [WS]" withString:@""];
    self.titleLabel.text = inbox.subject;
    self.titleLabel.textAlignment=NSTextAlignmentCenter;
    [headerView addSubview:self.titleLabel];
    self.titleLabel.center = headerView.center;
    self.titleLabel.adjustsFontSizeToFitWidth=YES;
    
    self.contentLabel.frame = CGRectMake(10, headerView.frame.size.height+10, backgroundView.frame.size.width-20,backgroundView.frame.size.height-headerView.frame.size.height-10);
    inbox.content = [inbox.content stringByReplacingOccurrencesOfString:@" [WS]" withString:@""];
    inbox.content = [inbox.content stringByReplacingOccurrencesOfString:@"\n \n \n" withString:@"\n\n"];
    self.contentLabel.text=inbox.content;
    self.contentLabel.numberOfLines=0;
    self.contentLabel.textAlignment=NSTextAlignmentLeft;
    self.contentLabel.font = [UIFont systemFontOfSize:14.0f];
    self.contentLabel.lineBreakMode = NSLineBreakByClipping;
    self.contentLabel.layer.masksToBounds=YES;
    
    [backgroundView addSubview:self.contentLabel];
    
    if (!self.hasShadow) {
        NSLog(@"Add shadow");
        [WSCore floatingView:self WithAlpha:0.5];
        self.hasShadow=YES;

    }
    
}
@end
