//
//  LastMinuteLocationItem.h
//  whatsalon
//
//  Created by Graham Connolly on 13/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LastMinuteLocationItem : NSObject

@property (nonatomic) NSString * city_lat;
@property (nonatomic) NSString * city_lon;
@property (nonatomic) NSString * city_name;
@property (nonatomic) NSString * country_name;
@property (nonatomic) NSString * county_id;
@property (nonatomic) NSString * county_latitude;
@property (nonatomic) NSString *county_longitude;
@property (nonatomic) NSString *county_name;
@property (nonatomic) BOOL google_success;//if google_success is no then dont add to array
@property (nonatomic) NSString *salon_address1;
@property (nonatomic) NSString * salon_address2;
@property (nonatomic) NSString * salon_address3;
@property (nonatomic) NSString * salon_lat;
@property (nonatomic) NSString * salon_lon;
@property (nonatomic) BOOL isCurrentLocation;

-(instancetype) initWithSalonAddress3 : (NSString *) salon_address_3;
+(instancetype) locationWithSalonAddress3:(NSString *)salon_address_3;
@end
