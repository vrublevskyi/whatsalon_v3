//
//  StyleObject.h
//  whatsalon
//
//  Created by Graham Connolly on 04/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StyleObject : NSObject

@property (nonatomic) NSString * styleTypeId;
@property (nonatomic) NSString * styleTypeName;

@end
