//
//  TabSearchSalonsFilterViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 28/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabSearchSalonsFilterViewController.h"
#import "TabSearchSalonsTableViewController.h"

@interface TabSearchSalonsFilterViewController ()<UITableViewDataSource,UITableViewDelegate,SearchDelegate>

/*! @brief represents the array of services. */
@property (nonatomic) NSMutableArray * servicesArray;

/*! @brief represents the category string. */
@property (nonatomic) NSString * catString;

/*! @brief represents the category ID. */
@property (nonatomic) NSString * catID;

/*! @brief determines the search button is pressed. */
@property (nonatomic) BOOL isSearchPressed;


/*! @brief represents the background tint image. */
#define kBackgroundTintForSearch [UIColor colorWithWhite:1.0 alpha:0.9]


@end

@implementation TabSearchSalonsFilterViewController

/*
 ViewDidLayoutSubviews
 
 ** Autolayout not implemented for this screen because it would effect the animation when hiding and showing the side menu **
 
    set navBarView frame origin y to include status bar height
    sets the height to 73
 
    adjusts the tableview frame origin y to include navBarView height and kStatusBarHeight
 
 
 */
-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    CGRect frame = self.view.frame;
    frame.origin.y = self.view.frame.origin.y+kStatusBarHeight;
    frame.size.height=73;
    self.navBarView.frame =frame;
   
    
    CGRect tableFrame = self.tableView.frame;
    tableFrame.origin.y = self.navBarView.frame.size.height + kStatusBarHeight;
    tableFrame.size.height = tableFrame.size.height-(self.navBarView.frame.size.height+kStatusBarHeight);
    self.tableView.frame =tableFrame;
    
}

/*
 viewDidDisappear
 resets values
 
 sets catId, catString to empty strings
 sets isSearchPressed= NO
 */
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    self.catID=@"";
    self.catString=@"";
    self.isSearchPressed=NO;
    
    
    
}
#pragma tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.servicesArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [self.servicesArray objectAtIndex:indexPath.row];
    
    //Sets cell images based on textLabel.text
    if ([cell.textLabel.text isEqualToString:@"Body"]) {
        cell.imageView.image = [UIImage imageNamed:@"body_service_icon"];
    }
    else if ([cell.textLabel.text isEqualToString:@"Face"]) {
        cell.imageView.image = [UIImage imageNamed:@"face_service_icon"];
        
    }else if ([cell.textLabel.text isEqualToString:@"Hair"]) {
        cell.imageView.image = [UIImage imageNamed:@"hair_service_icon"];
        
    }else if ([cell.textLabel.text isEqualToString:@"Hair Removal"]) {
        cell.imageView.image = [UIImage imageNamed:@"wax_service_icon"];
        
    }else if ([cell.textLabel.text isEqualToString:@"Massage"]) {
        cell.imageView.image = [UIImage imageNamed:@"massage_service_icon"];
        
    }
    else if ([cell.textLabel.text isEqualToString:@"Nails"]) {
        
        cell.imageView.image = [UIImage imageNamed:@"nail_service_icon"];
        
    }
    
    //tint cell image
    cell.imageView.image = [cell.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.imageView setTintColor:kWhatSalonBlue];
    
    
    cell.backgroundView=nil;
    cell.backgroundColor = kBackgroundTintForSearch;
    cell.textLabel.backgroundColor=[UIColor clearColor];
    
    [WSCore addBottomIndentedLine:cell :[UIColor asbestosColor]];
    
    return cell;
}


/*
 didSelectRowAtIndexPath
 
 sets catId based on the index pressed + 1 (+1 because category ids on the server start at 1 instead of 0)
 set catString
 
 go toSearchPage
 
 deselect cell - deselects cell so that the screen is reset when returning
 
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    self.catID = [NSString stringWithFormat:@"%@",@(indexPath.row+1)];
    NSLog(@"Cat %@",self.catID);
    self.catString = [self.servicesArray objectAtIndex:indexPath.row];
    
    [self toSearchPage];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.searchHolder.layer.cornerRadius=kCornerRadius;
    self.searchHolder.userInteractionEnabled=YES;
    self.searchText.userInteractionEnabled=NO;
    self.searchImage.userInteractionEnabled=NO;
    self.navBarView.backgroundColor=kWhatSalonBlue;
    [WSCore transparentNavigationBarOnView:self];
    
    UITapGestureRecognizer * searchPageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchPressed)];
    searchPageGesture.numberOfTapsRequired=1;
    [self.searchHolder addGestureRecognizer:searchPageGesture];
    
   
    //sets navigationController text attributes
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor whiteColor],NSForegroundColorAttributeName,
                                nil];
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    self.servicesArray = [NSMutableArray arrayWithObjects:@"Hair",@"Hair Removal",@"Nails",@"Massage",@"Face",@"Body", nil];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    self.view.backgroundColor=kBackgroundColor;
    self.tableView.backgroundColor=[UIColor clearColor];
    self.tableView.backgroundView=nil;
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView=footerView;
    
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    CGRect frame = self.view.frame;
    frame.origin.y = kStatusBarHeight;
    frame.size.height= frame.size.height- kStatusBarHeight;
    UIImageView * backgroundView = [[UIImageView alloc] initWithFrame:frame];
    backgroundView.image = [UIImage imageNamed:kSearch_placeholder_Image_Name];
    UIView * trans = [[UIView alloc] initWithFrame:self.view.frame];
    
    trans.backgroundColor = kGoldenTintForOverView;
    [backgroundView addSubview:trans];
    [self.view insertSubview:backgroundView atIndex:0];
    
    self.tableView.scrollEnabled=NO;
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    self.navBarView.backgroundColor=kBackgroundTintForSearch;
    self.searchHolder.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.8];
   
    self.searchText.textColor = [UIColor blackColor];
    self.searchImage.image = [self.searchImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.searchImage.tintColor = [UIColor asbestosColor];
    
}

/*
 viewDidAppear 
 ensures navigation bar is hidden
 
 ****  2/10/2015 ****
 removed for the moment as Im not sure why it was in place
 
 */
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
}
/*
 viewWillAppear
 ensures navigation bar is hidden
 sets status bar color to black
 
 */
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden=YES;
    
    [WSCore statusBarColor:StatusBarColorBlack];
}

/*
 searchPressed
 
 UItapgestureRecognizer action
 
 sets isSearchPressed=YES
 perfomr toSearchPage
 
 */
-(void)searchPressed{
    self.isSearchPressed=YES;
    [self toSearchPage];
}

/*
 toSearchPage
 sets hidden tabController tabBar to hidden
 sets BookingType to BookingTypeSearch - helps to determine the uwnwind segue
 
 performs fade transition to TabSearchSalonsTableViewController
 hides bottom bar when pushed
 */
-(void)toSearchPage{
   
    self.tabBarController.tabBar.hidden=YES;
    
    [WSCore setBookingType:BookingTypeSearch];
    TabSearchSalonsTableViewController * searchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVC"];
    searchVC.catString =self.catString;
    searchVC.catID = self.catID;
    searchVC.wasSearchPressed=self.isSearchPressed;
    searchVC.delegate =self;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:searchVC animated:NO];
    self.navigationController.hidesBottomBarWhenPushed=NO;
}

#pragma mark - SearchDelegate methods
/*
 didDismissSearchSalonsTable
 sets tabbar to hidden to NO
 */
-(void)didDismissSearchSalonsTable{
    self.tabBarController.tabBar.hidden=NO;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






#pragma mark - UNWIND Segue
/*
 unwindToTabSearch
 
 Following a booking - when the users presses cancel, the unwind segue is performed
 resets the booking type 
 sets tabbarcontroller tab to not hidden
 */
- (IBAction)unwindToTabSearch:(UIStoryboardSegue *)unwindSegue
{
    
    self.tabBarController.tabBar.hidden=NO;
    [WSCore resetBookingType];
}

@end
