//
//  NSString+VersionNumber.h
//  whatsalon
//
//  Created by Graham Connolly on 22/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VersionNumber)

- (NSString *)shortenedVersionNumberString;
@end
