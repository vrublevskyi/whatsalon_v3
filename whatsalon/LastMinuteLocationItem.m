//
//  LastMinuteLocationItem.m
//  whatsalon
//
//  Created by Graham Connolly on 13/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "LastMinuteLocationItem.h"

@implementation LastMinuteLocationItem

-(instancetype) initWithSalonAddress3:(NSString *)salon_address_3{
    self = [super init];
    if (self) {
        self.salon_address3= salon_address_3;
     
    }
    
    return self;
}

+(instancetype)locationWithSalonAddress3:(NSString *)salon_address_3{
    return [[self alloc]  initWithSalonAddress3:salon_address_3];
}
@end
