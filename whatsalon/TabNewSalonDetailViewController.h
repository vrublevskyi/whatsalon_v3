//
//  TabNewSalonDetailViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 02/09/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header TabNewSalonDetailViewController.h
  
 @brief This is the header file for TabNewSalonDetailViewController.
  
 'New' is used because there was a 'SalonDetailViewController' but it was not dynamic enough.
 @author Graham
 @copyright  2015 What Applications Ltd
 @version    1.8+
 */
#import <UIKit/UIKit.h>
#import "Salon.h"
#import "LastMinuteItem.h"


/*!
     @protocol TabSalonDetailIndividualDelegate
  
     @brief The TabSalonDetailIndividualDelegate protocol
  
     The protocol is used to notify the delegate to update its appearance
 */
@protocol TabSalonDetailIndividualDelegate <NSObject>

@optional

/*! @brief notifies the delegate that the salon has been favourited.
 
    @param salon Salon object.
 */
-(void)didFavouriteSalonOnDetailView: (Salon*) salon;

/*!
 @brief notifies the delegate that the salon has been unfavourited.
 
 @param salon Salon object.
 */
-(void)didUnFavouriteSalonOnDetailView: (Salon *)salon;

/*!
 @brief notifies the delegate that the salon array should be updated with the updated salon object.
 
 @param salon Salon object
 */
-(void)refreshSalonObjectsWithUpdatedSalonObject: (Salon *) salon;

@end
@interface TabNewSalonDetailViewController : UIViewController


/*! @brief represents the table view. */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/*! @brief represents the Salon object. */
@property (nonatomic) Salon* salonData;

/*! @brief determines if the presenting view controller was the favourite view controller. */
@property (nonatomic) BOOL isFromFavourite;

/*! @brief determines if the present view controller is from the search filter view controller and should later unwind to the search filter screen. */
@property (nonatomic) BOOL unwindBackToSearchFilter;


/*! @brief represents the TabSalonDetailIndividualDelegate. */
@property (nonatomic,assign) id<TabSalonDetailIndividualDelegate> myDelegate;
@end
