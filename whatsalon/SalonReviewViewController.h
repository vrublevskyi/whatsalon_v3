//
//  SalonReviewViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 07/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//


/*!
 @header SalonReviewViewController.h
  
 @brief This is the header file for the SalonReviewViewController.
 
 ****** PLEASE USE SUBMITREVIEWCONTROLLER FROM NOW ON FOR REVIEWS *******
  
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.3
 */

#import <UIKit/UIKit.h>
#import <EDStarRating/EDStarRating.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "MyBookings.h"

/*!
     @protocol ReviewDelegate
  
     @brief The ReviewDelegate protocol
  
     
 */
@protocol ReviewDelegate <NSObject>
@optional

/*! @brief notifies the delegate to reload the bookings table. */
-(void) reloadBookings;

/*! @brief notifies the delegate that a review has been added. */
-(void) addedReview;

@end

@interface SalonReviewViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, EDStarRatingProtocol, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

/*! @brief represnts the stylists name. */
@property (strong, nonatomic) UILabel *stylistName;

/*! @brief represents the salons names. */
@property (strong, nonatomic) UILabel *salonName;

/*! @brief represents the submit button. */
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

/*! @brief represents the tableview. */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/*! @brief represnts the star ratings view. */
@property (weak,nonatomic)  EDStarRating * starRatingsView;

/*! @brief represents the stylists rating view. */
@property (weak,nonatomic)  EDStarRating * stylistRatingView;

/*! @brief represents the salons rating that has been given. */
@property (copy,nonatomic) NSString * salonRating;

/*! @brief represents the stylists rating that has been give. */
@property (copy,nonatomic) NSString * styleRating;

/*! @brief represents the text view for addeding the description. */
@property (strong,nonatomic) UITextView * descTextView;

/*! @brief represents the UIImagePickerController. */
@property (strong,nonatomic) UIImagePickerController * imagePicker;

/*! @brief represents the confirmation id. */
@property (nonatomic) NSString *confirmID;

/*! @brief represents the MyBookings object. */
@property (nonatomic) MyBookings *booking;


/*! @brief represents the ReviewDelegate. */
@property (nonatomic,retain) id<ReviewDelegate> myDelegate;

/*! @brief the action for adding a photo. */
- (IBAction)addPhoto:(id)sender;

/*! @brief the action for submitting information. */
- (IBAction)submit:(id)sender;

/*! @brief the action for showing information. */
- (IBAction)showInfo:(id)sender;


@end
