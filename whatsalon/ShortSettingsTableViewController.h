//
//  ShortSettingsTableViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 27/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShortSettingsTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *debugLabel;

@end
