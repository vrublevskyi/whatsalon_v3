//
//  TwilioVerifiyViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 18/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header TwilioVerifiyViewController.h
  
 @brief This is the header file for the TwilioVerifiyViewController.
  
  
 @author Graham Connolly
 @copyright  2015 WhatApplication Ltd.
 @version    -
 */
#import <UIKit/UIKit.h>
#import "FUIButton.h"
#import "UIColor+FlatUI.h"
#import "FUITextField.h"


/*!
     @protocol TwilioDelegate
  
     @brief The TwilioDelegate protocol
  
     It's a protocol used to notify the delegate that a change should be made to its appearance/process.
 */
@protocol TwilioDelegate <NSObject>

@optional

/*! @brief it is called when Twilio has been verified. */
-(void)twilioWasVerified;

/*! @brief it is called when Twilio has been cancelled. */
-(void)twilioCanceled;

@end
@interface TwilioVerifiyViewController : UIViewController

/*! @brief represents the TwilioDelegate. */
@property (nonatomic,assign) id<TwilioDelegate> delegate;

/*! @brief represents the Navigation bar. */
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

/*! @brief represents the cancel action. */
- (IBAction)cancel:(id)sender;

/*! @brief represents the label for holding the description. */
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;

/*! @brief represents the text field for the mobile number. */
@property (weak, nonatomic) IBOutlet FUITextField *mobileNumberTextField;

/*! @brief represents the text field for the verification code. */
@property (weak, nonatomic) IBOutlet FUITextField *verificationCode;

/*! @brief represents the submit button. */
@property (weak, nonatomic) IBOutlet FUIButton *submitButton;

/*! @brief represents the submit action. */
- (IBAction)submit:(id)sender;

/*! @brief represents the verify action. */
- (IBAction)verify:(id)sender;

/*! @brief represents the verify button. */
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;



@end
