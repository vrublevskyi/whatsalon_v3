//
//  WalletViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 25/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"

@interface WalletViewController : UIViewController

/*! @brief represents the tableview. The wallet view controller once contained other options than the credit card. */
@property (weak, nonatomic) IBOutlet UITableView *tableview;

/*! @brief represents the header view. */
@property (weak, nonatomic) IBOutlet UIView *headerView;

/*! @brief represents the card button. */
@property (nonatomic) FUIButton *addCardButton;

/*! @brief determines if the WalletViewController is being presenting from the right side menu. */
@property (nonatomic) BOOL hasRightMenu;


@end
