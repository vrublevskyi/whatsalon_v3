//
//  SelectServicesViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 08/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SelectServicesViewController.h"
#import "SalonService.h"
#import "ServicesTableViewCell.h"
#import "FriendlyService.h"

@interface SelectServicesViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *serviceHolder;
@property (weak, nonatomic) IBOutlet UILabel *titleHeader;
@property (nonatomic) UIDynamicAnimator * animator;

@end

@implementation SelectServicesViewController


#pragma mark - TableView

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ServicesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.currencySymbol = self.salonObj.currencySymbolUtf8;//added for currency symbol handling
    
    
    if (self.isFromLastMinute) {
        
        FriendlyService * fServ = [self.servicesArray objectAtIndex:indexPath.row];
        [cell setUpServicesTableViewCellWithPhorestService:fServ];
        
    }
    else{
        
        SalonService * pServ = [self.servicesArray objectAtIndex:indexPath.row];
        [cell setUpServicesTableViewCellWithPhorestService:pServ];

    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView=nil;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [WSCore popOutViewAnimationWithView:self.contentHolder];
    
    if (self.isFromLastMinute) {
        
        FriendlyService * fServ = [self.servicesArray objectAtIndex:indexPath.row];
        self.friendlySelectedService = fServ;
        
    }
    else{
        
        SalonService * pServ = [self.servicesArray objectAtIndex:indexPath.row];
       
        self.selectedService=pServ;
        
    }
   
   // [WSCore popOutViewAnimationWithView:self.contentHolder];
    
    if (self.isFromLastMinute) {
        
        [self performSegueWithIdentifier:@"unwindToLastMinute" sender:self];
        
    }
    else{
        
        [self performSegueWithIdentifier:@"unwindWithSelectedService" sender:self];
        
    }
    
    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];

    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.servicesArray.count;
    
}
-(void)cancelAction{
    
    [WSCore popOutViewAnimationWithView:self.contentHolder];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.contentHolder.hidden=NO;
    
    [WSCore popInViewAnimationWithView:self.contentHolder];
    
    
    if (self.isFromLastMinute) {
        
        FriendlyService * fServ = [self.servicesArray objectAtIndex:0];
        
        self.titleHeader.text =[NSString stringWithFormat:@"%@ Services",fServ.friendly_category_title];
 
    }
    else{
        
        SalonService * pServ = [self.servicesArray objectAtIndex:0];
        
        self.titleHeader.text =[NSString stringWithFormat:@"%@ Services",pServ.category_name];

    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    self.titleHeader.textColor = [UIColor peterRiverColor];
    self.titleHeader.font = [UIFont boldSystemFontOfSize:22.0f];
    
    self.view.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.6];
    self.contentHolder.backgroundColor=[UIColor clearColor];
    self.serviceHolder.backgroundColor = [UIColor cloudsColor];
    self.cancel.buttonColor=[UIColor silverColor];
    self.cancel.shadowColor = [UIColor asbestosColor];
    self.cancel.shadowHeight = 1.0f;
    self.cancel.cornerRadius = kCornerRadius;
    
    [self.cancel setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    self.cancel.titleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    [self.cancel addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];

    self.serviceHolder.layer.cornerRadius=6.0;
    self.serviceHolder.layer.masksToBounds=YES;
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;


    //if its not from the last minute controller
    if (!self.isFromLastMinute) {
        
        for (int i=0; i<self.servicesArray.count; i++) {
            
            SalonService * pServ = [self.servicesArray objectAtIndex:i];
            if (pServ.isHeader) {
                
                //remove header objects
                [self.servicesArray removeObject:pServ];
            }
        }
  
    }
            
    
    
    [self.tableView registerClass:[ServicesTableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.tableView reloadData];
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView=footerView;
    
    self.contentHolder.hidden=YES;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
