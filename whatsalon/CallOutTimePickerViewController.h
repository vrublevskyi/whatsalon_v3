//
//  CallOutTimePickerViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 03/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"

@interface CallOutTimePickerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *sunImageView;
@property (weak, nonatomic) IBOutlet UIImageView *moonImageView;
@property (weak, nonatomic) IBOutlet FUIButton *bookButton;

- (IBAction)book:(id)sender;
@end
