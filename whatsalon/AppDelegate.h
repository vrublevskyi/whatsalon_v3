//
//  AppDelegate.h
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

/*!
 @header AppDelegate.h
  
 @brief This is the header file is for the AppDelegate.
  
 This file contains the most important method and properties decalaration in the AppDelegate. 
  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd
 @version    10+
 */

#import <UIKit/UIKit.h>
#import "GCNetworkManager.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate,GCNetworkManagerDelegate,UIAlertViewDelegate>

/*! This BOOL helps determine which application is being compiled, either the Tab applications (current application) or the old <b>3.0</b> utility app version. */
@property (nonatomic) BOOL isTab;

/*! The window that manages and coordinates the views an app displays on a device screen. */
@property (strong, nonatomic) UIWindow *window;
/*! The salon id of the salon to be reviewed */
@property (nonatomic) NSString * reviewSalonID;

-(void)openActiveSessionWithPermissions:(NSArray *)permissions allowLoginUI:(BOOL)allowLoginUI DEPRECATED_MSG_ATTRIBUTE("Old facebook method, please see CGFacebookHelper class from now on");
/*!
 @brief revokes the users Facebook Permissions.
 
 @discussion creates an instance of GCFacebookHelp and invokes @c[<i>object name</i> revokePermissions].
 */
-(void)revokeFacebookPermissions;
@end
