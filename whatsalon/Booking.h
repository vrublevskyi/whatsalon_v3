//
//  Booking.h
//  whatsalon
//
//  Created by Graham Connolly on 05/12/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//


/*!
 @header Booking.h
  
 @brief This is the header file for the Booking object which contains information for making a booking.

  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd.
 @version    -
 */

#import <Foundation/Foundation.h>
#import "IsalonBookingDetails.h"
#import "Salon.h"

@interface Booking : NSObject<NSCoding>

/*! @brief represents the booking salon. */
@property (nonatomic) Salon * bookingSalon;

/*! @brief represents the users id. */
@property (nonatomic) NSString * user_id;

/*! @brief represents the slot id of the booking. */
@property (nonatomic) NSString * slot_id;

/*! @brief represents the room id of the booking. */
@property (nonatomic) NSString * room_id;

/*! @brief represents the internal_start_datetime of the booking. */
@property (nonatomic) NSString * internal_start_datetime;

/*! @brief represents the internal end date time of the booking. */
@property (nonatomic) NSString * internal_end_datetime;

/*! @brief represents the staff id of the booking. */
@property (nonatomic) NSString * staff_id;

/*! @brief represents the service id of the booking. */
@property (nonatomic) NSString * service_id;

/*! @brief represents the appointment reference number. */
@property (nonatomic) NSString * appointment_reference_number;

/*! @brief represents the chosen service for the booking. */
@property (nonatomic) NSString * chosenService;

/*! @brief represents the chosen price for the booking. */
@property (nonatomic) NSString * chosenPrice;

/*! @brief represents the chosen stylist for the booking. */
@property (nonatomic) NSString * chosenStylist;

/*! @brief represents the chosen day for the booking .*/
@property (nonatomic) NSString * chosenDay;

/*! @brief represents the chosen time. */
@property (nonatomic) NSString * chosenTime;

/*! @brief represents the end time of the booking. */
@property (nonatomic) NSString *endTime;

/*! @brief represents the start time of the booking. */
@property (nonatomic) NSString * start_datetime;

/*! @brief determines if a patch test is required. */
@property (nonatomic) BOOL patchTestRequired;

/*! @brief represents the iSalon bookings details. 
    @discussion bookings for iSalon requires different details compared to Phorest and Premier. 
 */
@property (nonatomic) IsalonBookingDetails * isalonBookingDetails;

/*! @brief creates an instancetype of booking using salon object. */
-(instancetype) initWithSalonID :(Salon *)salon;

/*! @brief creates an instancetype of booking using the salon object. */
+(instancetype) bookingWithSalonID: (Salon *) salon;

/*! @brief returns a string ofbooking information. */
-(NSString *)getBookingDescription;
@end
