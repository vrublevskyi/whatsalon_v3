//
//  EditProfileViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 28/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 @brief delegate for EditProfile - used when editing profile from menu
 */

/*!
     @protocol EditProfileDelegate
  
     @brief The EditProfileDelegate protocol
  
     It's a protocol used to notify the delegate that a change has taken place.
 */
@protocol EditProfileDelegate <NSObject>

@optional
/*
 @brief notifies the delegate to update the menu
 */

-(void)updateMenuProfile;

@end
@interface EditProfileViewController : UIViewController<UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

/*!
 @brief represents the EditProfileDelegate
 */
@property (nonatomic,assign) id<EditProfileDelegate> myDelegate;
/*!
 @brief represents the view for the navigation bar. 
 
 @discussion viewForNavigationBar - A navigation bar is not present with a modal view presentation. viewForNavigationBar is a view that will mimic a navigation bar.
 */
@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;

/*!
 @brief represents the UIBarButtonItem for cancel action
 */
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelBarButton;

/*!
 @brief represents the UIBarButtonItem for done action
 */
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButton;

/*!
 @brief represents the placeholder UIImageView for the users profile image or the default camera image.
 */
@property (weak, nonatomic) IBOutlet UIImageView *cameraAvatar;

/*!
 @brief represents a UIView which is place behind a UITextField
 */
@property (weak, nonatomic) IBOutlet UIView *viewForFirstName;

/*!
 @brief represents a UIView which is place behind a UITextField
 */
@property (weak, nonatomic) IBOutlet UIView *viewForSecondName;

/*!
 @brief represents a UIView which is place behind a UISegmentControl
 */
@property (weak, nonatomic) IBOutlet UIView *viewForGender;

/*!
 @brief represents the navigation bar.
 
 @discussion A navigation bar is not present by default when presenting a modal view controller. navigationBar is to mimic the default navigation bar.
 */
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;

/*!
 @brief imagePicker - UIImagePickerController handles the taking of images, or selecting existing images
 */
@property (strong,nonatomic) UIImagePickerController * imagePicker;

/*!
 @brief represents a UITextField for the users first name
 */
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextfield;

/*!
 @brief represents a UITextField for the users surname
 */
@property (weak, nonatomic) IBOutlet UITextField *surNameTextfield;

/*!
 @brief represents a UISegmentedControl for selection of gender
 */
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentedControl;


/*!
 @brief SegmentedControl action for selecting/changing the users gender
 @param sender generic type
 */
- (IBAction)genderChange:(id)sender;

/*!
 @brief represents the cancel edit profile action.
 @discussion cancelEditiProfile - Cancel editing profile. All changes are lost unless saved.
 @param sender generic type
 */
- (IBAction)cancelEditProfile:(id)sender;

/*!
 @brief Used for saving the users profile (saving changes).
 @param sender generic type
 */
- (IBAction)done:(id)sender;

/*!
 @brief determine wether EditProfileView is being presented by the MenuViewController
 */
@property (nonatomic) BOOL isFromMenu;

/*! @brief determins if the view controller is being presented from the right menu. */
@property (nonatomic) BOOL isFromRightMenu;
@end
