//
//  SocialShareViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 20/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SocialShareViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "WhatsAppKit.h"

@interface SocialShareViewController ()<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

/*! @brief represents the Facebook button. */
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;

/*! @brief represents the share with Facebook action. */
- (IBAction)shareWithFacebook:(id)sender;

/*! @brief represents the share with Twitter button. */
@property (strong, nonatomic) IBOutlet UIButton *twitterButton;

/*! @brief represents the share with Twitter action. */
- (IBAction)shareWithTwitter:(id)sender;

/*! @brief represents the WhatsAppButton. */
@property (strong, nonatomic) IBOutlet UIButton *whatsAppButton;

/*! @brief represents the share with WhatsApp action. */
- (IBAction)shareWithWhatsApp:(id)sender;

/*! @brief represents the email button. */
@property (strong, nonatomic) IBOutlet UIButton *emailButton;

/*! @brief represents the share with email action. */
- (IBAction)shareWithEmail:(id)sender;

/*! @brief represents the button for the sms. */
@property (strong, nonatomic) IBOutlet UIButton *smsButton;

/*! @brief represents the share with SMS action. */
- (IBAction)shareWithSMS:(id)sender;


@end

@implementation SocialShareViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    [WSCore addDarkBlurToView:self.view];
    
    UINavigationBar* navigBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    
    [navigBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    navigBar.shadowImage = [UIImage new];
    navigBar.translucent = YES;
    navigBar.backgroundColor = [UIColor clearColor];
    
    [navigBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont systemFontOfSize:20],
      NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    
    navigBar.tintColor = [UIColor whiteColor];
    
    [self.view addSubview:navigBar];
    
    UINavigationItem *navigItem = [[UINavigationItem alloc] initWithTitle:@"Share"];
    navigBar.items = [NSArray arrayWithObject:navigItem];
    
    [WSCore addBottomLine:navigBar :[UIColor whiteColor]];
    
    self.facebookButton.layer.cornerRadius=3.0;
    self.facebookButton.layer.masksToBounds=YES;
    self.whatsAppButton.layer.cornerRadius=3.0;
    self.whatsAppButton.layer.masksToBounds=YES;
    self.twitterButton.layer.cornerRadius=3.0;
    self.twitterButton.layer.masksToBounds=YES;
    
    self.emailButton.layer.cornerRadius=3.0;
    self.emailButton.layer.borderColor=[UIColor whiteColor].CGColor;
    self.emailButton.layer.borderWidth=1.0f;
    self.emailButton.layer.masksToBounds=YES;
    
    self.smsButton.layer.cornerRadius=3.0;
    self.smsButton.layer.borderWidth=1.0f;
    self.smsButton.layer.borderColor=[UIColor whiteColor].CGColor;
    self.smsButton.layer.masksToBounds=YES;
    
    [self.view bringSubviewToFront:self.facebookButton];
    [self.view bringSubviewToFront:self.whatsAppButton];
    [self.view bringSubviewToFront:self.twitterButton];
    [self.view bringSubviewToFront:self.emailButton];
    [self.view bringSubviewToFront:self.smsButton];
    [self.view bringSubviewToFront:navigBar];
    
    self.headerLabel.textAlignment=NSTextAlignmentCenter;
    self.headerLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    self.headerLabel.textColor=[UIColor cloudsColor];
    self.headerLabel.text = self.headerText;
    self.headerLabel.numberOfLines=0;
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [cancelButton setImage:[UIImage imageNamed:@"icon_x_cancel"] forState:UIControlStateNormal];
    [self.view addSubview:cancelButton];
    cancelButton.center = CGPointMake(self.view.center.x, self.view.frame.size.height-64);
    
    [cancelButton addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)cancelButtonPressed{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shareWithFacebook:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *fbPost = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        [fbPost setInitialText:self.messageToShare];
        [fbPost addURL:[NSURL URLWithString:@"http://www.whatsalon.com/"]];
        
        
        [self presentViewController:fbPost animated:YES completion:nil];
        
        [fbPost setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
    }
    else{
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Facebook Error" message:@"Cannot post to Facebook, please ensure that you are logged into Facebook in the device settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
}
- (IBAction)shareWithTwitter:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *twitterPost = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        
        [twitterPost setInitialText:[NSString stringWithFormat:@"%@ http://www.whatsalon.com/",self.messageToShare]];
        
        
        [self presentViewController:twitterPost animated:YES completion:nil];
        
        [twitterPost setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        
    }
    else{
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Twitter Error" message:@"Cannot post to Twitter, please ensure that you are logged into Twitter in the device settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"SMS cancelled");
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            NSLog(@"SMS sent");
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareWithWhatsApp:(id)sender {
    
    if ([WhatsAppKit isWhatsAppInstalled]) {
        
        NSString * message = [NSString stringWithFormat:@"%@ %@",self.messageToShare,kWhatSalonWebsite];
        [WhatsAppKit launchWhatsAppWithMessage:message];
        
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot open WhatsApp at the current time. Please ensure WhatsApp is installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
}
- (IBAction)shareWithEmail:(id)sender {
    
    
    if ([MFMailComposeViewController canSendMail]==YES) {
    // Email Subject
    NSString *emailTitle = @"WhatSalon!";
    // Email Content
    //NSString *messageBody = @"<h1>Join me at WhatSalon!</h1>";
    NSString * messageBody = self.messageToShare;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@""];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
        
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    }
    else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An email cannot be sent from this device. Please ensure you have set up an email address on your phone." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareWithSMS:(id)sender {
    
    MFMessageComposeViewController * text = [[MFMessageComposeViewController alloc] init];
    [text setMessageComposeDelegate:self];
    if ([MFMessageComposeViewController canSendText]) {
        
        [text setRecipients:nil];
        [text setBody:self.messageToShare];
        
        [self presentViewController:text animated:YES completion:nil];
    }
    else{
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"The current device is not capable of sending text messages." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    
    

}
@end
