//
//  PhorestStaffMember.m
//  whatsalon
//
//  Created by Graham Connolly on 28/11/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "PhorestStaffMember.h"

@implementation PhorestStaffMember

-(id)init {
    if ( self = [super init] ) {
        self.times = [NSMutableArray array];
        self.isAnyStylist=NO;
        self.isWorking=YES;
        
        self.isalon_booking_details = [[IsalonBookingDetails alloc] init];
    }
    return self;
}

- (BOOL)isEqual:(id)object
{
    if (![object isKindOfClass:[PhorestStaffMember class]])
        return NO;
    PhorestStaffMember *other = (PhorestStaffMember *)object;
    return self.staff_id == other.staff_id;
}
@end
