//
//  LastMinuteLocationViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 13/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LastMinuteLocationItem.h"
#import <CoreLocation/CoreLocation.h>



@protocol LastMinuteLocation <NSObject>

@required
//delegate method that handles the users selection
-(void)didPickerLocation:(LastMinuteLocationItem *) lastMinItem;

@end
@interface LastMinuteLocationViewController : UIViewController
@property (nonatomic) id<LastMinuteLocation> delegate;
@property (nonatomic) CLLocation * usersLocation;
@property (nonatomic) BOOL hasUsersLocation;


@end
