//
//  EmptyWalletView.h
//  whatsalon
//
//  Created by admin on 10/1/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class EmptyWalletView;
@protocol EmptyWalletViewDelegate <NSObject>
- (void) onAddCardTapped: (EmptyWalletView *) sender;
@end


@interface EmptyWalletView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIButton *addCardButton;

@property (nonatomic, weak) id <EmptyWalletViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
