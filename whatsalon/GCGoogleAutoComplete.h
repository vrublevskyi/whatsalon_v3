//
//  GCGoogleAutoComplete.h
//  GoogleAutoComplete
//
//  Created by Graham Connolly on 04/11/2015.
//  Copyright © 2015 GrahamConnolly. All rights reserved.
//


/*!
 @header GCGoogleAutoComplete.h
  
 @brief This is the header file for making it easier to connect to the Google places API.
  
 This was used to make it easier to make requests to Google to get places information.
 
 I opted for my own implementation rather than the Google iOS SDK due to the fact it only relied on Cocoa Pods to install it.
  
 @author     Graham Connolly
 @copyright  2015 Graham Connolly
 @version    1.0.1
 */
#import <Foundation/Foundation.h>

@interface GCGoogleAutoComplete : NSObject

/*!
 @brief performs an autocomplete search to GooglePlaces API, a call back is also performed to return the results.
 
 @param text - the text to perform the search on
 
 @param callBack - fetches the results of the network request
 
 */

-(void)autoCompleteWithString: (NSString *) text callBack: (void (^)(NSArray * results, NSError *error, BOOL hasFailed, NSString * googleStatusCode))callBack;

/*

 @brief performs a network request to get Google Place details based on a place id.
 
 @param places_id - the place id for the search
 
 @param callback - fetches the results of the network request.
 */
-(void)getPlacesDetailsWithID: (NSString *) places_id callBack:(void (^)(NSDictionary * jsonDict, NSError *error, BOOL hasFailed))callBack;

@end
