//
//  SelectCategoryViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 05/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SelectCategoryViewController.h"
#import "SalonService.h"
#import "SelectServicesViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "FriendlyService.h"
#import "ServiceTimeCell.h"
#import "SelectCategoryCell.h"
#import "whatsalon-Swift.h"

@interface SelectCategoryViewController () <UITableViewDataSource,UITableViewDelegate,UIViewControllerTransitioningDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic) BOOL categorySelected;
@property (nonatomic) BOOL serviceSelected;

@property (nonatomic) SalonService* category;
@property (nonatomic) SalonService* service;

@end

@implementation SelectCategoryViewController
@synthesize delegate;


- (void) viewDidLoad {
    [super viewDidLoad];
    [self registerCells];
    [self configureCategories];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [self configureNavigationBar];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonBlue}];
}

-(void) registerCells {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ServiceTimeCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"ServiceTimeCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"SelectCategoryCell" bundle:nil] forCellWithReuseIdentifier:@"SelectCategoryCell"];
}

-(void) configureCategories {
    self.servicesCategory = [NSMutableArray array];
    for (SalonService *pServ in self.servicesArray) {
        if (pServ.isHeader) {
            [self.servicesCategory addObject:pServ];
        }
    }
    self.collectionView.reloadData;
}

-(void) configureNavigationBar {
    CGRect frame = CGRectMake(0,0, 25,25);
    
    //BACK button
    UIImageView* backImage = [[UIImageView alloc] init];
    backImage.image = [UIImage imageNamed:@"back_arrow.png"];
    backImage.image = [backImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [backImage setTintColor:kWhatSalonSubTextColor];
    
    UIButton *backBttn = [[UIButton alloc] initWithFrame:frame];
    [backBttn setBackgroundImage:backImage.image forState:UIControlStateNormal];
    [backBttn addTarget:self action:@selector(back)
         forControlEvents:UIControlEventTouchUpInside];
    [backBttn setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *backButton =[[UIBarButtonItem alloc] initWithCustomView:backBttn];
    
    //CHECK MARK Button
    UIImageView* checkMark = [[UIImageView alloc] init];
    checkMark.image = [UIImage imageNamed:@"checkmark_icon"];
    checkMark.image = [checkMark.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [checkMark setTintColor:kWhatSalonSubTextColor];
    [checkMark setContentMode: UIViewContentModeScaleAspectFit];

    UIButton *checkBttn = [[UIButton alloc] initWithFrame:frame];
    [checkBttn setBackgroundImage:checkMark.image forState:UIControlStateNormal];

    [checkBttn addTarget:self action:@selector(listsStatueses)
         forControlEvents:UIControlEventTouchUpInside];
    [checkBttn setShowsTouchWhenHighlighted:YES];
    checkBttn.layer.cornerRadius = 12.5;
    checkBttn.layer.borderWidth = 1;
    checkBttn.layer.borderColor = kWhatSalonSubTextColor.CGColor;
    
    
    [checkBttn.widthAnchor constraintEqualToConstant:25].active = YES;
    [checkBttn.heightAnchor constraintEqualToConstant:25].active = YES;
    
    [backBttn.widthAnchor constraintEqualToConstant:25].active = YES;
    [backBttn.heightAnchor constraintEqualToConstant:25].active = YES;
    
    UIBarButtonItem *checkMarkButton =[[UIBarButtonItem alloc] initWithCustomView:checkBttn];
    
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.rightBarButtonItem = checkMarkButton;

    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonSubTextColor,
                              NSFontAttributeName:[UIFont systemFontOfSize:17]}];
    
    self.navigationController.navigationBar.tintColor = kWhatSalonSubTextColor;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.title = @"Pick a Categody";
}

-(void)back
{
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)listsStatueses
{
    if (!self.categorySelected && self.category != nil) {
        self.categorySelected = true;
            [self selected:self.category];
    } else if (self.categorySelected && self.service != nil) {
        self.serviceSelected = true;
        [self.delegate onServiceSelected:self.service sender:self];
        [self back];
    }
}

#pragma mark - Tableview
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ServiceTimeCell *cell = (ServiceTimeCell *) [self.tableView cellForRowAtIndexPath:indexPath];

    cell.stateSelected = true;

    SalonService *service = [self.servicesArray objectAtIndex:indexPath.row];
    self.service = service;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    ServiceTimeCell *cell = (ServiceTimeCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    cell.stateSelected = false;

}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     ServiceTimeCell *cell = (ServiceTimeCell *) [self.tableView dequeueReusableCellWithIdentifier:@"ServiceTimeCell"];
    
        SalonService *pServ = [self.servicesArray objectAtIndex:indexPath.row];
        cell.serviceNameLabel.text = pServ.service_name;
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isService) {
        return self.servicesArray.count;
    }
    return 0;
}

#pragma mark - CollectionView
- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    SelectCategoryCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"SelectCategoryCell" forIndexPath:indexPath];
    SalonService *pServ = [self.servicesCategory objectAtIndex:indexPath.row];
    NSString* header = pServ.category_name;
    cell.categoryLabel.text = header;
    
    if ([header isEqualToString:@"Body"]) {
        cell.logoImageView.image = [UIImage imageNamed:@"body_service_icon"];
    }
    else if ([header isEqualToString:@"Face"]) {
        cell.logoImageView.image = [UIImage imageNamed:@"face_service_icon"];
        
    }else if ([header isEqualToString:@"Hair"]) {
        cell.logoImageView.image = [UIImage imageNamed:@"hair_service_icon"];
        
    }else if ([header isEqualToString:@"Hair Removal"]) {
        cell.logoImageView.image = [UIImage imageNamed:@"wax_service_icon"];
        
    }else if ([header isEqualToString:@"Massage"]) {
        cell.logoImageView.image = [UIImage imageNamed:@"massage_service_icon"];
    }
    else if ([header isEqualToString:@"Nails"]) {
        cell.logoImageView.image = [UIImage imageNamed:@"nail_service_icon"];
    }
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.servicesCategory.count;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    SelectCategoryCell *cell = (SelectCategoryCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    SalonService *category = [self.servicesCategory objectAtIndex:indexPath.row];
    self.category = category;
    [Gradients addPinkToPurpleDiagonalGradientWithView:cell.view];

}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    SelectCategoryCell *cell = (SelectCategoryCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.view.backgroundColor = [UIColor whiteColor];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/3, self.view.frame.size.width/3);
}

-(void) selected:(SalonService*) service {
    self.title = @"Pick a Service";

    NSMutableArray * services = [NSMutableArray array];
    for (SalonService*p in self.servicesArray) {
        
        if ([p.category_name isEqualToString:service.category_name]) {
            [services addObject:p];
            
        }
    }
    
    //TODO: - check why first element have all fields NIL
    [services removeObjectAtIndex:0];
    
    self.servicesArray = services;
    self.isService = true;
    self.collectionView.hidden = true;
    self.tableView.reloadData;
}
@end
