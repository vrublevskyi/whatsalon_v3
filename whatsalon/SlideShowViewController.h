//
//  SlideShowViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 10/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header SlideShowViewController.h
  
 @brief This is the header file for the SlideShowViewController.h
  
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.3
 */
#import <UIKit/UIKit.h>
#import "Salon.h"

/*!
     @protocol SlideShowDelegate
  
     @brief The SlideShowDelegate protocol
  
     This protocol is used to notify the delegate to make an appearance updated
 */

@protocol SlideShowDelegate <NSObject>

@optional
/*!
 @brief returns the last page on screen.
 
 @param lastPage the last page in the slide show before the UIViewController is dismissed.
 */
-(void)getLastPage: (NSInteger)lastPage;

@end

@interface SlideShowViewController : UIViewController

/*! @brief represents the SlideShowDelegate object. */
@property (nonatomic,assign) id<SlideShowDelegate>myDelegate;

/*! @brief represents the Salon object. */
@property (nonatomic) Salon * salonData;

/*! @brief represents the index to start the slide show on. */
@property (nonatomic) NSInteger startIndex;

/*! @brief represents a dummy index image array. */
@property (nonatomic) NSMutableArray * dummyImageArray DEPRECATED_MSG_ATTRIBUTE("No longer used, was only used for testing.");
@end
