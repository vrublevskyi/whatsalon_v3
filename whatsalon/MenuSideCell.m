//
//  MenuSideCell.m
//  whatsalon
//
//  Created by admin on 10/1/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import "MenuSideCell.h"

@implementation MenuSideCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
