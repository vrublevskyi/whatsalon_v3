//
//  CategoriesSearchCell.swift
//  whatsalon
//
//  Created by admin on 10/5/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit
import Reusable

class CategoriesSearchCell: UITableViewCell, NibReusable {
    
    //MARK: - Properties
    
    @IBOutlet private var categoryName: UILabel!
    @IBOutlet private var categoryLogoImageVIew: UIImageView! {
        didSet {
            categoryLogoImageVIew.image?.withRenderingMode(.alwaysTemplate)
        }
    }

    var categortSelected = false {
        didSet {
            let tintColor = categortSelected ?
                LayoutDefaults.defPinkColor : .lightGray
            categoryName.textColor =  tintColor
            categoryLogoImageVIew.tintColor = tintColor
        }
    }
    
    var category:String? {
        didSet {
            categoryName.text = category
        }
    }
    var logoName: String? {
        didSet {
            categoryLogoImageVIew.image = UIImage(named: logoName ?? "")
        }
    }
}
