//
//  DirectionsMapTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 24/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface DirectionsMapTableViewCell : UITableViewCell

@property (nonatomic) UIImageView * mapImageView;
@property (nonatomic) UIImageView * directionsMapIcon;
@property (nonatomic) UILabel *directionsLabel;


-(void)setImageWithURL : (NSString *)url;
@end
