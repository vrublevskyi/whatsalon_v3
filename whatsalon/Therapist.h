//
//  Therapist.h
//  whatsalon
//
//  Created by Graham Connolly on 07/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Therapist : NSObject

@property (nonatomic) NSString * name;
@property (nonatomic) NSString * price;
@property (nonatomic) NSString * pic_url;
@property (nonatomic) NSString * time;
@property (nonatomic) NSString * therapist_id;


-(instancetype) initWithTherapistID :(NSString *)t_id;
+(instancetype) bookingWithTherapistID: (NSString *) t_id;

@end
