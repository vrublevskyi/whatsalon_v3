//
//  SalonAvailableTime.h
//  whatsalon
//
//  Created by Graham Connolly on 01/12/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

/*!
 @header SalonAvailableTime.h
  
 @brief This is the header file for SalonAvailableTime

  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd.
 @version    1.0.4
 */


#import <Foundation/Foundation.h>

@interface SalonAvailableTime : NSObject

/*! @brief represents the slot id */
@property (nonatomic) NSString * slot_id;

/*! @brief represents the start time. */
@property (nonatomic) NSString * start_time;

/*! @brief represents the end time. */
@property (nonatomic) NSString * end_time;

/*! @brief represents the internal start datetime. */
@property (nonatomic) NSString * internal_start_datetime;

/*! @brief represents the internal end datetime. */
@property (nonatomic) NSString * internal_end_datetime;

/*! @brief represents the room id. */
@property (nonatomic) NSString * room_id;

/*! @brief represents the service id. */
@property (nonatomic) NSString * service_id;

/*! @brief determines whether a patch test is required. */
@property (nonatomic) BOOL patch_test_required;

/*! @brief represents the staff id. */
@property (nonatomic) NSString * staff_id;


/*!
 
    @brief returns a formatted date string (EE MMM,dd) from an NSString date.
    
    @param date - the date to be formatted.
 
    @return NSString.
 
 */
-(NSString *) formattedDate : (NSString *) date;

/*!
    @brief get time (h:mm a) from the date
 
    @param date - the date to be formatted.
 
    @return NSString
 
 */
-(NSString *) getTimeFromDate : (NSString *) date;

@end
