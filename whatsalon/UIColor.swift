//
//  UIColor.swift
//  whatsalon
//
//  Created by admin on 10/7/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit
extension UIColor {
    static var lightGrayCalendar = UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 1)
    static var darkGray = UIColor(red: 91/255, green: 91/255, blue: 91/255, alpha: 1)

}
