//
//  LoginView.h
//  whatsalon
//
//  Created by Graham Connolly on 07/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"

@interface LoginView : UIView

/*! @brief represents the title label . */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! @brief represents the description label. */
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

/*! @brief represents the login button. */
@property (weak, nonatomic) IBOutlet FUIButton *loginButton;

/*! @brief represents the view for hosting the content. */
@property (strong, nonatomic) IBOutlet UIView *view;

@end
