//
//  PrimativeComparison.h
//  whatsalon
//
//  Created by Graham Connolly on 21/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrimativeComparison : NSNumber

- (NSComparisonResult) compareWithInt:(int)i;
- (BOOL) isEqualToInt:(int)i;
- (BOOL) isGreaterThanInt:(int)i;
- (BOOL) isGreaterThanOrEqualToInt:(int)i;
- (BOOL) isLessThanInt:(int)i;
- (BOOL) isLessThanOrEqualToInt:(int)i;

@end
