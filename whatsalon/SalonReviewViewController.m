//
//  SalonReviewViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 07/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonReviewViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "ConfirmReviewViewController.h"
#import "User.h"
#import "UIView+AlertCompatibility.h"
#import "GCNetworkManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "FUIAlertView.h"


@interface SalonReviewViewController () <UIViewControllerTransitioningDelegate,ConfirmDelegate,GCNetworkManagerDelegate,UIActionSheetDelegate>

@property (nonatomic) float tableViewYPos;
@property (nonatomic,assign) BOOL hasPic;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak,nonatomic) UIButton * addPicButton;
@property (weak,nonatomic) UIView *viewToTap;
@property (strong, nonatomic) IBOutlet UIImageView *imageStatus;
@property (nonatomic) NSURLSessionDataTask * dataTask;
@property (nonatomic) GCNetworkManager * networkManager;
@property (nonatomic) UIImage * reviewedImage;

@end


@implementation SalonReviewViewController

#pragma mark - GCNetworkManager
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    NSLog(@"JSON Error \n %@",jsonDictionary);
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    [UIView showSimpleAlertWithTitle:@"Thanks" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [WSCore statusBarColor:StatusBarColorBlack];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.4];
    self.submitButton.backgroundColor = kWhatSalonBlue;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.styleRating = [NSString stringWithFormat:@"%f",0.0f] ;
    self.salonRating= [NSString stringWithFormat:@"%f",0.0f] ;

    

    
    self.hasPic=NO;
  
    
    NSLog(@"Confirm ID %@",self.confirmID);
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    
}


-(void)dismissModalView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

   



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * cell;
    
    switch (indexPath.row) {
        case 0:
            cell = [tableView dequeueReusableCellWithIdentifier:@"salRatingCell"];
            
            self.salonName = (UILabel *)[cell viewWithTag:8881];
            self.salonName.text=self.booking.bookingSalon.salon_name;
            self.starRatingsView = (EDStarRating *)[cell viewWithTag:kSalonStarRating];
            self.starRatingsView.starImage = [UIImage imageNamed:@"emptyStar"];
            self.starRatingsView.starHighlightedImage = [UIImage imageNamed:@"blueStar"];
            self.starRatingsView.maxRating = 5.0;
            self.starRatingsView.delegate = self;
            self.starRatingsView.horizontalMargin = 12;
            self.starRatingsView.editable=YES;
            self.starRatingsView.displayMode=EDStarRatingDisplayFull;
            self.starRatingsView.rating= 0;
            [self starsSelectionChanged:_starRatingsView rating:0];
            
            

            break;
        case 1:{
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"styRatingCell"];
            self.stylistName = (UILabel*)[cell viewWithTag:9991];
            self.stylistName.text = self.booking.stylistName;
            self.stylistRatingView = (EDStarRating *)[cell viewWithTag:kStyleStarRating];
            self.stylistRatingView.starImage = [UIImage imageNamed:@"emptyStar"];
            self.stylistRatingView.starHighlightedImage = [UIImage imageNamed:@"blueStar"];
            self.stylistRatingView.maxRating = 5.0;
            self.stylistRatingView.delegate = self;
            self.stylistRatingView.horizontalMargin = 12;
            self.stylistRatingView.editable=YES;
            self.stylistRatingView.displayMode=EDStarRatingDisplayFull;
            self.stylistRatingView.rating= 0;
            [self starsSelectionChanged:_stylistRatingView rating:0];
            
                   }
            break;
            
        case 2:{
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"showLookCell"];
            
            UIButton * photoBtn = (UIButton *)[cell viewWithTag:kAddPhoto];
            self.addPicButton = photoBtn;
            [self.imageStatus removeFromSuperview];
            
           
            UIButton * infoButton = (UIButton *)[cell viewWithTag:8051];
            UIImage * imageV = [infoButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [infoButton setImage:imageV forState:UIControlStateNormal];
            [infoButton setTintColor:kWhatSalonBlue];
            
            UIImage *templateImage;
            
            if (self.hasPic==NO) {
                NSLog(@"NO pic");
                templateImage=[[UIImage imageNamed:@"pic.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [photoBtn setImage:templateImage forState:UIControlStateNormal];
               
                [photoBtn setTintColor:kWhatSalonBlue];
            }else{
                NSLog(@"Has pic");
                templateImage=[[UIImage imageNamed:@"CheckMark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [photoBtn setImage:templateImage forState:UIControlStateNormal];
                
                [photoBtn setTintColor:kWhatSalonBlue];
            }
            [cell.contentView addSubview:self.imageStatus];
            
            
            
            [photoBtn setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];        }
            break;
            
        case 3:
            cell = [tableView dequeueReusableCellWithIdentifier:@"experienceCell"];
            self.descTextView = [[UITextView alloc] initWithFrame:CGRectMake(14, 0, 293, 99)];
            self.descTextView.delegate = self;
            self.descTextView.text = @"Describe your experience";
            self.descTextView.font = [UIFont systemFontOfSize:17];
            self.descTextView.returnKeyType = UIReturnKeyDone;
            self.descTextView.textColor=kWhatSalonSubTextColor;
            [cell.contentView addSubview:self.descTextView];
            
            break;
        default:
            break;
    }
    
    cell.clipsToBounds=YES;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row == 0 || indexPath.row == 1) {
        return 60;
    }
    if (indexPath.row ==3) {
        return 120;
    }
        return 44;
}

#pragma mark - EDStarRating methods
-(void)starsSelectionChanged:(EDStarRating *)control rating:(float)rating
{
    NSString *ratingString = [NSString stringWithFormat:@"%d", (int)rating];
    if([control isEqual:_starRatingsView]){
        self.salonRating = ratingString;
        NSLog(@"Salon %@",ratingString);
    }else{
        self.styleRating = ratingString;
        NSLog(@"Style %@",ratingString);
    }
    
}


- (IBAction)close:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex != [actionSheet cancelButtonIndex]) {
        
        if (buttonIndex==0) {
            [self takePicture];
        }
        else if(buttonIndex ==1){
            [self choosePhoto];
        }
    }
}

-(void)choosePhoto{
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing=NO;//set to NO - can cause memory issues
    
    //choose from photo library
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}
-(void)takePicture{
    
    if (!self.imagePicker) {
        self.imagePicker = [[UIImagePickerController alloc] init];
        self.imagePicker.delegate = self;
        self.imagePicker.allowsEditing=NO;//set to NO -can cause memory issues
    }
    
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
    }else{
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    
    //self.imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    [self presentViewController:self.imagePicker animated:YES completion:nil];

}
- (IBAction)addPhoto:(id)sender {
    
    //[self picturePrompt];
    
    UIActionSheet * actionSheet = [[UIActionSheet alloc]initWithTitle:@"Camera" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Picture",@"Choose Existing", nil];
    [actionSheet showInView:self.view];
    
    
    
}

- (IBAction)submit:(id)sender {
    
        [self submitReview];
     
}

- (IBAction)showInfo:(id)sender {
    
    [self picturePrompt];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag==1) {
        if (buttonIndex==1) {
           
            [self submitReview];
        }
    }
    if (alertView.tag==2) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}

#pragma mark - UITextView delegate methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        
        return NO;
    }
    
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    

    textView.text = @"";
    textView.textColor=[UIColor blackColor];
        //start animation
    [self startAnimation];
    self.viewToTap.userInteractionEnabled=NO;
       
    
    
}

-(void) textViewDidEndEditing:(UITextView *)textView{
    
    NSString * textLength = textView.text;
    if (textLength.length == 0) {
        textView.text = @"Describe your experience";
        textView.textColor=kWhatSalonSubTextColor;
    }
   
    
    [self hideAnimation];
    self.viewToTap.userInteractionEnabled=YES;
}

-(void)startAnimation{
    
        
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{

            float yValye = self.tableView.frame.size.height - kKeyBoardHeight - 50-40;
            self.tableView.frame = CGRectMake(0, yValye, self.tableView.frame.size.width, self.tableView.frame.size.height);
        } completion:nil];

}

-(void)hideAnimation{
        
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{

            self.tableView.frame = CGRectMake(0, self.view.frame.size.height-self.tableView.frame.size.height, self.tableView.frame.size.width, self.tableView.frame.size.height);
        } completion:nil];
    
}

#pragma mark - Image Picker Controller delegate

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.imageView.image=image;

    
    if (self.imagePicker.sourceType == UIImagePickerControllerSourceTypeCamera){
        //save the image to photos library
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        self.hasPic=YES;
        self.imageView.hidden=NO;
        self.viewToTap.userInteractionEnabled=NO;
        ConfirmReviewViewController *viewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"confirmReview"];
        viewController.modalPresentationStyle = UIModalPresentationCustom;
        viewController.transitioningDelegate = self;
        viewController.takenImage=image;
        viewController.myDelegate=self;
        viewController.booking=self.booking;
        viewController.confirmID=self.confirmID;
        
        [self presentViewController:viewController animated:YES completion:nil];
        
    }];
    
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}


- (void)secondViewControllerDismissed:(BOOL)hasPic WithImage:(UIImage *)image
{
    if (image) {
        NSLog(@"image is not nil");
        self.reviewedImage = image;
    }
    else{
        NSLog(@"Image is nil");
        self.reviewedImage=nil;
        
    }
    self.hasPic=hasPic;
    //[self.tableView reloadData];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}


-(void)submitReview{
    
    if (self.descTextView.text.length==0 || (@"Describe your experience" && [self.descTextView.text caseInsensitiveCompare:@"Describe your experience"] == NSOrderedSame)) {
        
        [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please describe your experience." cancelButtonTitle:@"OK"];
    }
    else{
        
        NSLog(@"1");
        if (self.reviewedImage!=nil) {
           
        
            NSData *imageData = UIImageJPEGRepresentation(self.reviewedImage, 0.3);
            NSLog(@"Image Data %@",imageData);
            NSString *encodedString = [imageData base64EncodedStringWithOptions:0];
            
            NSLog(@"2");
            
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            NSDictionary *parameters;
            if ([[User getInstance] fetchKey].length!=0) {
                parameters = @{@"user_id": [[User getInstance] fetchAccessToken],@"booking_id":self.confirmID,@"salon_id":self.booking.bookingSalon.salon_id,@"salon_rating":self.salonRating,@"stylist_rating":self.styleRating,@"message":self.descTextView.text,@"image":encodedString,@"secret_key":[[User getInstance] fetchKey]};
            }else{
                parameters = @{@"user_id": [[User getInstance] fetchAccessToken],@"booking_id":self.confirmID,@"salon_id":self.booking.bookingSalon.salon_id,@"salon_rating":self.salonRating,@"stylist_rating":self.styleRating,@"message":self.descTextView.text,@"image":encodedString};

            }
            
            
            [WSCore showNetworkLoadingOnView:self.view];
            [manager POST:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,@"salon_review"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"JSON: %@", responseObject);
                
                [WSCore dismissNetworkLoadingOnView:self.view];
                [UIView showSimpleAlertWithTitle:@"Thanks" message:responseObject[@"message"] cancelButtonTitle:@"OK"];
                
                if ([self.myDelegate respondsToSelector:@selector(addedReview)]) {
                    [self.myDelegate addedReview];
                }
                [self dismissViewControllerAnimated:YES completion:nil];
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                [WSCore dismissNetworkLoadingOnView:self.view];
                [UIView showSimpleAlertWithTitle:@"Oh Oh!" message:@"An error has occurred when submitting a review. Please try again later"cancelButtonTitle:@"OK"];
                NSLog(@"Error: %@", error);
            }];
            
            
        }
        else{
            
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            NSDictionary *parameters = @{@"user_id": [[User getInstance] fetchAccessToken],@"booking_id":self.confirmID,@"salon_id":self.booking.bookingSalon.salon_id,@"salon_rating":self.salonRating,@"stylist_rating":self.styleRating,@"message":self.descTextView.text};
            
            [WSCore showNetworkLoadingOnView:self.view];
            
            [manager POST:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,@"salon_review"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"JSON: %@", responseObject);
                
                [WSCore dismissNetworkLoadingOnView:self.view];
                [UIView showSimpleAlertWithTitle:@"Thanks" message:responseObject[@"message"] cancelButtonTitle:@"OK"];
                
                [self dismissViewControllerAnimated:YES completion:^{
                    if ([self.myDelegate respondsToSelector:@selector(addedReview)]) {
                        [self.myDelegate addedReview];
                    }

                }];
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                [WSCore dismissNetworkLoadingOnView:self.view];
                [UIView showSimpleAlertWithTitle:@"Oh Oh!" message:@"An error has occurred when submitting a review. Please try again later"cancelButtonTitle:@"OK"];
                NSLog(@"Error: %@", error);
            }];

        }
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [self.dataTask cancel];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}


-(void)picturePrompt{
    
    [self.view endEditing:YES];
    [self hideAnimation];
    FUIAlertView *alertView = [[FUIAlertView alloc] initWithTitle:@"What is this?"
                                                          message:@"\"Happy with the results? Show it off! By uploading an image, you give other WhatSalon users the chance to look at you with envy and to \"Book the Look\" with the same salon and stylist through our \"Inspire\" section.\""
                                                         delegate:nil cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
    alertView.titleLabel.textColor = [UIColor cloudsColor];
    //alertView.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    alertView.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    alertView.messageLabel.textColor = [UIColor cloudsColor];
    alertView.messageLabel.font = [UIFont systemFontOfSize:14.0];
    //alertView.messageLabel.font = [UIFont flatFontOfSize:14];
    alertView.backgroundOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    alertView.alertContainer.backgroundColor = kWhatSalonBlue;
    alertView.defaultButtonColor = [UIColor cloudsColor];
    alertView.defaultButtonShadowColor = [UIColor asbestosColor];
    alertView.defaultButtonShadowHeight=kShadowHeight;
    //alertView.defaultButtonFont = [UIFont boldFlatFontOfSize:16];
    alertView.defaultButtonTitleColor = [UIColor asbestosColor];
    [alertView show];
    /*
    FUIAlertView *alertView = [[FUIAlertView alloc] initWithTitle:@"What is this?"
                                                          message:@"Here you can submit a pic. Here are our terms and conditions. Would you like to submit a Photo?"
                                                         delegate:nil cancelButtonTitle:@"No"
                                                otherButtonTitles:@"YES",nil];
    alertView.titleLabel.textColor = [UIColor cloudsColor];
    //alertView.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    alertView.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    alertView.messageLabel.textColor = [UIColor cloudsColor];
    alertView.messageLabel.font = [UIFont systemFontOfSize:14.0];
    //alertView.messageLabel.font = [UIFont flatFontOfSize:14];
    alertView.backgroundOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    alertView.alertContainer.backgroundColor = kWhatSalonBlue;
    alertView.defaultButtonColor = [UIColor cloudsColor];
    alertView.defaultButtonShadowColor = [UIColor asbestosColor];
    alertView.defaultButtonShadowHeight=kShadowHeight;
    //alertView.defaultButtonFont = [UIFont boldFlatFontOfSize:16];
    alertView.defaultButtonTitleColor = [UIColor asbestosColor];
    [alertView show];
    
    alertView.onOkAction = ^ {
        
        UIActionSheet * actionSheet = [[UIActionSheet alloc]initWithTitle:@"Camera" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Picture",@"Choose Existing", nil];
        [actionSheet showInView:self.view];
    };
 */

}
@end
