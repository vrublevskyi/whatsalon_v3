//
//  TabDirectoryViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 21/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabDirectoryViewController : UIViewController

/*! @brief resets the search. */
- (IBAction)resetSearch:(id)sender;

/*! @brief represents the search bar. */
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

/*! @brief represents the table view for the content. */
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
