//
//  GCPopInTransition.m
//  transition
//
//  Created by Graham Connolly on 17/02/2015.
//  Copyright (c) graham connolly All rights reserved.
//

#import "GCPopInTransition.h"

@implementation GCPopInTransition

-(instancetype)initWithFrame: (CGRect) frame{
    self = [super init];
    if (self) {
        self.frame =frame;
    }
    return self;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    //it gets us the view that we are tranistioning to
    UIViewController * detail = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    
    UIView * containerView = [transitionContext containerView];
    
    detail.view.frame = containerView.bounds;
    [containerView addSubview:detail.view];
    // instantaneously make the image view small (scaled to 1% of its actual size)
    
    detail.view.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        detail.view.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        
        [transitionContext completeTransition:YES];
    }];
    
}

-(NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    return 0.4;
}

@end
