//
//  FAQTableViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 21/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQTableViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
