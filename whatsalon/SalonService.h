//
//  SalonService.h
//  whatsalon
//
//  Created by Graham Connolly on 27/11/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//


/*!
 @header SalonService.h
  
 @brief This is the header file for a salons service (once previously PhorestService).
   
 @author Graham Connolly
 @copyright  2014 What Applications Ltd.
 @version    1.0.5
 */
#import <Foundation/Foundation.h>

@interface SalonService: NSObject

/*! @brief represents the category name. */
@property (nonatomic) NSString * category_name;

/*! @brief represents the service color. */
@property (nonatomic) NSString * service_color;

/*! @brief represents the duration of the service. */
@property (nonatomic) NSString * service_duration;

/*! @brief represents the id of the service. */
@property (nonatomic) NSString * service_id;

/*! @brief represents the name of the service. */
@property (nonatomic) NSString * service_name;

/*! @brief determines if a patch test is required. */
@property (nonatomic) BOOL service_patch_test_required;

/*! @brief represents the price of the service. */
@property (nonatomic) NSString * price;

/*! @brief determine whether the SalonService is a header object. */
@property (nonatomic,assign) BOOL isHeader;
@end
