//
//  UISegmentedControl+setSegments.h
//  whatsalon
//
//  Created by Graham Connolly on 18/07/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISegmentedControl (setSegments)

- (void)setSegments:(NSArray *)segments;

@end
