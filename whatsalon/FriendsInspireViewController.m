//
//  FriendsInspireViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "FriendsInspireViewController.h"

@interface FriendsInspireViewController ()

@end

@implementation FriendsInspireViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    self.connectFB_btn.layer.cornerRadius=10.0f;
    self.connectFB_btn.layer.borderWidth=3.0f;
    self.connectFB_btn.layer.borderColor = [[UIColor blackColor] CGColor] ;
}



- (IBAction)connectToFB:(id)sender {
}
@end
