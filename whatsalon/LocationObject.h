//
//  LocationObject.h
//  whatsalon
//
//  Created by Graham Connolly on 20/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationObject : NSObject

/*! @brief represents the full address. */
@property (nonatomic)  NSString *fullAddress;

/*! @brief represents the latitude. */
@property (nonatomic) NSString * lat;

/*! @brief represents the longitude. */
@property (nonatomic) NSString * lon;

/*! @brief represents the place id. */
@property (nonatomic) NSString * place_id;//only used for GCGoogleAutoCompleteObject

/*! @brief represents the title. */
@property (nonatomic) NSString * title;

/*! @brief represents the subtitle. */
@property (nonatomic) NSString * subtitle;

/*! @brief determines if the cell is the current location cell. */
@property (nonatomic) BOOL isCurrentLocation;

@end
