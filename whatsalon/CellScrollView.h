//
//  CellScrollView.h
//  whatsalon
//
//  Created by Graham Connolly on 13/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellScrollView : UIScrollView

@end
