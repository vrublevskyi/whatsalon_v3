//
//  ResetPasswordViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 08/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "UITextField+PaddingText.h"
#import "GCNetworkManager.h"
#import "UIView+AlertCompatibility.h"
#import "NSString+Validations.h"
#import "FUIButton.h"

@interface ResetPasswordViewController ()<GCNetworkManagerDelegate>
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet FUIButton *resetPasswordBtn;
@property (nonatomic) GCNetworkManager * networkManager;
- (IBAction)resetPassword:(id)sender;

- (IBAction)cancel:(id)sender;
@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    [WSCore addTopLine:self.email :kCellLinesColour :0.5];
    [WSCore addBottomLine:self.email :kCellLinesColour];
    
    self.resetPasswordBtn.buttonColor = kWhatSalonBlue;
    self.resetPasswordBtn.shadowColor = kWhatSalonBlueShadow;
    self.resetPasswordBtn.shadowHeight = 1.5f;
    self.resetPasswordBtn.cornerRadius = 3.0f;
    [self.resetPasswordBtn setTitle:@"Reset" forState:UIControlStateNormal];
    [self.resetPasswordBtn setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];    self.email.keyboardType = UIKeyboardTypeEmailAddress;
    
    [self.email setLeftPadding:20.0f];
    
    [self navigationBarSetUp];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
-(void)navigationBarSetUp{
    //make navigation bar completely transparent
    self.navBarView.backgroundColor=[UIColor clearColor];
    [self.navBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navBar.shadowImage = [UIImage new];
    self.navBar.translucent = YES;
    
    
    //change font style of navigation bar title
    [self.navBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont systemFontOfSize:18],
      NSFontAttributeName,[UIColor darkGrayColor],NSForegroundColorAttributeName, nil]];
    
    [self.cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:18], NSFontAttributeName, [UIColor darkGrayColor],NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)resetPassword:(id)sender {
    
    [self.view endEditing:YES];
    
    if (self.email.text.length==0 ) {
        [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please enter an email address." cancelButtonTitle:@"OK"];
        return;
    }
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kResetPassword_URL]];
    NSString * param = [NSString stringWithFormat:@"email=%@",self.email.text];
    [self.networkManager loadWithURL:url withParams:param WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
}

- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - GCNetworkDelegate
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    [UIView showSimpleAlertWithTitle:@"Oops" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    [UIView showSimpleAlertWithTitle:@"Password Reset" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
}
@end
