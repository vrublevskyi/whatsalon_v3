//
//  GCStackedGridLayout.h
//  StaggeredCollectionView
//
//  Created by Graham Connolly on 21/10/2014.
//  Copyright (c) 2014 Graham Connolly. All rights reserved.
//

#import <UIKit/UIKit.h>

/*custom delegate that inherits from UICollectionViewDelegate
 This is because there is a few differented things your custom layout has to know, such as how many columns there are in a section
 */

@protocol StackedGridLayoutDelegate <UICollectionViewDelegate>

//1
/*
 Returns the number of columns for a give section
 */
-(NSInteger)collectionView:(UICollectionView*)cv layout:(UICollectionViewLayout *)cvl numberOfColumnsInSection:(NSInteger)section;

//2
/*
 Returns the size for an item, give the width of the column it will go into. For example, in the case of showing photos, the delegate works out how tall the cell needs to be to maintain the aspect ratio of the photo.
 */
- (CGSize)collectionView:(UICollectionView*)cv layout:(UICollectionViewLayout*)cvl
    sizeForItemWithWidth:(CGFloat)width atIndexPath:(NSIndexPath *)indexPath;

//3
/*
 Returns the insets of a given item. These will be used to inset the cell, such that there are gaps between items in a column and between collumns, if so desired.
 */
- (UIEdgeInsets)collectionView:(UICollectionView*)cv layout:(UICollectionViewLayout*)cvl
   itemInsetsForSectionAtIndex:(NSInteger)section;

@end
@interface GCStackedGridLayout : UICollectionViewLayout

@property (nonatomic,assign) CGFloat headerHeight;//defines how big the header of each section should be.

@end
