//
//  BugViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 28/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "BugViewController.h"
#import "User.h"
#import "GCNetworkManager.h"
#import "UIView+AlertCompatibility.h"
#import "FUIButton.h"
#import "UIColor+FlatUI.h"

@interface BugViewController ()<UITextViewDelegate,GCNetworkManagerDelegate>

@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet FUIButton *submitButton;
@property (nonatomic) GCNetworkManager * networkManager;
- (IBAction)submit:(id)sender;
@end

@implementation BugViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate =self;
    self.networkManager.parentView = self.view;
    
    self.textView.text =@"";
    self.textView.layer.cornerRadius=3.0;
    self.textView.layer.borderColor=kCellLinesColour.CGColor;
    self.textView.layer.borderWidth=0.5;
    self.textView.layer.masksToBounds=YES;
    self.textView.returnKeyType=UIReturnKeyDone;
    self.textView.delegate=self;
    
    self.submitButton.layer.cornerRadius=3.0;
    self.submitButton.layer.masksToBounds=YES;
    self.submitButton.backgroundColor=kWhatSalonBlue;
    self.submitButton.buttonColor = kWhatSalonBlue;
    self.submitButton.shadowColor = kWhatSalonBlueShadow;
    self.submitButton.shadowHeight = 1.0f;
    self.submitButton.cornerRadius = 3.0f;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
    if ([text length] == 1 && resultRange.location != NSNotFound) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


- (void)submitBug {
    [self.view endEditing:YES];
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kFeed_Back_URL]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_content=%@",self.textView.text]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&device_used=ios"]];
    NSString * deviceNumber = [[UIDevice currentDevice] systemVersion];
    deviceNumber = [deviceNumber stringByReplacingOccurrencesOfString:@"." withString:@"_"];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&device_version_number=%@",deviceNumber]];
    
    NSString * deviceName = [WSCore fetchDeviceName];
    deviceName = [deviceName stringByReplacingOccurrencesOfString:@"," withString:@"_"];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&device_model_number=%@",deviceName]];
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
    /*
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:60.f];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        if (data && dataDict!=nil) {
            
            //add switch
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //[WSCore dismissNetworkLoading];
                            [WSCore dismissNetworkLoadingOnView:self.view];
                            
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Yikes!" message:[NSString stringWithFormat:@"Thanks! %@. How did we miss that?!",dataDict[@"message"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            
                            self.textView.text=@"";
                        });
                        
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //[WSCore dismissNetworkLoading];
                            [WSCore dismissNetworkLoadingOnView:self.view];
                            [WSCore showServerErrorAlert];
                        });
                    }
                    break;
                    
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                       // [WSCore dismissNetworkLoading];
                        [WSCore dismissNetworkLoadingOnView:self.view];
                        [WSCore showServerErrorAlert];
                    });
                    break;
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //[WSCore dismissNetworkLoading];
                [WSCore dismissNetworkLoadingOnView:self.view];
                [WSCore showServerErrorAlert];
            });
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        
        //[WSCore showNetworkLoading];
        [WSCore showNetworkLoadingOnView:self.view];
        [dataTask resume];
    }
    else{
        [WSCore showServerErrorAlert];
    }
    */
}

- (IBAction)submit:(id)sender {
    //params
    //user_id
    //message
    
    if (self.textView.text.length!=0) {
       [self submitBug]; 
    }
    else{
        
        [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please fill out the form before submitting." cancelButtonTitle:@"OK"];
        
    }
    
    
}

#pragma mark - UIRepsonder touchesBegan
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}

#pragma mark - GCNetworkDelegate Methods
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    [WSCore showServerErrorAlert];
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    [UIView showSimpleAlertWithTitle:@"Yikes!" message:[NSString stringWithFormat:@"Thanks! %@. How did we miss that?!",jsonDictionary[@"message"]] cancelButtonTitle:@"OK"];
    self.textView.text=@"";
    
}
@end
