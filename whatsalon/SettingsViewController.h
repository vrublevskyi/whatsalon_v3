//
//  SettingsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 19/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu/RESideMenu.h"

@interface SettingsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

/*!
 * @brief A UIView which contains sign up and log in buttons. signUpView is initially visible. Once a user has logged in it is set to hidden. Once the user logs out, it reappears.
 */
@property (weak, nonatomic) IBOutlet UIView *signUpView;

/*!
 * @brief An IBOutlet UITableView which contains settings options as rows
 */
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;

/*!
 * @brief An IBOutlet UIButton which references the button for signing up.
 */
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;



/*!
 * @discussion Action whichs reveals the Menu when selected
 */

- (IBAction)showMenuButton:(id)sender;
- (IBAction)signUp:(id)sender;

@end
