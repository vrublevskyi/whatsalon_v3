//
//  FavSalonsModel.h
//  whatsalon
//
//  Created by Graham Connolly on 27/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FavSalonsDelegate <NSObject>

@optional
-(void)reloadFavSalonsData;
-(void)downloadingFavSalonsFinished;
-(void)downloadingFavSalonsStarted;

@end
@interface FavSalonsModel : NSObject
@property (nonatomic,assign) id<FavSalonsDelegate> myDelegate;
@property (nonatomic) NSString * loginKey;
@property (nonatomic) NSString * searchTerm;
@property (nonatomic) NSMutableArray * arryResults;
@property (nonatomic) int totalNumberOfPages;

-(void)load: (NSURL *)url withParams: (NSString *)params;

-(NSMutableArray*)fetchFavSalons;

-(void)removeFavSalonObjectsFromArray;

-(int)fetchTotalNumberOfPages;
@end
