//
//  InspireCollectionViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 16/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "InspireCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "User.h"

@implementation InspireCollectionViewCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        self.layer.cornerRadius = 6.0;
        
        self.bgImageView = [[UIImageView alloc] init];
        self.book = [[UILabel alloc] init];
        self.infoLabel = [[UILabel alloc] init];
        self.overlayImageView= [[UIImageView alloc] init];
        self.favImageView = [[UIImageView alloc] init];
        self.bgImageView.image = [UIImage imageNamed:@"hair1"];
        
        
        self.overlayImageView.image = [UIImage imageNamed:@"overlay.png"];
        self.favImageView.frame=CGRectMake(115, 10, 20, 20);
        //self.favImageView.image = [UIImage imageNamed:@"favUnselected"];
        self.favImageView.image = [UIImage imageNamed:@"White_Heart_With_Gray_Solid"];
        self.infoLabel.frame=CGRectMake(6, self.bgImageView.bounds.size.height - 38, 129, 38);
        self.infoLabel.textColor = [UIColor whiteColor];
        self.infoLabel.textAlignment = NSTextAlignmentLeft;
        self.infoLabel.adjustsFontSizeToFitWidth=YES;
        self.infoLabel.numberOfLines=2;
        self.infoLabel.font = [UIFont systemFontOfSize: 11.0f];;
        
        [self.contentView insertSubview:self.infoLabel atIndex:2];

        
        self.book = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 41, 140, 41)];
        
        self.book.text = @"Book the Look";
        self.book.textColor = [UIColor whiteColor];
        self.book.adjustsFontSizeToFitWidth=YES;
        self.book.textAlignment = NSTextAlignmentCenter;
        self.book.backgroundColor= [UIColor darkGrayColor];
        self.book.font = [UIFont systemFontOfSize: 17.0f];
        self.book.hidden = YES;
        
        [self.contentView insertSubview:self.book atIndex:2];
        
        [self.contentView insertSubview:self.bgImageView atIndex:0];
        [self.contentView insertSubview:self.overlayImageView atIndex:1];
        [self.contentView insertSubview:self.favImageView atIndex:2];
        
        UILabel * selectFavLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - 45, 0, 45, 45)];
        selectFavLabel.backgroundColor = [UIColor clearColor];
        selectFavLabel.userInteractionEnabled=YES;
        [self.contentView addSubview:selectFavLabel];
        
        UITapGestureRecognizer * tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
        tapPress.numberOfTapsRequired=1;
        tapPress.delegate=self;
        tapPress.delaysTouchesBegan=YES;
        tapPress.cancelsTouchesInView=YES;
        [selectFavLabel addGestureRecognizer:tapPress];
    }
    return self;
}

-(void)tapPress: (UIGestureRecognizer*) hold{
    
    NSString * accessToken =[[User getInstance] fetchAccessToken];
    
    if (accessToken==nil) {
        [WSCore showLoginSignUpAlert];
        
        return;
    }
    
    if (self.isFav) {
        
        
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_Style_Favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&style_id=%@",self.styleID]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&unmark=false"]];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
            if (data && dict!=nil) {
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                        NSLog(@"Data dict /n %@",dict);
                        if ([dict[@"success"] isEqualToString:@"true"]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.isFav = NO;
                                self.favImageView.image = [UIImage imageNamed:@"White_Heart_With_Gray_Solid"];
                                self.favImageView.contentMode = UIViewContentModeScaleToFill;
                                
                                if (self.isFromFavScreen) {
                                    NSLog(@"From Fav");
                                    [self postNotificationFavRemoveFavourite:self.styleID];
                                }
                                else{
                                    [self postNotificationInspireRemoveFavourite:self.styleID];
                                }
                                
                                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                            });
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oh Oh" message:@"A network error occurred." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert show];
                            });
                        }
                        break;
                        
                    default:
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                            [WSCore showServerErrorAlert];
                        });
                        break;
                }
            }
        }];
        
        if ([WSCore isNetworkReachable]) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
            [task resume];
        }
        else{
            [WSCore showNetworkErrorAlert];
        }
        
        
        
        
        
    }else{
        
        
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_Style_Favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&style_id=%@",self.styleID]];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
            if (data) {
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                        NSLog(@"Data dict /n %@",dict);
                        if ([dict[@"success"] isEqualToString:@"true"]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.favImageView.image = [UIImage imageNamed:@"White_Heart_Solid.png"];
                                self.favImageView.contentMode = UIViewContentModeScaleToFill;
                                self.isFav = YES;
                                if (self.isFromFavScreen) {
                                  [self postNotificationFavIsFavourite:self.styleID];
                                }
                                else{
                                    [self postNotificationInspireIsFavourite:self.styleID];
                                }
                                
                                [self showLikeCompletion];
                                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                            });
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oh Oh" message:@"A network error occurred." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert show];
                            });
                        }
                        break;
                        
                    default:
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                            [WSCore showServerErrorAlert];
                        });
                        break;
                }
            }
        }];
        
        if ([WSCore isNetworkReachable]) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
            [task resume];
        }
        else{
            [WSCore showNetworkErrorAlert];
        }
        
        
    }
    
}

- (void)postNotificationFavIsFavourite:(NSString *)inspId //post notification method and logic
{
    NSString *notificationName = @"favIsFav";
    NSString *key = @"Inspire_id";
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:inspId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

- (void)postNotificationFavRemoveFavourite:(NSString *)inspId //post notification method and logic
{
    NSString *notificationName = @"favNotFavCell";
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:inspId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

- (void)postNotificationInspireIsFavourite:(NSString *)inspId //post notification method and logic
{
    NSString *notificationName = @"inspireIsFav";
    NSString *key = @"Inspire_id";
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:inspId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

- (void)postNotificationInspireRemoveFavourite:(NSString *)inspId //post notification method and logic
{
    NSString *notificationName = @"inspireNotFavCell";
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:inspId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}



-(void)showLikeCompletion{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Added to Favourites!" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    
    double delayInSeconds = 1.0;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });
    
}

-(void)setCellImageWithURL: (NSString*) url{
    
    if (url.length <1) {
        self.bgImageView.image = [UIImage imageNamed:@"empty_cell"];
        self.bgImageView.contentMode=UIViewContentModeScaleAspectFill;
        
        
    }
    else{
        [self.bgImageView sd_setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"empty_cell"]];
    }
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    const CGRect bounds = self.bounds;
    self.overlayImageView.frame =CGRectMake(0, 0, bounds.size.width, bounds.size.height);
    if(self.isBookable){
        self.bgImageView.frame = CGRectMake(0, 0, 140, bounds.size.height - 41);
        self.book.hidden = NO;
        self.infoLabel.frame=CGRectMake(6, bounds.size.height - 80, 129, 38);
        
        self.book.frame =CGRectMake(0, bounds.size.height - 41, 140, 41);
        
        self.book.text = @"Book the Look";
        self.book.textColor = [UIColor whiteColor];
        self.book.adjustsFontSizeToFitWidth=YES;
        self.book.textAlignment = NSTextAlignmentCenter;
        self.book.backgroundColor= [UIColor darkGrayColor];
        self.book.hidden=NO;
    }
    else{
        self.bgImageView.frame = CGRectMake(0, 0, bounds.size.width, bounds.size.height);
        self.book.hidden = YES;
        self.infoLabel.frame=CGRectMake(6, self.bgImageView.bounds.size.height - 38, 129, 38);
        [self.book removeFromSuperview];
        self.book=nil;
    }
}

@end
