//
//  GCNetworkManager.m
//  whatsalon
//
//  Created by Graham Connolly on 05/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "GCNetworkManager.h"


@interface GCNetworkManager ()<NSURLSessionDelegate, NSURLSessionDataDelegate>
@property (nonatomic) NSMutableData * recievedData;
@property (nonatomic) NSURLSessionDownloadTask * downloadTask;
@property (nonatomic) UIView * blackOverlay;
@end

@implementation GCNetworkManager


-(void)startSessionWithRequest: (NSMutableURLRequest *) request AndWithLoadingDialog: (BOOL)b{
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    
    self.dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        if (data!=nil) {
                    
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
       
       // NSLog(@"dict %@",dataDict);
        //1NSLog(@"Message %@",dataDict[@"message"]);
       // NSLog(@"SUccess %@",dataDict[@"success"]);
        if (!error && dataDict!=nil) {
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    
                    //success
                    if ([[dataDict[kSuccessIndex]  lowercaseString] isEqualToString:@"true"]||[dataDict[@"status"] isEqualToString:@"OK"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if (b){
                                [WSCore dismissNetworkLoadingOnView:self.parentView];
                            }else{
                                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                            }
                            
                            
                            if ([self.delegate respondsToSelector:@selector(manager:handleSuccessMessageWithDictionary:)] && error.code != NSURLErrorCancelled) {
                                [self.delegate manager:self handleSuccessMessageWithDictionary:dataDict];
                            }
                            else{
                                NSLog(@"Delegate not set");
                            }
                            
                        });
                        
                    }
                    
                    //fail
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if (b){
                                
                                [WSCore dismissNetworkLoadingOnView:self.parentView];
                            }else{
                                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                            }
                            
                            NSLog(@"Fail %@",dataDict);
                            if([self.delegate respondsToSelector:@selector(manager:handleFailureMessageWitDictionary:)]){
                                [self.delegate manager:self handleFailureMessageWitDictionary:dataDict];
                            }
                            else{
                                NSLog(@"Delegate not set");
                            }
                        });
                    }
                    
                    break;
                    
                default:
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (b) {
                            [WSCore dismissNetworkLoadingOnView:self.parentView];
                        }else{
                            [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                        }
                        if (!self.shouldNotShowMessages) {
                             [WSCore showServerErrorAlert];
                        }
                       
                    });
                    break;
            }
        }
        //error occurred or nil NSDictionary
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (b) {
                    [WSCore dismissNetworkLoadingOnView:self.parentView];
                }
                else{
                    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                }
                
                if (error.code != NSURLErrorCancelled) {
                    NSLog(@"Show server error");
                    if (!self.shouldNotShowMessages) {
                        [WSCore showServerErrorAlert];
                    }
                    
                    
                    if ([self.delegate respondsToSelector:@selector(manager:handleServerFailureWithError:)]) {
                        
                        NSLog(@"Responds to");
                        [self.delegate manager:self handleServerFailureWithError:error];
                    }
                }
                else{
                    NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                }

            });
        }
            
        }//end if data
        else{

            if (self.parentView) {
                NSLog(@"Remove networking loader on parent view");
                [WSCore dismissNetworkLoadingOnView:self.parentView];
            }
            else{
               [WSCore dismissNetworkLoading];
            }
            
            NSLog(@"No Data for view controller %@ \n tag %@",self.parentViewController,self.tagDescription);
        }
    }];
        
        
    
    if ([WSCore isNetworkReachable]) {
        if (b){
            
        
            [WSCore showNetworkLoadingOnView:self.parentView];
        }
        else{
            [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        }
        
        [self.dataTask resume];
    }
    else{
        if (!self.shouldNotShowMessages) {
            [WSCore showNetworkErrorAlert];
        }
        
    }
    

}


-(void)loadWithURL:(NSURL *)url withDictionary: (NSDictionary *)dict WithHTTPMethod: (NSString *) method  AndWithLoadingDialog: (BOOL) b{
    
    NSMutableArray * arr = [[NSMutableArray alloc] init];
    
    [arr addObject:dict];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arr options:0 error:nil];
    NSString *paramString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
  
    paramString = [paramString stringByReplacingOccurrencesOfString:@"[" withString:@""];
    paramString = [paramString stringByReplacingOccurrencesOfString:@"]" withString:@""];
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.f];
    
    [request setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:method];
    
    
    [self startSessionWithRequest:request AndWithLoadingDialog:b];
}


-(void)loadWithURL:(NSURL *)url WithHTTPMethod:(NSString *)method AndWithLoadingDialog:(BOOL)b{
   
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.f];
    
    [request setHTTPMethod:method];
    
    
    [self startSessionWithRequest:request AndWithLoadingDialog:b];
}

-(void)loadWithURL:(NSURL *)url withParams:(NSString *)params WithHTTPMethod: (NSString *) method AndWithLoadingDialog:(BOOL)b{
    
    //NSLog(@"******** \n\n Params %@ \n\n *******",params);
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.f];
    

    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:method];
    
    
    [self startSessionWithRequest:request AndWithLoadingDialog:b];
}


-(void)cancelTask{
    NSLog(@"Cancel Task");
    [self.dataTask cancel];
}


//loading progress indicator
-(void)progressLoaderWithURL:(NSURL *)url withParams:(NSString *)params WithHTTPMethod: (NSString *) method AndWithLoadingDialog:(BOOL)b{
    self.blackOverlay = [[UIView alloc] initWithFrame:self.parentView.frame];
    self.blackOverlay.backgroundColor = [UIColor blackColor];
    self.blackOverlay.alpha=0.7;
    [self.parentView addSubview:self.blackOverlay];
    self.progressView = [[UIProgressView alloc] init];
    self.progressView.frame = CGRectMake(100,100,200,30);
    [self.blackOverlay addSubview:self.progressView];
    self.progressView.center = self.blackOverlay.center;
    //[self.parentView bringSubviewToFront:self.progressView];
    
    //[self.progressView setMode:MRProgressOverlayViewModeDeterminateHorizontalBar];
    [self.progressView setProgress:0 animated:NO];
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.f];
    
    
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:method];

    [self startSessionProgressWithRequest:request AndWithLoadingDialog:YES];
}

-(void)startSessionProgressWithRequest: (NSMutableURLRequest *) request AndWithLoadingDialog: (BOOL)b{
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]delegate:self delegateQueue:nil];
    self.downloadTask = [session downloadTaskWithRequest:request];
    [self.downloadTask resume];
    
}
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    
    
    NSData *data = [NSData dataWithContentsOfURL:location];
    NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    if (dataDict!=nil) {
        //success
        if ([[dataDict[kSuccessIndex]  lowercaseString] isEqualToString:@"true"]||[dataDict[@"status"] isEqualToString:@"OK"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                [self.progressView setHidden:YES];
                self.blackOverlay.hidden=YES;
                
                if ([self.delegate respondsToSelector:@selector(manager:handleSuccessMessageWithDictionary:)]) {
                    [self.delegate manager:self handleSuccessMessageWithDictionary:dataDict];
                }
                
            });
            
        }
        
        //fail
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.blackOverlay.hidden=YES;
                self.progressView.hidden=YES;
                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                
                
                NSLog(@"Fail %@",dataDict);
                if([self.delegate respondsToSelector:@selector(manager:handleFailureMessageWitDictionary:)]){
                    [self.delegate manager:self handleFailureMessageWitDictionary:dataDict];
                }
            });
        }

    }
    else{
       
            NSLog(@"Show server error");
        [WSCore showServerErrorAlert];
    }
    
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    float progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    
    dispatch_async(dispatch_get_main_queue(), ^{
       [self.progressView setProgress:progress];
        //NSLog(@"Progress View %f",progress);
    });
}

-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    NSLog(@"Completed");
    
    
}

-(void)destroy{
    
    
    NSLog(@"Destroy");
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    [WSCore dismissNetworkLoading];
    [self cancelTask];
    self.delegate=nil;
    
    NSLog(@"***************** \n \n Destroyed \n\n ***************");
}
@end
