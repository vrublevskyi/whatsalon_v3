//
//  NoSalonsMessageView.h
//  whatsalon
//
//  Created by Graham Connolly on 02/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoSalonsMessageView : UIView

/*! @brief represents the view for holding the content. */
@property (strong, nonatomic) IBOutlet UIView *view;

/*! @brief represents the image view. */
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

/*! @brief represents the small text label. */
@property (strong, nonatomic) IBOutlet UILabel *smallText;

/*! @brief represents the main text label. */
@property (strong, nonatomic) IBOutlet UILabel *mainText;

/*! @brief represents the title text. */
@property (strong, nonatomic) IBOutlet UILabel *titleText;

@end
