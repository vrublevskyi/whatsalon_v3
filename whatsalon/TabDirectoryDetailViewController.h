//
//  TabDirectoryDetailViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 21/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalonTier4.h"

@interface TabDirectoryDetailViewController : UIViewController

/*! @brief represents the SalonTier4 object. */
@property (nonatomic) SalonTier4 * salon;
@end
