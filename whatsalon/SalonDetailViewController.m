//
//  SalonDetailViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 27/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonDetailViewController.h"
#import "WSCore.h"
#import "ReviewsViewController.h"
#import "ConfirmViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SalonInfoViewController.h"
#import "GetDirectionsViewController.h"
#import "LoginSignUpViewController.h"
#import "LinkCCViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "User.h"
#import "LoginSignUpViewController.h"
#import "SalonBookingModel.h"
#import "SalonService.h"
#import "UIView+AlertCompatibility.h"
#import "PhorestStaffMember.h"
#import "SalonAvailableTime.h"
#import "Booking.h"
#import "TimeCollectionViewCell.h"

#define kScrollViewPadding 20
#define kCheckForAppointmentButtonHeight 45.0f

@interface SalonDetailViewController ()<UIViewControllerTransitioningDelegate,UIPickerViewDataSource,UIPickerViewDelegate,ServicesDelegate,UICollectionViewDataSource,UICollectionViewDelegate>

//services picker
@property (nonatomic,copy) NSString * serviceTextLabel;//hold values for table view
@property (nonatomic,copy) NSString * serviceDetailLabel;//hold values for tableview

//stylist picker
@property (nonatomic,copy) NSString * stylistTextLabel;//hold values for tableview
@property (nonatomic,copy) NSString * stylistDetailLabel;//hold values for tableview
@property (nonatomic,copy) NSString * dateTextLabel;//hold values for tableview
@property (nonatomic,copy) NSString * dateDetailLabel;//hold values for table view


/*
    FOR SLIDESHOW/SCROLLVIEW
 */

@property (nonatomic,strong) NSMutableArray * pageViews;
-(void)loadVisiblePages;
-(void)loadPage: (NSInteger)page;
-(void)purgePage:(NSInteger)page;

//for views - shown during form selections
@property (nonatomic,weak) UIView* viewForServices;
@property (nonatomic,weak) UIView * viewForStylists;
@property (nonatomic,weak) UIView *viewForDate;
@property (nonatomic,weak) UIView * viewForLaterDate;
@property (nonatomic,weak) UILabel * preferedDayLabel;

/*
 BOOLEAN values
 */
@property (nonatomic,assign) BOOL isServicesFormFilled;
//@property (nonatomic,assign) BOOL isStylistFormFilled;
@property (nonatomic,assign) BOOL isTimeFormFilled;
@property (nonatomic,assign) BOOL isLaterDateViewShowing;



@property (nonatomic) NSString * dateDetailString;//holds date value
@property (nonatomic) UIDatePicker * laterDatePicker;
@property (weak, nonatomic) IBOutlet UISegmentedControl *optionsSegment;//today,Tomorrow,later
@property (nonatomic) CGFloat viewAdjustment;
@property (nonatomic) BOOL isFav;//determines if salon is favourited


@property (nonatomic,assign) BOOL isSlideshowOpen;//determines if the slide show is open
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *optionsViewContainerBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *checkAppointmentsBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pageControlViewTop;


//to keep track of selected and previously selected segment
@property (nonatomic) NSInteger oldSegmentIndex;
@property (nonatomic) NSInteger actualSegmentIndex;

/*
 Variables for Salon Booking
 */
@property (nonatomic) SalonBookingModel * salonBookingModel;
@property (nonatomic) NSMutableArray * arrayOfServices;
@property (nonatomic,assign) BOOL isArrayOfServicesDownloaded;
@property (nonatomic) NSMutableArray * workingStaffArray;
@property (nonatomic) UIPickerView * timePicker;
@property (nonatomic) NSMutableArray * timesArray;
@property (nonatomic,assign) BOOL isArrayOfStylistsDownloaded;
@property (nonatomic) int servicePickerIndex;
@property (nonatomic) int stylistPickerIndex;
@property (nonatomic) int timePickerIndex;
@property (nonatomic) Booking * bookObj;
@property (nonatomic) NSMutableArray * allStaffArray;


//collection view
@property (nonatomic) UICollectionView * collectionView;
@property (nonatomic) NSIndexPath * collectionViewSelectedIndex;

@property (nonatomic,assign) BOOL showCollectioViewTimesView;

@property (nonatomic) CGFloat h;

@property (nonatomic) NSTimer * scrollTimer;
@end

@implementation SalonDetailViewController

/*
 When using Card.io a “Unbalanced calls to begin/end appearance transitions for <UINavigationController>” error is called.
 
 */

-(void)createCollectionView{
    
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 44, self.bookingsTableView.frame.size.width, 120) collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource =self;
    
    [self.collectionView registerClass:[TimeCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.backgroundColor=[UIColor clearColor];
    self.collectionView.allowsMultipleSelection = NO;

}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.timesArray.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    TimeCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    SalonAvailableTime * time = [self.timesArray objectAtIndex:indexPath.row];
    cell.timeLabel.text = [time getTimeFromDate:time.start_time];
    cell.priceLabel.text = @"€20.00";
    cell.midLabel.hidden=YES;
    cell.layer.cornerRadius=6.0;
    cell.layer.masksToBounds=YES;
    cell.layer.borderColor=[UIColor whiteColor].CGColor;
    cell.layer.borderWidth=1.0f;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    TimeCollectionViewCell * cell = (TimeCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    cell.timeLabel.textColor = [UIColor blackColor];
    cell.priceLabel.textColor = [UIColor blackColor];
    cell.midLabel.textColor = [UIColor blackColor];
    self.collectionViewSelectedIndex=indexPath;
    
    NSLog(@"object %@",self.timesArray[indexPath.row]);
    
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    TimeCollectionViewCell * cell = (TimeCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    cell.timeLabel.textColor = [UIColor whiteColor];
    cell.priceLabel.textColor = [UIColor whiteColor];
    cell.midLabel.textColor = [UIColor whiteColor];
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(130, 60);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 15,10,15);
}

#pragma mark - Application lifecycle

-(NSString *)getChosenDay{
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init] ;
    [deltaComps setDay:1];
    NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
    NSDate * today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    switch (self.PreferredDay) {
        case PreferdDayToday:
            
          
            return [dateFormatter stringFromDate:today];
            
            break;
        case PreferdDayTomorrow:
            
            return [dateFormatter stringFromDate:tomorrow];
            break;
        case PrefferedDayLater:
           
            return [dateFormatter stringFromDate:[self.laterDatePicker date]];
            break;
            
        default:
            
            return [dateFormatter stringFromDate:today];
            break;
    }
    
  
    return [dateFormatter stringFromDate:today];
}

/*
 Changes the visual appearance of the button based on whether
 The user is logged in
 the user has a credit card
 
 */
- (void)checkIfUserIsLoggedInOrSignedUpOrNeedsToAddPaymentInfo
{
    NSLog(@"Users balance %ld chosen price %ld",(long)[[[User getInstance] fetchUsersBalance] integerValue], (long)[self.bookObj.chosenPrice integerValue]);
    
    if (![[User getInstance] isUserLoggedIn]) {
        
        [self.checkForAppointmentsButton setTitle:@"Login or Sign up" forState:UIControlStateNormal];
    }
    else if(![[User getInstance] hasCreditCard] && [[[User getInstance] fetchUsersBalance] integerValue] <=[self.bookObj.chosenPrice integerValue]){
        [self.checkForAppointmentsButton setTitle:@"Add payment info" forState:UIControlStateNormal];
    }
    else{
        
        [self.checkForAppointmentsButton setTitle:@"Make Booking" forState:UIControlStateNormal];
    }
    NSLog(@"Check");
    self.checkForAppointmentsButton.backgroundColor=[UIColor whiteColor];
    [self.checkForAppointmentsButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
   
}

/*
 Change the button to Book Now
 The form is filled in and the user is logged in and has a credit card assigned.
 Therefore we are ready to book
 */
-(void)setButtonToBookNow{
    
    NSLog(@"Time");
    [self.checkForAppointmentsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.checkForAppointmentsButton setBackgroundColor:kWhatSalonBlue];
    [self.checkForAppointmentsButton setTitle:@"Book Now" forState:UIControlStateNormal];
    [self.checkForAppointmentsButton setNeedsLayout];
}

- (void)setUpSalonDetails
{
    //adjust details
    self.nameOfSalon.text = self.salonData.salon_name;
    NSString * fullAddress = [self.salonData fetchFullAddress];
    
    self.addressOfSalon.numberOfLines = 0;
    self.addressOfSalon.lineBreakMode = NSLineBreakByWordWrapping;
    self.addressOfSalon.text = fullAddress;
    self.addressOfSalon.adjustsFontSizeToFitWidth=YES;
    
    self.salonImages = [self.salonData fetchSlideShowGallery];
    [self.salonImages removeLastObject];//the last object is the letter box
    
    //if there are no images have just show the default images
    if ([self.salonData fetchSlideShowGallery].count ==0) {
        [self.salonImages removeAllObjects];
        self.salonImages =self.salonData.placeHolderImageGallery;
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Images" message:[NSString stringWithFormat:@"Unable to donwload images for %@. Please try again later.",self.salonData.salon_name] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    
    switch ((int)self.salonData.ratings) {
        case 0:
            self.numberOfStars.image=[UIImage imageNamed:@"white_no_star_rating"];
            break;
        case 1:
            self.numberOfStars.image=[UIImage imageNamed:@"white_one_star_rating"];
            break;
        case 2:
            self.numberOfStars.image = [UIImage imageNamed:@"white_two_star_rating"];
            break;
        case 3:
            self.numberOfStars.image=[UIImage imageNamed:@"white_three_star_rating"];
            break;
        case 4:
            self.numberOfStars.image = [UIImage imageNamed:@"white_four_star_rating"];
            break;
        case 5:
            self.numberOfStars.image = [UIImage imageNamed:@"white_five_star_rating"];
            break;
        default:
            self.numberOfStars.image = [UIImage imageNamed:@"white_four_star_rating"];
            break;
    }

}

-(void)addLightBlurEffectToView: (UIView *) viewToBlur{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = viewToBlur.bounds;
        viewToBlur.backgroundColor=[UIColor clearColor];
        [viewToBlur insertSubview:visualEffectView atIndex:0];
    }
    else{
        
        viewToBlur.backgroundColor=[UIColor lightGrayColor];
        viewToBlur.alpha=0.9;
    }
}

-(void)addDarkBlurEffectToView: (UIView *) viewToBlur{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = viewToBlur.bounds;
        viewToBlur.backgroundColor=[UIColor clearColor];
        [viewToBlur insertSubview:visualEffectView atIndex:0];
    }
    else{
        
        viewToBlur.backgroundColor=[UIColor blackColor];
        viewToBlur.alpha=0.9;
    }

}

- (void)addLightBlurToTableViewBackground: (UITableView *) tableView
{
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = tableView.bounds;
        tableView.backgroundView=visualEffectView;
        
    }
    else{
        UIView *blurView = [[UIView alloc] initWithFrame:tableView.frame];
        blurView.backgroundColor=[UIColor lightGrayColor];
        blurView.alpha=0.9;
        tableView.backgroundView=blurView;
    }
}


- (void)addBlurredVisualEffect
{
    [self addLightBlurEffectToView:self.viewForSegmentedView];
    [self addDarkBlurEffectToView:self.viewForPageControl];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpDefaultValueState];
    //[self checkIfUserIsLoggedInOrSignedUpOrNeedsToAddPaymentInfo];
    [self setUpSalonDetails];
    
    [self navigationBarSetUp];//navigation bar set up
    [self scrollViewSetUp];//set up scroll view in background
    
    
    [self setUpSalonInfoTable];//set up the salon info table
    

    [self closeButtonSetUp];
    [self gestureRecognizersSetUp];
    
    [self setUpTableViewBackground];
    
    self.scrollview.backgroundColor=[UIColor blackColor];
    
    if (IS_iPHONE4) {
        self.viewAdjustment=88.0f;
    }
    else if(IS_iPHONE5_or_Above){
        self.viewAdjustment=0.0f;
    }
    
    if (self.salonData.is_favourite) {
        self.isFav = YES;
    }
    else{
        self.isFav=NO;
    }

   
    [self addBlurredVisualEffect];

    self.oldSegmentIndex = -1;
    self.actualSegmentIndex=self.optionsSegment.selectedSegmentIndex;
    
    self.salonBookingModel = [[SalonBookingModel alloc] init];
    self.salonBookingModel.view=self.view;
    self.salonBookingModel.myDelegate=self;
    [self.salonBookingModel fetchServicesListWithID:self.salonData.salon_id];
    [self.salonBookingModel fetchStaff:@""];
    self.arrayOfServices = [NSMutableArray array];
    
    self.workingStaffArray = [NSMutableArray array];
  
    self.timesArray = [NSMutableArray array];
    
    self.bookObj = [Booking bookingWithSalonID:self.salonData];
    
    self.allStaffArray = [NSMutableArray array];
    
    //self.showCollectioViewTimesView=YES;
    
    if (self.showCollectioViewTimesView) {
        [self createCollectionView];
    }
    
    self.scrollTimer =[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    
}

- (void)onTimer {
    
    
    NSInteger numberOfPages = self.salonImages.count - 1;
    if (self.pageControl.currentPage==numberOfPages) {
        /*
        [self.scrollview scrollRectToVisible:CGRectMake(0, 0, self.scrollview.frame.size.width, self.scrollview.frame.size.height) animated:NO];
        self.pageControl.currentPage=0;
        self.h = 0;
         */
        [self.scrollTimer invalidate];
        return;
    }
    
    // Updates the variable h, adding 100 (put your own value here!)
    self.h += CGRectGetWidth(self.scrollview.frame);
    
    //This makes the scrollView scroll to the desired position
   [self.scrollview setContentOffset:CGPointMake(self.h, 0) animated:YES];
    
    //[self loadVisiblePages];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self.scrollTimer invalidate];
    NSLog(@"User touch");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNeedsStatusBarAppearanceUpdate];
    
    CGSize pageScrollViewSize = self.scrollview.frame.size;
    
    if (IS_iPHONE4) {
        self.scrollview.contentSize = CGSizeMake((pageScrollViewSize.width+kScrollViewPadding) * self.salonImages.count, self.view.bounds.size.height);
    }
    else{
        
        self.scrollview.contentSize = CGSizeMake((pageScrollViewSize.width+kScrollViewPadding) * self.salonImages.count, pageScrollViewSize.height);
    }
 
   [self loadVisiblePages];
    
   
    
   
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    if (self.isMovingFromParentViewController) {
       
        self.salonImages = nil;
        self.pageViews = nil;
        self.closeScrollDetailLabel = nil;
        self.pageControl = nil;
        self.scrollview=nil;
        self.salonData=nil;
    }
}

-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];

    self.scrollview.frame = CGRectMake(self.scrollview.frame.origin.x, self.scrollview.frame.origin.y, self.scrollview.frame.size.width+kScrollViewPadding, self.view.bounds.size.height);
}

#pragma mark - Set Up
- (void)navigationBarSetUp
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
    //set back button arrow color
    UIBarButtonItem *btnHeart;
    if (self.salonData.is_favourite) {
         btnHeart = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"HeartSolid"] style:UIBarButtonItemStyleBordered target:self action:@selector(markAsFavourite)];
    }
    else{
         btnHeart = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Heart"] style:UIBarButtonItemStyleBordered target:self action:@selector(markAsFavourite)];
    }
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
   
    UIBarButtonItem * btnMap = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Map"] style:UIBarButtonItemStyleBordered target:self action:@selector(getDirections)];
    //UIBarButtonItem * btnBulb = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"inspireBarButtonItem"] style:UIBarButtonItemStyleBordered target:self action:nil];
    
    //[self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnMap, btnHeart,btnBulb, nil]];
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnMap, btnHeart, nil]];
}


- (void)setUpTableViewBackground
{
    self.bookingsTableView.backgroundColor = [UIColor clearColor];
    [self addLightBlurToTableViewBackground: self.bookingsTableView];
    
}

- (void)closeButtonSetUp
{
   
    self.closeScrollDetailLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.view.frame.origin.y - 30, 50, 30)];
    self.closeScrollDetailLabel.text = @"Close";
    self.closeScrollDetailLabel.textColor = [UIColor whiteColor];
    self.closeScrollDetailLabel.font = [UIFont  systemFontOfSize:14.0];
    self.closeScrollDetailLabel.layer.cornerRadius=6.0;
    self.closeScrollDetailLabel.layer.masksToBounds=YES;
    self.closeScrollDetailLabel.alpha=0.6;
    self.closeScrollDetailLabel.textAlignment = NSTextAlignmentCenter;
    self.closeScrollDetailLabel.backgroundColor= [UIColor blackColor];
    self.closeScrollDetailLabel.userInteractionEnabled=YES;
    [self.view addSubview:self.closeScrollDetailLabel];
}

/*
 Handles the default value state
 set the serviceTextLabel to Services
 set the serviceDetailLabel to Pick One
 set the stylistTextLabel to Stylist
 set the stylistDetailLabel to Any
 set the dateTextLabel to Best Time
 set the dateDetailLabel to "-"
 set the PreferredDay to PreferedDayToday
 
 set isSlideshowOpen to NO
 set isServicesFormFilled to NO
 set isStylistFormFilled to NO
 set isTimeFormFilled to NO
 set isLaterDateShowing to NO
 
 return pickerIndexes to default
 
 set the collectionViewSelectedIndex to -
 
 check if user is logged in or signed up or needs to add payment info
 
 */
- (void)setUpDefaultValueState
{
    //setting up variables for labels
    self.serviceTextLabel = @"Services";
    self.serviceDetailLabel = @"Pick one";
    self.stylistTextLabel = @"Stylist";
    self.stylistDetailLabel = @"Any";
    self.dateTextLabel = @"Best Time";
    self.dateDetailString = @"Any Time";
    self.PreferredDay=PreferdDayToday;
    
    //Bools
    self.isSlideshowOpen=NO;
    self.isServicesFormFilled=NO;
   // self.isStylistFormFilled=NO;//
    self.isTimeFormFilled=NO;
    self.isLaterDateViewShowing=NO;
   
    
    self.servicePickerIndex=1;//The header is at index 0, headers are not selectable, there start at 1
    self.stylistPickerIndex=0;
    self.timePickerIndex=0;
    
    self.collectionViewSelectedIndex=0;
    
    [self checkIfUserIsLoggedInOrSignedUpOrNeedsToAddPaymentInfo];
    
}

- (void)setUpSalonInfoTable
{
    self.viewContainerOptions.backgroundColor = [UIColor clearColor];//tableview set up
    self.bookingsTableView.delegate = self;
    self.bookingsTableView.dataSource = self;
    self.reviewButton.layer.cornerRadius = 10;
    self.reviewButton.clipsToBounds = YES;
    self.reviewButton.layer.borderWidth = 1;
    self.reviewButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    if ((int)self.salonData.reviews==1) {
        [self.reviewButton setTitle:[NSString stringWithFormat:@"%d review",(int)self.salonData.reviews] forState:UIControlStateNormal];
    }
    else{
        [self.reviewButton setTitle:[NSString stringWithFormat:@"%d reviews",(int)self.salonData.reviews] forState:UIControlStateNormal];
    }
    
    self.reviewButton.titleLabel.adjustsFontSizeToFitWidth=YES;
    [WSCore addBottomLine:self.viewForSegmentedView: [UIColor whiteColor]];
}

- (void)gestureRecognizersSetUp
{
    //tap to close Detail view
    UITapGestureRecognizer * tapToCloseDetailView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showFormViewAnimation)];
    tapToCloseDetailView.numberOfTapsRequired=1;
    [self.closeScrollDetailLabel addGestureRecognizer:tapToCloseDetailView];
    
    //swipe gesture
    UISwipeGestureRecognizer *swipeGestureDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreenDown:)];
    swipeGestureDown.numberOfTouchesRequired = 1;
    swipeGestureDown.direction = (UISwipeGestureRecognizerDirectionDown);
    [self.view addGestureRecognizer:swipeGestureDown];
    
    UISwipeGestureRecognizer *swipeGestureUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreenUp:)];
    swipeGestureUp.numberOfTouchesRequired = 1;
    swipeGestureUp.direction = (UISwipeGestureRecognizerDirectionUp);
    [self.view addGestureRecognizer:swipeGestureUp];
    
    //scroll Tap
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(scrollTap)];
    tap.numberOfTapsRequired=1;
    [self.scrollview addGestureRecognizer:tap];
    
    
    //Tap Name
    UITapGestureRecognizer * tapName = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapName)];
    [self.nameOfSalon addGestureRecognizer:tapName];
}

#pragma mark - mark as fovourite

-(void)markAsFavourite{

    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey];
    if (accessToken==nil) {
        [WSCore showLoginSignUpAlert];
        return;
    }
    self.navigationItem.rightBarButtonItems = nil;
    UIBarButtonItem * btnHeart;
    if (self.isFav) {
        btnHeart = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Heart"] style:UIBarButtonItemStyleBordered target:self action:@selector(markAsFavourite)];
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonData.salon_id]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&unmark=false"]];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
            if (data) {
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                       
                        if ([dict[@"success"] isEqualToString:@"false"]) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [UIView showSimpleAlertWithTitle:@"Oops" message:@"An error occurred." cancelButtonTitle:@"OK"];
                            });
                        }
                        break;
                        
                    default:
                        break;
                }
            }
        }];
        
        if ([WSCore isNetworkReachable]) {
            [task resume];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self postNotificationWithSalonRemoveFavourite:self.salonData.salon_id];
        });
        
        self.isFav=NO;
    }
    else{
        
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",[[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonData.salon_id]];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
            if (data) {
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                     
                        if ([dict[@"success"] isEqualToString:@"false"]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [UIView showSimpleAlertWithTitle:@"Oops" message:@"An error occurred." cancelButtonTitle:@"OK"];
                            });
                        }
                        break;
                        
                    default:
                        break;
                }
            }
        }];
        
        if ([WSCore isNetworkReachable]) {
            [task resume];
        }
        self.isFav=YES;
        self.navigationItem.rightBarButtonItem.tintColor = UIColor.redColor;
        btnHeart = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"HeartSolid"] style:UIBarButtonItemStyleBordered target:self action:@selector(markAsFavourite)];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self postNotificationWithSalonIsFavourite:self.salonData.salon_id];
            [self showLikeCompletion];
        });
    }
    
    
    UIBarButtonItem * btnMap = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Map"] style:UIBarButtonItemStyleBordered target:self action:@selector(getDirections)];
    UIBarButtonItem * btnBulb = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"inspireBarButtonItem"] style:UIBarButtonItemStyleBordered target:self action:nil];
    
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnMap, btnHeart,btnBulb, nil]];
    
}

-(void)showLikeCompletion{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Added to Favourites!" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    
    double delayInSeconds = 1.0;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });
    
}




#pragma mark - get directions
-(void)getDirections{
    
    GetDirectionsViewController *viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
    viewController.modalPresentationStyle = UIModalPresentationCustom;
    viewController.transitioningDelegate = self;
    viewController.salonMapData = self.salonData;
    
    [self presentViewController:viewController animated:YES completion:NULL];
    
}




#pragma mark - UIGesture recognizer selector methods
-(void)tapName{
    
    [self.scrollTimer invalidate];
    
    SalonInfoViewController *viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"SalonInfo"];
    viewController.modalPresentationStyle = UIModalPresentationCustom;
    viewController.transitioningDelegate = self;
    viewController.salonInfoData = self.salonData;
    viewController.prices = self.arrayOfServices;
    
    [self presentViewController:viewController animated:YES completion:NULL];
}

-(void)scrollTap{
    
    if (self.isLaterDateViewShowing==NO) {
        
        if (self.isSlideshowOpen == NO) {
            
            [self.navigationController setNavigationBarHidden:YES animated:YES];
            [self hideOptionsViewAnimation];
            self.isSlideshowOpen =YES;
            
        
        }else{
            
            [self showFormViewAnimation];
            self.isSlideshowOpen =NO;
        }
        
        
    }
    
    
}

-(void)swipedScreenDown:(UISwipeGestureRecognizer*)swipeGesture {
    if (self.isSlideshowOpen == NO) {
        [self.viewBlackLayer setHidden:YES];
        self.viewBlackLayer.alpha=0.0;
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [self hideOptionsViewAnimation];
        self.isSlideshowOpen =YES;
        
    }
}

-(void)swipedScreenUp:(UISwipeGestureRecognizer*)swipeGesture {
    if (self.isSlideshowOpen == YES) {
        [self showFormViewAnimation];
        self.isSlideshowOpen =NO;
    }
}




#pragma mark - UITableView datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = nil;
    
    if (indexPath.row == 0 || indexPath.row ==1 || indexPath.row ==2) {
        
        cell = [self.bookingsTableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = self.serviceTextLabel;
                cell.detailTextLabel.text = self.serviceDetailLabel;
                break;
            case 1:
                cell.textLabel.text = self.stylistTextLabel;
                cell.detailTextLabel.text = self.stylistDetailLabel;
                break;
            case 2:
                cell.textLabel.text = @"Best Time";
                cell.detailTextLabel.text = self.dateDetailString;
                break;
            default:
                break;
        }
    }
    
    cell.clipsToBounds=YES;
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
   
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    //return the normal height
    return tableView.rowHeight;
}





/*
 
 BOOKING
 
 */
- (void)resetServicesAndTimesCellValues {
    
    
    //Reset tables values for Time Service and Time
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell *cell = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.text = @"Any";
    self.stylistPickerIndex=0;
    
    //times cell
    indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    UITableViewCell * cell2 = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
    cell2.detailTextLabel.text =@"Any Time";
    self.timePickerIndex=0;
}

/*
 Creates the UIElements for the Services View.
 UIView view for holding the elements
 UILabel servicesNameLabel for the Services title
 UIButton cancelButton 
 UIButton nextButton
 UIPickerView ServicesPicker
 Adds Topline
 Adds background blur
 
 */
- (void)createServicesView {
    //create the view
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, [WSCore screenSize].height-225, [WSCore screenSize].width, 225)];
    self.viewForServices = view;
    
    //Center label
    UILabel * servicesNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.viewForServices.frame)/2-75, 11, 150, 21)];
    servicesNameLabel.text = @"Services";
    servicesNameLabel.adjustsFontSizeToFitWidth=YES;
    servicesNameLabel.font = [UIFont systemFontOfSize:17.0];
    servicesNameLabel.textColor = [UIColor whiteColor];
    servicesNameLabel.textAlignment=NSTextAlignmentCenter;
    [self.viewForServices insertSubview:servicesNameLabel atIndex:1];
    
    //cancel
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 2, 80, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelServicesForm) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.viewForServices insertSubview:cancelButton atIndex:1];
    
    //button
    UIButton * nextButton = [[UIButton alloc] initWithFrame:CGRectMake(235, 2, 80, 40)];
    [nextButton setTitle:@"Done" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(doneServicesForm) forControlEvents:UIControlEventTouchUpInside];
    nextButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.viewForServices insertSubview:nextButton atIndex:1];
    
    //services picker
    self.servicesPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 320, 163)];
    self.servicesPicker.delegate=self;//.servicesDataSource;
    self.servicesPicker.dataSource=self;//.servicesDataSource;
    [self.servicesPicker selectRow:self.servicePickerIndex inComponent:0 animated:NO];
    
    //UI customixation
    [WSCore addTopLine:self.servicesPicker :[UIColor whiteColor] :0.5];
    [self.viewForServices addSubview:self.servicesPicker];
    [self.view insertSubview:self.viewForServices belowSubview:self.checkForAppointmentsButton];
    [self addLightBlurEffectToView:self.viewForServices];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
            
        /*
        Services Row
             
        */
        case 0:{
            
            /*
             If the array of services is not downloaded yet, inform the user.
             
             */
            if (self.isArrayOfServicesDownloaded==NO) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView showSimpleAlertWithTitle:@"Please Wait" message:@"Please wait, the list of available services are still downloading" cancelButtonTitle:@"OK"];
                });
                
                return;
                break;
            }
            
            
            /*
             The user is selecting options so disable booking button and other option.
             
             */
            [self disableScreenElements];
            
            //creates the view for the services
            [self createServicesView];
       
            //hide the table
            [self showPickerOptions];
           
        }
            break;
            
        /*
            Stylist Row
        */
            
        case 1:
            
            /*
             
             Check if the array of stylists have been downloaded, this is downloaded following the selection of a service. Also the isServiceFormFilled will be set to true
             */
            if (self.isArrayOfStylistsDownloaded==NO && self.isServicesFormFilled==NO) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please pick a service in order to get the available stylists." cancelButtonTitle:@"OK"];
                });
                
                return;
             
                break;
            }
            else if(self.workingStaffArray.count<=1){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView showSimpleAlertWithTitle:@"Bummer!" message:@"There are no stylists available for that service today. Please try another service, or try another day." cancelButtonTitle:@"OK"];
                });
                
                return;
                
                break;
            }
            
            
            [self showStylistView];
            [self showPickerOptions];

            break;
            
        /*
         Time row
         
         */
        case 2:
            
            //if the array of stylists is not downloaded then a service needs to be picked. After a service is picked, an array of stylists are downloaded.
            if (self.isArrayOfStylistsDownloaded==NO) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please pick a service in order to get the available times." cancelButtonTitle:@"OK"];
                });
                
                return;
                break;
            }
            /*
             If the Array of stylists is less than or equal to one, then the only option in the array is 'Any Stylist', and no other options. Therefore there are no staff on so Any Stylist is redundant.
             
             Show alert
             */
            else if(self.workingStaffArray.count<=1){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView showSimpleAlertWithTitle:@"Bummer!" message:@"There are no stylists available for that service today. Please try another service, or try another day." cancelButtonTitle:@"OK"];
                });
                
                return;
                
                break;
            }
            
            //set laterDatePicker to nil in order to free up memory for reinstantiation
            self.laterDatePicker=nil;
            
            //creates a view for holding the times form
            [self showTimesView];
            
            //hides the form etc
            [self showPickerOptions];
            break;
            
        default:
            break;
    }
    
}


#pragma mark - Rest the bookings table
-(void)resetBookingsTable{
    
    [self.bookingsTableView reloadData];
    self.scrollview.userInteractionEnabled=YES;
    self.optionsSegment.userInteractionEnabled=YES;
    self.checkForAppointmentsButton.backgroundColor = [UIColor whiteColor];
    [self.checkForAppointmentsButton setTitle:@"Check for Appointments" forState:UIControlStateNormal];
    [self.checkForAppointmentsButton setTitleColor:[UIColor colorWithRed:1.0/255 green:148.0/255 blue:199.0/255 alpha:1.0] forState:UIControlStateNormal];

    
}

#pragma mark - form actions

#pragma mark - Services Form
-(void)cancelServicesForm{
    
    [self enableScreenElements];
    
    self.viewForServices.hidden=YES;
    self.viewForServices=nil;
    [self.viewForServices removeFromSuperview];
    self.viewForServices.hidden=YES;
    [self.servicesPicker removeFromSuperview];
    
    [self finishedPickingOptions];
}
-(void)doneServicesForm{
    
    
    
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    UITableViewCell * cell = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
    
    NSInteger row = [self.servicesPicker selectedRowInComponent:0];
    
     SalonService * pServ = [self.arrayOfServices objectAtIndex:row];
    
    /*
     if the service is a header then it is not selectable
     
     */
     if (pServ.isHeader==YES) {
         ///Do nothing
         
         
         /*
         cell.detailTextLabel.text = @"Pick One";
         self.serviceDetailLabel = @"Pick One";
         self.isServicesFormFilled=NO;
         self.bookObj.service_id=@"";
          */
         
         
     
     }
     else if(pServ.isHeader==NO){
         
         //if the stylists have already been downloaded, then we need to reset them. This is because the user is making a new selection.
         if (self.isArrayOfStylistsDownloaded) {
             
             /*
              Return to default state
              */
             [self resetServicesAndTimesCellValues];
             [self setUpDefaultValueState];
         }
        
         cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", pServ.service_name ];
         
         self.serviceDetailLabel = pServ.service_name;
         
         //hold onto the picker index for future reference
         self.servicePickerIndex=(int)row;
         
         //add to booking object
         self.bookObj.service_id=pServ.service_id;
         self.bookObj.chosenPrice=pServ.price;
         self.bookObj.chosenService=pServ.service_name;
         
         
         [self.salonBookingModel fetchStaffAndTimesWithSalonID:@"" WithServiceID:pServ.service_id WithStaffID:@"" WithStartDatetime:[NSString stringWithFormat:@"%@ 00:00:00",[self getChosenDay]] WithEndDateTime:[NSString stringWithFormat:@"%@ 23:59:59",[self getChosenDay]]];
         
         //the services form is filled
        self.isServicesFormFilled=YES;
         
        [self enableScreenElements];
        self.viewForServices.hidden=YES;
        self.viewForServices=nil;
        [self.viewForServices removeFromSuperview];
        self.viewForServices.hidden=YES;
        [self.servicesPicker removeFromSuperview];
        
        [self finishedPickingOptions];
     }
    
}

#pragma mark - Stylist Form
/*
 Creates the UIView for holding the stylist view
 Adds UILabel for Stylist title
 UIButton cancelButton
 UIButton doneButton
 UIPickerView stylistPicker
 UICustomisation
 */
-(void)showStylistView{

    [self disableScreenElements];
    

    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, [WSCore screenSize].height-225, [WSCore screenSize].width, 225)];

    UILabel * stylistNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(view.frame)/2-75, 11, 150, 21)];
    stylistNameLabel.text = @"Stylist";
    stylistNameLabel.adjustsFontSizeToFitWidth=YES;
    stylistNameLabel.font = [UIFont systemFontOfSize:17.0];
    stylistNameLabel.textColor = [UIColor whiteColor];
    stylistNameLabel.textAlignment=NSTextAlignmentCenter;
    [view insertSubview:stylistNameLabel atIndex:1];
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 2, 80, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelStylistForm) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [view insertSubview:cancelButton atIndex:1];
    
    UIButton * doneButton = [[UIButton alloc] initWithFrame:CGRectMake(240, 2, 80, 40)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneStylistForm) forControlEvents:UIControlEventTouchUpInside];
    doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    doneButton.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    [view insertSubview:doneButton atIndex:1];
    
    
    self.stylistPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 320, 163)];
    self.stylistPicker.delegate=self;
    self.stylistPicker.dataSource = self;
    [self.stylistPicker selectRow:self.stylistPickerIndex inComponent:0 animated:NO];
  
    
    [WSCore addTopLine:self.stylistPicker :[UIColor whiteColor] :0.5];
    [view insertSubview:self.stylistPicker atIndex:1];
    self.viewForStylists = view;
    [self.view insertSubview:self.viewForStylists belowSubview:self.checkForAppointmentsButton];
    
    [self addLightBlurEffectToView:self.viewForStylists];
    
}

-(void)cancelStylistForm{
    self.viewForStylists.hidden=YES;
    [self.viewForStylists removeFromSuperview];
    self.viewForStylists = nil;
    self.stylistPicker.hidden=YES;
    [self.stylistPicker removeFromSuperview];
    
    [self enableScreenElements];
    [self finishedPickingOptions];

    
    
}

/*
 The User can pick either a specific stylist or any stylist option
 
 */
-(void)doneStylistForm{
    
    NSInteger row = [self.stylistPicker selectedRowInComponent:0];
    PhorestStaffMember * staffMember = [self.workingStaffArray objectAtIndex:row];
    
    /*
     Available workers are selectable
     
     */
    if (staffMember.isWorking) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        UITableViewCell * cell = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
        [self.timesArray removeAllObjects];
        
        if (!staffMember.isAnyStylist) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@",staffMember.staff_first_name,staffMember.staff_last_name];
            
            [self.timesArray addObjectsFromArray:staffMember.times];
            
            self.bookObj.chosenStylist = [NSString stringWithFormat:@"%@ %@",staffMember.staff_first_name,staffMember.staff_last_name];
            self.bookObj.staff_id = staffMember.staff_id;
            
        }
        else{
            
            /*
             User wants any stylist
             */
            
            //TODO: Picker random stylist
            self.bookObj.chosenStylist = @"Any Stylist";
            self.bookObj.staff_id=@"";
            
            cell.detailTextLabel.text = @"Any Stylist";
            
            //creates an array of all available times rather than times specific for one stylist
            [self createAllTimesArray];
        }
        
        
        //save the index of the stylist for reference
        self.stylistPickerIndex=(int)row;
        
        self.viewForStylists.hidden=YES;
        [self.viewForStylists removeFromSuperview];
        self.viewForStylists = nil;
        self.stylistPicker.hidden=YES;
        [self.stylistPicker removeFromSuperview];
        
        [self enableScreenElements];
        [self finishedPickingOptions];
    }
    else{
        
        [UIView showSimpleAlertWithTitle:@"Oh Oh" message:[NSString stringWithFormat:@"%@ %@ is unavailble today. Please try another stylist.",staffMember.staff_first_name,staffMember.staff_last_name] cancelButtonTitle:@"OK"];
    }
    
    
    
    
}

/*
 Creates an array of all times
 
 */
-(void)createAllTimesArray{
    
    for (int i=0; i<self.workingStaffArray.count; i++) {
        
        PhorestStaffMember * staffM = self.workingStaffArray[i];
        
        
        if (!staffM.isAnyStylist)
            [self.timesArray addObjectsFromArray:staffM.times];
    }
    
    //sort the array of times by there start time
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"start_time"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [self.timesArray sortedArrayUsingDescriptors:sortDescriptors];
    
    //remove all previous times
    [self.timesArray removeAllObjects];
    [self.timesArray addObjectsFromArray:sortedArray];
    
    
}

#pragma mark - Times form
/*
Creates a UIView for holding Time form
 Disables certain screen elements
 UIView for holding elements
 UILabel for the title of the label
 UIButton cancelButton
 UIButton doneButton
 
 Has a conditional statement whether to show a carousel or a picker
 
 UIPickerView timePicker
 UICollectionView collectionView
*/
-(void)showTimesView{
    
    [self disableScreenElements];
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, [WSCore screenSize].height-225, [WSCore screenSize].width, 225)];
    
    UILabel * timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(view.frame)/2-100, 11, 200, 21)];
    timeLabel.text = @"Available Times";
    timeLabel.adjustsFontSizeToFitWidth=YES;
    timeLabel.font = [UIFont systemFontOfSize: 17.0];
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.textAlignment=NSTextAlignmentCenter;
    [view insertSubview:timeLabel atIndex:1];
    
    
    //cancel
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 2, 80, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelTimesForm) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [view insertSubview:cancelButton atIndex:1];
    
    UIButton * doneButton = [[UIButton alloc] initWithFrame:CGRectMake(240, 2, 80, 40)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneTimesForm) forControlEvents:UIControlEventTouchUpInside];
    doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    doneButton.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    [view insertSubview:doneButton atIndex:1];
    
    /*
    UIDatePicker * datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 44, 320, 163)];
    datePicker.clipsToBounds=YES;
    datePicker.datePickerMode=UIDatePickerModeTime;
    datePicker.minuteInterval = 15;
    if (self.PreferredDay==PrefferedDayLater) {
        datePicker.date=[WSCore getConfiguredStartDate];
        //datePicker.date = datePicker.date;
        //datePicker.minimumDate=nil;
    }
    else if(self.PreferredDay ==PreferdDayTomorrow){
        datePicker.date=[WSCore getConfiguredStartDate];
        //datePicker.date =datePicker.date;
        //datePicker.minimumDate=nil;
    }else{
        datePicker.date=[WSCore getConfiguredStartDate];
        datePicker.minimumDate=datePicker.date;
    }
    
    
    
    [WSCore addTopLine:datePicker :[UIColor whiteColor] :0.5];
    self.datePicker=datePicker;
    [view insertSubview:self.datePicker atIndex:1];
     */
    
    if (!self.showCollectioViewTimesView) {
        self.timePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 320, 163)];
        self.timePicker.delegate=self;
        self.timePicker.dataSource = self;
        [self.timePicker selectRow:self.timePickerIndex inComponent:0 animated:NO];
        
        
        
        [WSCore addTopLine:self.timePicker :[UIColor whiteColor] :0.5];
        [view insertSubview:self.timePicker atIndex:1];
    }
    else{
        
        [self.collectionView reloadData];
        
        [WSCore addTopLine:self.collectionView :[UIColor whiteColor] :0.5];
        [view insertSubview:self.collectionView atIndex:1];
    }
    //removed for collection view
    /*
    self.timePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 320, 163)];
    self.timePicker.delegate=self;
    self.timePicker.dataSource = self;
    [self.timePicker selectRow:self.timePickerIndex inComponent:0 animated:NO];
   
    
    
    [WSCore addTopLine:self.timePicker :[UIColor whiteColor] :0.5];
    [view insertSubview:self.timePicker atIndex:1];
*/
    /*
    [self.collectionView reloadData];

    [WSCore addTopLine:self.collectionView :[UIColor whiteColor] :0.5];
    [view insertSubview:self.collectionView atIndex:1];
    
   */
    self.viewForDate = view;
    [self.view insertSubview:self.viewForDate belowSubview:self.checkForAppointmentsButton];
    
    [self addLightBlurEffectToView:self.viewForDate];
    
}

-(void)cancelTimesForm{
    
    [self enableScreenElements];
    self.viewForDate.hidden=YES;
    self.viewForDate = nil;
    [self.viewForDate removeFromSuperview];
    [self finishedPickingOptions];//animation
    //[self.datePicker removeFromSuperview];
    self.laterDatePicker =nil;
   
}

/*
 Two user scenarios when the done button is pressed. If the showCollectionViewTimesView is showing then collectionViewDoneTimesMethods are implemented, else the donePickerViewTimesMethods.
 */
-(void)doneTimesForm{
    
    
    if (self.showCollectioViewTimesView) {
        
        //collection view methods
        [self collectionViewDoneTimesMethods];
    }
    else{
        
        //picker view methods
        [self donePickerViewTimesMethods];
    }
    

    //if the user is logged it, set the button to book now
    if (([[User getInstance] isUserLoggedIn] && [[User getInstance] hasCreditCard]) ||([[User getInstance] isUserLoggedIn] && [[User getInstance] hasEnoughCreditForBooking:self.bookObj.chosenPrice])) {
    
        [self setButtonToBookNow];
    }
    
   
    
    
    
    
    
}

-(void)collectionViewDoneTimesMethods{
    NSInteger row = self.collectionViewSelectedIndex.item;
    SalonAvailableTime * time = [self.timesArray objectAtIndex:row];
    //end added for collection view
    
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    UITableViewCell *cell = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    
    
    
    if (self.PreferredDay == PreferdDayToday || self.PreferredDay == PreferdDayTomorrow){
        self.dateDetailString = time.start_time;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[time getTimeFromDate:time.start_time]];
        self.dateDetailLabel = [time getTimeFromDate:time.start_time];
        self.bookObj.chosenTime=[time getTimeFromDate:time.start_time];
        
    }
    else{
        self.dateDetailString=[NSString stringWithFormat:@"%@ - %@",self.laterDate,[time getTimeFromDate:time.start_time ]];
        cell.detailTextLabel.text = self.dateDetailString;
        self.bookObj.chosenTime=self.dateDetailString;
        
        
    }
    
    self.bookObj.room_id = time.room_id;
    self.bookObj.slot_id = time.slot_id;
    self.bookObj.staff_id= time.staff_id;
    self.bookObj.internal_start_datetime = time.internal_start_datetime;
    self.bookObj.internal_end_datetime = time.internal_end_datetime;
    
    self.timePickerIndex=(int)row;
    
    //end added
    
    self.isTimeFormFilled=YES;
    [self enableScreenElements];
    self.viewForDate.hidden=YES;
    self.viewForDate = nil;
    [self.viewForDate removeFromSuperview];
    [self finishedPickingOptions];
    self.laterDatePicker =nil;
    
}

/*
 Done methods for when the user selects a time using picker.
 */
-(void)donePickerViewTimesMethods{
    
    NSInteger row = [self.timePicker selectedRowInComponent:0];
    if (self.timesArray.count>0) {
        
        SalonAvailableTime * time = [self.timesArray objectAtIndex:row];
        
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        UITableViewCell *cell = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
        
        
        /*
         If the preferedDay is Today or Tomorrow then
         display the start time in human readable form on the cell
         assign this also to the dateDetailLabel
         set the chosen time of the booking object
         */
        if (self.PreferredDay == PreferdDayToday || self.PreferredDay == PreferdDayTomorrow){
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[time getTimeFromDate:time.start_time]];
            self.dateDetailLabel = [time getTimeFromDate:time.start_time];
            self.bookObj.chosenTime=[time getTimeFromDate:time.start_time];
            
        }
        
        /*
         If the preferedDay is not Today or Tomorrow then it is LATER date
         set the dateDetailString to the later chosen Date, plus the human readable time from the start_time
         display the cell.detailTextLabe.text to dateDetailString.
         assign it to the chosen time
         */
        else if(self.PreferredDay == PrefferedDayLater){
            
            self.dateDetailString=[NSString stringWithFormat:@"%@ - %@",self.laterDate,[time getTimeFromDate:time.start_time ]];
            cell.detailTextLabel.text = self.dateDetailString;
            self.bookObj.chosenTime=self.dateDetailString;
            
            
        }
        
        self.bookObj.room_id = time.room_id;
        self.bookObj.slot_id = time.slot_id;
        self.bookObj.staff_id= time.staff_id;
        self.bookObj.internal_start_datetime = time.internal_start_datetime;
        self.bookObj.internal_end_datetime = time.internal_end_datetime;
        
        //reference to the chosen index
        self.timePickerIndex=(int)row;
        
        //end added
        
        //the time form is filled
        self.isTimeFormFilled=YES;
        
        
        [self enableScreenElements];
        
        self.viewForDate.hidden=YES;
        self.viewForDate = nil;
        [self.viewForDate removeFromSuperview];
        [self finishedPickingOptions];
        self.laterDatePicker =nil;
    }
    
    

}
#pragma mark - Later Times Segment Form
/*
 When the later segment is pressed, show the later times view.
 disables certain screen elements
 showPickerOptions - hides the tableview form
 hide the viewForSegmentView - contains Today,Tomorrow, Later
 set isLaterDateShowing to Yes - the later date view is showing
 
 view - UiView for holding the UIDatePicker
 cancelButton
 doneButton
 
 set the options segment index to 2
 Creates a UIDatePicker that starts on the 3rd day (Today, and Tomorrow are already covered)
 */

-(void)showLaterTimesView{
    
    [self disableScreenElements];
    [self showPickerOptions];
    self.viewForSegmentedView.hidden=YES;
    self.isLaterDateViewShowing=YES;
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, [WSCore screenSize].height-225, [WSCore screenSize].width, 225)];
    self.laterDatePicker=nil;
    UIDatePicker * datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 24, 320, 163)];
    NSTimeInterval secondsPerHour = 3600;
    NSTimeInterval secondsPerDay = secondsPerHour * 24;
    datePicker.minimumDate= [[NSDate date] dateByAddingTimeInterval:2*secondsPerDay];
    datePicker.clipsToBounds=YES;
    datePicker.datePickerMode=UIDatePickerModeDate;
    self.laterDatePicker=datePicker;
    [WSCore addTopLine:view :[UIColor whiteColor] :0.5 WithYvalue:44.0f];
    [view insertSubview:self.laterDatePicker atIndex:1];
    
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 2, 100, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelLaterForm) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    cancelButton.titleEdgeInsets= UIEdgeInsetsMake(0, 10, 0, 0);
    [view insertSubview:cancelButton atIndex:2];
    
    //button
    UIButton * doneButton = [[UIButton alloc] initWithFrame:CGRectMake(240, 2, 80, 40)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneLaterForm) forControlEvents:UIControlEventTouchUpInside];
    doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    doneButton.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    [view insertSubview:doneButton atIndex:2];
    
    //self.bookingsTableView.hidden=YES;//hide table view
    self.viewForLaterDate = view;
    
    self.optionsSegment.selectedSegmentIndex=2;
    
    [self addLightBlurEffectToView:self.viewForLaterDate];
    
    [self.view insertSubview:self.viewForLaterDate belowSubview:self.checkForAppointmentsButton];
    
}

/*
 Action for cancel form
 enables screen elements
 hides the viewForLaterDate
 sets isLaterDateViewShowing to NO
 finished picking options so return the tableview form
 
 The user would have picker the later segment (index 2), on cancel, it will need to return to the last chosen segment.
 */
-(void)cancelLaterForm{
    
    [self enableScreenElements];
    self.viewForSegmentedView.hidden=NO;
    self.viewForLaterDate.hidden=YES;
    self.viewForLaterDate=nil;
    [self.viewForLaterDate removeFromSuperview];
    self.isLaterDateViewShowing=NO;
    [self finishedPickingOptions];
    
    
    //maybe reset everythinh
    if (self.oldSegmentIndex!= -1) {
        self.optionsSegment.selectedSegmentIndex = self.oldSegmentIndex;
        self.actualSegmentIndex = self.oldSegmentIndex;
        self.PreferredDay = self.actualSegmentIndex;//PreferredDay is an enum, therefore can be assigned 0,1,2. 0 being today, 1 being tomorrow, and 2 being later.
    }
    
   
}

/*
 Handles the selection of the done action on the later form
 enables some on screen elements
 reshows the viewForSegmentedView
 weekdayFormatter - NSDateFormatter for short hand display of date selection
 the date picked from the laterDatePicker is then assigned to a formattedDateString.
 Which is then added to a self.laterDate
 
 The date has been selected, but no time has been. Add this to the dateDetailString
 
 Assign the dateDetailString to the appropriate cells detailTextLabel
 
 set the viewForLaterDate to be hidden
 
 do some clean up
 
 set isLaterDateViewShowing to NO
 */
-(void)doneLaterForm{
    
    [self enableScreenElements];
    self.viewForSegmentedView.hidden=NO;
    
    
    NSDateFormatter *weekdayFormatter = [[NSDateFormatter alloc] init];
    [weekdayFormatter setDateStyle:NSDateFormatterMediumStyle];
    [weekdayFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSString *formattedDateString = [weekdayFormatter stringFromDate:[self.laterDatePicker date]];
    self.laterDate =formattedDateString;
    
    //NSDateFormatter * yyMMddDate = [[NSDateFormatter alloc] init];
    //[yyMMddDate setDateFormat:@"yyyy-MM-dd"];
    
    self.dateDetailString= [NSString stringWithFormat:@"%@ - %@",self.laterDate,@"No time"];
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    UITableViewCell *cell = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
    cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:16.0];
    cell.detailTextLabel.text=self.dateDetailString;
    
    self.viewForLaterDate.hidden=YES;
    self.viewForLaterDate = nil;
    [self.viewForLaterDate removeFromSuperview];
    [self finishedPickingOptions];
    self.isLaterDateViewShowing=NO;
    

    
}

#pragma mark - segment action
/*
 Resets the users selection i.e starts from scratch
 returns a number of values back to there default (beginning) state
 sets isArrayOfStylistsDownlaoded to NO
 resets the state of the form table
 */
-(void)resetUsersSelection{
    
    [self setUpDefaultValueState];
    
    self.isArrayOfStylistsDownloaded=NO;
    
    [self resetTable];
    
}

/*
 Return the table to its starting values
 */
-(void)resetTable{
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    UITableViewCell * tableViewCell = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
    tableViewCell.detailTextLabel.text = @"Any Time";
    
    indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    tableViewCell = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
    tableViewCell.detailTextLabel.text =@"Pick One";
    
    indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    tableViewCell = [self.bookingsTableView cellForRowAtIndexPath:indexPath];
    tableViewCell.detailTextLabel.text =@"Any";
}


/*
 Manages Segment selection
 Assigns the actualSegmentIndex to the oldSegmentIndex value
 Then reassigns the actualSegmentIndex to the current selected index
 
 if a segment is changed then all selection values are to be reset
 
 manages the selection of segments and assigns respective values
 
 */
- (IBAction)todayTomorrowOrLaterSegment:(UISegmentedControl *) sender {
    
    self.oldSegmentIndex = self.actualSegmentIndex;
    self.actualSegmentIndex = sender.selectedSegmentIndex;
    
    [self resetUsersSelection];
    
    switch (sender.selectedSegmentIndex) {
        
        /*
         Today is selected
         set PreferredDAy to PreferedDayToday
         set laterDate string to empty string
         */
        case 0:{
            self.laterDate=@"";
            self.PreferredDay = PreferdDayToday;
            
            
        }
            break;
            
        /*
         Tomorrow is selected
         set PreferredDay to PreferedDayTomorrow
         set laterDate string to empty string
         */
        case 1:{
            self.PreferredDay = PreferdDayTomorrow;
            self.laterDate = @"";
            
            break;
        }
            
        /*
         Later is selected
         set PreferredDay to PrefferedDayLater
         If laterDate view is not already showing then show
         */
        case 2:{
            self.PreferredDay = PrefferedDayLater;
            
            //show date picker
            if (self.isLaterDateViewShowing==NO)
                
                [self showLaterTimesView];
            
            break;
            
        }
        
        /*
         Default state i.e if none of the above are selected then
         PreferredDay is set to PreferedDayToday
         laterDate is set to an empty string
         */
        default:
            self.PreferredDay = PreferdDayToday;
            self.laterDate=@"";
            break;
    }
    
}


#pragma mark - Enable/Disable CheckForAppointments buttons
-(void)enableScreenElements{
 
    [self.optionsSegment setUserInteractionEnabled:YES];
    self.optionsSegment.alpha=1;
    self.scrollview.userInteractionEnabled=YES;
    self.checkForAppointmentsButton.userInteractionEnabled=YES;
    self.checkForAppointmentsButton.alpha=1.0;
    self.checkForAppointmentsButton.titleLabel.textColor=kDefaultTintColor;
}
-(void)disableScreenElements{
   
    [self.optionsSegment setUserInteractionEnabled:NO];
    self.optionsSegment.alpha=0.8;
    self.scrollview.userInteractionEnabled=NO;
    self.checkForAppointmentsButton.userInteractionEnabled=NO;
    self.checkForAppointmentsButton.alpha=0.6;
    self.checkForAppointmentsButton.titleLabel.textColor=[UIColor grayColor];
}


- (IBAction)checkButtonAction:(id)sender {
    
    
    
    /*
     Check if the forms are filled
     If they are, check users credentials
     */
    if (self.isServicesFormFilled== YES /*&& self.isStylistFormFilled ==YES*/ && self.isTimeFormFilled == YES) {
        
        [self checkForCredentials];
        self.scrollview.userInteractionEnabled=NO;
        
        
    }
    /*
     If the form is not filled show an Alert
     */
    else{
        
        [UIView showSimpleAlertWithTitle:@"Form" message:@"Please ensure to complete the form." cancelButtonTitle:@"OK"];
    }
    
    
    
}
#pragma mark - check for credentials
- (void)checkForCredentials {
    
    
    
   // NSString * accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey];
   // NSString * paymentRef = [[User getInstance] fetchCreditCardPaymentRef];
    
    /*
     Check if the user is not logger in
     */
    if (![[User getInstance] isUserLoggedIn]) {
        
        //Go to login controller
        LoginSignUpViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignupVC"];
        login.modalPresentationStyle=UIModalPresentationCurrentContext;
        [self presentViewController:login animated:YES completion:nil];
        
    }
    /*
     Check if the user has a credit card assigned
     */
    else if(![[User getInstance] hasCreditCard] && ![[User getInstance] hasEnoughCreditForBooking:self.bookObj.chosenPrice]){
        
        //show credit card
        LinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
        linkCC.modalPresentationStyle = UIModalPresentationCurrentContext;
        //linkCC.isOnlyAddingCreditCard=YES;
        linkCC.onlyAddCard=YES;
        [self presentViewController:linkCC animated:YES completion:nil];
        
        
    }
    /*
     If the user is logged in, and has a credit card attached to their account, then everything is ok
     */
    else{
        
        
        [self makeBooking];
        
        
        
    }
}

-(void)makeBooking{
    
    /*
     Makes a booking using the information provided during selection
     */
    
    NSLog(@"self.booking obj %@",self.bookObj.getBookingDescription);
    [self.salonBookingModel bookPhorestServiceWithUserID:[[User getInstance] fetchAccessToken] WithSalonID:@"" WithSlotID:self.bookObj.slot_id WIthServiceID:self.bookObj.service_id WithRoomID:self.bookObj.room_id WithInternalStartDateTime:self.bookObj.internal_start_datetime WithInternalEndDateTime:self.bookObj.internal_end_datetime AndStaff_id:self.bookObj.staff_id];
    
}

-(void)showPickerOptions{
    
    self.bookingsTableView.hidden=YES;
    self.viewForSegmentedView.hidden=YES;
}


-(void)finishedPickingOptions{
    
    self.bookingsTableView.hidden=NO;
    self.viewForSegmentedView.hidden=NO;
    
}






/**
 
 SCROLL VIEW METHODS
 
 */
#pragma mark - ScrollView SetUp Methods etc.

- (void)scrollViewSetUp
{
    
    self.scrollview.delegate = self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //page count
    NSInteger pageCount = self.salonImages.count;
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0.0,0.0,self.view.frame.size.width,40.0)];
    [self.pageControl setNumberOfPages:pageCount];
    [self.pageControl setCurrentPage:0];
    self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    [self.pageControl setBackgroundColor:[UIColor clearColor]];
    [self.viewForPageControl addSubview:self.pageControl];
    

    
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; i++) {
        [self.pageViews addObject:[NSNull null]];
    }

}

-(void)loadPage:(NSInteger)page{
    if (page < 0 || (page >= self.salonImages.count)) {
        //if its outside the range of what you have to display, then do nothing
        return;
    }
    
    UIView * pageView = [self.pageViews objectAtIndex:page];
    
    if ((NSNull*) pageView == [NSNull null]) {
       
        CGRect frame = self.view.bounds;//set to views bounds instead of scrollviews bounds because it doesnt work very well with autolayout
        frame.origin.x = (frame.size.width+kScrollViewPadding) * page;
        frame.origin.y = 0.0f;
        
        UIImageView * newPageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"empty_cell"]];
        [newPageView sd_setImageWithURL:[self.salonImages objectAtIndex:page] placeholderImage:[UIImage imageNamed:@"black"]];
        
        newPageView.contentMode = UIViewContentModeScaleToFill;
        newPageView.frame = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width, frame.size.height);

        [self.scrollview addSubview:newPageView];//parent
        
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
    
    
}

-(void)purgePage:(NSInteger)page{
    if (page <0 || page >= self.salonImages.count) {
        //if it's outside the range of what you have to display, then do nothing
        return;
    }
    
    //Remove a page from the scroll view and reset the container array
    UIView * pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull *)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        pageView = nil;
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
        
    }
    
}

-(void)loadVisiblePages{
    //first determine which page is currently visible
    CGFloat pageWidth = self.scrollview.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollview.contentOffset.x * 2.0f + pageWidth)/(pageWidth * 2.0f));
    
    self.pageControl.currentPage = page;
   

    NSInteger firstPage = page -1;
    NSInteger lastPage = page+1;
    
    //purge anything before the first page
    for (NSInteger i = 0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
    //load pages in our range
    for (NSInteger i = firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    //purge anything after the last page
    for (NSInteger i = lastPage +1; i<self.salonImages.count; i++) {
        [self purgePage:i];
    }
    
}


#pragma mark - UIScrollView Delegate

//All this does is ensure that as the scroll view is scrolled, the relevant pages are always loaded (and that the unnecessary pages are purged).
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    //load the pages that are now on screen
    [self loadVisiblePages];
}


/**
 
 END OF SCROLL VIEW METHODS
 
 */


#pragma mark - Custom animations

-(void)showFormViewAnimation{
    
    [self.viewBlackLayer setHidden:NO];
    self.optionsViewContainerBottom.constant=0;
    [self.viewContainerOptions setNeedsUpdateConstraints];
    self.checkAppointmentsBottom.constant=0;
    [self.checkForAppointmentsButton setNeedsUpdateConstraints];
    self.pageControlViewTop.constant=0;
    [self.viewForPageControl setNeedsUpdateConstraints];
    self.scrollview.userInteractionEnabled=NO;
    [UIView animateWithDuration:0.5  delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.viewBlackLayer.alpha = 0.45;
        
        self.closeScrollDetailLabel.frame = CGRectMake(5, self.view.frame.origin.y - 30, 50, 30);
        [self.viewContainerOptions layoutIfNeeded];
        [self.checkForAppointmentsButton layoutIfNeeded];
        [self.viewForPageControl layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        self.scrollview.userInteractionEnabled=YES;
        
    }];
    
     self.isSlideshowOpen = NO;
   
}

-(void)hideOptionsViewAnimation{
    
    self.optionsViewContainerBottom.constant=-CGRectGetHeight(self.viewContainerOptions.bounds);
    [self.viewContainerOptions setNeedsUpdateConstraints];
    self.checkAppointmentsBottom.constant = -CGRectGetHeight(self.checkForAppointmentsButton.bounds);
    [self.checkForAppointmentsButton setNeedsUpdateConstraints];
    self.pageControlViewTop.constant=-CGRectGetHeight(self.viewForPageControl.bounds);
    [self.viewForPageControl setNeedsUpdateConstraints];
    self.scrollview.userInteractionEnabled=NO;
    [UIView animateWithDuration:0.5  delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    
        self.closeScrollDetailLabel.frame = CGRectMake(5, 20, 50, 30);
        self.viewBlackLayer.alpha=0.0;
        
        [self.viewContainerOptions layoutIfNeeded];
        [self.checkForAppointmentsButton layoutIfNeeded];
        [self.viewForPageControl layoutIfNeeded];
        
        
        } completion:^(BOOL finished) {
            
            if (self.viewForStylists.hidden == NO || self.viewForServices.hidden == NO || self.viewForDate.hidden == NO) {
                
                self.viewForServices.hidden=YES;
                self.viewForStylists.hidden=YES;
                self.viewForDate.hidden=YES;
                self.bookingsTableView.hidden=NO;
                [self.viewBlackLayer setHidden:YES];
                self.scrollview.userInteractionEnabled=YES;//prevents the user from double tapping during the animation, which messes it up
            }
        }];

    
}






- (IBAction)reviewsButton:(id)sender {
    
    [self.scrollTimer invalidate];
    
    ReviewsViewController *viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"reviewController"];
    viewController.modalPresentationStyle = UIModalPresentationCustom;
    viewController.transitioningDelegate = self;
    
    [self presentViewController:viewController animated:YES completion:nil];
   
}


#pragma mark - prepareForSegue
/*
 Notifies the view controller that a segue is about to be performed
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    /*
     Invalidate the scroll timer for the background images
     */
    [self.scrollTimer invalidate];
    
    
    if ([segue.identifier isEqualToString:@"confirmBooking"]) {
        
        /*
         Go to confirm page
         create an instance of ConfirmViewController
         assign confirmViewControllers salonData instance our salonData instance
         assign confirmViewController bookingObj instance our bookObj instance
         */
        ConfirmViewController * confirmVC = (ConfirmViewController *)segue.destinationViewController;
        confirmVC.salonData = self.salonData;
        confirmVC.bookingObj=self.bookObj;
       
        
        /*
         Check which day is selected
         */
        switch (self.PreferredDay) {
                
            /*
             If today is selected then
             Assign chosenDay a string containing Today and the chosen Time
             */
            case PreferdDayToday:
                self.bookObj.chosenDay=[NSString stringWithFormat:@"Today, %@",self.bookObj.chosenTime];
                break;
                
            /*
             If tomorrow is selected as the preferred day
             Assign chosenday a string containing Tomorrow plus the chosenTime
             */
            case PreferdDayTomorrow:
                self.bookObj.chosenDay=[NSString stringWithFormat:@"Tomorrow, %@",self.bookObj.chosenTime];
                
                break;
                
            /*
             If later is selected as the preferred day
             Assign chosenDay the self.dateDetailString which contains the Date and Time
             
             */
            case PrefferedDayLater:
                self.bookObj.chosenDay=[NSString stringWithFormat:@"%@",self.dateDetailString];
                break;
                
            default:
                self.bookObj.chosenDay=[NSString stringWithFormat:@"Today, %@",self.bookObj.chosenTime];
                break;
        }
       
    }
}

#pragma mark - UIViewControllerTransitioningDelegate


    

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return [[DismissDetailTransition alloc] init];
}

#pragma mark - UNWIND Segue
- (IBAction)unwindToSalonDetail:(UIStoryboardSegue *)unwindSegue
{
    
    if (([[User getInstance] hasCreditCard] && [[User getInstance] isUserLoggedIn]) || ([[User getInstance] isUserLoggedIn] && [[User getInstance] hasEnoughCreditForBooking:self.bookObj.chosenDay])) {
        [self setButtonToBookNow];
    }
    else{
        [self checkIfUserIsLoggedInOrSignedUpOrNeedsToAddPaymentInfo];

    }
        /*
    NSString * paymentRef = [[User getInstance] fetchCreditCardPaymentRef];
    
    
    if (![[User getInstance] hasCreditCard]) {
       
        
        [self.checkForAppointmentsButton setTitle:@"Check For Appointments" forState:UIControlStateNormal];
        
    }else if([WSCore stringIsEmpty:paymentRef]){
        
        [self.checkForAppointmentsButton setTitle:@"Add payment info" forState:UIControlStateNormal];
    }
    */
    
}




#pragma marks - Notifcations
-(void)postNotificationWithSalonIsFavourite:(NSString *)salonId //post notification method and logic
{
    NSString *notificationName = @"salonIsAddedToFavourites";
    NSString *key = @"Salon_id";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:salonId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

- (void)postNotificationWithSalonRemoveFavourite:(NSString *)salonId //post notification method and logic
{
    NSString *notificationName = @"salonIsNotFavourite";
    NSString *key = @"Salon_id";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:salonId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

#pragma mark - ServicesDelegate method
-(void)didGetServices:(NSMutableArray *)services{
    
    self.isArrayOfServicesDownloaded=YES;
    [self.arrayOfServices removeAllObjects];
    [self.arrayOfServices addObjectsFromArray:services];
   
}

-(void)didGetStaff:(NSMutableArray *)staff{
    
    self.isArrayOfStylistsDownloaded=YES;
    [self.workingStaffArray removeAllObjects];
    
    
    
    //`any stylist` staff object
    PhorestStaffMember * staffM = [[PhorestStaffMember alloc] init];
    staffM.isAnyStylist=YES;
    [self.workingStaffArray addObject:staffM];
    
    //add staff
    [self.workingStaffArray addObjectsFromArray:staff];
    
    //added
    [self createAllTimesArray];
    
    [self sortStaff];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [WSCore dismissNetworkLoadingOnView:self.view];
       
    });
    
   
}

/*
 Delegate method for making a booking
 If not successful inform the user
 If it was successful, assgin the booking reference to the appointment_reference_number of the booking object.
 They push to a new view controller
 */
-(void)didReserveAppointment:(BOOL)success WithRefNumber:(NSString *)ref{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [WSCore dismissNetworkLoadingOnView:self.view];
        
        if (!success) {
            
           
                [UIView showSimpleAlertWithTitle:@"Oh Oh!" message:@"A booking could not be made. Please try again later." cancelButtonTitle:@"OK"];
        
            
        }
        else{
            
            self.bookObj.appointment_reference_number=ref;
            
            //pushes to a new view control, values are also pushed, please see prepare for segue
            [self performSegueWithIdentifier:@"confirmBooking" sender:self];
            
        }

    });
}

-(void)didGetAllStaff:(NSMutableArray *)allStaff{
    
    [self.allStaffArray addObjectsFromArray:allStaff];
   
}



#pragma mark - Sort staff

/*
 Uses the array of working staff, and the full array (allStaffArray).
 Finds which Staff Members are not on today.
 Staff members not working today get a boolean value of NO for .isWorking.
 THis is then added to a full array, double checking that the object does not already exist. 
 Finally the contents of the full array is added to the workingStaffArray.
 
 */
-(void)sortStaff{
    
    NSMutableArray * fullArray = [NSMutableArray array];
    NSMutableArray * idArray = [NSMutableArray array];
    //NSMutableArray * noDuplicatesArray = [NSMutableArray array];
     NSMutableSet *staffSet = [NSMutableSet new];

    for (int i =1; i<self.workingStaffArray.count; i++) {
        PhorestStaffMember * workingStaffMember = [[PhorestStaffMember alloc] init];
            workingStaffMember = [self.workingStaffArray objectAtIndex:i];
        
        for (int j=0; j<self.allStaffArray.count; j++) {
            PhorestStaffMember * allStaffMember =[[PhorestStaffMember alloc] init];
           
            allStaffMember=[self.allStaffArray objectAtIndex:j];
            
            
            if (![workingStaffMember isEqual:allStaffMember]) {
                NSLog(@"working id %d != %d all Staff id add",[workingStaffMember.staff_id intValue],[allStaffMember.staff_id intValue]);
                allStaffMember.isWorking=NO;
                
                [idArray addObject:allStaffMember.staff_id];
                [staffSet addObject:allStaffMember];
                
                /*
                //loop through the objs
                for (NSString * obj in idArray)
                {
                    
                    NSMutableSet *staffSet = [NSMutableSet new];
                   
                    [staffSet addObject:obj];
                    //add the staff id to the noDuplicatesArray if it already exists
                    if (![noDuplicatesArray containsObject:obj])
                    {
                        
                        [noDuplicatesArray addObject: obj];
                        //if it doesnt already exist in the noduplicates array it means we have not passed it already, add the staff object to the fullArray
                        [fullArray addObject:allStaffMember];
                    }
                }
                */
                
            }
            
        }
    }
    
    [fullArray addObjectsFromArray:[staffSet allObjects]];
    
  
    [fullArray sortUsingDescriptors:[NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"staff_first_name" ascending:YES selector:@selector(caseInsensitiveCompare:)], nil]];
   

    for (int i=1; i<self.workingStaffArray.count; i++) {
         PhorestStaffMember * workingMember = [self.workingStaffArray objectAtIndex:i];
        
        for (int j=0; j<fullArray.count; j++) {
            PhorestStaffMember * staff = [fullArray objectAtIndex:j];
            
            if ([workingMember isEqual:staff]) {
                [fullArray removeObject:staff];
            }
            
        }
        
    }
    

    
    [self.workingStaffArray addObjectsFromArray:fullArray];
    
    /*
     Removes Any stylist - if issue remove this
     */
    NSMutableArray * workingArray = [NSMutableArray array];
    for (PhorestStaffMember*staff in self.workingStaffArray) {
        if (staff.isWorking) {
            [workingArray addObject:staff];
        }
        
    }
    
    NSLog(@"Working array %lu",(unsigned long)workingArray.count);
    if (workingArray.count==2) {
        NSLog(@"There is only one working");
        for (PhorestStaffMember * staff in workingArray) {
            if (staff.isAnyStylist) {
                NSLog(@"is any stylist");
                [self.workingStaffArray removeObject:staff];
            }
        }
    }
    /*
     End of removes any stylist
     */
}
/*
 
 Picker
 
 */

#pragma mark - picker 

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (pickerView==self.servicesPicker) {
        return self.arrayOfServices.count;
    }
    if (pickerView==self.timePicker) {
        return self.timesArray.count;
    }
    return self.workingStaffArray.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}




-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 44)];
    label.backgroundColor = [UIColor clearColor];
    
    if (pickerView==self.servicesPicker) {
        
        
         SalonService * pServ = [self.arrayOfServices objectAtIndex:row];
         if (pServ.isHeader==YES) {
             pServ = [self.arrayOfServices objectAtIndex:row];
         label.text = [NSString stringWithFormat:@"  %@",pServ.category_name];
         label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
         label.textColor=[UIColor whiteColor];
         
         }
         else if (pServ.isHeader==NO){
         label.text = [NSString stringWithFormat:@"     -%@",pServ.service_name ];
         label.font = [UIFont systemFontOfSize:16];
         label.textColor=[UIColor whiteColor];
         }
        
    }
    
    
    
    else if (pickerView==self.stylistPicker){
       PhorestStaffMember * staffMember = [self.workingStaffArray objectAtIndex:row];
        
        if (!staffMember.isAnyStylist) {
            label.text = [NSString stringWithFormat:@"     -%@ %@",staffMember.staff_first_name,staffMember.staff_last_name];
           
        }
        else{
            label.text =@"     -Any Stylist";
           
        }
        
        label.adjustsFontSizeToFitWidth=YES;
        label.font = [UIFont systemFontOfSize:18];
        if (staffMember.isWorking) {
            label.alpha=1;
            
            
        }
        else{
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:label.text];
            [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                    value:@2
                                    range:NSMakeRange(0, [attributeString length])];
            
            label.attributedText = attributeString;
            label.alpha=0.3;
            
            
        }
        label.textColor=[UIColor whiteColor];
        
    }
    else if(pickerView==self.timePicker){
        SalonAvailableTime * times = [self.timesArray objectAtIndex:row];
       
       // label.text = [NSString stringWithFormat:@"     -%@",[times getTimeFromDate:times.start_time]];
        label.text = [NSString stringWithFormat:@"%@",[times getTimeFromDate:times.start_time]];
        label.adjustsFontSizeToFitWidth=YES;
        //label.font = [UIFont fontWithName:kAppFont size:18];
        label.font = [UIFont fontWithName:@"Helvetica-Neue" size:22];
        label.textColor=[UIColor whiteColor];
        label.textAlignment=NSTextAlignmentCenter;
    
    }
    
    return label;

}



@end
