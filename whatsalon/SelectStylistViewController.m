//
//  SelectStylistViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 10/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "PhorestStaffMember.h"
#import "SelectStylistViewController.h"
#import "UIView+AlertCompatibility.h"
#import "ServiceTimeCell.h"

@class SelectStylistViewController;


@interface SelectStylistViewController ()<UITableViewDataSource,UITableViewDelegate>

 @property (weak, nonatomic) IBOutlet NSString *selectedStylist;

@end


@implementation SelectStylistViewController
    
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerCells];
    [self configureNavigationBar];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ServiceTimeCell *cell = (ServiceTimeCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    
    cell.stateSelected = true;
    _selectedStylist = @"Stivie Wonder";
        
//    PhorestStaffMember * staffMember = [self.workers objectAtIndex:indexPath.row];
//
//
//    if (staffMember.isWorking) {
//
//
//
//        if ([self.delegate respondsToSelector:@selector(didSelectStylist:)]) {
//            [self.delegate didSelectStylist:staffMember];
//        }
//        [WSCore popOutViewAnimationWithView:self.contentHolder];
//        [self dismissViewControllerAnimated:YES completion:nil];
//
//    }
//    else{
//         [UIView showSimpleAlertWithTitle:@"Oh Oh" message:[NSString stringWithFormat:@"%@ %@ is unavailble today. Please try another stylist.",staffMember.staff_first_name,staffMember.staff_last_name] cancelButtonTitle:@"OK"];
//    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ServiceTimeCell *cell = (ServiceTimeCell *) [self.tableView dequeueReusableCellWithIdentifier:@"ServiceTimeCell"];
    cell.serviceNameLabel.text = @"Stivie Wonder";
    return cell;
//    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
//    PhorestStaffMember * staffMember = [self.workers objectAtIndex:indexPath.row];
//    if (staffMember.isAnyStylist) {
//        cell.textLabel.text =@"Any Stylist";
//    }
//    else{
//
//        NSString * detail;
//        if (staffMember.isWorking) {
//
//            /*
//             if staff_last_name is greater than 0 then append the surname,currency symbol, and service price
//             else
//                only use first name, currency symbol, and service price
//
//             */
//            if (staffMember.staff_last_name.length>0) {
//                detail=[NSString stringWithFormat:@"%@ %@ - %@%@",staffMember.staff_first_name,staffMember.staff_last_name,self.salonObj.currencySymbolUtf8,staffMember.staff_service_price];
//            }
//            else{
//                detail =[NSString stringWithFormat:@"%@ - %@%@",staffMember.staff_first_name,self.salonObj.currencySymbolUtf8,staffMember.staff_service_price];
//            }
//
//        }else{
//
//            /*
//             if staff_last_name is greater than 0 then append the surname
//             else
//             only use first name
//
//             */
//
//            if (staffMember.staff_last_name.length>0) {
//                detail =[NSString stringWithFormat:@"%@ %@",staffMember.staff_first_name,staffMember.staff_last_name];
//            }
//            else{
//                detail =[NSString stringWithFormat:@"%@",staffMember.staff_first_name];
//            }
//
//        }
//
//        cell.textLabel.text = detail;
//    }
//
//     cell.textLabel.text=[cell.textLabel.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
//
//    if (staffMember.isWorking) {
//        cell.detailTextLabel.text = @"Available";
//        cell.detailTextLabel.textColor = kWhatSalonBlue;
//        cell.textLabel.textColor=[UIColor blackColor];
//    }
//    else{
//        cell.detailTextLabel.text = @"Unavailable";
//        cell.detailTextLabel.textColor = [UIColor silverColor];
//        cell.textLabel.textColor=[UIColor silverColor];
//    }
//
//    cell.backgroundView=nil;
//    cell.backgroundColor=[UIColor clearColor];
//    return cell;
}
    
    - (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
    {
        ServiceTimeCell *cell = (ServiceTimeCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        cell.stateSelected = false;
        
    }

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
//    return self.workers.count;
}
    
    - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        return 50;
    }


-(void) registerCells {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ServiceTimeCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"ServiceTimeCell"];
}

    
    -(void) configureNavigationBar {
        CGRect frame = CGRectMake(0,0, 25,25);
        
        //BACK button
        UIImageView* backImage = [[UIImageView alloc] init];
        backImage.image = [UIImage imageNamed:@"back_arrow.png"];
        backImage.image = [backImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [backImage setTintColor:kWhatSalonSubTextColor];
        
        UIButton *backBttn = [[UIButton alloc] initWithFrame:frame];
        [backBttn setBackgroundImage:backImage.image forState:UIControlStateNormal];
        [backBttn addTarget:self action:@selector(back)
           forControlEvents:UIControlEventTouchUpInside];
        [backBttn setShowsTouchWhenHighlighted:YES];
        UIBarButtonItem *backButton =[[UIBarButtonItem alloc] initWithCustomView:backBttn];
        
        //CHECK MARK Button
        UIImageView* checkMark = [[UIImageView alloc] init];
        checkMark.image = [UIImage imageNamed:@"checkmark_icon"];
        checkMark.image = [checkMark.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [checkMark setTintColor:kWhatSalonSubTextColor];
        [checkMark setContentMode: UIViewContentModeScaleAspectFit];
        
        UIButton *checkBttn = [[UIButton alloc] initWithFrame:frame];
        [checkBttn setBackgroundImage:checkMark.image forState:UIControlStateNormal];
        
        [checkBttn addTarget:self action:@selector(stylistSeelcted)
            forControlEvents:UIControlEventTouchUpInside];
        [checkBttn setShowsTouchWhenHighlighted:YES];
        checkBttn.layer.cornerRadius = 12.5;
        checkBttn.layer.borderWidth = 1;
        checkBttn.layer.borderColor = kWhatSalonSubTextColor.CGColor;
        
        
        [checkBttn.widthAnchor constraintEqualToConstant:25].active = YES;
        [checkBttn.heightAnchor constraintEqualToConstant:25].active = YES;
        
        [backBttn.widthAnchor constraintEqualToConstant:25].active = YES;
        [backBttn.heightAnchor constraintEqualToConstant:25].active = YES;
        
        UIBarButtonItem *checkMarkButton =[[UIBarButtonItem alloc] initWithCustomView:checkBttn];
        
        self.navigationItem.leftBarButtonItem = backButton;
        self.navigationItem.rightBarButtonItem = checkMarkButton;
        
        [self.navigationController.navigationBar
         setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonSubTextColor,
                                  NSFontAttributeName:[UIFont systemFontOfSize:17]}];
        
        self.navigationController.navigationBar.tintColor = kWhatSalonSubTextColor;
        self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
        self.title = @"Pick a Stylist";
    }

-(void) back {
    [self dismissViewControllerAnimated:true completion:nil];
}
    
    -(void)stylistSeelcted
    {
        [self.delegate onStylistSelected:_selectedStylist sender:self];
    }
- (IBAction)cancel:(id)sender {
   
}
@end
