//
//  ViewController.m
//  NewWalkthrought
//
//  Created by Graham Connolly on 08/12/2015.
//  Copyright © 2015 GrahamConnolly. All rights reserved.
//

#import "TutorialViewController.h"
#import "FUIButton.h"

@interface TutorialViewController ()

/*! @brief represents the page images array. */
@property (nonatomic, strong) NSArray *pageImages;

/*! @brief represents the NSMutableArray for the views. */
@property (nonatomic, strong) NSMutableArray *pageViews;

/*! @brief represents the NSArray of titles. */
@property (nonatomic) NSArray * titles;

/*! @brief represents the title label. */
@property (nonatomic) UILabel * titleLabel;

/*! @brief represents the subtitle label. */
@property (nonatomic) UILabel * subTitleLabel;

/*! @brief represents the array of subtitles. */
@property (nonatomic) NSArray *subTitles;

/*! @brief represents the subtitle y position. */
@property (nonatomic) CGFloat subtitleY;

/*! @brief represents the page control y position. */
@property (nonatomic) CGFloat pageControlY;

/*! @brief determines if the login sigup view is animating. */
@property (nonatomic) BOOL isLoginSignUpAnimating;

/*! @brief determines the start animation. */
@property (nonatomic) BOOL startAnimation;

/*! @brief represents the bottom view. */
@property (nonatomic) UIView * bottomView;

/*! @brief the action for loading the visible pages. */
- (void)loadVisiblePages;

/*! @brief the action for loading a page.
    
    @param page represents the page to load.
 */
- (void)loadPage:(NSInteger)page;

/*! @brief the action for destroying a page. 
 
    @param page represents the page to destroy
 */
- (void)purgePage:(NSInteger)page;

@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    /*
     scroll view set up
     */
    self.scrollView.pagingEnabled=YES;
    self.scrollView.delegate=self;
    self.scrollView.backgroundColor = [UIColor clearColor];
    
    
   
    
    /*
     page images array
     */
    self.pageImages = [NSArray arrayWithObjects:
                         [UIImage imageNamed:@"tutorial_1"],
                         [UIImage imageNamed:@"tutorial_2"],
                        [UIImage imageNamed:@"tutorial_3"],
                       [UIImage imageNamed:@"tutorial_4"],
                        [UIImage imageNamed:@"tutorial_5"],
                        [UIImage imageNamed:@"tutorial_7"],
                       nil];
    
    /*
     titles contains an array of titles
     */
    self.titles = [NSArray arrayWithObjects:@"Welcome",@"Salons Nearby",@"Book Instantly",@"Browse",@"Book",@"Services",@"It's that easy", nil];
    
    /*
     subtitles contain array of subtitles
     */
    self.subTitles = [NSArray arrayWithObjects:@"Book Hair and Beauty Appointments in an Instant",@"Take a virtual tour of all the salons near you",@"Book Hair and Beauty Appointments in an Instant \n\n If Instant Book is not available you can call to book your appointment",@"Browse Beautiful Photos and Read Reviews of previous customers",@"Simply select the service and even the stylist you'd like to see. Choose the time that suits you best and book!",@"Now go relax and enjoy yourself in one of our amazing salons. You deserve it!", nil];
   
    /*
     pageControl
     */
    self.pageControl.backgroundColor = [UIColor clearColor];
    self.pageControl.pageIndicatorTintColor =  [UIColor colorWithRed:0/255.0 green:206.0/255.0 blue:255.0/255.0 alpha:0.6];//WhatSalon blue

    self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    NSInteger pageCount = self.pageImages.count;
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
  
   
    self.view.backgroundColor = kWhatSalonBlue;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    /*
     titleLabel set up
     */
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont systemFontOfSize:24.0f];
    [self.view addSubview:self.titleLabel];
    self.titleLabel.text = [[self.titles objectAtIndex:self.pageControl.currentPage] uppercaseString];
    
    /*
     blueView set up
     blue view is used to partially cover some of the image
     */
    UIView * blueView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-(self.view.frame.size.height/2.7), self.view.frame.size.width, (self.view.frame.size.height/2.7)-self.pageControl.frame.size.height)];
    blueView.backgroundColor= self.view.backgroundColor;
    [self.view addSubview:blueView];
    [self.view bringSubviewToFront:self.pageControl];
    blueView.userInteractionEnabled=NO;
    
    /*
     subtitleLabel
     used for more information
     */
    self.subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 280, blueView.frame.size.height)];
    [blueView addSubview:self.subTitleLabel];
    self.subTitleLabel.numberOfLines=0;
    self.subTitleLabel.textAlignment=NSTextAlignmentCenter;
    self.subTitleLabel.center = CGPointMake(blueView.frame.size.width/2.0f, blueView.frame.size.height/2.0f);
    self.subTitleLabel.font = [UIFont systemFontOfSize:20.0f];
    self.subTitleLabel.text = [self.subTitles objectAtIndex:self.pageControl.currentPage];
    self.subTitleLabel.textColor = [UIColor cloudsColor];
    self.subTitleLabel.userInteractionEnabled=NO;
    if (!IS_IPHONE_6_or_above) {
        self.subTitleLabel.adjustsFontSizeToFitWidth=YES;
    }
    self.subtitleY = self.subTitleLabel.frame.origin.y;
    
    
    /*
     cancel button design
     */
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(cancel)
     forControlEvents:UIControlEventTouchUpInside];
   
    if (self.isFromSettings) {
        [button setTitle:@"Close" forState:UIControlStateNormal];
    }
    else{
       [button setTitle:@"Skip" forState:UIControlStateNormal];
    }
    
    button.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    button.frame = CGRectMake(10, 10, 44, 44.0);
    button.tintColor = [UIColor whiteColor];
    [self.view addSubview:button];
    
    
    
    
    /*
     Bottom view for Login And Sign Up buttons
     */
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 70)];
    self.bottomView.backgroundColor = [UIColor cloudsColor];
    [self.view addSubview:self.bottomView];
    
    
    /*
     Login button
     */
   FUIButton * loginButton = [[FUIButton alloc] init];
    loginButton.frame = CGRectMake(10, 10, (self.view.frame.size.width/2)-10, 50) ;
    loginButton.buttonColor = kWhatSalonBlue;
   loginButton.shadowColor = kWhatSalonBlueShadow;
    loginButton.shadowHeight = kShadowHeight;
    loginButton.cornerRadius = kCornerRadius;
    [loginButton setTitle:@"Login" forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:loginButton];
    
    
    /*
     Sign Up button
     */
    FUIButton * signUpButton = [[FUIButton alloc] init];
    signUpButton.frame = CGRectMake((self.view.frame.size.width/2)+5, 10, (self.view.frame.size.width/2)-10, 50) ;
    
    signUpButton.buttonColor = kWhatSalonBlue;
    signUpButton.shadowColor = kWhatSalonBlueShadow;
    signUpButton.shadowHeight = kShadowHeight;
    signUpButton.cornerRadius = kCornerRadius;
    [signUpButton setTitle:@"Sign Up" forState:UIControlStateNormal];
    [signUpButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomView addSubview:signUpButton];

    
}

-(void)cancel{
    [self dismissAndPresentLoginSignUpScreen];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
     [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

/*
 Used to change subviews layout
 */
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    self.scrollView.frame = self.view.frame;
    self.pageControl.frame = CGRectMake(0, self.view.frame.size.height-60, self.view.frame.size.width, 60);
    self.pageControlY = self.pageControl.frame.origin.y;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set up the content size of the scroll view
    CGSize pagesScrollViewSize = self.view.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.pageImages.count, pagesScrollViewSize.height);
    
    // Load the initial set of pages that are on screen
    [self loadVisiblePages];
    
     //self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 loadPage
 loads the current page
 */
- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    
    UIView *pageView = [self.pageViews objectAtIndex:page];
    
    if ((NSNull*)pageView == [NSNull null]) {
        // 2
        CGRect frame = self.view.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        // 3
        UIImageView *newPageView = [[UIImageView alloc] initWithImage:[self.pageImages objectAtIndex:page]];
        newPageView.contentMode = UIViewContentModeScaleAspectFit;
        newPageView.frame = frame;
       
        
        [self.scrollView addSubview:newPageView];
        // 4
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
        
        
    }
}

/*
 purgePage
 remove page
 */
- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
   
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
  
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
    // Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    // Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.pageImages.count; i++) {
        [self purgePage:i];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"END");
    
    
    if (self.startAnimation) {
        /*
         updates the titleLabel and subTitleLabel
         */
        NSTimeInterval duration = 0.5f;
        [UIView transitionWithView:self.subTitleLabel
                          duration:duration
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            NSLog(@"Animate text");
                           
                            self.subTitleLabel.text = [self.subTitles objectAtIndex:self.pageControl.currentPage];
                        } completion:nil];
        
       
        [UIView transitionWithView:self.titleLabel
                          duration:duration
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            NSLog(@"Animate text");
                            self.titleLabel.text = [[self.titles objectAtIndex:self.pageControl.currentPage] uppercaseString];
                           
                        } completion:nil];

        

        
        }
    
    self.startAnimation=NO;
    
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSLog(@"DId scroll");
    self.startAnimation=YES;
    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Load the pages that are now on screen
    
    
    
    /*
        if user is not logged in
            if the page count -1 is equal to the current page AND it is not from settings
                then perform revealLoginAndSignUpField with a positive bool
            else if it is not from Settings
                then perform revealLoginAndSignUpField with Negative bool
     */
    if (![[User getInstance] isUserLoggedIn]) {
        
        if (self.pageControl.currentPage==(self.subTitles.count-1) && !self.isFromSettings) {
            [self revealLoginAndSignUpField:YES];
        }
        else if(!self.isFromSettings){
            
            [self revealLoginAndSignUpField:NO];
        }
        
    }
   
    [self loadVisiblePages];
}

/*
 dismissAndPresentLoginSignUpScreen
 dismisses Tutorial
    if it responds to myDelegate
        then perform goToLoginSignUp
 
 */
-(void)dismissAndPresentLoginSignUpScreen{
    
    
    [self dismissViewControllerAnimated:YES completion:^{
      
            if ([self.myDelegate respondsToSelector:@selector(goToLoginSignUp)]) {
                [self.myDelegate goToLoginSignUp];
            }else{
                NSLog(@"No delegate");
            }
        
        
        
    }];
    
}
/*
 revealLoginAndSignUpField
 params (BOOL) yn
    if positive
        perform animation - adjust frame of subTitleLabel, and pageControl
        then perform showLoginView with Positive Bool
    else
        perform animation - adjust frame of subTitleLabel, and pageControl
        then perform showLoginView with Negative Bool
 */
-(void)revealLoginAndSignUpField: (BOOL) yn{
    if (yn) {
        
        [UIView animateWithDuration:1.0 animations:^{
            
            NSLog(@"Animate up");
            self.subTitleLabel.frame = CGRectMake(self.subTitleLabel.frame.origin.x, self.subtitleY-40, self.subTitleLabel.frame.size.width, self.subTitleLabel.frame.size.height);
            self.pageControl.frame = CGRectMake(self.pageControl.frame.origin.x, self.pageControlY-70, self.pageControl.frame.size.width, self.pageControl.frame.size.height);
        }];
        
        [self showLoginView:YES];
    }
    else{
        [UIView animateWithDuration:1.0 animations:^{
            
            NSLog(@"Animate down");
           self.subTitleLabel.frame = CGRectMake(self.subTitleLabel.frame.origin.x, self.subtitleY, self.subTitleLabel.frame.size.width, self.subTitleLabel.frame.size.height);
            
            self.pageControl.frame = CGRectMake(self.pageControl.frame.origin.x, self.pageControlY, self.pageControl.frame.size.width, self.pageControl.frame.size.height);
        }];

        [self showLoginView:NO];
    }
    
}

/*
 showLoginView
 
 if positive then show bottomView
 else
    animate off screen
 */
-(void)showLoginView: (BOOL) yn{
 
    [UIView animateWithDuration:1.0 animations:^{

        if (yn) {
            self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.view.frame.size.height-70, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
        }
        else {
            self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.view.frame.size.height, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
        }
    }];
    
}
@end
