//
//  BiographyTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 13/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BiographyTableViewCell : UITableViewCell

@property (nonatomic) UILabel * bioTitle;
@property (nonatomic) UILabel * descriptionLabel;
@property (nonatomic) UILabel * readMore;

@end
