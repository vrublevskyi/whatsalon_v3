//
//  MyBookingsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 25/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"
#import "RESideMenu.h"
#import "MyBookings.h"
#import "SalonReviewViewController.h"
#import "Salon.h"

@interface MyBookingsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ReviewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *bookingTableView;

@property (nonatomic) Salon *salon;


@end
