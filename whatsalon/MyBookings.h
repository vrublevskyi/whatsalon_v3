//
//  MyBookings.h
//  whatsalon
//
//  Created by Graham Connolly on 25/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Salon.h"

@interface MyBookings : NSObject

/*! @brief represents the date the booking was created. */
@property (nonatomic) NSString * bookingCreatedData;

/*! @brief represents the booking deposit. */
@property (nonatomic) NSString * bookingDeposit;

/*! @brief represents the booking end time. */
@property (nonatomic) NSString * bookingEndTime;

/*! @brief represents the booking id. */
@property (nonatomic) NSString * bookingId;

/*! @brief determines if the booking has been paid. */
@property (nonatomic) BOOL bookingPaid;

/*! @brief represents the date the booking was paid. */
@property (nonatomic) NSString * bookingPaidDate;

/*! @brief represents the booking start time. */
@property (nonatomic) NSString *bookingStartTime;

/*! @brief represents the booking total. */
@property (nonatomic) NSString * bookingTotal;

/*! @brief represents the staff id. */
@property (nonatomic) NSString*staffID;

/*! @brief represents the service id. */
@property (nonatomic) NSString * serviceId;

/*! @brief represents the service name. */
@property (nonatomic) NSString * servceName;

/*! @brief represents the booking Salon object. */
@property (nonatomic) Salon *bookingSalon;

/*! @brief represents the stylists name. */
@property (nonatomic) NSString * stylistName;

/*! @brief determines if a review has occured. */
@property (nonatomic) BOOL reviewOccurred;

/*! @brief determines if a salon is active. */
@property (nonatomic) BOOL active;

/*! @brief determins if a booking has occurred. */
@property (nonatomic) BOOL bookingOccurred;

/*! @brief determines if a booking is cancelled. */
@property (nonatomic) BOOL isCancelled;


/*! @brief creates an instance of the MyBookings object based on a booking id. */
-(instancetype) initWithBookingID : (NSString *) book_id;

/*! @brief creates an instance of the MyBookings object based on a booking id. */
+(instancetype) bookingWithID : (NSString *) book_id;



/*! 
 @brief returns a dictionary containg the properties of an object that are not nil.
 
 */
- (NSDictionary *)dictionaryRepresentation;
@end
