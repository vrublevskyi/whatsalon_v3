//
//  LastMinuteItem.m
//  whatsalon
//
//  Created by Graham Connolly on 30/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "LastMinuteItem.h"

@implementation LastMinuteItem
-(NSString *)getSimpleDateISO8601{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * tempDate = [dateFormatter dateFromString: self.start_datetime];
    [dateFormatter setDateFormat:@"h:mm a"];
    
    return [dateFormatter stringFromDate:tempDate];
}
@end
