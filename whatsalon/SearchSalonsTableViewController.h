//
//  SearchSalonsTableViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 04/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchDelegate <NSObject>

@optional
/*!
 * @discussion delegate method that returns the search term.
 * @param searchTerm NSString taken from the search bar

 */
-(void)makeSearchBasedOnSearchTerm: (NSString *)searchTerm;

@end
@interface SearchSalonsTableViewController : UIViewController

@property (nonatomic,assign) id<SearchDelegate> delegate;

@property (weak, nonatomic) IBOutlet UISearchBar *salonsSearchBar;

/*!
 * @brief searchBarHolder UIView for holding searchbar
 */
@property (weak, nonatomic) IBOutlet UIView *searchBarHolder;

/*!
 * @brief searchSuggestionsTableView contains a list of suggestions based on the search term beign typed in the search bar
 */
@property (weak, nonatomic) IBOutlet UITableView *searchSuggestionsTableView;

@property (nonatomic) NSString * searchTerm;

@property (nonatomic) BOOL wasSearchPressed;
@property (nonatomic) NSString * catString;
@property (nonatomic) NSString * catID;

@end
