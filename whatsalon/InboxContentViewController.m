//
//  InboxDetailViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 03/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "InboxContentViewController.h"
#import "whatsalon-Swift.h"

@interface InboxContentViewController ()<MessagesVCDelegate>

/*! @brief represents the inbox detail text view. */
//@property (weak, nonatomic) IBOutlet UITextView *inboxDetailTextView;

/*! @brief represents the email image view. */
//@property (weak, nonatomic) IBOutlet UIImageView *emailImage;
@property (nonatomic, strong) MessagesVC *messagesViewControllerr;

@end

@implementation InboxContentViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//
//    [WSCore transparentNavigationBarOnView:self];
//    [WSCore changeNavigationTitleColor:[UIColor cloudsColor] OnViewController:self];
//
    
    [self configureNavigationBar];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                              NSFontAttributeName:[UIFont systemFontOfSize:17]}];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.title = @"Inbox";
    
}


- (void)viewDidLoad {
    [super viewDidLoad];

    MessagesVC * messagesVC = [[MessagesVC alloc]
                                  initWithNibName:@"MessagesVC" bundle:nil];
    self.messagesViewControllerr = messagesVC;
    self.messagesViewControllerr.delegate = self;
    
    self.messagesViewControllerr.definesPresentationContext = true;
    self.messagesViewControllerr.modalPresentationStyle = UIModalPresentationCurrentContext;

    self.messagesViewControllerr.view.frame = self.view.bounds;
    [self.view addSubview:self.messagesViewControllerr.view];
    
//    // Do any additional setup after loading the view.
//
//    self.emailImage.image = [UIImage imageNamed:@"Background.jpg"];
//    self.emailImage.contentMode = UIViewContentModeScaleAspectFill;
//    [WSCore addDarkBlurAsSubviewToView:self.emailImage];
//
//    self.inboxDetailTextView.text=self.inboxItem.content;
//
//    [self.view bringSubviewToFront:self.inboxDetailTextView];
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
//    label.backgroundColor = [UIColor clearColor];
//    label.numberOfLines = 0;
//    label.font = [UIFont boldSystemFontOfSize: 14.0f];
//    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor whiteColor];
//    label.text = self.inboxItem.subject;
//
//    self.navigationItem.titleView = label;
    
}

-(void)onMessageSelected:(NSString * )salonName on:(MessagesVC *)controller {
    MessageDetailsVC * messagesVC = [[MessageDetailsVC alloc]
                               initWithNibName:@"MessageDetailsVC" bundle:nil];
    messagesVC.salonName = salonName;
    [self.navigationController pushViewController:messagesVC animated:true];
}

-(void) configureNavigationBar {
    CGRect frame = CGRectMake(0,0, 25,25);
    
    //FILTER button
    UIImageView* filterImage = [[UIImageView alloc] init];
    filterImage.image = [UIImage imageNamed:@"filterLogo"];
    filterImage.image = [filterImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [filterImage setTintColor: [UIColor whiteColor]];
    [filterImage setContentMode: UIViewContentModeScaleAspectFit];
    
    UIButton *filterBttn = [[UIButton alloc] initWithFrame:frame];
    [filterBttn setBackgroundImage:filterImage.image forState:UIControlStateNormal];

    [filterBttn setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *filterButton =[[UIBarButtonItem alloc] initWithCustomView:filterBttn];
    
    //SEARCH MARK Button
    UIImageView* checkMark = [[UIImageView alloc] init];
    checkMark.image = [UIImage imageNamed:@"searchGradientLogo"];
    checkMark.image = [checkMark.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [checkMark setTintColor: [UIColor whiteColor]];
    [checkMark setContentMode: UIViewContentModeScaleAspectFit];
    
    UIButton *checkBttn = [[UIButton alloc] initWithFrame:frame];
    [checkBttn setBackgroundImage:checkMark.image forState:UIControlStateNormal];
    
    [checkBttn addTarget:self action:@selector(stylistSeelcted)
        forControlEvents:UIControlEventTouchUpInside];
    [checkBttn setShowsTouchWhenHighlighted:YES];
    
    
    [checkBttn.widthAnchor constraintEqualToConstant:25].active = YES;
    [checkBttn.heightAnchor constraintEqualToConstant:25].active = YES;
    
    [filterBttn.widthAnchor constraintEqualToConstant:25].active = YES;
    [filterBttn.heightAnchor constraintEqualToConstant:25].active = YES;
    
    UIBarButtonItem *checkMarkButton =[[UIBarButtonItem alloc] initWithCustomView:checkBttn];
    
    self.navigationItem.leftBarButtonItem = checkMarkButton;
    self.navigationItem.rightBarButtonItem = filterButton;
}


-(void)stylistSeelcted
{
    self.messagesViewControllerr.newMessages = 5;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
