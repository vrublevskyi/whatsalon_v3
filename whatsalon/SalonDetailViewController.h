//
//  SalonDetailViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 27/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Salon.h"


@interface SalonDetailViewController : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>

typedef NS_ENUM(int16_t, PreferedDay){
    PreferdDayToday =0,
    PreferdDayTomorrow =1,
    PrefferedDayLater =2
    
};

@property (nonatomic) PreferedDay PreferredDay;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) UIPageControl * pageControl;//gives ARC warning if weak
@property (weak, nonatomic) IBOutlet UIView *viewContainerOptions;
@property (weak, nonatomic) IBOutlet UIView *viewForPageControl;
@property (weak, nonatomic) IBOutlet UILabel *nameOfSalon;
@property (weak, nonatomic) IBOutlet UIImageView *numberOfStars;
@property (weak, nonatomic) IBOutlet UILabel *addressOfSalon;
@property (weak, nonatomic) IBOutlet UITableView *bookingsTableView;
@property (weak, nonatomic) IBOutlet UIButton *reviewButton;
@property (weak, nonatomic) IBOutlet UIView *viewBlackLayer;

//Pickers
@property (strong,nonatomic) UIPickerView * servicesPicker;
@property (strong,nonatomic) UIPickerView * stylistPicker;


//slide show array
@property (nonatomic,strong) NSMutableArray * salonImages;
@property (weak,nonatomic) Salon * salonData;
@property (weak, nonatomic) IBOutlet UIButton *checkForAppointmentsButton;
@property (strong,nonatomic) UILabel * closeScrollDetailLabel;

@property (weak, nonatomic) IBOutlet UIView *viewForSegmentedView;
@property (nonatomic) NSString * laterDate;


- (IBAction)todayTomorrowOrLaterSegment:(id)sender;
- (IBAction)checkButtonAction:(id)sender;
- (IBAction)reviewsButton:(id)sender;




@end
