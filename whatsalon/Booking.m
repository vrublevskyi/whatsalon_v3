//
//  Booking.m
//  whatsalon
//
//  Created by Graham Connolly on 05/12/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "Booking.h"

@implementation Booking

-(instancetype) initWithSalonID:(Salon *)salon{
    
    self=[super init];
    if (self) {
        self.bookingSalon=salon;
        self.user_id =@"";
        self.slot_id=@"";
        self.room_id=@"";
        self.internal_end_datetime=@"";
        self.internal_start_datetime=@"";
        self.staff_id=@"";
        self.service_id=@"";
                
    }
    return self;
}

+(instancetype) bookingWithSalonID:(Salon *)salon{
    
    return [[self alloc] initWithSalonID:salon];
}

-(NSString *)getBookingDescription{
    
    return [NSString stringWithFormat:@"Booking: \n Salon Id %@ \n user id %@ \n slot id %@ \n room id %@ \n internal_start_datetime %@ \n internal_end_datetime %@ \n staff id %@ \n service id %@ \n appt ref %@ \n service %@ \n price %@ \n stylist %@ \n day %@ \n",self.bookingSalon.salon_id,self.user_id,self.slot_id,self.room_id,self.internal_start_datetime,self.internal_end_datetime,self.staff_id,self.service_id,self.appointment_reference_number,self.chosenService,self.chosenPrice,self.chosenStylist,self.chosenDay];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.bookingSalon.salon_id = [aDecoder decodeObjectForKey:@"salon_id"];
        self.user_id = [aDecoder decodeObjectForKey:@"user_id"];
        self.slot_id = [aDecoder decodeObjectForKey:@"slot_id"];
        self.room_id = [aDecoder decodeObjectForKey:@"room_id"];
        self.internal_start_datetime = [aDecoder decodeObjectForKey:@"internal_start_time"];
        self.internal_end_datetime = [aDecoder decodeObjectForKey:@"internal_end_time"];
        self.staff_id = [aDecoder decodeObjectForKey:@"staff_id"];
        self.service_id = [aDecoder decodeObjectForKey:@"service_id"];
        self.appointment_reference_number = [aDecoder decodeObjectForKey:@"appointment_refernce_number"];
        self.chosenService = [aDecoder decodeObjectForKey:@"service"];
        self.chosenPrice = [aDecoder decodeObjectForKey:@"price"];
        self.chosenStylist = [aDecoder decodeObjectForKey:@"stylist"];
        self.chosenDay = [aDecoder decodeObjectForKey:@"day"];
        self.chosenTime = [aDecoder decodeObjectForKey:@"time"];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.bookingSalon.salon_id forKey:@"salon_id"];
    [aCoder encodeObject:self.user_id forKey:@"user_id"];
    [aCoder encodeObject:self.slot_id forKey:@"slot_id"];
    [aCoder encodeObject:self.room_id forKey: @"room_id"];
    [aCoder encodeObject:self.internal_start_datetime forKey: @"internal_start_time"];
    [aCoder encodeObject:self.internal_end_datetime forKey:@"internal_end_time"];
    [aCoder encodeObject:self.staff_id forKey:@"staff_id"];
    [aCoder encodeObject:self.service_id forKey:@"service_id"];
    [aCoder encodeObject:self.appointment_reference_number forKey: @"appointment_refernce_number"];
    [aCoder encodeObject:self.chosenService forKey:@"service"];
    [aCoder encodeObject:self.chosenPrice forKey: @"price"];
    [aCoder encodeObject:self.chosenStylist forKey:@"stylist"];
    [aCoder encodeObject:self.chosenDay forKey:@"day"];
    [aCoder encodeObject:self.chosenTime forKey:@"time"];
}
@end
