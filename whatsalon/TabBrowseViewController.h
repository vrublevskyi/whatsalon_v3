//
//  TabBrowseViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 30/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header TabBrowseViewController.h
  
 @brief
  
 This file contains the most important method and propertie decalarations for TabBrowseViewController.
 Its named 'TabBrowseViewController' because there was a BrowseViewController in <b>3.0</b>. 
 The View for this controller contain a list of salons.
 
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    10+
 */

#import <UIKit/UIKit.h>

@interface TabBrowseViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

/*!
 @brief filterBarButtonItem - UIButton - shows the filter screen
 */
@property (weak, nonatomic) IBOutlet UIButton *filterBarButtonItem;

/*! @brief represents the TableView */
@property (nonatomic) IBOutlet UITableView *tableView;

/*! @brief represents the navbarItem */
@property (weak, nonatomic) IBOutlet UINavigationItem *navbarItem;

/*!
@brief browsePageCurrentPage - NSInteger represents the current page of results shown in the UITableView
 */
@property (nonatomic) NSInteger browsePageCurrentPage;

/*!
 @brief totalBrowsePageCount - NSInteger represents the total number of pages of results
 */
@property (nonatomic) NSInteger totalBrowsePageCount;

/*!
@brief buttonView - UIView a view that acts as a holder for filterBarButtonItem and mapButton
 */
@property (weak, nonatomic) IBOutlet UIView *buttonView;

/*!
@brief mapButton - UIButton shows the MapViewController
 */
@property (weak, nonatomic) IBOutlet UIButton *mapButton;

/*!
 @brief hasDiscoveryLatLong - BOOL whether a latitude or longitude has been passed from the DiscoveryViewController
 */
@property (nonatomic) BOOL hasDiscoveryLatLong;

/*!
@brief discoveryLat - NSString holds the discovery latitude passed from the DiscoveryViewController (if any). 
 @remark NSString is used as its a JSON value.
 */
@property (nonatomic) NSString * discoveryLat;

/*!
@brief discoveryLon - NSString holds the discoveryLat passed form the DiscoveryViewController (if any). 
 @remark NSString is used as its a JSON value.
 */
@property (nonatomic) NSString *discoveryLon;

/*!
@brief browseTitle - NSString represents the title
 */
@property (nonatomic) NSString *browseTitle;

/*!
 @brief represents whether a service category was selected from Discovery <i>e.g selecting the Hair category.</i>
 */
@property (nonatomic) BOOL filterByDiscoveryCategory;


/*!
 @brief represents the object id type if a service category was selected from Discovery <i> e.g selecting the Hair category contains an object ID for filtering. </i>
 
 */
@property (nonatomic) int discoveryObjectIdType;

/*! @brief directs to the map controller */
- (IBAction)mapButton:(id)sender;

/*!
 @brief shows the filter screen.
 */
- (IBAction)filterBarButtonAction:(id)sender;

@end
