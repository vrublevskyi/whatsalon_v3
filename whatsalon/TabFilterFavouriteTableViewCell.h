//
//  TabFilterFavouriteTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 18/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouriteFilterItem.h"

@interface TabFilterFavouriteTableViewCell : UITableViewCell

/*! @brief represents the filter background image view. */
@property (weak, nonatomic) IBOutlet UIImageView *filterBackgroundImage;

/*! @brief represents the title label. */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! @brief represents the number of favourites label. */
@property (weak, nonatomic) IBOutlet UILabel *noOfFavouritesLabel;

/*! @brief represents the tint subview. */
@property (nonatomic)  UIView * tintSub;

/*! @brief sets up the table view cells appearance based on the FavouriteFilterItem.
    @param item the FavouriteFilterItem
 */
-(void)setUpCellWithItem: (FavouriteFilterItem *) item;;

@end
