//
//  NSString+NumberChecking.h
//  whatsalon
//
//  Created by Graham Connolly on 05/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validations)


+(bool)isNumber:(NSString *) string;

+ (BOOL)validateStringIsExpiryDate:(NSString *)string;

+(BOOL)isValidSurname: (NSString *) string;

+(BOOL)isValidFirstName: (NSString *)string;
+(BOOL)isValidEmail: (NSString *)emailString;
@end
