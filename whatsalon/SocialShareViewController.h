//
//  SocialShareViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 20/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialShareViewController : UIViewController

/*! @brief represents the label for the header text. */
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

/*! @brief represents the text for the header. */
@property (nonatomic) NSString *headerText;

/*! @brief represents the message to share. */
@property (nonatomic) NSString * messageToShare;
@end
