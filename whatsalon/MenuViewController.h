//
//  MenuViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "EditProfileViewController.h"

@interface MenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate,EditProfileDelegate>

/*!
 * @brief profileImage - UIImageView for holding the users profile image
 */

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

/*!
 * @brief pointsLabel - UILabel for holding the users points/credit
 */

@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;

/*!
 * @brief tableViewMenu - UITableView for holding the menu options
 */

@property (weak, nonatomic) IBOutlet UITableView *tableViewMenu;

/*!
 * @brief nameLabel - UILabel for holding the users name
 */

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

/*!
 * @brief rewardPoints - UILabel for holding the text 'Balance' or some other description of the users balance
 */

@property (weak, nonatomic) IBOutlet UILabel *rewardPoints;

/*!
 * @brief salonLogo - UIImageView for holding the WhatSalon logo
 */

@property (weak, nonatomic) IBOutlet UIImageView *salonLogo;


/*!
 * @brief menuOptions - NSArray for holding the menu options
 */

@property (nonatomic) NSArray * loggedInMenuOptions;

/*!
 * @brief images - NSArray for holding the menu options images
 */

@property (nonatomic) NSArray * images;

- (IBAction)showBalanceInfo:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;

@end
