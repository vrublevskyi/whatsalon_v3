//
//  GCStackedGridLayoutSection.m
//  StaggeredCollectionView
//
//  Created by Graham Connolly on 21/10/2014.
//  Copyright (c) 2014 Graham Connolly. All rights reserved.
//

#import "GCStackedGridLayoutSection.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
//class extension

@interface GCStackedGridLayoutSection () {
    
    CGRect _frame;//the current frame of the entire section
    UIEdgeInsets _itemInsets;//the insets for each item
    CGFloat _columnWidth;//the width of each column
    NSMutableArray * _columnHeights;//an array to contain the current height of each column
    NSMutableDictionary * _indexToFrameMap;//a dictionary to map an item idnex to the frame of that item
}

@end
@implementation GCStackedGridLayoutSection

/*
 Initialize the class with the origin point of the section in the coordinate space of the collection view.
 
 You also pass in the width of the entire section, the number of columns and the item insets.
 
 The method then creates the varous internal states and initializes the column heights array to 0 for the number of columns passed in.
 
 
 */
-(id)initWithOrigin:(CGPoint)origin with:(CGFloat)width columns:(NSInteger)columns itemInsets:(UIEdgeInsets)itemInsets{
    
    if ((self = [super init])) {
        _frame = CGRectMake(origin.x, origin.y, width, 0.0f);
        _itemInsets = itemInsets;
        _columnWidth = floorf(width/columns);
        _columnHeights = [NSMutableArray array];
        _indexToFrameMap = [NSMutableDictionary new];
        
        for (NSInteger i = 0; i< columns; i++) {
            [_columnHeights addObject:@(0.0f)];
        }
    }
    return self;
}

-(CGRect)frame{
    
    return _frame;
}

-(CGFloat)columnWidth{
    
    return _columnWidth;
}

-(NSInteger)numberOfItems{
    
    return _indexToFrameMap.count;
}

/*
 The layout will call this method once for each item after it has determined the size of the item by asking its delegate
 */

-(void)addItemOfSize:(CGSize)size forIndex:(NSInteger)index{
    
    //1
    /*
     First of all, you need to calculate the column in which to add the item. You do this by enumerating the array of column heights and working out which is the shortest. I've implemented this with a block-based enumeration method, so a couple of variables are set up as __block so that they can be edited from within a block. These variables will eventually hold the shortest column height and its index
     
     */
    __block CGFloat shortestColumnHeight = CGFLOAT_MAX;
    __block NSUInteger shortestColumnIndex = 0;
    
    //2
    /*
     You use a standard algorithm to calculate the height of the shortest column and its index
     
     */
   [_columnHeights enumerateObjectsUsingBlock:^(NSNumber *height, NSUInteger idx, BOOL *stop) {
       
       CGFloat thisColumnHeight = [height floatValue];
       if (thisColumnHeight < shortestColumnHeight) {
           shortestColumnHeight = thisColumnHeight;
           shortestColumnIndex = idx;
       }
   }];
    
    //3
    /*
     You calculate the frame of the item using the origin of the section (i.e the _frame instance variable), the column size, the cell insets and the item size.
     */
    CGRect frame;
    frame.origin.x = _frame.origin.x + (_columnWidth * shortestColumnIndex) + _itemInsets.left;
    frame.origin.y = _frame.origin.y + shortestColumnHeight + _itemInsets.top;
    frame.size = size;
    
    //4
    /*
     You update the indexToFrameMap by creating an NSValue object to contain the frame. This will be read later when the layout wants to retrieve the frame of a give item.
     */
    _indexToFrameMap[@(index)] = [NSValue valueWithCGRect:frame];
    
    //5
    /*
     If the y-value of this items frame is greater than the section frames y-value, then you update the section frame. This keeps the section frame instance variable up-to-date.
     */
    if (CGRectGetMaxY(frame) > CGRectGetMaxY(_frame)) {
        _frame.size.height = (CGRectGetMaxY(frame) - _frame.origin.y) + _itemInsets.bottom;
        
    }
    
    //6
    /*
     Finally, you update the column heights array to reflect the fact that the select colum will now be taller, given that there's a new item it it.
     */
    [_columnHeights replaceObjectAtIndex:shortestColumnIndex withObject:@(shortestColumnHeight + size.height + _itemInsets.bottom)];
    
}

//returns the frame of a given item.
-(CGRect)frameForItemAtIndex:(NSInteger)index{
    return [_indexToFrameMap[@(index)] CGRectValue];
}

@end
