//
//  CallOutServiceViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 02/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CallOutServiceViewController.h"
#import "FUIButton.h"

@interface CallOutServiceViewController ()

@end

@implementation CallOutServiceViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGFloat buttonHeight = 44;
    CGFloat buttonWidth = self.view.frame.size.width;
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-(buttonHeight*2)-10, buttonWidth, buttonHeight)];
    [self.view addSubview:self.pageControl];
    NSLog(@"Page Control %@",NSStringFromCGRect(self.pageControl.frame));
    [self.view bringSubviewToFront:self.pageControl];
    
    FUIButton * serviceButton = [[FUIButton alloc] initWithFrame:self.buttonRect];
    //FUIButton * serviceButton = [[FUIButton alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+ [[UIApplication sharedApplication] statusBarFrame].size.height, buttonWidth, buttonHeight)];
    serviceButton.buttonColor=kWhatSalonBlue;
    serviceButton.shadowColor=kWhatSalonBlueShadow;
    serviceButton.shadowHeight=kShadowHeight;
    serviceButton.tag=0;
    [serviceButton setTitle:self.chosenService forState:UIControlStateNormal];
    [serviceButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [serviceButton addTarget:self action:@selector(popView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:serviceButton];
    
    //[serviceButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    self.view.backgroundColor=kBackgroundColor;
    
    [UIView animateWithDuration:1.5 animations:^{
        serviceButton.frame = CGRectMake(0, kNavBarAndStatusBarHeight, buttonWidth, buttonHeight);
    }];
    
    FUIButton * bookButton = [[FUIButton alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height-buttonHeight-10, buttonWidth-20, buttonHeight)];
    bookButton.buttonColor=kWhatSalonBlue;
    bookButton.shadowColor=kWhatSalonBlueShadow;
    bookButton.shadowHeight=kShadowHeight;
    bookButton.cornerRadius=kCornerRadius;
   // bookButton.tag=0;
    [bookButton setTitle:@"Book Now" forState:UIControlStateNormal];
    [bookButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.view addSubview:bookButton];
    [bookButton addTarget:self action:@selector(goToTimePage) forControlEvents:UIControlEventTouchUpInside];

    
    self.scrollView.delegate=self;
    self.scrollView.pagingEnabled=YES;
    
    // 1
    self.pageImages = [NSArray arrayWithObjects:
                       [UIImage imageNamed:@"walkthrough_1a.JPG"],
                       [UIImage imageNamed:@"walkthrough_2.jpg"],
                       [UIImage imageNamed:@"walkthrough_3.jpg"],
                       [UIImage imageNamed:@"walkthrough_4.jpg"],
                       nil];
    
    NSInteger pageCount = self.pageImages.count;
    
    // 2
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    // 3
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //self.backgroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    //[self.view insertSubview: self.backgroundImage belowSubview:self.scrollView];
    
    self.titles = [[NSMutableArray alloc] initWithObjects:@"BROWSE",@"BOOK",@"BE BEAUTIFUL",@"BE BEAUTIFUL", nil];
    self.paragraphs = [[NSMutableArray alloc] initWithObjects:@"Take a virtual tour of all the salons near you. Browse beautiful photos and read reviews of previous customers.",@"Simply select the service and even the stylist you'd like to see. Choose the time that suits you best and book!",@"Go relax and enjoy yourself in one of our amazing salons. You deserve it!",@"BE BEAUTIFUL", nil];
    
    //self.backgroundImage.image = [self.pageImages objectAtIndex:0];
    
   // self.startAnimation=NO;
    
    
    
    
}

-(void)popView{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)goToTimePage{
    self.navigationController.delegate=nil;
    [self performSegueWithIdentifier:@"calloutTimeVC" sender:self];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
      [WSCore nonTransparentNavigationBarOnView:self];
    // 4
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.pageImages.count, pagesScrollViewSize.height);
    
    // 5
    [self loadVisiblePages];
}

- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // 1
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        // 2
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        // 3
        
         UIImageView *newPageView = [[UIImageView alloc] initWithImage:[self.pageImages objectAtIndex:page]];
         newPageView.contentMode = UIViewContentModeScaleAspectFit;
         newPageView.frame = frame;
    

         //[self.scrollView addSubview:newPageView];
        
        //UIView * newPageView = [[UIView alloc] init];
        //newPageView.frame = frame;
        //newPageView.backgroundColor=[UIColor clearColor];
        
        [self.scrollView addSubview:newPageView];
        
        UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(16, self.view.frame.size.height-268-44, 288, 24)];
        title.font = [UIFont boldSystemFontOfSize:21.0f];
        title.textColor=[UIColor blackColor];
        title.text = [self.titles objectAtIndex:page];
        [newPageView addSubview:title];
        
        UILabel * descText = [[UILabel alloc] initWithFrame:CGRectMake(16, self.view.frame.size.height-238-44, 260, 100)];
        //descText.text = @"Take a virtual tour of all the salons near you. Browse beautiful photos and read reviews of previous customers.";
        descText.text = [self.paragraphs objectAtIndex:page];
        descText.font = [UIFont systemFontOfSize:18.0f];
        descText.textColor=[UIColor blackColor];
        descText.numberOfLines=0;
        [newPageView addSubview:descText];
        
        // 4
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
    // Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    // Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.pageImages.count; i++) {
        [self purgePage:i];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Load the pages that are now on screen
    [self loadVisiblePages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
