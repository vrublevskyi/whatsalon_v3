//
//  FavouriteViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 23/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouriteViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic) BOOL isFromMenu;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)showMenu:(id)sender;

@end
