//
//  FadePopAnimation.h
//  whatsalon
//
//  Created by Graham Connolly on 02/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FadePopAnimation : NSObject<UIViewControllerAnimatedTransitioning>
@end
