//
//  TabFavouritesViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 24/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabFavouritesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


/*! @brief represents the table view. */
@property (weak, nonatomic) IBOutlet UITableView *tableView;


/*! @brief determines if it is from the favourite filter scree.
 
    @remark TabFavouriteFilterViewController is no longer in use because we dropped the experiences cell.
 
 */
@property (nonatomic) BOOL isFromFavFilter;

@end
