//
//  TabRegisterViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 21/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header TabRegisterViewController.h
  
 @brief This is the header file for registering a user.
  
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    -
 */


#import <UIKit/UIKit.h>


@interface TabRegisterViewController : UIViewController
<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>


/*! @brief represents view for the navigation bar. */
@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;

/*! @brief represents the view for the email text field. */
@property (weak, nonatomic) IBOutlet UIView *viewForEmailTF;

/*! @brief represents the view for the password text field. */
@property (weak, nonatomic) IBOutlet UIView *viewForPasswordTF;

/*! @brief represents the view for the phone number textfield. */
@property (weak, nonatomic) IBOutlet UIView *viewForPhoneNumber;

/*! @brief represents the sms verification view. */
@property (weak, nonatomic) UIView *smsVerificationView;

/*! @brief represents the area code button. */
@property (weak, nonatomic) IBOutlet UIButton *areaCodeButton;

/*! @brief represents the email text field. */
@property (weak, nonatomic) IBOutlet UITextField *emailAddressTextField;

/*! @brief represents the password text field. */
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

/*! @brief represents the phone number text field. */
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;

/*! @brief represents the sign up button. */
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;

/*! @brief represents the view for the area code picker. */
@property (weak, nonatomic)  UIView *viewForAreaCodePicker;

/*! @brief determines if the country code is showing. */
@property (nonatomic,assign) BOOL isCountryCodeViewShowing;

/*! @brief represents an NSMutableArray for the country codes. */
@property (nonatomic,strong) NSMutableArray * countryCodes;

/*! @brief represents an NSMutableArray for the countries. */
@property (nonatomic,strong) NSMutableArray * countries;

/*! @brief represents the country code picker. */
@property (strong,nonatomic)  UIPickerView * countryCodePicker;

/*! @brief represents the navigation bar. */
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;

/*! @brief represents the cancel button. */
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;

/*! @brief reoresents the verification label. */
@property (weak, nonatomic) UILabel *smsVerifcationLabel;

/*! @brief represents the verification textfield. */
@property (weak, nonatomic) IBOutlet UITextField *vericationCodeTextField;

/*! @brief represents the resend message bar button. */
@property (weak, nonatomic) UIBarButtonItem *resendMessageBarButton;

/*! @brief represents the navigation bar item. */
@property (strong, nonatomic) IBOutlet UINavigationItem *myNavigationItem;

/*! @brief represents the scroll view used to move the textfields out of the way when a keyboard appears. */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

/*! @brief represents the sign up action. */
- (IBAction)signUp:(id)sender;

/*! @brief represents the cancel registration action. */
- (IBAction)cancelRegistration:(id)sender;

/*! @brief represents the area code change action. */
- (IBAction)changeAreaCode:(id)sender;

/*! @brief represents the verify signup action. */
- (IBAction)verifySignUp:(id)sender;

/*! @brief represents the view for the first name textfield. */
@property (weak, nonatomic) IBOutlet UIView *firstNameView;
/*! @brief represents the first name textfield. */
@property (weak, nonatomic) IBOutlet UITextField *firstName;

/*! @brief represents the view for the last name. */
@property (weak, nonatomic) IBOutlet UIView *lastNameView;

/*! @brief represents the textfield for the last name. */
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@end
