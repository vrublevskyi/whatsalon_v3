//
//  UICollectionView+Animations.m
//  whatsalon
//
//  Created by Graham Connolly on 28/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "UICollectionView+Animations.h"

@implementation UICollectionView (Animations)

-(void)fadeInReload{
   
    [UIView animateWithDuration:0.8 animations:^{
        [self reloadData];
        self.alpha=1.0;
    }];
}

-(void)fadeOut{
    [UIView animateWithDuration:0.8 animations:^{
        self.alpha=0.0;
    }];
}

-(void)fadeOutFadeInReload{
    
    
    
    //fade out
    [UIView animateWithDuration:0.6f animations:^{
        
        self.alpha=0.0;
        
    } completion:^(BOOL finished) {
        
        [self reloadData];
        //fade in
        [UIView animateWithDuration:0.8f animations:^{
            
            self.alpha=1.0;
            
        } completion:nil];
        
    }];
}
@end
