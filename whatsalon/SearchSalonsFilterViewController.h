//
//  SearchSalonsFilterViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 10/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchSalonsFilterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *searchHolder;
@property (weak, nonatomic) IBOutlet UILabel *searchText;

@property (weak, nonatomic) IBOutlet UIImageView *searchImage;
@property (weak, nonatomic) IBOutlet UIView *navBarView;

@end
