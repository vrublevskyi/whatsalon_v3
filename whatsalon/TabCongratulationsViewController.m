//
//  TabCongratulationsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabCongratulationsViewController.h"
#import "WSCore.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "GetDirectionsViewController.h"
#import <UIImage+ImageEffects.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "UILabel+Boldify.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SocialShareViewController.h"
#import "User.h"

#import "UIView+AlertCompatibility.h"

@interface TabCongratulationsViewController ()<UIViewControllerTransitioningDelegate>
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation TabCongratulationsViewController

#pragma mark - Life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureNavigationBar];
    [self configureTap];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self fillDescription];
}

-(void)configureTap {
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleTap:)];
    [self.containerView addGestureRecognizer:singleFingerTap];
}
-(void)fillDescription {
    if (![self.descriptionLabel.attributedText.string isEqualToString:@""]) {
        return;
    }
    
    NSString *bookingString = @"BOOKING";
    NSString *dayString = [self.bookingObj.chosenDay uppercaseString];
    NSString *timeString = [self.bookingObj.chosenTime uppercaseString];
    NSString *masterName = [self.bookingObj.chosenStylist uppercaseString];
    NSString *salonName = [self.bookingObj.bookingSalon.salon_name uppercaseString];

    
    NSString *fullDescription = [NSString stringWithFormat:@"You have a %@ for %@ at %@ with %@ in %@", bookingString,dayString,timeString, masterName,salonName];
    
    // Define general attributes for the entire text
  
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName: self.descriptionLabel.textColor,
                              NSFontAttributeName: self.descriptionLabel.font
                              };
    NSMutableAttributedString *attributedText =
    [[NSMutableAttributedString alloc] initWithString:fullDescription
                                           attributes:attribs];
    
    //custom text color
    UIFont *font = [UIFont systemFontOfSize:14];

    NSRange bookingRange = [fullDescription rangeOfString:bookingString];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:kCongratsWordsColor,
                                    NSFontAttributeName:font}
                            range:bookingRange];
    NSRange dayRange = [fullDescription rangeOfString:dayString];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:kCongratsWordsColor,
                                    NSFontAttributeName:font}
                            range:dayRange];
    NSRange timeRange = [fullDescription rangeOfString:timeString];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:kCongratsWordsColor,
                                    NSFontAttributeName:font}
                            range:timeRange];
    NSRange masterRange = [fullDescription rangeOfString:masterName];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:kCongratsWordsColor,
                                    NSFontAttributeName:font}
                            range:masterRange];
    NSRange salonRange = [fullDescription rangeOfString:salonName];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:kCongratsWordsColor,
                                    NSFontAttributeName:font}
                            range:salonRange];

    self.descriptionLabel.attributedText = attributedText;
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    [self back];
}
    
    -(void) configureNavigationBar {
        CGRect frame = CGRectMake(0,0, 25,25);
        
        //BACK button
        UIImageView* backImage = [[UIImageView alloc] init];
        backImage.image = [UIImage imageNamed:@"back_arrow.png"];
        backImage.image = [backImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [backImage setTintColor:kWhatSalonSubTextColor];
        
        UIButton *backBttn = [[UIButton alloc] initWithFrame:frame];
        [backBttn setBackgroundImage:backImage.image forState:UIControlStateNormal];
        [backBttn addTarget:self action:@selector(back)
           forControlEvents:UIControlEventTouchUpInside];
        [backBttn setShowsTouchWhenHighlighted:YES];
        UIBarButtonItem *backButton =[[UIBarButtonItem alloc] initWithCustomView:backBttn];

        [backBttn.widthAnchor constraintEqualToConstant:25].active = YES;
        [backBttn.heightAnchor constraintEqualToConstant:25].active = YES;
        
        self.navigationItem.leftBarButtonItem = backButton;
        
        [self.navigationController.navigationBar
         setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonSubTextColor,
                                  NSFontAttributeName:[UIFont systemFontOfSize:17]}];
        
        self.navigationController.navigationBar.tintColor = kWhatSalonSubTextColor;
        self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    }
    
-(void) back {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [NSNotificationCenter.defaultCenter postNotificationName:@"BookSuccessFinish" object:nil];
}

@end
