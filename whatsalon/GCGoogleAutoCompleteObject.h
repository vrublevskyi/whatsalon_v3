//
//  GCGoogleAutoCompleteObject.h
//  GoogleAutoComplete
//
//  Created by Graham Connolly on 04/11/2015.
//  Copyright © 2015 GrahamConnolly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GCGoogleAutoCompleteObject : NSObject

/*! @brief the name of the GoogleAutoCompleteObject. */
@property (nonatomic) NSString * name;

/*! @brief the place id. */
@property (nonatomic) NSString * placeID;

/*! @brief represents the title. */
@property (nonatomic) NSString * title;

/*! @brief represents the subtitle. */
@property (nonatomic) NSString * subtitle;

/*! @brief creates an instancetype of GCGoogleAutoCompleteObject. 
 
    @param prediction - the Google Places API result to cretae the object. 
 */
-(instancetype)initWithPrediction: (NSDictionary *) prediction;

@end
