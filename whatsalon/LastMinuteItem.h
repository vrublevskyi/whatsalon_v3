//
//  LastMinuteItem.h
//  whatsalon
//
//  Created by Graham Connolly on 30/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Salon.h"

@interface LastMinuteItem : NSObject
@property (nonatomic) NSString *branchServiceRef;
@property (nonatomic) NSString * branch_identity;
@property (nonatomic) Salon * lastMinuteSalon;
@property (nonatomic) NSString *endTime;
@property (nonatomic) NSString *startTime;
@property (nonatomic) NSString *service_id;
@property (nonatomic) NSString *service_identity;
@property (nonatomic) NSString * slot_id;
@property (nonatomic) NSString * staffRef;
@property (nonatomic) NSString * staffRequest;
@property (nonatomic) NSString * staff_phorest_id;
@property (nonatomic) NSString * timestamp_tag;
@property (nonatomic) NSString * service_price;
@property (nonatomic) NSString *service_name;
@property (nonatomic) NSString * room_id;
@property (nonatomic) NSString *staff_id;
@property (nonatomic) NSString *internal_start_time;
@property (nonatomic) NSString *internal_end_time;
@property (nonatomic) NSString *end_datetime;
@property (nonatomic) NSString *start_datetime;

-(NSString *)getSimpleDateISO8601;
@end
