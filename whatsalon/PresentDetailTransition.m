//
//  PresentDetailTransition.m
//  whatsalon
//
//  Created by Graham Connolly on 08/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "PresentDetailTransition.h"

@implementation PresentDetailTransition

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    //it gets us the view that we are tranistioning to
    UIViewController * detail = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    
    UIView * containerView = [transitionContext containerView];
    
    detail.view.alpha = 0.0;
    
    
    /*
     show status bar at top
    CGRect frame = containerView.bounds;
    frame.origin.y += 20.0;
    frame.size.height -=20.0;
    detail.view.frame = frame;
    */
    detail.view.frame = containerView.bounds;
    [containerView addSubview:detail.view];
    [UIView animateWithDuration:0.3 animations:^{
        detail.view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
    
}

-(NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    return 0.3;
}




@end
