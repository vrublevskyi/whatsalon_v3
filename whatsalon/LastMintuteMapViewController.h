//
//  LastMintuteMapViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 09/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "LastMinuteItem.h"
@interface LastMintuteMapViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *salonName;

@property (nonatomic) LastMinuteItem *lastMin;

- (IBAction)cancelButton:(id)sender;
@end
