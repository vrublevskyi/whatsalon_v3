//
//  GCPopOutTransition.m
//  whatsalon
//
//  Created by Graham Connolly on 17/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "GCPopOutTransition.h"
@implementation GCPopOutTransition

-(instancetype)initWithFrame: (CGRect) frame{
    self = [super init];
    if (self) {
        self.frame =frame;
    }
    return self;
}


-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    //it gets us the view that we are tranistioning from
    UIViewController * detail = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    
    detail.view.transform = CGAffineTransformIdentity;
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        //detail.view.frame=self.frame;
       // NSLog(@"frame %@",NSStringFromCGRect(self.frame));
        detail.view.transform = CGAffineTransformMakeScale(0.01, 0.01);
      
    } completion:^(BOOL finished) {
        
        [detail.view removeFromSuperview];
        [transitionContext completeTransition:YES];

    }];

    
}

-(NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    return 0.4;
}
@end
