//
//  RegisterStep2TableViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 19/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileTableViewController : UITableViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic,strong) UIImagePickerController * imagePicker;
@property (strong, nonatomic) IBOutlet UIView *cameraOptionsView;
@property (weak, nonatomic) IBOutlet UITextField *FirstNameTF;
@property (nonatomic,assign) BOOL isFromCC;


- (IBAction)chooseExistingPic:(id)sender;
- (IBAction)takePic:(id)sender;
- (IBAction)close:(id)sender;
- (IBAction)done:(id)sender;

@end
