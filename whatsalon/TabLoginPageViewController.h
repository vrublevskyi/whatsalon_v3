//
//  TabLoginPageViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 21/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header TabLoginPageViewController.h
  
 @brief This is the header file for logging in.
  

  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    -
 */

#import <UIKit/UIKit.h>

@interface TabLoginPageViewController : UIViewController


/*! @brief represents the view for holding the email textfield. */
@property (weak, nonatomic) IBOutlet UIView *emailView;

/*! @brief represents the view for holding the password textfield. */
@property (weak, nonatomic) IBOutlet UIView *passwordView;

/*! @brief represents the login button. */
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

/*! @brief represents the users avatar. */
@property (weak, nonatomic) IBOutlet UIImageView *userAvatar;

/*! @brief represents the cancel bar button. */
@property (weak, nonatomic) IBOutlet UIBarButtonItem *CancelBarButton;

/*! @brief represents the navigation bar. */
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;

/*! @brief represents the view for navigation bar. */
@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;

/*! @brief represents the email address textfield. */
@property (weak, nonatomic) IBOutlet UITextField *emailAddressTextField;

/*! @brief represents the password textfield. */
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

/*! @brief represents the cancel action. */
- (IBAction)cancel:(id)sender;

/*! @brief represents the login action. */
- (IBAction)login:(id)sender;

/*! @brief represents the background image. */
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@end
