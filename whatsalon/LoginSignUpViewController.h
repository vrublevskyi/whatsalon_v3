//
//  LoginSignUpViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 10/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginSignUpDelegate <NSObject>

@optional
-(void)didLoginWithFacebook;

@end
@interface LoginSignUpViewController : UIViewController

@property (nonatomic,assign) id<LoginSignUpDelegate> delegate;
@property (nonatomic) BOOL isFromLocalSignUpBookingScreen;


- (IBAction)cancel:(id)sender;

@end
