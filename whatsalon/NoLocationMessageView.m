//
//  NoLocationMessageView.m
//  whatsalon
//
//  Created by Graham Connolly on 06/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "NoLocationMessageView.h"

@implementation NoLocationMessageView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        //1. load .xib
        [[NSBundle mainBundle] loadNibNamed:@"NoLocationView" owner:self options:nil];
        
        //2 adjust bounds
        self.bounds = self.view.bounds;
        
        self.messageImage.contentMode = UIViewContentModeScaleAspectFit;
        self.messageText.adjustsFontSizeToFitWidth=YES;
        self.messageText.textColor=[UIColor whiteColor];
        self.messageTitle.textColor=[UIColor whiteColor];
        self.messageTitle.adjustsFontSizeToFitWidth=YES;
        
        
        self.tryAgainButton.backgroundColor=[UIColor clearColor];
        self.tryAgainButton.buttonColor = kWhatSalonBlue;
        self.tryAgainButton.shadowColor = kWhatSalonBlueShadow;
        self.tryAgainButton.shadowHeight = 1.0f;
        self.tryAgainButton.cornerRadius = 3.0f;
        [self.tryAgainButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        UIImage * templateImage = [[UIImage imageNamed:@"Compass" ] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.messageImage.image = templateImage;
        self.messageImage.tintColor=kWhatSalonBlue;
        //3 add as subview
        [self addSubview:self.view];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //load the interface file from .xib
        [[NSBundle mainBundle] loadNibNamed:@"NoLocationView" owner:self options:nil];
        
        //Add as a subview
        [self addSubview:self.view];
    }
    return self;
}




@end
