//
//  AllSalonsModel.m
//  whatsalon
//
//  Created by Graham Connolly on 27/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "AllSalonsModel.h"
#import "OpeningDay.h"
#import "SalonReview.h"
#import "UIView+AlertCompatibility.h"

@implementation AllSalonsModel

- (id) init
{
	self = [super init];
	if (self != nil) {
        self.arryResults = [[NSMutableArray alloc] init];
	}
	
	return self;
}

-(void)load:(NSURL *)url withParams:(NSString *)params AndWithSpinner:(BOOL) spinner{
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
       NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    self.task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        NSLog(@"Data %@,",dataDict);
        if (data && dataDict) {
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                       
                        if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                            self.totalNumberOfPages = [dataDict[@"message"][@"total_pages"] intValue];
                            
                            NSLog(@"Data Dict \n\n\n%@",dataDict);
                            
                            NSArray * salonsArray = dataDict[@"message"][@"results"];
                            for (NSDictionary *sDict in salonsArray) {
                                
                                Salon *salon = [Salon salonWithID:[sDict[@"salon_id"] stringValue] ];
                                if ([sDict[@"salon_latest_review"]count]!=0) {
                                    
                                    salon.hasReview=YES;
                                    SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:sDict[@"salon_latest_review"][@"user_name"] AndWithLastName:sDict[@"salon_latest_review"][@"user_last_name"]];
                                    NSLog(@"first name %@",sDict[@"salon_latest_review"][@"user_name"]);
                                    salonReview.message = sDict[@"salon_latest_review"][@"message"];
                                    salonReview.review_image=sDict[@"salon_latest_review"][@"review_image"];
                                    salonReview.salon_rating=sDict[@"salon_latest_review"][@"salon_rating"];
                                    salonReview.stylist_rating=sDict[@"salon_latest_review"][@"stylist_rating"];
                                    salonReview.date_reviewed=sDict[@"salon_latest_review"][@"date_reviewed"];
                                    salonReview.user_avatar_url=sDict[@"salon_latest_review"][@"user_avatar"];
                                    salon.salonReview=salonReview;
                                    
                                }
                                if (sDict[@"salon_name"] !=[NSNull null]) {
                                    salon.salon_name=sDict[@"salon_name"];
                                }
                                if (sDict[@"salon_description"] !=[NSNull null]) {
                                    salon.salon_description = sDict[@"salon_description"];
                                }
                                if (sDict[@"salon_phone"] != [NSNull null]) {
                                    salon.salon_phone = sDict[@"salon_phone"];
                                }
                                if (sDict[@"salon_lat"] !=[NSNull null]) {
                                    salon.salon_lat = [sDict[@"salon_lat"] doubleValue];
                                }
                                if (sDict[@"salon_lon"] != [NSNull null]) {
                                    salon.salon_long = [sDict[@"salon_lon"] doubleValue];
                                }
                                if (sDict[@"salon_address_1"] != [NSNull null]) {
                                    salon.salon_address_1 = sDict[@"salon_address_1"];
                                }
                                if (sDict[@"salon_address_2"] != [NSNull null]) {
                                    salon.salon_address_2 = sDict[@"salon_address_2"];
                                }
                                if (sDict[@"salon_address_3"] != [NSNull null]) {
                                    salon.salon_address_3 = sDict[@"salon_address_3"];
                                }
                                if (sDict[@"city_name"] != [NSNull null]) {
                                    salon.city_name = sDict[@"city_name"];
                                }
                                if (sDict[@"county_name"]) {
                                    salon.country_name =sDict[@"county_name"];
                                }
                                if (sDict[@"country_name"] != [NSNull null]) {
                                    salon.country_name = sDict[@"country_name"];
                                }
                                if (sDict[@"salon_landline"] != [NSNull null]) {
                                    
                                    salon.salon_landline = sDict[@"salon_landline"];
                                }
                                if (sDict[@"salon_website"] !=[NSNull null]) {
                                    salon.salon_website = sDict[@"salon_website"];
                                }
                                if (sDict[@"salon_image"] != [NSNull null]) {
                                    salon.salon_image = sDict[@"salon_image"];
                                }
                                if (sDict[@"salon_type"] != [NSNull null]) {
                                    salon.salon_type = sDict[@"salon_type"];
                                }
                                if (sDict[@"rating"] != [NSNull null]) {
                                    salon.ratings = [sDict[@"rating"] doubleValue];
                                }
                                if (sDict[@"reviews"] != [NSNull null]) {
                                    salon.reviews = [sDict[@"reviews"] doubleValue];
                                }
                                if (sDict[@"salon_tier"] != [NSNull null]) {
                                    salon.salon_tier = [sDict[@"salon_tier"] doubleValue];
                                }
                                if (sDict[@"is_favourite"] !=[NSNull null]) {
                                    salon.is_favourite = [sDict[@"is_favourite"] boolValue];
                                }
                                if (sDict[@"distance"] !=[NSNull null]) {
                                    salon.distance = [sDict[@"distance"] doubleValue];
                                }
                                if (sDict[@"salon_description"] !=[NSNull null]) {
                                    salon.salon_description= sDict[@"salon_description"];
                                }
                                if (sDict[@"salon_images"]!=[NSNull null]) {
                                    
                                    NSArray *messageArray = sDict[@"salon_images"];
                                    
                                    for (NSDictionary * dataDict in messageArray) {
                                        
                                        NSString * imagePath = [dataDict objectForKey:@"image_path"];
                                        
                                        
                                        [salon.slideShowGallery addObject:imagePath];
                                    }
                                }
                                
                                if (sDict[@"rating"]!=[NSNull null]) {
                                    salon.ratings = [sDict[@"rating"] doubleValue];
                                }
                                if (sDict[@"reviews"]!=[NSNull null]) {
                                    salon.reviews = [sDict[@"reviews"] doubleValue];
                                }
                                
                                if ([sDict[@"salon_categories"] count]!=0) {
                                    salon.salonCategories =  @{
                                                               @"Hair" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair"] boolValue]],
                                                               @"Nails" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Nails"]boolValue]],
                                                               @"Face" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Face"] boolValue]],
                                                               @"Body" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Body"] boolValue]],
                                                               @"Massage" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Massage"] boolValue]],
                                                               @"Hair Removal" : [NSNumber numberWithBool:[sDict[@"salon_categories"][@"Hair Removal"] boolValue]],
                                                               };
                                }
                                
                                NSLog(@"%@ categories %@",salon.salon_name,salon.salonCategories);
                                
                                if ([sDict[@"salon_opening_hours"] count]!=0) {
                                    
                                    NSArray * openingHours =sDict[@"salon_opening_hours"];
                                    for (NSDictionary* day in openingHours) {
                                        NSLog(@"%@ day %@",salon.salon_name, day);
                                        OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
                                        
                                        [salon.openingHours addObject:openingDay];
                                    }
                                    
                                    
                                }
                                
                               
                                
                                [self.arryResults addObject:salon];
                            }
                            
                            
                            if ([self.myDelegate respondsToSelector:@selector(reloadSalonDataWithSalons:AndWithPageNumber:)]) {
                                
                                [self.myDelegate reloadSalonDataWithSalons:self.arryResults AndWithPageNumber: [dataDict[@"message"][@"total_pages"] intValue]];
                            }

                             [WSCore dismissNetworkLoadingOnView:self.parentView];
                        }
                        else{
                             [WSCore dismissNetworkLoadingOnView:self.parentView];
                            
                            
                            [UIView showSimpleAlertWithTitle:@"No Salons" message:dataDict[@"message"] cancelButtonTitle:@"OK"];
                        }
                        
                        
                       
                        
                    });
                    break;
                }
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[WSCore dismissNetworkLoading];
                    
                        [WSCore dismissNetworkLoadingOnView:self.parentView];
                        [WSCore showServerErrorAlert];
                    });
                    break;
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [WSCore dismissNetworkLoadingOnView:self.parentView];
                if (error.code != NSURLErrorCancelled) {
                    NSLog(@"Show server error");
                    [WSCore showServerErrorAlert];
                }
                else{
                    NSLog(@"Is canceled Bool = %d",error.code == NSURLErrorCancelled);
                }
            });
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
      
        [WSCore showNetworkLoadingOnView:self.parentView];
        
        [self.task resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
        
    }

}

-(void)stopNetworkTask{
    [self.task cancel];
}
-(void)removeAllSalonObjectsFromArray{
    
    [self.arryResults removeAllObjects];
}
-(NSMutableArray*)fetchAllSalons{
    
    return self.arryResults;
}

-(int)fetchTotalNumberOfPages{
    return self.totalNumberOfPages;
}


@end
