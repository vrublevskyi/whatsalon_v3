//
//  TabReviewOptionsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabReviewOptionsViewController.h"
#import "GetDirectionsViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SalonReviewViewController.h"
#import "UIView+AlertCompatibility.h"
#import "SocialShareViewController.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "SubmitReviewSalonDetailViewController.h"

@interface TabReviewOptionsViewController ()<UIViewControllerTransitioningDelegate,ReviewDelegate,GCNetworkManagerDelegate,ReviewSalonDelegate>

/*! @brief represents the cancel action. */
- (IBAction)cancel:(id)sender;

/*! @brief represents the view for the options. */
@property (weak, nonatomic) IBOutlet UIView *optionsView;

/*! @brief represents the first button. */
@property (weak, nonatomic) IBOutlet UIButton *button1;

/*! @brief represents the third button. */
@property (weak, nonatomic) IBOutlet UIButton *button3;

/*! @brief represents the second button. */
@property (weak, nonatomic) IBOutlet UIButton *button2;

/*! @brief represents the network manager for making requests. */
@property (nonatomic) GCNetworkManager * networkManager;

@end

@implementation TabReviewOptionsViewController

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    [UIView showSimpleAlertWithTitle:@"Oops" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    self.label3.alpha=0.6;
    self.button3.enabled=NO;
    

    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Canceled" message:jsonDictionary[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    alert.tag=333;
    [alert show];
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==333) {
        
        if(buttonIndex==[alertView cancelButtonIndex]){
            
            if ([self.delegate respondsToSelector:@selector(canceledAppointment:)]) {
                [self.delegate canceledAppointment:self.bookingData];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        
    }
    else if(alertView.tag==701){
        
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            [self cancelBooking];
        }
    }
}

-(void)cancelBooking{
    
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&booking_id=%@",self.bookingData.bookingId]];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kCancel_Appointment_URL]];
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [WSCore statusBarColor:StatusBarColorWhite];
    self.view.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.8];
    self.optionsView.backgroundColor=kWhatSalonBlue;
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.parentView=self.view;
    self.networkManager.delegate=self;
    
    if (self.bookingData.bookingOccurred) {
        

        
        // Date has passed
        UIImage *image = [[UIImage imageNamed:@"reload_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.button1 setImage:image forState:UIControlStateNormal];
        if (!self.bookingData.active) {
            self.label1.alpha=0.6;
            self.button1.enabled=NO;
            self.button1.alpha=0.6;
        }
        else{
            self.label1.alpha=1.0;
            self.button1.enabled=YES;
            self.button1.alpha=1.0;
        }
        
        self.label1.text=@"Book";
        
        UIImage *image2 = [[UIImage imageNamed:@"star_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.button2 setImage:image2 forState:UIControlStateNormal];
        
        self.label2.text=@"Rate";
        

        
        if (self.bookingData.reviewOccurred ) {
            self.button2.enabled=NO;
            self.label2.alpha=0.6;
        }
        
        UIImage *image3 = [[UIImage imageNamed:@"Share"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        [self.button3 setImage:image3 forState:UIControlStateNormal];
        self.button3.tintColor=[UIColor whiteColor];
        self.label3.text = @"Share";
        
        
    }else{
        UIImage *image = [[UIImage imageNamed:@"Map"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.button1 setImage:image forState:UIControlStateNormal];
        
        self.label1.text=@"Map";
        
        UIImage *image2 = [[UIImage imageNamed:@"Telephone"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        [self.button2 setImage:image2 forState:UIControlStateNormal];
        self.label2.text=@"Call";
        
        UIImage *image3 = [kCancelButtonImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        [self.button3 setImage:image3 forState:UIControlStateNormal];
        self.button3.tintColor=[UIColor whiteColor];
        self.label3.text = @"Cancel \n Appointment";
        self.label3.font=[UIFont systemFontOfSize:14.0f];
        self.label3.numberOfLines=0;
        self.label3.adjustsFontSizeToFitWidth=YES;
        
        
    }
    
    self.button1.tintColor=[UIColor whiteColor];
    self.button2.tintColor=[UIColor whiteColor];
    
    
    
    
    self.label2.textColor=[UIColor whiteColor];
    self.label2.textAlignment=NSTextAlignmentCenter;
    
    self.label1.textColor=[UIColor whiteColor];
    self.label1.textAlignment=NSTextAlignmentCenter;
    
    self.label3.textColor=[UIColor whiteColor];
    self.label3.textAlignment=NSTextAlignmentCenter;
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 Handles dimissal of screen
 */
- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
 The action corresponding to button1 press
 Allows the user to booking again
 or if the time of booking has not passed then shows the user the map view
 */
- (IBAction)buttonAction1:(id)sender {
    
    
    if (self.bookingData.bookingOccurred) {
        
        if (self.bookingData.active) {
            
            if ([self.delegate respondsToSelector:@selector(bookAgainWithSalon:)]) {
                [self.delegate bookAgainWithSalon:self.bookingData.bookingSalon];
            }
        }
        
    }else{
        
        GetDirectionsViewController *destinationViewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
        destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
        destinationViewController.transitioningDelegate = self;
        destinationViewController.salonMapData = self.bookingData.bookingSalon;
        
        [self presentViewController:destinationViewController animated:YES completion:NULL];
    }
    
}

/*
 Handles the action of button2
 if the date has passed then allow the user to review
 else allow the user to call
 */
- (IBAction)buttonAction2:(id)sender {
    
    if (self.bookingData.bookingOccurred){
        
        SubmitReviewSalonDetailViewController * submitVC = [self.storyboard instantiateViewControllerWithIdentifier:@"submitReviewVC"];
        submitVC.delegate =self;
        submitVC.transitioningDelegate=self;
        submitVC.modalPresentationStyle = UIModalPresentationCustom;
        submitVC.salonData = self.bookingData.bookingSalon;
        [self presentViewController:submitVC animated:YES completion:nil];
       
    }
    else{
        
        if (self.bookingData.bookingSalon.salon_landline.length >=6) {
            [WSCore makePhoneCallWithNumber:self.bookingData.bookingSalon.salon_landline withViewController:self AndWithSalonName:self.bookingData.bookingSalon.salon_name];
        }
        else{
            [UIView showSimpleAlertWithTitle:@"No number provided." message:@"Please contact WhatSalon for further assistance." cancelButtonTitle:@"OK"];
        }
        
    }
    
}

/*
 Handles the actions of button 3
 Presents the social page
 */
- (IBAction)buttonAction3:(id)sender {
    
    if(self.bookingData.bookingOccurred){
        
        SocialShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"socialVC"];
        shareVC.modalPresentationStyle=UIModalPresentationCustom;
        shareVC.transitioningDelegate=self;
        NSString * service = [WSCore vowelCheckString:self.bookingData.servceName];
        shareVC.messageToShare= [NSString stringWithFormat: @"I just booked an appointment for %@ in %@ using WhatSalon.",service,self.bookingData.bookingSalon.salon_name];
        [self presentViewController:shareVC animated:YES completion:nil];
        
    }
    else{
       
        
        
        if (![WSCore isApptTimeGreaterThan24Hours:self.bookingData.bookingStartTime]) {
            
            UIAlertView * cancelAlert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[NSString stringWithFormat:@"You can't cancel within 24 hours of an appointment on WhatSalon. Please phone %@ for assistance.",self.bookingData.bookingSalon.salon_name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [cancelAlert show];
            
        }
        else{
            
            UIAlertView * cancelAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:@"If you cancel a booking your account will still be charged the booking fee. Do you wish to cancel?" delegate:self cancelButtonTitle:@"No, it's OK" otherButtonTitles:@"Yes, Cancel Booking", nil];
            cancelAlert.tag=701;
            [cancelAlert show];
            
        }
        
        
        
    }
    
}


#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}



-(void)reviewsDelegate:(SubmitReviewSalonDetailViewController *)submitReviewSalonViewController didSubmitReviewWithSalonData:(Salon *)salon{
    
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.delegate respondsToSelector:@selector(addedReview)]) {
            [self.delegate addedReview];
        }
    }];
}


@end
