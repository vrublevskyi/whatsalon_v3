//
//  SalonBookingTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 28/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalonBookingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *salonBookingImage;
@property (weak, nonatomic) IBOutlet UILabel *serviceTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ratingStars;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountDue;
@property (weak, nonatomic) IBOutlet UILabel *salonAndStylistLabel;

@end
