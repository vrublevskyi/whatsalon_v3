//
//  UILabelPushAnimation.h
//  whatsalon
//
//  Created by Graham Connolly on 02/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FUIButton.h"

@interface UILabelPushAnimation : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic) NSString * service;
@property (nonatomic) FUIButton * button;
@property (nonatomic) CGRect buttonFrame;


-(instancetype)initWithFrame: (CGRect) frame WithService: (NSString *) service;

@end
