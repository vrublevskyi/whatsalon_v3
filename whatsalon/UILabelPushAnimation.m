//
//  UILabelPushAnimation.m
//  whatsalon
//
//  Created by Graham Connolly on 02/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "UILabelPushAnimation.h"

@implementation UILabelPushAnimation


-(instancetype)initWithFrame: (CGRect) frame WithService: (NSString *) service{
    self = [super init];
    if (self) {
        self.buttonFrame =frame;
        self.service=service;
    }
    return self;
}
- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.3;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController* toViewController   = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [[transitionContext containerView] addSubview:toViewController.view];
    toViewController.view.frame = fromViewController.view.frame;
    toViewController.view.alpha = 0.0;
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0.0 options:0 animations:^{
 
        toViewController.view.alpha = 1.0;
       
    } completion:^(BOOL finished) {
  
        [fromViewController.view removeFromSuperview];
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}
@end
