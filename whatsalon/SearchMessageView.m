//
//  SearchMessageView.m
//  whatsalon
//
//  Created by Graham Connolly on 03/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SearchMessageView.h"

@implementation SearchMessageView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        //1. load .xib
        [[NSBundle mainBundle] loadNibNamed:@"SearchMessageView" owner:self options:nil];
        
        //2 adjust bounds
        self.bounds = self.view.bounds;
        
        self.searchImage.contentMode = UIViewContentModeScaleAspectFit;
        self.searchText.adjustsFontSizeToFitWidth=YES;
        self.searchText.textColor=[UIColor grayColor];
        self.searchTitle.textColor=[UIColor grayColor];
        self.searchTitle.adjustsFontSizeToFitWidth=YES;
        
        UIImage * templateImage = [[UIImage imageNamed:@"Search" ] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.searchImage.image = templateImage;
        self.searchImage.tintColor=kWhatSalonBlue;
        //3 add as subview
        [self addSubview:self.view];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //load the interface file from .xib
        [[NSBundle mainBundle] loadNibNamed:@"SearchMessageView" owner:self options:nil];
        
        //Add as a subview
        [self addSubview:self.view];
    }
    return self;
}



@end
