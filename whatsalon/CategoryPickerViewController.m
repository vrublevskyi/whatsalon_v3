//
//  CategoryPickerViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 04/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CategoryPickerViewController.h"
#import "SalonService.h"

@interface CategoryPickerViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic) NSMutableArray * sectionHeaders;
@property (nonatomic) NSMutableArray * hairMutableArray;
@property (nonatomic) NSMutableArray * hairRemovalArray;
@property (nonatomic) NSMutableArray * nailsArray;
@property (nonatomic) NSMutableArray * massageArray;
@property (nonatomic) NSMutableArray * faceArray;
@property (nonatomic) NSMutableArray * bodyArray;

@property (nonatomic) BOOL isHairShowing;
@property (nonatomic) BOOL isHairRemovalShowing;
@property (nonatomic) BOOL isNailsShowing;
@property (nonatomic) BOOL isMassageShowing;
@property (nonatomic) BOOL isFaceShowing;
@property (nonatomic) BOOL isBodyShowing;

@property (nonatomic) NSInteger showSection;

@property (nonatomic) NSMutableArray * headerBoolArray;

@end

@implementation CategoryPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.sectionHeaders = [NSMutableArray arrayWithObjects:@"Hair",@"Hair Removal",@"Nails",@"Massage",@"Face",@"Body", nil];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.showSection=-1;
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 0)];
    self.tableView.tableFooterView=footerView;
    self.hairMutableArray = [NSMutableArray array];
    
    for (SalonService *serv in self.fullServicesList) {
        if ([serv.category_name isEqualToString:@"Hair"]) {
            [self.hairMutableArray addObject:serv];
        }
    }
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //return self.sectionHeaders.count;
    return self.hairMutableArray.count;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = 44;
    switch (indexPath.section) {
        case 0:
            if (self.isHairShowing) {
                return height;
            }
            return 0;
            break;
            
        case 1:
            if (self.isHairRemovalShowing) {
                return height;
            }
            return 0;
            break;
        case 2:
            if (self.isNailsShowing) {
                return height;
            }
            return 0;
            break;
        case 3:
            if (self.isMassageShowing) {
                return height;
            }
            return 0;
            break;
        case 4:
            if (self.isFaceShowing) {
                return height;
            }
            return 0;
            break;
        case 5:
            if (self.isBodyShowing) {
                return height;
            }
            return 0;
            break;
            
        default:
            break;
    }
    
    return 0;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    SalonService * serv = [self.hairMutableArray objectAtIndex:indexPath.row];
    cell.textLabel.text=serv.service_name;
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    view.backgroundColor=kCellLinesColour;
    UIButton * headerLabel = [[UIButton alloc] initWithFrame:view.frame];
    [headerLabel setTitle:[self.sectionHeaders objectAtIndex:section] forState:UIControlStateNormal];
    ;
    [headerLabel setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    headerLabel.tag=section;
    [headerLabel addTarget:self action:@selector(headerTouch:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:headerLabel];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
-(void)headerTouch: (UIButton *) btn{
    
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    switch (btn.tag) {
        case 0:
            self.isHairShowing=YES;
            self.isHairRemovalShowing=NO;
            self.isNailsShowing=NO;
            self.isMassageShowing=NO;
            self.isFaceShowing=NO;
            self.isBodyShowing=NO;
            break;
            
        case 1:
            self.isHairShowing=NO;
            self.isHairRemovalShowing=YES;
            self.isNailsShowing=NO;
            self.isMassageShowing=NO;
            self.isFaceShowing=NO;
            self.isBodyShowing=NO;
            break;

        case 2:
            self.isHairShowing=NO;
            self.isHairRemovalShowing=NO;
            self.isNailsShowing=YES;
            self.isMassageShowing=NO;
            self.isFaceShowing=NO;
            self.isBodyShowing=NO;
            break;
        case 3:
            self.isHairShowing=NO;
            self.isHairRemovalShowing=NO;
            self.isNailsShowing=NO;
            self.isMassageShowing=YES;
            self.isFaceShowing=NO;
            self.isBodyShowing=NO;
            break;
        case 4:
            self.isHairShowing=NO;
            self.isHairRemovalShowing=NO;
            self.isNailsShowing=NO;
            self.isMassageShowing=NO;
            self.isFaceShowing=YES;
            self.isBodyShowing=NO;
            break;

        case 5:
            self.isHairShowing=NO;
            self.isHairRemovalShowing=NO;
            self.isNailsShowing=NO;
            self.isMassageShowing=NO;
            self.isFaceShowing=NO;
            self.isBodyShowing=YES;
            break;
            
        default:
            break;
    }

    self.showSection=btn.tag;
    //[self.tableView reloadData];
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionHeaders.count;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
