//
//  GCImageDismissTransition.h
//  transition
//
//  Created by Graham Connolly on 18/02/2015.
//  Copyright (c) graham connolly All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GCImageDismissTransition : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic) CGRect endImageFrame;
@property (nonatomic) UIImageView * startImage;
@property (nonatomic) NSString * imageUrl;
@property (nonatomic) UIImage * dummyImage;


-(instancetype)initWithFrame: (CGRect) frame AndImagePath: (NSString *) imagePath;

-(instancetype)initWithFrame: (CGRect) frame AndImage: (UIImage *) image;
@end
