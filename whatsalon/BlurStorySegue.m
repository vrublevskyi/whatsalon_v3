//
//  BlurStorySegue.m
//  whatsalon
//
//  Created by Graham Connolly on 20/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "BlurStorySegue.h"
#import "UIImage+ImageEffects.h"

@implementation BlurStorySegue

-(void)perform{
    
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    destinationViewController.view.alpha=0.0;
    UIView *sourceView = sourceViewController.view;
    UIView *destinationView = destinationViewController.view;
    destinationView.backgroundColor = [UIColor clearColor];
    
    UIImageView *blurredImageView = [[UIImageView alloc] initWithFrame: CGRectMake(0.0, sourceView.frame.size.height, sourceView.frame.size.width, 0.0)];
    blurredImageView.clipsToBounds = YES;
    blurredImageView.contentMode = UIViewContentModeBottom;
    UIGraphicsBeginImageContextWithOptions(sourceView.frame.size, YES, [[UIScreen mainScreen] scale]);
    [sourceView drawViewHierarchyInRect:sourceView.frame afterScreenUpdates:NO];
    UIImage *sourceViewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImage *blurredSourceImage = [sourceViewImage applyDarkEffect];
    blurredImageView.image = blurredSourceImage;
    [sourceView addSubview:blurredImageView];
    
    CGPoint destinationCenter = destinationView.center;
    destinationView.center = CGPointMake(destinationCenter.x, destinationCenter.y+destinationView.frame.size.height);
    [sourceView addSubview:destinationView];
    destinationView.center = destinationCenter;
    blurredImageView.frame = sourceView.frame;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        destinationViewController.view.alpha=1;
        
    }
                     completion:^(BOOL finished){
                         [blurredImageView removeFromSuperview];
                         [destinationView removeFromSuperview];
                         [destinationView insertSubview:blurredImageView atIndex:0];
                         [sourceViewController presentViewController:destinationViewController animated:NO completion:NULL];
                     }];
    
    
    
}

@end
