//
//  TabMakeBookingViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabMakeBookingViewController.h"
#import "SalonBookingModel.h"
#import "Salon.h"
#import "UIView+AlertCompatibility.h"
#import "SalonService.h"
#import "Booking.h"
#import "PhorestStaffMember.h"
#import "SalonAvailableTime.h"
#import "CKCalendarView.h"
#import "User.h"
#import "TabLoginOrSignUpViewController.h"
#import "TabLinkCCViewController.h"
#import "TabConfirmViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TwilioVerifiyViewController.h"
#import "UIImage+ColoredImage.h"
#import "NSNumber+PrimitiveComparison.h"
#import "SelectCategoryViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SelectServicesViewController.h"
#import "SelectStylistViewController.h"
#import "SelectTimeViewController.h"
#import "TabRegisterViewController.h"
#import "TabLoginPageViewController.h"
#import "whatsalon-Swift.h"

@interface TabMakeBookingViewController ()<ServicesDelegate,CKCalendarDelegate,TwilioDelegate,LoginDelegate,UIViewControllerTransitioningDelegate,SelectStylistVCDelegate,SelectTimeDelegate,LinkCCDelegate,SelectServiceDelegate,DatePickerViewDelegate>

/*! @brief represents the view that holds the content which is then place in a UIScrollView. */
@property (weak, nonatomic) IBOutlet UIView *contentHolderView;

/*! @brief represents the stylist title label. */
@property (weak, nonatomic) IBOutlet UILabel *stylistHeaderLabel;

/*! @brief represents the time title label. */
@property (weak, nonatomic) IBOutlet UILabel *timeHeaderLabel;

/*! @brief represents the date title label. */
@property (weak, nonatomic) IBOutlet UILabel *dateHeaderLabel;

/*! @brief represents the service title label. */
@property (weak, nonatomic) IBOutlet UILabel *serviceHeaderLabel;


/*! @brief used to host the today, tomorrow, later buttons. */
@property (weak, nonatomic) IBOutlet UIView *datePickerContainer;

/*! @brief represents the picker color. */
@property (nonatomic) UIColor * pickerButtonColor;


/*!
 @typedef PreferedDay
  
 @brief  An Enum representing the users prefered day for making a booking.
  
 @discussion
 The values of this enum represent the prefered day for making a booking.
  
 @field PreferredDayToday represents today
 @field PrefferredTomorrow represents tomorrow
 @field PrefferedDayLater represents later
 
 */
typedef NS_ENUM(int16_t, PreferedDay){
    PreferredDayToday =0,
    PreferredDayTomorrow =1,
    PreferredDayLater =2
    
};

/*! @brief represents the button for choosing the time of the booking. */
@property (weak, nonatomic) IBOutlet UIButton *timeButton;

/*! @brief represents the button for choosing the service of the booking. */
@property (weak, nonatomic) IBOutlet UIButton *serviceButton;

/*! @brief represents the button for choosing the stylists for the booking. */
@property (weak, nonatomic) IBOutlet UIButton *stylistButton;

/*! @brief action when picking the day (today, tomorrow, later) */
- (IBAction)pickDay:(id)sender;

/*! @brief represents the button for today. */
@property (weak, nonatomic) IBOutlet UIButton *todayButton;

/*! @brief represents the button for later. */
@property (weak, nonatomic) IBOutlet UIButton *laterButton;

/*! @brief represents the button for tomorrow. */
@property (weak, nonatomic) IBOutlet UIButton *tomorrowButton;

/*! @brief represents the button for the appointment time. */
@property (nonatomic) NSString * appointmentTime;

/*! @brief represents the book button */
@property (weak, nonatomic) IBOutlet UIButton *bookButton;


//booking model
/*! @brief represents the SalonBookingModel. */
@property (nonatomic) SalonBookingModel * salonBookingModel;

//services
/*! @brief represents the NSMutableArray of services. */
@property (nonatomic) NSMutableArray * arrayOfServices;

/*! @brief determines whether the array of services has been downloaded. */
@property (nonatomic,assign) BOOL isArrayOfServicesDownloaded;

/*! @brief represents the view for containing the later picker container. */
@property (nonatomic) UIView * laterPickerContainer;

/*! @brief represents the view that covers controls when the later view is showing. */
@property (nonatomic) UIView * coverView;

/*! @brief determines if a service has been picked. */
@property (nonatomic,assign) BOOL hasPickedService;

/*! @brief . */
//@property (nonatomic) NSInteger servicePickerIndex;

/*! @brief represents the Booking object. */
@property (nonatomic) Booking * bookObj;

/*! @brief represents the enum PreferedDay. */
@property (nonatomic) PreferedDay PreferredDay;

//stylist

/*! @brief represents an array of staff who are working. */
@property (nonatomic,strong) NSMutableArray * workingStaffArray;

/*! @brief determine if an array of stylists have been downloaded. */
@property (nonatomic,assign) BOOL isArrayOfStylistDownloaded;

/*! @brief represents an array of all the staff members. */
@property (nonatomic) NSMutableArray * allStaffArray;



//time
//@property (nonatomic) UIPickerView * timePicker;

/*! @brief represents an array of the available times. */
@property (nonatomic) NSMutableArray * availableTimesArray;
//@property (nonatomic) NSInteger timePickerIndex;

/*! @brief determines if a time has been picked. */
@property (nonatomic,assign) BOOL isTimePicked;


//later view
/*! @brief the calendar used for the later times. */
@property(nonatomic, strong) CKCalendarView *calendar;

/*! @brief represents the minimum allowable date for the later booking. */
@property(nonatomic, strong) NSDate *minimumDate;

/*! @brief represents the previous day option chosen before changing as a numeric value.
 
    @discussion If 'Today' is chosen first, and then 'Tomorrow' is chosen, <b>previousTag</b> is now the index of 'Today'.
 */
@property (nonatomic) NSInteger previousTag;

/*! @brief the actual day chosen as a numeric value. */
@property (nonatomic) NSInteger actualTag;

/*! @breif represents the selected date of the booking. */
@property (nonatomic) NSDate * selectedDate;

/*! @brief represents the UIViewControllers UIScrollView. */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

/*! @brief represents the details of the chosen time. */
@property (nonatomic) NSString * timeDetail;

/*! @brief represents the height of the picker. */
@property (nonatomic) CGFloat pickerHeight;

/*! @brief represents the chosen date. */
@property (nonatomic) NSDate * chosenDate;

/*! @brief determines if an unwind segue has been executed. */
@property (nonatomic) BOOL unwindSegueExecuted;

/*! @brief represents the done button on the later form. */
@property (nonatomic) UIButton * doneLaterFormButton;

/*! @brief determines if an unwind segue has been executed from registering a user. */
@property (nonatomic) BOOL unwindRegisteredUser;

/*! @brief used to determine is a stylist has been selected. */
@property (nonatomic) BOOL didSelectStylist;
@end

@implementation TabMakeBookingViewController

#pragma mark - UIViewController life Cycle


#pragma mark - View lifecycle
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*!
 @brief if an unwind segue from register user is true.
        A check is then made to see if the user has money to make a booking.
        if there is no money then they must add a credit card.
 */
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (self.unwindRegisteredUser) {
        self.unwindRegisteredUser=NO;
        if(![self hasMoneyToMakeBooking]){
            
            TabLinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
            linkCC.delegate=self;
            [self presentViewController:linkCC animated:YES completion:nil];
            
        }
    }
    
}

/*!
 @brief cancel salonBookingModel if the view disappears.
 */
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    [self.salonBookingModel cancelBookingModel];
    
}

/*!
 @brief the navigation bars appearance is updated.
        if this view is coming from a parent view controller.
        updates the current button state.
 */
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonSubTextColor}];
    self.navigationController.navigationBar.tintColor = kWhatSalonSubTextColor;
    
    
    if (self.isMovingToParentViewController) {
        [self dateButtonSetUp];
        [self buttonSetUp];
        
    }
    [self currentButtonState];
}

/*!
 @brief a call is made to get the udpated profile information.
        registers for UILocal Notifications
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[User getInstance] updateProfile];
    self.bookButton.hidden=YES;

    
    [WSCore registerForUILocalNotifications];
    
    [WSCore statusBarColor:StatusBarColorWhite];
    
    
    if ([WSCore isDummyMode]) {
        
        /*
         self.arrayOfServices = [[NSMutableArray alloc] initWithArray:[WSCore getDummyServices]];
         self.timesArray = [[NSMutableArray alloc] initWithArray:[WSCore getDummyTimes]];
         self.workingStaffArray = [[NSMutableArray alloc] initWithArray:[WSCore getDummyStylist]];
         self.bookObj = [Booking bookingWithSalonID:self.salonData];
         */
    }
    else{
        
        
        self.salonBookingModel = [[SalonBookingModel alloc] init];
        self.salonBookingModel.view = self.view;
        self.salonBookingModel.myDelegate=self;
        
        //fetches the services list
        [self.salonBookingModel fetchServicesListWithID:self.salonData.salon_id];
        self.arrayOfServices = [NSMutableArray array];
        
        self.bookObj = [Booking bookingWithSalonID:self.salonData];
        
        //working array
        self.workingStaffArray = [NSMutableArray array];
        
        //fetches the staff
        [self.salonBookingModel fetchStaff:self.salonData.salon_id];
        self.allStaffArray = [NSMutableArray array];
        
        //times array
        self.availableTimesArray = [NSMutableArray array];
        
    }
    
    
    
    self.PreferredDay=PreferredDayToday;
    
    self.previousTag= -1;
    self.actualTag=0;
    
    
    
    self.contentHolderView.backgroundColor=[UIColor whiteColor];
    
    [WSCore transparentNavigationBarOnView:self];
    self.title=self.salonData.salon_name;

    self.pickerButtonColor=[UIColor whiteColor];
    self.pickerHeight=300.0f;
    
    [Gradients addPinkToPurpleHorizontalLayerWithView:_bookButton];
}



#pragma mark - SelectTimeDelegate / Selecting Time Process

-(void)didSelectTime:(SalonAvailableTime *)time{
    
    
    if (self.availableTimesArray.count>0) {
        
        //SalonAvailableTime * time = time;
        
        if (self.PreferredDay == PreferredDayToday || self.PreferredDay == PreferredDayTomorrow){
            [self.timeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
            [self.timeButton setTitle:[time getTimeFromDate:time.start_time] forState:UIControlStateNormal];
            self.bookObj.chosenTime=[time getTimeFromDate:time.start_time];
            self.appointmentTime=self.bookObj.chosenTime;
            
            
        }
        else if(self.PreferredDay == PreferredDayLater){
            
            
            [self.timeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
            self.appointmentTime=[time getTimeFromDate:time.start_time];
            self.timeDetail =[NSString stringWithFormat:@"%@ - %@",[self laterFormattedDate:self.selectedDate],[time getTimeFromDate:time.start_time ]];
            [self.timeButton setTitle:self.timeDetail forState:UIControlStateNormal];
            self.bookObj.chosenTime=self.timeDetail;
            
            
        }
        
       
        self.bookObj.start_datetime = time.start_time;
        
        self.bookObj.room_id = time.room_id;
        self.bookObj.slot_id = time.slot_id;
        self.bookObj.staff_id= time.staff_id;
        self.bookObj.internal_start_datetime = time.internal_start_datetime;
        self.bookObj.internal_end_datetime = time.internal_end_datetime;
        self.bookObj.endTime = time.end_time;
        
      
        self.bookObj.patchTestRequired = time.patch_test_required;
       
        //the time form is filled
        self.isTimePicked=YES;
        
    }
    
   
    [self currentButtonState];
    
}
#pragma mark - SelectStylistDelegate

-(void)didCancelSelectStylist{
    
    //if staff array is not empty and the time is not picked then clear then empty the array.
    //if the cancel button has been pressed but a stylist
    if (self.workingStaffArray.count!=0 && !self.didSelectStylist) {
        [self.workingStaffArray removeAllObjects];
    }
    if (self.isArrayOfStylistDownloaded && !self.didSelectStylist) {
        self.isArrayOfStylistDownloaded =NO;
    }
}
-(void)didSelectStylist:(PhorestStaffMember *)stylist{
    
    [self.timeButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.timeButton setTitle:@"Select Time" forState:UIControlStateNormal];
    self.isTimePicked=NO;
    
    self.didSelectStylist=YES;
    
    
    PhorestStaffMember * staffMember = stylist;
    
    
    
    if (staffMember.isWorking) {
        [self.availableTimesArray removeAllObjects];
        
        if (!staffMember.isAnyStylist) {
            
           
            
            [self.availableTimesArray addObjectsFromArray:staffMember.times];
            
            
            
            self.bookObj.chosenStylist = [NSString stringWithFormat:@"%@ %@",staffMember.staff_first_name,staffMember.staff_last_name];
            self.bookObj.staff_id = staffMember.staff_id;
            
            
            
            self.bookObj.chosenPrice = staffMember.staff_service_price;//different price is possible after selecting stylist
            /*
             salon booking details for iSalon
             */
            if ([self.salonData.source_id intValue]==2) {
                self.bookObj.isalonBookingDetails = staffMember.isalon_booking_details;
                
                
                
                
            }
            
        }
        else{
            
            self.bookObj.chosenStylist = @"Any Stylist";
            self.bookObj.staff_id=@"";
            
            
            //creates an array of all available times rather than times specific for one stylist
            [self createAnArrayOfAllAvailableTimes];
        }
        
        [self.stylistButton setTitle:[NSString stringWithFormat:@"%@ - %@%@",self.bookObj.chosenStylist,self.salonData.currencySymbolUtf8,self.bookObj.chosenPrice] forState:UIControlStateNormal];
        [self.stylistButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];

        
        
        
    }
    else{
        
        [UIView showSimpleAlertWithTitle:@"Oh Oh" message:[NSString stringWithFormat:@"%@ %@ is unavailble today. Please try another stylist.",staffMember.staff_first_name,staffMember.staff_last_name] cancelButtonTitle:@"OK"];
    }
    
}

#pragma mark - hasMoneyToMakeBooking
/*
 Determines if ths user has balance to make a booking or needs to add a card
 */
-(BOOL)hasMoneyToMakeBooking{
    
    //we are no longer using money values as credit. We now use coupon codes.
    if ([[User getInstance] hasCreditCard]) {
        return YES;
    }
    else{
        return NO;
    }
    
    
    //this was useful when WhatSalon was dealing with money values as credit.
    /*
    double usersBalanceDouble = [[[User getInstance] fetchUsersBalance] doubleValue] ;     NSDecimalNumber* usersBalance = [[NSDecimalNumber alloc] initWithDouble:usersBalanceDouble]; //create an NSDecimalNumber using your previously extracted double
    
    double chosenPrice = [self.bookObj.chosenPrice doubleValue];
    double bookingPrice = (chosenPrice/100)*10;
    
    NSDecimalNumber* bookingPriceDM= [[NSDecimalNumber alloc] initWithDouble:bookingPrice];
    
    if ([[User getInstance] hasCreditCard] ||[usersBalance compare:bookingPriceDM]==NSOrderedDescending || [bookingPriceDM compare:usersBalance]==NSOrderedSame) {
        
        return YES;
    }
    return NO;
     */
    
}









#pragma mark - Button init
- (void)buttonSetUp
{
    self.todayButton.layer.cornerRadius = 2;
    self.tomorrowButton.layer.cornerRadius = 2;
    self.laterButton.layer.cornerRadius = 2;
    
    self.datePickerContainer.layer.borderWidth = 2;
    self.datePickerContainer.layer.cornerRadius = 2;
    [Gradients addLightBlueToBlueWithPurpleBorderWithView:_datePickerContainer];
    
    
    self.bookButton.layer.cornerRadius=3.0f;
    self.bookButton.layer.masksToBounds=YES;
    
    if (!IS_iPHONE5_or_Above) {
        
        self.serviceButton.layer.cornerRadius=3.0f;
        self.serviceButton.backgroundColor=[UIColor clearColor];
        self.serviceButton.layer.borderColor=[UIColor whiteColor].CGColor;
        self.serviceButton.layer.borderWidth=1.0;
        
        self.stylistButton.layer.cornerRadius=3.0f;
        self.stylistButton.backgroundColor=[UIColor clearColor];
        self.stylistButton.layer.borderColor=[UIColor whiteColor].CGColor;
        self.stylistButton.layer.borderWidth=1.0;
        
        self.timeButton.layer.cornerRadius=3.0f;
        self.timeButton.backgroundColor=[UIColor clearColor];
        self.timeButton.layer.borderColor=[UIColor whiteColor].CGColor;
        self.timeButton.layer.borderWidth=1.0;
        
        self.dateHeaderLabel.textColor=kWhatSalonBlue;
        self.stylistHeaderLabel.textColor=kWhatSalonBlue;
        self.timeHeaderLabel.textColor=kWhatSalonBlue;
        self.serviceHeaderLabel.textColor=kWhatSalonBlue;
        
    }
    else{

        //self.serviceButton.alpha=0.7;
        self.serviceButton.layer.cornerRadius=3.0f;
        self.serviceButton.backgroundColor=[UIColor clearColor];
        self.serviceButton.layer.borderColor=kWhatSalonSubTextColor.CGColor;
        self.serviceButton.layer.borderWidth=2.0;
        
        //self.stylistButton.alpha=0.7;
        self.stylistButton.layer.cornerRadius=3.0f;
        self.stylistButton.backgroundColor=[UIColor clearColor];
        self.stylistButton.layer.borderColor=kWhatSalonSubTextColor.CGColor;
        self.stylistButton.layer.borderWidth=2.0;
        
        
        //self.timeButton.alpha=0.7;
        self.timeButton.layer.cornerRadius=3.0f;
        self.timeButton.backgroundColor=[UIColor clearColor];
        self.timeButton.layer.borderColor=kWhatSalonSubTextColor.CGColor;
        self.timeButton.layer.borderWidth=2.0;
        
        
        //self.todayButton.alpha=0.7;
        //self.tomorrowButton.alpha=0.7;
        //self.laterButton.alpha=0.7;
        
        //self.dateHeaderLabel.alpha=0.7;
        self.dateHeaderLabel.textColor=kWhatSalonBlue;
        
        //self.stylistHeaderLabel.alpha=0.7;
        self.stylistHeaderLabel.textColor=kWhatSalonBlue;
        
        //self.timeHeaderLabel.alpha=0.7;
        self.timeHeaderLabel.textColor=kWhatSalonBlue;
        
        //self.serviceHeaderLabel.alpha=0.7;
        self.serviceHeaderLabel.textColor=kWhatSalonBlue;
        
    }
}






#pragma mark - Twilio Delegate
-(void)twilioWasVerified{
    
    if (![[User getInstance] hasCreditCard]) {
        
        TabLinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
        linkCC.delegate=self;
        [self presentViewController:linkCC animated:YES completion:nil];
        
    }
    else{
        
        [self currentButtonState];
    }
    
}
#pragma mark - Current title for button
-(void)currentButtonState{
    
    if (self.isTimePicked) {
        [self.bookButton setTitle:[[User getInstance] buttonTitleStateWithServicePrice:self.bookObj.chosenPrice] forState:UIControlStateNormal];
        self.bookButton.backgroundColor=kWhatSalonBlue;
        [self.bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [Gradients addLightBlueToBlueWithPurpleBorderWithView:_datePickerContainer];
        [Gradients addLightBlueToBlueWithPurpleBorderWithView:_serviceButton];
        [Gradients addLightBlueToBlueWithPurpleBorderWithView:_stylistButton];
        [Gradients addLightBlueToBlueWithPurpleBorderWithView:_timeButton];
        [Gradients addPinkToPurpleHorizontalLayerWithView:_bookButton];
    }
    else{
    }
    
}





#pragma mark - check for credentials

/////new login feature
-(void)userDidLoginWithFacebook{
    
    [WSCore resetLoginType];
    
    
    
    
    if (![[User getInstance] isTwilioVerified]) {
        
        TwilioVerifiyViewController * twilio =[self.storyboard instantiateViewControllerWithIdentifier:@"verifyVC"];
        twilio.delegate=self;
        [self presentViewController:twilio animated:YES completion:nil];
    }
    
    else if(![[User getInstance] hasCreditCard]){
    
    //else if(![self hasMoneyToMakeBooking]){
        
        TabLinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
        linkCC.delegate=self;
        [self presentViewController:linkCC animated:YES completion:nil];
    }
    
    else{
        //[self changeButtonToBookNow];
        [self currentButtonState];
    }
    

}


#pragma mark - Credit Card Delegate methods
-(void)didAddCard{
    NSLog(@"Did add card");
}
-(void)didSkipCard{
    NSLog(@"Did skip card");
}



#pragma mark - UIButton custom actions - imitates UISegmentedControl
- (IBAction)pickDay:(id)sender {
    
    UIButton * btn = (UIButton *)sender;
    NSLog(@"Btn tag %ld",(long)btn.tag);
    self.previousTag=self.actualTag;
    self.actualTag=btn.tag;
    
    
    switch (btn.tag) {
        case 0:
            self.PreferredDay = PreferredDayToday;
            [self selectedButton:btn];
            [self deselectButton:self.tomorrowButton];
            [self deselectButton:self.laterButton];
            
            break;
        case 1:
            self.PreferredDay = PreferredDayTomorrow;
            [self selectedButton:btn];
            [self deselectButton:self.todayButton];
            [self deselectButton:self.laterButton];
            
            break;
        case 2:
            self.PreferredDay = PreferredDayLater;
            [self selectedButton:btn];
            [self deselectButton:self.todayButton];
            [self deselectButton:self.tomorrowButton];
            
        
        [self presentCalendar];
            //create later view
//            [self createLaterView];
        break;
            
        default:
            
            self.PreferredDay = PreferredDayToday;
            [self selectedButton:btn];
            [self deselectButton:self.tomorrowButton];
            [self deselectButton:self.laterButton];
            
            break;
    }
    
    //handles rest of values
    [self defaultValues];
}

-(void)presentCalendar
{
        
    DatePickerView * serachVC = [[DatePickerView alloc]
                                     initWithNibName:@"DatePickerView" bundle:nil];
    serachVC.delegate = self;
    serachVC.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:serachVC animated:YES];
}

- (void)didSelectWithDate:(NSDate *)date on:(DatePickerView *)controller
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM d"];
    NSString * day = [NSString new];
    day = [dateFormatter stringFromDate:date];
    
    self.bookObj.chosenDay = day;
 [self.laterButton setTitle:[NSString stringWithFormat:@"Later \n%@",[self laterFormattedDate:date]] forState:UIControlStateNormal];
    self.selectedDate = date;
}

-(void)createLaterView{
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, [WSCore screenSize].height-360, [WSCore screenSize].width, 360)];
    self.laterPickerContainer= view;
    
    [self addDarkBlurToPickerView: self.laterPickerContainer];
    //Center label
    UILabel * servicesNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.laterPickerContainer.frame)/2-75, 11, 150, 21)];
    servicesNameLabel.text = @"Pick a Date";
    servicesNameLabel.adjustsFontSizeToFitWidth=YES;
    servicesNameLabel.font = [UIFont systemFontOfSize: 17.0];
    servicesNameLabel.textColor =self.pickerButtonColor;
    servicesNameLabel.textAlignment=NSTextAlignmentCenter;
    [self.laterPickerContainer insertSubview:servicesNameLabel atIndex:1];
    
    //cancel
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 2, 80, 40)];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:self.pickerButtonColor forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelLaterForm) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.laterPickerContainer insertSubview:cancelButton atIndex:1];
    
    //button
    UIButton * doneButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-85, 2, 80, 40)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:self.pickerButtonColor forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneLaterForm) forControlEvents:UIControlEventTouchUpInside];
    doneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.doneLaterFormButton=doneButton;
    
    if (self.selectedDate==nil) {
        self.doneLaterFormButton.hidden=YES;
    }
    
    [self.laterPickerContainer insertSubview:self.doneLaterFormButton atIndex:1];
    
    
    [self createCalendar];
    
    [WSCore addTopLine:self.laterPickerContainer :[UIColor lightGrayColor] :0.5f];
    [self.view addSubview:self.laterPickerContainer];
    
    UIView * coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-self.laterPickerContainer.frame.size.height)];
    coverView.alpha=0.0;
    coverView.backgroundColor=[UIColor blackColor];
    self.coverView=coverView;
    [self.view addSubview: self.coverView];
    
    [self popInViewAnimationWithView:self.laterPickerContainer andWithCoverView:self.coverView];
    
}

-(void)addDarkBlurToPickerView: (UIView *)view{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = view.bounds;
        view.backgroundColor=[UIColor clearColor];
        [view addSubview:visualEffectView];
        
    }
    
    else{
        
        view.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.9];
        
    }
    
}
-(void)cancelLaterForm{
    
    /*
     If selected date is nil set it back to the today button
     selected date is nil if no date has been selected.
     */
    if (self.selectedDate==nil) {
        [self pickDay:self.todayButton];
    }
    
    [self popOutViewAnimationWithView:self.laterPickerContainer andWithCoverView:self.coverView];
    
}



-(void)doneLaterForm{
    
    /*
     only assign selected date if the user selected a date from the calendar
     */
    self.selectedDate = self.chosenDate;
    
    
    [self popOutViewAnimationWithView:self.laterPickerContainer andWithCoverView:self.coverView];
    
    
    [self.laterButton setTitle:[NSString stringWithFormat:@"Later \n%@",[self laterFormattedDate:self.selectedDate]] forState:UIControlStateNormal];
    [self.laterButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    
    [self defaultValues];
    
}

#pragma mark - Select Service Methods

- (void)createServiceView {
    
    
    SelectCategoryViewController * s = [self.storyboard instantiateViewControllerWithIdentifier:@"selectCategoryVC"];
    
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:s];
    s.delegate = self;

    s.salonData = self.salonData;
    s.servicesArray = self.arrayOfServices;
    //now present this navigation controller modally
    [self presentViewController:navigationController
                       animated:YES
                     completion:^{
                         
                     }];

}

-(void)popOutViewAnimationWithView: (UIView *) container andWithCoverView: (UIView *) blockView{
    // animate it to the identity transform (100% scale)
    
    
    [UIView animateWithDuration:0.1 animations:^{
        blockView.alpha=0.0f;
    } completion:^(BOOL finished) {
        
        container.transform = CGAffineTransformIdentity;
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            container.transform = CGAffineTransformMakeScale(0.01, 0.01);
        } completion:^(BOOL finished) {
            
            [self destroyPickerContainerView];
            
        }];
        
    }];
}
-(void)popInViewAnimationWithView: (UIView *) container andWithCoverView: (UIView *)blockView{
    
    
    container.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        container.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // if you want to do something once the animation finishes, put it here
        
        [UIView animateWithDuration:0.1 animations:^{
            blockView.alpha=0.5;
        }];
        
    }];
}
- (IBAction)selectService:(id)sender {
    
    
    if ([WSCore isDummyMode]) {
        
        [self createServiceView];
    }
    else{
        if (!self.isArrayOfServicesDownloaded) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView showSimpleAlertWithTitle:@"Please Wait" message:@"Please wait, the list of available services are still downloading" cancelButtonTitle:@"OK"];
            });
            
        }
        else{
            
            [self createServiceView];
        }
    }
}
    
- (void) onStylistSelected: (NSString *) stylist sender:(SelectStylistViewController*) sender {
    [self.stylistButton  setTitle:stylist forState:UIControlStateNormal];
    [self.stylistButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    self.bookObj.chosenStylist = stylist;
    [Gradients addLightBlueToBlueWithPurpleBorderWithView:_stylistButton];

    [sender dismissViewControllerAnimated:true completion:nil];

}

    
    - (void) onTimeSelected: (NSString *) stylist sender:(SelectTimeViewController*) sender
    {
        [self.timeButton  setTitle:stylist forState:UIControlStateNormal];
        [self.timeButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        self.bookObj.chosenTime = stylist;
        [Gradients addLightBlueToBlueWithPurpleBorderWithView:_timeButton];

        [sender dismissViewControllerAnimated:true completion:nil];
        self.bookButton.hidden=NO;

    }
    
    
- (void) onServiceSelected: (SalonService *) service sender:(SelectCategoryViewController*) sender {
    [self.serviceButton setTitle:service.service_name forState:UIControlStateNormal];
    [self.serviceButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    self.hasPickedService=YES;
    self.bookObj.service_id = service.service_id;
    self.bookObj.chosenService = service.service_name;
    [Gradients addLightBlueToBlueWithPurpleBorderWithView:_serviceButton];

    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}

//used to get the staff
- (void)downloadAvailableStaffForService {
    [self.salonBookingModel fetchStaffAndTimesWithSalonID:self.salonData.salon_id WithServiceID:self.bookObj.service_id WithStaffID:@"" WithStartDatetime:[NSString stringWithFormat:@"%@ 00:00:00",[self getChosenDay]] WithEndDateTime:[NSString stringWithFormat:@"%@ 23:59:59",[self getChosenDay]]];
}

#pragma mark - ServicesDelegate methods

-(void)didGetServices:(NSMutableArray *)services{
    self.isArrayOfServicesDownloaded=YES;
    [self.arrayOfServices removeAllObjects];
    [self.arrayOfServices addObjectsFromArray:services];
    
}

#pragma mark - Services Delegate - didGetStaff
//working staff
-(void)didGetStaff:(NSMutableArray *)staff{
    
    self.isArrayOfStylistDownloaded=YES;
    [self.workingStaffArray removeAllObjects];
    
    //any stylist staff object
    /*
    PhorestStaffMember * staffM = [[PhorestStaffMember alloc] init];
    staffM.isAnyStylist=YES;
    [self.workingStaffArray addObject:staffM];
    */
    //add staff
    [self.workingStaffArray addObjectsFromArray:staff];
    
    [self createAnArrayOfAllAvailableTimes];
    [self sortWorkingAndNonWorkingStaff];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [WSCore dismissNetworkLoadingOnView:self.view];
        
        //added
        if(self.workingStaffArray.count==0){
            
            [UIView showSimpleAlertWithTitle:@"Bummer!" message:@"There are no stylists available for that service today. Please try another service, or try another day." cancelButtonTitle:@"OK"];
            
        }
        else{
            
            [self createStylistView];
            
        }
        
    });
    
    
}

//all the staff
-(void)didGetAllStaff:(NSMutableArray *)allStaff{
    
    [self.allStaffArray addObjectsFromArray:allStaff];
    //NSLog(@"All Staff array %lu",(unsigned long)self.allStaffArray.count);
}

-(void)failureAppointmentWithDictionary:(NSDictionary *)jsonDictionary{
    //NSLog(@"** \n Booking failure %@ \n **",jsonDictionary);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView showSimpleAlertWithTitle:@"Unable to book" message:[NSString stringWithFormat:@"%@", jsonDictionary[@"message"]] cancelButtonTitle:@"OK"];
    });
    
}

-(void)successfullAppointmentWithDictionary:(NSDictionary *)jsonDictionary{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSLog(@"** \n Booking success %@ \n **",jsonDictionary);
        NSString * ref = jsonDictionary[@"message"][@"appointment_reference_number"];
        self.bookObj.appointment_reference_number=ref;
        
        //pushes to a new view control, values are also pushed, please see prepare for segue
        [self performSegueWithIdentifier:@"confirmBookingB" sender:self];
    });
    
}



#pragma mark - Times array
/*
 Creates an array of all available times
 
 */
-(void)createAnArrayOfAllAvailableTimes{
    
    //ensure there are no duplciates
    if (self.availableTimesArray.count>0) {
        [self.availableTimesArray removeAllObjects];
    }
    
    /*
     Loop through the working staff array and add the staff members time to available time if the PhorestStaffMember object is no 'Any Stylist'.
     */
    for (int i=0; i<self.workingStaffArray.count; i++) {
        
        PhorestStaffMember * staffM = self.workingStaffArray[i];
        if (!staffM.isAnyStylist)
            //NSLog(@"Staff name %@",staffM.staff_first_name);
        [self.availableTimesArray addObjectsFromArray:staffM.times];
        
    }
    
    /*
     sort the array by the staff members time
     */
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"start_time"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [self.availableTimesArray sortedArrayUsingDescriptors:sortDescriptors];
    
    //remove all previous times
   
    [self.availableTimesArray removeAllObjects];
    [self.availableTimesArray addObjectsFromArray:sortedArray];
    
}

#pragma mark - Sort the staff array

-(void)sortWorkingAndNonWorkingStaff{
    
    NSMutableArray * fullArray = [NSMutableArray array];
    NSMutableArray * idArray = [NSMutableArray array];
    //NSMutableArray * noDuplicatesArray = [NSMutableArray array];
    NSMutableSet *staffSet = [NSMutableSet new];
    
    
    /*
     loop through working staff array
     
     */
    
    //set int i to 0 which resolved a bug - remove if bug shows up again (no full list for Phorest salons)
    //for (int i =1; i<self.workingStaffArray.count; i++) {
    for (int i =0; i<self.workingStaffArray.count; i++) {

        PhorestStaffMember * workingStaffMember = [[PhorestStaffMember alloc] init];
        workingStaffMember = [self.workingStaffArray objectAtIndex:i];
        
        for (int j=0; j<self.allStaffArray.count; j++) {
            PhorestStaffMember * allStaffMember =[[PhorestStaffMember alloc] init];
            
            allStaffMember=[self.allStaffArray objectAtIndex:j];
            
            
            if (![workingStaffMember isEqual:allStaffMember]) {
               // NSLog(@"working id %d != %d all Staff id add",[workingStaffMember.staff_id intValue],[allStaffMember.staff_id intValue]);
                allStaffMember.isWorking=NO;
                
                [idArray addObject:allStaffMember.staff_id];
                [staffSet addObject:allStaffMember];
                
                
            }
            
        }
    }
    
    [fullArray addObjectsFromArray:[staffSet allObjects]];
    
    
    [fullArray sortUsingDescriptors:[NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"staff_first_name" ascending:YES selector:@selector(caseInsensitiveCompare:)], nil]];
    
    
    for (int i=1; i<self.workingStaffArray.count; i++) {
        PhorestStaffMember * workingMember = [self.workingStaffArray objectAtIndex:i];
        
        for (int j=0; j<fullArray.count; j++) {
            PhorestStaffMember * staff = [fullArray objectAtIndex:j];
            
            if ([workingMember isEqual:staff]) {
                [fullArray removeObject:staff];
            }
            
        }
        
    }
    
    
    [self.workingStaffArray addObjectsFromArray:fullArray];
    
    
    
    
    
    
    /*
     ////////////////////////////////////////////
     
     Removes Any stylist - if issue remove this
     */
    NSMutableArray * workingArray = [NSMutableArray array];
    for (PhorestStaffMember*staff in self.workingStaffArray) {
        if (staff.isWorking) {
            [workingArray addObject:staff];
        }
        
    }
    
    if (workingArray.count==2) {
        
        for (PhorestStaffMember * staff in workingArray) {
            if (staff.isAnyStylist) {
                
                [self.workingStaffArray removeObject:staff];
            }
        }
    }
    /*
     End of removes any stylist
     
     /////////////////////////////////////////////////
     */
    
    
}


#pragma mark - Get Chosen day
-(NSString *)getChosenDay{
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init] ;
    [deltaComps setDay:1];
    NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
    NSDate * today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    switch (self.PreferredDay) {
        case PreferredDayToday:
            
           // NSLog(@"Date %@",[dateFormatter stringFromDate:today]);
            self.chosenDate=today;
            return [dateFormatter stringFromDate:today];
            
            break;
        case PreferredDayTomorrow:
            
            //NSLog(@"Date %@",[dateFormatter stringFromDate:tomorrow]);
            self.chosenDate=tomorrow;
            return [dateFormatter stringFromDate:tomorrow];
            break;
        case PreferredDayLater:
            
            self.chosenDate=self.selectedDate;
            //NSLog(@"Selected Date %@",[dateFormatter stringFromDate:self.selectedDate]);
            return [dateFormatter stringFromDate:self.selectedDate];
            break;
            
        default:
            
            return [dateFormatter stringFromDate:today];
            break;
    }
    
    
    return [dateFormatter stringFromDate:today];
}




#pragma mark - Select stylist Process
- (void)createStylistView {
    

    
    SelectStylistViewController * s = [self.storyboard instantiateViewControllerWithIdentifier:@"selectStylistVC"];
    s.delegate = self;
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:s];
 
    //now present this navigation controller modally
    [self presentViewController:navigationController
                       animated:YES
                     completion:^{
                         
                     }];
    
    
//
//    SelectStylistViewController * stylistView = [self.storyboard instantiateViewControllerWithIdentifier:@"selectStylistVC"];
//    stylistView.modalPresentationStyle = UIModalPresentationCustom;
//
////    stylistView.workers=self.workingStaffArray;
//    stylistView.transitioningDelegate = self;
//    stylistView.delegate=self;
////    stylistView.salonObj = self.salonData;
//    [self presentViewController:stylistView animated:YES completion:nil];
    
}

- (IBAction)selectStylist:(id)sender {
    [self createStylistView];

//    if (self.isArrayOfStylistDownloaded==NO && self.hasPickedService==NO) {
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please pick a service in order to get the available stylists." cancelButtonTitle:@"OK"];
//        });
//    }else{
//
//        [self downloadAvailableStaffForService];
//    }
    
}

/*
-(void)cancelStylistForm{
    
    
    [self popOutViewAnimationWithView:self.laterPickerContainer andWithCoverView:self.coverView];
}
 */




#pragma mark - Select Time Process
- (void)createTimeView {
    
    
    SelectTimeViewController * s = [self.storyboard instantiateViewControllerWithIdentifier:@"selectTimeVC"];
    
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:s];

//    s.timesArray=self.availableTimesArray;
    s.delegate=self;
    //now present this navigation controller modally
    [self presentViewController:navigationController
                       animated:YES
                     completion:^{
                         
                     }];
    
}
- (IBAction)selectTime:(id)sender {
    
    [self createTimeView];
    return;
    if ([WSCore isDummyMode]) {
        
        [self createTimeView];
    }
    else{
        if (self.isArrayOfStylistDownloaded==NO && self.workingStaffArray.count==0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please pick a stylist in order to get the available times." cancelButtonTitle:@"OK"];
            });
        }
        else if (self.isArrayOfStylistDownloaded==NO) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please pick a service in order to get the available times." cancelButtonTitle:@"OK"];
            });
        }else if(self.workingStaffArray.count==0){
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView showSimpleAlertWithTitle:@"Bummer!" message:@"There are no stylists available for that service today. Please try another service, or try another day." cancelButtonTitle:@"OK"];
            });
        }else{
            [self createTimeView];
        }
    }
}

#pragma mark - Default values

/*
 Returns values to their original state/
 */
-(void)defaultValues{
    
    
    [self.timeButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.timeButton setTitle:@"Select Time" forState:UIControlStateNormal];
    
    [self.stylistButton setTitle:@"Select Stylist" forState:UIControlStateNormal];
    [self.stylistButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    
    self.isTimePicked=NO;
    self.isArrayOfStylistDownloaded=NO;
    self.calendar=nil;
    
}

- (void)dateButtonSetUp {
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init] ;
    [deltaComps setDay:1];
    NSDate* tomorrow = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
    NSDate * today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd"];
    
    [self.todayButton setTitleColor:[UIColor lightGrayColor]forState:UIControlStateNormal];
    [self.tomorrowButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.laterButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    
    [self.todayButton setTitle:[NSString stringWithFormat:@"Today \n%@%@",[dateFormatter stringFromDate:today],[WSCore daySuffixForDate:today] ] forState:UIControlStateNormal];
    [self.tomorrowButton setTitle:[NSString stringWithFormat:@"Tomorrow \n%@%@",[dateFormatter stringFromDate:tomorrow],[WSCore daySuffixForDate:tomorrow]] forState:UIControlStateNormal];
    [self.laterButton setTitle:@"Later \n-" forState:UIControlStateNormal];
    
    self.todayButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.todayButton.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    self.todayButton.titleLabel.textAlignment=NSTextAlignmentCenter;
    
    
    self.tomorrowButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.tomorrowButton.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    self.tomorrowButton.titleLabel.textAlignment=NSTextAlignmentCenter;
    
    self.laterButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.laterButton.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    self.laterButton.titleLabel.textAlignment=NSTextAlignmentCenter;
    

    self.todayButton.tag=0;
    self.tomorrowButton.tag=1;
    self.laterButton.tag=2;
    [self selectedButton:self.todayButton];
 
}

#pragma mark - Create and Destroy Calendar methods
-(void)createCalendar{
    if (self.calendar==nil) {
        self.calendar = [[CKCalendarView alloc] init];
        self.calendar.delegate=self;
        self.calendar.titleColor=self.pickerButtonColor;
        self.calendar.frame = CGRectMake(10, 40, 300, 300);
        //self.calendar.center = self.view.center;
        self.calendar.backgroundColor=[UIColor clearColor];
        if (self.selectedDate!=nil) {
            [self.calendar selectDate:self.selectedDate makeVisible:YES];
        }
    }
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = 1;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    
    self.minimumDate= nextDate;
    
    self.calendar.onlyShowCurrentMonth = NO;
    
    [self.calendar setInnerBorderColor:[UIColor whiteColor]];
    
    [self.laterPickerContainer addSubview:self.calendar];
    
}

-(void)destroyPickerContainerView{
    [self.laterPickerContainer removeFromSuperview];
    [self.coverView removeFromSuperview];
    self.laterPickerContainer=nil;
    self.coverView=nil;
}

#pragma mark - CKCalendar Delegate
- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date {
    
    /*
     the date is chosen from the calendar - this is used later when done is selected on the later form
     */
    self.chosenDate=date;
    
    self.doneLaterFormButton.hidden=NO;
    NSLog(@"User picked %@",date);
}


- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
    return YES;
}


- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date {
    
    if ([date compare: self.minimumDate] != NSOrderedDescending) {
        
        dateItem.textColor = [UIColor lightGrayColor];
        dateItem.backgroundColor = [UIColor clearColor];
        
    }
}


- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    
    return [date compare: self.minimumDate] == NSOrderedDescending;
}


- (void)calendar:(CKCalendarView *)calendar didChangeToMonth:(NSDate *)date {
    
    //NSLog(@"Hurray, the user changed months!");
}

- (BOOL)dateIsDisabled:(NSDate *)date {
    
    if ([date compare: self.minimumDate] != NSOrderedDescending) {
        
        return YES;
        
    }
    
    return NO;
}

#pragma mark - prepareForSegue
/*
 Notifies the view controller that a segue is about to be performed
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"confirmBookingB"]) {
        TabConfirmViewController * confirmVC = (TabConfirmViewController *)segue.destinationViewController;
        
        confirmVC.salonData = self.salonData;
        confirmVC.bookingObj=self.bookObj;
       // confirmVC.isFromFavourite=self.isFromFavourite;
        //confirmVC.unwindBackToSearchFilter=self.unwindBackToSearchFilter;
        
        NSArray *timeSegments = [self.appointmentTime componentsSeparatedByString:@":"];
        
        
        //get pm/am
        NSArray * minsAmPmSegments = [timeSegments[1] componentsSeparatedByString:@" "];
        
        
        NSDate *currentDate = self.chosenDate;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate]; // Get necessary date components
        
        [components month]; //gives you month
        [components day]; //gives you day
        [components year]; // gives you year
        
        NSString* firstString = [timeSegments objectAtIndex:0];
        NSString* secondString = [minsAmPmSegments objectAtIndex:0];
        
        if ([[minsAmPmSegments[1] lowercaseString ]isEqualToString:@"pm"]) {
            firstString = [NSString stringWithFormat:@"%d",[firstString intValue] + 12];
            
        }
        
        NSDate * fullDate = [WSCore dateWithYear:[components year] month:[components month] day:[components day] WithHour:[firstString intValue] AndMin:[secondString intValue]];
        confirmVC.chosenDate=fullDate;
        
        /*
         Check which day is selected
         */
        switch (self.PreferredDay) {
                
                /*
                 If today is selected then
                 Assign chosenDay a string containing Today and the chosen Time
                 */
            case PreferredDayToday:
                
                self.bookObj.chosenDay=[NSString stringWithFormat:@"Today, %@",self.bookObj.chosenTime];
                
                break;
                
                /*
                 If tomorrow is selected as the preferred day
                 Assign chosenday a string containing Tomorrow plus the chosenTime
                 */
            case PreferredDayTomorrow:
                
                self.bookObj.chosenDay=[NSString stringWithFormat:@"Tomorrow, %@",self.bookObj.chosenTime];
                
                break;
                
                /*
                 If later is selected as the preferred day
                 Assign chosenDay the self.dateDetailString which contains the Date and Time
                 
                 */
            case PreferredDayLater:
                
                self.bookObj.chosenDay=[NSString stringWithFormat:@"%@",self.timeDetail];
                
                break;
                
            default:
                
                self.bookObj.chosenDay=[NSString stringWithFormat:@"Today, %@",self.bookObj.chosenTime];
                
                break;
        }
        
        
        
    }
}

-(void)goToBookingScreenInDummyMode{
    
    
    [WSCore dismissNetworkLoadingOnView:self.view];
    [self performSegueWithIdentifier:@"confirmBookingB" sender:self];
}

#pragma mark - UNWIND Segue

- (IBAction)unwindToTabBookUI:(UIStoryboardSegue *)unwindSegue
{
    [WSCore resetLoginType];
    //NSLog(@"Registered User");
    if ([unwindSegue.sourceViewController isKindOfClass:[TabRegisterViewController class]] || [unwindSegue.sourceViewController isKindOfClass:[TabLoginPageViewController class]]) {
        self.unwindRegisteredUser=YES;
    }
    
}


#pragma mark - UIViewControllerTransitioningDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    
    
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    
    return [[DismissDetailTransition alloc] init];
    
    
}
#pragma mark - Premier Client Information
-(void)createPremierClientSuccessfulWithMessage:(NSString *)message{
    //NSLog(@"Success Message: %@",message);
    
    [self performSegueWithIdentifier:@"confirmBookingB" sender:self];
}

-(void)createPremierClientFailureWithMessage:(NSString *)message{
    [UIView showSimpleAlertWithTitle:@"An Error Occurred" message:message cancelButtonTitle:@"OK"];
    //NSLog(@"Failure Message: %@",message);
}

#pragma mark - Formating Date
-(NSString *)laterFormattedDate: (NSDate *) date{
    
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init] ;
    [deltaComps setDay:1];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM"];
    
    return [dateFormatter stringFromDate:date];
}

#pragma mark - Custom button methods
-(void)selectedButton: (UIButton *)btn{
    
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    switch (btn.tag) {
        case 0:
            [Gradients addTodayButtonLayerWithView:_todayButton];
            break;
        case 1:
            [Gradients addTomorrowButtonLayerWithView:_tomorrowButton];
            break;
        case 2:
            [Gradients addLaterButtonLayerWithView:_laterButton];
            break;
        default:
            break;
    }
}

-(void)deselectButton: (UIButton *)btn{
    btn.layer.borderColor=[UIColor clearColor].CGColor;
    btn.layer.borderWidth=0.0f;
    btn.backgroundColor = [UIColor whiteColor];
    [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
}


- (IBAction)bookNow:(id)sender {
    [self book];
}

/////////////////////////////////////////
#pragma mark - Booking action (Main Button) ***
-(void)book{

    if ([[User getInstance] isUserLoggedIn]) {
        
        if (![[User getInstance] isTwilioVerified]) {
            [self destinationViewController];
        }
        else if (!self.hasPickedService && !self.isTimePicked && !self.isArrayOfStylistDownloaded) {
            
            [UIView showSimpleAlertWithTitle:@"Oh Oh" message:@"Please fill out the entire form." cancelButtonTitle:@"OK"];
        }
        else{
            
            [self destinationViewController];
        }
        
    }
    else{
        
        [self destinationViewController];
    }
    
    
}





#pragma mark - Destination view controller
/*
 Determines which viwe controller to go to
 */
- (void)destinationViewController {
    
    
    /*
     Check if the user is not logger in
     */
    if (![[User getInstance] isUserLoggedIn]) {
        
        //Go to login controller
        //loginSignUpVC
        TabLoginOrSignUpViewController * tab = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignUpVC"];
        tab.delegate=self;
        [WSCore setLoginType:LoginTypeMakeBooking];
        [self presentViewController:tab animated:YES completion:nil];
        
        
    }
    
    else if(![[User getInstance] isTwilioVerified]){
        
        
        TwilioVerifiyViewController * twilioVC = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyVC"];
        twilioVC.delegate=self;
        [self presentViewController:twilioVC animated:YES completion:nil];
    }
    
    /*
     Check if the user has a credit card assigned
     */
    else if(![self hasMoneyToMakeBooking]){
        
        //show credit card
        TabLinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
        linkCC.delegate=self;
        [self presentViewController:linkCC animated:YES completion:nil];
        
        
    }
    /*
     If the user is logged in, and has a credit card attached to their account, then everything is ok
     */
    else{
        
        if (![WSCore isDummyMode]) {
            
            
            if (self.bookObj.patchTestRequired) {
                
                UIAlertView * patchTestAlert = [[UIAlertView alloc] initWithTitle:@"Patch Test Required" message:[NSString stringWithFormat:@"If you have not already had a patch test, please contact %@. Have you had a patch test?",self.salonData.salon_name] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                patchTestAlert.tag=1212;
                [patchTestAlert show];
            }
            else{
                [self makeBooking];
            }
            
        }
        else{
            
            
            [WSCore showNetworkLoadingOnView:self.view];
            [self performSelector:@selector(goToBookingScreenInDummyMode) withObject:nil afterDelay:1.5];
            
        }
        
    }
}


- (void)makeBooking{
    
    //if salon type is Phorest - make booking straight away
    //else pass details to next screen
    if ([self.salonData fetchSalonType] == SalonTypePhorest) {
        
        [self.salonBookingModel bookPhorestServiceWithUserID:[[User getInstance] fetchAccessToken] WithSalonID:self.salonData.salon_id WithSlotID:self.bookObj.slot_id WIthServiceID:self.bookObj.service_id WithRoomID:self.bookObj.room_id WithInternalStartDateTime:self.bookObj.internal_start_datetime WithInternalEndDateTime:self.bookObj.internal_end_datetime AndStaff_id:self.bookObj.staff_id];
        
    }
    else if([self.salonData fetchSalonType] == SalonTypeIsalon){
        
        /*
         different booking process for iSalon only segue is needed
         there is no prebooking with iSalon
         */
        
        //go to next screen and pass details
        [self performSegueWithIdentifier:@"confirmBookingB" sender:self];
    }
    else if([self.salonData fetchSalonType] == SalonTypePremier){
        
        
        [self.salonBookingModel premierCreateClientWithBookingObject:self.bookObj];
        
    }
    
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    //patchTestAlert
    if (alertView.tag == 1212) {
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            [self makeBooking];
        }
    }
}



@end
