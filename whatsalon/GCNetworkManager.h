//
//  GCNetworkManager.h
//  whatsalon
//
//  Created by Graham Connolly on 05/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GCNetworkManager;

/*!
     @protocol GCNetworkManagerDelegate
  
     @brief The GCNetworkManagerDelegate protocol
  
     This protocol is used to handle the success, failure, and server failure responses from a web service.
 */
@protocol GCNetworkManagerDelegate <NSObject>

@optional
/*!
 method for handling a successful response from the web service.
 */
-(void)manager:(GCNetworkManager *) manager handleSuccessMessageWithDictionary:(NSDictionary *) jsonDictionary;

/*!
 method for handling a failure response from the web service.
 */
-(void)manager:(GCNetworkManager *) manager handleFailureMessageWitDictionary:(NSDictionary *) jsonDictionary;

/*!
 method for handling a server failure response from the server.
 */
-(void)manager: (GCNetworkManager *) manager handleServerFailureWithError: (NSError *)error;

@end
@interface GCNetworkManager : NSObject

/*!
 @brief the GCNetworkManagerDelegate object
 */
@property (nonatomic,assign) id<GCNetworkManagerDelegate> delegate;

/*!
 @brief  parentView - UIView that references the calling views view
 */
@property (nonatomic) UIView * parentView;

/*!
 @brief dataTask - NSURLSessionDataTask used for network calls in GCNetworkManager
 */
@property (nonatomic) NSURLSessionDataTask * dataTask;

/*!
 @brief the UIViewController of the calling Controller
 */
@property (nonatomic) UIViewController * parentViewController;

/*!
 @brief An NSString that can be set to any description
 */
@property (nonatomic) NSString * tagDescription;

/*!
 @brief bool value that determines whether loading messages should be shown or not
 */
@property (nonatomic) BOOL shouldNotShowMessages;

/*!
 @brief the progressView that can show the progress of the data request
 */
@property (nonatomic)UIProgressView* progressView;

/*!
 @brief performs a network request with the given URL, parameters, HTTP Method, and whether a loading dialog should be shown.
 @param url - NSURL the URL to make the request
 @param params - represents the parameters passed in.
 @param method - represents the HTPP Method
 @param b - represents the BOOL whether a loading dialog should be shown or not
 */
-(void)loadWithURL:(NSURL *)url withParams:(NSString *)params WithHTTPMethod: (NSString *) method AndWithLoadingDialog:(BOOL)b;

/*!
 @brief performs a network request with the given URL, HTTP Method, and whether a loading dialog should be shown.
 @param url - NSURL the URL to make the request
 @param method - represents the HTPP Method
 @param b - represents the BOOL whether a loading dialog should be shown or not
 */
-(void)loadWithURL:(NSURL *)url WithHTTPMethod:(NSString *)method AndWithLoadingDialog:(BOOL)b;

/*!
 @brief performs a network request with the given URL, parameters (as an NSDictionary), HTTP Method, and whether a loading dialog should be shown.
 @param url - NSURL the URL to make the request
 @param dict - represents an NSDictionary of parameters passed in.
 @param method - represents the HTPP Method
 @param b - represents the BOOL whether a loading dialog should be shown or not
 */
-(void)loadWithURL:(NSURL *)url withDictionary: (NSDictionary *)dict WithHTTPMethod: (NSString *) method  AndWithLoadingDialog: (BOOL) b;

/*!
 @brief cancels the network request
 @discussion performs cancel on the NSURLSessionDataTask
 */
-(void)cancelTask;

/*!
 @brief performs a network request with a progress loader with the given URL, parameters, HTTP Method, and whether a loading dialog should be shown.
 @param url - NSURL the URL to make the request
 @param params - represents the parameters passed in.
 @param method - represents the HTPP Method
 @param b - represents the BOOL whether a loading dialog should be shown or not
 */
-(void)progressLoaderWithURL:(NSURL *)url withParams:(NSString *)params WithHTTPMethod: (NSString *) method AndWithLoadingDialog:(BOOL)b;

/*!
 @brief kills the current state of CGNetworkManager
 @discussion hides the network Activity Indicator, dismisses the network loading indicator, cancels the data task and sets the delegate to nil
 */
-(void)destroy;
@end
