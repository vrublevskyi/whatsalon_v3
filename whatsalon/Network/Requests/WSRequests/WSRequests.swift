//
//  WSRequests.swift
//  WhatSalon
//
//  Created by admin on 7/11/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

import Alamofire
import UIKit
//TODO: Maybe separate to different requests classes
extension WSAPI {

    enum Request: RequestConvertible {

        case updateWorkingHours(parameters: Parameters)


        //MARK: Private.Property

        var baseURLString: String {
            return "https://sheets-dev.whatsalon.com/api/"
        }

        var method: HTTPMethod {
            return .post
        }

        var path: String {
            switch self {
            case .updateWorkingHours:
                return "Open_hours/update"
      
            }
        }

        var parameters: Parameters? {
            switch self {
            case .updateWorkingHours(let parameters):
                return parameters
 
            default: return nil
            }
        }
//        var headers: Headers? {
//            var headers: Headers = ["Content-Type" : self.contentType]
//            if let id = User.shared?.id {
//                headers["id"] = id
//            }
//            if let token = User.shared?.auth_token {
//                headers["auth_token"] = token
//            }
//            return headers
//        }

        //MARK: Private

        private var contentType: String {
            return "application/json;charset=UTF-8"
        }
    }
}
