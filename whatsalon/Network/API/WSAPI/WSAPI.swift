//
//  WSAPI.swift
//  WhatSalon
//
//  Created by admin on 7/5/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Alamofire
//import SVProgressHUD
import AlamofireObjectMapper

class WSAPI: APIHandable {

    //MARK: APIHandable

    var service: ServiceProtocol = AlamofireService()

    func isValid<T>(response: DataResponse<T>) -> Bool {
        guard let data = response.data, data.count > 0 else {
            return false
        }

        if let json = try? JSONSerialization.jsonObject(with: data) as? [String: Any] {
            return (json?["success"] as? Bool) ?? false
        }

        return false
    }

    func error<T>(response: DataResponse<T>) -> [ErrorProtocol]? {
        guard let data = response.data, data.count > 0 else {
            if let error = response.error {
                let error = error as NSError
                if error.code == URLError.Code.notConnectedToInternet.rawValue {
                    return [WSError.internetConnection]
                }
                if error.code == URLError.Code.cancelled.rawValue {
                    return [WSError.requestCancelled]
                }
                return [WSError.custom(error: error, statusCode: error.code)]
            }

            return [WSError.somethingWentWrong]
        }

        if let json = try? JSONSerialization.jsonObject(with: data) as? [String: Any],
            let message = json?["message"] as? [String : Any],
            let formErrors = message["form_errors"] as? [[String : String]] {
            return formErrors.map {
                if let text = $0["err_msg"] {
                    return WSError.texted(text: text)
                }
                return WSError.somethingWentWrong
            }
        }
        return [WSError.somethingWentWrong]
    }

    //MARK: Public

//    @discardableResult
//    func login(with email: String, password: String, completion: @escaping LoginCompletion) -> DataRequest? {
//        let request = Request.updateWorkingHours(parameters: ["login_email" : email,
//                                                 "login_password" : password])
//        return self.perform(request)?.responseObject(keyPath: "message") { (response: DataResponse<String>) in
//            var error: ErrorProtocol?
//            if let errorResponse = self.handle(response: response)?.first {
//                error = errorResponse
//            }
//
//            completion(response.result.value, error)
//        }
//    }
    
    typealias LoginCompletion = ((_ user:  String?,_ error: ErrorProtocol?) -> Void)

}
