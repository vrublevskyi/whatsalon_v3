//
//  DiscoveryImageTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 12/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoveryImageTableViewCell : UITableViewCell

@property (nonatomic) UIImageView * mainImage;
@property (nonatomic) UILabel * titleLabel;
@property (nonatomic) UILabel * descriptionLabel;

-(void)setCellImageWithURL: (NSString *)url;

@end
