//
//  NSString+NumberChecking.m
//  whatsalon
//
//  Created by Graham Connolly on 05/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "NSString+Validations.h"

@implementation NSString (Validations)

+(bool) isNumber: (NSString *) string {
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([string rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9
        return YES;
    }

    return NO;
    
}

+ (BOOL)validateStringIsExpiryDate:(NSString *)string
{
    NSString * pattern=@"^(0[1-9]|[12][0-9]|3[01])[/](19|20)\\d\\d$";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    //NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = NO;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = YES;
    
    return didValidate;
}

+(BOOL)isValidFirstName: (NSString *)string{
    //should be composed of standard English letters and between one and twenty-five characters in length.
    NSString * pattern=@"^[a-z'. ]{1,25}$";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    //NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = NO;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = YES;
    
    return didValidate;
}

+(BOOL)isValidSurname: (NSString *) string{
    
    //should be composed of standard English letters and between one and ten characters in length.
    NSString * pattern=@"^[a-z' ]{1,25}$";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    //NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = NO;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = YES;
    
    return didValidate;
    
}

+(BOOL)isValidEmail: (NSString *)emailString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[\\da-zA-Z_+\\.\\-\\/(“|”)]+@(([a-zA-Z\\-]+(\\.[a-zA-Z]+){1,2})|([?\\d{1,3}\\.\\d{1,3}\\.‌​\\d{1,3}\\.\\d{1,3}]?))";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailString];

}
@end
