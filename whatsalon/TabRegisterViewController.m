//
//  TabRegisterViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 21/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabRegisterViewController.h"
#import "GCNetworkManager.h"
#import "NSString+Validations.h"
#import "UIView+AlertCompatibility.h"
#import "User.h"

@interface TabRegisterViewController ()<GCNetworkManagerDelegate>

/*! @brief represents the original x coordinate for the sign up button. */
@property (nonatomic) CGFloat signUpButtonOriginalX;

/*! @brief represents the original y coordinate for the sign up button. */
@property (nonatomic) CGFloat signUpButtonOriginalY;

/*! @brief represents the number NSString. */
@property (nonatomic) NSString * number;

/*! @brief represents the NSString for the password. */
@property (nonatomic) NSString * password;

/*! @brief represents the NSString for the coutnry. */
@property (nonatomic) NSString * country;

/*! @brief represents the NSString for the country code. */
@property (nonatomic) NSString * countryCode;

/*! @brief represents the NSString for the email address. */
@property (nonatomic) NSString * emailAddress;

/*! @brief represents the verify button. */
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;

/*! @brief represents the NSString for the user id. */
@property (nonatomic) NSString * user_id;

/*! @brief represent the NSString for the user phone. */
@property (nonatomic) NSString * user_phone;

/*! @brief represents the NSString for the user email. */
@property (nonatomic) NSString * user_email;

/*! @brief determines if the twilio message has been sent. */
@property (nonatomic,assign) BOOL isTwilioSent;

/*! @brief represents the background image view. */
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

/*! @brief represents the country code row selected. */
@property (nonatomic) NSInteger countryCodeRow;

/*! @brief represents the GCNetworkManager for making a request to register the user. */
@property (nonatomic) GCNetworkManager * registerNetworkManager;

/*! @brief represents the GCNetworkManager for validating Twilio. */
@property (nonatomic) GCNetworkManager * validateTwilioNetworkManager;

/*! @brief represents the GCNetworkManager for resend the twilio request. */
@property (nonatomic) GCNetworkManager * resendTwilioNetworkManager;
 
 /*! @brief represents the cover view that blocks UI Elements when the country picker is in view. */
@property (nonatomic) UIView * coverView;

@end

@implementation TabRegisterViewController


#pragma mark - UIViewController Delegate methods
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
   
    
    self.vericationCodeTextField.hidden=YES;
    self.vericationCodeTextField.alpha=0.0;
    self.smsVerifcationLabel.hidden=YES;
    self.smsVerifcationLabel.alpha=0.0;
    self.signUpButton.backgroundColor = [UIColor clearColor];
    self.signUpButton.layer.cornerRadius = 6.0;
    self.signUpButton.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    self.signUpButton.layer.borderWidth =1.0;
    [self.signUpButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.myNavigationItem.rightBarButtonItem=nil;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [WSCore addDarkBlurAsSubviewToView:self.backgroundImage];
    
    
    
    self.isTwilioSent=NO;
    
    self.title=@"Register";
    [self navigationBarSetUp];
    
    
    [WSCore addTopLine:self.firstNameView :kCellLinesColour :0.5];
    [WSCore addTopLine:self.lastNameView :kCellLinesColour :0.5];
    [WSCore addBottomIndentedLine:self.firstNameView :kCellLinesColour];
    [WSCore addBottomLine:self.lastNameView :kCellLinesColour];
    [WSCore addBottomIndentedLine:self.viewForEmailTF :kCellLinesColour];
    [WSCore addBottomIndentedLine:self.viewForPasswordTF :kCellLinesColour];
    [WSCore addBottomLine:self.viewForPhoneNumber :kCellLinesColour];
    
    
    self.smsVerificationView.backgroundColor = [UIColor clearColor];
    self.smsVerificationView.alpha=0.0;
    self.isCountryCodeViewShowing = NO;
    
    self.countryCodePicker.delegate =self;
    self.countryCodePicker.dataSource =self;
    self.countryCodePicker.clipsToBounds=YES;
    
    [self.viewForAreaCodePicker addSubview:self.countryCodePicker];
    self.viewForAreaCodePicker.clipsToBounds=YES;
    
    self.countryCodes = [[NSMutableArray alloc] initWithObjects:@"+353",@"+44",@"+1", nil];
    self.countries = [[NSMutableArray alloc] initWithObjects:@"Ireland",@"United Kingdom",@"United States", nil];
    
    self.emailAddressTextField.autocapitalizationType=UITextAutocapitalizationTypeNone;
    self.emailAddressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.emailAddressTextField.delegate=self;
    self.emailAddressTextField.keyboardType=UIKeyboardTypeEmailAddress;
    self.passwordTextField.delegate =self;
    self.phoneNumberTextField.delegate =self;
    self.phoneNumberTextField.keyboardType=UIKeyboardTypePhonePad;
    self.vericationCodeTextField.delegate=self;
    self.vericationCodeTextField.keyboardType=UIKeyboardTypeNumberPad;
    
    self.smsVerifcationLabel.hidden=YES;
    self.smsVerifcationLabel.alpha=0.0;
    
    self.vericationCodeTextField.hidden=YES;
    self.vericationCodeTextField.alpha=0.0;
    
    self.signUpButtonOriginalX = self.signUpButton.frame.origin.x;
    self.signUpButtonOriginalY = self.signUpButton.frame.origin.y;
    
    
    self.viewForNavigationBar.backgroundColor=[UIColor clearColor];
    
    self.signUpButton.enabled=YES;
    
    
    self.vericationCodeTextField.keyboardType = UIKeyboardTypeDefault;
    
    [WSCore addTopLine:self.vericationCodeTextField :kCellLinesColour :0.5];
    [WSCore addBottomLine:self.vericationCodeTextField :kCellLinesColour];
    
    self.firstName.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.lastName.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.firstName.autocorrectionType=UITextAutocorrectionTypeNo;
    self.lastName.autocorrectionType=UITextAutocorrectionTypeNo;
    
    self.signUpButton.hidden=YES;
    self.vericationCodeTextField.placeholder=@"###";
    self.vericationCodeTextField.returnKeyType=UIReturnKeyDone;
    
}

-(void)backButtonAction{
    
    NSLog(@"Pop");
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Set up methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

-(void)navigationBarSetUp{
    
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
    self.navigationBar.tintColor=[UIColor whiteColor];
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    [self.cancelButton setImage:[UIImage imageNamed:@"icon_x_cancel"]];
}





#pragma mark - UIPickerView delegate and datasoure methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return self.countryCodes.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    
    return [NSString stringWithFormat:@"%@ %@",[self.countries objectAtIndex:row],[self.countryCodes objectAtIndex:row]];
}


#pragma mark - UITextfield delegate methods
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (self.isCountryCodeViewShowing)
        [self hideCountryPicker];
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
#pragma warning - animation no longer works
    int movementDistance=0; // tweak as needed
    
    if (IS_iPHONE4) {
        movementDistance = 145;
    }else if (IS_iPHONE5_or_Above){
        
        movementDistance= 50;
    }
    
    const float movementDuration = 0.2f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView transitionWithView:self.scrollView duration:movementDuration options:UIViewAnimationOptionCurveLinear animations:^{
        self.scrollView.frame = CGRectOffset(self.scrollView.frame, 0, movement);
    } completion:nil];
}

#pragma mark - UIPickerView datasource methods

-(void)resetPage{
    self.verifyButton.enabled=YES;
    [self hideVerifactionCodeArea];
    self.isTwilioSent=NO;
    
    if (self.isCountryCodeViewShowing==NO) {
        [self doneCountryCodePicker];
    }
}



#pragma mark - Custom animation

- (void)doneCountryCodePicker {
    
    self.countryCodeRow = [self.countryCodePicker selectedRowInComponent:0];
    NSLog(@"country code row %ld",(long)[self.countryCodePicker selectedRowInComponent:0]);
    [self.areaCodeButton setTitle:[self.countryCodes objectAtIndex:self.countryCodeRow] forState:UIControlStateNormal];
    
    [self.coverView removeFromSuperview];
    [self.viewForAreaCodePicker removeFromSuperview];
    self.isCountryCodeViewShowing = NO;
    
}

-(void)hideCountryPicker{
    
    [self.viewForAreaCodePicker removeFromSuperview];
    self.isCountryCodeViewShowing = NO;
    
}

#pragma mark - IBActions

- (IBAction)changeAreaCode:(id)sender {
    
    [self.view endEditing:YES];
    if (!self.isCountryCodeViewShowing) {
        
        
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-191, 320, 191)];
        [WSCore addBlurToViewForPicker:view];
        UIPickerView * picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, 320, 162)];
        picker.showsSelectionIndicator=YES;
        picker.delegate = self;
        self.countryCodePicker = picker;
        [picker selectRow:self.countryCodeRow inComponent:0 animated:NO];
        
        
        if (self.coverView==nil) {
            self.coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-view.frame.size.height)];
            self.coverView.backgroundColor=[UIColor blackColor];
            self.coverView.alpha=0.4;
            
        }
        [self.view addSubview:self.coverView];
        
        UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 2, 80, 40)];
        [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [cancelButton setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelCountryCodePicker) forControlEvents:UIControlEventTouchUpInside];
        cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [view addSubview:cancelButton];
        
        UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(254, 0, 66, 44)];
        [button setTitle:@"Done" forState:UIControlStateNormal];
        [button setTitleColor:kDefaultTintColor forState:UIControlStateNormal];
        [button addTarget:self action:@selector(doneCountryCodePicker) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:button];
        
        
        [view addSubview:picker];
        
        self.viewForAreaCodePicker= view;
        
        [self.view addSubview:self.viewForAreaCodePicker];
        
        
        self.isCountryCodeViewShowing = YES;
    }
    else{
        
        
        [self doneCountryCodePicker];
        
    }
    
}

-(void)cancelCountryCodePicker{
    [self.coverView removeFromSuperview];
    [self hideCountryPicker];
}

#pragma mark - Sign up process
//1a
- (IBAction)verifySignUp:(id)sender {
    
    [self.view endEditing:YES];
    if (self.isCountryCodeViewShowing)
        [self doneCountryCodePicker];
    
    if (![NSString isValidEmail:self.emailAddressTextField.text]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Email Error." message:@"Please enter a valid email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([self.emailAddressTextField.text length] == 0 ||[self.passwordTextField.text length]==0 ||  [self.phoneNumberTextField.text length]==0 || [self.firstName.text length]==0 || [self.lastName.text length] ==0) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oops." message:@"Please fill out all fields." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if([self.passwordTextField.text length]<6){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Password Error." message:@"Your password must be at least 6 characters long." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    else{
        
        self.number = [NSString stringWithFormat:@"%@%@",self.areaCodeButton.titleLabel.text, self.phoneNumberTextField.text ];
        self.number = [self.number stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        self.emailAddress=self.emailAddressTextField.text;
        self.emailAddress = [self.emailAddress stringByReplacingOccurrencesOfString:@"`" withString:@""];
        
        [self registerForTwilio];
        
    }
    
    
}

//1b
-(void)registerForTwilio{
    
    
    [self.view endEditing:YES];
    if (self.registerNetworkManager==nil) {
        self.registerNetworkManager = [[GCNetworkManager alloc] init];
        self.registerNetworkManager.parentView=self.view;
        self.registerNetworkManager.delegate=self;
    }
    
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kRegister_User_v2_URL]];
    
    
    NSString * params =[NSString stringWithFormat:@"email=%@",self.emailAddress];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&password=%@",self.passwordTextField.text]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&phone_number=%@",self.number]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&name=%@",self.firstName.text]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_last_name=%@",self.lastName.text]];
    
    NSLog(@"Register Params %@",params);
    [self.registerNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"json dictionary %@",jsonDictionary);
    
    if (manager==self.registerNetworkManager) {
        [self revealVerificationCodeTextArea];
        
        [UIView showSimpleAlertWithTitle:@"Verification Number" message:@"User registered successfully. A verfication number will be sent to your phone. Please use this number to continue." cancelButtonTitle:@"OK"];
        
    }else if(manager==self.validateTwilioNetworkManager){
        
        self.user_id = jsonDictionary[@"message"][@"user_id"];
        self.user_phone =jsonDictionary[@"message"][@"user_phone"];
        self.user_email = jsonDictionary[@"message"][@"user_email"];
        [[User getInstance] updateKey:jsonDictionary[@"message"][@"secret_key"]];
        [self addDetailsToDefaults];
        
        [[User getInstance] updateTwilioVerified:YES];
        NSLog(@"Is twilio verfied %d",[[User getInstance] isTwilioVerified]);
        
       
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Verified number" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        [alert show];
        
        double delayInSeconds = 1.0;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alert dismissWithClickedButtonIndex:0 animated:YES];
            
            
            if ([WSCore fetchLoginType]==LoginTypeSideMenu) {
                NSLog(@"Unwind to right menu");
                [self performSegueWithIdentifier:@"unwindToRightMenu" sender:self];
            }
            else if([WSCore fetchLoginType] == LoginTypeWalkthrough){
                NSLog(@"Unwind to tab discovery");
                [self performSegueWithIdentifier:@"discoveryUnwind" sender:self];
            }
            else if([WSCore fetchLoginType] == LoginTypeMakeBooking ){
                NSLog(@"unwind to make booking");
                [self performSegueWithIdentifier:@"unwindToTabBookUI" sender:self];
            }else if([WSCore fetchLoginType] == LoginTypeMyBookings){
                NSLog(@"unwind to *My Bookings*");
                [self performSegueWithIdentifier:@"unwindToTabMyBookings" sender:self];
            }else if ([WSCore fetchLoginType] == LoginTypeFavourites){
                NSLog(@"Unwind to tab favourites");
               // [self performSegueWithIdentifier:@"unwindToTabFavourites" sender:self];
                [self performSegueWithIdentifier:@"unwindToTabFavouritesAfterLoginOrSignUp" sender:self];

            }
            
        });
        
        
    }else if(manager==self.resendTwilioNetworkManager){
        [UIView showSimpleAlertWithTitle:jsonDictionary[@"message"] message:nil cancelButtonTitle:@"OK"];
        
        
    }
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Failure Json %@",jsonDictionary);
    
    if (manager==self.registerNetworkManager) {
        
        [UIView showSimpleAlertWithTitle:@"Registration Error" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
        
    }else if(manager==self.validateTwilioNetworkManager){
        
        NSString * message;
        if ([jsonDictionary[@"message"] isEqualToString: @"Verification string missing"]){
            message = @"Please Enter the Verfication Number";
        }
        else{
            message = jsonDictionary[@"message"];
        }
        [UIView showSimpleAlertWithTitle:@"Error" message:message cancelButtonTitle:@"OK"];
    }else if(manager==self.validateTwilioNetworkManager){
        [UIView showSimpleAlertWithTitle:@"Error" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
    }
}
//2
-(void)revealVerificationCodeTextArea{
 
    [self.verifyButton setTitle:@"Resend" forState:UIControlStateNormal];
    self.verifyButton.titleLabel.adjustsFontSizeToFitWidth=YES;
    
    self.isTwilioSent=YES;
    
    [UIView transitionWithView:self.view duration:0.4 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        
        
        self.vericationCodeTextField.hidden=NO;
        self.vericationCodeTextField.alpha=1.0;
        self.signUpButton.hidden=NO;
        self.signUpButton.backgroundColor=kWhatSalonBlue;
        self.signUpButton.layer.cornerRadius=3.0;
        self.signUpButton.layer.borderColor=[UIColor clearColor].CGColor;
        [self.signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.signUpButton.enabled=YES;
        self.signUpButton.layer.borderWidth =0.0;
        
    } completion:nil];
    
    
}

//validating Twilio

- (IBAction)signUp:(id)sender {
    
    [self.vericationCodeTextField endEditing:YES];
    
    if (self.vericationCodeTextField.text.length >0) {
        
        if (self.validateTwilioNetworkManager==nil) {
            self.validateTwilioNetworkManager = [[GCNetworkManager alloc] init];
            self.validateTwilioNetworkManager.parentView=self.view;
            self.validateTwilioNetworkManager.delegate=self;
        }
        
        
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kValidate_Twilio_URL]];
        NSString *params = [NSString stringWithFormat:@"phone_number=%@",self.number];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&password=%@",self.passwordTextField.text]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&verification_string=%@",self.vericationCodeTextField.text]];
        
        
        [self.validateTwilioNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
        
    }
    else{
        
        [UIView showSimpleAlertWithTitle:@"Oops" message:@"Verification field cannot be empty." cancelButtonTitle:@"OK"];
        
        
    }
}

#pragma mark - ResendTwilio
- (IBAction)resendTwilio:(id)sender {
    
    [self.view endEditing:YES];
    if (self.resendTwilioNetworkManager==nil) {
        self.resendTwilioNetworkManager = [[GCNetworkManager alloc] init];
        self.resendTwilioNetworkManager.parentView=self.view;
        self.resendTwilioNetworkManager.delegate=self;
    }
    NSURL * urlString = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kResend_Twilio_URL]];
    
    NSString * params = [NSString stringWithFormat:@"phone_number=%@",self.number];
    [self.resendTwilioNetworkManager loadWithURL:urlString withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}

- (IBAction)cancelRegistration:(id)sender {
    self.verifyButton.enabled=YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Hide verfication code
-(void)hideVerifactionCodeArea{
    self.verifyButton.hidden=NO;
    self.isTwilioSent=NO;
    [UIView transitionWithView:self.view duration:0.2 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.smsVerifcationLabel.hidden=YES;
        self.smsVerifcationLabel.alpha=0.0;
        
        self.vericationCodeTextField.hidden=YES;
        self.vericationCodeTextField.alpha=0.0;
        
        self.signUpButton.backgroundColor = kWhatSalonBlue;
        self.signUpButton.layer.cornerRadius = 6.0;
        [self.signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.signUpButton.enabled=YES;
        self.signUpButton.layer.borderWidth =0.0;
        
    } completion:^(BOOL finished) {
        
        [UIView transitionWithView:self.view duration:0.2 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            
            self.signUpButton.frame = CGRectMake(self.signUpButtonOriginalX, self.signUpButtonOriginalY, self.signUpButton.frame.size.width, self.signUpButton.frame.size.height);
            
        } completion:^(BOOL finished) {
            
            
        }];
    }];
}





-(void)addDetailsToDefaults{
    
    [[User getInstance] updateUsersFirstName:self.firstName.text];
    [[User getInstance] updateUsersLastName:self.lastName.text];
    [[User getInstance] updateAccessToken:self.user_id];
    [[User getInstance] updatePhoneNumber:self.user_phone];
    [[User getInstance] updateEmailAddress:self.user_email];
    [[User getInstance] downloadUsersBalance];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
}



@end
