//
//  DiscoveryItem.h
//  whatsalon
//
//  Created by Graham Connolly on 15/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header DiscoveryItem.h
  
 @brief This is the header file for DiscoveryItem
  
  
 @author Graham Connolly
 @copyright  2015 What Application Ltd.
 @version    3.0
 */
#import <Foundation/Foundation.h>
#import "Salon.h"

@interface DiscoveryItem : NSObject

/*! represents the icon image name */
@property (nonatomic) NSString * iconImageName;

/*! represents the title for the discovery item */
@property (nonatomic) NSString * title;

/*! represents the string representation of the url image */
@property (nonatomic) NSString *imageURL;

/*! the subtitle of the discovery item */
@property (nonatomic) NSString * subtitle;

/*! the latitude, if any, of the discovery item */
@property (nonatomic) NSString * lat;

/*! the longitude, if any, of the discovery item */
@property (nonatomic) NSString *lon;

/*! the sort id for sorting the discovery items */
@property (nonatomic) NSString * sort_id;

/*! the discovery item type. */
@property (nonatomic) NSString * type;

/*!
 @brief returns an instance of the DiscoveryItem
 @param type the discovery type
 */
-(instancetype) initWithDiscoveryType: (NSString *) type;

/*!
 @brief returns an instance of the DiscoveryItem
 @param type the discovery type
 */
+(instancetype) discoveryItemWithDiscoveryType: (NSString *) type;
@end
