//
//  AboutMenuViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 22/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "AboutMenuViewController.h"
#import "PolicyDetailViewController.h"
#import "FAQTableViewCell.h"
@interface AboutMenuViewController () <UITableViewDataSource,UITableViewDelegate>

/*! 
 @brief represents the table view.
 */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/*! 
 @brief represents the array of menu options. 
 */
@property (nonatomic,strong) NSArray * menuOptionsArray;

/*! 
 @brief represents the cancel action. 
 */
- (IBAction)cancel:(id)sender;

@end

@implementation AboutMenuViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.menuOptionsArray = [[NSArray alloc] initWithObjects:@"Help",@"Cancellation Policy",@"Where You Can Find Us",@"Terms and Conditions", nil];
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView=footerView;
    
    
    self.title = @"Help";
    [self registerCells];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore flatNavBarOnView:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}

-(void) registerCells {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FAQTableViewCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"FAQTableViewCell"];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.menuOptionsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0&&self.isFromRightSideMenu) {
        return 0;
    }
    return self.tableView.rowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"help" sender:self];
    }
    if (indexPath.row == 1 || indexPath.row==2 || indexPath.row==3) {
        [self performSegueWithIdentifier:@"privacy" sender:self];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"privacy"]) {
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        
        PolicyDetailViewController * policyViewController = (PolicyDetailViewController *)segue.destinationViewController;
        if (indexPath.row == 1) {
            //Cancellation policy
            policyViewController.pageID=kContent_pageID_cancelPolicy;
        }
        else if(indexPath.row ==2){
            policyViewController.pageID=kContent_pageID_WhatCities;
        }else if (indexPath.row == 3){
            
            policyViewController.pageID=@"78";
        }
        
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

      FAQTableViewCell *cell = (FAQTableViewCell *) [self.tableView dequeueReusableCellWithIdentifier:@"FAQTableViewCell"];
    cell.titleLabel.text = [self.menuOptionsArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
