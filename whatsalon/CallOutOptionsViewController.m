//
//  CallOutOptionsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 02/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CallOutOptionsViewController.h"
#import "FUIButton.h"
#import "RESideMenu.h"
#import "CallOutServiceViewController.h"
#import "UILabelPushAnimation.h"
#import "FadePopAnimation.h"

@interface CallOutOptionsViewController ()<UIViewControllerTransitioningDelegate,UINavigationControllerDelegate>
- (IBAction)showMenu:(id)sender;

@property (nonatomic) NSMutableArray * buttonsArray;
@property (nonatomic) FUIButton * makeUpButton;
@property (nonatomic) FUIButton * hairButton;
@property (nonatomic) FUIButton * massageButton;
@property (nonatomic) FUIButton * selectedButton;
@property (nonatomic) CGRect button0;
@property (nonatomic) CGRect button1;
@property (nonatomic) CGRect button2;



@property (nonatomic, strong) NSArray *pageImages;
@property (nonatomic, strong) NSMutableArray *pageViews;

- (void)loadVisiblePages;
- (void)loadPage:(NSInteger)page;
- (void)purgePage:(NSInteger)page;

@property (nonatomic) UIImageView * backgroundImage;
@property (nonatomic) BOOL startAnimation;

@property (nonatomic) NSMutableArray * titles;
@property (nonatomic) NSMutableArray * paragraphs;
@end

@implementation CallOutOptionsViewController



- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC
{
    if (operation == UINavigationControllerOperationPush) {
        return [[UILabelPushAnimation alloc] init];
    } else {
        return [[FadePopAnimation alloc] init];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGFloat spacing = 5;
    CGFloat buttonHeight = 44;
    CGFloat buttonWidth = self.view.frame.size.width;
    
    self.makeUpButton = [[FUIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-buttonHeight-spacing, buttonWidth, buttonHeight)];
    self.makeUpButton.buttonColor=kWhatSalonBlue;
    self.makeUpButton.shadowColor=kWhatSalonBlueShadow;
 
    self.makeUpButton.shadowHeight=kShadowHeight;
    self.makeUpButton.tag=0;
    self.button0=self.makeUpButton.frame;
    [self.makeUpButton setTitle:@"Book Make-Up" forState:UIControlStateNormal];
    [self.makeUpButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.view addSubview:self.makeUpButton];
    
    self.hairButton = [[FUIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-((buttonHeight*2)+ (spacing*2)),buttonWidth, buttonHeight)];
    self.hairButton.buttonColor=kWhatSalonBlue;
    self.hairButton.shadowColor=kWhatSalonBlueShadow;
    self.hairButton.shadowHeight=kShadowHeight;
    self.hairButton.tag=1;
    self.button1 = self.hairButton.frame;
    [self.hairButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.hairButton setTitle:@"Book Hair" forState:UIControlStateNormal];
    [self.view addSubview:self.hairButton];
    
    self.massageButton = [[FUIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-((buttonHeight*3)+ (spacing*3)), buttonWidth, buttonHeight)];
    self.massageButton.buttonColor=kWhatSalonBlue;
    self.massageButton.shadowColor=kWhatSalonBlueShadow;
    self.massageButton.shadowHeight=kShadowHeight;
    self.massageButton.tag=2;
    self.button2=self.massageButton.frame;
    [self.massageButton setTitle:@"Book Massage" forState:UIControlStateNormal];
    [self.massageButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.view addSubview:self.massageButton];
    
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-((buttonHeight*4)+ (spacing*4)), buttonWidth, buttonHeight)];
    [self.view insertSubview:self.pageControl aboveSubview:self.scrollView];
    
    
    [self.makeUpButton addTarget:self action:@selector(buttonSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.hairButton addTarget:self action:@selector(buttonSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.massageButton addTarget:self action:@selector(buttonSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    self.buttonsArray =[[NSMutableArray alloc] initWithObjects:self.makeUpButton,self.massageButton,self.hairButton, nil];
    
    
    self.scrollView.delegate=self;
    self.scrollView.pagingEnabled=YES;

    // 1
    self.pageImages = [NSArray arrayWithObjects:
                       [UIImage imageNamed:@"walkthrough_1a.JPG"],
                       [UIImage imageNamed:@"walkthrough_2.jpg"],
                       [UIImage imageNamed:@"walkthrough_3.jpg"],
                       [UIImage imageNamed:@"walkthrough_4.jpg"],
                       nil];
    
    NSInteger pageCount = self.pageImages.count;
    
    // 2
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    // 3
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.backgroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    [self.view insertSubview: self.backgroundImage belowSubview:self.scrollView];
    
    self.titles = [[NSMutableArray alloc] initWithObjects:@"BROWSE",@"BOOK",@"BE BEAUTIFUL",@"BE BEAUTIFUL", nil];
    self.paragraphs = [[NSMutableArray alloc] initWithObjects:@"Take a virtual tour of all the salons near you. Browse beautiful photos and read reviews of previous customers.",@"Simply select the service and even the stylist you'd like to see. Choose the time that suits you best and book!",@"Go relax and enjoy yourself in one of our amazing salons. You deserve it!",@"BE BEAUTIFUL", nil];
    
    self.backgroundImage.image = [self.pageImages objectAtIndex:0];
    
    self.startAnimation=NO;
    
    UIView * blackView = [[UIView alloc]initWithFrame:self.backgroundImage.frame];
    blackView.backgroundColor=[UIColor colorWithRed:51.0/255.0 green:34.0/255.0 blue:0/255.0f alpha:03];
    blackView.alpha=0.4;
    [self.backgroundImage addSubview:blackView];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore nonTransparentNavigationBarOnView:self];
    // 4
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.pageImages.count, pagesScrollViewSize.height);
    
    // 5
    [self loadVisiblePages];
}

- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // 1
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        // 2
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        // 3
        /*
        UIImageView *newPageView = [[UIImageView alloc] initWithImage:[self.pageImages objectAtIndex:page]];
        newPageView.contentMode = UIViewContentModeScaleAspectFit;
        newPageView.frame = frame;
        [self.scrollView addSubview:newPageView];
        */
        UIView * newPageView = [[UIView alloc] init];
        newPageView.frame = frame;
        newPageView.backgroundColor=[UIColor clearColor];
        
        [self.scrollView addSubview:newPageView];

        UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(16, self.view.frame.size.height-268-self.massageButton.frame.size.height, 288, 24)];
        title.font = [UIFont boldSystemFontOfSize:21.0f];
        title.textColor=[UIColor whiteColor];
        title.text = [self.titles objectAtIndex:page];
        [newPageView addSubview:title];
        
        UILabel * descText = [[UILabel alloc] initWithFrame:CGRectMake(16, self.view.frame.size.height-238-self.massageButton.frame.size.height, 260, 100)];
        //descText.text = @"Take a virtual tour of all the salons near you. Browse beautiful photos and read reviews of previous customers.";
        descText.text = [self.paragraphs objectAtIndex:page];
        descText.font = [UIFont systemFontOfSize:18.0f];
        descText.textColor=[UIColor whiteColor];
        descText.numberOfLines=0;
        [newPageView addSubview:descText];

        // 4
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
    // Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    // Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.pageImages.count; i++) {
        [self purgePage:i];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Load the pages that are now on screen
    [self loadVisiblePages];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"selected Tag %ld and Frame %@",(long)self.selectedButton.tag,NSStringFromCGRect( self.selectedButton.frame));
    
    NSLog(@"isMovingToParentViewController: %d ",self.isMovingToParentViewController);
    
    self.navigationController.delegate=self;
    for (FUIButton * btn in self.buttonsArray) {
        if (btn.tag!=self.selectedButton.tag) {
            btn.alpha=1.0;
        }
    }
    [UIView animateWithDuration:1.0 animations:^{
        self.backgroundImage.alpha=1.0;
        self.scrollView.alpha=1.0;
        
        switch (self.selectedButton.tag) {
            case 0:
                
                self.selectedButton.frame=self.button0;
                break;
            case 1:
                 self.selectedButton.frame=self.button1;
                break;
                
            case 2:
                 self.selectedButton.frame=self.button2;
                break;
                
            default:
                break;
        }
    }];
}
-(void)buttonSelected: (id) sender{
    
    self.selectedButton = (FUIButton *)sender;
    NSLog(@"Selected Button %ld",(long)self.selectedButton.tag);
    for (FUIButton * btn in self.buttonsArray) {
        if(btn.tag !=self.selectedButton.tag){
          
            NSLog(@"tag %ld",(long)btn.tag);
            [UIView animateWithDuration:1.0 animations:^{
                self.backgroundImage.alpha=0.0;
                self.scrollView.alpha=0.0;
               
                btn.alpha=0;
                //self.view.backgroundColor=[UIColor colorWithWhite:0.3 alpha:1];
            }];
        }
        
    }
    
    //self.selectedButton.alpha=0.0;
    [UIView animateWithDuration:0.8 delay:0.6 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        
        self.selectedButton.frame = CGRectMake(self.selectedButton.frame.origin.x, self.navigationController.navigationBar.frame.size.height +[[UIApplication sharedApplication] statusBarFrame].size.height, self.selectedButton.frame.size.width, 44);
        
        
    } completion:^(BOOL finished) {
        
        CallOutServiceViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"calloutServiceVC"];
        cvc.chosenService = self.selectedButton.titleLabel.text;
        cvc.buttonRect =self.selectedButton.frame;
                NSLog(@"chosen service %@",self.selectedButton.titleLabel.text);
        self.navigationController.delegate=self;
        /*
        CATransition *transition = [CATransition animation];
        transition.duration = 0.2;
        transition.type = kCATransitionFade;
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:cvc animated:NO];
         */
        [self.navigationController pushViewController:cvc animated:YES];
        
        }];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)showMenu:(id)sender {
    
    self.navigationController.delegate=nil;
    [self.sideMenuViewController presentMenuViewController];

}


-(void)fadeImage: (NSInteger) pageNo{
    
    NSLog(@"Page No %ld",(long)pageNo);
    NSLog(@"%@",[self.pageImages objectAtIndex:pageNo]);
    UIImage * toImage = [self.pageImages objectAtIndex:pageNo];
    [UIView transitionWithView:self.view
                      duration:0.33f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.backgroundImage.image =toImage;
                    } completion:NULL];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"END");
    
    
    if (self.startAnimation) {
        UIImage * toImage = [self.pageImages objectAtIndex:self.pageControl.currentPage];
        [UIView transitionWithView:self.backgroundImage
                          duration:0.4f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.backgroundImage.image = toImage;
                        } completion:nil];
    }
    
    self.startAnimation=NO;
    
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSLog(@"DId scroll");
    self.startAnimation=YES;
    
    
}

@end
