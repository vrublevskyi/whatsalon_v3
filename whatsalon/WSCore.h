//
//  WSCore.h
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

/*!
 @header WSCore.h
  
 @brief This is the WhatSalon core class containing common resusable methods that are used through out the app
    
 @author Graham Connolly
 @copyright  2014 WhatSalon
 @version    10+
 */


#import <Foundation/Foundation.h>
#import <sys/utsname.h>
#import "Salon.h"
#import "Booking.h"
#import <CoreLocation/CoreLocation.h>



/**
 Core class containing common methods used throughout the app.
 
 How: Call [WSCore methodName]; in desired class
 
 1. Customisation of views (adding blurs, lines to views
 2. Showing/Dismissing Common HUD
 3. Checks Network Reachability
 4. Shows UIAlert views
 5. Common network calls
 6. Date Config
 7. UIImage methods
 
 */

/*!
 @typedef Status bar color
  
 @brief  An enum about the weather.
  
 @discussion The values of this enum represent the different StatusBarColors in iOS.
 
  
 @field StatusBarColorBlack - represents the default black status bar color.
 
 @field StatusBarColorWhite - represents the white status bar color.
 */
typedef enum StatusBarColor : NSUInteger {
    StatusBarColorBlack=0,
    StatusBarColorWhite,
} StatusBarColor;


/*!
 @typedef BookingType
  
 @brief  An enum about the various booking types
  
 @discussion The values of this enum represent the different booking types/flow found within the app. For example, when entering the browse flow of control, the BookingType is set to BookingTypeBrowse. The BookingTypes are useful when performing the unwind segue on the <i>TabCongratulationsViewController</i>. Once the unwind segue is performed the corresponding UIStoryboardSegue resets the booking Type
  
 @field BookingTypeNone - represents no booking
 
 @field BookingTypeBrowse - represents booking flow for browse section
 
 @field BookingTypeFavourite - represents the booking flow for my favourites section
 
 @field BookingTypeMyBookings - represents the booking flow for my bookings section
 
 @field BookingTypeSearch - represents the booking flow for the search section
 
 @field BookingTypeLastMinute - represents the booking flow for the last minute section (not being used)
 */
typedef enum BookingType : NSUInteger {
    BookingTypeNone=0,
    BookingTypeBrowse,
    BookingTypeFavourite,
    BookingTypeMybookings,
    BookingTypeSearch,
    BookingTypeLastMinute,
} BookingType;

/*!
 @typedef LoginType
  
 @brief  An enum representing the different login types/scenarios
  
 @discussion The values of this enum represent the different Login or Sign Up scenarios with the app. For example, LoginTypeSideMenu is set when the login process is taking place via the <i>RightSideMenuController</i>.
  
 
 @field LoginTypeNone - represents the default for LoginType when no login type is set.
 
 @field LoginTypeSideMenu - represents the login type/scenario for the side menu.
 
 @field LoginTypeWalkthrough - represents the login type/scenario from the walkthrough.
 
 @field LoginTypeMakeBooking - represents the login type/scenario during the booking process.
 
 @field LoginTypeMyBookings - represents the login type/scenario for the MyBookings Section of the app.
 
 @field LoginTypeFavourites - repsents the login type/scenario for the Favourites section.
 */
typedef enum LoginType : NSUInteger{
    LoginTypeNone=0,
    LoginTypeSideMenu,
    LoginTypeWalkthrough,
    LoginTypeMakeBooking,
    LoginTypeMyBookings,
    LoginTypeFavourites,
} LoginType;


/*!
 @typedef TabType
  
 @brief  An enum representing the different Tabs within the app
  
 @discussion
 These enum values represent the different tabs found in the application. The numbers represent the numeric ordering of the tabs.
  
 @field TabTypeDefault - represents the default (first) tab
 
 @field TabTypeSearch -  represents the tab for search, or the second tab
 
 @field TabTypeFavourites - represents the tab for favourites section, or the third tab
 
 @field TabTypeMyBooking - represents the tab for my bookings section, or the fourth tab
 
 @remark the menu section of the app represents the fourth tab, but does not have a TabType
 */
typedef enum TabType : NSUInteger{
    TabTypeDefault=0,
    TabTypeSearch=1,
    TabTypeFavourites=2,
    TabTypeMyBookings=3,
} TabType;




@interface WSCore : NSObject

/*! A Booking Type object. */
@property (nonatomic) BookingType bkType;
/*! A CLLocation object representing the guest users location. */
@property (nonatomic) CLLocation * guestUserLocation;



/*!
 
 @brief it converts a timestamp represented as a double to an short NSString repsentation of a date.
 
 @param  timestamp - the timestamp to be converted to an NSString representation of a date.
 
 @return  NSString - string of a short date with the format dd MMM, yyyy .
 
 */
+(NSString *) shortDateStringWithUnixTimestamp: (double) timestamp;

/*!
  @brief It sets the TabType
  @param tt enum value of TabType
 */
+(void)setTabType: (TabType) tt;

/*!
 
 @brief it fetches the current TabType
 
 @return  TabType - The current TabType
 
 */
+(TabType) fetchTabType;

/*!
 
 @brief it resets the TabType to the default TabTypeDefault
 
 */
+(void)resetTabType;

/*!
 
 @brief logs the current TabType
 
 */
+(void)logTabType;


/*!
 
 @brief it sets the BookingType
 
 @param  bk - BookingType enum represnts the current BookingType
 
 */
+(void)setBookingType: (BookingType) bk;

/*!
 
 @brief fetches the current BookingType.
 
 @return  BookingType - returns the current BookingType.
 
 */
+(BookingType) fetchBookingType;

/*!
 
 @brief resets the BookingType to BookingTypeNone.
 
 */
+(void)resetBookingType;

/*!
 
 @brief sets the LoginType.
 @param lt - represents the LoginType enum.
 
 */
+(void)setLoginType: (LoginType) lt;

/*!
 
 @brief fetches the LoginType
 
 @return  LoginType - returns the current LoginType enum
 
 */
+(LoginType) fetchLoginType;

/*!
 
 @brief resets the LoginType to the default LoginTypeNone
 
 
 */
+(void)resetLoginType;

/*!
@brief adds a blurred view to be used under the navigation bar.
 
@discussion a blurred view is created to be used under the navigation bar. A view is taken as a parameter. This view is blurred using UIVisualEffects with style UIBlurEffectStyleLight. A UIVisualEffectView is then created to host the UIVisualEffect. The incoming views background is set to clear. The UIVisualEffectView is inserted as a subview of view and placed below the subview param belowView.
 
 @remark - it used to add a NavigationBar style blur to a view. Gives the appearance of a bigger navigation bar.
 
 @warning for iOS7 the views background color is set to white and its alpha 0.9. UIVisualEffect and UIVisualEffectView are not available in iOS7
 
 
 @param view - in <i>iOS8</i> the view to add the blurred eeffect as a subview to. In <i>iOS7</i> the view whose background is set to White with an alpha of 0.9.
 
 @param belowView - The view that the blurred effect needs to be added under which is used in <i>iOS8</i> only.
 
 @return A customised view.
 */

+ (UIView *)blurredViewForNav:(UIView *) view below: (UIView *) belowView;

/*!
@brief adds a dark effect to a view
 
@discussion A dark effect is added to the view. viewToBlur acts merely as a container view to host the blurred effect. The blurred effect is acieved using UIVisualEffectView and UIVisualEffect. The UIVisualEffectView is added at index 0. viewToBlurs background is set to clear.

@param viewToBlur the view to be customised.
    
@warning in iOS7 the viewToBlur.backgroundColor is changed to black with an alpha of 0.9. Gives a tranparent appearance as UIVisualEffects are not supported in iOS7.

@return A customised view
 
 */

+(UIView *)addDarkBlurToView: (UIView *) viewToBlur;


/*!
 @brief Adds a blur effect behind a view.

 @discussion Adds a blur effect behind a view. Used commonly for UIPickerView. The blurred effect is achieved using the UIVisualEffectView and UIVisualEffect. The UIVisualEffectView is added as a subview of viewForPicker
 
 @param viewForPicker - The UIView that is placed behind the UIPickerView.
 
 @warning in iOS7 the viewForPicker.backgroundColor is changed to white to give it the extra light effect using in the UIVisualEffect. An alpha of 0.9 is also used.
 
 @return A customised view
 */

+(UIView *)addBlurToViewForPicker: (UIView*) viewForPicker;


#pragma mark - UINavigationController custom methods

/*!
 @discussion Makes the UINavigationBar appear if hidden.
 @param navController The UINavigationController that contains the UINavigationBar
 */

+(void) returnNavBar:(UINavigationController *)navController;

#pragma mark - Adding lines to views

/*!
 @brief adds a thin UIView to the half way point of the UIView passed in.
 @discussion this method is ideally used to add a thin line at the half way point of the view passed in.
 
 @param view - the view passed in to have a line added to.
 
 @param color - the UIColor to be applied to the line.
 
 @param h - CGFloat to represent the h of the line.
 */
+(void)addMiddleLineOnView: (UIView *) view WithColor: (UIColor *) color AndHeight: (CGFloat) h;

/*!
@brief adds a line to the bottom of a view.

@discussion Creates a UIView with a height of 0.5 and adds it as a subview of the incoming view.

@param view - the view that will have the bottom line added to it.
 
@param color - the color of the line.
 */

+ (void)addBottomLine: (UIView *) view :(UIColor *) color;

/*!
@brief adds an indented line to the bottom of a view.

@discussion Creates a UIView with a height of 0.5, indents it by 15pts, and adds it to the view.

@param view - the view to be customised.
   
@param color - the color of the line.
 */

+(void)addBottomIndentedLine: (UIView *) view : (UIColor *) color;

/*!
@brief adds a top line to the view.
 
@discussion Creates a UIView with a specified height and adds it as a subview of the incoming view.
 
@param view - the view to be customised.
 
@param color - the color of the line
   
@param lineHeight - the height of the line
 */

+ (void)addTopLine : (UIView *) view : (UIColor *) color : (float) lineHeight;

/*!
 @brief adds a bottom line to the view.
 
 @discussion adds a bottom line to the passed in view, with a custom color and line width height. The bottom line is a UIView with a custom line width height.
 
 @param view - the UIView on which the bottom line will be added.
 
 @param color - the UIColor to be applied to the UIView line.
 
 @param lwh - CGFloat for the height of the line
 */
+ (void)addBottomLine: (UIView *) view WithColor: (UIColor *) color AndWithLineWidthHeight: (CGFloat)lwh;

/*!
@brief adds a line to the right of a view.
 
@discussion Adds a line to the right of a view. Uses the color stored in the constant kCellLinesColour. Creates a UIView with the width of 'size'. Adds the lineView to the view passed in.
 
@param view - The view that will have the line as a subview.
 
@param size - the width of the line.
 
 */

+(void)addRightLineToView: (UIView *) view withWidth: (float)size;


/*!
@brief adds a line to the right of a view with a specifed color.
 
@discussion Adds a line to the right of a view.  Creates a UIView with the width of 'size'. Adds the lineView as a subview of the view parameter.
 
@param view - The view that will have the line as a subview.
 
@param color - The color of the line.
 */

+(void)addRightLineToView: (UIView *) view withWidth: (float)size AndColor : (UIColor *) color;


/*!
@brief Adds a line to the top of a view

@discussion Creates a UIView with a specified color, line height, and y value. Adds this UIView as a subview of the incoming view. Gives the appears of a line.
 
@param view - the specified view to be customised
@param color - the specified color

@param y - the y origin of the created line.
 */

+ (void)addTopLine : (UIView *) view : (UIColor *) color : (float) lineHeight WithYvalue : (float) y;

#pragma mark - Checking for Network methods

/*!
@brief checks for internet connection
   Author Apple
 
@discussion Uses the reachability class supplied by Apple

@return BOOL - whether there is a network present of not
*/

+ (BOOL) isNetworkReachable;

#pragma mark - Network Loading methods

/*!
 
 @brief shows a Progress HUD and makes the network Activity Indicator Visible.
 
 */

+(void)showNetworkLoading;

/*!
 
 @brief dismisses the Progress HUD and dismisses the network Activity Indicator.
 
 */

+(void)dismissNetworkLoading;

/*!
@brief shows a MRProgress HUD on specified view
 
@param view - MRProgress HUD will be placed on this view
 
*/

+(void)showNetworkLoadingOnView: (UIView *) view;

/*!
 
 @brief dismisses a MRProgress HUD on specified view
 
 @param view - the view containing the MRProgress HUD
 */
+(void)dismissNetworkLoadingOnView: (UIView *) view;

/*!
 
 @brief show a MRProgressOverlayView on a view with a custom title
 
 @discussion shows the networkActivityIndicator and adds the MRProgressOverlayView to the view with a custom string
 
 @param view - the view to add the MRProgressOverlayView
 
 @param string - the title to be used in the overlay
 
 */
+(void)showNetworkLoadingOnView :(UIView *) view WithTitle: (NSString *) string;

#pragma mark - Show Alerts

/*!
 
@discussion shows a UIAlertView informing the user that "A network error occurred. Please ensure you are connected to the internet." Also hides the network Activity indicator
 */

+(void)showNetworkErrorAlert;

/*!

 @discussion shows a UILaertView informing the user that "An Error occurred when trying to connect to the server..." Also hides the network Activity indicator.
 */

+(void)showServerErrorAlert;

/*!

 @discussion Shows Alert informing user, "In order to use the favourites feature, please login, or sign up."
 */


+(void)showLoginSignUpAlert;


/*!
 @brief Shows an Alert with a localizedDescription of the error
 
 @discussion Dismisses the network loading HUD. Shows an Alert with a localized description of the error.
 
 @param error - An NSError object
 */

+ (void)errorAlert:(NSError *)error;

#pragma mark - Network Calls

/*!

@discussion Gets the users balance. Accesses the users kAccessTokenKey. If it exists fetcth balance over network. If successful, stores users balance in NSUserDefaults for future usage.
 
@remark kAccessTokenKey is !nil if user has signed in/up
*/

+(void)getUsersBalance DEPRECATED_MSG_ATTRIBUTE("No longer any use for the users balance as we dont have a users balance anymore");
;

/*!
   @brief downloads the list of categories from the Server.
 * @discussion Makes a network call to request the categories offered. This is then used to filter through the Salons. Compares the downloaded list to the already stored list in the NSUserDefaults. If they do not match, then the newer list is saved over it. Handles error checking also
 */

+(void)downloadCategoriesList DEPRECATED_MSG_ATTRIBUTE("There is no longer any need to download all the available catoegories. Was used in earlier version of the app");
;

/*!
 @brief Fetches a list of style categories (hair, eyes, etc.).
 * @discussion A network call to fetch a list of style categories for Inspire. This is later used to request certain inspire objects. Compares the list to the saved list, if new list does not match saved list in NSUserDefaults, the saved is overwritten.
 */

+(void)downloadStyleLists;

/*!
@brief Cancels a Tier 2 service request

@discussion A network call that cancels the current Tier 2 service request. Presents an Alert informing whether the call was successful or not.

Use: Used to cancel a service request (a request to a number of salons to be accomodated for a service)
 
 @warning Tier 2 only

@param s_id The search id assigned to the user
 */

+(void)cancelSearchWithSearchId : (NSString *) s_id DEPRECATED_MSG_ATTRIBUTE("Tier 2 bookings now longer used in app");

/*!
 * @discussion Creates an instance of PhorestCategories and calls the downloadCategories method.
 */


+(void)downloadPhorestCategoriesList DEPRECATED_MSG_ATTRIBUTE("WhatSalon no longer relies on Phorests category list. Instead we use our own.");

/*!
@brief compares App version with Server.

@discussion Makes a call to the server to compare the Apps version number, with the version number on the server. If the server has a new number e.g 1.0.1, then an alert informs the user that an Update is available.
 */

+(void)checkAppVersion;


/*!
@brief Session configuraton for NSURLSessionConfiguration
 
@discussion Creates an instance of NSURLSessionConfiguration. Allows Cellular Access, has a timeoutIntervalForRequest, set to 60.0f. Has a timeoutIntervalForResource, set to 60.0fl

@return sessionConfig -  NSURLSessionConfiguration object
 */

#pragma mark - Configuartions
+(NSURLSessionConfiguration *)sessionConfigWithTimeOut;


/*!
@brief configures the Start Time for an NSDate object. Ensures that the user can only book in 15 mins increments.

 @discussion Ensures that if the time is 15:13 that 15:15 cannot be booked. It would show the user 15:30. This ensures a realistic booking.

 @return A configured NSDate object.
 
*/


+(NSDate *)getConfiguredStartDate;


#pragma mark - BOOLs
/*!
 @discussion Determines if the device is iPhone.
 
 @return BOOL value
 */

+(BOOL)isDeviceIPhone;

/*!
@brief Determines if the string is empty.
 
 @discussion Determines if the string is empty. Compares if to an NSNull object, or if its nil,or if it is not an NSString object, or if its lenght is 0.
 
 @param string - The string to be check
 
 @return BOOL
 */

+(BOOL)stringIsEmpty: (NSString *) string;

#pragma mark - UIImage methods

/*!
@brief Blurs the Users profile image for the background of the Menu
 
@discussion Uses the users accessToken to locate their Profile photo (as NSData) in NSUserDefaults. Creates an image from the NSData. 
 
 @remark If the Device is Lower than iOS8 then a blurred snapshot of the image is taken. An instance of image is returned.
 
@return img An instance of UIImage
 */


+(UIImage *) blurMenuImage;

/*!
@brief Resizes an Image to portrait mode. The max Ratio is 320/480.
 
@discussion Resizes an Image to portrait mode. The max Ratio is 320/480. The actual width and height of the iamge are calculated. The Ratio of the image is then calculated. If the Image Ratio does not equal the maxRatio then adjustments are made. A new UIImage is created.
 
@param img - The UIImage to be resized
 
@return newImage - The newly sized Image
 */

+(UIImage *)resizeImage: (UIImage *) img;

/*!
@discussion Creates a Custom Image in the Appearance of a pin.
 
@param userImage the Users profile iamge.
 
@return newImage the Custom Pin with the users avatar placed within the pin head.
 */


+(UIImage *)drawCustomUserPin:(UIImage*)userImage;

#pragma mark - CGSize
/*!
@discussion Determines the size of the devices window.
 
@return screenSize CGSize
 */

+(CGSize)screenSize;

#pragma mark - Day Suffix
/*!
@discussion Returns the date suffix.

@warning - only for English speaking countries.
 
@return NSString
 */
+ (NSString *)daySuffixForDate:(NSDate *)date;

/*!
 
 @brief removes the background image to give a transparent image
 
 @discussion sets the backgroundimage to a new UIImage. Also sets the navigation bar to translucent. Sets the backgroundColor to clear
 
 @param  cont - the current UIViewController that contains the navigation bar
 
 */

+(void)transparentNavigationBarOnView: (UIViewController *) cont;

/*!
 
 @brief returns the navigation bar to its default appearance
 
 @discussion ensures the navigation bar is present and visible. The tint color of the navigation bar is also set to the constant WhatSalonBlue
 
 @param  cont - the current UIViewController that contains the navigation bar
 
 */
+(void)nonTransparentNavigationBarOnView: (UIViewController *) cont;

/*!
 
 @brief sets the navigation bar to a transparent appearance
 
 @discussion sets the navigation bar backgroundImage to a new image. Sets the navigation bar shadowImage to a new UIImage. Sets the navigation bar to a translucent bar. Sets the backgroundColor of the UINavigationBar to a clear color
 
 @param navBar - the UINavigationBar to customise
 
 */
+(void)transparentNavigationBar: (UINavigationBar *)navBar;

/*!
 
 @brief places a white UIView behind the navigation bar
 
 @discussion sets the navigation bar backgroundImage to a new image. Sets the navigation bar shadowImage to a new UIImage. Sets the navigation bar to a translucent bar. Sets the backgroundColor of the UINavigationBar to a clear color. It creates a white UIView add inserts it behind the navigation bar
 
 @param vc - the UIViewController to have a flat navigation bar apearrance
 
 */
+(void)flatNavBarOnView: (UIViewController *) vc;

/*!
 
 @brief returns the device name
 
 @return NSString - representing the device name
 
 */
+(NSString *) fetchDeviceName;


/*!
 
 @brief gets the current day of the week
 
 @return NSInteger - the current numeric day of the week
 
 */
+(NSInteger)numericDayOfWeek;

/*!
 
 @brief sets the status bar color
 
 @param color - the StatusBarColor
 */
+(void)statusBarColor:(enum StatusBarColor) color;

/*!
 
 @brief shows an alert prompting the user do they want make make a phone call. If a positive response is made then the phone call begins
 
 @param number - the phone number to be called.
 
 @param view - the UIViewController to which the UIAlertController is added (iOS8 only)
 
 @param salonName - NSString representing the salons name
 */
+(void)makePhoneCallWithNumber: (NSString *) number withViewController: (UIViewController *)view AndWithSalonName: (NSString *)salonName;
/*!
 
 @brief sets the app to either a live or test environment
 
 @param yesNo - Bool representing whether the app is to be test or live
 */

+(void)setLiveEnvironment: (BOOL) yesNo DEPRECATED_MSG_ATTRIBUTE("No longer used") ;




//+(void)hideStatusBar: (BOOL) hide;

//+(BOOL)isLiveEnvironment;

//+(NSMutableArray *)getDummyTier1Salons;

/*!
 
 @brief returns an array of Dummy Salon images
 
 @return NSMutableArray - returns an array of UIImage objects
 */
+(NSMutableArray *)getDummySalonGallery DEPRECATED_MSG_ATTRIBUTE("No longer used");

/*!
 
 @brief returns an array of services as strings
 
 @return NSMutableArray - returns an array of strings
 */
+(NSMutableArray *)getDummyServices DEPRECATED_MSG_ATTRIBUTE("No longer used");

/*!
 
 @brief returns an array of stylists as strings
 
 @return NSMutableArray - returns an array of strings
 */
+(NSMutableArray *)getDummyStylist DEPRECATED_MSG_ATTRIBUTE("No longer used");

/*!
 
 @brief returns an array of times as strings
 
 @return NSMutableArray - returns an array of strings
 */
+(NSMutableArray *)getDummyTimes DEPRECATED_MSG_ATTRIBUTE("No longer used");

/*!
 
 @brief returns a BOOL whether the app should be in dummy mode or not
 
 @return BOOL
 
 @remark - no longer used
 */
+(BOOL)isDummyMode ;//DEPRECATED_MSG_ATTRIBUTE("Is no longer supported. It was used at the early stage of development for display purposes.");

/*!
 
 @brief returns a UIImage of the star rating
 
 @param rating - doube representing the rating to be shown on the UIImage returned
 
 @return UIImage of the stars
 */
+(UIImage *)getStarRatingImage: (double) rating;

/*!
 
 @brief encodes a string as a based 64 string
 
 @return string
 */
+(NSString *)base64EncodeWithString: (NSString *)string;

/*!
 
 @brief adds a notification like badge (UIView) to UIView passed in.
 
 @param host - UIView to have the notification badge added to.
 
 @param num - NSString of the numeric value.
 
 @param frame - the size of the notification like badge

 @return UIView
 */
+(UIView *)addNotificationBubbleToView: (UIView *) host WithNumber: (NSString *) num WithFrame: (CGRect) frame;


/*!
 
@brief returns a list of country codes and their initials
 
@return NSDictionary
 
 */
+(NSDictionary *)getCountryCodes;

/*!
 
 @brief adds a notification like badge (UIView) to UIView passed in.
 
 @discussion - uses the @c[self getCountryCodes] and the users NSLocale to return the calling code
 
 @return NSString - returns the users  country code
 
 */
+(NSString *)getUsersCountryCode;


/*!
 
 @brief makes an invisible UINavigationBar that has a line across the bottom.
 
 @param navBar - UINavigationBar to be made invisible
 
 */
+(void)setInvisibleNavBar: (UINavigationBar *)navBar;

/*!
 
 @brief a bool whether the walkthrough should be shown or not
 
 */
+(BOOL)showWalkthrough;

/*!
 
 @brief a bool whether the walkthrough has been shown
 
 */
+(void)walkthroughHasBeenShown;

/*!
 
 @brief returns an NSDictionary of the app supported country codes.
 
 @remark - used for prefixing the users phone number
 */
+(NSDictionary *)currentCountryCodes;


/*!
 
 @brief gets an NSDate based on the parameters sent into the method
 
 @param year - represents the year
 
 @param month - represents the month
 
 @param day - represents the day
 
 @param hr - represents the hour
 
 @param min - represents the minute
 
 @return NSDate
 
 */
+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day WithHour: (NSInteger)hr AndMin: (NSInteger)min;

/*!
 
 @brief determine whether the appointment time is greater than 24 hours
 
 @param apptDate - represents an NSString of the appointment date
 
 @return BOOL
 
 */
+(BOOL)isApptTimeGreaterThan24Hours: (NSString *) apptDate;

/*!
 
 @brief returns an NSDate which is one hour before the appointment time.
 
 @param apptTime - represents an NSDate of the apptTime
 
 @return NSDate
 
 */
+(NSDate *)oneHourBeforeUsersApointmentDate: (NSDate *) apptTime;

/*!
 
 @brief sets a UILocalNotfication to remind the user to review the salon.
 
 @param salon - the salon to review.
 
 @param bookObj - the Booking object.
 
 @oaram date - the date of the appointment.
 
 
 */
+(void)setReviewReminderWithSalon: (Salon *) salon WithBookingObj: (Booking *) bookObj AndWithChosenDate: (NSDate *)date DEPRECATED_MSG_ATTRIBUTE("No longer used, please use: setReviewReminderWithTitle:AndAlertBody:AndWithBookingDateString:AndSalon");

/*!
 
 @brief sets a UILocalNotfication to remind the user to review the salon.
 
 @discussion - the UILocalNotification includes a 'type: 2' which is used in the AppDelegate handleLocationNoficationWithApplication:AndNotification:
 @param title - the title for the UILocationNotification
 
 @param body - the body for the UILocationNotification
 
 @param dateString - string representation of the appointment date.
 
 @param salon - the salon to review.
 
 
 */
+(void)setReviewReminderWithTitle: (NSString *) title AndAlertBody:(NSString *) body AndWithBookingDateString: (NSString *) dateString AndSalon: (Salon *)salon;

//+(void)setLocalNotificationWithTitle: (NSString *)title AndAlertBody: (NSString *) body AndWithBookingDate: (NSDate *)date;
/*!
 
 @brief returns a short date string from the string representation of the date passed in as a parameter
 
 @remark - date format is 'h:mm:ss'
 
 @param date - the date
 
 @return NSString - the string from the date
 
 
 */
+(NSString *) getShortTimeFromDate : (NSString *) date;

/*!
 
 @brief returns a short date string from the string representation of the date passed in as a parameter using the specified format.
 
 @param date - the date.
 
 @param format - the formt to update the date.
 
 @return NSString - the string from the date.
 
 
 */

+(NSString *) getShortTimeFromDate : (NSString *) date WithFormat: (NSString *) format;

/*!
 
 @brief creates a UILocalNotification with a title and body using a datestring
 
 @param title - the title of the notification
 
 @param body - the body of the notification
 
 @param dateString - the date of the notification
 
 @remark - typically used to remind the user of an appointment
 */
+(void)setLocalNotificationWithTitle: (NSString *) title AndAlertBody:(NSString *)body AndWithBookingDateString:(NSString *)dateString;

/*!
 
 @brief prompts the user to register for recieving UILocalNotification services

 */
+(void)registerForUILocalNotifications;

//+(void)setReviewReminderWithTitle: (NSString *) title AndAlertBody:(NSString *) body AndWithBookingDateString: (NSString *) dateString;

/*!
 
 @brief returns an array of search terms (salon names)
 
 */
+(NSMutableArray *)searchSuggestions DEPRECATED_MSG_ATTRIBUTE("not used anymore but consider if having a set of predefined serarch terms");

/*!
 
 @brief returns an array of NSDate objects of the next 7 days
 
 */
+(NSMutableArray *)getDaysOfTheWeek;


/*!
 
 @brief changes the title on the UINavigationBar.
 
 @param navColor - UIColor.
 
 @param vc - UIViewController.
*/
+(void)changeNavigationTitleColor: (UIColor *)navColor OnViewController: (UIViewController *) vc;

/*!
 @brief gets the current hour.
 
 @return NSInteger - the current hour.
 */
+(NSInteger)getCurrentHour;


/*!
 @brief returns a string representation of a date in the form of MMM yyyy.
 
 @param date - NSString representing the date to be used.
 
 @param format - NSString representing the format of the date string being passed in.
 
 @return NSString
 */
+(NSString *)getDeviceMonthAndYearShortDateFromString: (NSString *) date WithFormat: (NSString *)format;


/*!
 @brief takes a screen shot of the what is currently visible on the screen. An alert is displayed once the view has been taken
 
 @param view - the view to be taken
 
 @remark - used in TabCongratulationsViewController (final screen) as a proof of purchase
 
 */
+(void)saveViewAsImageWithView: (UIView *) view;

/*!
 @brief returns an Array of years based on a numeric value passed in as a parameter.
 
 @param noOfYears - the number of numbers to be returned
 
 @return NSMutableArray
 */
+(NSMutableArray *)getYearsArrayWithNumberOfYears: (int) noOfYears;

/*!
 @brief returns an array of the number of months in a year
 
 @return NSMutableArray
 */
+(NSMutableArray *)getMonthsArray;

/*!
 @brief returns the names of the currently supported credit cards.
 
 @return NSArray of NSString objects
 
 */
+(NSArray *)getSupportedCardsArray;

/*!
 @brief returns an NSDate object representation of string.
 
 @param format - the format of the given string to be converted to an NSDate.
 
 @param date - the NSString of a given date
 
 */
+(NSDate *) getDateFromStringWithFormat:(NSString *) format WithDateString: (NSString *) date;


/*!
 @brief gives a pop-into-view animation
 
 @param container - the view to have the animation
 
 */
+(void)popInViewAnimationWithView: (UIView *) container;

/*!
 @brief gives a pop-out-of-view animation
 
 @param container - the view to have the animation
 
 */
+(void)popOutViewAnimationWithView: (UIView *) container;


/*!
 @brief - adds a placeholder view with the text 'We don't have any salons in your area. Please check back soon!'
 
 @param table - the placeholder is added to the table
 
 @remark - Used in BrowseViewController which was version <b>3.0 until 4.0</b> of the app.
 
*/
+(void)addNoSalonsPlaceholderForTableView: (UITableView *)table;

/*!
 @brief removes the no salons placeholder for the tableview
 
 @param table - the table containing the placeholder
 
 @remark - Used in BrowseViewController which was version <b>3.0 until 4.0</b> of the app.
 */
+(void)removeNoSalonsPlaceholderForTableView: (UITableView *)table;

/*!
 @brief removes the background view added to the tableview
 
 @params tv - UITableView containg the background
 
*/
+(void)removeDescriptiveTableViewBackgroundWithTableView: (UITableView *) tv;

/*!
 @brief adds a custom description to a view, along with an image and sub text.
 
 @discussion - this method creates a UIView which contains a placeholder image. Another view is created which contains the title, subtitle and icon. The main view is then added to the background view of the UITableView.
 
 @param tv - UITableView
 
 @param title - NSString
 
 @param sub - NSString
 
 @param icon - UIImage
 
 
 */
+(void)addDescriptiveTableViewBackgroundWithTableView: (UITableView *) tv withTitle: (NSString *)title withSubtitle: (NSString *) sub andWithIcon: (UIImage *) icon;

/*!
 @brief gives UINavigationBar of the UIViewController the default app appearance
 @param controller - UIViewController
 */
+(void)setDefaultNavBar: (UIViewController*)controller;

/*!
 @brief takes a screenshot of the UIView
 
 @param view - the view to be converted to an image
 
 @return UIImage
 
 */
+(UIImage *)takeScreenShotOfView: (UIView *) view;

/*!
 @brief gives the view a floating appearance
 
 @param view - the UIView to be give the appearance
 
 @return UIView
 */
+(UIView *)floatingView: (UIView *)view;

/*!
 @brief gives the view a floating appearance with a custom alpha value.
 
 @param view - the UIView to be give the appearance.
 
 @param alpha - the alpha to be applied.
 
 @return UIView
 */
+(UIView *)floatingView: (UIView *)view WithAlpha:(float) alpha;

/*!
 @brief checks if the string is a vowel, if it is it adds 'an' otherwise it adds an 'a' before the string
 
 @param str - the string to be prefixed
 
 @return NSString
 */
+(NSString *)vowelCheckString: (NSString *) str;



//+ (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated WithTabBarController: (UITabBarController *) tc OnView: (UIView *) view completion:(void (^)(BOOL))completion;

//+ (BOOL)tabBarIsVisible: (UITabBarController *)tc OnView: (UIView *) view ;

//+(UIColor*)colorWithHexString:(NSString*)hex;

//+(void)hideTabAnimated: (BOOL)animated WithTabBarController: (UITabBarController *) tc;


/*!
 @brief fetches the current operating system (iOS) in human readable form.
 
 @return NSString
 */
+(NSString *) fetchOperatingSystem;


/*!
 @brief takes in CLLocation object and updates the users city and country name.
 
 @discussion performs reverse Geocoding on the CLLocation object location and extracst the country name and city name. If the user is logged in and the city and country name length are greater than 0, then @c[User getInstance] updateUsersCityAndCountry:] using a string containg both the city name and the country name. If the user is logged in and the city and country name length is less than 0 the perform @c[User getInstance] updateUsersCityAndCountry:] with an empty string.
 
 @param location - CLLocation
 */
+(void)updateUsersCityAndCountry: (CLLocation *) location;

/*!
 @brief hides the status bar
 
 @param hide - BOOL
 */

+(void)hideStatusBar: (BOOL) hide;

/*!
 @brief returns the source image of the UIImageView parameter.
 
 @param iv UIImageView
 
 @return UIImage
 
 */
+(UIImage *)facebookImageView: (UIImageView *)iv DEPRECATED_MSG_ATTRIBUTE("No longer used in 4.0 - was used on 3.0+");

/*!
 @brief in iOS8 or later UIVisualEffect is applied to the parameter, everything other OS has a black semi transparent view applied to the parameter. A Dark appearance is applied.
 
 @param viewToBlur - the view to have the blur applied
 
 @return UIView - the parameter now customised
 
 */
+(UIView *)addDarkBlurAsSubviewToView: (UIView *)viewToBlur;

/*!
 @brief in iOS8 or later UIVisualEffect is applied to the parameter, everything other OS has a black semi transparent view applied to the parameter. A Light Appearance is applied.
 
 @param viewToBlur - the view to have the blur applied
 
 @return UIView - the parameter now customised
 
 */
+(UIView *)addLightBlurAsSubviewToView: (UIView *)viewToBlur;

/*!
 @brief in iOS8 or later UIVisualEffect is applied to the parameter, everything other OS has a black semi transparent view applied to the parameter. An Extra light appearance is applied.
 
 @param viewToBlur - the view to have the blur applied
 
 @return UIView - the parameter now customised
 
 */
+(UIView *)addExtraLightBlurAsSubviewToView: (UIView *)viewToBlur;



@end
