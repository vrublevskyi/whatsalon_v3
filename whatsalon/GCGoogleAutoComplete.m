//
//  GCGoogleAutoComplete.m
//  GoogleAutoComplete
//
//  Created by Graham Connolly on 04/11/2015.
//  Copyright © 2015 GrahamConnolly. All rights reserved.
//

#import "GCGoogleAutoComplete.h"
#import "GCGoogleAutoCompleteObject.h"
#import "UIView+AlertCompatibility.h"


@implementation GCGoogleAutoComplete

const NSString * kKey = @"AIzaSyDw-zx4KRne1s3GmMBI9eqWe-nulQPiGzg";//Google Places API Key

-(void)getPlacesDetailsWithID:(NSString *)places_id callBack:(void (^)(NSDictionary * jsonDict, NSError *error, BOOL hasFailed))callBack{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@",places_id,kKey]];
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data==nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
            });
            return ;
        }
        NSDictionary * jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
       
        
        dispatch_async(dispatch_get_main_queue(), ^{
           callBack(jsonDict,error,NO); 
        });
        
        
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        [dataTask resume];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [WSCore showNetworkErrorAlert];
        });
    }
    

}
-(void)autoCompleteWithString: (NSString *) text callBack: (void (^)(NSArray * results, NSError *error, BOOL hasFailed, NSString * googleStatusCode))callBack{
    
   
    text =  [text stringByReplacingOccurrencesOfString:@" "  withString:@"+"];
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&key=%@",text,kKey]];
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        BOOL hasFailed = false;
        if (data==nil) {
            NSLog(@"data is nil");
            hasFailed = true;
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
            });
            return;
        }
        
        NSDictionary * jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        NSHTTPURLResponse * httpResonse = (NSHTTPURLResponse *) response;
        
       
        NSString * googleStatusCode;
        
        if ((error !=nil || httpResonse.statusCode != 200 || ![jsonDict[@"status"] isEqualToString:@"OK"]) && hasFailed == false) {
            NSLog(@"An error occured %@", [error localizedDescription]);
            hasFailed = true;
            
            if (jsonDict[@"status"] !=nil) {
                googleStatusCode = jsonDict[@"status"];
            }
            
            
        }
        
        
        NSMutableArray * predictionResult = [NSMutableArray array];
        
        if (jsonDict[@"predictions"] !=nil) {
            NSArray * salonsArray = jsonDict[@"predictions"];
            for (NSDictionary *objects in salonsArray) {
                
                //NSLog(@"Name %@ and PlacesID %@",objects[@"description"],objects[@"place_id"]);
                GCGoogleAutoCompleteObject * result = [[GCGoogleAutoCompleteObject alloc] initWithPrediction:objects];
                
                [predictionResult addObject:result];
            };
        }

        NSArray * result = [NSArray arrayWithArray:predictionResult];
        
        dispatch_async(dispatch_get_main_queue(), ^{
             callBack(result,error, hasFailed,googleStatusCode);
        });
       
       
            
    }];
    if ([WSCore isNetworkReachable]) {
        [dataTask resume];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [WSCore showNetworkErrorAlert];
        });
    }


}

@end
