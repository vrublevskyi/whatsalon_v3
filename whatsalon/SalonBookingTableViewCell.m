//
//  SalonBookingTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 28/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SalonBookingTableViewCell.h"

@implementation SalonBookingTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
