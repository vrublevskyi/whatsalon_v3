//
//  UITabBarViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 24/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarViewController : UITabBarController

@end
