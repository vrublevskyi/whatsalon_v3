//
//  FavInspireViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "FavInspireViewController.h"
#import "RESideMenu.h"
#import "InspireDetailViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "InspireCollectionViewCell.h"
#import "SettingsViewController.h"
#import "InspireImage.h"
#import "UISegmentedControl+setSegments.h"
#import "User.h"
#import "StyleCategoryListModel.h"
#import "StyleObject.h"
#import "FavMessageView.h"

#import "GCStackedGridLayout.h"
#import "UICollectionView+Animations.h"
#import "GCNetworkManager.h"

#define FAV_LOADING_CELL_IDENTIFIER @"LoadingItemCell"

@interface FavInspireViewController () <UISearchBarDelegate,UISearchDisplayDelegate,UIViewControllerTransitioningDelegate,GCNetworkManagerDelegate>




@property (nonatomic) NSInteger selectedPhotoIndex;
@property (nonatomic,strong) UISearchBar * searchbar;

@property (nonatomic) NSString * gender;
@property (nonatomic) NSInteger listIndex;
@property (nonatomic,assign) BOOL isMale;

@property (nonatomic) NSMutableArray * favArray;
@property (nonatomic) int style_id;
@property (nonatomic) UIRefreshControl * refreshControl;
@property (nonatomic) int currentPage;
@property (nonatomic) int totalNumberOfPages;
@property (nonatomic) NSMutableArray * styleList;
@property (nonatomic) NSMutableArray * styleIdsArray;
@property (nonatomic) NSMutableArray * styleNamesArray;
@property (nonatomic) BOOL isSearching;


@property (nonatomic) GCStackedGridLayout * stackedGridLayout;
@property (nonatomic) GCNetworkManager * networkManager;

@end

@implementation FavInspireViewController

#pragma mark - GCStackedGridLayout Delegate

- (NSInteger)collectionView:(UICollectionView*)cv layout:(UICollectionViewLayout*)cvl
   numberOfColumnsInSection:(NSInteger)section {
    return 2;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)cv layout:(UICollectionViewLayout*)cvl
   itemInsetsForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemWithWidth:(CGFloat)width atIndexPath:(NSIndexPath *)indexPath{
    
    
    
    NSArray *mElements = [NSArray arrayWithObjects:[NSValue valueWithCGSize:
                                                    CGSizeMake(600.0, 900.0)],
                          [NSValue valueWithCGSize:CGSizeMake(500.0, 880.0)],
                          [NSValue valueWithCGSize:CGSizeMake(546.0, 1032.0)],
                          [NSValue valueWithCGSize:CGSizeMake(300.0, 700.0)],
                          [NSValue valueWithCGSize:CGSizeMake(300.0, 800.0)],
                          
                          nil];
    CGSize size = size = [[mElements objectAtIndex:indexPath.row % [mElements count]] CGSizeValue];
    
    CGSize picSize =  size.width>0.0f ? size : CGSizeMake(100.0f, 100.0f);
    
    picSize.height += 35.0f; picSize.width += 35.0f;
    CGSize retval = CGSizeMake(width, picSize.height * width / picSize.width);
    
    return retval;
}

#pragma mark - LoginOrSignUp View
- (UIButton *)getInspiredLoginOrSignUp:(UIView *)blockView
{
    
    UIButton * loginSignUpButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    loginSignUpButton.frame =CGRectMake(0, 0, 200, 44);
    [loginSignUpButton setTitle:@"Get Inspired!" forState:UIControlStateNormal];
    [loginSignUpButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    loginSignUpButton.titleLabel.font = [UIFont  systemFontOfSize:16.0];
    [blockView addSubview:loginSignUpButton];
    
    [loginSignUpButton setCenter:CGPointMake(blockView.frame.size.width/2, (blockView.frame.size.height/2)- loginSignUpButton.frame.size.height/2)];
    
    [loginSignUpButton addTarget:self action:@selector(loginSignUpAction) forControlEvents:UIControlEventTouchUpInside];
    loginSignUpButton.layer.cornerRadius=6.0;
    loginSignUpButton.layer.borderColor=[UIColor darkGrayColor].CGColor;
    
    UILabel * textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 220, 45)];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey]==nil) {
            textLabel.text = @"Help us to Inspire you!\n Login or Sign Up to edit your Profile.";
    }
    else if(self.gender ==nil||self.gender.length==0 ||[self.gender isEqualToString:@"N/A"]){
        textLabel.text = @"Help us to Inspire you!\n But first, you must edit your Profile.";
    }

    textLabel.font = [UIFont systemFontOfSize:15.0f];
    textLabel.numberOfLines=3;
    textLabel.adjustsFontSizeToFitWidth=YES;
    textLabel.textColor=[UIColor darkGrayColor];
    textLabel.textAlignment=NSTextAlignmentCenter;
    
    [blockView addSubview:textLabel];
    [textLabel setCenter:CGPointMake(blockView.frame.size.width / 2, (blockView.frame.size.height / 2)+(loginSignUpButton.frame.size.height+20))];
    
    return loginSignUpButton;
}

-(void)loginSignUpAction{
  
    SettingsViewController * settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
    self.tabBarController.tabBar.hidden=YES;
    self.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:settingsViewController animated:YES];
    
}

-(void)backToSearchTab{
    self.tabBarController.selectedIndex=0;
}

#pragma mark - GCNetworkManagerDelegate
- (void)addFavouriteBackgroundView {
    UIView * view = [[UIView alloc] initWithFrame:self.collectionView.frame];
    view.backgroundColor=kGroupedTableGray;
    FavMessageView * favMessage = [[FavMessageView alloc] init];
    favMessage.messageText.text=@"Feeling Inspired? Click the heart to favourite and we'll store them here.";
    favMessage.view.backgroundColor=[UIColor clearColor];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backToSearchTab)];
    tap.numberOfTapsRequired=1;
    [favMessage.plusButton addTarget:self action:@selector(backToSearchTab) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:favMessage];
    favMessage.center=CGPointMake(view.bounds.size.width/2.0, view.bounds.size.height/2.0);
    self.collectionView.backgroundView=view;
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Called");
    self.totalNumberOfPages=[jsonDictionary[@"message"][@"total_pages"] intValue];
    NSArray * inspireObjects = jsonDictionary[@"message"][@"results"];
    for (NSDictionary *iDict in inspireObjects) {
        
        InspireImage *inspire = [InspireImage inspireWithStyle_id:[iDict[@"style_id"] stringValue]];
        inspire.is_favourite =[iDict[@"is_favourite"] boolValue];
        inspire.isBookable =[iDict[@"is_bookable"] boolValue];
        inspire.salon_name = iDict[@"salon"][@"salon_name"];
        inspire.style_gender =iDict[@"style_gender"];
        inspire.image_name = iDict[@"style_image"];
        inspire.style_type_id=iDict[@"style_type_id"];
        inspire.style_type_name=iDict[@"style_type_name"];
        inspire.stylist_name = iDict[@"stylist_name"];
        
        //salon obj
        inspire.salon_id = iDict[@"salon"][@"salon_id"];
        inspire.salon_des=iDict[@"salon"][@"salon_description"];
        inspire.salon_phone=iDict[@"salon"][@"salon_phone"];
        inspire.salon_lat=iDict[@"salon"][@"salon_lat"];
        inspire.salon_lon=iDict[@"salon"][@"salon_lon"];
        inspire.salon_address_1 = iDict[@"salon"][@"salon_address_1"];
        inspire.salon_address_2=iDict[@"salon"][@"salon_address_2"];
        inspire.salon_address_3=iDict[@"salon"][@"salon_address_3"];
        inspire.salon_city_name=iDict[@"salon"][@"city_name"];
        inspire.salon_country_name=iDict[@"salon"][@"country_name"];
        inspire.salon_landline=iDict[@"salon"][@"salon_landline"];
        inspire.salon_image_url=iDict[@"salon"][@"salon_image"];
        inspire.salon_tier=iDict[@"salon"][@"salon_tier"];
        inspire.salon_rating=iDict[@"salon"][@"rating"];
        inspire.salon_review=iDict[@"salon"][@"reviews"];
        
        
        [self.favArray addObject:inspire];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        
        
        if (self.favArray.count==0) {
            if (!self.isSearching) {
               [self addFavouriteBackgroundView];
            }else{
                [self.collectionView reloadData];
            }
            
        }
        else{
            self.collectionView.backgroundView=nil;
            
            if (!self.isSearching) {
                [self.collectionView fadeOutFadeInReload];//added
            }else{
                [self.collectionView reloadData];
            }
            
        }
        //[self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.collectionView.numberOfSections)]];
    });

}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    [WSCore showServerErrorAlert];
    
}
#pragma mark - FetchInspireObjects
-(void)fetchInspireObjects{
 
    NSString * accessToken =[[User getInstance] fetchAccessToken];
    if (accessToken==nil) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Oh oh!" message:@"You must login in or sign up in order to view your favourites." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kInspire_User_URL]];
   
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.f];
    [request setHTTPMethod:@"POST"];
    if (accessToken!=nil) {
        NSString *params =[NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&only_favourited=YES"]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&style_type_id=%d",self.style_id]];
        params= [params stringByAppendingString:[NSString stringWithFormat:@"&page=%d",self.currentPage]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&search_string=%@",self.searchbar.text]];
        //[request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:!self.isSearching];
    }else{
        
        [self.networkManager loadWithURL:url WithHTTPMethod:@"POST" AndWithLoadingDialog:!self.isSearching];
    }
    
    /*
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
     
        if(!error){
            if (data && dataDict!=nil) {
                
                switch (httpResp.statusCode) {
                    case kOKStatusCode:{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                                
                                self.totalNumberOfPages=[dataDict[@"message"][@"total_pages"] intValue];
                                NSArray * inspireObjects = dataDict[@"message"][@"results"];
                                for (NSDictionary *iDict in inspireObjects) {
                                    NSLog(@"IN");
                                    InspireImage *inspire = [InspireImage inspireWithStyle_id:[iDict[@"style_id"] stringValue]];
                                    inspire.is_favourite =[iDict[@"is_favourite"] boolValue];
                                    inspire.isBookable =[iDict[@"is_bookable"] boolValue];
                                    inspire.salon_name = iDict[@"salon"][@"salon_name"];
                                    inspire.style_gender =iDict[@"style_gender"];
                                    inspire.image_name = iDict[@"style_image"];
                                    inspire.style_type_id=iDict[@"style_type_id"];
                                    inspire.style_type_name=iDict[@"style_type_name"];
                                    inspire.stylist_name = iDict[@"stylist_name"];
                                    
                                    //salon obj
                                    inspire.salon_id = iDict[@"salon"][@"salon_id"];
                                    inspire.salon_des=iDict[@"salon"][@"salon_description"];
                                    inspire.salon_phone=iDict[@"salon"][@"salon_phone"];
                                    inspire.salon_lat=iDict[@"salon"][@"salon_lat"];
                                    inspire.salon_lon=iDict[@"salon"][@"salon_lon"];
                                    inspire.salon_address_1 = iDict[@"salon"][@"salon_address_1"];
                                    inspire.salon_address_2=iDict[@"salon"][@"salon_address_2"];
                                    inspire.salon_address_3=iDict[@"salon"][@"salon_address_3"];
                                    inspire.salon_city_name=iDict[@"salon"][@"city_name"];
                                    inspire.salon_country_name=iDict[@"salon"][@"country_name"];
                                    inspire.salon_landline=iDict[@"salon"][@"salon_landline"];
                                    inspire.salon_image_url=iDict[@"salon"][@"salon_image"];
                                    inspire.salon_tier=iDict[@"salon"][@"salon_tier"];
                                    inspire.salon_rating=iDict[@"salon"][@"rating"];
                                    inspire.salon_review=iDict[@"salon"][@"reviews"];
                                    
                                   
                                    [self.favArray addObject:inspire];
                                }
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    //[self.collectionView reloadData];
                                    if (!self.isSearching) {
                                        //[WSCore dismissNetworkLoading];
                                        [WSCore dismissNetworkLoadingOnView:self.view];
                                    }
                                    
                                    if (self.favArray.count==0) {
                                        UIView * view = [[UIView alloc] initWithFrame:self.collectionView.frame];
                                        view.backgroundColor=kGroupedTableGray;
                                        FavMessageView * favMessage = [[FavMessageView alloc] init];
                                        favMessage.messageText.text=@"Feeling Inspired? Click the heart to favourite and we'll store them here.";
                                        favMessage.view.backgroundColor=[UIColor clearColor];
                                        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backToSearchTab)];
                                        tap.numberOfTapsRequired=1;
                                        [favMessage.plusButton addTarget:self action:@selector(backToSearchTab) forControlEvents:UIControlEventTouchUpInside];
                                        
                                        [view addSubview:favMessage];
                                        favMessage.center=CGPointMake(view.bounds.size.width/2.0, view.bounds.size.height/2.0);
                                        self.collectionView.backgroundView=view;
                                    }
                                    else{
                                        self.collectionView.backgroundView=nil;
                                        
                                        [self.collectionView fadeOutFadeInReload];//added
                                    }
                                    //[self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.collectionView.numberOfSections)]];
                                });
                            }
                            else{
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    if (!self.isSearching) {
                                        //[WSCore dismissNetworkLoading];
                                        [WSCore dismissNetworkLoadingOnView:self.view];
                                    }
                                    
                                    if (error) {
                                        //[WSCore dismissNetworkLoading];
                                        [WSCore dismissNetworkLoadingOnView:self.view];
                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@", [error localizedDescription] ]message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                        [alert show];
                                        return ;
                                    }
                                    [WSCore showServerErrorAlert];
                                });
                                
                            }
                        });
                    }
                        break;
                        
                    default:
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (!self.isSearching) {
                                //[WSCore dismissNetworkLoading];
                                [WSCore dismissNetworkLoadingOnView:self.view];
                            }
                            [WSCore showServerErrorAlert];
                        });
                        break;
                }
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [WSCore dismissNetworkLoading];
                    [WSCore dismissNetworkLoadingOnView:self.view];
                    [WSCore showServerErrorAlert];
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //[WSCore dismissNetworkLoading];
                [WSCore dismissNetworkLoadingOnView:self.view];
                [WSCore errorAlert:error];
            });
        }
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        if (!self.isSearching) {
           //[WSCore showNetworkLoading];
            [WSCore showNetworkLoadingOnView:self.view];
        }
       
        [dataTask resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
    
    */
}

#pragma mark - UIViewController LifeCylce methods

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"favNotFav" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"favIsFav" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"scrollToIndexFav" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"favNotFavCell" object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    self.collectionView.alpha=0.0;
    self.view.backgroundColor=kGroupedTableGray;
    self.collectionView.backgroundColor=kGroupedTableGray;
    
    NSString * string = [[NSUserDefaults standardUserDefaults] objectForKey:kGenderKey];
    
    if ([string isEqualToString:kMale]) {
        self.isMale=YES;
        self.style_id=[[StyleCategoryListModel getInstance] fetchMaleStartPage];
    }
    else{
        self.isMale=NO;
        self.style_id=[[StyleCategoryListModel getInstance] fetchFemalStartPage];
    }
    
    
    self.currentPage=1;
    self.totalNumberOfPages=0;
    
    if (self.isMale) {
        self.styleList = [[NSMutableArray alloc] initWithArray:[[StyleCategoryListModel getInstance] fetchMaleStylesList]];
        
    }
    else{
        self.styleList = [[NSMutableArray alloc] initWithArray:[[StyleCategoryListModel getInstance] fetchFemaleStylesList]];
        
    }
    
    self.styleIdsArray = [[NSMutableArray alloc] init];
    self.styleNamesArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<self.styleList.count; i++) {
        StyleObject * style = [self.styleList objectAtIndex:i];
        
        NSString *styleName = style.styleTypeName;
        styleName = [styleName stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[styleName substringToIndex:1] uppercaseString]];
        [self.styleNamesArray addObject:styleName];
        [self.styleIdsArray addObject:style.styleTypeId];
    }
    [self.segmentedControl setSegments:self.styleNamesArray];
    [self.segmentedControl setSelectedSegmentIndex:0];
    
   
    //make navigation bar completely transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];

    [self addBlurToView];
    
    //add search bar
    self.searchbar = [[UISearchBar alloc] init];
    self.navigationItem.titleView = self.searchbar;
    self.searchbar.delegate=self;
    [self.searchbar setSearchFieldBackgroundImage:[UIImage imageNamed:@"searchbarBckground"] forState:UIControlStateNormal];
    self.searchbar.placeholder = @"Inspire me";
    
    
    //uicollection view set up
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.overlayBlockView.hidden=YES;
    
    UITapGestureRecognizer * tapToDismissKeyboard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tapToDismissKeyboard.numberOfTapsRequired=1;
    [self.overlayBlockView addGestureRecognizer:tapToDismissKeyboard];
    
    [self.collectionView registerClass:[InspireCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:FAV_LOADING_CELL_IDENTIFIER];
    
    self.favArray = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateFavRemoveFavourite:)
                                                 name:@"favNotFav"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateFavAsFavourite:)
                                                 name:@"favIsFav"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateFavRemoveFavouriteCell:)
                                                 name:@"favNotFavCell"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scrollToIndexFav:)
                                                 name:@"scrollToIndexFav"
                                               object:nil];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh)
                  forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
    self.isSearching=NO;
    
    [self.searchbar setImage:[UIImage imageNamed:@"Multiply"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
    [self.searchbar setImage:[UIImage imageNamed:@"Multiply"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateHighlighted];
    
    self.stackedGridLayout = [[GCStackedGridLayout alloc] init];
    self.stackedGridLayout.headerHeight=0.0f;
    self.collectionView.collectionViewLayout=self.stackedGridLayout;

}

-(void)resetPage{
    [self.favArray removeAllObjects];
   // [self.collectionView reloadData];
    [self.collectionView fadeOutFadeInReload];
    self.currentPage=1;
    self.totalNumberOfPages=0;
}
-(void)startRefresh{
    
    
    if (self.refreshControl) {
       
        [self.refreshControl endRefreshing];
        [self resetPage];
        [self fetchInspireObjects];
    }
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [self resetPage];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
    
    NSString * accessToken = [[User getInstance] fetchAccessToken];
    self.gender = [[NSUserDefaults standardUserDefaults] objectForKey:kGenderKey];
   
    
    if (accessToken==nil ||self.gender==nil || self.gender.length==0|| [self.gender isEqualToString:@"N/A"]) {
        self.searchbar.userInteractionEnabled=NO;
        self.searchbar.alpha=0.8;
        self.segmentedControl.userInteractionEnabled=NO;
        self.segmentedControl.alpha=0.7;
        
        UIView * blockView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 + self.viewForFavNavBar.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height- self.viewForFavNavBar.frame.size.height)];
        blockView.backgroundColor = kGroupedTableGray;
        [self.view addSubview:blockView];
        
        
        UIButton *loginSignUpButton;
        loginSignUpButton = [self getInspiredLoginOrSignUp:blockView];
        loginSignUpButton.layer.borderWidth=0.5;
        
        return;
        
    }
    else{
        [self resetPage];
        [self fetchInspireObjects];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.isSearching) {
        [self resetSearchBar];
    }
    [self.collectionView setNeedsDisplay];
}
#pragma mark - NSNotification Methods

-(void)scrollToIndexFav:(NSNotification *) notification{
    
    NSString * key = @"index";
    NSDictionary * dictionary = [notification userInfo];
    NSInteger rowIndex = [[dictionary valueForKey:key] integerValue];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
}
-(void)updateFavRemoveFavourite:(NSNotification *)notification {
   
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    
    for (int i =0; i<self.favArray.count; i++) {
        InspireImage * insp = [self.favArray objectAtIndex:i];
        if ([insp.style_id isEqualToString: stringValueToUse]) {
            
            insp.is_favourite=NO;
            [self.favArray replaceObjectAtIndex:i withObject:insp];
            
            //[self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.collectionView.numberOfSections)]];
            [self.collectionView fadeOutFadeInReload];
            return;
        }
        
    }
    
}


-(void)updateFavAsFavourite:(NSNotification *)notification {
    NSLog(@"Add fav fav");
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    for (int i =0; i<self.favArray.count; i++) {
        InspireImage * insp = [self.favArray objectAtIndex:i];
        
        if ([insp.style_id isEqualToString: stringValueToUse]) {
            insp.is_favourite=YES;
            [self.favArray replaceObjectAtIndex:i withObject:insp];
            
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.collectionView.numberOfSections)]];;
            return;
        }
        
        
    }
    
}

-(void)updateFavRemoveFavouriteCell:(NSNotification *)notification {
    
    NSString *key = @"Inspire_id";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    
    for (int i =0; i<self.favArray.count; i++) {
        InspireImage * insp = [self.favArray objectAtIndex:i];
        if ([insp.style_id isEqualToString: stringValueToUse]) {
            
            insp.is_favourite=NO;
            [self.favArray replaceObjectAtIndex:i withObject:insp];
            
            return;
        }
        
    }
    
}




-(void)dismissKeyboard{
   
    [self.searchbar resignFirstResponder];
    self.overlayBlockView.hidden=YES;
    self.segmentedControl.enabled=YES;
}

#pragma mark - Set Up methods
-(void) addBlurToView{
   
    UIView *blurredView = [[UIView alloc] initWithFrame:self.viewForFavNavBar.frame];
    [WSCore blurredViewForNav:blurredView below:self.viewForFavNavBar];

    
}

#pragma mark - UICollectionView datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (self.currentPage<self.totalNumberOfPages &&self.favArray!=0) {
        return self.favArray.count+1;
    }
   
    return self.favArray.count;


}

- (UICollectionViewCell *)loadingCellForIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Show loading");
    UICollectionViewCell *cell = (UICollectionViewCell *)[self.collectionView dequeueReusableCellWithReuseIdentifier:FAV_LOADING_CELL_IDENTIFIER forIndexPath:indexPath];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]
                                                  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.frame = CGRectMake((cell.frame.size.width/2)-20, (cell.frame.size.height/2)-20, 40, 40);
    cell.backgroundColor=[UIColor grayColor];
    cell.layer.cornerRadius=3.0;
    cell.layer.borderColor=[UIColor blackColor].CGColor;
    cell.layer.masksToBounds=YES;
    cell.layer.borderWidth=0.5;
    
    [cell.contentView addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    return cell;
}

- (UICollectionViewCell *)itemCellForIndexPath:(NSIndexPath *)indexPath {
    InspireCollectionViewCell * cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.clipsToBounds = YES;
    
    
   
    InspireImage * inspire=nil;
    inspire =[self.favArray objectAtIndex:indexPath.row];
    cell.infoLabel.text = [NSString stringWithFormat:@"Created by %@ at %@.",inspire.stylist_name,inspire.salon_name];
    [cell setCellImageWithURL:inspire.image_name];
    cell.styleID=inspire.style_id;
    cell.isFromFavScreen=YES;
    cell.isBookable=inspire.isBookable;
    /*
    NSLog(@"Inspire is bookable %d",inspire.isBookable);
    if(inspire.isBookable){
        cell.bgImageView.frame = CGRectMake(0, 0, 140, cell.bounds.size.height - cell.book.bounds.size.height);
        cell.book.hidden = NO;
        cell.infoLabel.frame=CGRectMake(6, cell.bgImageView.bounds.size.height - 38, 129, 38);
    }
    else{
        cell.bgImageView.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
        cell.book.hidden = YES;
        cell.infoLabel.frame=CGRectMake(6, cell.bgImageView.bounds.size.height - 38, 129, 38);
        
    }
    
    [cell layoutIfNeeded];
    */
    
    if (inspire.is_favourite) {
        cell.favImageView.image = [UIImage imageNamed:@"White_Heart_Solid"];
        cell.isFav = YES;
    }
    else{
        
        cell.favImageView.image = [UIImage imageNamed:@"White_Heart_With_Gray_Solid"];
        cell.isFav=NO;
    }
    cell.bgImageView.contentMode=UIViewContentModeScaleAspectFill;
    
    return cell;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell=nil;
    
    if (indexPath.item <self.favArray.count) {
        return [self itemCellForIndexPath:indexPath];
    }
    else{
        //loading
        self.currentPage++;
        [self fetchInspireObjects];
        return [self loadingCellForIndexPath:indexPath];
    }
    return cell;
}




-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    InspireDetailViewController *viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"inspireDetail"];
    NSString *imageName;
   
    viewController.imagesToDisplay=self.favArray;
    self.listIndex = indexPath.row;
    viewController.modalPresentationStyle = UIModalPresentationCustom;
    viewController.transitioningDelegate = self;
    viewController.imageName =imageName;
    viewController.listIndex=self.listIndex;
    viewController.isFromFavPage=YES;
    
    [self presentViewController:viewController animated:YES completion:NULL];

    

}


- (IBAction)changeCollectionData:(id)sender {
    
    
    if (self.segmentedControl.selectedSegmentIndex ==0) {
       
        self.style_id=[[self.styleIdsArray objectAtIndex:0] intValue];
        [self.favArray removeAllObjects];
        //[self.collectionView reloadData];
        [self.collectionView fadeOutFadeInReload];
        [self fetchInspireObjects];
    
    }
    else if (self.segmentedControl.selectedSegmentIndex ==1) {
       
        self.style_id=[[self.styleIdsArray objectAtIndex:1] intValue];
        [self.favArray removeAllObjects];
        //[self.collectionView reloadData];
        [self.collectionView fadeOutFadeInReload];
        [self fetchInspireObjects];
    }
    else if (self.segmentedControl.selectedSegmentIndex ==2) {
       
        self.style_id=[[self.styleIdsArray objectAtIndex:2] intValue];
        [self.favArray removeAllObjects];
        //[self.collectionView reloadData];
        [self.collectionView fadeOutFadeInReload];
        [self fetchInspireObjects];
    }
    else if (self.segmentedControl.selectedSegmentIndex ==3) {
      
        self.style_id=[[self.styleIdsArray objectAtIndex:3] intValue];
        [self.favArray removeAllObjects];
        //[self.collectionView reloadData];
        [self.collectionView fadeOutFadeInReload];
        [self fetchInspireObjects];
    }
    else{
       self.style_id=[[self.styleIdsArray objectAtIndex:0] intValue];
        [self.favArray removeAllObjects];
        //[self.collectionView reloadData];
        [self.collectionView fadeOutFadeInReload];
        [self fetchInspireObjects];
    }

[self.collectionView setContentOffset:CGPointMake(-self.collectionView.contentInset.left, -self.collectionView.contentInset.top) animated:YES];}

- (IBAction)showMenu:(id)sender {
    
    [self.sideMenuViewController presentMenuViewController];
    //[self.sideMenuViewController presentLeftMenuViewController];
}


#pragma mark - UISearchBar datasource and delegate methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    self.searchbar.showsCancelButton=YES;
    self.segmentedControl.enabled=NO;
    //self.searchbar.showsBookmarkButton=YES;
    self.isSearching=YES;
    
}

-(void)resetSearchBar{
    
    [self.searchbar resignFirstResponder];
    self.searchbar.showsBookmarkButton=NO;
    self.segmentedControl.enabled=YES;
    [self.view endEditing:YES];
    self.isSearching=NO;
}
- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar{
    [self resetSearchBar];
    
    if (searchBar.text.length ==0) {
        [self resetPage];
        [self fetchInspireObjects];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.overlayBlockView.hidden=YES;
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    self.searchbar.showsCancelButton=NO;
    [self resetSearchBar];
    
    if (searchBar.text.length ==0) {
        [self resetPage];
        [self fetchInspireObjects];
    }
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.overlayBlockView.hidden=YES;
    self.segmentedControl.enabled=YES;
    self.isSearching=NO;
    
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if ([searchText length]==0) {
        [self.favArray removeAllObjects];
        self.currentPage=1;
        self.totalNumberOfPages=0;
        //[self.collectionView reloadData];
        [self.collectionView fadeOutFadeInReload];
    }
    else{
        if (self.favArray.count!=0) {
            [self.favArray removeAllObjects];
        }
        
        [self fetchInspireObjects];
    }
}


#pragma mark - UIViewControllerTransitioningDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}






@end
