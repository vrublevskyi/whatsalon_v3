//
//  Gradient.swift
//  WhatSalon
//
//  Created by new user on 7/19/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import GradientKit
import HexColors


@objc class Gradients: NSObject {
     override init() {}
    
    @objc class func addBlueToPurpleLayer(view: UIView)  { Gradient.addAppBlueToPurpleGradient(on: view) }
    @objc class func addBlueToPurpleHorizontalLayer(view: UIView)  { Gradient.addAppBlueToPurpleHorizontalGradient(on: view)}
    @objc class func addPinkToPurpleHorizontalLayer(view: UIView)  { Gradient.addPinkToPurpleHorizontalGradient(on: view)}
    @objc class func addPinkToPurpleDiagonalGradient(view: UIView)  { Gradient.addPinkToPurpleDiagonalGradient(on: view)}

    //Buttons
    @objc class func addTodayButtonLayer(view: UIView)  { Gradient.addTodayButtonGradient(on: view)}
    @objc class func addTomorrowButtonLayer(view: UIView)  { Gradient.addTomorrowButtonGradient(on: view)}
    @objc class func addLaterButtonLayer(view: UIView)  { Gradient.addLaterButtonGradient(on: view)}
    @objc class func addBookingUpcommingButtonLayer(view: UIView)  { Gradient.addUpcommingButtonGradient(on: view)}
    @objc class func addPreviousUpcommingButtonLayer(view: UIView)  { Gradient.addPreviousButtonGradient(on: view)}

    
    //Border gradient
    @objc class func addLightBlueToBlueWithPurpleBorder(view: UIView)  { Gradient.addLightBlueToBlueWithPurpleBorder(on: view)}
    @objc class func addPinkToPurpleBorder(view: UIView)
    { Gradient.addPinkToPurpleHorizontalGradient(on: view)}

}

struct Gradient {

    private struct Const {
        struct Colors {
            static let lightBlue = UIColor(red: 119.0/255, green: 194.0/255, blue: 249.0/255, alpha: 1)

            static let blueWithPurple = UIColor(red: 119.0/255, green: 111.0/255, blue: 292.0/255, alpha: 1)
            
            static let walletPink = UIColor(red: 182/255, green: 65/255, blue: 153/255, alpha: 1)
            static let walletPurple = UIColor(red: 135/255, green: 120/255, blue: 192/255, alpha: 1)
            
            //Buttons
            static let todayEndBlue = UIColor(red: 118.0/255, green: 171.0/255, blue: 230.0/255, alpha: 1)
            static let tomorrowEndBlue = UIColor(red: 127.0/255, green: 145.0/255, blue: 249/255, alpha: 1)
            static let bookingEndBlue = UIColor(red: 129.0/255, green: 151.0/255, blue: 249/255, alpha: 1)

        }
    }
    
    static func addAppBlueToPurpleGradient(on view: UIView) {
        let layer = appBlueToPurpleLayer(withDirection: .vertical, frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }

    static func addAppBlueToPurpleHorizontalGradient(on view: UIView) {
        let layer = appBlueToPurpleLayer(withDirection: .horizontal, frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }
    
    static func addAppBlueToBookingEndBlueHorizontalGradient(on view: UIView) {
        let layer = appBlueToPurpleLayer(withDirection: .horizontal, frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }
    
    static func addPinkToPurpleHorizontalGradient(on view: UIView) {
        let layer = appPinkToPurpleLayer(withDirection: .horizontal, frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }
    static func addPinkToPurpleDiagonalGradient(on view: UIView) {
        let layer = appPinkToPurpleLayer(withDirection: .diagonal(true), frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }
    static func addLightBlueToBlueWithPurpleBorder(on view: UIView) {
        let layer = appBlueToPurpleLayer(withDirection: .horizontal, frame: view.bounds)
        view.layer.borderColor = UIColor.gradient(withLayer: layer).cgColor
    }
    
    static private func appBlueToPurpleLayer(withDirection direction: LinearGradientLayer.Direction, frame: CGRect) -> GradientLayer {
        let layer = LinearGradientLayer(direction: direction)
        layer.colors = [Const.Colors.lightBlue, Const.Colors.blueWithPurple]
        layer.frame = frame
        return layer
    }
    static private func appPinkToPurpleLayer(withDirection direction: LinearGradientLayer.Direction, frame: CGRect) -> GradientLayer {
        let layer = LinearGradientLayer(direction: direction)
        layer.colors = [Const.Colors.walletPink, Const.Colors.walletPurple]
        layer.frame = frame
        return layer
    }
    
    //Buttons
    
    static private func buttonTodayLayer(withDirection direction: LinearGradientLayer.Direction, frame: CGRect) -> GradientLayer {
        let layer = LinearGradientLayer(direction: direction)
        layer.colors = [Const.Colors.lightBlue, Const.Colors.todayEndBlue]
        layer.frame = frame
        return layer
    }
    static private func buttonTomorrowLayer(withDirection direction: LinearGradientLayer.Direction, frame: CGRect) -> GradientLayer {
        let layer = LinearGradientLayer(direction: direction)
        layer.colors = [Const.Colors.todayEndBlue, Const.Colors.tomorrowEndBlue]
        layer.frame = frame
        return layer
    }
    static private func buttonLaterLayer(withDirection direction: LinearGradientLayer.Direction, frame: CGRect) -> GradientLayer {
        let layer = LinearGradientLayer(direction: direction)
        layer.colors = [Const.Colors.tomorrowEndBlue, Const.Colors.blueWithPurple]
        layer.frame = frame
        return layer
    }
    
    static private func buttonUpcommingLaterLayer(withDirection direction: LinearGradientLayer.Direction, frame: CGRect) -> GradientLayer {
        let layer = LinearGradientLayer(direction: direction)
        layer.colors = [Const.Colors.lightBlue, Const.Colors.bookingEndBlue]
        layer.frame = frame
        return layer
    }
    static private func buttonPreviousLaterLayer(withDirection direction: LinearGradientLayer.Direction, frame: CGRect) -> GradientLayer {
        let layer = LinearGradientLayer(direction: direction)
        layer.colors = [Const.Colors.bookingEndBlue, Const.Colors.blueWithPurple]
        layer.frame = frame
        return layer
    }
    
    static func addTodayButtonGradient(on view: UIView) {
        let layer = buttonTodayLayer(withDirection: .horizontal, frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }
    static func addTomorrowButtonGradient(on view: UIView) {
        let layer = buttonTomorrowLayer(withDirection: .horizontal, frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }
    static func addLaterButtonGradient(on view: UIView) {
        let layer = buttonLaterLayer(withDirection: .horizontal, frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }
    static func addUpcommingButtonGradient(on view: UIView) {
        let layer = buttonUpcommingLaterLayer(withDirection: .horizontal, frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }
    static func addPreviousButtonGradient(on view: UIView) {
        let layer = buttonPreviousLaterLayer(withDirection: .horizontal, frame: view.bounds)
        view.backgroundColor = UIColor.gradient(withLayer: layer)
    }
}
