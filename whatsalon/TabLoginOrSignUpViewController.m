//
//  TabLoginOrSignUpViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 21/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabLoginOrSignUpViewController.h"
#import "UIView+AlertCompatibility.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "GCFacebookHelper.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface TabLoginOrSignUpViewController ()<GCNetworkManagerDelegate,GCFacebookDelegate>

/*! @brief represents the GCNetworkManager for making network requests. */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief represents the view that pops up when a Facebook Login is taking place. */
@property (nonatomic) UIView * facebookLoginHolder;

/*! @brief represents the GCFacebookHelper for make connections to Facebook. */
@property (nonatomic) GCFacebookHelper * facebookHelper;
@end

@implementation TabLoginOrSignUpViewController

-(void)facebookLoginPlaceholderView{
    
    self.facebookLoginHolder = [[UIView alloc] initWithFrame:self.view.frame];
    self.facebookLoginHolder.backgroundColor = [UIColor blackColor];
    self.facebookLoginHolder.alpha=0.6;
    
    UIActivityIndicatorView * activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activityView startAnimating];
    [self.facebookLoginHolder addSubview:activityView];
    activityView.center = self.facebookLoginHolder.center;
    
    [self.view addSubview:self.facebookLoginHolder];
    
}

-(void)dismissFacebookLoginPlaceholderView{
    [self.facebookLoginHolder removeFromSuperview];
    self.facebookLoginHolder=nil;
    
    if ([self.delegate respondsToSelector:@selector(didCancelLoginSignUpView)]) {
        [self.delegate didCancelLoginSignUpView];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - GCFacebookHelper Delegate

-(void)userDidCancelLogin{
    NSLog(@"User did cancel Login");
    
    [self dismissFacebookLoginPlaceholderView];
}

-(void)userDidLogin{
    NSLog(@"User did login");
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"first_name, last_name, picture.type(large), email, gender, birthday, age_range"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             
             if (!error) {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self postToServerWithFirstName:[result objectForKey:@"first_name"] WithLastName:[result objectForKey:@"last_name"]  WithEmailAddress:[result objectForKey:@"email"]  WithUserGender:[result objectForKey:@"gender"]  WithUserImageURL:result[@"picture"][@"data"][@"url"]  WithAccessToken:[NSString stringWithFormat:@"%@",[FBSDKAccessToken currentAccessToken]] WithSocialID:result[@"id"] AndWithSocialProvider:@"Facebook"];
                     
                        [self dismissFacebookLoginPlaceholderView];

                 });
                 
             }
             else{
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self dismissFacebookLoginPlaceholderView];

                     [UIView showSimpleAlertWithTitle:@"Facebook Error" message:@"We could not log you in, a Facebook error has occurred, please try again later." cancelButtonTitle:@"OK" ];
                 });
             }
         }];
    }
    
    
    
}

-(void)errorOccurredWhenLoggingIn:(NSError *)error{
    
    [self dismissFacebookLoginPlaceholderView];
    NSLog(@"Error occurred when logging in %@",[error localizedFailureReason]);
    [UIView showSimpleAlertWithTitle:@"Facebook Error" message:@"A Facebook login error occured. Please try again later" cancelButtonTitle:@"OK"];
}


-(void)revokeErrorOccured:(NSError *)error{
    
    NSLog(@"A revoke error has occurred");
    [self dismissFacebookLoginPlaceholderView];
}

-(void)permissionsRevoked{
    NSLog(@"Permissions have been revoked");
    [self dismissFacebookLoginPlaceholderView];
}



#pragma mark - UIView lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"LOGIN SIGNUP CONTROLLER");
    self.loginButton.layer.cornerRadius=3.0;
    self.signUp.layer.cornerRadius=3.0;
    [self.loginButton setTitle:@"LOGIN" forState:UIControlStateNormal];
    [self.signUp setTitle:@"SIGN UP" forState:UIControlStateNormal];
    [self.signUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.signUp.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    self.loginButton.titleLabel.font=[UIFont systemFontOfSize:18.0f];
    
    
    self.facebookButton.layer.cornerRadius=3.0f;
    [self.facebookButton setTitle:@"CONTINUE WITH FACEBOOK" forState:UIControlStateNormal];
    self.facebookButton.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    self.signUp.backgroundColor=[UIColor clearColor];
    [self.signUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.loginButton.backgroundColor=[UIColor clearColor];
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.loginButton.layer.borderColor=[UIColor whiteColor].CGColor;
    self.signUp.layer.borderColor=[UIColor whiteColor].CGColor;
    self.loginButton.layer.borderWidth=1.0f;
    self.signUp.layer.borderWidth=1.0f;
    
    UIView * coverView = [[UIView alloc] initWithFrame:self.backgroundImage.frame];
    coverView.backgroundColor=[UIColor blackColor];
    coverView.alpha=0.6;
    
    [self.backgroundImage addSubview:coverView];
    [self.view bringSubviewToFront:self.cancel];
    
   
    
    [self.view bringSubviewToFront:self.facebookButton];
    [self.view bringSubviewToFront:self.signUp];
    [self.view bringSubviewToFront:self.loginButton];
    [self.view bringSubviewToFront:self.logoImage];
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    self.facebookHelper = [[GCFacebookHelper alloc] init];
    self.facebookHelper.delegate=self;
}

//handles failure of GCNetwork
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    
    /*
     If social login failed then revoke Facebook permissions.
     */
    [UIView showSimpleAlertWithTitle:@"Login Failure" message:[NSString stringWithFormat:@"%@ %@",jsonDictionary[@"message"],@"Alternatively, go to 'Sign Up' and create a WhatSalon account."] cancelButtonTitle:@"OK"];
    
    
    [self.facebookHelper revokePermissions];
}

///handles a success message recieved from the server
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    if (manager==self.networkManager) {
        [[User getInstance] setUpFacebookProfile:jsonDictionary];
        
        if ([WSCore fetchLoginType]==LoginTypeMakeBooking) {
            
            [self dismissViewControllerAnimated:YES completion:^{
                if ([self.delegate respondsToSelector:@selector(userDidLoginWithFacebook)]) {
                    [self.delegate userDidLoginWithFacebook];
                }
            }];
        }
        else{
            if ([self.delegate respondsToSelector:@selector(userDidLoginWithFacebook)]) {
                
                
                [self dismissViewControllerAnimated:YES completion:^{
                    [self.delegate userDidLoginWithFacebook];
                }];
                ;
            }

        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)cancel:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(didCancelLoginSignUpView)]) {
        [self.delegate didCancelLoginSignUpView];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)loginWithFacebook:(id)sender {
    
    NSLog(@"Login with Facebook");
    
    [self facebookLoginPlaceholderView];
    [self.facebookHelper loginFacebookUserWithPermissions:@[@"public_profile",@"email"] FromViewController:self];
   
    /*
     The current session, also named active session, is accessed just like this:
     Using the state property of the active session, we determine if there is an open session or not.
     */
    
    /*
    if ([FBSession activeSession].state != FBSessionStateOpen &&
        [FBSession activeSession].state != FBSessionStateOpenTokenExtended) {
        NSLog(@"Open active session");
        //The openActiveSessionWithPersmissions:allowLoginUI public method of the AppDelegate is first called, which invokes in turn the openActiveSessionWithReadPermissions:allowLoginUI:completionHandler of the FBSession class. Once the whole login process is over, we inform the ViewController using a notification, where we'll handle the various session states in a while.
        [self.appDelegate openActiveSessionWithPermissions:@[@"public_profile",@"email"] allowLoginUI:YES];
        
        
    }
    else{
        
    }
     */

}




/**
 postToServerWithFirstName...
 posts Facebook data to server
 */
-(void)postToServerWithFirstName: (NSString *)f_name WithLastName: (NSString *) l_name WithEmailAddress: (NSString *)email WithUserGender: (NSString *)gender WithUserImageURL: (NSString *)imageUrl WithAccessToken: (NSString *)access_Token WithSocialID: (NSString *) socialID AndWithSocialProvider: (NSString *)social{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,@"social_login"]];
    
    NSData * nsdata = [imageUrl dataUsingEncoding:NSUTF8StringEncoding];
    // Get NSString from NSData object in Base64
    NSString *imageBase64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    NSString * params = [NSString stringWithFormat:@"first_name=%@",f_name];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&last_name=%@",l_name]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&email_address=%@",email]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&user_gender=%@",gender]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&image_url=%@",imageBase64Encoded]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&social_access_token=%@",access_Token]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&social_user_identifier=%@",socialID]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&social_login_source=%@",@"FACEBOOK"]];
    
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
    
}


///performs segue to sign up screen
- (IBAction)signUp:(id)sender {
}

///performs segue to login screen
- (IBAction)Login:(id)sender {
}
@end
