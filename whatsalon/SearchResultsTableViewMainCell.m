//
//  SearchResultsTableViewMainCell.m
//  SearchResultsPageExample
//
//  Created by Graham Connolly on 05/05/2014.
//  Copyright (c) 2014 Graham Connolly. All rights reserved.
//

#import "SearchResultsTableViewMainCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation SearchResultsTableViewMainCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.showInfo=NO;
        
        self.hoursLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, 21, 21)];
        self.hoursLabel.text = @"14";
        self.hoursLabel.font =[UIFont systemFontOfSize:16.0];
        self.hoursLabel.adjustsFontSizeToFitWidth = YES;
        
        [self.contentView addSubview:self.hoursLabel];
        
        
        self.minsLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 24, 25, 13)];
        self.minsLabel.text = @"00";
        self.minsLabel.font = [UIFont systemFontOfSize:10.0];
        self.minsLabel.adjustsFontSizeToFitWidth = YES;
        
        [self.contentView addSubview:self.minsLabel];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(57, 20, 190, 16)];
        self.titleLabel.text = @"Preen";
        self.titleLabel.font = [UIFont systemFontOfSize:16.0];
        self.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.titleLabel.textAlignment=NSTextAlignmentLeft;
        
        [self.contentView addSubview:self.titleLabel];
        
        self.addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(57, 37, 223, 16)];
        self.addressLabel.text = @"24 patrick street";
        self.addressLabel.font = [UIFont systemFontOfSize:13.0];
        self.addressLabel.adjustsFontSizeToFitWidth = YES;
        self.addressLabel.textAlignment=NSTextAlignmentLeft;
        
        [self.contentView addSubview:self.addressLabel];
        
        self.reviewsLabel = [[UILabel alloc] initWithFrame:CGRectMake(170, 54, 78, 20)];
        self.reviewsLabel.text = @"11 reviews";
        self.reviewsLabel.textColor=[UIColor whiteColor];
        self.reviewsLabel.font = [UIFont systemFontOfSize:13.0];
        self.reviewsLabel.textAlignment = NSTextAlignmentCenter;
        self.reviewsLabel.adjustsFontSizeToFitWidth = YES;
        self.reviewsLabel.layer.cornerRadius=9.0;
        self.reviewsLabel.clipsToBounds=YES;
        self.reviewsLabel.layer.masksToBounds = YES;
        self.reviewsLabel.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        
        [self.contentView addSubview:self.reviewsLabel];
        
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(56, 110, 109, 40)];
        self.priceLabel.textAlignment = NSTextAlignmentCenter;
        self.priceLabel.text = @"€40.00";
        self.priceLabel.adjustsFontSizeToFitWidth = YES;
        self.priceLabel.font = [UIFont systemFontOfSize:17.0];
        self.priceLabel.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.3];
        self.priceLabel.layer.cornerRadius=10.0;
        self.priceLabel.clipsToBounds=YES;
        self.priceLabel.layer.masksToBounds = YES;
        
        [self.contentView addSubview:self.priceLabel];
        
        self.bookButton = [[UIButton alloc] initWithFrame:CGRectMake(170, 110, 80, 40)];
        [self.bookButton setTitle:@"Book Now" forState:UIControlStateNormal];
        self.bookButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        self.bookButton.backgroundColor = [UIColor blueColor];
        [self.bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.bookButton.layer.cornerRadius=10.0;
        [self.bookButton addTarget:self action:@selector(bookJob) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self.contentView addSubview:self.bookButton];
        
        /*
       self.progressBar = [[TYMProgressBarView alloc] initWithFrame:CGRectMake(57, 82, 190, 18)];
        self.progressBar.barBorderWidth = 0.5f;
        self.progressBar.barBorderColor = [UIColor blackColor];
        self.progressBar.barFillColor = [UIColor blackColor];
        self.progressBar.alpha=0.5;
        self.progressBar.barInnerPadding=0.0;
        self.progressBar.usesRoundedCorners=0;
        self.progressBar.layer.cornerRadius=6.0;
        
        [self.contentView addSubview:self.progressBar];
        *
        self.progressBarSecondsLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 82, 180, 16)];
        self.progressBarSecondsLabel.text = @"180 seconds";
        self.progressBarSecondsLabel.font = [UIFont systemFontOfSize:12];
        self.progressBarSecondsLabel.textColor=[UIColor blackColor];
        
        [self.contentView addSubview:self.progressBarSecondsLabel];
        */
        
        
        self.infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 52, self.contentView.frame.size.width,102)];
        self.infoView.backgroundColor=[UIColor clearColor];
        [self.contentView addSubview:self.infoView];
        self.salonImageV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, self.infoView.frame.size.width/3, self.infoView.frame.size.height-10)];
        self.salonImageV.image=[UIImage imageNamed:@"mata.jpg"];
        self.salonImageV.layer.cornerRadius=4.0;
        self.salonImageV.layer.masksToBounds=YES;
        [self.infoView addSubview:self.salonImageV];
        
        self.textInfo = [[UITextView alloc] initWithFrame:CGRectMake((self.salonImageV.frame.size.width)+5, 5, 150, self.infoView.frame.size.height-15)];
        self.textInfo.font = [UIFont systemFontOfSize: 10.0f];;
        self.textInfo.backgroundColor=[UIColor clearColor];
        [self.infoView addSubview:self.textInfo];
        self.textInfo.textColor=self.textColor;
        self.textInfo.text = @"Tonys hair are an Irish hairdressing chain founded in 1969 by two brothers Peter and Mark Keaveney. The first shop they opened was in Dublin's Grafton Street. They currently have 74 shops in Ireland. The company train and recruit all of their own staff in training schools in Dublin and Belfast. Tony Kavanagh is the creative director who has worked on most major fashion events in Ireland and with some international models.";
        self.infoView.hidden=YES;
        self.backgroundColor = [UIColor clearColor];
        self.backgroundView = nil;
        
        self.starImageView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 54, 100, 20)];
        self.starImageView.image = [UIImage imageNamed:@"fiveStars"];
        
        [self.contentView addSubview:self.starImageView];
        
        self.dotsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width-115, 0, 80, 40)];
        self.dotsImageView.image = [UIImage imageNamed:@"blackdotsSmall"];
        self.dotsImageView.contentMode=UIViewContentModeScaleAspectFit;
        self.dotsImageView.userInteractionEnabled=YES;
        [self.contentView addSubview:self.dotsImageView];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moreInfo)];
        tap.numberOfTapsRequired=1;
        [self.dotsImageView addGestureRecognizer:tap];
        
        self.clipsToBounds=YES;
        
        for (UIView *currentView in self.subviews) {
            if ([currentView isKindOfClass:[UIScrollView class]]) {
                ((UIScrollView *)currentView).delaysContentTouches = NO;
                break;
            }
        }
       
    
        

    }
    return self;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 0)
    {
        if (buttonIndex == 1)
        {
            [self confirmBookJob];
        }
    }
    else{
        if (buttonIndex !=[alertView cancelButtonIndex]) {
            // Post a notification to loginComplete
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete" object:nil userInfo:nil];
        }
    }
    
    
    
}

-(void)bookService{
    
    self.bookButton.highlighted=YES;
   [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete" object:nil userInfo:nil];
}



-(void)moreInfo{
    NSLog(@"Tapped more info");
    
    if (self.showInfo==NO) {
        NSLog(@"Show the info");
        self.showInfo=YES;
        self.infoView.hidden=NO;
        
        self.starImageView.hidden=YES;
        self.progressBar.hidden=YES;
        self.priceLabel.hidden=YES;
        self.bookButton.hidden=YES;
        self.reviewsLabel.hidden=YES;
        self.progressBarSecondsLabel.hidden=YES;
        
    }
    else{
        
        self.infoView.hidden=YES;
        self.showInfo=NO;
        
        self.starImageView.hidden=NO;
        self.progressBar.hidden=NO;
        self.priceLabel.hidden=NO;
        self.bookButton.hidden=NO;
        self.reviewsLabel.hidden=NO;
        self.progressBarSecondsLabel.hidden=NO;
    }
}

-(void)methods{
    NSLog(@"Hello");
    self.progressBarSecondsLabel.text = @"130";
}

-(void)stopMethods{
    NSLog(@"Bye bye");
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)bookJob{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Book: %@?",self.titleLabel.text] message:@"Are you sure you want to book this appointment?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = 0;
    [alert show];
    
}
-(void)confirmBookJob{
    NSMutableDictionary * userInfo = [NSMutableDictionary dictionary];
    [userInfo setValue:[NSString stringWithFormat:@"%@",self.job_confirm_id] forKey:@"confirm_id"];
    [userInfo setValue:[NSString stringWithFormat:@"%@",self.job_online_payment] forKey:@"online_pay"];
    [userInfo setValue:[NSString stringWithFormat:@"%@",self.job_price] forKey:@"full_price" ];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"book_job" object:nil userInfo:userInfo];
}

@end
