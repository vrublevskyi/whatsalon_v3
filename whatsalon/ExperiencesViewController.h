//
//  ExperiencesViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 25/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExperiencesViewController : UIViewController

@property (nonatomic) BOOL showOnlyFavourites;

@end
