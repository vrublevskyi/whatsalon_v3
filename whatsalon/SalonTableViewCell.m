//
//  SalonTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 12/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SalonTableViewCell.h"
#import "CellScrollView.h"

@implementation SalonTableViewCell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        /*
        self.scrollView = [[CellScrollView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, DiscoveryCellHeight-58)];
        [self.contentView insertSubview:self.scrollView atIndex:0];
        self.scrollView.pagingEnabled=YES;
        self.scrollView.scrollEnabled=YES;
        self.scrollView.delegate=self;
        self.scrollView.clipsToBounds=YES;
        */
        self.mainImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, DiscoveryCellHeight-58)];
        //self.mainImage.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.mainImage.image = [UIImage imageNamed:@"ucc.jpg"];
        self.mainImage.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView insertSubview:self.mainImage atIndex:0];;
        
        
        self.infoView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.bounds)-60, self.contentView.bounds.size.width, 60)];
        //remove all contraints for view
        self.infoView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;;
        self.infoView.backgroundColor=[UIColor colorWithWhite:0.9 alpha:1.0];
        [self.contentView insertSubview:self.infoView atIndex:1];
        
        
        self.salonName = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, 304, 24)];
        self.salonName.text = @"Blushington Salon";
        self.salonName.font = [UIFont systemFontOfSize:18];
        self.salonName.textColor = [UIColor darkGrayColor];
        self.salonName.adjustsFontSizeToFitWidth=YES;
        [self.infoView addSubview:self.salonName];
        
        self.salonAddress = [[UILabel alloc] initWithFrame:CGRectMake(8, 31, 242, 21)];
        self.salonAddress.text = @"31 Fake Street, The Wells, Cork";
        self.salonAddress.textColor = [UIColor grayColor];
        self.salonAddress.adjustsFontSizeToFitWidth=YES;
        [self.infoView addSubview:self.salonAddress];
        
        self.salonMiles = [[UILabel alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width - 50, 31, 42, 21)];
        self.salonMiles.text = @"0.3km";
        self.salonMiles.textAlignment=NSTextAlignmentRight;
        self.salonMiles.adjustsFontSizeToFitWidth=YES;
        self.salonMiles.textColor = [UIColor lightGrayColor];
        [self.infoView addSubview:self.salonMiles];
        
        [WSCore addTopLine:self.infoView :kCellLinesColour :0.5];
        
        self.contentView.clipsToBounds=YES;
        self.clipsToBounds = YES;
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.favLabel = [[UIImageView alloc] initWithFrame:CGRectMake(286, 10, 24, 23)];
        self.selectFavLabel = [[UILabel alloc] initWithFrame:CGRectMake(270, 0, 60, 60)];
        self.selectFavLabel.backgroundColor=[UIColor clearColor];
        self.selectFavLabel.userInteractionEnabled=YES;
        self.favLabel.image = [UIImage imageNamed:@"White_Heart_With_Gray_Solid"];
        
        [self insertSubview:self.favLabel atIndex:2];
        [self.contentView addSubview:self.selectFavLabel];
        
        UITapGestureRecognizer * tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
        tapPress.numberOfTapsRequired=1;
        tapPress.delegate=self;
        tapPress.delaysTouchesBegan=YES;
        tapPress.cancelsTouchesInView=YES;
        [self.selectFavLabel addGestureRecognizer:tapPress];

    }
    return self;

}

-(void)addPageImagesAndCreatePageViews: (NSArray *)images{
    
    
    NSInteger pageCount = images.count;
    
    // 2
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    // 3
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }

    
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.scrollView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-58);
    
    [self addPageImagesAndCreatePageViews:self.pageImages];
    [self setScrollSize];
}
-(void)tapPress: (UIGestureRecognizer*) hold{
 
    NSLog(@"Favourite");
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCellImageWithURL: (NSString *)url{
    
}

-(void)setScrollSize{
    NSLog(@"Cell");
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.pageImages.count, pagesScrollViewSize.height);
    
    // 5
    [self loadVisiblePages];
}

- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // 1
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        // 2
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        // 3
        UIImageView *newPageView = [[UIImageView alloc] initWithImage:[self.pageImages objectAtIndex:page]];
        newPageView.contentMode = UIViewContentModeScaleAspectFill;
        newPageView.frame = frame;
        [self.scrollView addSubview:newPageView];
        
        // 4
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
    // Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    // Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.pageImages.count; i++) {
        [self purgePage:i];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Load the pages that are now on screen
    [self loadVisiblePages];
}
@end
