//
//  PartnerAccessViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 29/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "PartnerAccessViewController.h"


@interface PartnerAccessViewController ()<UIWebViewDelegate>

@end

@implementation PartnerAccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    NSURL * url = [NSURL URLWithString:kWhatSalonWebsite];
   
    NSURLRequest * requestURL = [NSURLRequest requestWithURL:url];
    self.webView.delegate = self;
    [self.webView loadRequest:requestURL];
    
    self.title=@"Learn More";
    
   
}


-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    [WSCore showNetworkLoading];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
   
    [WSCore dismissNetworkLoading];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    [WSCore dismissNetworkLoading];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"A connection Error occurred." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [alert show];
}

- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)rq
{
   
    [WSCore showNetworkLoading];
    return YES;
}

- (void)webViewDidFinishLoading:(UIWebView *)wv
{
    [WSCore dismissNetworkLoading];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    self.webView = nil;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [WSCore dismissNetworkLoading];
    self.webView = nil;
}





@end
