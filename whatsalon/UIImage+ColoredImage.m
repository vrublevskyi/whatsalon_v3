//
//  UIImage+ColoredImage.m
//  whatsalon
//
//  Created by Graham Connolly on 27/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "UIImage+ColoredImage.h"

@implementation UIImage (ColoredImage)

+ (UIImage *)coloredImage_resizeableImageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 3.0f, 3.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(1, 1, 1, 1)];
    
    return image;
}
@end
