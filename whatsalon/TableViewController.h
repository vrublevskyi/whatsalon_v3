//
//  TableViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 27/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

@end
