//
//  LastMintuteMapViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 09/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "LastMintuteMapViewController.h"
#import "INTULocationManager.h"
#import "UIView+AlertCompatibility.h"
#import "SalonAnnotation.h"
#import "SalonAnnotationView.h"
#import "UserAnnotationView.h"
#import "User.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UILabel+Boldify.h"

@interface LastMintuteMapViewController ()<MKMapViewDelegate>

@end

@implementation LastMintuteMapViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [WSCore addDarkBlurToView:self.view];
    
    self.salonName.text=[NSString stringWithFormat:@"%@ \n %@",self.lastMin.lastMinuteSalon.salon_name,self.lastMin.lastMinuteSalon.fetchFullAddress];
    if (self.lastMin.lastMinuteSalon.fetchFullAddress.length>40) {
        self.salonName.font=[UIFont systemFontOfSize:14.0f];
        self.salonName.adjustsFontSizeToFitWidth=YES;
    }
    
    [self.salonName boldSubstring:self.lastMin.lastMinuteSalon.salon_name];
    self.salonName.numberOfLines=0;
    self.salonName.textAlignment=NSTextAlignmentCenter;
    self.mapView.delegate=self;
    self.mapView.showsUserLocation=YES;
    self.mapView.showsPointsOfInterest=YES;

    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess) {
                                                 // Request succeeded, meaning achievedAccuracy is at least the requested accuracy, and
                                                 // currentLocation contains the device's current location.
                                                 
                                                 self.mapView.showsUserLocation=YES;
                                                 
                                                 
                                             }
                                             else if (status == INTULocationStatusTimedOut) {
                                                 // Wasn't able to locate the user with the requested accuracy within the timeout interval.
                                                 // However, currentLocation contains the best location available (if any) as of right now,
                                                 // and achievedAccuracy has info on the accuracy/recency of the location in currentLocation.
                                                 
                                             }
                                             else {
                                                 // An error occurred, more info is available by looking at the specific status returned.
                                                 
                                                 switch (status) {
                                                     case INTULocationStatusServicesDenied:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"You have denied WhatSalon access to location services. In order to use this app, please accept access. \n Searching Salons will default to Cork City, Ireland" cancelButtonTitle:@"OK"];
                                                         break;
                                                     case INTULocationStatusServicesRestricted:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location services are restricted for WhatSalon. e.g (parental controls corporate policy, etc). \n Searching Salons will default to Cork City, Ireland" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                     case INTULocationStatusServicesDisabled:
                                                         
                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"Location Services are turned off on your device. For the best experience please turn them on in the Settings app. \n Searching Salons will default to Cork City, Ireland" cancelButtonTitle:@"OK"];
                                                         break;
                                                         
                                                     case INTULocationStatusError:
                                                         
//                                                         [UIView showSimpleAlertWithTitle:@"Location Error" message:@"An error occurred while using the system location services." cancelButtonTitle:@"OK"];
                                                         break;
                                                     default:
                                                         break;
                                                 }
                                                 
                                             }
                                             
                                             
                                         }];
    
    
    SalonAnnotation * sAnnot = [[SalonAnnotation alloc] init];
    [sAnnot setUpAnnotationWithSalon:self.lastMin.lastMinuteSalon];
    
    [self.mapView addAnnotation:sAnnot];
    
    MKCoordinateRegion mapRegion;
    mapRegion.center = sAnnot.coordinate;
    mapRegion.span.latitudeDelta = 0.008;
    mapRegion.span.longitudeDelta = 0.008;
    
    [self.mapView setCenterCoordinate:sAnnot.coordinate];
    [self.mapView setRegion:mapRegion animated: NO];

    
 
}

#pragma mark - MKMapView delegate methods

- (MKAnnotationView *)mapView:(MKMapView *)mv viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    
    NSString *annotationIdentifier = @"PinViewAnnotation";
    
    if (annotation == mv.userLocation){
        
        UserAnnotationView * customPinView = [[UserAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
        
        UIImageView * imageV = [[UIImageView alloc] init];
        
        if ([[User getInstance] isUserLoggedIn]) {
            
            
            [imageV sd_setImageWithURL:[[User getInstance] fetchImageURL ] placeholderImage:[UIImage imageNamed:kProfile_image] ];
        }else{
            
            imageV.image = [UIImage imageNamed:kProfile_image];
        }
        imageV.frame = CGRectMake(9, 7, 38, 38);
        
        imageV.layer.cornerRadius = roundf(imageV.frame.size.width/2.0);
        imageV.layer.masksToBounds = YES;
        imageV.contentMode=UIViewContentModeScaleAspectFill;
        [customPinView addSubview:imageV];
        
        return customPinView;
    }
    
    UIImageView *houseIconView = [[UIImageView alloc] init];
    
    SalonAnnotationView *pinView = [[SalonAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
    
    pinView.canShowCallout=YES;
    
    [houseIconView setFrame:CGRectMake(0, 0, 40, 40)];
    houseIconView.layer.cornerRadius=3.0;
    houseIconView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    houseIconView.layer.borderWidth=0.5;
    houseIconView.layer.masksToBounds=YES;
    houseIconView.contentMode=UIViewContentModeScaleAspectFill;
    
    pinView.leftCalloutAccessoryView = houseIconView;
    pinView.leftCalloutAccessoryView.backgroundColor=[UIColor clearColor];
    
    
    
    SalonAnnotation * salonAnnot = (SalonAnnotation *)annotation;
    
    [houseIconView sd_setImageWithURL:[NSURL URLWithString:salonAnnot.salon.salon_image]
                     placeholderImage:[UIImage imageNamed:@"empty_cell"]];
  
    pinView.tag=[salonAnnot.salon.salon_id intValue];
    
    if (!salonAnnot.salon.open) {
        
        pinView.salonImageType =kTier1SalonIcon;
    }
    else{
        
        int tierType = salonAnnot.salon.salon_tier;
        
        switch (tierType) {
            case 1:
                pinView.salonImageType =kTier1SalonIcon;
                break;
            case 2:
                pinView.salonImageType =kTier2SalonIcon;
                break;
            case 3:
                pinView.salonImageType =kTier3SalonIcon;
                break;
            default:
                
                pinView.salonImageType =kTier1SalonIcon;
                break;
        }
        
    }
    
    pinView.annotation=annotation;
    
    return pinView;
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
  //trying to free up memory
        self.mapView.showsUserLocation = NO;
        self.mapView.delegate = nil;
        [self.mapView removeFromSuperview];
        self.mapView = nil;
        
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cancelButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
