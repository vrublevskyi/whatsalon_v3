//
//  NoInspireMessageView.h
//  whatsalon
//
//  Created by Graham Connolly on 22/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoInspireMessageView : UIView

/*! @brief represents the view for holding the content. */
@property (strong, nonatomic) IBOutlet UIView *view;

/*! @brief represents the title label. */
@property (weak, nonatomic) IBOutlet UILabel *inspireTitle;

/*! @brief represents the inspire text label. */
@property (weak, nonatomic) IBOutlet UILabel *inspireText;

/*! @brief represents the inspire image view. */
@property (weak, nonatomic) IBOutlet UIImageView *inspireImage;

@end
