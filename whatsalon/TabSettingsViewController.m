//
//  LoggedInSettingsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 20/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabSettingsViewController.h"
#import "FUIButton.h"
#import "EditProfileViewController.h"
#import  "FeedbackViewController.h"
#import "User.h"
#import "TwilioVerifiyViewController.h"
#import "ChangePasswordViewController.h"
#import "TutorialViewController.h"
#import "FAQTableViewCell.h"

@interface TabSettingsViewController ()<UITableViewDataSource,UITableViewDelegate,TwilioDelegate>

/*! @brief represents the array of settings options. */
@property (nonatomic) NSMutableArray * optionsArray;

/*! @brief determines whether to update the gear image on the right side menu. */
@property (nonatomic) BOOL shouldUpdateGearImageOnMenu;
@end

@implementation TabSettingsViewController

///ensures the statusbar color
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
   }

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore flatNavBarOnView:self];

    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    CGRect tableFrame = self.tableView.frame;
    tableFrame.origin.y=5;
    tableFrame.size.height=tableFrame.size.height-5;
    self.tableView.frame=tableFrame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([[User getInstance] isUserLoggedIn]) {
        self.optionsArray = [NSMutableArray arrayWithObjects:@"Account Verified",@"Change Password",@"Send Feedback",@"WhatSalon.com",@"Version",@"Tutorial", @"Log Out", nil];
    }
    else{
        self.optionsArray = [NSMutableArray arrayWithObjects:@"WhatSalon.com",@"Version",@"Tutorial", nil];
    }

    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    
    self.tableView.backgroundColor=[UIColor clearColor];
    self.tableView.backgroundView=nil;
    
    [WSCore addBottomLine:self.tableView :kCellLinesColour];
    
    self.title=@"Settings";
    [self registerCells];
}

-(void) registerCells {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FAQTableViewCell" bundle:NSBundle.mainBundle] forCellReuseIdentifier:@"FAQTableViewCell"];
}

-(void)logOut{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Log out" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
    alert.tag = 101;
    
}

///manages the alertView button click
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==101) {
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            if ([self.delegate respondsToSelector:@selector(didLogOut)]) {
                NSLog(@"Perform Log Out");
                [[User getInstance] logout];
                
                if ([self.delegate respondsToSelector:@selector(didLogOut)]) {
                    NSLog(@"User access Token %@",[[User getInstance] fetchAccessToken]);
                    
                    [self.delegate didLogOut];
                }
                
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }
}

/**
    twilioCellSetUp - manages the appearance of the twilio cell
    if the isTwilioVerified then add the checkmark_icon to the cell
    else
    add the delete_icon that represents an 'X'
 */
- (void)twilioCellSetUp:(FAQTableViewCell *)cell {
    if ([[User getInstance] isTwilioVerified]) {
        
        cell.arrowImageView.image =  [UIImage imageNamed:@"greenCheckMark"];
//         cell.arrowImageView.tintColor = [UIColor emerlandColor];
//        cell.arrowImageView.image = [cell.arrowImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//        cell.arrowImageView.layer.cornerRadius = cell.arrowImageView.frame.size.width /3.2;
//        cell.arrowImageView.layer.borderWidth = 2;
//        cell.arrowImageView.layer.borderColor = [UIColor emerlandColor].CGColor;
    }
    else{
        
        cell.arrowImageView.image =  [UIImage imageNamed:@"delete_icon"];
         cell.arrowImageView.tintColor = [UIColor pumpkinColor];
        cell.arrowImageView.image = [cell.arrowImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
//        UIImageView * xImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
//        xImage.image = [UIImage imageNamed:@"delete_icon"];
//        xImage.image = [xImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//        xImage.tintColor = [UIColor pumpkinColor];
//        cell.accessoryView= xImage;
    }
}

///handles cell presentation
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FAQTableViewCell *cell = (FAQTableViewCell *) [self.tableView dequeueReusableCellWithIdentifier:@"FAQTableViewCell"];

    cell.titleLabel.text = [self.optionsArray objectAtIndex:indexPath.row];
    
    switch (indexPath.row) {
            
            //verify account and WhatSalon.com
        case 0:
            
            //twilio
            if ([[User getInstance] isUserLoggedIn]) {
                [self twilioCellSetUp:cell];
            }
            //WhatSalon.com
            else{
                
            }
            
            break;
            
        //Change password and Currency
        case 1:
            //change password
            
            if ([[User getInstance] isUserLoggedIn]) {
     
            }
            else{
               
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",kVersionNumber];
                cell.userInteractionEnabled=NO;

            }

            
            break;
        //send feedback and  version
        case 2:
            //Send feedback
            if ([[User getInstance] isUserLoggedIn]) {
               
            }
            else{
            }
            break;
            
        //whatsalon.com and nothing
        case 3:
            //WhatSalon.com
            if ([[User getInstance] isUserLoggedIn]) {
               // cell.textLabel.text = [self.optionsArray objectAtIndex:indexPath.row];
            }
            
           
            break;
        //Currency
        case 4:
           
            if ([[User getInstance] fetchAccessToken]) {
                cell.rightInfoLabel.text = [NSString stringWithFormat:@"%@",kVersionNumber];
                cell.userInteractionEnabled=NO;
                cell.arrowImageView.hidden = YES;
                
            }

            
            break;
        //version
        case 5:
            //version
            if ([[User getInstance] fetchAccessToken]) {
             
            }
            
        default:
            break;
    }
    
    if(indexPath.row==self.optionsArray.count-1 && [[User getInstance] isUserLoggedIn]){
        [WSCore addBottomLine:cell :kCellLinesColour];
        cell.titleLabel.textColor = kTwitterBlue;
        cell.arrowImageView.image = [UIImage imageNamed:@"logOutIcon"];
        
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
    
    return 50.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
        
        switch (indexPath.row) {
            case 0:{
               //verify account and whatsalon.com
                if ([[User getInstance] isUserLoggedIn]) {
                    ///if twilio is verified perform 'phoneNumberSegue'
                    if ([[User getInstance] isTwilioVerified]) {
                        [self performSegueWithIdentifier:@"phoneNumberSegue" sender:self];
                    }
                    
                    ///else present verify by twilio
                    else{
                        TwilioVerifiyViewController * twilio =[self.storyboard instantiateViewControllerWithIdentifier:@"verifyVC"];
                        twilio.delegate=self;
                        [self presentViewController:twilio animated:YES completion:nil];
                    }

                }
                else{
                    
                    
                    [self performSegueWithIdentifier:@"whatSalonVC" sender:self];
                }
                
            }
                break;
                
            ///go to Feedback screen
            //change password and currency
            case 1:
             
            
            {
                ChangePasswordViewController * changePasswordVC = [self.storyboard instantiateViewControllerWithIdentifier:@"changePasswordVC"];
                [self.navigationController pushViewController:changePasswordVC animated:YES];
                
                
            }
                
                break;
                
                //send feedback and versuin
                
            
            case 2:{
                
                if ([[User getInstance] isUserLoggedIn]) {
                    FeedbackViewController *feedback =[self.storyboard instantiateViewControllerWithIdentifier:@"feedback"];
                    [self.navigationController pushViewController:feedback animated:YES];
                }
                else{
                    TutorialViewController * tutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialVC"];
                    tutVC.isFromSettings=YES;
                    [self presentViewController:tutVC animated:YES completion:^{
                        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
                    }];
                }
                
            }
                break;
                //website
            case 3:
                
                if ([[User getInstance] isUserLoggedIn]) {
                [self performSegueWithIdentifier:@"whatSalonVC" sender:self];
            }
                
                break;
                
            case 5:
            {
                TutorialViewController * tutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialVC"];
                tutVC.isFromSettings=YES;
                [self presentViewController:tutVC animated:YES completion:^{
                    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
                }];
            }

                break;
                
            default:
                break;
        }
    if(indexPath.row==self.optionsArray.count-1 && [[User getInstance] isUserLoggedIn]){
        [self logOut];
        
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.optionsArray.count;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Twilio Delegate method
///twilioWasVerified - reloads the tableView
-(void)twilioWasVerified{
    self.shouldUpdateGearImageOnMenu=YES;
    [self.tableView reloadData];
}

///dismiss the view
- (IBAction)cancel:(id)sender {
    
    if (self.shouldUpdateGearImageOnMenu) {
        if ([self.delegate respondsToSelector:@selector(updateGearColor)]) {
            [self.delegate updateGearColor];
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
