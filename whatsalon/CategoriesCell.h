//
//  CategoriesCell.h
//  whatsalon
//
//  Created by admin on 9/25/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CategoriesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *categoryLogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;

@property (nonatomic) NSNumber* catID;

-(void)categoryLogo:(NSNumber *) Id;

@end

