//
//  TabLoginPageViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 21/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabLoginPageViewController.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "UIView+AlertCompatibility.h"

@interface TabLoginPageViewController ()<GCNetworkManagerDelegate>

@property (nonatomic) GCNetworkManager * loginNetworkManager;

@end

@implementation TabLoginPageViewController

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (IS_iPHONE4) {
        self.userAvatar.frame = CGRectMake(self.userAvatar.frame.origin.x+10, self.userAvatar.frame.origin.y, 83, 83);
        self.userAvatar.image = [UIImage imageNamed:@"grayCameraAvatar.png"];
        self.userAvatar.contentMode=UIViewContentModeScaleToFill;
        self.userAvatar.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.userAvatar.layer.cornerRadius=self.userAvatar.frame.size.width/2;
        self.userAvatar.layer.borderWidth=1.0;
        self.userAvatar.layer.masksToBounds=YES;
        
    }
    
}
#pragma mark - Set up methods

-(void)navigationBarSetUp{
    //make navigation bar completely transparent
    self.viewForNavigationBar.backgroundColor=[UIColor clearColor];
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
    
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
    
    self.navigationBar.tintColor=[UIColor whiteColor];
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    
    [self.CancelBarButton setImage:[UIImage imageNamed:@"icon_x_cancel"]];
    self.CancelBarButton.title=@"";
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [WSCore addDarkBlurAsSubviewToView:self.backgroundImage];
    [self navigationBarSetUp];
    self.title = @"Welcome back";
    
    [WSCore addTopLine:self.emailView :kCellLinesColour :0.5f];
    [WSCore addBottomIndentedLine:self.emailView :kCellLinesColour];
    [WSCore addBottomLine:self.passwordView :kCellLinesColour];
    
    self.loginButton.backgroundColor=kWhatSalonBlue;
    self.loginButton.layer.cornerRadius=3.0f;
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.emailAddressTextField.keyboardType=UIKeyboardTypeEmailAddress;
    self.emailAddressTextField.autocapitalizationType=UITextAutocapitalizationTypeNone;
    self.emailAddressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.passwordTextField.secureTextEntry=YES;
    
    self.userAvatar.clipsToBounds=YES;
    self.userAvatar.layer.cornerRadius = self.userAvatar.frame.size.width/2.0;
    self.userAvatar.hidden=YES;
    self.loginNetworkManager = [[GCNetworkManager alloc] init];
    self.loginNetworkManager.delegate=self;
    self.loginNetworkManager.parentView=self.view;

}


#pragma mark - UITapGestureMethod

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}



#pragma mark - IBActions
- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)login:(id)sender {
    
    //log in
    [self logInConnection];
    
    
}

#pragma mark - Logging in stuff
-(void)logInConnection{
    NSLog(@"Login IN");
    
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kLogin_URL]];
    NSString * params = [NSString stringWithFormat:@"password=%@",self.passwordTextField.text];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&email=%@",self.emailAddressTextField.text]];
    NSLog(@"params %@",params);
    [self.loginNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
    
    
}
- (IBAction)forgotPassword:(id)sender {
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GCNetworkManager

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    [UIView showSimpleAlertWithTitle:@"Oh Oh" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"%@",jsonDictionary);
        [[User getInstance] setUpProfile:jsonDictionary];
        
        [[User getInstance] updateCreditCardDetailsWithFirstName:jsonDictionary[@"message"][@"card_details"][@"card_first_name"] withLastName:jsonDictionary[@"message"][@"card_details"][@"card_last_name"] withCardNumber:jsonDictionary[@"message"][@"card_details"][@"card_number"] withCardType:jsonDictionary[@"message"][@"card_details"][@"card_type"] withExp:jsonDictionary[@"message"][@"card_details"][@"expiry"] withPaymentRef:jsonDictionary[@"message"][@"card_details"][@"payment_ref"]];
        
        if ([WSCore fetchLoginType] ==LoginTypeSideMenu) {
            NSLog(@"Unwind to right menu");
            [self performSegueWithIdentifier:@"unwindToRightMenu" sender:self];
  
        }else if([WSCore fetchLoginType] ==LoginTypeWalkthrough){
            NSLog(@"Unwind to discovery");
            [self performSegueWithIdentifier:@"discoveryUnwind" sender:self];
        }else if([WSCore fetchLoginType] == LoginTypeMakeBooking){
            NSLog(@"Unwind to make booking");
            [self performSegueWithIdentifier:@"unwindToMakeBooking" sender:self];
        }else if([WSCore fetchLoginType] == LoginTypeMyBookings){
            
            NSLog(@"Unwind to *MY* bookings");
            [self performSegueWithIdentifier:@"unwindToTabMyBookings" sender:self];
        }
        else if([WSCore fetchLoginType] == LoginTypeFavourites){
            NSLog(@"Unwind to favourites");
           // [self performSegueWithIdentifier:@"unwindToTabFavourites" sender:self];
            [self performSegueWithIdentifier:@"unwindToTabFavouritesAfterLoginOrSignUp" sender:self];
        }
            
        else{
            switch ([WSCore fetchBookingType]) {
                case BookingTypeFavourite:
                    NSLog(@"Unwind to make bookings fav");
                    [self performSegueWithIdentifier:@"unwindToMakeBooking" sender:self];
                    break;
                    
                    
                default:
                    
                    break;
            }
 
        }
        
        [[User getInstance] downloadUsersBalance];

 
    });
    
    
}


@end
