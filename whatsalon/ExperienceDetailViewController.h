//
//  ExperienceDetailViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 25/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExperienceItem.h"


@protocol ExperienceDetailDelegate <NSObject>

@optional
-(void)updateBlogPostAsFavouriteWithBlogID: (NSString *)b_id;
-(void)updateBlogPostAsNotFavouriteWithBlogID: (NSString *)b_id;


@end
@interface ExperienceDetailViewController : UIViewController
- (IBAction)favouriteAction:(id)sender;

@property (nonatomic) id<ExperienceDetailDelegate> myDelegate;
@property (nonatomic) ExperienceItem * expItem;

@end
