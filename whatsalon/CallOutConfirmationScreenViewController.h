//
//  CallOutConfirmationScreenViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 10/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallOutConfirmationScreenViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *traceImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomTutorialImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomImageView;
@property (weak, nonatomic) IBOutlet UIImageView *tutorialImageView;
@end
