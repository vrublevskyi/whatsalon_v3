//
//  UserAnnotationView.m
//  whatsalon
//
//  Created by Graham Connolly on 10/11/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "UserAnnotationView.h"

@implementation UserAnnotationView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void) customInit {
    [[NSBundle mainBundle] loadNibNamed:@"UserAnnotationView" owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = self.bounds;
}
-(void) drawRect:(CGRect)rect
{
//    [self.logoImageView setImage: [UIImage imageNamed:self.salonImageType]];
}

@end
