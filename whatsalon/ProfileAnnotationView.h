//
//  ProfileAnnotationView.h
//  whatsalon
//
//  Created by Graham Connolly on 13/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface ProfileAnnotationView : MKAnnotationView
@property (nonatomic,assign) float width;
@property (nonatomic,assign) float height;
@property (nonatomic,retain) UIImageView * imageView;


-(id) initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier annotationWithImage: (UIImage *) anonViewImage withUserImage: (UIImage *) userImage;

@end
