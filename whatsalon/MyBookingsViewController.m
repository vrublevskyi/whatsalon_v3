//
//  MyBookingsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 25/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "MyBookingsViewController.h"
#import "WSCore.h"
#import "SalonReviewViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SocialShareViewController.h"
#import "BookingTableViewCell.h"
#import "User.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NoBookingMessageView.h"
#import "UIView+AlertCompatibility.h"
#import "GCNetworkManager.h"
#import "BrowseViewController.h"
#import "SalonIndividualDetailViewController.h"
#import "CongratulationsScreenViewController.h"
#import "GetDirectionsViewController.h"
#import "OptionsReviewViewController.h"
#import "UILabel+Boldify.h"
#import "UITableView+ReloadTransition.h"
#import "OpeningDay.h"





@interface MyBookingsViewController ()<UIViewControllerTransitioningDelegate,GCNetworkManagerDelegate,BookingsReviewDelegate>

@property (nonatomic,strong) NSMutableArray * bookingsArray;
@property (nonatomic) NSURLSessionDataTask * dataTask;
@property (nonatomic,assign) BOOL hasSubmittedReview;
@property (nonatomic) UIRefreshControl * refreshControl;
@property (nonatomic) NSURLSessionDataTask * task;
@property (nonatomic) GCNetworkManager * networkManager;
@property (nonatomic) GCNetworkManager * deleteEntryNetworkManager;

@property (weak, nonatomic) IBOutlet UIButton *upcoming;
@property (weak, nonatomic) IBOutlet UIButton *previous;
@property (nonatomic) NSMutableArray * buttons;

@property (nonatomic) BOOL isUpcomingBookings;

@property (nonatomic) UIView *upcomingSelectedView;

@property (nonatomic) NSMutableArray * upcomingBookingsArray;
@property (nonatomic) NSMutableArray * previousBookingsArray;

@end

@implementation MyBookingsViewController


#pragma mark - UIViewController LifeCycle

/*
 Ensure that the statusBar is black
 */
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [WSCore statusBarColor:StatusBarColorBlack];
}

/*
 Cancels any network tasks if the view disappears
 */
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    [self.networkManager cancelTask];
    
    [self.deleteEntryNetworkManager cancelTask];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if ([[User getInstance] isUserLoggedIn]) {
        if (self.networkManager == nil) {
            self.networkManager = [[GCNetworkManager alloc] init];
            self.networkManager.delegate=self;
            self.networkManager.parentView = self.view;
        }
        
        [self fetchPreviousBookings];

    }
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.bookingTableView.frame = self.view.frame;
}
/*
 Set up on viewDidLoad
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    self.deleteEntryNetworkManager = [[GCNetworkManager alloc] init];
    self.deleteEntryNetworkManager.delegate =self;
    self.deleteEntryNetworkManager.parentView=self.view;
    
    [WSCore returnNavBar:self.navigationController];
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.bookingTableView.tableFooterView=footerView;
    self.bookingTableView.delegate = self;
    self.bookingTableView.dataSource = self;
    //self.bookingsArray = [[NSMutableArray alloc] init];
    self.upcomingBookingsArray = [NSMutableArray array];
    self.previousBookingsArray = [NSMutableArray array];
    
    
    
    
    [[self bookingTableView] registerClass:[BookingTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    
    
    self.hasSubmittedReview=NO;
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.bookingTableView addSubview:self.refreshControl];
    self.refreshControl.backgroundColor = kGroupedTableGray;
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatest)
                  forControlEvents:UIControlEventValueChanged];
    
    if (self.navigationController.navigationBar.tintColor ==[UIColor whiteColor]) {
        [WSCore nonTransparentNavigationBarOnView:self];
        self.navigationController.navigationBar.tintColor=self.view.tintColor;
    }
    
    [self.bookingTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.upcoming.tag=0;
    self.previous.tag=1;
    self.buttons =[[NSMutableArray alloc] initWithObjects:self.upcoming,self.previous, nil];
    
    [self selectedButton:self.upcoming];
    
    [self.upcoming addTarget:self action:@selector(selectedButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.previous addTarget:self action:@selector(selectedButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.upcoming setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.previous setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    
    self.upcomingSelectedView = [[UIView alloc] initWithFrame:CGRectMake(0, self.upcoming.frame.size.height-5, self.upcoming.frame.size.width, 5)];
    self.upcomingSelectedView.backgroundColor=kWhatSalonBlue;
    self.upcomingSelectedView.tag=0;
    
    [self.upcoming addSubview:self.upcomingSelectedView];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeLeft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeRight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
    self.view.backgroundColor=kBackgroundColor;
    
    self.bookingTableView.hidden=YES;

}

-(void)swipe: (UISwipeGestureRecognizer *)swipe{
    NSLog(@"Swipe");
    if (swipe.direction==UISwipeGestureRecognizerDirectionLeft) {
        [self selectedButton:self.previous];
    }else if(swipe.direction==UISwipeGestureRecognizerDirectionRight){
        [self selectedButton:self.upcoming];
    }
}

-(void)selectedButton: (UIButton *) button{
    
    if (button.tag==0) {
        self.isUpcomingBookings=YES;
        
    }else{
        self.isUpcomingBookings=NO;
    }
    for(UIButton * btn in self.buttons) {
        if (btn.tag==button.tag) {
            NSLog(@"Selected");
            btn.backgroundColor=[UIColor silverColor];
            [btn addSubview:self.upcomingSelectedView];
            
        }else{
            
            NSArray* subviews = [btn subviews];
            for (UIView* subview in subviews) {
                
                if (subview==self.upcomingSelectedView) {
                    [subview removeFromSuperview];
                }
                
            }
            NSLog(@"Not Selected");
            btn.backgroundColor=[UIColor concreteColor];
            
        }
    }
    [self.bookingTableView reloadDataAnimated:YES];
}

/*
 Adds a view to the background view of the tableview when there are no bookings.
 
 */
- (void)addNoBookingsView {
    UIView * view = [[UIView alloc] initWithFrame:self.bookingTableView.frame];
    view.backgroundColor=[UIColor blueColor];
    
    NSLog(@"height %@",NSStringFromCGRect( view.frame));
    
    UIImageView * backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    backgroundImage.image = [UIImage imageNamed:@"background_candy_stripe"];
    backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    [view addSubview:backgroundImage];
    
    NoBookingMessageView * noMessage = [[NoBookingMessageView alloc] init];
    noMessage.view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.7];
    
    if (IS_iPHONE5_or_Above) {
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toBrowse)];
        tap.numberOfTapsRequired=1;
        noMessage.plusImageView.userInteractionEnabled=YES;
        [noMessage addGestureRecognizer:tap];
        
    }
    else{
        noMessage.plusImageView.hidden=YES;
    }
    [view addSubview:noMessage];
    if (IS_iPHONE4) {
        noMessage.center = view.center;
    }else{
         noMessage.center = CGPointMake(CGRectGetWidth(view.bounds)/2, CGRectGetHeight(view.bounds)/2);
    }
   
    
    
    if (self.upcomingBookingsArray.count==0) {
        noMessage.bookingText.text = @"When you're finished making a booking, your upcoming appointments will appear here.";
        noMessage.bookingText.adjustsFontSizeToFitWidth=YES;
    }
    self.bookingTableView.backgroundView=view;
}



/*
 Triggered by the NoBookingsViews when the imageView is pressed
 */
-(void)toBrowse{
    
    BrowseViewController*viewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"browseController"];
    /*
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:
     UIViewAnimationTransitionFlipFromRight
                           forView:self.navigationController.view cache:NO];
    
    [self.navigationController pushViewController:viewController animated:YES];
    [UIView commitAnimations];
     */
    [self.navigationController pushViewController:viewController animated:YES];
}



/*
 Removes the No bookings view
 
 */
- (void)removeNoBookingsView {
    self.bookingTableView.backgroundView=nil;
}

#pragma mark - GCNetworkManagerDelegate
/*
 Handles the success response from GCNetworkManagerDelegate
 */
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    NSLog(@" Bookings array %@",jsonDictionary);
    
    
    self.bookingTableView.hidden=NO;
    if (manager==self.networkManager) {
        /*
        int item = [jsonDictionary[@"message"][@"size"] intValue];
        if (item==0 ) {
            if (self.bookingTableView.backgroundView==nil)
                [self addNoBookingsView];
            
            
            return ;
        }
     
        [self removeNoBookingsView];
         */
        NSArray * bookingsArray = jsonDictionary[@"message"][@"results"];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        
        for (NSDictionary *bDict in bookingsArray) {
            
            NSLog(@"b Dict %@",bDict);
            MyBookings * booking = [MyBookings bookingWithID:[NSString stringWithFormat:@"%@",bDict[@"booking_id"]] ];
            
            
            
            booking.bookingCreatedData = bDict[@"booking_created_date"];
            booking.bookingDeposit = bDict[@"booking_deposit"];
            booking.bookingEndTime = bDict[@"booking_end_time"];
            booking.bookingPaid = [bDict[@"booking_piad"] boolValue];
            
            booking.bookingPaidDate = bDict[@"booking_paid_date"];
            booking.bookingStartTime = bDict[@"booking_start_time"];
            booking.bookingTotal = bDict[@"booking_total"];
            
            if (bDict[@"staff_first_name"]!=[NSNull null]) {
                
                booking.stylistName=[NSString stringWithFormat:@"%@ %@",bDict[@"staff_first_name"],bDict[@"staff_last_name"]];
            }
            //set up salon object
            
            booking.bookingSalon.salon_id = [NSString stringWithFormat:@"%@",bDict[@"salon_id"] ];
            if (bDict[@"rating"] != [NSNull null]) {
                booking.bookingSalon.ratings = [bDict[@"salon_details"][@"rating"] doubleValue];
            }

            booking.bookingSalon.salon_name=bDict[@"salon_name"];
            booking.bookingSalon.salon_image=bDict[@"salon_details"][@"salon_image"];
            booking.bookingSalon.salon_description=bDict[@"salon_details"][@"salon_description"];
            booking.bookingSalon.salon_landline=bDict[@"salon_details"][@"salon_landline"];
            booking.bookingSalon.salon_address_1=bDict[@"salon_details"][@"salon_address_1"];
            booking.bookingSalon.salon_address_2=bDict[@"salon_details"][@"salon_address_2"];
            booking.bookingSalon.salon_address_3=bDict[@"salon_details"][@"salon_address_3"];
            booking.bookingSalon.is_favourite = [bDict[@"salon_details"][@"is_favourite"] boolValue];
            booking.active = [bDict[@"active"] boolValue];
            booking.reviewOccurred = [bDict[@"review_occurred"] boolValue];
            booking.bookingOccurred = [bDict[@"booking_occurred"] boolValue] ;
            booking.isCancelled = [bDict[@"is_cancelled"] boolValue];
            
            
            if ([bDict[@"salon_details"][@"salon_latest_review"]count]!=0) {
                NSLog(@"** \n\n Salon review %@ \n\n **",bDict[@"salon_details"][@"salon_latest_review"]);
                booking.bookingSalon.hasReview=YES;
                SalonReview *salonReview = [[SalonReview alloc] initWithReviewer:bDict[@"salon_details"][@"salon_latest_review"][@"user_name"] AndWithLastName:bDict[@"salon_latest_review"][@"user_last_name"]];
                NSLog(@"first name %@",bDict[@"salon_latest_review"][@"user_name"]);
                salonReview.message = bDict[@"salon_details"][@"salon_latest_review"][@"message"];
                salonReview.review_image=bDict[@"salon_details"][@"salon_latest_review"][@"review_image"];
                salonReview.salon_rating=bDict[@"salon_details"][@"salon_latest_review"][@"salon_rating"];
                salonReview.stylist_rating=bDict[@"salon_details"][@"salon_latest_review"][@"stylist_rating"];
                salonReview.date_reviewed=bDict[@"salon_details"][@"salon_latest_review"][@"date_reviewed"];
                salonReview.user_avatar_url=bDict[@"salon_details"][@"salon_latest_review"][@"user_avatar"];
                booking.bookingSalon.salonReview=salonReview;
                
            }

            if (bDict[@"salon_details"][@"salon_images"]!=[NSNull null]) {
                
                for (NSDictionary *imageDict in bDict[@"salon_details"][@"salon_images"]) {
                    NSString * url = imageDict[@"image_path"];
                    
                    [booking.bookingSalon.slideShowGallery addObject:url];
                    
                }
            }
            
            
            
            booking.bookingSalon.salon_lat=[bDict[@"salon_details"][@"salon_lat"] doubleValue];
            booking.bookingSalon.salon_long=[bDict[@"salon_details"][@"salon_lon"] doubleValue];
            
            
            booking.staffID=[NSString stringWithFormat:@"%@",bDict[@"staff_id"]];
            if (bDict[@"service_name"]!=[NSNull null]) {
                booking.servceName = bDict[@"service_name"];
                
            }
            
            
            if (bDict[@"rating"]!=[NSNull null]) {
                booking.bookingSalon.ratings = [bDict[@"rating"] doubleValue];
            }
            if (bDict[@"reviews"]!=[NSNull null]) {
                booking.bookingSalon.reviews = [bDict[@"reviews"] doubleValue];
            }
            
            if ([bDict[@"salon_details"][@"salon_categories"] count]!=0) {
                booking.bookingSalon.salonCategories =  @{
                                           @"Hair" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Hair"] boolValue]],
                                           @"Nails" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Nails"]boolValue]],
                                           @"Face" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Face"] boolValue]],
                                           @"Body" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Body"] boolValue]],
                                           @"Massage" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Massage"] boolValue]],
                                           @"Hair Removal" : [NSNumber numberWithBool:[bDict[@"salon_details"][@"salon_categories"][@"Hair Removal"] boolValue]],
                                           };
            }

            
            if ([bDict[@"salon_details"][@"salon_opening_hours"] count]!=0) {
                NSLog(@"**** has opening hours \n\n %@ \n\n",bDict[@"salon_details"][@"salon_opening_hours"]);
                NSArray * openingHours =bDict[@"salon_details"][@"salon_opening_hours"];

                for (NSDictionary* day in openingHours) {
                    NSLog(@"**11 %@ day %@ day of week %@",booking.bookingSalon.salon_name, day,day[@"day_of_week"]);
                    
                    OpeningDay * openingDay = [OpeningDay openingDayWithDayOfWeek:day[@"day_of_week"] WithStartTime:day[@"start"]  WithEndTime:day[@"end"]  WithDayName:day[@"day_name"]  AndIsOpene:[day[@"opened"] boolValue] ];
                    
                    [booking.bookingSalon.openingHours addObject:openingDay];
                
                }
                NSLog(@"opening hours count %lu",(unsigned long)booking.bookingSalon.openingHours.count);
                
            }

            NSLog(@"Booking has occured %d",booking.bookingOccurred);
            //[self.bookingsArray addObject:booking];
            
           
            NSLog(@"Booking start time %@",booking.bookingEndTime);
            NSLog(@"date %@",[dateFormat dateFromString:booking.bookingEndTime]);
            NSDate *date = [dateFormat dateFromString:booking.bookingEndTime];
            date =[date dateByAddingTimeInterval:60*30];
            NSLog(@"Date %@",date);
            if ([date timeIntervalSinceNow] < 0.0) {
                // Date has passed
                [self.previousBookingsArray addObject:booking];

            }
            /*
            if (booking.bookingOccurred ==YES) {
                [self.previousBookingsArray addObject:booking];
            }
             */
            else{
                [self.upcomingBookingsArray addObject:booking];
            }
            
            NSLog(@"\n\n****Booking id %@ start time %@ service %@ is cancelled %d\n",booking.bookingId,booking.bookingStartTime,booking.servceName,booking.isCancelled);
        }
        
        [self.bookingTableView reloadData];
        
        self.hasSubmittedReview=NO;
        
    }else if(manager==self.deleteEntryNetworkManager){
        
        
    }
    
}

/*
 Handles the failure message
 */
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    self.bookingTableView.hidden=NO;
    
    if (manager==self.networkManager) {
        [WSCore showServerErrorAlert];
    }
    else if(manager==self.deleteEntryNetworkManager){
        [UIView showSimpleAlertWithTitle:@"Oh Oh!" message: jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
    }
    
}

/*
 Fetch the Booking History
 */
-(void)fetchPreviousBookings{
    
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kBooking_History_URL]];
    
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    if ([[User getInstance] fetchKey].length>0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
    }
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}


/*
 Refreshes the TableView
 */
-(void)getLatest{
    //[self.bookingsArray removeAllObjects];
    [self.upcomingBookingsArray removeAllObjects];
    [self.previousBookingsArray removeAllObjects];
    [self fetchPreviousBookings];
    
    [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}


/*
 Reloads the tableview and handles the refreshControl
 
 */
- (void)reloadData
{
    
    
    [self.bookingTableView reloadData];
    
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}







#pragma mark - UITableView datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.isUpcomingBookings) {
        if (self.upcomingBookingsArray.count==0) {
            if (self.bookingTableView.backgroundView==nil)
                [self addNoBookingsView];
            return 0;
        }
        
        [self removeNoBookingsView];
        
        return self.upcomingBookingsArray.count;
    }
    else{
        //previous
        
        if (self.previousBookingsArray.count==0) {
            if (self.bookingTableView.backgroundView==nil)
                [self addNoBookingsView];
            return 0;
        }
        
        [self removeNoBookingsView];
        
        return self.previousBookingsArray.count;
    }
   
    return 0;
    
    //return self.bookingsArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BookingTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    MyBookings * bookings;
    if (self.isUpcomingBookings) {
        cell.isPrevious=NO;
        bookings = [self.upcomingBookingsArray objectAtIndex:indexPath.row];
    }
    else{
        cell.isPrevious=YES;
        bookings = [self.previousBookingsArray objectAtIndex:indexPath.row];
    }
    cell.salon=bookings.bookingSalon;
    NSLog(@"salon %f",bookings.bookingSalon.ratings);
    [cell updateCellWithBooking:bookings];
    
    
    return cell;
}

/*
 Handles the selection of the tableview
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //MyBookings * booking = [self.bookingsArray objectAtIndex:indexPath.row];
    MyBookings * booking;
    if (self.isUpcomingBookings) {
        booking = [self.upcomingBookingsArray objectAtIndex:indexPath.row];
    }
    else{
        booking = [self.previousBookingsArray objectAtIndex:indexPath.row];
    }
    
    if (!booking.isCancelled) {
        OptionsReviewViewController *destinationViewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"optionsVC"];
        destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
        destinationViewController.transitioningDelegate = self;
        destinationViewController.bookingData = booking;
        destinationViewController.delegate=self;
        
        [self presentViewController:destinationViewController animated:YES completion:^{
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }];

    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 100;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - RESideMenu Delegate Methods
- (IBAction)showMenu:(id)sender {
    
    [self.sideMenuViewController presentMenuViewController];
    
    
}



#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}


#pragma mark - Review Delegate
-(void)reloadBookings{
    self.hasSubmittedReview=YES;
    //[self.bookingsArray removeAllObjects];
    [self.upcomingBookingsArray removeAllObjects];
    [self.previousBookingsArray removeAllObjects];
    [self fetchPreviousBookings];
}

#pragma mark - UITableView Delegate
/*
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 //delete cells
 [self deleteBookingAtIndexPathRow:indexPath.row];
 [self.bookingsArray removeObjectAtIndex:indexPath.row];
 
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 
 if (self.bookingsArray.count==0) {
 if (self.bookingTableView.backgroundView==nil) {
 [self addNoBookingsView];
 
 }
 }
 else if(self.bookingTableView.backgroundView!=nil){
 [self removeNoBookingsView];
 }
 
 }
 
 }
 
 -(void)deleteBookingAtIndexPathRow: (NSInteger) row{
 NSLog(@"DELETE");
 
 
 MyBookings * booking = [self.bookingsArray objectAtIndex:row];
 
 
 NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kDelete_Booking_URL]];
 NSString * params = [NSString stringWithFormat:@"confirm_id=%@",booking.bookingId];
 params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",[[User getInstance] fetchAccessToken]]];
 
 [self.deleteEntryNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
 
 
 }
 */

/*
 Unwind segue - called when user makes a booking via the my bookings sections
 */
- (IBAction)unwindToMyBookings:(UIStoryboardSegue *)unwindSegue
{
    UIViewController* sourceViewController = unwindSegue.sourceViewController;
    
    if ([sourceViewController isKindOfClass:[CongratulationsScreenViewController class]])
    {
        [WSCore statusBarColor:StatusBarColorBlack];
        
        [WSCore nonTransparentNavigationBarOnView:self];
    }
}

#pragma mark - BookingsReviewDelegate Methods
-(void)bookAgainWithSalon:(Salon *)salon{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    SalonIndividualDetailViewController * sVC = [self.storyboard instantiateViewControllerWithIdentifier:@"salonVC"];
    salon.isReBooking=YES;
    sVC.salonData = salon;
    [self.navigationController pushViewController:sVC animated:YES];
    
}

-(void)canceledAppointment:(MyBookings *)booking{
    
    NSLog(@"Canceled");
    
    NSLog(@"Booking id %@ ",booking.bookingId);
   
    for (int i=0 ; i<self.upcomingBookingsArray.count; i++) {
        
        MyBookings * b = [self.upcomingBookingsArray objectAtIndex:i];
        if ([b.bookingId isEqualToString:booking
             .bookingId]) {
            NSLog(@"Before Booking id is cancelled %d",b.isCancelled);
            b.isCancelled=YES;
             NSLog(@"After Booking id is cancelled %d",b.isCancelled);
            [self.upcomingBookingsArray replaceObjectAtIndex:i withObject:b];
        }
      
    }
    [self.bookingTableView reloadData];
     
    
}


@end
