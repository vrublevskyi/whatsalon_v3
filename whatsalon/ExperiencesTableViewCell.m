//
//  ExperiencesTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 25/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "ExperiencesTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ColoredImage.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "UIView+AlertCompatibility.h"

@implementation ExperiencesTableViewCell

#pragma mark - GCNetworkDelegate
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    NSLog(@"Success %@",jsonDictionary);
    self.bottomView.userInteractionEnabled=YES;
   // [self heartAppearance:self.heartImage exp:self.expItem];
}

-(void)manager:(GCNetworkManager *)manager handleServerFailureWithError:(NSError *)error{
    NSLog(@"Server");
    self.bottomView.userInteractionEnabled=YES;
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Failure %@",jsonDictionary);
    self.bottomView.userInteractionEnabled=YES;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.clipsToBounds=YES;
        
        self.backgroundColor=[UIColor clearColor];
        self.contentView.backgroundColor=[UIColor clearColor];
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
        
        self.networkManager = [[GCNetworkManager alloc] init];
        self.networkManager.delegate=self;
        
       
        self.heartImage.userInteractionEnabled=NO;
        self.heartImage.layer.cornerRadius=kCornerRadius;
        self.heartImage.layer.borderColor=[UIColor blueColor].CGColor;
        self.heartImage.layer.borderWidth=1.0f;
    
    }
    return self;
}

-(void)favCall{
    
    if ([[User getInstance] isUserLoggedIn]) {
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kFavourite_Experience_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&experience_id=%@",self.expItem.experience_id]];
        
       
        if (self.isFav) {
            NSLog(@"\n\n *** Remove from fav ****\n\n");
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&unfavourite=1"]];
            
        }
        else{
            NSLog(@"\n\n *** Add fav **** \n\n");
        }
        
        self.bottomView.userInteractionEnabled=NO;
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        
        [self heartAppearance:self.heartImage withFav:self.isFav];//remove to go back to old code

    }
    else{
        
        [WSCore showLoginSignUpAlert];
    }
}
-(void)heartAppearance: (UIImageView *)heartImage withFav: (BOOL) isFav{
    
    if (self.isFav) {
        heartImage.image= [UIImage imageNamed:@"White_Heart"];
        heartImage.image = [heartImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [heartImage setTintColor:kWhatSalonSubTextColor];
        
        if ([self.myDelegate respondsToSelector:@selector(didUnFavouriteBlogPostWithID:)]) {
            [self.myDelegate didUnFavouriteBlogPostWithID:self.expItem.experience_id];
        }
        self.isFav=NO;
    }
    else{
        self.isFav=YES;
        heartImage.image = [UIImage imageNamed:@"White_Heart_Solid"];
        heartImage.image = [heartImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [heartImage setTintColor:kWhatSalonBlue];
        if ([self.myDelegate respondsToSelector:@selector(didFavouriteBlogPostWithID:)]) {
            [self.myDelegate didFavouriteBlogPostWithID:self.expItem.experience_id];
        }
        
    }

}
- (void)heartAppearance:(UIImageView *)heartImage exp:(ExperienceItem *)exp {

    if (self.isFav) {
        heartImage.image= [UIImage imageNamed:@"White_Heart"];
        heartImage.image = [heartImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [heartImage setTintColor:kWhatSalonSubTextColor];
        
        if ([self.myDelegate respondsToSelector:@selector(didUnFavouriteBlogPostWithID:)]) {
            [self.myDelegate didUnFavouriteBlogPostWithID:self.expItem.experience_id];
        }
        self.isFav=NO;
    }
    else{
        self.isFav=YES;
        heartImage.image = [UIImage imageNamed:@"White_Heart_Solid"];
        heartImage.image = [heartImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [heartImage setTintColor:kWhatSalonBlue];
        if ([self.myDelegate respondsToSelector:@selector(didFavouriteBlogPostWithID:)]) {
            [self.myDelegate didFavouriteBlogPostWithID:self.expItem.experience_id];
        }
        
    }
}

-(void)setUpExperiencesCellWithExperienceItem:(ExperienceItem *)exp{
    self.expItem = exp;
    
    self.isFav = (BOOL)self.expItem.is_user_favourite;
   
    self.holderView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.frame.size.width-20, self.frame.size.height-10)];
    self.holderView.backgroundColor=[UIColor whiteColor];
    self.holderView.layer.cornerRadius=kCornerRadius;
    self.holderView.layer.masksToBounds=YES;
    self.holderView.clipsToBounds=YES;
    self.holderView.layer.borderColor=[UIColor silverColor].CGColor;
    self.holderView.layer.borderWidth=0.5;
    [self.contentView addSubview:self.holderView];
    
    self.headerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height/2.0f)];
    [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:exp.expImageURL] placeholderImage:kPlace_Holder_Image];
    self.headerImageView.layer.masksToBounds=YES;
    self.headerImageView.clipsToBounds=YES;
    self.headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.holderView addSubview:self.headerImageView];
    
   
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, self.headerImageView.frame.size.height+5, self.holderView.frame.size.width-20, 22)];
    self.titleLabel.text=exp.title;
    self.titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    [self.holderView addSubview:self.titleLabel];
    
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(5, self.holderView.frame.size.height-40, self.holderView.frame.size.width-10, 44)];
    UITapGestureRecognizer * addToFavTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(favCall)];
    addToFavTap.numberOfTapsRequired=1;
    addToFavTap.delegate=self;
    addToFavTap.delaysTouchesBegan=YES;
    addToFavTap.cancelsTouchesInView=YES;
    [self.bottomView addGestureRecognizer:addToFavTap];
    [WSCore addTopLine:self.bottomView :kCellLinesColour :0.5 WithYvalue:0];
     self.bottomView.userInteractionEnabled=YES;
    [self.holderView addSubview:self.bottomView];
    
    self.previewLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, self.headerImageView.frame.size.height+self.titleLabel.frame.size.height, self.holderView.frame.size.width-20, self.holderView.frame.size.height-self.headerImageView.frame.size.height-self.titleLabel.frame.size.height-self.bottomView.frame.size.height)];
    self.previewLabel.text =exp.expDescription;
    self.previewLabel.font = [UIFont systemFontOfSize:14.0f];
    self.previewLabel.textColor=kWhatSalonSubTextColor;
    self.previewLabel.numberOfLines=0;
    if (self.previewLabel.text.length>258) {
        self.previewLabel.adjustsFontSizeToFitWidth=YES;
    }
    [self.holderView addSubview:self.previewLabel];

    self.heartImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
    
    if (self.isFav) {
        
        self.heartImage.image = [UIImage imageNamed:@"White_Heart_Solid"];
        self.heartImage.image = [self.heartImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.heartImage setTintColor:kWhatSalonBlue];
        
       
    }
    else{
        
        self.heartImage.image= [UIImage imageNamed:@"White_Heart"];
        self.heartImage.image = [self.heartImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.heartImage setTintColor:kWhatSalonSubTextColor];
    }

    
    
    self.heartImage.contentMode = UIViewContentModeScaleAspectFill;
    [self.bottomView addSubview:self.heartImage];
    
    self.numberOfHearts = [[UILabel alloc] initWithFrame:CGRectMake(10 +10 + self.heartImage.frame.size.width, (self.bottomView.frame.size.height/2)-10, 120, 20)];
    self.numberOfHearts.textColor=kWhatSalonSubTextColor;
    self.numberOfHearts.text = [NSString stringWithFormat:@"%@ likes",exp.favourite_dispaly];
    self.numberOfHearts.font = [UIFont systemFontOfSize:12.0f];
    [self.bottomView addSubview:self.numberOfHearts];
    self.numberOfHearts.userInteractionEnabled=NO;
    
    

    NSString *dateString = self.expItem.experienceDateToDisplay;
    UILabel * dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.holderView.frame.size.width/2.0, 10, (self.holderView.frame.size.width/2)-20, 20)];
    dateLabel.text =dateString;
    dateLabel.textAlignment=NSTextAlignmentRight;
    dateLabel.textColor=kWhatSalonSubTextColor;
    dateLabel.font = [UIFont systemFontOfSize:12.0f];
    [self.bottomView addSubview:dateLabel];
    
    if (!self.hasShadow) {
        [WSCore floatingView:self WithAlpha:0.5];
        self.hasShadow=YES;
    }
    
}

@end
