//
//  TabDirectoryDetailViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 21/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabDirectoryDetailViewController.h"
#import "UIImage+ColoredImage.h"
#import "FUIButton.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GetDirectionsViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"

@interface TabDirectoryDetailViewController ()<UIViewControllerTransitioningDelegate>

/*! @brief represents the view for holder the content. */
@property (weak, nonatomic) IBOutlet UIView *holderView;

/*! @brief the call action. */
- (IBAction)call:(id)sender;

/*! @brief represents the FUIButton for making a call. */
@property (weak, nonatomic) IBOutlet FUIButton *callBtn;

/*! @brief represents the get directions button. */
- (IBAction)getDirections:(id)sender;

/*! @brief represents the FUIButton to get the directions. */
@property (weak, nonatomic) IBOutlet FUIButton *getDirectionsBtn;

/*! @brief represents the salon name. */
@property (weak, nonatomic) IBOutlet UILabel *salonName;

/*! @brief represents the label for the address. */
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

/*! @brief represents a label for the distance. */
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

/*! @brief represents a salon image view. */
@property (weak, nonatomic) IBOutlet UIImageView *salonImage;

/*! @brief represents the report image view. */
@property (weak, nonatomic) IBOutlet UIImageView *reportImage;

/*! @brief represents the cancel action. */
- (IBAction)cancel:(id)sender;

/*! @brief represents the cancel UIButton. */
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@end

@implementation TabDirectoryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.holderView.backgroundColor=[UIColor whiteColor];
    self.holderView.layer.cornerRadius=kCornerRadius;
    self.holderView.layer.masksToBounds=YES;
    self.salonImage.layer.cornerRadius=kCornerRadius;
    self.salonImage.layer.masksToBounds=YES;
    
    self.reportImage.image = [self.reportImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.reportImage.tintColor = [UIColor alizarinColor];
    
    self.salonImage.image =[UIImage coloredImage_resizeableImageWithColor:[UIColor silverColor]];
    
    [self.cancelBtn setTitle:@"" forState:UIControlStateNormal];
    [self.cancelBtn setImage:kCancelButtonImage forState:UIControlStateNormal];
    
    self.cancelBtn.imageView.image = [self.cancelBtn.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.cancelBtn.tintColor = [UIColor whiteColor];
    
    self.callBtn.backgroundColor = kWhatSalonBlue;
    self.getDirectionsBtn.backgroundColor = kWhatSalonBlue;
    
    self.view.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.8];
    
    self.callBtn.layer.cornerRadius=kCornerRadius;
    self.callBtn.layer.masksToBounds=YES;
    self.callBtn.buttonColor = kWhatSalonBlue;
    self.callBtn.backgroundColor = [UIColor clearColor];
    self.callBtn.cornerRadius=kCornerRadius;
    self.callBtn.shadowColor=kWhatSalonBlueShadow;
    self.callBtn.shadowHeight=kShadowHeight;
    
    self.getDirectionsBtn.layer.cornerRadius=kCornerRadius;
    self.getDirectionsBtn.layer.masksToBounds=YES;
    self.getDirectionsBtn.buttonColor = kWhatSalonBlue;
    self.getDirectionsBtn.backgroundColor = [UIColor clearColor];
    self.getDirectionsBtn.cornerRadius=kCornerRadius;
    self.getDirectionsBtn.shadowColor=kWhatSalonBlueShadow;
    self.getDirectionsBtn.shadowHeight=kShadowHeight;
    
    
    [self.getDirectionsBtn setImage:[UIImage imageNamed:@"Map"] forState:UIControlStateNormal];
    [self.getDirectionsBtn setTitle:@"" forState:UIControlStateNormal];
    self.getDirectionsBtn.imageView.image = [self.getDirectionsBtn.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.getDirectionsBtn.tintColor=[UIColor whiteColor];
    
    self.reportImage.hidden=YES;
    
    self.distanceLabel.textColor = kWhatSalonBlue;
    
    self.salonName.text = self.salon.salonName;
    self.addressLabel.text = self.salon.salonAddress;
    self.addressLabel.numberOfLines=0;
    self.distanceLabel.text = [NSString stringWithFormat:@"%.1f km", [self.salon.distance doubleValue] ] ;
    self.distanceLabel.font = [UIFont boldSystemFontOfSize:self.distanceLabel.font.pointSize];
    
    if (self.salon.imageUrlLink.length <1) {
        self.salonImage.image = kPlace_Holder_Image;
        self.salonImage.contentMode=UIViewContentModeScaleAspectFill;
        
        
    }
    else{
        [self.salonImage sd_setImageWithURL:[NSURL URLWithString:self.salon.imageUrlLink] placeholderImage:kPlace_Holder_Image];
    }
    self.salonName.adjustsFontSizeToFitWidth=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)getDirections:(id)sender {
    
    GetDirectionsViewController *destinationViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
    destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
    destinationViewController.salonMapData = self.salon;
    destinationViewController.transitioningDelegate = self;

    [self presentViewController:destinationViewController animated:YES completion:nil];
}
- (IBAction)call:(id)sender {
    NSString *telno = [NSString stringWithFormat:@"tel://%@",self.salon.salonPhone];

    if (telno.length>0) {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telno]];
        }
      
    }
    
    
}
- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    
    return [[PresentDetailTransition alloc] init];
    
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return [[DismissDetailTransition alloc] init];
    
}
@end
