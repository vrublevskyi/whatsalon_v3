//
//  FAQDetailViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 22/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FAQItem.h"

@interface FAQDetailViewController : UIViewController

/*! @brief represents the web view for hosting the content. */
@property (weak, nonatomic) IBOutlet UIWebView *webview;

/*! @brief represents the FAQItem object. */
@property (strong, nonatomic) FAQItem * faqDetailItem;

@end
