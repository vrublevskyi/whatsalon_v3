//
//  User.h
//  whatsalon
//
//  Created by Graham Connolly on 11/07/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

/*!
 @header User.h
  
 @brief This is the header file for the User class
  
 This file contains the most important User methods and propertie decalarations. 
  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd.
 @version    10+
 */

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "GCNetworkManager.h"
#import "CurrencyItem.h"
#import <CoreLocation/CoreLocation.h>

@interface User : NSObject <GCNetworkManagerDelegate>

/*! @brief This property represents a guest users location. */
@property (nonatomic) CLLocation * guestUserLocation;

/*! @brief This property represents the url to the users social avatar. */
@property (nonatomic) NSString * social_avatar_url;

/*! @brief represents the users gender. */
@property (nonatomic) NSString *gender;

/*! @brief represents the users accessToken. */
@property (nonatomic) NSString * accessToken;

/*! @brief represents the users email address. */
@property (nonatomic) NSString *email;

/*! @brief represents the users surname. */
//@property (nonatomic) NSString *surname;

/*! @brief represents the users firstName. */
@property (nonatomic) NSString *firstName;

/*! @brief represents the users lastName. */
@property (nonatomic) NSString *lastName;

/*! @brief represents the users phoneNumber. */
@property (nonatomic) NSString *phoneNumber;

/*! @brief represents the users balance. */
@property (nonatomic) NSString *usersBalance DEPRECATED_MSG_ATTRIBUTE("Users balance is no longer used in the app. Now it the app focuses on coupon codes");

//@property (nonatomic) NSData * imagePathData;

/*! @brief represent the users first name on their Credit Card. */
@property (nonatomic) NSString * ccFirstName;

/*! @brief represents the users last name on their Credit Card. */
@property (nonatomic) NSString * ccLastName;

/*! @brief represents the users credit card number. */
@property (nonatomic) NSString * ccCardNo;

/*! @brief represents the users credit card type. */
@property (nonatomic) NSString * ccType;

/*! @brief represents the users credit card expiry. */
@property (nonatomic) NSString * ccExp;

/*! @brief represents the users credit card payment reference. */
@property (nonatomic) NSString * ccPaymentRef;

/*! @brief represents the url to the users image. */
@property (nonatomic) NSURL * usersImageURL;

/*! @brief represents the users key. */
@property (nonatomic) NSString*key;

/*! @brief represents whehter the user has a credit card, or whether it is nil. */
@property (nonatomic) NSNumber *hasCardYesNoOrNil;

/*! @brief represents whether the user is banned or not. */
@property (nonatomic) NSNumber *isBannedYesNoOrNil;

/*! @brief represents whether the user should be logged out or not. */
@property (nonatomic) NSNumber * needsLogOutYesNoOrNil;

/*! @brief represents the number of favourited experience items. */
@property (nonatomic) NSString * noOfExpFav DEPRECATED_MSG_ATTRIBUTE("Experiences has been removed from the app since 4.0.5");

/*! @brief represents the number of favourite salons. */
@property (nonatomic) NSString * noOfSalonsFav;// no of Salons favourited

/*! @brief represents whether the users phone number is Twilio verified. */
@property (nonatomic) NSNumber * isTwilioVerifiedYesNoOrNil;

/*! @brief represents the users currency object. */
@property (nonatomic) CurrencyItem * usersCurrency;

/*! @brief this is the CGNetworkManager for the users profile. */
@property (nonatomic) GCNetworkManager * profileManager;

/*! @brief network mangager used. */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief represents the users city and country. */
@property (nonatomic) NSString * usersCityAndCounty;

/*! @brief BOOL value representing whether a users profile is being fetched or not. */
@property (nonatomic) BOOL isFetchingProfile;

/*!
 @brief Get an Instance of User class.
 
 @return instancetype of user class.
 */

+(instancetype)getInstance;

/*!
 @brief Creates and saves the users profile.
 
 @discussion Accesses the keys of the profDict and sets them as objects for NSUserDefault keys. NSUserDefaults are then synchronized.
 
 @param profDict An NSDictionary object containing the users information.
 */

-(void)setUpProfile:(NSDictionary *)profDict;

/*!
 @brief fetches the users gender value.
 
 @return NSString - The users gender as a string.
 */

-(NSString *)fetchGender;

/*!
 @brief fetches the users AccessToken from NSUserDefaults. The user is given an AccessToken on logging in or signing up.
 
 @return NSString - The users access token.
 */

-(NSString *)fetchAccessToken;

/*!
 @brief fetches the users Email.
 
@return NSString - The users registered email address
 */
-(NSString *)fecthEmail;

/*!
 @brief fetches the users first name.
 
 @return NSString - The users registered first name.
 */
-(NSString *)fetchFirstName;

/*!
@brief fetches the users lastname.
 
@return NSString - The users registered last name.
 */
-(NSString *)fetchLastName;

/*!
@brief fetches the users registered phone number.

@return NSString - The users registered email address.
 */
-(NSString *)fetchPhoneNumber;

/*!
@brief fetches the users balance

@return NSString - The users balance
 */
-(NSString *)fetchUsersBalance;


/*!
@brief Updates the users Access Token.
 
@param token - The users new AccessToken.
 */
-(void)updateAccessToken:(NSString *)token;

/*!
@brief Updates the users email address.

@param email - The users new email address
 */

-(void)updateEmailAddress:(NSString *)email;

/*!
@brief update the users phone number.

@param phone The users new phone number.
 */

-(void)updatePhoneNumber: (NSString *)phone;

///*!
// * @discussion The path to the users image
// * @param userToken The users access token
// * @return string value containing the path to the users image
// */
//
//-(NSData *)fetchUserImagePath: (NSString *)userToken;

/*!
 @brief downloads the users balance.
 @discussion performs a network request to download the users balance.
 */
-(void)downloadUsersBalance DEPRECATED_MSG_ATTRIBUTE("Users balance is a deprecated feature and is no longer used");

/*!
 @brief logs the user out.
 @discussion removes users saved details from NSUserdefaults and Key chain. Restores the User state to the default state.
 */
-(void)logout;

/*!
 @brief updates the users credit card information
 @discussion updates the keychain store using the parameters. Also calls the method @c[self updateDoesUserHaveCard:];
 
 @param firstName - NSString representation of the users firstName
 @param lastName - NSString representation of the users lastName
 @param no - NSString representation of the users credit card number
 @param type - NSString representation of the users credit card type
 @param exp - NSString representation of the users credit card expiry date
 @param ref - NSString representation of the users payment reference (Realex)
 */
-(void)updateCreditCardDetailsWithFirstName:(NSString *) firstName withLastName: (NSString *) lastName withCardNumber: (NSString *) no withCardType: (NSString *) type withExp : (NSString *) exp withPaymentRef: (NSString *) ref;

/*!
 @brief gets the first name on the credit card.
 
 @return NSString - returns the users firstName on the credit card.
 
 */
-(NSString *) fetchCreditCardFirstName;

/*!
 @brief updates the first name on the credit card.
 
 @discussion updates the @c[self.ccFirstName] and the key chain store.
 
 @param firstName NSString representation of the firstName on the card.
 */
-(void) updateCreditCardFirstName: (NSString *) firstName;

/*!
 @brief gets the last name of the credit card.
 
 @discussion returns the last name on the credit card. If @c[self.ccLastName] is nil, it is then fetched from the KeyChain.
 
 @return NSString - @c[self.ccLastName] is returned.
 */
-(NSString *) fetchCreditCardLastName;

/*!
 @brief updates the credit card last name.
 
 @discussion updates @c[self.ccLastName] and updates the keychain store.
 
 @param lastName - NSString representation of the last name on the card.
 */
-(void)updateCreditCardLastName: (NSString *) lastName;

/*!
 @brief fetches the credi card number.
 
 @discussion This method returns the @c[self.ccCardNo] property, if it is nil, then it is retrieved from the keychain.
 
 @return NSString @c[self.ccCardNo]
 
 */
-(NSString *) fetchCreditCardNo;

/*!
 @brief updates the credit card number.
 
 @discussion updates the credit card  no. Updates the variable @c[self.ccCardNo]. Updates the UICKeyChainStore. Synchronizes the UICKeyChainStore.
 
 @param no NSString represents the credit card no.
 */
-(void)updateCreditCardNo: (NSString *) no;


/*!
@brief fetches the Credit Card Type. If nil assign from the UICKeyChainStore.

@return self.ccType NSString which represents type.
 */
-(NSString *) fetchCreditCardType;

/*!
@discussion Updates the Credit Card type. Sets the variable self.ccType to cType. Updates the UICKeyChainStore.
 
@param cType NSString represents the cType
 */
-(void)updateCreditCardType: (NSString *) cType;

/*!
 @discussion fetch the credit card exp. if ccExp is nil, fetch from UICKeyChainStore.
 
 @return @c[self.ccExp] represents the expiry type of the card.
 */
-(NSString *) fetchCreditCardExp;

/*!
@discussion Updates the credit card type. Sets the self.ccExp to exp. Updates the keyChainStore.
@param exp NSString represents the expiry
*/
-(void)updateCreditCardExp: (NSString *) exp;

/*!
 @discussion fetch Credit Card Payment Ref. If @c[ccPaymentRef] is nil fetch from UICKeyChainStore.
 
 @return @c[self.ccPaymentRef] NSString represents the payment ref.
 */
-(NSString *) fetchCreditCardPaymentRef;

/*!
@discussion Updates the CreditCard Payment ref. Sets @c[self.ccPatmentRef] to ref. Updates the KeyChainStore.
 
@param ref NSString Represents the payment ref.
 */
-(void)updateCreditCardPaymentRef: (NSString *) ref;

/*!
 @discussion Removes the credit card information.
 - sets the ccFirstName to nil. Removes from KeyChain.
 - sets the ccLastName to nil. Removes from KeyChain.
 - sets the cardNo to nil. Removes from KeyChain.
 - sets the ccType to nil. Removes from KeyChain.
 - sets the ccExp to nil. Removes from KeyChain.
 - sets the payment ref to nil. Removes from KeyChain.
 - sychronizes the keystore.
 */
-(void)removeCreditCardInfo;

//-(void)updateUserImagePath: (NSData *)imageData;

/*!
 @brief gets the users location.
 
 @discussion fetches the users latitude and longitude from NSUserdefaults. Using this information it creates a CLLocation object.
 
 @return CLLocation - the users location
 
 */
-(CLLocation *)fetchUsersLocation;

/*!
 @brief updates the users location 
 
 @discussion updates the NSUserDefaults for the keys corresponding to their latitude and longitude. The NSUserdefaults is then synchronized.
 
 @param loc - CLLocation object to be used for the NSUserDefaults
 
 */
-(void) updateUsersLocationWithLocation : (CLLocation *) loc;

/*!
 @brief checks whether the user is logged in or not.
 
 @discussion this method checks if @c[[User getInstance] fetchAccessToken]] is nil. If it is nil then the user is logged out. If its present then it means the user is logged in.
 
 @return BOOL representing whether the user is logged in or not.
 */
-(BOOL)isUserLoggedIn;

/*!
 @brief checks whether the user has a credit card attaced to his account or not.
 
 @discussion this method checks if @c[[[User getInstance] fetchCreditCardNo]] is nil. If its nil then it means that the user does not have a card. If it is not nil then it means the the user does have a card attached.
 */
-(BOOL)hasCreditCard;

/*!
 @brief checks whether the user has a first name <b>and</b> a last name.
 
 @discussion checks whether @c[[[User getInstance] fetchFirstName] is nil, or if @c[[[User getInstance] fetchLastName]] is nil. If either are nil, then it returns NO, otherwise is returns YES.
 
 */
-(BOOL)hasFirstNameAndLastName;

/*!
 * @discussion Updates the users balance. Sets the usersBalance variable to bal. Updates the NSUserDefaults for kUSersBalanceKey
 * @param bal NSString representing the new balance.
 */
-(void)updateUsersBalance: (NSString *)bal DEPRECATED_MSG_ATTRIBUTE("The app no longer uses a user balance since the introduction of discount codes");

/*!
 @discussion Updates the users first name. Sets the variable firstName to parameter firstName. It also updates NSUserDefaults for key kFirstNameKey.
 
 @param firstName NSString.
 */
-(void)updateUsersFirstName: (NSString *) firstName;

/*!
 @discussion updates the users last name. Updates NSUserDefaults representing the key for Surname.
 
 @param lastName NSString
 */
-(void)updateUsersLastName: (NSString *) lastName;

/*!
 @brief determines whether there is enought credit to make a booking
 
 @param price - NSString the price of the item
 
 */
-(BOOL)hasEnoughCreditForBooking: (NSString *) price DEPRECATED_MSG_ATTRIBUTE("The app no longer uses a user balance");

/*!
 @brief sets the NSUserDefault to YES for key "inviteCode"
 */
-(void)setInviteCodeToUsed DEPRECATED_MSG_ATTRIBUTE("Used during initial release - no longer used");

/*!
 @brief determines whether the user has used the invite code or not
 */
-(BOOL)hasUserUsedInviteCode;

//-(NSString *)fetchUsersSocialAvatar;

//-(void)updateUsersAvatarURL: (NSString *)users_avatar_url;

/*!
@discussion Sets up the users profile using the NSDictionary fBdict. fbDict is an NSDictionary which contains the returned profile from the server following Facebook Login (Facebook sign up).
 
 - updates the users gender for key kGenderKey. Store in NSUserDefaults.
 - sets up the users email for kEmailKey. Stored in UICKeyChainStore
 - sets up the users key for key s_key. Stored in UICKeyChainStore
 - sets up the users last name for key user_last_name. Stored in UICKeyChainStore
 - sets up the users first name for key kFirstNameKey. Stored in NSUserDefaults
 - updates the users accesstoken
 - updates the users phone number
 - updates the users credit card reference
 - updates the users credit card details
 checks whether the credit card_details count is not 0. If its not then update the users credit card details.
 + updates the card first name
 + updates the card last name
 + updates the card number
 + updates the card type
 + updates the cards expiry
 + updates the users credit card reference
 - checks if the users has validated Twilio
 - updates the users image url.
 - Set boolean to YES for Facebook login
 - Synchronizes NSUserdefaults
 
 
 @param fbDict NSDictionary
 */
-(void)setUpFacebookProfile: (NSDictionary *)fbDict;

/*!
 @brief determines whether Twilio has been show to the user or not. 
 
 @discussion determines whether Twilio has been shown or not by checking NSUserDefaults or not.
 */
-(BOOL)hasShownTwilio;

/*!
 
 @brief marks the NSUserDefault bool to yes for the key kHas_Show_Twilio.
 
 @param yn - BOOL determining whether the book should be marked as true or false.
 
 */
-(void)markTwilioAsShown: (BOOL) yn;

/*!
 @brief determines if Twilio is verified or not
 @discussion checks whether @c[self.isTwilioVerifiedYesNoOrNil] is nil, if it is nil, it fetches it from the NSUserDefaults. Else it returns it the bool value of @c[self.isTwilioVerifiedYesNoOrNil]
 
 @return Bool
 */
-(BOOL)isTwilioVerified;


/*!
 @brief updates the Twilio related values.
 
 @discussion updates the @c[self.isTwilioVerifiedYesNoOrNil] based on the Bool, and updateds the NSUserDefaults for the corresponding key.
 
 @param yn BOOL representing wether Twilio should be verified or not.
 
 
 */
-(void)updateTwilioVerified: (BOOL) yn;


/*!
 @brief sets the image url for the user.
 
 @discussion updates @c[self.usersImageURL] with urlString. And updates the corresponding NSUserDefaults.
 
 @param urlString NSString representing the url.
 */
-(void)setImageURL: (NSString *)urlString;

/*!
@discussion fetches the image url. If @c[self.usersImageURL] is nil fetch from NSUserDefaults.
 
@return NSURL the user for the image.
 */
-(NSURL *)fetchImageURL;

/*!
 @discussion Checks whether the user has an image URL i.e User Avatar
 
 @return bool
 */

-(BOOL)hasImageURL;

/*!
 @brief determines whether the user can make a booking based on the criteria needed for bookings.
 
 @discussion checks if the user is logged in and if the user is Twilio verified and if the user has a credit car.
 If the user has all three then the user is able to make a booking. Otherwise a No bool is returned.
 
 @return BOOL whether the user can make a booking or not
 
 */
-(BOOL)canMakeBooking;

/*!
 @brief returns the button title for the booking button in @c[TabMakeBookingViewController].
 
 @discussion if the user is not logged in, then the returned string informs the user to "Login or Sign Up", else if the user is not twilio verified then the string returned is "Verify Phone Number", else if the user does no have a credit card then the string returned is "Add Payment Info", else the returned string is "Book Now".
 
 @return NSString - represeting the title string for the button.
 
*/
-(NSString *)buttonTitleState;

/*!
 @brief returns a numeric value which can be used to determine which action to take.
 
 @discussion if the user is not logged in then a value of 1 is returned, if the user is not twilio verified then a value of 2 is returned, else if the user doesnt have a credit card then a value of 3 is returned, else a value of 0 is returned.
 
 @return int represent a numeric value which can be used to determin which action to take.
 
 */
-(int)bookingDestinationScreen;

/*!
 @brief returns the bool value for 'isLocalSignUp'
 
 */
-(BOOL)isLocalSignUp DEPRECATED_MSG_ATTRIBUTE("This method was used temporarily");//temp

/*!
 @brief sets the NSUserDefaults for the key 'isLocalSignUp'.
 
 @param yn BOOl value representing the Bool to be save as the Key.
 */
-(void)setLocalSignUp: (BOOL) yn DEPRECATED_MSG_ATTRIBUTE("This method was used temporarily");//temp

/*!
 @brief determines whether the user has credit in their account
 
 @remark - balance is no longer used in the app. There is a greater emphasis on discount codes.
 */
-(BOOL)hasCreditInAccount ;


/*!
 @brief determines what the title of the booking button should read based on the service price.
 
 @param servicePrice - NSString representing the price of the service
 
 @return NSString - the title to be applied to the button
*/
-(NSString *)buttonTitleStateWithServicePrice: (NSString *) servicePrice;

/*!
 @brief returns whether the user has signed up with Facebook
 */
-(BOOL)isSocialSignUp;

/*!
@discussion gets the credit card Image Type
 
@return img UIImage representing the credit card type
 */
-(UIImage *)getCreditCardImageType;

/*!
 @discussion updates @c[self.key] and updates the UICKeyChainStore
 
 @param key - NSString representing the key
 */
-(void)updateKey: (NSString *) key;

/*!
 @discussion fetches the key. If key is equal to nil fetch from UICKeyChainStore.
 
 @return NSString represents the key @c[self.key]
 */

-(NSString *)fetchKey;

/*!
 @brief makes a network request to get the users profile
 
 @discussion a network request is made to get the users profile. The @c[profileManager] delegate methods handles the information returned.
 */
-(void)updateProfile;

/*!
 @brief Grabs the users chosen currency symbol.
 
 @return NSString representing the users currency.
 */
-(NSString *) fetchCurrencySymbol;


/*!
 @bief conditional statement that checks whether the user has a card or not.
 
 @discussion checks if @[self.hasCardYesNoOrNil] is nil. If it is @c[self.hasCardYesNoOrNil] is assigned the @c][NSNumber numberWithBool: [self hasCreditCard]]].
 
 @return BOOL representing whether the user has a card or not.
 */
-(BOOL)doesUserHaveCard;

/*!
 @brief fetch the string number of favourites experiences
 
 @return NSString noOfExpFav - the number of experiences favourited
 */
-(NSString *)fetchStringNumberOfFavouriteExperiences;


/*!
 @brief fetch the string number of favourited salons
 
 @return NSString noOfSalonsFav representing the number of salons favourited
 */
-(NSString *)fetchStringNumberOfFavouriteSalons;

/*!
 @brief Updates the users currency
 @param cur CurrencyItem
 */
-(void)updateUsersCurrency: (CurrencyItem *) cur;

/*!
 @brief update whether the user has a card attached or not.
 
 @discussion  if @c[self.hasCardYesNoOrNil] is nill, then assign it the value from @c[self hasCreditCard].
 
 @param card NSString string representing whether the user needs a card or not.
 */
-(void)updateDoesUserHaveCard: (NSString *) card;

/*!
 @brief updates whether the user is banned or not.
 
 @discussion converts the JSON string to a boolValue and assigns it to @c[self.isBannedYesNoOrNil].
 
 @param banned - NSString that represents whether the users account it banned or not.
 */
-(void)updateIsUserBanned: (NSString *) banned;

/*!
@brief updates whether the user needs to log out or not.
 
@discussion converts the JSON string to a boolValue and assigns it to @c[self.needsLogOutYesNoOrNil].
 
@param logout JSON string representing whether the user needs to log out or not.
 
*/
-(void)updateUserNeedsToLogOut: (NSString *) logout;

/*!
 @brief returns whether the user needs to log out or not
 @return bool whether the user needs to log out or not
 */
-(BOOL)needsToLogOut;

/*!
 @brief Update the number of favourites Experiences
 @param no NSString representing the number of favourited Experienced items. Uses a string as it will be represented later as a string
 */
-(void)updateNumberOfExperiencesFavourite: (NSString *)no;

/*!
 @brief Update the number of salon favourites
 @param no NSString representing th enumber of salons favourited
 */
-(void)updateNumberOfSalonFavourites : (NSString *) no;


/*!
 @brief fetches the users city and country name
 
 @return NSString returns the users City And Country string variable
 
 */
-(NSString *) fetchUsersCityAndCountry;


/*!
 @brief updates the users city and country variable/
 @param cityCountry NSString representing the users city and country.
 */
-(void)updateUsersCityAndCountry: (NSString *) cityCountry;

/*!
 @brief determines if the user has a city and a country.
 
 @return BOOL represents whether @c[self.usersCityAndCountry] lenght is greater than 0.
 
 */
-(BOOL)hasUsersCityAndCounty;

/*!
 @brief fetchs a CLLocation with a latitude and longitude of Fisheries Tower, Wolfe Tone Bridge, Galway, Ireland. This is used to simulate locations where there are no WhatSalon salons.
 @return CLLocation object representing the latitude and longitude of Fisheries Tower, Wolfe Tone Bridge, Galway, Ireland.
 */
-(CLLocation *) fetchFarAwayLocation;

/*!
 @brief cancels the update of the profile
 @discussion determines if @c[self.isFetchingProfile] is true, if so, it cancels the task and sets @c[self.isFetchingProfile] to false.
 */
-(void)cancelUpdate;

/*!
 @brief if the user is not logged in then updated the guest users location.
 @param gl CLLocation representing the guest users location
 */
-(void)updateGuestUsersLocation: (CLLocation *) gl;

/*
 @brief fetchs the guest users location 
 @param CLLocation returns the guest users location
 */
-(CLLocation *)fetchGuestUsersLocation;
@end
