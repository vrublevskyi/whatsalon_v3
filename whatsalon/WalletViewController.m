//
//  WalletViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 25/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "WalletViewController.h"
#import "WalletTableViewCell.h"
#import "CreditCardDetailsViewController.h"
#import "FUIAlertView.h"
#import "GCNetworkManager.h"
#import "SocialShareViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "ReedemCodeViewController.h"
#import "EmptyWalletView.h"
#import "whatsalon-Swift.h"

/*
 Updated November 18th - removed code that would show RedeemCode
 no longer using redeem codes for next version
 
 */
@interface WalletViewController ()<UITableViewDataSource,UITableViewDelegate,CreditCardDetailsVCDelegate,WalletTableViewDelegate,GCNetworkManagerDelegate,UIViewControllerTransitioningDelegate, EmptyWalletViewDelegate>


/*! @brief represents the cancel action. */
- (IBAction)cancel:(id)sender;
    @property (weak, nonatomic) IBOutlet UIButton *addNewCardButton;
    
/*! @brief represents the network manager for making network requests. */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief represents the redeem code string. */
@property (nonatomic) NSString * redeemCode;
@end

@implementation WalletViewController


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore flatNavBarOnView:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor=[UIColor whiteColor];
    self.tableview.delegate=self;
    self.tableview.dataSource=self;

    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    footerView.backgroundColor=[UIColor clearColor];
    self.tableview.tableFooterView=footerView;
    
    self.headerView.backgroundColor=kBackgroundColor;
    
    self.title=@"Wallet";
    
   

    self.tableview.backgroundColor = [UIColor clearColor];
    
    [self.tableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    self.tableview.scrollEnabled=NO;

    [Gradients addPinkToPurpleHorizontalLayerWithView:_addNewCardButton];

}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",jsonDictionary[kMessageIndex]] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    [[User getInstance] updateUsersBalance:jsonDictionary[@"message"][@"balance"]];
   
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:jsonDictionary[@"message"][@"message"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    NSLog(@"%@",jsonDictionary[@"message"][@"message"]);
    [alert show];
    
    [self.tableview reloadData];
    
}
-(void)submitRedeemCode{
    
    [self.view endEditing:YES];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kRedeem_Code_URL]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken ]];params = [params stringByAppendingString:[NSString stringWithFormat:@"&discount_code=%@",self.redeemCode]];
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}



/*
 Update November 18th - no longer being used whilst redeem code has been removed.
 */
-(void)didTriggerActionWithType:(BOOL)type{
    if (type) {
        NSLog(@"Share Code");
        
        SocialShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"socialVC"];
        shareVC.modalPresentationStyle=UIModalPresentationCustom;
        shareVC.transitioningDelegate=self;
        shareVC.messageToShare= [NSString stringWithFormat:@"%@ invited you to join WhatSalon. You get <Price goes here> if you sign up and enter code <Discount Code goes here>",[[User getInstance] fetchFirstName]];
        shareVC.headerText = @"When a friend signs up for WhatSalon with your referal code, you'll both get <Money Goes here> when they book an appointment";
        [self presentViewController:shareVC animated:YES completion:nil];

    }
    else{
        NSLog(@"Redeem Code");
        NSLog(@"Reede Code");
        
        /*
        FUIAlertView *alertView = [[FUIAlertView alloc] initWithTitle:@"Redeem A Code"
                                                              message:@"Lucky enough to have a redeem code, enter it below and see what you get!!"
                                                             delegate:nil cancelButtonTitle:@"Cancel"
                                                    otherButtonTitles: @"Submit",nil];
        alertView.alertViewStyle=FUIAlertViewStylePlainTextInput;
        alertView.titleLabel.textColor = [UIColor cloudsColor];
        //alertView.titleLabel.font = [UIFont boldFlatFontOfSize:16];
        alertView.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        alertView.messageLabel.textColor = [UIColor cloudsColor];
        alertView.messageLabel.font = [UIFont systemFontOfSize:14.0];
        //alertView.messageLabel.font = [UIFont flatFontOfSize:14];
        alertView.backgroundOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        alertView.alertContainer.backgroundColor = kWhatSalonBlue;
        alertView.defaultButtonColor = [UIColor cloudsColor];
        alertView.defaultButtonShadowColor = [UIColor asbestosColor];
        alertView.defaultButtonShadowHeight=kShadowHeight;
        //alertView.defaultButtonFont = [UIFont boldFlatFontOfSize:16];
        alertView.defaultButtonTitleColor = [UIColor asbestosColor];
       [[alertView textFieldAtIndex:0] setPlaceholder:@"Redeem Code"];
        [[alertView textFieldAtIndex:0] setBackgroundColor:[UIColor cloudsColor]];
        [alertView textFieldAtIndex:0].layer.cornerRadius=kCornerRadius;
        [alertView textFieldAtIndex:0].autocapitalizationType=UITextAutocapitalizationTypeAllCharacters;;
        UITextField * text = [alertView textFieldAtIndex:0];
        
        alertView.onOkAction=^{
        
            self.redeemCode = text.text;
            
            [self submitRedeemCode];
        };
        [alertView show];
         
         */
        /*
        ReedemCodeViewController * rVC = [self.storyboard instantiateViewControllerWithIdentifier:@"redeemVC"];
        [self.navigationController pushViewController:rVC animated:YES];
         */
    }
}


-(UITableViewCell *)creditCardTableViewCellForIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView{
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if ([[User getInstance] hasCreditCard]) {
        cell.textLabel.text = [NSString stringWithFormat:@" %@ %@",[[User getInstance] getCreditCardImageType],[[User getInstance] fetchCreditCardNo]];
        cell.textLabel.textColor = [UIColor lightGrayColor];
        NSLog(@"Table view height %f",self.tableview.rowHeight);
        
    }
    else{
        cell.textLabel.text=@"-";
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.numberOfLines=0;
    cell.layer.masksToBounds=YES;
    return cell;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * cell=[self creditCardTableViewCellForIndexPath:indexPath tableView:tableView];
    cell.layer.masksToBounds=YES;
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
 
    if ([[User getInstance] hasCreditCard]) {
        return 60;
    }
    return 100;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
    if ([[User getInstance] hasCreditCard]) {
        
        return 1;
    }
    
        return 0;
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        if ([[User getInstance] hasCreditCard]) {
            return 40;
        }
        return 80;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
        
    [self goToCreditCardScreen];
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    
    if (![[User getInstance] hasCreditCard] && [[User getInstance] isUserLoggedIn]) {
        _addNewCardButton.hidden = true;
        EmptyWalletView * emptyView = [[EmptyWalletView alloc] initWithFrame:CGRectMake(0, 0, self.tableview.frame.size.width, self.tableview.frame.size.height)];
        emptyView.delegate = self;
        [tableView addSubview:emptyView];
        
    }
    
    [view setBackgroundColor:kBackgroundColor];
    return view;
}

- (void)goToCreditCardScreen {
    CreditCardDetailsViewController *ccVc = [self.storyboard instantiateViewControllerWithIdentifier:@"creditCardDetailsVC"];
    ccVc.delegate=self;
    [self.navigationController pushViewController:ccVc animated:YES];
}

-(void)addCardScreen{
    
    [self goToCreditCardScreen];
}

#pragma mark - CreditCardDelegate
-(void)didAddCard{
    [self.tableview reloadData];
}

- (void) onAddCardTapped: (EmptyWalletView *) sender {
    [self addCardScreen];
}

- (IBAction)addNewCard:(id)sender {
    [self addCardScreen];
}
    
- (IBAction)cancel:(id)sender {
    
    if (!self.hasRightMenu) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"WalletNotification"
         object:self];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
