//
//  ReedemCodeViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 09/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReedemCodeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *warningMessage;
@property (weak, nonatomic) IBOutlet UILabel *subWarningMessage;

@end
