//
//  ResultsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 16/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"

@interface ResultsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewForNavBar;
@property (weak, nonatomic) IBOutlet UIView *viewForAlternativeTimes;
@property (weak, nonatomic) IBOutlet UITableView *tableViewForAlternativeTimes;
@property (weak, nonatomic) IBOutlet UIView *headerViewTableView;
@property (nonatomic) NSMutableArray * jobsArray;
@property (nonatomic) NSMutableArray * altJobsArray;
@property (nonatomic) Salon * salonData;
@property (nonatomic) NSString * searchID;
- (IBAction)cancel:(id)sender;

@end
