//
//  TherapistTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 07/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TherapistTableViewCell.h"
#import "User.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "User.h"
#import "UIImage+ColoredImage.h"
#import "UIColor+FlatUI.h"

@implementation TherapistTableViewCell

- (void)awakeFromNib {
    // Initialization code
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.therapistBackgroundImage = [[UIImageView alloc] initWithFrame:self.frame];
        self.therapistBackgroundImage.image = [UIImage imageNamed:@"Therapie_Cork_1.JPG"];
        
        self.therapistBackgroundImage.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.therapistBackgroundImage.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:self.therapistBackgroundImage];
        
       
        
        UIImageView * bubbleImage = [[UIImageView alloc] initWithFrame:CGRectMake(-10, CellHeight-60, self.frame.size.width+20, 65)];
        bubbleImage.image = [UIImage imageNamed:@"top_bubble_gray_glow"];
        [self.contentView addSubview:bubbleImage];
        
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, bubbleImage.frame.origin.y, self.frame.size.width-10, bubbleImage.frame.size.height)];
        self.timeLabel.text = @"Sarah at 3pm";
        self.timeLabel.textAlignment=NSTextAlignmentCenter;
        [self.contentView addSubview:self.timeLabel];
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, bubbleImage.frame.origin.y, self.frame.size.width, bubbleImage.frame.size.height)];
        self.nameLabel.text = @"name";
        self.nameLabel.textAlignment=NSTextAlignmentCenter;
        //[self.contentView addSubview:self.nameLabel];
        
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, bubbleImage.frame.origin.y, self.frame.size.width-10, bubbleImage.frame.size.height)];
        self.priceLabel.text = @"name";
        self.priceLabel.textAlignment=NSTextAlignmentRight;
        //[self.contentView addSubview:self.priceLabel];
        
        self.favImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-35, 10, 24, 23)];
        self.favImageView.image = [UIImage imageNamed:@"info_icon"];
        self.favImageView.layer.cornerRadius=self.favImageView.frame.size.width/2.0f;
        self.favImageView.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.4];
        self.favImageView.image = [self.favImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.favImageView setTintColor:[UIColor cloudsColor]];
        self.favImageView.contentMode=UIViewContentModeScaleAspectFit;
        
        self.selectFavLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-60, 0, 60, 60)];
        self.selectFavLabel.backgroundColor=[UIColor clearColor];
        self.selectFavLabel.userInteractionEnabled=YES;

        [self.contentView addSubview:self.favImageView];
        [self.contentView addSubview:self.selectFavLabel];
        
        UITapGestureRecognizer * tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
        tapPress.numberOfTapsRequired=1;
        tapPress.delegate=self;
        tapPress.delaysTouchesBegan=YES;
        tapPress.cancelsTouchesInView=YES;
        [self.selectFavLabel addGestureRecognizer:tapPress];

        
        UILabel * seperator = [[UILabel alloc] initWithFrame:CGRectMake(0, CellHeight-1.0,self.frame.size.width,1.0)];
        seperator.backgroundColor = [UIColor blackColor];
        [self.contentView addSubview:seperator];
        [self.contentView bringSubviewToFront:seperator];
        self.clipsToBounds=YES;
        
        
        UIView * holderView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width-215, CellHeight-bubbleImage.frame.size.height-25, 205, 30)];
        holderView.backgroundColor=kWhatSalonBlue;
        holderView.layer.cornerRadius=kCornerRadius;
        holderView.clipsToBounds=YES;
        [self.contentView addSubview:holderView];
        
        UITapGestureRecognizer * tapHolderView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showLegendView)];
        tapHolderView.numberOfTapsRequired=1;
        tapHolderView.delegate=self;
        tapHolderView.delaysTouchesBegan=YES;
        tapHolderView.cancelsTouchesInView=YES;
        [holderView addGestureRecognizer:tapHolderView];
        
        UILabel * hygieneLabel = [[UILabel alloc] initWithFrame:CGRectMake(holderView.frame.size.width-45, 0, 40, holderView.frame.size.height)];
        hygieneLabel.text=@"100%";
        hygieneLabel.adjustsFontSizeToFitWidth=YES;
        hygieneLabel.textColor=[UIColor cloudsColor];
        hygieneLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        hygieneLabel.textAlignment=NSTextAlignmentCenter;
        [holderView addSubview:hygieneLabel];
        UIImageView *hygieneImageView = [[UIImageView alloc] initWithFrame:CGRectMake(hygieneLabel.frame.origin.x-holderView.frame.size.height+5, 5, holderView.frame.size.height-5, holderView.frame.size.height-10)];
        hygieneImageView.image=[UIImage imageNamed:@"hygiene_icon"];
        hygieneImageView.image = [hygieneImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [hygieneImageView setTintColor:[UIColor cloudsColor]];
        
        [holderView addSubview:hygieneImageView];
        
        UILabel * timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(hygieneImageView.frame.origin.x-40, 0, 40, holderView.frame.size.height)];
        timeLabel.text=@"90%";
        timeLabel.adjustsFontSizeToFitWidth=YES;
        timeLabel.textColor=[UIColor cloudsColor];
        timeLabel.textAlignment=NSTextAlignmentCenter;
        timeLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [holderView addSubview:timeLabel];
        UIImageView *timeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(timeLabel.frame.origin.x-holderView.frame.size.height+5, 5, holderView.frame.size.height-5, holderView.frame.size.height-10)];
        timeImageView.contentMode=UIViewContentModeScaleAspectFit;
        timeImageView.image=[UIImage imageNamed:@"timing_icon"];
        timeImageView.image = [timeImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [timeImageView setTintColor:[UIColor cloudsColor]];
        
        [holderView addSubview:timeImageView];
        
        UILabel * ratingLabel = [[UILabel alloc] initWithFrame:CGRectMake(timeImageView.frame.origin.x-holderView.frame.size.height-15, 0, 40, holderView.frame.size.height)];
        ratingLabel.text=@"90%";
        ratingLabel.adjustsFontSizeToFitWidth=YES;
        ratingLabel.textColor=[UIColor cloudsColor];
        ratingLabel.textAlignment=NSTextAlignmentRight;
        ratingLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [holderView addSubview:ratingLabel];
        UIImageView *ratingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ratingLabel.frame.origin.x-holderView.frame.size.height+10, 5, holderView.frame.size.height-10, holderView.frame.size.height-10)];
        ratingImageView.image=[UIImage imageNamed:@"star_icon"];
        ratingImageView.image = [ratingImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [ratingImageView setTintColor:[UIColor cloudsColor]];
        
        [holderView addSubview:ratingImageView];
        
        

       
    }
    
    return self;
}

-(void) showLegendView{
    
    NSLog(@"show legend");
    if ([self.delegate respondsToSelector:@selector(showLegend)]) {
        
        [self.delegate showLegend];
    }
}
-(void)tapPress:(UIGestureRecognizer*) tap{
    NSLog(@"Pressed");
    //self.favImageView.image = [UIImage imageNamed:@"fav_heart"];
    if ([self.delegate respondsToSelector:@selector(showBio:)]) {
        
        [self.delegate showBio:self.nameLabel.text];
    }
}
-(void)setCellImageWithURL: (NSString*) url{
    
    if ([WSCore isDummyMode]) {
        self.therapistBackgroundImage.image=[UIImage imageNamed:url];
    }
    else{
        if (url.length <1) {
            self.therapistBackgroundImage.image = kPlace_Holder_Image;
            self.therapistBackgroundImage
            .contentMode=UIViewContentModeScaleAspectFill;
            
            
        }
        else{
            [self.therapistBackgroundImage sd_setImageWithURL:[NSURL URLWithString:url]
                               placeholderImage:kPlace_Holder_Image];
        }
    }

    
}


@end
