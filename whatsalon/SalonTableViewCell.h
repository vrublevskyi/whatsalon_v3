//
//  SalonTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 12/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalonTableViewCell : UITableViewCell <UIScrollViewDelegate>

@property (nonatomic) UIImageView * mainImage;
@property (nonatomic) UIView * infoView;
@property (nonatomic) UILabel * salonName;
@property (nonatomic) UILabel * salonAddress;
@property (nonatomic) UILabel * salonMiles;
@property (nonatomic) UIImageView * favLabel;
@property (nonatomic) UILabel * selectFavLabel;

@property (nonatomic) UIScrollView * scrollView;
@property (nonatomic) UIPageControl * pageControl;

@property (nonatomic) NSArray * pageImages;//This will hold all the images to display – 1 per page.
@property (nonatomic) NSMutableArray * pageViews;/*
                                                  This will hold instances of UIImageView to display each image on its respective page. It’s a mutable array, because you’ll be loading the pages lazily (i.e. as and when you need them) so you need to be able to insert and delete from the array.
                                                  */

-(void)loadVisiblePages;
-(void)loadPage: (NSInteger)page;
-(void)purgePage: (NSInteger)page;


-(void)setCellImageWithURL: (NSString *)url;

-(void)setScrollSize;
-(void)addPageImagesAndCreatePageViews: (NSArray *)images;
@end
