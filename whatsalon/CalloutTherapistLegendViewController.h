//
//  CalloutTherapistLegendViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 17/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalloutTherapistLegendViewController : UIViewController
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImage;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *hygieneImageView;
@property (weak, nonatomic) IBOutlet UILabel *hygieneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *timingImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeKeepingLabel;

@end
