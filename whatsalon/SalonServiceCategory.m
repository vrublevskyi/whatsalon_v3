//
//  SalonServiceCategory.m
//  whatsalon
//
//  Created by Graham Connolly on 15/10/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//

#import "SalonServiceCategory.h"

@implementation SalonServiceCategory



-(instancetype) initWithServiceType:(ServiceType)s_t{
    
    self=[super init];
    if (self) {
        self.serviceType = s_t;
        //NSLog(@"Service type %lu",(unsigned long)s_t);
        switch (self.serviceType) {
            case ServiceTypeHair:
                self.serviceImage=[UIImage imageNamed:@"hair_service_icon"];

                break;
            case ServiceTypeNails:
                self.serviceImage=[UIImage imageNamed:@"nail_service_icon"];
                
                break;
            case ServiceTypeMassage:
                self.serviceImage=[UIImage imageNamed:@"massage_service_icon"];
                
                break;
            case ServiceTypeHairRemoval:
                self.serviceImage=[UIImage imageNamed:@"wax_service_icon"];
                
                break;
            case ServiceTypeFace:
                self.serviceImage=[UIImage imageNamed:@"face_service_icon"];
                
                break;
            case ServiceTypeBody:
                self.serviceImage=[UIImage imageNamed:@"body_service_icon"];
                
                break;

            
                
            default:
                self.serviceImage=[UIImage imageNamed:@"hair_service_icon"];;
                break;
        }
        
    }
    return self;
}

+(instancetype) serviceWithServiceType:(ServiceType)s_t{
    
    return [[self alloc] initWithServiceType:s_t];

}


-(ServiceType) fetchServiceType{
    return self.serviceType;
}
-(UIImage *) fetchServiceImage{
    return self.serviceImage;
}
@end
