//
//  InboxDetailViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 03/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Inbox.h"

@interface InboxContentViewController : UIViewController
/*! @brief represents the Inbox object. */
//@property (nonatomic) Inbox * inboxItem;
@end
