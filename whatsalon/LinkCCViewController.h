//
//  LinkCCViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 22/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"

@protocol LinkCCDelegate <NSObject>

@optional
-(void)didAddCard;
-(void)didSkipCard;

@end
@interface LinkCCViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic,assign) id<LinkCCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *viewForCCNumber;
@property (weak, nonatomic) IBOutlet UIView *viewForFirstName;
@property (weak, nonatomic) IBOutlet UIView *viewForExpiration;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelBarButton;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *cVVTextField;
@property (weak, nonatomic) IBOutlet FUIButton *linkButton;
@property (nonatomic,assign) BOOL onlyAddCard;

//last min
@property (nonatomic) BOOL isFromLastMin;
- (IBAction)cancel:(id)sender;
- (IBAction)linkCCCard:(id)sender;
- (IBAction)skipCCEntry:(id)sender;

@property (nonatomic) BOOL isFromSettings;
@property (nonatomic) BOOL isFromLocalSignUpBookingScreen;
@end
