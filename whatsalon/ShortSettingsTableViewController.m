//
//  ShortSettingsTableViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 27/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "ShortSettingsTableViewController.h"
#import "RESideMenu.h"
#import "WhatSalonDotComViewController.h"
#import "AboutMenuViewController.h"

@interface ShortSettingsTableViewController ()



@end

@implementation ShortSettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [WSCore statusBarColor:StatusBarColorBlack
     ];
    
    
    if (self.navigationController.navigationBar.tintColor ==[UIColor whiteColor]) {
        [WSCore nonTransparentNavigationBarOnView:self];
        self.navigationController.navigationBar.tintColor=self.view.tintColor;
    }
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = YES;
    
  
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    if (indexPath.row ==0) {
        cell.textLabel.text = [NSString stringWithFormat:@"Version %@",kVersionNumber];
        //cell.detailTextLabel.text = kVersionNumber;
        cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
        [WSCore addTopLine:cell :[UIColor lightGrayColor] :0.5];
    }
   
    if (indexPath.row ==1) {
        cell.textLabel.text = @"About";
        cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    if (indexPath.row ==2) {
        cell.textLabel.text = @"WhatSalon.com";
        cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [WSCore addBottomLine:cell :[UIColor lightGrayColor]];
    }
    

    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(15, 26, 180, 21);
    myLabel.backgroundColor = [UIColor clearColor];
    myLabel.font = [UIFont systemFontOfSize: 17.0f];
    myLabel.textColor = [UIColor darkGrayColor];
    myLabel.text = @"WhatSalon";
    
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:245.0/255 green:245.0/255 blue:247.0/255 alpha:1.0];
    [headerView addSubview:myLabel];
    
    return headerView;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        //nothing
    }
    if (indexPath.row==1) {
        //go to about
        AboutMenuViewController *about = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutVC"];
        [self.navigationController pushViewController:about animated:YES];
    }
    if (indexPath.row==2) {
        //go to whatsalon
        
        WhatSalonDotComViewController * website = [self.storyboard instantiateViewControllerWithIdentifier:@"websiteVC"];
        website.pageTitle=@"WhatSalon.com";
        website.urlString=kWhatSalonWebsite;
        [self.navigationController pushViewController:website animated:YES];
    }
    
   
}
- (IBAction)showMenu:(id)sender {
    
    [self.sideMenuViewController presentMenuViewController];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 55;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
/*
- (IBAction)debugOnOff:(id)sender {
    UISwitch * switchOnOff = (UISwitch *)sender;
    if (switchOnOff.on ==NO) {
        NSLog(@"Turn off");
        [WSCore setLiveEnvironment:YES];
    }else{
        NSLog(@"Turn on");
        [WSCore setLiveEnvironment:NO];
    }
}
 */
@end
