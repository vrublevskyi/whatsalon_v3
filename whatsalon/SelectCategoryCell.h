//
//  SelectCategoryCell.h
//  whatsalon
//
//  Created by admin on 9/30/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectCategoryCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

@end

NS_ASSUME_NONNULL_END
