//
//  DiscoveryTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 11/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "DiscoveryTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ColoredImage.h"

@implementation DiscoveryTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.layer.masksToBounds=YES;
        self.titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.titleLabel];
        self.icon = [[UIImageView alloc] init];
        [self.contentView addSubview:self.icon];
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        self.clipsToBounds=YES;
    }
    return self;

}
- (void)awakeFromNib {
    // Initialization code
}



-(void)setUpDiscoveryCell:(DiscoveryItem *) disc AndIsSalonsNearMe : (BOOL) yn{
    
    CGRect frame=CGRectMake(0, 0, self.contentView.frame.size.width, 60);
    if (disc.title.length>18) {
    
        frame=CGRectMake(0, 0, self.contentView.frame.size.width, 100);
    }

    
    
    self.titleLabel.frame = frame;
    
    if (self.backgroundImage==nil) {
        self.backgroundImage = [[UIImageView alloc] init];
        self.backgroundImage.clipsToBounds=YES;
        [self.contentView addSubview:self.backgroundImage];
        
        CGRect imageFrame;
        if (!IS_IOS_8_OR_LATER) {
            
            imageFrame = CGRectMake(0, 0, self.contentView.frame.size.width, 370);
        }else{
            imageFrame = CGRectMake(0, 0, self.contentView.frame.size.width , self.contentView.frame.size.height - 5);
        }
        
        self.backgroundImage.frame =imageFrame;
        self.tintView = [[UIView alloc] initWithFrame:self.backgroundImage.frame];
        self.tintView.backgroundColor=kGoldenTintForOverView;
        [self.backgroundImage addSubview:self.tintView];
        [self.tintView addSubview:self.titleLabel];
        
        CGRect imageRect = CGRectMake(0, 0, 64, 54);
        self.icon.frame=imageRect;

    }
    self.titleLabel.textColor=[UIColor cloudsColor];
    self.titleLabel.textAlignment=NSTextAlignmentCenter;
    self.titleLabel.font = [UIFont systemFontOfSize:16];
    self.titleLabel.text = disc.title;
    self.titleLabel.numberOfLines=0;

    self.titleLabel.text = self.titleLabel.text;
    [self.contentView bringSubviewToFront:self.titleLabel];
    
    [self.backgroundImage sd_setImageWithURL:[NSURL URLWithString:disc.imageURL]
                                placeholderImage:kPlace_Holder_Image];
    
 
    self.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;

    if (IS_IOS_8_OR_LATER) {
        self.titleLabel.center = CGPointMake(self.contentView.center.x, self.contentView.center.y+20);
    }else{
        self.titleLabel.center = CGPointMake(self.frame.size.width/2, 370/2);
    }
    
  /*
   set the icon image based of the discovery type (disc.type)
   */
    UIImage * iconImage;
    switch ([disc.type intValue]) {
        case 0:
            iconImage = [UIImage imageNamed:@"instant_booking_filled_discovery"];
            break;
        case 1:
             iconImage = [UIImage imageNamed:@"heart_filled_discovery"];
            break;
        case 2:
            iconImage = [UIImage imageNamed:@"compass_filled_discovery"];
            break;
        case 3:
            iconImage=[UIImage imageNamed:@"clock_filled_discovery"];
            break;
        case 4:
            iconImage=[UIImage imageNamed:@"experiences_discovery_filled"];
            break;
        case 5:
            iconImage = [UIImage imageNamed:@"map_pin"];
            break;
        case 6:
            iconImage = [UIImage imageNamed:@"jewel_discovery_filled"];
            break;
        case  11:
            iconImage = [UIImage imageNamed:@"hair_service_icon"];
            break;
        case 22:
           iconImage = [UIImage imageNamed:@"wax_service_icon"];
            break;
        case 33:
           iconImage = [UIImage imageNamed:@"nail_service_icon"];
            break;
        case 44:
            iconImage = [UIImage imageNamed:@"massage_service_icon"];
            break;
        case 55:
            iconImage = [UIImage imageNamed:@"face_service_icon"];
            break;
        case 66:
            iconImage = [UIImage imageNamed:@"body_service_icon"];
            break;
            
        default:
        iconImage = [UIImage imageNamed:@"salons_nearby_discovery_filled"];
            break;
    }
    
    self.icon.image = iconImage;
    self.icon.contentMode = UIViewContentModeScaleAspectFit;
    
    
    if (IS_IOS_8_OR_LATER) {
        
        
        if (disc.title.length>18) {
            self.icon.center = CGPointMake(self.contentView.center.x, self.contentView.center.y-40);
        }
        else{
            self.icon.center = CGPointMake(self.contentView.center.x, self.contentView.center.y-20);
        }
  
    }
    else{
        self.icon.center=CGPointMake(160, 140);
    }
    
   
    [self.contentView bringSubviewToFront:self.icon];
    
    self.icon.image = [self.icon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.icon.tintColor = [UIColor cloudsColor];
    
    
}

-(void)setSelected:(BOOL)selected{
    [super setSelected:selected];
}
@end
