//
//  DismissDetailTransition.h
//  whatsalon
//
//  Created by Graham Connolly on 08/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//


/*!
 @header DismissDetailTransition.h
  
 @brief This is the header file for DismissDetailTransition.
  
 This file for a fade transition.
  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd.
 @version    -
 */
#import <Foundation/Foundation.h>

@interface DismissDetailTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end
