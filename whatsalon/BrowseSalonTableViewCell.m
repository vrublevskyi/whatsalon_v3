//
//  BrowseSalonTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 20/11/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//

#import "BrowseSalonTableViewCell.h"
#import "Salon.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ColoredImage.h"
#import "UIColor+FlatUI.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "NSString+Icons.h"
#import "UIFont+FlatUI.h"
#import "whatsalon-Swift.h"

@interface BrowseSalonTableViewCell ()

/*! @brief a view that is used for the touchable area when selecting @c[heartImageView]. */
@property (nonatomic) UIView * selectFavView;

/*! @brief represents the current salon. */
@property (nonatomic) Salon * salon;

/*! @brief represents the GCNetworkManager */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief whether the salon is favourited */
@property (nonatomic) BOOL isFav;
@property (weak, nonatomic) IBOutlet UIView *gradientView;

/*! @brief the label for the salon name. */
@property (weak, nonatomic) IBOutlet UILabel *salonNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *salonAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UIImageView *heartImageView;
@property (weak, nonatomic) IBOutlet UILabel *instantBookLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;

/*! @brief the label for the distance label. */

/*! @brief the label for the instant tag. */
@end


@implementation BrowseSalonTableViewCell


/*
 favouriteButtonState
 updates the appearance of the 'heart' icon
 */
- (void)favouriteButtonState {
    if (self.isFav) {
        
        if ([self.myDelegate respondsToSelector:@selector(didUnFavourite:)]) {
            [self.myDelegate didUnFavourite:self.salon];
        }
        
        self.isFav=NO;
        self.heartImageView.image = [UIImage imageNamed:@"unselectedFavLogo"];
        
        
    }
    else{
        self.isFav=YES;
        self.heartImageView.image = [UIImage imageNamed:@"selectedFavLogo"];
        
    
        
        if ([self.myDelegate respondsToSelector:@selector(didAddFavourite:)]) {
            [self.myDelegate didAddFavourite:self.salon];
        }
        
        
    }
}

#pragma mark - GCNetwork Delegate
/*
 handleFailureMessageWithDictionary
 handles the failure of favouriting a salon
 if 'This user already favourited this salon' then call favouriteButtonState
 call enableFavLabel
 */
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    
    
    if ([jsonDictionary[@"message"] isEqualToString:@"This user already favourited this salon"]) {
        [self favouriteButtonState];
        
    }
    [self enableFavLabel];
    
    
}

/*
 handleSuccessMessageWithDictionary
 
 calls favouriteButtonState
 adjusts heartImageView contentMode
 calls enableFavLabel
 
 */
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    [self favouriteButtonState];
    
    self.heartImageView.contentMode = UIViewContentModeScaleToFill;
    [self enableFavLabel];
    
}

/*
 setCellImageWithUrl
 if url is greater than 1
    then there is a url present
    use the default image
 else
    set the background image using the url passed in
 */
-(void)setCellImageWithURL: (NSString*) url{
        if (url.length <1) {
            self.backGroundImageView.image = kPlace_Holder_Image;
        }
        else{
            [self.backGroundImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:kPlace_Holder_Image];
        }
}

-(void)setUpCellWithSalon: (id) s{

    if([s isKindOfClass:[Salon class]]){
        self.salon = (Salon *)s;
    }
    else{
        return;
    }
    
    CGSize size = self.contentView.bounds.size;
   
    
        self.selectFavView = [[UIView alloc] initWithFrame:CGRectMake(size.width-60, 0, 60, 60)];
        [self.contentView addSubview:self.selectFavView];
        [self.contentView bringSubviewToFront:self.selectFavView];
        
       
        self.selectFavView.userInteractionEnabled=YES;
        
        UITapGestureRecognizer * tapPress = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
        tapPress.numberOfTapsRequired=1;
        tapPress.delegate=self;
        tapPress.delaysTouchesBegan=YES;
        tapPress.cancelsTouchesInView=YES;
        [self.selectFavView addGestureRecognizer:tapPress];

    if (self.salon.is_favourite) {
        self.heartImageView.image = [UIImage imageNamed:@"selectedFavLogo"];
        self.isFav = YES;
        

    }
    else{
        self.heartImageView.image = [UIImage imageNamed:@"unselectedFavLogo"];
        self.isFav=NO;
    }

    /*
     Salon address formatter
     */
    if (![self.salon.salon_address_1 isEqual:[NSNull null]]) {
        if (self.salon.salon_address_1.length!=0)
            self.salonAddressLabel.text = [self.salonAddressLabel.text stringByAppendingString:[NSString stringWithFormat:@"%@",self.salon.salon_address_1]];
    } else
    if (![self.salon.salon_address_2 isEqual:[NSNull null]] && ![self.salon.salon_address_1 isEqualToString:self.salon.salon_address_2]) {
        if (self.salon.salon_address_2.length!=0)
            self.salonAddressLabel.text = [self.salonAddressLabel.text stringByAppendingString:[NSString stringWithFormat:@", %@",self.salon.salon_address_2]];
    }
    if (![self.salon.salon_address_3 isEqual:[NSNull null]] && (self.salon.salon_address_3 != self.salon.city_name)) {
        if (self.salon.salon_address_3.length!=0)
            self.salonAddressLabel.text = [self.salonAddressLabel.text stringByAppendingString:[NSString stringWithFormat:@", %@",self.salon.salon_address_3]];
    }
    if (![self.salon.city_name isEqual:[NSNull null]]) {
        if (self.salon.city_name.length!=0)
            self.cityLabel.text = [self.cityLabel.text stringByAppendingString:[NSString stringWithFormat:@"%@",self.salon.city_name]];
    }
    
    self.salonNameLabel.text = self.salon.salon_name;
    
    [self setCellImageWithURL:self.salon.salon_image];
    
    //customise distance label
    if (self.isNearest) {
        self.distanceLabel.text = @"Nearest";
    }
    else{
        self.distanceLabel.text = [NSString stringWithFormat:@"%.1f KM", self.salon.distance] ;
    }
    
    //instantLabel
    if (self.salon.salon_tier == 1) {
        [self showInstant];
    }else{
        self.hasInstantLabel = NO;
    }
}

-(void)setGradientView:(UIView *)gradientView {
    _gradientView = gradientView;
    [Gradients addBlueToPurpleHorizontalLayerWithView:_gradientView];
}

-(void)showInstant {
    self.instantBookLabel.layer.cornerRadius = 15;
    self.instantBookLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    self.instantBookLabel.layer.borderWidth = 1;
    self.instantBookLabel.layer.masksToBounds = YES;
    
    NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
    imageAttachment.image = [UIImage imageNamed:@"lightningLogo"];
    CGFloat imageOffsetY = -5.0;
    imageAttachment.bounds = CGRectMake(0, imageOffsetY, imageAttachment.image.size.width/2, imageAttachment.image.size.height/2);
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
    NSMutableAttributedString *completeText= [[NSMutableAttributedString alloc] initWithString:@""];
    [completeText appendAttributedString:attachmentString];
    NSMutableAttributedString *textAfterIcon = [[NSMutableAttributedString alloc] initWithString:@" Instant Book"];
    [completeText appendAttributedString:textAfterIcon];
    self.instantBookLabel.attributedText = completeText;

}

-(void)tapPress: (UIGestureRecognizer*) t{
 
    
    NSString * accessToken =[[User getInstance] fetchAccessToken];
    
    if (accessToken==nil) {
        [WSCore showLoginSignUpAlert];
        
        return;
    }
    
    if (self.isFav) {
        
       // NSLog(@"unmark");
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salon.salon_id]];//changed
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&unmark=true"]];
        
        if ([[User getInstance] fetchKey].length!=0) {
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
        }
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        [self disableFavLabel];
        
    }
    else{
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salon.salon_id]];
        
        if ([[User getInstance] fetchKey].length!=0) {
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
        }
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        
        [self disableFavLabel];
        
    }
    
    
}

/*
 disableFavLabel
 set selectFavView userInteractionEnabled to NO so user can not keep pressing button
 set heartImageView alpha to 0.5
 */
- (void)disableFavLabel {
    self.selectFavView.userInteractionEnabled=NO;
    self.heartImageView.alpha=0.5;
}

/*
 enableFavLabel
 set selectFavView userInteractionEnable to YES
 set heartImageView alpha to 1.0
 
 */
-(void)enableFavLabel{
    self.selectFavView.userInteractionEnabled=YES;
    self.heartImageView.alpha=1.0;
}

- (IBAction)didTapOnPin {
    if (self.myDelegate && [self.myDelegate respondsToSelector:@selector(didClickOnMapPin:)]) {
        [self.myDelegate didClickOnMapPin:self.salon ];
    }
}

- (IBAction)instantBookAction:(id)sender {
    if (self.myDelegate && [self.myDelegate respondsToSelector:@selector(didClickIntantBookFor:)]) {
        [self.myDelegate didClickIntantBookFor:self.salon];
    }
}

/*
 showLikeCompletion
 show added to favourites alert
 */
-(void)showLikeCompletion{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Added to favourites!" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    
    double delayInSeconds = 1.0;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });
    
}

/*
 initWithStyle
 init networkManager
 set networkManager delegate to self
 */
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.clipsToBounds = YES;
        self.contentView.clipsToBounds=YES;
        
        self.networkManager = [[GCNetworkManager alloc] init];
        self.networkManager.delegate = self;
        
    }
    return self;
}

@end
