//
//  SalonTier4.m
//  whatsalon
//
//  Created by Graham Connolly on 15/09/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SalonTier4.h"

@implementation SalonTier4


-(instancetype) initWithSalonName: (NSString *) salon_name{
    
    self = [super init];
    if (self) {
        self.salonName = salon_name;
    }
    return self;
}
+ (instancetype) salonWithSalonName: (NSString *) salonName{
    
    return [[self alloc] initWithSalonName:salonName];
    
}

-(NSString *)getFullAddress{
    NSString * address = self.salonAddress;
    while ([address rangeOfString:@"  "].location != NSNotFound) {
        address = [address stringByReplacingOccurrencesOfString:@" " withString:@" "];
    }
    NSLog(@"%@", address);
    
    return address;
}
@end
