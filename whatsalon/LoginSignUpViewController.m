//
//  LoginSignUpViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 10/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "LoginSignUpViewController.h"
#import "RegisterViewController.h"
#import "LoginPageViewController.h"
//#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "UIView+AlertCompatibility.h"


@interface LoginSignUpViewController ()<GCNetworkManagerDelegate>

- (IBAction)cancel:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;


@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *signUp;


@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (nonatomic, strong) AppDelegate *appDelegate;

-(void)handleFBSessionStateChangeWithNotification:(NSNotification *)notification;

@property (nonatomic) GCNetworkManager * networkManager;


@end



@implementation LoginSignUpViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
    self.loginButton.layer.cornerRadius=3.0;
    self.signUp.layer.cornerRadius=3.0;
    [self.loginButton setTitle:@"LOGIN" forState:UIControlStateNormal];
    [self.signUp setTitle:@"SIGN UP" forState:UIControlStateNormal];
    [self.signUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.signUp.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    self.loginButton.titleLabel.font=[UIFont systemFontOfSize:18.0f];
    
    
    self.facebookButton.layer.cornerRadius=3.0f;
    [self.facebookButton setTitle:@"CONTINUE WITH FACEBOOK" forState:UIControlStateNormal];
    self.facebookButton.titleLabel.font = [UIFont systemFontOfSize:18.0f];
    self.signUp.backgroundColor=[UIColor clearColor];
    [self.signUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.loginButton.backgroundColor=[UIColor clearColor];
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.loginButton.layer.borderColor=[UIColor whiteColor].CGColor;
    self.signUp.layer.borderColor=[UIColor whiteColor].CGColor;
    self.loginButton.layer.borderWidth=1.0f;
    self.signUp.layer.borderWidth=1.0f;
    
    UIView * coverView = [[UIView alloc] initWithFrame:self.backgroundImage.frame];
    coverView.backgroundColor=[UIColor blackColor];
    coverView.alpha=0.6;
    
    [self.backgroundImage addSubview:coverView];
    
        
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(linkCCDismissView)
                                                 name:@"linkCCDismiss"
                                               object:nil];
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //we force our class to observe for the notification specified by the given name, and when it arrives we will call the handleFBSessionStateChangeWithNotification: method.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFBSessionStateChangeWithNotification:) name:@"SessionStateChangeNotification" object:nil];
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;

}
    
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Fail object %@",jsonDictionary);
   
    /*
     If social login is not successful then revoke the facebook permissions.
     */
    
    [UIView showSimpleAlertWithTitle:@"Login Failure" message:[NSString stringWithFormat:@"%@ %@",jsonDictionary[@"message"],@"Alternatively, go to 'Sign Up' and create a WhatSalon account."] cancelButtonTitle:@"OK"];
    
    //revoke facebook permissions
    [self.appDelegate revokeFacebookPermissions];
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    NSLog(@"Success object %@",jsonDictionary);
    if (manager==self.networkManager) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedIn" object:nil userInfo:nil];
        [[User getInstance] setUpFacebookProfile:jsonDictionary];
        
        
        [self dismissViewControllerAnimated:YES completion:^{
            if ([self.delegate respondsToSelector:@selector(didLoginWithFacebook)]) {
                [self.delegate didLoginWithFacebook];
            }
        }];
    
    }
}
-(void)linkCCDismissView{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBActions

- (IBAction)signUp:(id)sender {
    
  
    RegisterViewController * regViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"registerVC"];
    regViewController.modalPresentationStyle=UIModalPresentationCurrentContext;
    
    regViewController.isTier1Booking=YES;
    
    
    
    [self presentViewController:regViewController animated:YES completion:nil];
}

- (IBAction)Login:(id)sender {
    
    LoginPageViewController * loginPage = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
    loginPage.modalPresentationStyle=UIModalPresentationCurrentContext;
    
       loginPage.isTier1Booking=YES; 
    

    
    [self presentViewController:loginPage animated:YES completion:nil];
}

- (IBAction)cancel:(id)sender {
    
  [self dismissViewControllerAnimated:YES completion:nil];  
    
}

#pragma mark - UNWIND Segue
- (IBAction)unwindToLoginSignup:(UIStoryboardSegue *)unwindSegue
{
    
        [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (IBAction)loginWithFacebook:(id)sender {
    
    NSLog(@"Login SignUp - FAcebook sign in");
    
//    NSLog(@"Facebook active session state %lu",(unsigned long)[FBSession activeSession].state);
//    /*
//     The current session, also named active session, is accessed just like this:
//     Using the state property of the active session, we determine if there is an open session or not.
//     */
//    if ([FBSession activeSession].state != FBSessionStateOpen &&
//        [FBSession activeSession].state != FBSessionStateOpenTokenExtended) {
//        NSLog(@"Open active session");
//        //The openActiveSessionWithPersmissions:allowLoginUI public method of the AppDelegate is first called, which invokes in turn the openActiveSessionWithReadPermissions:allowLoginUI:completionHandler of the FBSession class. Once the whole login process is over, we inform the ViewController using a notification, where we'll handle the various session states in a while.
//        [self.appDelegate openActiveSessionWithPermissions:@[@"public_profile",@"email"] allowLoginUI:YES];
//        
//        
//    }else{
//        NSLog(@"Facebook %d",(int)[FBSession activeSession].state)
//        ;
//    }

}

//1.From the notification paramater object, we'll extract the dictionary.
//2. If no error occurred, then if the session is open, we'll amke a call to Facebook Graph API to get all the info we want. If the session is closed or failed, notify the UI
//3 If an error occurred, we'll output the error description and perform any UI-related tasks.
-(void)handleFBSessionStateChangeWithNotification:(NSNotification *)notification{
    
    
    
    //Get the session, state and error values from the notifications userInfo dictionary
   // NSDictionary * userInfo = [notification userInfo];
    
//    FBSessionState sessionState = [[userInfo objectForKey:@"state"] integerValue];
//    NSError * error = [userInfo objectForKey:@"error"];
//    
//    //handle the session state
//    //Usually the only interesting states are the opened session, the closed session and the failed login.
//    if (!error) {
//        
//        //In case that there's not any error, then check if the session is opened or closed
//        
//        if (sessionState==FBSessionStateOpen) {
//            
//            //The session is    OPEN    . Get the uesr information and update the UI
//            
//            [FBRequestConnection startWithGraphPath:@"me" parameters:@{@"fields":@"first_name, last_name, picture.type(large), email, gender, birthday, age_range"} HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                
//                if (!error) {
//                    
//                    NSLog(@"Result %@",result);
//                    
//                    NSLog(@"Session token is still alive, so log in automatically");
//                    
//                    //register user
//                    
//                    NSLog(@"______AccessToken______ %@",[[FBSession activeSession] accessTokenData]);
//                    // Close an existing session.
//                    //[[FBSession activeSession] closeAndClearTokenInformation];
//                    //NSString * results = [NSString stringWithFormat:@"%@",result];
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [self postToServerWithFirstName:[result objectForKey:@"first_name"] WithLastName:[result objectForKey:@"last_name"]  WithEmailAddress:[result objectForKey:@"email"]  WithUserGender:[result objectForKey:@"gender"]  WithUserImageURL:result[@"picture"][@"data"][@"url"]  WithAccessToken:[NSString stringWithFormat:@"%@",[[FBSession activeSession] accessTokenData]] WithSocialID:result[@"id"] AndWithSocialProvider:@"Facebook"];
//                    });
//                    
//                    
//                }
//                else{
//                    
//                    //A graph error occurred
//                    NSLog(@"A graph error occurred %@", [error localizedDescription]);
//                    
//                    [UIView showSimpleAlertWithTitle:@"Facebook Graph Error" message:@"We could not log you in, a Facebook error has occurred, please try again later." cancelButtonTitle:@"OK" ];
//                    
//                    [[FBSession activeSession] closeAndClearTokenInformation];
//                    
//                }
//            }];
//            
//            
//            
//            NSLog(@"You are logged in");
//            
//            
//            
//        }
//        else if (sessionState==FBSessionStateClosed || sessionState == FBSessionStateClosedLoginFailed){
//            
//            // A session was closed or the login was failed or canceled. Update the UI Accordingly
//            NSLog(@"You are NOT logged in");
//        }
//    }
//    else{
//        
//        //Facebook error occurred
//        
//        [UIView showSimpleAlertWithTitle:@"Facebook Error" message:@"We could not log you in, a Facebook error has occurred, please try again later." cancelButtonTitle:@"OK" ];
//    }
}


-(void)postToServerWithFirstName: (NSString *)f_name WithLastName: (NSString *) l_name WithEmailAddress: (NSString *)email WithUserGender: (NSString *)gender WithUserImageURL: (NSString *)imageUrl WithAccessToken: (NSString *)access_Token WithSocialID: (NSString *) socialID AndWithSocialProvider: (NSString *)social{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,@"social_login"]];
    
    NSData * nsdata = [imageUrl dataUsingEncoding:NSUTF8StringEncoding];
    // Get NSString from NSData object in Base64
    NSString *imageBase64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    NSString * params = [NSString stringWithFormat:@"first_name=%@",f_name];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&last_name=%@",l_name]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&email_address=%@",email]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&user_gender=%@",gender]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&image_url=%@",imageBase64Encoded]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&social_access_token=%@",access_Token]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&social_user_identifier=%@",socialID]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&social_login_source=%@",@"FACEBOOK"]];
    
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
    
}


@end
