//
//  NoLocationMessageView.h
//  whatsalon
//
//  Created by Graham Connolly on 06/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FUIButton.h"

@interface NoLocationMessageView : UIView

/*! @brief represents the view for hosting the content. */
@property (strong, nonatomic) IBOutlet UIView *view;

/*! @brief represents the title message. */
@property (strong, nonatomic) IBOutlet UILabel *messageTitle;

/*! @brief represents the image that complements the message. */
@property (strong, nonatomic) IBOutlet UIImageView *messageImage;

/*! @brief represents the message label. */
@property (strong, nonatomic) IBOutlet UILabel *messageText;

/*! @brief represents the try again button. */
@property (strong, nonatomic) IBOutlet FUIButton *tryAgainButton;

@end
