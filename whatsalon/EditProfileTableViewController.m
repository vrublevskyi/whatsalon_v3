//
//  RegisterStep2TableViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 19/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "EditProfileTableViewController.h"
#import "BrowseViewController.h"

@interface EditProfileTableViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *avatarImage;
@property (strong,nonatomic) UIToolbar * bgToolbar;

@end

@implementation EditProfileTableViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
   /*
    // Replace titleView
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 160, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = YES;
    
    CGRect titleFrame = CGRectMake(0, 2, 160, 24);
    UILabel *titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont fontWithName:kAppFont size:20];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor blackColor];
    titleView.shadowOffset = CGSizeMake(0, -1);
    titleView.text = @"Sign up";
    titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    
    CGRect subtitleFrame = CGRectMake(0, 24, 160, 44-24);
    UILabel *subtitleView = [[UILabel alloc] initWithFrame:subtitleFrame] ;
    subtitleView.backgroundColor = [UIColor clearColor];
    subtitleView.font = [UIFont fontWithName:kAppFont size:13];
    subtitleView.textAlignment = NSTextAlignmentCenter;
    subtitleView.textColor = [UIColor grayColor];
    subtitleView.shadowOffset = CGSizeMake(0, -1);
    subtitleView.text = @"Personal information";
    subtitleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:subtitleView];
    
    _headerTitleSubtitleView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                                 UIViewAutoresizingFlexibleRightMargin |
                                                 UIViewAutoresizingFlexibleTopMargin |
                                                 UIViewAutoresizingFlexibleBottomMargin);
    
    self.navigationItem.titleView = _headerTitleSubtitleView;
    */
    
    self.title=@"Edit Profile";
    //change font style of navigation bar title
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:kAppFont size:17],
      NSFontAttributeName,[UIColor blackColor],NSForegroundColorAttributeName, nil]];
    [self.avatarImage setUserInteractionEnabled:YES];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [self.avatarImage addGestureRecognizer:singleTap];
   
    // use the image's layer to mask the image into a circle
    self.avatarImage.layer.cornerRadius = roundf(self.avatarImage.frame.size.width/2.0);
    self.avatarImage.layer.masksToBounds = YES;
    
    //add blur
    self.cameraOptionsView.backgroundColor = [UIColor clearColor];
    self.bgToolbar = [[UIToolbar alloc] initWithFrame:self.cameraOptionsView.frame];
    self.bgToolbar.barStyle = UIBarStyleDefault;
    self.bgToolbar.alpha=0.9f;
    [self.cameraOptionsView.superview insertSubview:self.bgToolbar belowSubview:self.cameraOptionsView];
    self.bgToolbar.frame = self.cameraOptionsView.frame;

    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

//hide the camera options off the screen
-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.cameraOptionsView.frame = CGRectMake(0, 218, 320, 50);
    self.bgToolbar.frame = self.cameraOptionsView.frame;
}


-(void)singleTapping:(UIGestureRecognizer *)recognizer
{
  
    [self showCameraOptions];
    
    /*
     just show the camera on click
     
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing=NO;//can cause memory issues
    NSLog(@"image click");
   
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
    */
    
}

-(void) setHeaderTitle:(NSString*)headerTitle andSubtitle:(NSString*)headerSubtitle {
    assert(self.navigationItem.titleView != nil);
    UIView* headerTitleSubtitleView = self.navigationItem.titleView;
    UILabel* titleView = [headerTitleSubtitleView.subviews objectAtIndex:0];
    UILabel* subtitleView = [headerTitleSubtitleView.subviews objectAtIndex:1];
    assert((titleView != nil) && (subtitleView != nil) && ([titleView isKindOfClass:[UILabel class]]) && ([subtitleView isKindOfClass:[UILabel class]]));
    titleView.text = headerTitle;
    subtitleView.text = headerSubtitle;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Image Picker Controller delegate

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:^{
        [self hideCameraOptions];
    }];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.avatarImage.image = image;
    
    if (self.imagePicker.sourceType == UIImagePickerControllerSourceTypeCamera){
        //save the image to photos library
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
   
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self hideCameraOptions];
    }];
   
}


-(void)showCameraOptions{
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (screenSize.height > 480.0f) {
            /*Do iPhone 5 stuff here.*/
            [UIView transitionWithView:self.cameraOptionsView duration:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
               
                self.cameraOptionsView.frame = CGRectMake(0, 168, 320, 50);
                self.bgToolbar.frame = self.cameraOptionsView.frame;
                
            } completion:nil];
            
        } else {
            /*Do iPhone Classic stuff here.*/
            [UIView transitionWithView:self.cameraOptionsView duration:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                
            } completion:nil];
            
        }
    } else {
        /*Do iPad stuff here.*/
        
    }
    
    
}

-(void)hideCameraOptions{
    NSLog(@"Hide Camera Options");
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    self.bgToolbar.frame = self.cameraOptionsView.frame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (screenSize.height > 480.0f) {

            [UIView transitionWithView:self.cameraOptionsView duration:0.2 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.cameraOptionsView.frame = CGRectMake(0, 218, 320, 50);
                self.bgToolbar.frame = self.cameraOptionsView.frame;
                
            } completion:nil];
            
            
        } else {
            /*Do iPhone Classic stuff here.*/
            [UIView transitionWithView:self.cameraOptionsView duration:0.2 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                
                
            } completion:nil];
            
        }
    } else {
        /*Do iPad stuff here.*/
        
    }
    
}



- (IBAction)chooseExistingPic:(id)sender {
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing=NO;//set to NO - can cause memory issues
   
    //choose from photo library
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (IBAction)takePic:(id)sender {
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing=NO;//set to NO -can cause memory issues
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)done:(id)sender {
    
    if (self.isFromCC) {
        NSLog(@"Jump to browse");
       
        [self.navigationController popToRootViewControllerAnimated:YES];
    
           }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


@end
