//
//  CalloutBioViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 16/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalloutBioViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *bioLabel;
@property (weak, nonatomic) IBOutlet UILabel *bioText;
- (IBAction)cancel:(id)sender;

@end
