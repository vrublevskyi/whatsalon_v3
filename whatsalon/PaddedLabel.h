//
//  PaddedLabel.h
//  whatsalon
//
//  Created by Graham Connolly on 04/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaddedLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
