//
//  ReviewsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 04/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "ReviewsViewController.h"
#import "WSCore.h"

@interface ReviewsViewController ()

//dummy data
@property (strong,nonatomic) NSMutableArray * userArray;
@property (strong,nonatomic) NSMutableArray * dateArray;
@property (strong,nonatomic) NSMutableArray * serviceArray;
@property (strong,nonatomic) NSMutableArray * ratingArray;
@end

@implementation ReviewsViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    if (IS_IOS_8_OR_LATER) {
        [WSCore addDarkBlurToView:self.view];
    }
    else{
        self.view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.60];
  
    }
    self.reviewTableView.delegate = self;
    self.reviewTableView.dataSource = self;
    
      
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissModalView)];
    [self.tapView addGestureRecognizer:tap];
    
    
   
    [self dummyData];
}

-(void)dismissModalView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dummyData{
    self.userArray = [[NSMutableArray alloc] initWithObjects:@"Jenna",@"Brenda", @"Penny", @"Emma",@"Jan", @"Angela", nil];
    self.dateArray = [[NSMutableArray alloc] initWithObjects:@"March 12, 2014",@"March 11, 2014", @"March 10, 2014", @"March 10, 2014", @"March 9, 2014", @"March 7, 2014", nil];
    self.serviceArray = [[NSMutableArray alloc] initWithObjects:@"Blow Dry Long Hair",@"Eye Makeup",@"Face Makeup", @"Blow Dry Short Hair", @"Airbrush Foundation", @"Brow Maintenance", nil];
    self.ratingArray = [[NSMutableArray alloc] initWithObjects:@"fourStar.png",@"fiveStar.png",@"fiveStar.png",@"fourStar.png",@"fourStar.png", @"threeStar.png",@"fiveStar.png", nil];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.userArray.count+1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell;
    
    if (indexPath.row == 0) {
         cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        _reviewServiceLabel = (UILabel *)[cell viewWithTag:kReviewServiceName];
        _reviewDateLabel = (UILabel *)[cell viewWithTag:kReviewDate];
        _reviewUserLabel = (UILabel *)[cell viewWithTag:kReviewUser];
        _starRating = (UIImageView *)[cell viewWithTag:kStarRating];
        
        _reviewUserLabel.text = [self.userArray objectAtIndex:indexPath.row-1];
        _reviewDateLabel.text = [self.dateArray objectAtIndex:indexPath.row-1];
        _reviewServiceLabel.text = [self.serviceArray objectAtIndex:indexPath.row-1];
        _starRating.image = [UIImage imageNamed:[self.ratingArray objectAtIndex:indexPath.row-1]];
        
    }
    
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        return 1;
    }else{
        
        return 62;
    }
    
    return 0;
}


- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.view.superview.backgroundColor = [UIColor clearColor];
}


@end
