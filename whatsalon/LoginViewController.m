//
//  LoginViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 18/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "LoginViewController.h"
#import "RESideMenu/RESideMenu.h"
#import "RegisterTableViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    //show navigation bar - hidden in TabBarController
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    //ensure navigation bar is visible - it was made invisible in `BrowseViewController`
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
   
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.signUpBtn.frame.size.width, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:0.0/255 green:168.0/255 blue:233.0/255 alpha:1.0];
    [self.signUpBtn addSubview:lineView];

}




- (IBAction)showMenu:(id)sender {
    
   [self.sideMenuViewController presentMenuViewController];
}
- (IBAction)signUp:(id)sender {
    
   
    
    
}

- (IBAction)login:(id)sender {
}
@end
