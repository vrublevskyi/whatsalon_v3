//
//  Inbox.h
//  whatsalon
//
//  Created by Graham Connolly on 26/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Inbox : NSObject


/*! @brief represents the inbox id. */
@property (nonatomic) NSString * inbox_id;

/*! @brief represents the to field. */
@property (nonatomic) NSString *to;

/*! @brief represents the from field. */
@property (nonatomic) NSString *from;

/*! @brief represents the subject. */
@property (nonatomic) NSString * subject;

/*! @brief represents the content. */
@property (nonatomic) NSString *content;

/*! @brief represents the recieved parameter of the inbox json. */
@property (nonatomic) NSString * recieved;

/*! @brief represents the image name url string. */
@property (nonatomic) NSString * imageName;


/*! @brief creates an inbox instance using the inbox id. */
-(instancetype) initWithInboxID :(NSString *)i_id;

/*! @brief cretaes an inbox instance using the inbox id. */
+(instancetype) inboxWithInboxID: (NSString *) i_id;
@end
