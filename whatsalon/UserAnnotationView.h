//
//  UserAnnotationView.h
//  whatsalon
//
//  Created by Graham Connolly on 10/11/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface UserAnnotationView : MKAnnotationView
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *salonNameLabel;
@property (nonatomic,copy) NSString * salonImageType;
@property (strong, nonatomic) IBOutlet UIView *view;

@end
