//
//  SalonBookingModel.h
//  whatsalon
//
//  Created by Graham Connolly on 27/11/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Booking.h"

@protocol ServicesDelegate <NSObject>

@optional
-(void)didGetServices: (NSMutableArray *) services;
-(void)didGetStaff: (NSMutableArray *) staff;

//-(void)didReserveAppointment: (BOOL) success WithRefNumber: (NSString *) ref;

-(void)successfullAppointmentWithDictionary: (NSDictionary *) jsonDictionary;
-(void)failureAppointmentWithDictionary: (NSDictionary *) jsonDictionary;


//Phorest
-(void)phorestBookingResult : (BOOL) success WithMessage: (NSString *) message;
-(void)phorestSuccessfulBookingWithMessage: (NSString *) message;
-(void)phorestFailureBookingWithMessage: (NSString *) message;

-(void)didGetAllStaff: (NSMutableArray *) allStaff;

//iSalon
-(void)iSalonSuccessfulBookingWithMessage: (NSString *) message;
-(void)iSalonFailureBookingWithMessage: (NSString *) message;

//Premier
-(void)createPremierClientSuccessfulWithMessage: (NSString *) message;
-(void)createPremierClientFailureWithMessage: (NSString *) message;
-(void)premierSuccessfulBookingWithMessage: (NSString *) message;
-(void)premierFailureBookingWithMessage: (NSString *) message;

@end;
@interface SalonBookingModel : NSObject

@property (nonatomic,assign) id<ServicesDelegate> myDelegate;
@property (nonatomic) UIView *view;
@property (nonatomic) NSURLSessionDataTask * dataTask;

/*!
 * @discussion returns the list of services of a Tier 1 Phorest Salon
 * @param s_id Integer Required: The ID of the Salon
 * @return mArray - An array of objects representing each service offered
 */
-(NSMutableArray *)fetchServicesListWithID : (NSString *) s_id;



/*!
 * @discussion returns the list of free slots for this salon, this service and this staff member (optional)
 * @param s_id Integer Required: The ID of the salon
 * @param service_id Integer Required: The ID of the service you want to book
 * @param staff_id Integer (Optional) The id of the staff member you want to choose
 * @param start_datetime String (MYSQL datetime) Required Beginning of the date range
 * @param end_datetime String (MYSQL datetime) Required End of the date range
 * @warning When doing a booking you need to send the following values: slot_id, room_id, internal_start_datetime, internal_end_datetime, staff_id
 */

-(void)fetchStaffAndTimesWithSalonID:(NSString *) s_id WithServiceID: (NSString *) service_id WithStaffID: (NSString *) staff_id WithStartDatetime: (NSString *) start_datetime WithEndDateTime: (NSString *) end_datetime;

/*!
 * @discussion Books a service in a Phorest salon.
 * @param user_id NSString Required The ID of the logged in user
 * @param s_id NSString Required The ID of the Salon
 * @param slot_id NSString Required The ID of the available slot. Returned by fetchPhorestStaffAndTimesWithSalonID
 * @param service_id NSString String Required The ID of the service you want to book. Returned by fetchPhorestStafAndTimesWithSalonID
 * @param room_id NSString Required The ID of the room available. Returned by fetchPhorestStafAndTimesWithSalonID
 * @param startTime NSString Required The start of the time slot. Returned by fetchPhorestStafAndTimesWithSalonID
 * @param endTime NSString Required The end of the time slot. Returned by fetchPhorestStafAndTimesWithSalonID
 * @param staff_id NSString Required The ID of the Staff member who will do the job. Returned by fetchPhorestStafAndTimesWithSalonID
 */

-(void)bookPhorestServiceWithUserID: (NSString *) user_id WithSalonID: (NSString *) s_id WithSlotID: (NSString *)slot_id WIthServiceID: (NSString *)service_id WithRoomID: (NSString *)room_id WithInternalStartDateTime: (NSString *) startTime WithInternalEndDateTime: (NSString *) endTime AndStaff_id: (NSString *) staff_id;
/*!
 * @discussion Processes the payment of a Phorest booking. The appointment_reference_number is used for the booking, which is returned by bookPhorestServiceWithUserID. If the user has available balance, then it will be reduced. If they dont, then it will be taken from the card. Card details stored in RealVault.
 * @param appt_ref NSString Required The reference number for the appointment. Returned by bookPhorestServceWithUserID.
 */

-(void) phorestPaymentWithApptRef: (NSString *) appt_ref DEPRECATED_MSG_ATTRIBUTE("Please use: phorestPaymentWithApptRef: AndWithPromoCode:");

/*!
 * @discussion returns the list of staff members inside a Salon
 * @param s_id NSString Required: The ID of the salon
 */

-(void) fetchStaff: (NSString *) s_id;

/*!
 * @brief - cancels NSURLSessionDataTask and dimisses network loading
 */
-(void)cancelBookingModel;



-(void) phorestPaymentWithApptRef: (NSString *) appt_ref AndWithPromoCode:(NSString *) coupon;

-(void)iSalonPaymentWithBookingObject: (Booking *) bookingObject AndWithPromoCode: (NSString *) coupon;

-(void)premierCreateClientWithBookingObject: (Booking *) bookingObject;

-(void)premierBookServiceWithBookingObject: (Booking *) bookingObject withPromoCode: (NSString *) coupon;
@end
