//
//  InboxTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 26/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Inbox.h"

@interface InboxTableViewCell : UITableViewCell

/*! @brief represents the title label. */
@property (nonatomic) UILabel * titleLabel;

/*! @brief represents the content label. */
@property (nonatomic) UILabel * contentLabel;

/*! @brief represents the Inbox object. */
@property (nonatomic) Inbox * inbox;

/*! @brief determines if a shadow has been applied. */
@property (nonatomic) BOOL hasShadow;


/*! @brief sets up the content of the cells. */
-(void)setUpCellWithInbox: (Inbox *) inbox;
@end
