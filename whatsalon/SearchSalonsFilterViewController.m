//
//  SearchSalonsFilterViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 10/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SearchSalonsFilterViewController.h"
#import "SearchSalonsTableViewController.h"

@interface SearchSalonsFilterViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic) NSMutableArray * servicesArray;
@property (nonatomic) NSString * catString;
@property (nonatomic) NSString * catID;
@property (nonatomic) BOOL isSearchPressed;


@end

@implementation SearchSalonsFilterViewController

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.catID=@"";
    self.catString=@"";
    self.isSearchPressed=NO;
    

    
}
#pragma tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.servicesArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = [self.servicesArray objectAtIndex:indexPath.row];
    
    if ([cell.textLabel.text isEqualToString:@"Body"]) {
        cell.imageView.image = [UIImage imageNamed:@"body_service_icon"];
    }
    else if ([cell.textLabel.text isEqualToString:@"Face"]) {
        cell.imageView.image = [UIImage imageNamed:@"face_service_icon"];
        
    }else if ([cell.textLabel.text isEqualToString:@"Hair"]) {
        cell.imageView.image = [UIImage imageNamed:@"hair_service_icon"];
        
    }else if ([cell.textLabel.text isEqualToString:@"Hair Removal"]) {
        cell.imageView.image = [UIImage imageNamed:@"wax_service_icon"];
        
    }else if ([cell.textLabel.text isEqualToString:@"Massage"]) {
        cell.imageView.image = [UIImage imageNamed:@"massage_service_icon"];
        
    }
    else if ([cell.textLabel.text isEqualToString:@"Nails"]) {
        
        cell.imageView.image = [UIImage imageNamed:@"nail_service_icon"];
        
    }
    cell.imageView.image = [cell.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.imageView setTintColor:kWhatSalonBlue];
    [WSCore addBottomLine:cell :kCellLinesColour];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.catID = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    self.catString = [self.servicesArray objectAtIndex:indexPath.row];
    [self toSearchPage];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Search filter left menu");
    // Do any additional setup after loading the view.
    
    self.searchHolder.layer.cornerRadius=kCornerRadius;
    self.searchHolder.userInteractionEnabled=YES;
    self.searchText.userInteractionEnabled=NO;
    self.searchImage.userInteractionEnabled=NO;
    self.navBarView.backgroundColor=kWhatSalonBlue;
    [WSCore transparentNavigationBarOnView:self];
    
    UITapGestureRecognizer * searchPageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchPressed)];
    searchPageGesture.numberOfTapsRequired=1;
    [self.searchHolder addGestureRecognizer:searchPageGesture];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor whiteColor],NSForegroundColorAttributeName,
                                nil];
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:attributes forState:UIControlStateNormal];
   self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};    
    self.title=@"Search";

    self.automaticallyAdjustsScrollViewInsets=NO;
    
    self.servicesArray = [NSMutableArray arrayWithObjects:@"Hair",@"Hair Removal",@"Nails",@"Massage",@"Face",@"Body", nil];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    self.view.backgroundColor=kBackgroundColor;
    self.tableView.backgroundColor=[UIColor clearColor];
    self.tableView.backgroundView=nil;
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView=footerView;
    
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    
}

-(void)searchPressed{
    self.isSearchPressed=YES;
    [self toSearchPage];
}
-(void)cancel{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)toSearchPage{
    NSLog(@"TO SEARCH");
    SearchSalonsTableViewController * searchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVC"];
    searchVC.catString =self.catString;
    searchVC.catID = self.catID;
    searchVC.wasSearchPressed=self.isSearchPressed;
     CATransition *transition = [CATransition animation];
     transition.duration = 0.3;
     transition.type = kCATransitionFade;
     [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
     [self.navigationController pushViewController:searchVC animated:NO];
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)unwindToSaerchFilterScreen:(UIStoryboardSegue *)unwindSegue
{
    
    
    NSLog(@"Unwind to search filter");
}

@end
