//
//  DiscoveryItem.m
//  whatsalon
//
//  Created by Graham Connolly on 15/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "DiscoveryItem.h"

@implementation DiscoveryItem
-(instancetype) initWithDiscoveryType: (NSString *) type{
    
    self = [super init];
    if (self) {
        self.type = type;
    }
    return self;
    
}

+(instancetype) discoveryItemWithDiscoveryType: (NSString *) type {
    
    return [[self alloc] initWithDiscoveryType:type];
}

@end
