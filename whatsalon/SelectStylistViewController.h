//
//  SelectStylistViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 10/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header SelectStylistViewController.h
  
 @brief This is the header file for the SelectStylistViewController.h
  
 
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.3
 */
#import <UIKit/UIKit.h>
#import "Salon.h"
#import "PhorestStaffMember.h"
#import "FUIButton.h"


@class SelectStylistViewController;
@protocol SelectStylistVCDelegate <NSObject>
- (void) onStylistSelected: (NSString *) stylist sender:(SelectStylistViewController*) sender;
@end

@interface SelectStylistViewController : UIViewController

/*! @brief represents the SelectStylistDelegate. */
@property (nonatomic) id<SelectStylistVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic)  NSArray *stylist;

@end
