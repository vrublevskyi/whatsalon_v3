//
//  UserBooking.h
//  whatsalon
//
//  Created by Graham Connolly on 26/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserBooking : NSObject

@property (nonatomic) NSString * salon_id;
@property (nonatomic) NSString * user_id;
@property (nonatomic) NSString * slot_id;
@property (nonatomic) NSString * room_id;
@property (nonatomic) NSString * internal_start_datetime;
@property (nonatomic) NSString * internal_end_datetime;
@property (nonatomic) NSString * staff_id;
@property (nonatomic) NSString * service_id;
@property (nonatomic) NSString * appointment_reference_number;
@property (nonatomic) NSString * chosentService;
@property (nonatomic) NSString * chosenPrice;
@property (nonatomic) NSString * chosenStylist;
@property (nonatomic) NSString * chosenDay;
@property (nonatomic) NSString * chosenTime;
@property (nonatomic) NSString *endTime;

+(instancetype)getInstance;
@end
