//
//  SalonImageGalleryScrollView.h
//  whatsalon
//
//  Created by Graham Connolly on 16/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalonImageGalleryScrollView : NSObject<UIScrollViewDelegate>

@property (nonatomic) NSMutableArray * pageViews;
@property (nonatomic) int numberOfPages;
@property (nonatomic) NSMutableArray * salonImages;
@property (nonatomic) UIScrollView * parent;
@property (nonatomic) CGRect parentsFrame;

-(void)createPages;
-(void)loadVisiblePages;
@end
