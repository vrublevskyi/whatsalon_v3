//
//  SettingsViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 19/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SettingsViewController.h"
#import "DismissDetailTransition.h"
#import "PresentDetailTransition.h"
#import "EditProfileTableViewController.h"
#import "WhatSalonDotComViewController.h"
#import "RESideMenu/RESideMenu.h"
#import <UIImage+ImageEffects.h>
#import "User.h"
#import "SignUpMenuViewController.h"
#import "RegisterViewController.h"
#import "LoginPageViewController.h"
//#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "GCNetworkManager.h"
#import "FUIButton.h"
#import "UIColor+FlatUI.h"
#import "BrowseViewController.h"
#import "TwilioVerifiyViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "UIView+AlertCompatibility.h"
#import "LinkCCViewController.h"
#import "DiscoveryViewController.h"





@interface SettingsViewController () <UIViewControllerTransitioningDelegate,GCNetworkManagerDelegate,TwilioDelegate,LinkCCDelegate>
/*!
 * @brief An NSArray that contains the section headers
 */
@property (nonatomic,copy) NSArray * sectionTitles;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
- (IBAction)login:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic) UIView * blackView;

-(void)handleFBSessionStateChangeWithNotification:(NSNotification *)notification;

- (IBAction)loginWithFacebook:(id)sender;

@property (nonatomic) GCNetworkManager * networkManager;


@property (nonatomic) BOOL unwindLogInCalled;
@end

@implementation SettingsViewController

- (void)setUpNavigationBar
{
    [WSCore nonTransparentNavigationBarOnView:self];
}

#pragma mark - UIViewController Lifecycle Methods
-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
   
    
}

- (void)setLoginScreen
{
    self.signUpView.hidden=NO;
    //self.signUpButton.backgroundColor = kWhatSalonBlue;
    //self.loginButton.backgroundColor=kWhatSalonBlue;
    self.loginButton.layer.cornerRadius=3.0;
    self.signUpButton.layer.cornerRadius=3.0;
    [self.loginButton setTitle:@"LOGIN" forState:UIControlStateNormal];
    [self.signUpButton setTitle:@"SIGN UP" forState:UIControlStateNormal];
    [self.signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.signUpButton.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    self.loginButton.titleLabel.font=[UIFont systemFontOfSize:16.0f];
    self.image.hidden=YES;
    self.headerLabel.hidden=YES;
    self.descriptionLabel.hidden=YES;
    
    self.facebookBtn.layer.cornerRadius=3.0f;
    [self.facebookBtn setTitle:@"CONTINUE WITH FACEBOOK" forState:UIControlStateNormal];
    self.facebookBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    self.signUpButton.backgroundColor=[UIColor clearColor];
    [self.signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    self.loginButton.backgroundColor=[UIColor clearColor];
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.loginButton.layer.borderColor=[UIColor whiteColor].CGColor;
    self.signUpButton.layer.borderColor=[UIColor whiteColor].CGColor;
    self.loginButton.layer.borderWidth=1.0f;
    self.signUpButton.layer.borderWidth=1.0f;
    
    [WSCore transparentNavigationBarOnView:self];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    self.title=@"";
    
    if (self.blackView==nil) {
        self.blackView = [[UIView alloc] initWithFrame:self.backgroundImage.frame];
 
    }
    self.blackView.alpha=0.6;
    self.blackView.backgroundColor=[UIColor blackColor];
    [self.backgroundImage addSubview:self.blackView];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [WSCore statusBarColor:StatusBarColorBlack];
    

    self.settingsTableView.delegate =self;
    self.settingsTableView.dataSource = self;
    
    self.sectionTitles = [[NSArray alloc] initWithObjects:@"ACCOUNT",@"WHATSALON", nil];
    
    NSLog(@"Is user logged in %d",[[User getInstance] isUserLoggedIn]);
    /*
     Check if the user is signed in or not. Show self.signUpView if they are not, vice versa.
     */
    if (![[User getInstance] isUserLoggedIn]) {
        NSLog(@"Show screen");
        [self setLoginScreen];
    }else{
        self.signUpView.hidden=YES;
      [self setUpNavigationBar];
        [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], NSForegroundColorAttributeName,nil]];
    }
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //we force our class to observe for the notification specified by the given name, and when it arrives we will call the handleFBSessionStateChangeWithNotification: method.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFBSessionStateChangeWithNotification:) name:@"SessionStateChangeNotification" object:nil];
    
    
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    NSLog(@"Failure object %@",jsonDictionary);
    
    /*
     If social login failed then revoke Facebook permissions.
     */
    [UIView showSimpleAlertWithTitle:@"Login Failure" message:[NSString stringWithFormat:@"%@ %@",jsonDictionary[@"message"],@"Alternatively, go to 'Sign Up' and create a WhatSalon account."] cancelButtonTitle:@"OK"];
    
    [self.appDelegate revokeFacebookPermissions];
}

#pragma mark - Twilio Delegate Methods
-(void)twilioWasVerified{
    NSLog(@"Twilio Verified");
    [self.settingsTableView reloadData];
    
    /*
    if (![[User getInstance] hasCreditCard]) {
        
        NSLog(@"Got to Credit Card");
        [self gotToCreditCardScreen];
    }else{
     */
        [self gotToHomePage];
    /*
    }
     */
}
-(void)twilioCanceled{
    /*
    BrowseViewController * browse = [self.storyboard instantiateViewControllerWithIdentifier:@"browseController"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:browse animated:NO];
     */
    [self gotToHomePage];
}

#pragma mark - LinkCCDelegate methods
-(void)didSkipCard{
    [self gotToHomePage];
}

-(void)didAddCard{
    [self gotToHomePage];
}
-(void)gotToCreditCardScreen{
    LinkCCViewController * linkCC = [self.storyboard instantiateViewControllerWithIdentifier:@"linkCCVC"];
    //linkCC.modalPresentationStyle = UIModalPresentationCurrentContext;
    linkCC.delegate=self;
    [self presentViewController:linkCC animated:YES completion:nil];

}
-(void)gotToHomePage{
    
    //implemenation for Discovery
    
    //BrowseViewController * browse = [self.storyboard instantiateViewControllerWithIdentifier:@"browseController"];
    DiscoveryViewController *disc = [self.storyboard instantiateViewControllerWithIdentifier:@"discovery1"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    //[self.navigationController pushViewController:browse animated:NO];
    [self.navigationController pushViewController:disc animated:NO];

}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
   
    NSLog(@"Success object %@",jsonDictionary);
    if (manager==self.networkManager) {
        [[User getInstance] setUpFacebookProfile:jsonDictionary];
       

        [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedIn" object:nil userInfo:nil];
        
        
        //show twilio
        if (![[User getInstance] isTwilioVerified]) {
            TwilioVerifiyViewController * twilioVC = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyVC"];
            twilioVC.delegate=self;
            [self presentViewController:twilioVC animated:YES completion:nil];
        }
        /*
        else if(![[User getInstance] hasCreditCard]){
            [self gotToCreditCardScreen];
        }
         */
        else{
            
            NSLog(@" Has credit card %d",[[User getInstance] hasCreditCard]);
            [self gotToHomePage];
            
        }
        
    }        
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    /*
     If the signUpView is hidden then set up the options screen.
     
     */
    
    if (self.unwindLogInCalled) {
        self.unwindLogInCalled=NO;
        NSLog(@"View Appears so show browse");
        [self gotToHomePage];
    }else{
        if ([[User getInstance] isLocalSignUp]) {
            //[self gotToHomePage];
            [WSCore nonTransparentNavigationBarOnView:self];
            self.title=@"Settings";
            self.navigationController.navigationBar.tintColor=self.view.tintColor;
            self.signUpView.hidden=YES;
            [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], NSForegroundColorAttributeName,nil]];
            
        }
        if (self.signUpView.hidden == YES) {
            [self setUpSettingsPage];
            
        }

    }
    

}

/*
 Sets background color of self.settingsTableView
 Creates a tableFooterView which hosts the log out button.
 */
- (void)setUpSettingsPage {
    self.settingsTableView.backgroundColor = [UIColor colorWithRed:245.0/255 green:245.0/255 blue:247.0/255 alpha:1.0];
    
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.settingsTableView.frame.size.width , 80)];
    footerView.backgroundColor = [UIColor colorWithRed:245.0/255 green:245.0/255 blue:247.0/255 alpha:1.0];
    self.settingsTableView.tableFooterView = footerView;
    
    FUIButton * logoutButton = [[FUIButton alloc] initWithFrame:CGRectMake(10, 20, footerView.bounds.size.width - 20, kButtonHeight)];
    [logoutButton setTitle:@"Log Out" forState:UIControlStateNormal];
    logoutButton.backgroundColor = kWhatSalonBlue;
    logoutButton.layer.cornerRadius=3.0;
    logoutButton.buttonColor = kWhatSalonBlue;
    logoutButton.shadowColor = kWhatSalonBlueShadow;
    logoutButton.shadowHeight = 1.0f;
    logoutButton.cornerRadius = 3.0f;
    
    [footerView addSubview:logoutButton];
    [logoutButton addTarget:self action:@selector(logOut) forControlEvents:UIControlEventTouchUpInside];
}
/*!
 * @discussion logOut - display a UIAlert prompt to the user
 */
-(void)logOut{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Log out" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
    alert.tag = 101;
   
}

#pragma mark - UIAlertView Delegate Method
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 101) {
        if (buttonIndex==0) {
            //do nothing
            
        }else if(buttonIndex==1){
            
            //log user out
            [[User getInstance] logout];
            
            //show sign up view
            self.signUpView.hidden=NO;
            [self setLoginScreen];
            
            //scroll tableview back to top i.e rest tableview
            [self.settingsTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
            
            
        }
    }
}

#pragma mark - UITableView datasource and Delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    //return 3;
    return self.sectionTitles.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 55;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  
        UILabel *myLabel = [[UILabel alloc] init];
        myLabel.frame = CGRectMake(15, 26, 180, 21);
        myLabel.backgroundColor = [UIColor clearColor];
        myLabel.font = [UIFont systemFontOfSize: 17.0f];;
        myLabel.textColor = [UIColor darkGrayColor];
        myLabel.text = [self.sectionTitles objectAtIndex:section];
        
        UIView *headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor colorWithRed:245.0/255 green:245.0/255 blue:247.0/255 alpha:1.0];
        [headerView addSubview:myLabel];
        
        return headerView;
    
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        
        if ([[User getInstance] isTwilioVerified] && [[User getInstance] isSocialSignUp]) {
            if (indexPath.row==1) {
                return 0;
            }
        }
    }
    return self.settingsTableView.rowHeight;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
   
    if (indexPath.section == 1) {
        if (indexPath.row==0) {
            cell.userInteractionEnabled=NO;
        }
    }
    
    if (indexPath.section ==0) {
        
        if (indexPath.row ==0) {
            cell.textLabel.text = @"Edit Profile";
            cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            [WSCore addTopLine:cell :[UIColor lightGrayColor] :0.5];            
            
        }
        if (indexPath.row ==1) {
            if (![[User getInstance] isTwilioVerified]) {
                cell.textLabel.text = @"Verify Account";
            }
            else{
                cell.textLabel.text = @"Change Password";
            }
            
            cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        if (indexPath.row ==2) {
            cell.textLabel.text = @"Change Phone & Email";
            cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        if (indexPath.row ==3) {
            
            cell.textLabel.text = @"Redeem code";
            cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            

        }
        if (indexPath.row ==4) {
            cell.textLabel.text = @"Credit Card Details";
            cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            [WSCore addBottomLine:cell :[UIColor lightGrayColor]] ;
            

        }
        
    }
    
    if (indexPath.section ==1) {
        
        if (indexPath.row ==0) {
            cell.textLabel.text = [NSString stringWithFormat:@"Version %@",kVersionNumber];
            //cell.detailTextLabel.text = kVersionNumber;
            cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
            cell.detailTextLabel.font = [UIFont systemFontOfSize: 17.0f];
            [WSCore addTopLine:cell :[UIColor lightGrayColor] :0.5];
        }
        if (indexPath.row==1) {
            cell.textLabel.text = @"Send Feedback";
            cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        if (indexPath.row ==2) {
            cell.textLabel.text = @"About";
            cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        }
        if (indexPath.row ==3) {
            cell.textLabel.text = @"WhatSalon.com";
            cell.textLabel.font = [UIFont systemFontOfSize: 17.0f];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            [WSCore addBottomLine:cell :[UIColor lightGrayColor]];
        }
        
        
    }
    
    cell.clipsToBounds=YES;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //AccountSection
    if (indexPath.section==0) {
        if (indexPath.row ==0) {
            
           EditProfileTableViewController *viewController =
            [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileController"];
            viewController.modalPresentationStyle = UIModalPresentationCustom;
            viewController.transitioningDelegate = self;
            
            [self presentViewController:viewController animated:YES completion:nil];
             
        }
        if (indexPath.row ==1) {
            if (![[User getInstance] isTwilioVerified]) {
                //go to twilio
                TwilioVerifiyViewController * twilioVC = [self.storyboard instantiateViewControllerWithIdentifier:@"verifyVC"];
                twilioVC.delegate=self;
                [self presentViewController:twilioVC animated:YES completion:nil];
                
            }else{
                [self performSegueWithIdentifier:@"changePassword" sender:self];
            }
        }
        if (indexPath.row ==2) {
            [self performSegueWithIdentifier:@"changeEmail" sender:self];
        }
        if (indexPath.row ==3) {
            [self performSegueWithIdentifier:@"redeemCode" sender:self];
        }
        if (indexPath.row==4) {
            [self performSegueWithIdentifier:@"ccDetails" sender:self];
        }
    }
    
   
    
    //WhatSalon Section
    if (indexPath.section == 1) {
        if (indexPath.row==0) {
            
        }
        if (indexPath.row==1) {
            [self performSegueWithIdentifier:@"submitBug" sender:self];
        }
        
        if (indexPath.row == 2) {
            [self performSegueWithIdentifier:@"about" sender:self];
        }
        if (indexPath.row ==3) {
            [self performSegueWithIdentifier:@"website" sender:self];
        }
        
        
    }
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
   
    if (section ==0) {
        return 5;
    }
    if (section==1) {
        return 4;
    }

    
    return 0;
}

#pragma mark - UIStoryboard method
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"website"]) {
        
        WhatSalonDotComViewController * websiteVC = (WhatSalonDotComViewController *)segue.destinationViewController;
        websiteVC.pageTitle = @"WhatSalon.com";
        //websiteVC.urlString = kWhatSalonWebsite;
        
    }
    
    
}
#pragma mark - UNWIND Segue
- (void)userHasLoggedIn
{
    
    [WSCore nonTransparentNavigationBarOnView:self];
    [self.settingsTableView reloadData];
    self.title=@"Settings";
    self.navigationController.navigationBar.tintColor=self.view.tintColor;
    self.signUpView.hidden=YES;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], NSForegroundColorAttributeName,nil]];
    //[self.sideMenuViewController presentMenuViewController];
    //[self gotToHomePage];
}

- (IBAction)unwindToSettings:(UIStoryboardSegue *)unwindSegue
{
    NSLog(@"Unwind home page");
    
    [self userHasLoggedIn];
    self.unwindLogInCalled=YES;
   //[self gotToHomePage];
    
}

#pragma mark - Show Menu action
- (IBAction)showMenuButton:(id)sender {
    
     [self.sideMenuViewController presentMenuViewController];
    
}

- (IBAction)signUp:(id)sender {
    
    RegisterViewController * reg =[self.storyboard instantiateViewControllerWithIdentifier:@"registerVC"];
    reg.isFromSettings=YES;
   [self presentViewController:reg animated:YES completion:nil];
   // [self.navigationController pushViewController:reg animated:YES];

}


#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}

- (IBAction)login:(id)sender {
    
    LoginPageViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
    [self presentViewController:login animated:YES completion:nil];
    /*
    SignUpMenuViewController * signUpMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"signUpMenu"];
    signUpMenuVC.isSignUp=NO;
    [self presentViewController:signUpMenuVC animated:YES completion:nil];
     */

}
- (IBAction)loginWithFacebook:(id)sender {
    NSLog(@"Settings -- FAcebook sign in");
    
    /*
     The current session, also named active session, is accessed just like this:
     Using the state property of the active session, we determine if there is an open session or not.
     */
/*
    if ([FBSession activeSession].state != FBSessionStateOpen &&
        [FBSession activeSession].state != FBSessionStateOpenTokenExtended) {
        NSLog(@"Open active session");
        //The openActiveSessionWithPersmissions:allowLoginUI public method of the AppDelegate is first called, which invokes in turn the openActiveSessionWithReadPermissions:allowLoginUI:completionHandler of the FBSession class. Once the whole login process is over, we inform the ViewController using a notification, where we'll handle the various session states in a while.
        [self.appDelegate openActiveSessionWithPermissions:@[@"public_profile",@"email"] allowLoginUI:YES];
        
        
    }
    else{
        
    }
 */
    

}

//1.From the notification paramater object, we'll extract the dictionary.
//2. If no error occurred, then if the session is open, we'll amke a call to Facebook Graph API to get all the info we want. If the session is closed or failed, notify the UI
//3 If an error occurred, we'll output the error description and perform any UI-related tasks.
-(void)handleFBSessionStateChangeWithNotification:(NSNotification *)notification{
    
    
    
    //Get the session, state and error values from the notifications userInfo dictionary
   // NSDictionary * userInfo = [notification userInfo];
    
    /*
    FBSessionState sessionState = [[userInfo objectForKey:@"state"] integerValue];
    NSError * error = [userInfo objectForKey:@"error"];
    
    //handle the session state
    //Usually the only interesting states are the opened session, the closed session and the failed login.
    if (!error) {
        
        //In case that there's not any error, then check if the session is opened or closed
        
        if (sessionState==FBSessionStateOpen) {
            
            //The session is    OPEN    . Get the uesr information and update the UI
            
            [FBRequestConnection startWithGraphPath:@"me" parameters:@{@"fields":@"first_name, last_name, picture.type(large), email, gender, birthday, age_range"} HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                
                if (!error) {
                    
                    NSLog(@"Result %@",result);
                    
                    NSLog(@"Session token is still alive, so log in automatically");
                    
                    //register user
                    
                    NSLog(@"______AccessToken______ %@",[[FBSession activeSession] accessTokenData]);
                    // Close an existing session.
                    //[[FBSession activeSession] closeAndClearTokenInformation];
                    //NSString * results = [NSString stringWithFormat:@"%@",result];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self postToServerWithFirstName:[result objectForKey:@"first_name"] WithLastName:[result objectForKey:@"last_name"]  WithEmailAddress:[result objectForKey:@"email"]  WithUserGender:[result objectForKey:@"gender"]  WithUserImageURL:result[@"picture"][@"data"][@"url"]  WithAccessToken:[NSString stringWithFormat:@"%@",[[FBSession activeSession] accessTokenData]] WithSocialID:result[@"id"] AndWithSocialProvider:@"Facebook"];
                    });
                    
                    
                }
                else{
                    
                    //A graph error occurred
                    NSLog(@"A graph error occurred %@", [error localizedDescription]);
                    [UIView showSimpleAlertWithTitle:@"Facebook Error" message:@"We could not log you in, a Facebook error has occurred, please try again later." cancelButtonTitle:@"OK" ];
                }
            }];
            
            
            
            NSLog(@"You are logged in");
            
            
            
        }
        else if (sessionState==FBSessionStateClosed || sessionState == FBSessionStateClosedLoginFailed){
            
            // A session was closed or the login was failed or canceled. Update the UI Accordingly
            NSLog(@"You are NOT logged in");
        }
    }
    else{
        
        //Facebook error occurred
        
        [UIView showSimpleAlertWithTitle:@"Facebook Error" message:@"We could not log you in, a Facebook error has occurred, please try again later." cancelButtonTitle:@"OK" ];
    }
     */
}


-(void)postToServerWithFirstName: (NSString *)f_name WithLastName: (NSString *) l_name WithEmailAddress: (NSString *)email WithUserGender: (NSString *)gender WithUserImageURL: (NSString *)imageUrl WithAccessToken: (NSString *)access_Token WithSocialID: (NSString *) socialID AndWithSocialProvider: (NSString *)social{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,@"social_login"]];
    
    NSData * nsdata = [imageUrl dataUsingEncoding:NSUTF8StringEncoding];
    // Get NSString from NSData object in Base64
    NSString *imageBase64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    NSString * params = [NSString stringWithFormat:@"first_name=%@",f_name];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&last_name=%@",l_name]];
     params=[params stringByAppendingString:[NSString stringWithFormat:@"&email_address=%@",email]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&user_gender=%@",gender]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&image_url=%@",imageBase64Encoded]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&social_access_token=%@",access_Token]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&social_user_identifier=%@",socialID]];
    params=[params stringByAppendingString:[NSString stringWithFormat:@"&social_login_source=%@",@"FACEBOOK"]];
    
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
 
}


@end
