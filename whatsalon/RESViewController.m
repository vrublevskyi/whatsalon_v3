//
//  RESViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "RESViewController.h"
#import "MenuViewController.h"
#import "UIImage+ImageEffects.h"

@interface RESViewController ()

@end

@implementation RESViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)awakeFromNib
{
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
    self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuController"];
    self.delegate = (MenuViewController *)self.menuViewController;
    

}


- (void)blurBackgroundImage
{
   
    
    //self.backgroundImage=[WSCore blurMenuImage];
    self.backgroundImage=[UIImage imageNamed:kMenu_wallpaper];

    

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self blurBackgroundImage];
   
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
