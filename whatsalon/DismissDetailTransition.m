//
//  DismissDetailTransition.m
//  whatsalon
//
//  Created by Graham Connolly on 08/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "DismissDetailTransition.h"

@implementation DismissDetailTransition

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    //it gets us the view that we are tranistioning from
    UIViewController * detail = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
        [UIView animateWithDuration:0.3 animations:^{
        detail.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [detail.view removeFromSuperview];
        [transitionContext completeTransition:YES];
    }];
    
}

-(NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    return 0.3;
}



@end
