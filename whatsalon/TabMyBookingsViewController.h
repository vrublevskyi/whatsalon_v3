//
//  TabMyBookingsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyBookings.h"
#import "SalonReviewViewController.h"
#import "Salon.h"

@interface TabMyBookingsViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate,ReviewDelegate>

/*! @brief represents the booking table view. */
@property (weak, nonatomic) IBOutlet UITableView *bookingTableView;

/*! @brief represents the Salon object. */
@property (nonatomic) Salon *salon;

/*! @brief determines if the navigation bar should be shown. */
@property (nonatomic) BOOL showNavigationBar;

@end
