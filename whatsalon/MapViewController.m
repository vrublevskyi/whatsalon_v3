//
//  MapViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 08/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "MapViewController.h"
#import "User.h"
#import "ServiceCategory.h"
#import "CategorySingleton.h"
#import "Salon.h"
#import "SalonAnnotationView.h"
#import "SalonAnnotation.h"
#import "MKMapView-Additions.h"
#import "KAProgressLabel.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "ResultsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LoginSignUpViewController.h"
#import "LinkCCViewController.h"
#import "RESideMenu/RESideMenu.h"
#import "Job.h"
#import <AudioToolbox/AudioServices.h>
#import "SalonDetailViewController.h"
#import "UserAnnotationView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ColoredImage.h"
#import "TabNewSalonDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TabDirectoryViewController.h"
#import "whatsalon-Swift.h"

#define kMaxAltitude 1933.182580
@interface MapViewController ()<MKMapViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonRight;
@property (nonatomic) CLLocation * startLocation;
@property (nonatomic) CLLocationManager * locationManager;
    @property (weak, nonatomic) IBOutlet UIView *gradientView;
    
@property (weak, nonatomic) IBOutlet UILabel *salonNameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *salonImageView;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *salonInfoHolder;
@property (nonatomic) Salon * selectedSalon;
@property (nonatomic) NSInteger nearestSalonTag;
@property (nonatomic) BOOL readyForUpdate;
@end

@implementation MapViewController

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    
    if (self.readyForUpdate) {
        self.readyForUpdate=NO;
        //NSLog(@"Update NOW");
        
        //MKMapRect visibleMapRect = mapView.visibleMapRect;
        //NSSet * visibleAnnotations = [self.map annotationsInMapRect:visibleMapRect];
        
        for (id<MKAnnotation> annotation in mapView.annotations){
            MKAnnotationView* anView = [mapView viewForAnnotation: annotation];
            if (anView){
                // Process annotation view
                //BOOL annotationsIsVisible = [visibleAnnotations containsObject:annotation];
               // NSLog(@"Is visible %@",StringFromBOOL(annotationsIsVisible));
            }
        }
        
    }
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)didDragMap:(UIGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        NSLog(@"drag ended");
        self.readyForUpdate = YES;
    }
}
-(void)updateSalonInfoWithSalon: (Salon *) salon{
    self.selectedSalon = salon;
    self.salonNameLabel.text = salon.salon_name;
    self.salonNameLabel.adjustsFontSizeToFitWidth=YES;
    self.addressLabel.text = [salon fetchFullAddress];
    
    CLLocationCoordinate2D newCoord;
    newCoord.latitude = salon.salon_lat;
    newCoord.longitude = salon.salon_long;
    [self.map setCenterCoordinate:newCoord animated:YES];
    self.addressLabel.numberOfLines=0;

    
    if (salon.salon_image.length <1) {
        self.salonImageView.image = kPlace_Holder_Image;
        self.salonImageView.contentMode=UIViewContentModeScaleAspectFill;
        
        
    }
    else{
        [self.salonImageView sd_setImageWithURL:[NSURL URLWithString:salon.salon_image] placeholderImage:kPlace_Holder_Image];
        
        
    }
    
}
#pragma mark - Views LifeCyle methods
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
    self.startLocation = self.map.userLocation.location;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    BOOL pushed = [self isMovingToParentViewController];
    if (!pushed) {

    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : kWhatSalonSubTextColor, NSFontAttributeName:[UIFont systemFontOfSize:17]}];
    
    self.navigationController.navigationBar.tintColor = kWhatSalonSubTextColor;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
    self.navigationItem.title=@"Map";

}

-(void)showMenu{
    [self.sideMenuViewController presentMenuViewController];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
 
    //ensure navigation bar is visible - it was made invisible in `BrowseViewController`
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    
    if (!IS_iPHONE4) {
       
        
        //TODO: added for WebSummit
        self.navigationItem.rightBarButtonItem=nil;
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.hidesBackButton = YES;
        if (!self.tabBarController) {
            
            UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Compass"] style:UIBarButtonItemStyleBordered target:self action:@selector(rightBarButton:)];
            self.navigationItem.leftBarButtonItem = rightButton;

        }
        else{
            UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_arrow"] style:UIBarButtonItemStyleBordered target:self action:@selector(backButton)];
            self.navigationItem.leftBarButtonItem = leftButton;
            [self.navigationItem.leftBarButtonItem setTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
        }
        
        
    }
   
    self.map.delegate=self;
    self.map.showsUserLocation=YES;
    self.map.showsPointsOfInterest=YES;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    
    
    if (self.salonArray.count==0) {
        self.salonNameLabel.hidden=YES;
        self.salonImageView.hidden=YES;
        self.addressLabel.hidden=YES;
        
        self.seeDIrectoryButton.hidden=NO;
        self.seeDirectoryDescription.hidden=NO;
    }
    else{
        
        self.seeDIrectoryButton.hidden=YES;
        self.seeDirectoryDescription.hidden=YES;
        self.salonNameLabel.hidden=NO;
        self.salonImageView.hidden=NO;
        self.addressLabel.hidden=NO;

        
        for (int i=0; i<self.salonArray.count; i++) {
            
            Salon * salon = [self.salonArray objectAtIndex:i];
            SalonAnnotation * sAnnot = [[SalonAnnotation alloc] init];
            [sAnnot setUpAnnotationWithSalon:salon];
            
           
            [self.map addAnnotation:sAnnot];
            
            if (i==0) {
                [self.map selectAnnotation:sAnnot animated:NO];
                self.nearestSalonTag = [salon.salon_id integerValue];
                [self updateSalonInfoWithSalon:salon];
            }
        }
        
    }
  
    self.map.zoomEnabled=NO;
    self.addressLabel.userInteractionEnabled=NO;
    
    UITapGestureRecognizer * goToDetailPage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDetail)];
    goToDetailPage.numberOfTapsRequired=1;
    [self.salonInfoHolder addGestureRecognizer:goToDetailPage];
    
    UIPanGestureRecognizer* panRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [panRec setDelegate:self];
    [self.map addGestureRecognizer:panRec];
    
    [Gradients addBlueToPurpleHorizontalLayerWithView:_gradientView];

    
}

-(void)showDetail{
    
    Salon *salon;
    for (Salon *s in self.salonArray) {
        if ([s.salon_id intValue] == [self.selectedSalon.salon_id intValue]) {
            salon=s;
        }
    }
    
   TabNewSalonDetailViewController * detailSalon = [self.storyboard instantiateViewControllerWithIdentifier:@"newSalonDetailVC"];
    detailSalon.salonData = salon;
    [WSCore setBookingType:BookingTypeBrowse];
    [self.navigationController pushViewController:detailSalon animated:YES];

}
#pragma mark Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation * location = [locations lastObject];
    
    if (location!=nil) {
        [self.locationManager stopUpdatingLocation];
    
        
        MKCoordinateRegion mapRegion;
        mapRegion.center = location.coordinate;
        mapRegion.span.latitudeDelta = 0.008;
        mapRegion.span.longitudeDelta = 0.008;
        self.map.delegate = self;
        [self.map setCenterCoordinate:location.coordinate];
        [self.map setRegion:mapRegion animated: NO];

      
    }
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    BOOL popped = [self isMovingFromParentViewController];
    
    printf("viewWillDisappear     %d\n", popped);
    if (popped) {
        self.map.showsUserLocation = NO;
        self.map.delegate = nil;
        [self.map removeFromSuperview];
        self.map = nil;

    }
   
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
}


-(void)popToBrowse{
    [self.navigationController popToRootViewControllerAnimated:NO];
}


-(void)backButton{
    [self popViewController];
}

- (void)popViewController {
    if (!IS_iPHONE4) {
        [UIView beginAnimations:@"View Flip" context:nil];
        [UIView setAnimationDuration:0.80];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        
        [UIView setAnimationTransition:
         UIViewAnimationTransitionFlipFromRight
                               forView:self.navigationController.view cache:NO];
        
        [self.navigationController popViewControllerAnimated:YES];
        [UIView commitAnimations];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)rightBarButton:(id)sender {
    
    [self popViewController];
    
}

#pragma mark - MKMapView delegate methods

- (MKAnnotationView *)mapView:(MKMapView *)mv viewForAnnotation:(id <MKAnnotation>)annotation
{

    
    NSString *annotationIdentifier = @"PinViewAnnotation";
    
    if (annotation == mv.userLocation){
    
        UserAnnotationView * customPinView = [[UserAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
        
        UIImageView * imageV = [[UIImageView alloc] init];
       
        if ([[User getInstance] isUserLoggedIn]) {
            
            
            [imageV sd_setImageWithURL:[[User getInstance] fetchImageURL ] placeholderImage:[UIImage imageNamed:kProfile_image] ];
        }else{
            
            imageV.image = [UIImage imageNamed:kProfile_image];
        }
        imageV.frame = CGRectMake(9, 7, 38, 38);
        
        imageV.layer.cornerRadius = roundf(imageV.frame.size.width/2.0);
        imageV.layer.masksToBounds = YES;
        imageV.contentMode=UIViewContentModeScaleAspectFill;
        [customPinView addSubview:imageV];
        
        return customPinView;
    }
    
    UIImageView *houseIconView = [[UIImageView alloc] init];
  
    SalonAnnotationView *pinView = [[SalonAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
  
    pinView.canShowCallout=NO;

    [houseIconView setFrame:CGRectMake(0, 0, 40, 40)];
    houseIconView.layer.cornerRadius=3.0;
    houseIconView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    houseIconView.layer.borderWidth=0.5;
    houseIconView.layer.masksToBounds=YES;
    houseIconView.contentMode=UIViewContentModeScaleAspectFill;
    
    pinView.leftCalloutAccessoryView = houseIconView;
   
    SalonAnnotation * salonAnnot = (SalonAnnotation *)annotation;
    [houseIconView sd_setImageWithURL:[NSURL URLWithString:salonAnnot.salon.salon_image]
                     placeholderImage:[UIImage imageNamed:@"empty_cell"]];
    pinView.tag=[salonAnnot.salon.salon_id intValue];

    if (!salonAnnot.salon.open) {
        
         pinView.salonImageType =kTier1SalonIcon;
    }
    else{
     
        int tierType = salonAnnot.salon.salon_tier;
        
        switch (tierType) {
            case 1:
                pinView.salonImageType =kTier1SalonIcon;
                break;
            case 2:
                pinView.salonImageType =kTier2SalonIcon;
                break;
            case 3:
                pinView.salonImageType =kTier3SalonIcon;
                break;
            default:
                
            pinView.salonImageType =kTier1SalonIcon;
                break;
        }
    
    }
    
    pinView.annotation=annotation;
    
    return pinView;
    
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [mapView deselectAnnotation:view.annotation animated:YES];
    
    NSLog(@"Did select annotation %@",[view class]);
    
    if ([view isKindOfClass:[SalonAnnotationView class]]) {
        Salon *salon;
        for (Salon *s in self.salonArray) {
            if ([s.salon_id intValue] == view.tag) {
                salon=s;
            }
        }
        [self updateSalonInfoWithSalon:salon];
    }
    
   
}


- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    
   
    for (MKAnnotationView *view in views) {
        view.alpha=0.0;
        
        [UIView animateWithDuration:0.5 animations:^{
            view.alpha=1.0;
        } completion:nil];
        
        
    }
}

- (IBAction)seeDirectory:(id)sender {
    [self performSegueWithIdentifier:@"showDIrectory" sender:self];
    
}
@end
