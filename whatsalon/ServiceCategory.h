//
//  ServiceCategory.h
//  whatsalon
//
//  Created by Graham Connolly on 30/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceCategory : NSObject
@property (nonatomic,strong) NSString * catID;
@property (nonatomic,strong) NSString * catName;
@property (nonatomic,strong) NSString * catService;

@end
