//
//  TabDirectorySalonTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 21/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "TabDirectorySalonTableViewCell.h"
#import "UIImage+ColoredImage.h"
#import "SalonTier4.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation TabDirectorySalonTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell: (SalonTier4 *) salon{
    
    self.salonTitle.text = salon.salonName;
    self.salonTitle.adjustsFontSizeToFitWidth=YES;
    self.distanceLabel.text = [NSString stringWithFormat:@"%.1f km", [salon.distance doubleValue ] ] ;
    
    self.salonAddress.text = salon.salonAddress;
    self.salonAddress.text  = [self.salonAddress.text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    self.salonAddress.numberOfLines=0;
    self.salonImage.layer.cornerRadius=kCornerRadius;
    self.salonImage.layer.masksToBounds=YES;
    self.salonImage.image = kPlace_Holder_Image;
    self.distanceLabel.textColor = kWhatSalonBlue;
    self.distanceLabel.font = [UIFont boldSystemFontOfSize:self.distanceLabel.font.pointSize];
    
   
}
@end
