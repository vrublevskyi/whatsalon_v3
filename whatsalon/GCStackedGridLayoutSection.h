//
//  GCStackedGridLayoutSection.h
//  StaggeredCollectionView
//
//  Created by Graham Connolly on 21/10/2014.
//  Copyright (c) 2014 Graham Connolly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface GCStackedGridLayoutSection : NSObject

/*
 What you need to do is to work out exactly where each cell will be displayed so that you can tell the collection view how big its content is, and eventually, where to display each cell.
 The best way to do this is to pre-calculate it all when the layout is prepared. So the first thing you need is a data model to store the layout information. I’m going to show you how to create such a data model using a section object.
 The layout will create a section object for every section that exists. As the layout is prepared, each section object will be given items as their sizes are determined via the delegate. It will put each item into whichever column is currently the shortest.
 
 The section will also be responsible for working out the frame of each item. The layout can then ask for the frame when it needs to display the cells. Remember that the frame is the location of the view within the coordinate system of the collection view.
 
 This class will hold information about each item in a section and report back each
 item’s frame.
 */
@property (nonatomic,assign,readonly) CGRect frame;
@property (nonatomic,assign,readonly) UIEdgeInsets itemInsets;
@property (nonatomic,assign,readonly) CGFloat columnWidth;
@property (nonatomic,assign,readonly) NSInteger numberOfItems;

-(id) initWithOrigin: (CGPoint) origin with: (CGFloat) width columns: (NSInteger) columns itemInsets: (UIEdgeInsets) itemInsets;

-(void)addItemOfSize: (CGSize) size forIndex: (NSInteger) index;

-(CGRect) frameForItemAtIndex: (NSInteger) index;


@end
