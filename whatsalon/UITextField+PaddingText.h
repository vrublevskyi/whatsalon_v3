//
//  UITextField+PaddingText.h
//  WhatSalonCompanion
//
//  Created by Graham Connolly on 01/07/2014.
//  Copyright (c) 2014 Graham Connolly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (PaddingText)

-(void) setLeftPadding:(int) paddingValue;

-(void) setRightPadding:(int) paddingValue;

@end
