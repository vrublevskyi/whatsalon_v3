//
//  SalonImageGalleryScrollView.m
//  whatsalon
//
//  Created by Graham Connolly on 16/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonImageGalleryScrollView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define kScrollViewPadding 20

@implementation SalonImageGalleryScrollView

/*
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.salonImages = [NSMutableArray array];
    }
    return self;
}*/

-(id)init{
    
    self=[super init];
    if (self) {
        self.salonImages = [NSMutableArray array];
    }
    
    return self;
}

-(void)createPages{
    for (NSInteger i = 0; i < self.numberOfPages; i++) {
        [self.pageViews addObject:[NSNull null]];
    }
}
-(void)loadPage:(NSInteger)page{
    if (page < 0 || (page >= self.salonImages.count)) {
        //if its outside the range of what you have to display, then do nothing
        return;
    }
    
    UIView * pageView = [self.pageViews objectAtIndex:page];
    
    if ((NSNull*) pageView == [NSNull null]) {
        
        CGRect frame = self.parentsFrame;//set to views bounds instead of scrollviews bounds because it doesnt work very well with autolayout
        frame.origin.x = (frame.size.width+kScrollViewPadding) * page;
        frame.origin.y = 0.0f;
        
        UIImageView * newPageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"empty_cell"]];
        [newPageView sd_setImageWithURL:[self.salonImages objectAtIndex:page] placeholderImage:[UIImage imageNamed:@"black_placeholder.jpg"]];
        
        newPageView.contentMode = UIViewContentModeScaleToFill;
        newPageView.frame = CGRectMake(frame.origin.x,frame.origin.y,frame.size.width, frame.size.height);
        
        [self.parent addSubview:newPageView];//parent
        
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
    
    
}

-(void)purgePage:(NSInteger)page{
    if (page <0 || page >= self.salonImages.count) {
        //if it's outside the range of what you have to display, then do nothing
        return;
    }
    
    //Remove a page from the scroll view and reset the container array
    UIView * pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull *)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        pageView = nil;
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
        
    }
    
}

-(void)loadVisiblePages{
    //first determine which page is currently visible
    CGFloat pageWidth = self.parent.frame.size.width;
    NSInteger page = (NSInteger)floor((self.parent.contentOffset.x * 2.0f + pageWidth)/(pageWidth * 2.0f));
    
    //self.pageControl.currentPage = page;
    
    NSInteger firstPage = page -1;
    NSInteger lastPage = page+1;
    
    //purge anything before the first page
    for (NSInteger i = 0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
    //load pages in our range
    for (NSInteger i = firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    //purge anything after the last page
    for (NSInteger i = lastPage +1; i<self.salonImages.count; i++) {
        [self purgePage:i];
    }
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [self loadVisiblePages];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
