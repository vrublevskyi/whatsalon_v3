//
//  SearchServiceVC.swift
//  whatsalon
//
//  Created by admin on 10/5/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit
import CoreLocation

@objc protocol SearchServiceVCDelegate: class {
    func onShowLocationSelect(on controller: SearchServiceVC)
    func currentLocationTapped(on controller: SearchServiceVC)
    func selectedSearchedService(salon: Salon, on controller: SearchServiceVC)
    func didSelectCategory(category: [String : String], on controller: SearchServiceVC)
}

//change to navigation controller
 class SearchServiceVC: UIViewController {
    
    //MARK: - Outlets
    
//    @IBOutlet weak var searchView: SearchView! {
//        didSet {
//            searchView.delegate = self
//        }
//    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(cellType: CategoriesSearchCell.self)
        }
    }
    
    //MARK: - Properties
    var locationManager:CLLocationManager!
    var userLocation :CLLocation?
    var selectedCell :IndexPath?
    @objc weak var delegate: SearchServiceVCDelegate?

    var categories:[Int:Dictionary<String, String>]? {
        didSet {
            tableView.reloadData()
        }
    }
    //remove from controller
    var selectedCategoriesLogos = ["hairGradientLogo", "hairRemoveGradientLogo", "waxGraidentLogo", "massageGradientLogo", "faceGradientLogo","bodyGradientLogo"]
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super .viewDidLoad()
        
        //чому не працює через дідсет?
        categories = [0:["Hair":"hair_service_icon"],
                      1:["Hair Removal":"wax_service_icon"],
                      2:["Nails":"nail_service_icon"],
                      3:["Massage":"massage_service_icon"],
                      4:["Face":"face_service_icon"],
                      5:["Body":"body_service_icon"]]
     

        self.locationmanagerDelegate()
    }
    
    private func locationmanagerDelegate() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
    }

}

//SearchViewDelegate

//TODO: === MOVE ALL actions to BASE VC
extension SearchServiceVC: SearchViewDelegate {
    func searchByTextTapped(_ text: String, on controller: SearchView) {
        
    }
    
    func currentLocationTapped(on controller: SearchView) {
        let searchVC = TabSearchSalonsTableViewController()
        searchVC.viewDidLoad()
        searchVC.networkManager.delegate = self
//        searchVC.search(forSalon: "ant", location: self.userLocation)
//        searchVC.search(forSalon: "", location: self.userLocation)
    }
    
    func locationTapped(on controller: SearchView) {
        let locationVC = AddAddressVC(nibName: "AddAddressVC", bundle: nil)
        locationVC.delegate = self
        self.present(locationVC, animated: true, completion: nil)

    }
}

extension SearchServiceVC:AddAddressVCDelegate {
    func onAddressSelected(location: CLLocation, address: String, on controller: AddAddressVC) {
        self.userLocation = location
//        self.searchView.searchedlocations = address
        controller.dismiss(animated: true, completion: nil)
    }
}

//tableView
extension SearchServiceVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CategoriesSearchCell = tableView.dequeueReusableCell(for: indexPath)
        let category = categories?[indexPath.row]
        
        let categoryName = category?.map{$0.key}.first
        let categoryImage = category?.map{$0.value}.first

        cell.category = categoryName
        cell.logoName = categoryImage
        
        //change to norm code
        if selectedCell == indexPath {
            cell.categortSelected = true
            cell.logoName = selectedCategoriesLogos[indexPath.row]
        } else {
            cell.categortSelected = false

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = categories?[indexPath.row]
        let categoryName = category?.map{$0.key}.first
        
//        searchView.searchedCategory =  " " + (categoryName ?? "")
//        searchView.searchIsActive = true
        selectedCell = indexPath
        tableView.reloadData()
        delegate?.didSelectCategory(category: category ?? [:], on: self)
    }
}

//locationManger

extension SearchServiceVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         self.userLocation = locations[0] as CLLocation
    }
}

extension SearchServiceVC: GCNetworkManagerDelegate {
    func manager(_ manager: GCNetworkManager!, handleFailureMessageWitDictionary jsonDictionary: [AnyHashable : Any]!) {
        let locationVC = SearchResultsVC(nibName: "SearchResultsVC", bundle: nil)
        self.present(locationVC, animated: true, completion: nil)
    }
    func manager(_ manager: GCNetworkManager!, handleSuccessMessageWith jsonDictionary: [AnyHashable : Any]!) {
      showResultsFor(jsonDictionary)
    }
    
    private func showResultsFor(_ search: [AnyHashable : Any]) {
        let searchVC = TabSearchSalonsTableViewController()
        searchVC.handleSalonSearchSuccessResult(search)
        
        let resultsVC = SearchResultsVC(nibName: "SearchResultsVC", bundle: nil)
        resultsVC.salons = searchVC.filteredSalons as? [Salon]
        resultsVC.delegate = self
//        resultsVC.searchedCategory = self.searchView.searchedCategory
        self.present(resultsVC, animated: true, completion: nil)
    }
}

extension SearchServiceVC: SearchResultsVCDelegate {
    func searchedSalonSelected(_ salon: Salon, on controller: SearchResultsVC) {
        controller.dismiss(animated: false) {
           self.delegate?.selectedSearchedService(salon: salon, on: self)
        }
    }    
}
