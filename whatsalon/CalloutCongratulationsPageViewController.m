//
//  CalloutCongratulationsPageViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 10/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CalloutCongratulationsPageViewController.h"
#import "CallOutOptionsViewController.h"
#import "UILabel+Boldify.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>


@interface CalloutCongratulationsPageViewController ()
- (IBAction)close:(id)sender;
@property (nonatomic) UILabel *summaryLabel;
@property (weak, nonatomic) IBOutlet UILabel *chargeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addReminderLabel;
@property (nonatomic) UIImageView * backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *reminderImageView;
@property (weak, nonatomic) IBOutlet UILabel *thanksLabel;

//reminder
@property (nonatomic,strong) EKEventStore * eventStore;
@property BOOL eventStoreAccessGranted;
@property (weak, nonatomic) IBOutlet UIView *reminderView;
- (IBAction)cancelReminder:(id)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *reminderDatePicker;
- (IBAction)setReminder:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelReminder;
@property (weak, nonatomic) IBOutlet UIButton *setReminderButton;
@property (nonatomic) UIView * coverView;


@end

@implementation CalloutCongratulationsPageViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore changeNavigationTitleColor:[UIColor whiteColor] OnViewController:self];
    [WSCore statusBarColor:StatusBarColorWhite];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=kBackgroundColor;
    
    
    self.addReminderLabel.textColor=kWhatSalonBlue;
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:self.backgroundImageView];
    [self.view sendSubviewToBack:self.backgroundImageView];
    if (IS_IOS_8_OR_LATER) {
        self.backgroundImageView.image=[UIImage imageNamed:@"salon1.jpg"];
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = self.backgroundImageView.bounds;
        
        [self.backgroundImageView addSubview:visualEffectView];
    }
    else{
        UIView * blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        blackView.backgroundColor=[UIColor blackColor];
        blackView.alpha=0.9;
        [self.backgroundImageView insertSubview:blackView atIndex:0];
        self.backgroundImageView.image =[UIImage imageNamed:@"salon1.jpg"];
        
    }
    
    self.summaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 125, 288, 138)];
    self.summaryLabel.text=@"Callout service booked & confirmed for a massage with Sarah O Leary at 31 South Lodge,Browningstown Douglas, Cork, Today 3 p.m. for 1 person.";
    self.summaryLabel.textAlignment=NSTextAlignmentCenter;
    self.summaryLabel.numberOfLines=0;
    [self.view addSubview:self.summaryLabel];
    self.summaryLabel.center=CGPointMake(self.view.center.x, self.summaryLabel.center.y);
    [WSCore addTopLine:self.view :[UIColor whiteColor] :0.5 WithYvalue:kNavBarAndStatusBarHeight];
    self.title=@"Congratulations";
    
    self.chargeLabel.textColor=[UIColor cloudsColor];
    self.summaryLabel.textColor=[UIColor cloudsColor];
    self.thanksLabel.textColor=[UIColor cloudsColor];
    
    self.reminderImageView.userInteractionEnabled=YES;
    self.addReminderLabel.userInteractionEnabled=YES;
    
    UITapGestureRecognizer * tapLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addReminder)];
    tapLabel.numberOfTapsRequired=1;
    
    UITapGestureRecognizer * tapImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addReminder)];
    tapLabel.numberOfTapsRequired=1;
    
    [self.reminderImageView addGestureRecognizer:tapImageView];
    [self.addReminderLabel addGestureRecognizer:tapLabel];
    
    
    
}

-(void)addReminder{
 
    self.coverView = [[UIView alloc] initWithFrame:self.view.frame];
    self.coverView.backgroundColor=[UIColor blackColor];
    self.coverView.alpha=0.6;
    [self.view addSubview:self.coverView];
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(10, 161, self.view.frame.size.width-10, 223)];
    view.backgroundColor = [UIColor lightGrayColor];
    view.layer.cornerRadius=kCornerRadius;
    view.clipsToBounds=YES;
    
    
    UIDatePicker * dp = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 162)];
    dp.datePickerMode=UIDatePickerModeDateAndTime;
    dp.minimumDate=[NSDate date];
    //NSLog(@"%@",self.chosenDate);
    //dp.date=self.chosenDate;
    // dp.date =
    self.reminderDatePicker = dp;
    [view addSubview:dp];
    
    
    UIButton * reminderButton = [UIButton buttonWithType:UIButtonTypeSystem];
    reminderButton.backgroundColor = kWhatSalonBlue;
    [reminderButton addTarget:self action:@selector(setReminder:) forControlEvents:UIControlEventTouchUpInside];
    [reminderButton setTitle:@"Set Reminder" forState:UIControlStateNormal];
    [reminderButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    reminderButton.frame = CGRectMake(view.frame.size.width/2, view.frame.size.height-44, view.frame.size.width/2, 44);
    self.setReminderButton=reminderButton;
    [view addSubview:self.setReminderButton];
    
    UIButton * cancelButton = [UIButton buttonWithType:UIButtonTypeSystem];
    cancelButton.backgroundColor = kWhatSalonBlue;
    [cancelButton addTarget:self action:@selector(cancelReminder:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(0, view.frame.size.height-44, view.frame.size.width/2, 44);
    [WSCore addRightLineToView:cancelButton withWidth:0.5f];
    self.cancelReminder = cancelButton;
    [view addSubview:self.cancelReminder];
    
    self.reminderView = view;
    
    [self.view addSubview:self.reminderView];
    self.reminderView.center=self.view.center;
    //[self disableControls];

}

-(void)createReminder
{
    EKReminder *reminder = [EKReminder
                            reminderWithEventStore:self.eventStore];
    
    reminder.title = self.summaryLabel.text;
    
    reminder.calendar = [_eventStore defaultCalendarForNewReminders];
    
    NSDate *date = [self.reminderDatePicker date];
    NSLog(@"Reminder date %@",date);
    EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:date];
    
    [reminder addAlarm:alarm];
    
    NSError *error = nil;
    
    [_eventStore saveReminder:reminder commit:YES error:&error];
    
    if (error){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"%@",[error localizedDescription]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            alert.tag=1;
            NSLog(@"error = %@", error);
        });
        
    }
    else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Reminder" message:@"Reminder set" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
            NSLog(@"%@",[self.reminderDatePicker date]);
            [self enableControls];
        });
        
    }
    
    
}

-(void)cancelReminder:(id)sender{
    self.coverView.hidden=YES;
    self.reminderView.hidden=YES;
    
    [self.coverView removeFromSuperview];
    [self.reminderView removeFromSuperview];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        if (buttonIndex ==[alertView cancelButtonIndex]) {
            [self enableControls];
        }
    }
    
}

- (IBAction)setReminder:(id)sender {
    
    if (self.eventStore == nil)
    {
        self.eventStore = [[EKEventStore alloc]init];
        
        [self.eventStore requestAccessToEntityType:EKEntityTypeReminder completion:^(BOOL granted, NSError *error) {
            
            if (!granted){
                
                [WSCore dismissNetworkLoadingOnView:self.view];
            }
            
            if (_eventStore!=nil) {
                
                [self createReminder];
                
            }
        }];
    }
    
    
    
}

-(void)enableControls{
    
    self.coverView.hidden=YES;
    self.reminderView.hidden=YES;
    
    [self.coverView removeFromSuperview];
    [self.reminderView removeFromSuperview];

    self.addReminderLabel.userInteractionEnabled=YES;
    self.addReminderLabel.alpha=1.0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)close:(id)sender {
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        
        if ([controller isKindOfClass:[CallOutOptionsViewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
        }
    }
}
@end
