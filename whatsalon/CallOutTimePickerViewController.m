//
//  CallOutTimePickerViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 03/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CallOutTimePickerViewController.h"
#import "FUIButton.h"
#import "UISlider+FlatUI.h"
#import "CalloutEnterAddressViewController.h"

@interface CallOutTimePickerViewController ()<EnterAddressDelegate>
@property (weak, nonatomic) IBOutlet UISlider *timeSlider;
- (IBAction)timeSliderChange:(UISlider *)sender;

@property (nonatomic) NSDateFormatter *formatter;
@property (weak, nonatomic) IBOutlet UILabel *serviceTitle;
@property (weak, nonatomic) IBOutlet UILabel *serviceSubtitle;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic) NSMutableArray *buttonsArrays;
@property (nonatomic) NSDate * selectedDate;
@property (nonatomic) NSMutableArray * dateArray;
@property (weak, nonatomic) IBOutlet UILabel *enterAddressLabel;
@property (nonatomic) NSDate * bookingSlot;
@end

@implementation CallOutTimePickerViewController


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore transparentNavigationBarOnView:self];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
}

- (void)timeSliderInit {
    //[self.timeSlider configureFlatSliderWithTrackColor:kBackgroundColor progressColor:kBackgroundColor thumbColor:[UIColor peterRiverColor]];
    self.timeSlider.maximumValue=42;//24hr clock -
    self.timeSlider.minimumValue=12;
    
    self.formatter = [NSDateFormatter new];
    [self.formatter setLocale:[NSLocale currentLocale]];
    [self.formatter setDateStyle:NSDateFormatterNoStyle];
    [self.formatter setTimeStyle:NSDateFormatterShortStyle];
    
    NSDate *today2 = [NSDate date];
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    // display in 12HR/24HR (i.e. 11:25PM or 23:25) format according to User Settings
    [dateFormatter2 setTimeStyle:NSDateFormatterShortStyle];
    NSString *currentTime = [dateFormatter2 stringFromDate:today2];
    
    double intervalSlot = [currentTime doubleValue]*2;
    
    
    self.timeSlider.value=(float)intervalSlot;
    NSLog(@"Intervale Slot %f",(float)intervalSlot);
    NSLog(@"User's current time in their preference format:%@",currentTime);
    // Lazy way to set up the initial time
    [self timeSliderChange:self.timeSlider];
    
    CGRect sliderWidth = CGRectMake(0, 0, self.timeSlider.frame.size.width, 4);
    UIImage * image = [self imageWithColor:kBackgroundColor OfSize:sliderWidth];
    
    CGRect circleRect =CGRectMake(0, 0, 30, 30);
    UIImage * circleImage = [self imageWithColor:kWhatSalonBlue OfSize:circleRect];
    UIImage * roundedImage = [self imageWithRoundedCornersSize:circleRect.size.width/2.0f usingImage:circleImage];
    
    [[UISlider appearance] setMaximumTrackImage:image forState:UIControlStateNormal];
    [[UISlider appearance] setMinimumTrackImage:image forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:roundedImage forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:roundedImage forState:UIControlStateHighlighted];
}

- (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original
{
    CGRect frame = CGRectMake(0, 0, original.size.width, original.size.height);
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContextWithOptions(original.size, NO, 1.0);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:frame
                                cornerRadius:cornerRadius] addClip];
    // Draw your image
    [original drawInRect:frame];
    
    // Get the image, here setting the UIImageView image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return image;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    self.backgroundImage.image = [UIImage imageNamed:@"salon1.jpg"];

    [WSCore addDarkBlurAsSubviewToView:self.backgroundImage];
    
    NSDateComponents* deltaComps = [[NSDateComponents alloc] init] ;
    [deltaComps setDay:1];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE d"];
    [deltaComps setDay:2];
   
    
    self.dateArray = [[NSMutableArray alloc] initWithObjects:[NSDate date], nil];
    for (int i=1; i<7; i++) {
        [deltaComps setDay:i];
        NSDate* date = [[NSCalendar currentCalendar] dateByAddingComponents:deltaComps toDate:[NSDate date] options:0];
        [self.dateArray addObject:date];
    }
    
   // NSMutableArray * dateArray = [[NSMutableArray alloc] initWithArray:[WSCore getDaysOfTheWeek]];
    
    CGFloat buttonW=65;
    CGFloat buttonH=65;
    
    CGFloat buttonSpace = buttonW*4;
    CGFloat spacing = (self.view.frame.size.width-buttonSpace)/5;
    
    CGFloat buttonSpaceBottom = buttonW*3;
    CGFloat spacingBottom = (self.view.frame.size.width-buttonSpaceBottom)/4;
    
    
    NSLog(@"Spacing %f and buttonSpace %f",spacing,buttonSpace);
    UIButton * button = [[UIButton alloc] initWithFrame:CGRectMake(spacing, kNavBarAndStatusBarHeight+self.dateLabel.frame.size.height+(spacing*3), buttonW, buttonH)];
    [button setTitle:[NSString stringWithFormat:@"%@%@",[dateFormatter stringFromDate:self.dateArray[0]],[WSCore daySuffixForDate:self.dateArray[0]]] forState:UIControlStateNormal];
    button.layer.cornerRadius = buttonW/2;
    button.layer.borderColor=kWhatSalonBlue.CGColor;
    [button setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    button.layer.borderWidth=1.0f;
    button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    button.titleLabel.textAlignment=NSTextAlignmentCenter;
    button.titleLabel.font=[UIFont systemFontOfSize:21.0f];
    button.tag=0;
    [self.view addSubview:button];
    self.selectedDate=self.dateArray[0];
    
    
    UIButton * button2 = [[UIButton alloc] initWithFrame:CGRectMake(spacing+buttonW+spacing, kNavBarAndStatusBarHeight+self.dateLabel.frame.size.height+(spacing*3), buttonW, buttonH)];
    button2.layer.cornerRadius = buttonW/2;
    [button2 setTitle:[NSString stringWithFormat:@"%@%@",[dateFormatter stringFromDate:self.dateArray[1]],[WSCore daySuffixForDate:self.dateArray[1]]]  forState:UIControlStateNormal];
    button2.layer.borderColor=[UIColor cloudsColor].CGColor;
    button2.layer.borderWidth=1.0f;
    button2.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button2.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    button2.titleLabel.textAlignment=NSTextAlignmentCenter;
     button2.titleLabel.font=[UIFont systemFontOfSize:21.0f];
    button2.tag=1;
    [self.view addSubview:button2];
    
    UIButton * button3 = [[UIButton alloc] initWithFrame:CGRectMake((spacing*3)+(buttonW*2), kNavBarAndStatusBarHeight+self.dateLabel.frame.size.height+(spacing*3), buttonW, buttonH)];
    button3.layer.cornerRadius = buttonW/2;
    [button3 setTitle:[NSString stringWithFormat:@"%@%@",[dateFormatter stringFromDate:self.dateArray[2]],[WSCore daySuffixForDate:self.dateArray[2]]]  forState:UIControlStateNormal];
    button3.layer.borderColor=[UIColor cloudsColor].CGColor;
    button3.layer.borderWidth=1.0f;
    button3.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button3.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    button3.titleLabel.textAlignment=NSTextAlignmentCenter;
    button3.titleLabel.font=[UIFont systemFontOfSize:21.0f];
    button3.tag=2;

    [self.view addSubview:button3];
    
    UIButton * button4 = [[UIButton alloc] initWithFrame:CGRectMake((spacing*4)+(buttonW*3), kNavBarAndStatusBarHeight+self.dateLabel.frame.size.height+(spacing*3), buttonW, buttonH)];
    button4.layer.cornerRadius = buttonW/2;
    [button4 setTitle:[NSString stringWithFormat:@"%@%@",[dateFormatter stringFromDate:self.dateArray[3]],[WSCore daySuffixForDate:self.dateArray[3]]]  forState:UIControlStateNormal];
    button4.layer.borderColor=[UIColor cloudsColor].CGColor;
    button4.layer.borderWidth=1.0f;
    button4.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button4.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    button4.titleLabel.textAlignment=NSTextAlignmentCenter;
     button4.titleLabel.font=[UIFont systemFontOfSize:21.0f];
    button4.tag=3;

    [self.view addSubview:button4];

    
    UIButton * button6 = [[UIButton alloc] initWithFrame:CGRectMake(spacingBottom+buttonW+spacingBottom, kNavBarAndStatusBarHeight+self.dateLabel.frame.size.height+(spacing*3)+(spacing + buttonH), buttonW, buttonH)];
    [button6 setTitle:[NSString stringWithFormat:@"%@%@",[dateFormatter stringFromDate:self.dateArray[5]],[WSCore daySuffixForDate:self.dateArray[5]]]  forState:UIControlStateNormal];
    button6.layer.cornerRadius = buttonW/2;
    button6.layer.borderColor=[UIColor cloudsColor].CGColor;
    button6.layer.borderWidth=1.0f;
    button6.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button6.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    button6.titleLabel.textAlignment=NSTextAlignmentCenter;
    button6.titleLabel.font=[UIFont systemFontOfSize:21.0f];
    button6.tag=5;
    [self.view addSubview:button6];
    button6.center = CGPointMake(self.view.center.x, button6.center.y);
    NSLog(@"Button 6 x %f",button6.frame.origin.x);
    
    UIButton * button5 = [[UIButton alloc] initWithFrame:CGRectMake(button6.frame.origin.x-spacing-buttonW, kNavBarAndStatusBarHeight+self.dateLabel.frame.size.height+(spacing*3)+(spacing + buttonH), buttonW, buttonH)];
    [button5 setTitle:[NSString stringWithFormat:@"%@%@",[dateFormatter stringFromDate:self.dateArray[4]],[WSCore daySuffixForDate:self.dateArray[4]]]  forState:UIControlStateNormal];
    button5.layer.cornerRadius = buttonW/2;
    button5.layer.borderColor=[UIColor cloudsColor].CGColor;
    button5.layer.borderWidth=1.0f;
    button5.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button5.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    button5.titleLabel.textAlignment=NSTextAlignmentCenter;
    button5.titleLabel.font=[UIFont systemFontOfSize:21.0f];
    button5.tag=4;
    [self.view addSubview:button5];
    NSLog(@"Button 5 frame %@",NSStringFromCGRect(button5.frame));

    
    UIButton * button7 = [[UIButton alloc] initWithFrame:CGRectMake(button6.frame.origin.x+spacing+buttonW, kNavBarAndStatusBarHeight+self.dateLabel.frame.size.height+(spacing*3)+(spacing + buttonH), buttonW, buttonH)];
    [button7 setTitle:[NSString stringWithFormat:@"%@%@",[dateFormatter stringFromDate:self.dateArray[6]],[WSCore daySuffixForDate:self.dateArray[6]]]  forState:UIControlStateNormal];
    button7.layer.cornerRadius = buttonW/2;
    button7.layer.borderWidth=1.0f;
    button7.layer.borderColor=[UIColor cloudsColor].CGColor;
    button7.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button7.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
    button7.titleLabel.textAlignment=NSTextAlignmentCenter;
    button7.tag=6;
    [self.view addSubview:button7];
     button7.titleLabel.font=[UIFont systemFontOfSize:21.0f];
    
    [dateFormatter setDateFormat:@"EEEE, MMMM d"];
    self.dateLabel.text =[NSString stringWithFormat:@"%@%@",[dateFormatter stringFromDate:self.dateArray[0]],[WSCore daySuffixForDate:self.dateArray[0]]];
    self.dateLabel.textColor=[UIColor cloudsColor];
    
    [self timeSliderInit];
    
    CGFloat timeSlotSpacing = self.timeSlider.frame.size.width/5;
    NSLog(@"timeslot spacing %f",timeSlotSpacing);
    UIView * segmentView = [[UIView alloc] initWithFrame:CGRectMake(self.timeSlider.frame.origin.x, self.timeSlider.frame.origin.y, 2.0, self.timeSlider.frame.size.height)];
    segmentView.backgroundColor=kBackgroundColor;
    [self.view insertSubview:segmentView belowSubview:self.timeSlider];
      NSLog(@"Segment %@",NSStringFromCGRect(segmentView.frame));
    
    UILabel * sixAm = [[UILabel alloc] initWithFrame:CGRectMake(segmentView.frame.origin.x-1, segmentView.frame.origin.y-20, 20, 20)];
    sixAm.text = @"6";
    //sixAm.textAlignment=NSTextAlignmentCenter;
    sixAm.font=[UIFont systemFontOfSize:15.0f];
    sixAm.textColor=[UIColor whiteColor];
    [self.view addSubview:sixAm];
    
    
    UIView * segmentView2 = [[UIView alloc] initWithFrame:CGRectMake(self.timeSlider.frame.origin.x+timeSlotSpacing, self.timeSlider.frame.origin.y, 2.0, self.timeSlider.frame.size.height)];
    segmentView2.backgroundColor=kBackgroundColor;
    [self.view insertSubview:segmentView2 belowSubview:self.timeSlider];
    
      NSLog(@"Segment %@",NSStringFromCGRect(segmentView2.frame));
    UILabel * nineAm = [[UILabel alloc] initWithFrame:CGRectMake(segmentView2.frame.origin.x-2, segmentView2.frame.origin.y-20, 20, 20)];
    nineAm.text = @"9";
    //nineAm.textAlignment=NSTextAlignmentCenter;
    nineAm.font=[UIFont systemFontOfSize:15.0f];
    nineAm.textColor=[UIColor whiteColor];
    [self.view addSubview:nineAm];
    
    UIView * segmentView3 = [[UIView alloc] initWithFrame:CGRectMake(self.timeSlider.frame.origin.x+timeSlotSpacing+timeSlotSpacing, self.timeSlider.frame.origin.y, 2.0, self.timeSlider.frame.size.height)];
    segmentView3.backgroundColor=kBackgroundColor;
    [self.view insertSubview:segmentView3 belowSubview:self.timeSlider];
      NSLog(@"Segment %@",NSStringFromCGRect(segmentView3.frame));
    
    UILabel * twelveAm = [[UILabel alloc] initWithFrame:CGRectMake(segmentView3.frame.origin.x-6, segmentView3.frame.origin.y-20, 20, 20)];
    twelveAm.text = @"12";
    //nineAm.textAlignment=NSTextAlignmentCenter;
    twelveAm.font=[UIFont systemFontOfSize:15.0f];
    twelveAm.textColor=[UIColor whiteColor];
    [self.view addSubview:twelveAm];
    
    UIView * segmentView4 = [[UIView alloc] initWithFrame:CGRectMake(self.timeSlider.frame.origin.x+timeSlotSpacing+timeSlotSpacing+timeSlotSpacing, self.timeSlider.frame.origin.y, 2.0, self.timeSlider.frame.size.height)];
    segmentView4.backgroundColor=kBackgroundColor;
    [self.view insertSubview:segmentView4 belowSubview:self.timeSlider];
      NSLog(@"Segment %@",NSStringFromCGRect(segmentView4.frame));
    
    UILabel * threePm = [[UILabel alloc] initWithFrame:CGRectMake(segmentView4.frame.origin.x-2, segmentView4.frame.origin.y-20, 20, 20)];
    threePm.text = @"3";
    //nineAm.textAlignment=NSTextAlignmentCenter;
    threePm.font=[UIFont systemFontOfSize:15.0f];
    threePm.textColor=[UIColor whiteColor];
    [self.view addSubview:threePm];
    
    UIView * segmentView5 = [[UIView alloc] initWithFrame:CGRectMake(self.timeSlider.frame.origin.x+timeSlotSpacing+timeSlotSpacing+timeSlotSpacing+timeSlotSpacing, self.timeSlider.frame.origin.y, 2, self.timeSlider.frame.size.height)];
    segmentView5.backgroundColor=kBackgroundColor;
    [self.view insertSubview:segmentView5 belowSubview:self.timeSlider];
      NSLog(@"Segment %@",NSStringFromCGRect(segmentView5.frame));
    
    
    UILabel * sixPm = [[UILabel alloc] initWithFrame:CGRectMake(segmentView5.frame.origin.x-2, segmentView5.frame.origin.y-20, 20, 20)];
    sixPm.text = @"6";
    //nineAm.textAlignment=NSTextAlignmentCenter;
    sixPm.font=[UIFont systemFontOfSize:15.0f];
    sixPm.textColor=[UIColor whiteColor];
    [self.view addSubview:sixPm];
    
    UIView * segmentView6 = [[UIView alloc] initWithFrame:CGRectMake(self.timeSlider.frame.origin.x+timeSlotSpacing+timeSlotSpacing+timeSlotSpacing+timeSlotSpacing+timeSlotSpacing-2, self.timeSlider.frame.origin.y, 2, self.timeSlider.frame.size.height)];
    segmentView6.backgroundColor=kBackgroundColor;
    [self.view insertSubview:segmentView6 belowSubview:self.timeSlider];
    
    NSLog(@"Segment %@",NSStringFromCGRect(segmentView6.frame));
    UILabel * ninePm = [[UILabel alloc] initWithFrame:CGRectMake(segmentView6.frame.origin.x-1, segmentView6.frame.origin.y-20, 20, 20)];
    ninePm.text = @"9";
    //nineAm.textAlignment=NSTextAlignmentCenter;
    ninePm.font=[UIFont systemFontOfSize:14.0f];
    ninePm.textColor=[UIColor whiteColor];
    [self.view addSubview:ninePm];
    
   self.moonImageView.image = [self.moonImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.moonImageView setTintColor:[UIColor whiteColor]];
    
    self.sunImageView.image = [self.sunImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.sunImageView setTintColor:[UIColor whiteColor]];
    
    [button addTarget:self action:@selector(timeSelected:) forControlEvents:UIControlEventTouchUpInside];
    [button2 addTarget:self action:@selector(timeSelected:) forControlEvents:UIControlEventTouchUpInside];
    [button3  addTarget:self action:@selector(timeSelected:) forControlEvents:UIControlEventTouchUpInside];
    [button4 addTarget:self action:@selector(timeSelected:) forControlEvents:UIControlEventTouchUpInside];
    [button5 addTarget:self action:@selector(timeSelected:) forControlEvents:UIControlEventTouchUpInside];
    [button6  addTarget:self action:@selector(timeSelected:) forControlEvents:UIControlEventTouchUpInside];
    [button7 addTarget:self action:@selector(timeSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    self.buttonsArrays = [[NSMutableArray alloc] initWithObjects:button,button2,button3,button4,button5,button6,button7, nil];

    self.bookButton.buttonColor=kWhatSalonBlue;
    self.bookButton.shadowColor=kWhatSalonBlueShadow;
    self.bookButton.cornerRadius=kCornerRadius;
    self.bookButton.shadowHeight=kShadowHeight;
    self.bookButton.tag=0;
    [self.bookButton setTitle:@"Book" forState:UIControlStateNormal];
    [self.bookButton setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.view addSubview:self.bookButton];
    
    self.enterAddressLabel.textColor=[UIColor cloudsColor];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toAddress)];
    tap.numberOfTapsRequired=1;
    [self.enterAddressLabel addGestureRecognizer:tap];
    self.enterAddressLabel.userInteractionEnabled=YES;
    
    [WSCore addTopLine:self.enterAddressLabel :[UIColor whiteColor] :0.5];
    
    [WSCore addBottomLine:self.enterAddressLabel :[UIColor whiteColor]];

    
    self.serviceSubtitle.textColor=[UIColor whiteColor];
    self.serviceTitle.textColor=[UIColor whiteColor];
    
    }

- (UIImage *)imageWithColor:(UIColor *)color OfSize: (CGRect) rect
{
    //CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
-(void)toAddress{
    
    CalloutEnterAddressViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"toAddressVC"];
    cvc.delegate=self;
    
    [self.navigationController pushViewController:cvc animated:YES];
    //[self performSegueWithIdentifier:@"toAddress" sender:self];
    
}
-(NSString *)chosenDateWithTime: (NSDate *)timeDate{
    
    NSDate * chosenDate = self.selectedDate;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:chosenDate]; // Get necessary date components
    
    [components month]; //gives you month
    NSLog(@"Month %ld",(long)[components month]);
    [components day]; //gives you day
    NSLog(@"day %ld",(long)[components day]);
    [components year]; // gives you year
    NSLog(@"Year %ld",(long)[components year]);
    
    NSDate * chosenTime = timeDate;
    NSDateComponents* components2 = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:chosenTime]; // Get necessary date components
    
    [components2 hour]; //gives you month
    NSLog(@"Hour %ld",(long)[components2 hour]);
    [components2 minute]; //gives you day
    NSLog(@"Min %ld",(long)[components2 minute]);
    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc]init];
    [tempFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString* message = [NSString stringWithFormat:@"%04ld-%02ld-%02ld %02ld:%02ld", (long)[components year], (long)[components month], (long)[components day], (long)[components2 hour], (long)[components2 minute]]
    ;
   
    return message;
    
}
-(void) timeSelected: (UIButton *)button{
    UIButton * selectedButton = button;
    NSLog(@"selected Button %ld",(long)button.tag);
    for (UIButton * button in self.buttonsArrays) {
        if (button.tag==selectedButton.tag) {
            button.layer.borderColor=kWhatSalonBlue.CGColor;
            [button setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
            self.selectedDate= self.dateArray[selectedButton.tag];
        }
        else{
            button.layer.borderColor=[UIColor whiteColor].CGColor;
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        
    }
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)timeSliderChange:(UISlider *)sender {
  ;
    NSUInteger slot = sender.value;
    if (sender.value>=12 && sender.value<=18) {
        sender.value=13.5;
        NSLog(@"Sender value %f",sender.value);
    }
    else if(sender.value>18 &&sender.value<=24){
        sender.value=20.5;
    }
    else if(sender.value>24 && sender.value<=30){
        sender.value=27;
    }
    else if(sender.value>30 &&sender.value<=36){
        sender.value=34;
    }
    else if(sender.value>36 && sender.value<=42){
        sender.value=41;
    }
    NSLog(@"Slot %lu",(unsigned long)slot);
    NSDate *slotDate = [self timeFromSlot:slot];
    self.bookingSlot=slotDate;
    NSLog(@"slotDate %@",slotDate);
    [self.formatter setDateFormat:@"h:mm a"];
    self.timeLabel.text = [self.formatter stringFromDate:slotDate];
    self.timeLabel.hidden=YES;
    
    NSLog(@"time %@",[self chosenDateWithTime:slotDate]);
}

- (NSDate *)timeFromSlot:(NSUInteger)slot{
    if ((slot > 47)) {
        return nil;
    }
    
    NSDateComponents *components = [NSDateComponents new];
    [components setMinute:30 * slot];
    
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}
- (IBAction)book:(id)sender {
    NSLog(@"Selected Date %@",self.selectedDate);
    
    NSLog(@"Chosen booking slot %@",[self chosenDateWithTime:self.bookingSlot]);
    
    [self performSegueWithIdentifier:@"therapistVC" sender:self];
}

#pragma mark - EnterViewDelegate
-(void)didUpdateAddress:(NSString *)address{
    self.enterAddressLabel.text = address;
}
@end
