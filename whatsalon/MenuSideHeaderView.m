//
//  MenuSideHeaderView.m
//  whatsalon
//
//  Created by admin on 10/1/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import "MenuSideHeaderView.h"

@implementation MenuSideHeaderView
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void) customInit {
    [[NSBundle mainBundle] loadNibNamed:@"MenuSideHeaderView" owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = self.bounds;
    self.profileImageView.clipsToBounds = true;
    self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.size.height /2;
}
@end
