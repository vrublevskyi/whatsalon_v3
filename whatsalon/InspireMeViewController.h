//
//  InspireMeViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface InspireMeViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UIView *overlayBlocView;
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (nonatomic) NSInteger listIndex;

- (IBAction)changeInspireListings:(id)sender;
- (IBAction)showMenu:(id)sender;

@end
