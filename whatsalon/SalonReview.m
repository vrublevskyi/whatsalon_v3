//
//  SalonReview.m
//  whatsalon
//
//  Created by Graham Connolly on 18/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SalonReview.h"

@implementation SalonReview

-(instancetype) initWithReviewer: (NSString *) firstName AndWithLastName : (NSString *) lastName{
    if (self = [super init]) {
        self.user_name =firstName;
        self.user_last_name=lastName;
        if (self.user_name==nil) {
            self.user_name=@"";
        }
        if (self.user_last_name==nil) {
            self.user_last_name=@"";
        }
    }
    return self;
}
+(instancetype) reviewWithReviewer: (NSString *) firstName AndWithLastName : (NSString *) lastName{
    return [[self alloc] initWithReviewer:firstName AndWithLastName:lastName];
}
@end
