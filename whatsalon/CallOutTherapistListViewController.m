//
//  TherapistListViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 07/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CallOutTherapistListViewController.h"
#import "BrowseTableViewCell.h"
#import "Therapist.h"
#import "TherapistTableViewCell.h"
#import "CallOutConfirmationScreenViewController.h"
#import "CalloutBioViewController.h"
#import "DismissDetailTransition.h"
#import "PresentDetailTransition.h"
#import "CalloutTherapistLegendViewController.h"

@interface CallOutTherapistListViewController ()<UITableViewDataSource,UITableViewDelegate,TherapistCellDelegate,UIViewControllerTransitioningDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSMutableArray * therapistArray;
@property (weak, nonatomic) IBOutlet UIView *titleHeader;
@property (weak, nonatomic) IBOutlet UILabel *serviceHeader;
@property (weak, nonatomic) IBOutlet UILabel *serviceSubHeader;

@end

@implementation CallOutTherapistListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore nonTransparentNavigationBarOnView:self];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    [WSCore nonTransparentNavigationBarOnView:self];
    
    [self setUpTherapistArray];
    
    [self.tableView registerClass:[TherapistTableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    self.titleHeader.backgroundColor=[UIColor clearColor];
    self.serviceHeader.textColor=kWhatSalonBlue;
    self.serviceSubHeader.textColor = kWhatSalonBlue;
    
}

-(void)setUpTherapistArray{
    self.therapistArray = [NSMutableArray array];
    Therapist * therapist1 = [[Therapist alloc] initWithTherapistID:@"1"];
    therapist1.name=@"Sarah";
    therapist1.pic_url=@"therapist3.jpg";
    Therapist * therapist2 = [[Therapist alloc] initWithTherapistID:@"2"];
    therapist2.name=@"Ciara";
    therapist2.price=@"€25";
    therapist2.pic_url=@"therapist4.jpg";
    Therapist * therapist3 = [[Therapist alloc] initWithTherapistID:@"3"];
    therapist3.name=@"Sandra";
    therapist3.pic_url=@"therapist3.jpg";
    Therapist * therapist4 = [[Therapist alloc] initWithTherapistID:@"4"];
    therapist4.name=@"Sam";
    therapist4.price=@"€35";
    therapist4.pic_url=@"therapist4.jpg";
    
    [self.therapistArray addObject:therapist1];
    [self.therapistArray addObject:therapist2];
    [self.therapistArray addObject:therapist3];
    [self.therapistArray addObject:therapist4];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewSources
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.therapistArray.count
    ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TherapistTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Therapist * therapist = [self.therapistArray objectAtIndex:indexPath.row];
    NSLog(@"name %@",therapist.name);
    cell.nameLabel.text = therapist.name;
    //[cell setCellImageWithURL:therapist.pic_url];
    cell.priceLabel.text = therapist.price;
    //cell.favImageView.image = [UIImage imageNamed:@"info_icon"];
    //cell.favImageView.layer.cornerRadius=kCornerRadius;
    
    cell.therapistBackgroundImage.image = [UIImage imageNamed:therapist.pic_url];
    cell.delegate=self;
           
    return cell;
    
}

-(void)showBio: (NSString *) name{
    NSLog(@"Show bio %@,",name);
    
    CalloutBioViewController *bioVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ccBioVC"];
    bioVC.modalPresentationStyle = UIModalPresentationCustom;
    bioVC.transitioningDelegate = self;
    [self presentViewController:bioVC animated:YES completion:nil];
}

-(void)showLegend{
    
    CalloutTherapistLegendViewController*legendVC = [self.storyboard instantiateViewControllerWithIdentifier:@"legendVC"];
    legendVC.modalPresentationStyle = UIModalPresentationCustom;
    legendVC.transitioningDelegate = self;
    [self presentViewController:legendVC animated:YES completion:nil];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CallOutConfirmationScreenViewController * cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"callOutConfirmVC"];
    [self.navigationController pushViewController:cvc animated:YES];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellHeight;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

#pragma mark - UIViewTransitionDelegate
-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return [[DismissDetailTransition alloc] init];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
