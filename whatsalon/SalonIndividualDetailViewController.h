//
//  DetailSalonTableViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 06/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"
#import "LastMinuteItem.h"


@protocol SalonIndividualDelegate <NSObject>

@optional
-(void)usersWantsToBook;

@end
@interface SalonIndividualDetailViewController : UITableViewController

@property (nonatomic) Salon* salonData;
@property (nonatomic) BOOL isFromFavourite;
@property (nonatomic) BOOL unwindBackToSearchFilter;



@property (nonatomic,assign) id<SalonIndividualDelegate> myDelegate;
@end
