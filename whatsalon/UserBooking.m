//
//  UserBooking.m
//  whatsalon
//
//  Created by Graham Connolly on 26/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "UserBooking.h"

@implementation UserBooking



static UserBooking *singletonInstance;

+(instancetype)getInstance{
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    
    return sharedInstance;
}

-(void)updateSalon_id: (NSString *) salon_id{
    
    self.salon_id=salon_id;
}
-(NSString *)fetchSalon_id{
    
    return self.salon_id;
}

-(void)updateSlot_id: (NSString *)slot_id{
    self.slot_id=slot_id;
}
-(NSString *)fetchSlot_id{
    return self.slot_id;
}

//room_id
-(void)updateRoom_id: (NSString *)r_id{
    self.room_id=r_id;
}

-(NSString *)fetchRoom_id{
    return self.room_id;
}
//internal_start_dateTime
-(void) updateInternal_start_dateTime: (NSString *) internal_start{
    self.internal_start_datetime=internal_start;
}
-(NSString *)fetchInternalStartDateTime{
    return self.internal_start_datetime;
}
//internal_end_dateTime
-(void)updateInternal_end_date_time: (NSString * ) internal_end{
    self.internal_end_datetime=internal_end;
}

-(NSString *)fetchInternal_end_date_time{
    return self.internal_end_datetime;
}
//staff_id
-(void)updateStaff_id: (NSString *)staff_id{
    self.staff_id = staff_id;
}

-(NSString *)fetchStaff_id{
    return self.staff_id;
}
//service_id
-(void)updateService_id: (NSString *)service_id{
    self.service_id = service_id;
}

-(NSString *) fetchService_id{
    return self.service_id;
}
//appointment_reference_number;
-(void)updateAppointment_ref: (NSString *)appt_ref{
    self.appointment_reference_number=appt_ref;
}
-(NSString *)fetchAppointment_ref: (NSString *)appt_ref{
    return self.appointment_reference_number;
}
//chosenService
//chosen price
//chosen stylist
//chosen day
//chosenTime
//end time

@end
