//
//  NoBookingMessageView.m
//  whatsalon
//
//  Created by Graham Connolly on 02/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "NoBookingMessageView.h"

@implementation NoBookingMessageView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        //1. load .xib
        [[NSBundle mainBundle] loadNibNamed:@"NoBookingsMessage" owner:self options:nil];
        
        //2 adjust bounds
        self.bounds = self.view.bounds;
        
        self.bookingImage.contentMode = UIViewContentModeScaleAspectFit;
        self.bookingText.adjustsFontSizeToFitWidth=YES;
        self.bookingText.textColor=kWhatSalonSubTextColor;
        self.bookingTitle.textColor=kWhatSalonSubTextColor;
        self.bookingTitle.adjustsFontSizeToFitWidth=YES;
        
        UIImage * templateImage = [[UIImage imageNamed:@"Recents" ] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.bookingImage.image = templateImage;
        self.bookingImage.tintColor=kWhatSalonBlue;
        self.bookingImage.hidden=YES;
        
        self.plusImageView.image = [self.plusImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.plusImageView.tintColor=kWhatSalonBlue;
        self.plusImageView.hidden=YES;
        //3 add as subview
        [self addSubview:self.view];
        
        self.getBookingBtn.backgroundColor=[UIColor clearColor];
        self.getBookingBtn.buttonColor = kWhatSalonBlue;
        self.getBookingBtn.shadowColor = kWhatSalonBlueShadow;
        self.getBookingBtn.shadowHeight = 1.0f;
        self.getBookingBtn.cornerRadius = 3.0f;
        [self.getBookingBtn setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //load the interface file from .xib
        [[NSBundle mainBundle] loadNibNamed:@"NoBookingsMessage" owner:self options:nil];
        
        //Add as a subview
        [self addSubview:self.view];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
