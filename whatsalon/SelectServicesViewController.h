//
//  SelectServicesViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 08/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header SelectServicesViewController.h
  
 @brief This is the header file for the SelectServicesViewController.
  
 This file handles the selecting service/
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.3
 */

#import <UIKit/UIKit.h>
#import "Salon.h"
#import "SalonService.h"
#import "FriendlyService.h"
#import "FUIButton.h"

@interface SelectServicesViewController : UIViewController

/*! @brief represents the NSMutableArray of services. */
@property (nonatomic) NSMutableArray * servicesArray;

/*! @brief represents the Salon object. */
@property (nonatomic) Salon *salonObj;

/*! @brief represents the holder for the content. */
@property (weak, nonatomic) IBOutlet UIView *contentHolder;

/*! @brief represents the table view. */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/*! @brief represents the cancel button. */
@property (weak, nonatomic) IBOutlet FUIButton *cancel;

/*! @brief represents the selected service. */
@property (nonatomic) SalonService * selectedService;


/*! @brief determine if the booking is from the last minute. 
    
    @remark LastMinute is no longer used. 
 */
@property (nonatomic) BOOL isFromLastMinute;


/*! @brief represents the FriendlyService. */
@property (nonatomic) FriendlyService * friendlySelectedService;


@end
