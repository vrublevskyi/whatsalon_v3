//
//  ConfirmViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 29/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"
#import "Booking.h"

@interface ConfirmViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;
@property (weak, nonatomic) IBOutlet UIView *viewForSalonInfo;
@property (weak, nonatomic) IBOutlet UIView *viewForSalonStylist;
@property (weak, nonatomic) IBOutlet UIView *viewForPrice;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelBarButtonItem;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *salonLabel;
@property (weak, nonatomic) IBOutlet UILabel *salonNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *stylistLabel;
@property (weak, nonatomic) IBOutlet UILabel *stylistNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateSelectedLabel;
@property (weak, nonatomic) IBOutlet UILabel *fullPriceAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceAtSalonLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *instructionsToTraceLabel;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;


@property (nonatomic) Booking * bookingObj;
@property (nonatomic) NSString * chosenService;
@property (nonatomic) NSString * chosenPrice;
@property (nonatomic) NSString * chosenStylist;
@property (nonatomic) NSString * chosenTime;
@property (nonatomic) NSString * chosenDay;
@property (nonatomic) NSDate * chosenDate;


@property (weak, nonatomic) IBOutlet UIImageView *traceTutorialImageView;
@property (weak, nonatomic) IBOutlet UIImageView *traceImageView;
@property (nonatomic) BOOL unwindBackToSearchFilter;

- (IBAction)confirm:(id)sender;
@property (weak,nonatomic) Salon * salonData;

@property (nonatomic) BOOL isFromFavourite;
@property (nonatomic) BOOL isFromLastMinute;
@property (nonatomic) BOOL isFromLastMinPopToSalonIndividual;

@end
