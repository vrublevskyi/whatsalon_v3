//
//  SalonAnnotation.m
//  whatsalon
//
//  Created by Graham Connolly on 15/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonAnnotation.h"

@implementation SalonAnnotation
@synthesize coordinate,title,subtitle;


-(instancetype)init
{
	return self;
}

-(instancetype)initWithCoordinate:(CLLocationCoordinate2D)inCoord
{
	coordinate = inCoord;
	return self;
}

-(void) setUpAnnotationWithSalon: (Salon *)salon{
    CLLocationCoordinate2D newCoord;
    newCoord.latitude = salon.salon_lat;
    newCoord.longitude = salon.salon_long;
    self.coordinate = newCoord;
    self.title = salon.salon_name;
    self.salon=salon;
}
@end
