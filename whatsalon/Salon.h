//
//  Salon.h
//  whatsalon
//
//  Created by Graham Connolly on 24/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

/*!
 @header Salon.h
  
 @brief This is the header file for the salon
  
  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd
 @version
 */

#import <Foundation/Foundation.h>
#import "SalonReview.h"

/*!
 @typedef SalonType
  
 @brief  An enum for the different salon types
  
 @discussion
 The values of this enum represent the number of different salon types.
 
 @field SalonTypePhorest represents the phorest salon type.
 
 @field SalonTypePremier represents the Premier salon type.
 
 @field SalonTypeIsalon  represents the iSalon salon type.
 */
typedef enum : NSUInteger {
    SalonTypePhorest,
    SalonTypePremier,
    SalonTypeIsalon,
} SalonType;

@interface Salon : NSObject <NSCoding>


/*! 
 @brief represents the salon id. 
 */
@property (nonatomic,strong) NSString * salon_id;

/*! 
 @brief represents the salon name. 
 */
@property (nonatomic,strong) NSString * salon_name;

/*! 
 @brief represents the salon description. 
 */
@property (nonatomic,strong) NSString * salon_description;

/*! @brief represents the salon phone. */
@property (nonatomic,strong) NSString * salon_phone;

/*! @brief represents the salons latitude. */
@property (nonatomic) double salon_lat;

/*! @brief represents the salons longitude. */
@property (nonatomic) double salon_long;

/*! @brief represents the salons first address line. */
@property (nonatomic) NSString * salon_address_1;

/*! @brief represents the salons second address line. */
@property (nonatomic) NSString * salon_address_2;

/*! @brief represents the salons third address line. */
@property (nonatomic) NSString * salon_address_3;

/*! @brief represents the city name. */
@property (nonatomic) NSString * city_name;

/*! @brief represents the county name. */
@property (nonatomic) NSString * county_name;

/*! @brief represents the country name. */
@property (nonatomic) NSString * country_name;

/*! @brief represents the salon landline. */
@property (nonatomic) NSString * salon_landline;

/*! @brief represents the salon website. */
@property (nonatomic) NSString * salon_website;

/*! @brief represents the salon image. */
@property (nonatomic) NSString * salon_image;

/*! @brief represents the salon type. */
@property (nonatomic) NSString * salon_type;

/*! @brief represents the rating of the salon. */
@property (nonatomic) double ratings;

/*! @brief represents the number of reviews for that salon. */
@property (nonatomic) double reviews;

/*! @brief whether the salon is a favourite or not. */
@property (nonatomic) BOOL is_favourite;

/*! @brief represents the salon tier. */
@property (nonatomic) double salon_tier;

/*! @brief the distance to the salon. */
@property (nonatomic) double distance;

/*! @brief BOOL value whether the salon is open or not. */
@property (nonatomic,assign) BOOL open;

/*! @brief BOOL whether this object is a rebooking salon. */
@property (nonatomic) BOOL isReBooking;

/*! @brief represents the slide show gallery of the salon. */
@property (nonatomic) NSMutableArray * imageGallery;//no longer being used

/*! @brief represents the slide show gallery of the salon. */
@property (nonatomic) NSMutableArray * slideShowGallery;

/*! @brief represents the booking deposit */
@property (nonatomic) NSString * bookingDeposit;

/*! @brief represents the total cost of the booking */
@property (nonatomic) NSString *bookingTotal;

/*! @brief represents the start time of the booking */
@property (nonatomic) NSDate *bookingStartTime;

/*! @brief represents the stylist */
@property (nonatomic) NSString *bookingStylist;

/*! @brief the ID of the booking */
@property (nonatomic) NSString * bookingID;

/*! @brief a dictionary represent the categories a salon offers.
 
    @discussion The keys represent the service name, e.g Hair, and the values represent the BOOL whether the service is done by the salon or not. */
@property (nonatomic) NSDictionary * salonCategories;

/*! @brief The opening hours for the salon. */
@property (nonatomic) NSMutableArray * openingHours;

/*! @brief represents a SalonReview object. */
@property (nonatomic) SalonReview * salonReview;

/*! @brief determines if a salon has a review. */
@property (nonatomic) BOOL hasReview;


//currency
/*! @brief represents the name of the currency. */
@property (nonatomic) NSString *currencyName;

/*! @brief represents the currency Id. */
@property (nonatomic) NSString *currencyId;

/*! @brief represents the currency symbol utf 8. */
@property (nonatomic) NSString * currencySymbolUtf8;

//source id
#warning double check the salon ids
/*! @brief represents the source id of the salon.
 
    @discussion 0 - Phorest, 1 - Premier, 2 - iSalon.
 */
@property (nonatomic) NSString * source_id;

/*! @brief represents the SalonType of the salon. */
@property (nonatomic) SalonType salonType;

/*! @brief represents the source string of the salon.*/
@property (nonatomic) NSString * source;


//designated Init
-(instancetype)initWithSalonID : (NSString *)s_id;
+(instancetype) salonWithID: (NSString *)s_id;

/*! @brief an array of placeholder images for the image gallery. 
 
    @return NSMutableArray */
-(NSMutableArray *)placeHolderImageGallery;

/*! @brief returns the full address of the salon.
 
    @return NSString */
-(NSString *)fetchFullAddress;

/*! @brief returns a string description of the salon. 
 
    @return NSString */
-(NSString *)fetchSalonDescription;

/*! @brief returns the salon slide show gallery.
 
    @return NSMutableArray
 */
-(NSMutableArray *)fetchSlideShowGallery;

/*! @brief returns a dictionary representation of the salons properties.
 
    @return NSDictionary */
- (NSDictionary *)dictionaryRepresentation;

/*! @brief creates a salon object passed on a dictionary. Used to parse JSON.
 
    @param dictionary JSON dictionary.
 
    @return instancetype
 */
-(instancetype)initWithDicionary:(NSDictionary *)dictionary;

/*! @brief returns whether the salon is open or not.
 
    @param day int the numberic day.
 @return BOOL
 */
-(BOOL)isSalonOpen: (int)day;


//currency method
/*!
 @brief updates the salons currency values.
 
 @param currencyName - The name of the currency.
 
 @param currencyId - The id for the currency.
 
 @param symbol - the currency utf8.
 */
-(void)updatedCurrencyName: (NSString *) currencyName WithCurrencyId: (NSString *) currencyId AndWithSymbolUtf8: (NSString *) symbol;


/*!
 @brief updates the salon source.
 
 @return NSString returns the salon source as a string.
 */
-(NSString *)fetchSalonSource;

/*!
 @brief grabs the SalonType.
 
 @return SalonType
 */
-(SalonType)fetchSalonType;

/*!
 @brief updates the salon review.
 
 @param json NSDictionary representation of JSON.
 */
-(void)updateSalonReviewWithJsonDictionary: (NSDictionary *) json;

/*!
 @brief updates the currency object using JSON.
 
 @param json NSDictionary representation of JSON.
 */
-(void) updateCurrenecyWithJsonDictionary: (NSDictionary *) json;

/*!
 @brief updates the salon details.
 
 @param json NSDictionary representation of JSON.
 */
-(void)updateSalonDetailsWithJsonDictionary: (NSDictionary *) json;
@end
