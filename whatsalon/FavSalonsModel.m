//
//  FavSalonsModel.m
//  whatsalon
//
//  Created by Graham Connolly on 27/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "FavSalonsModel.h"
#import "Salon.h"

@implementation FavSalonsModel
- (id) init
{
	self = [super init];
	if (self != nil) {
        self.arryResults = [[NSMutableArray alloc] init];
	}
	
	return self;
}

-(void)load:(NSURL *)url withParams:(NSString *)params{
    
    [self.arryResults removeAllObjects];
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        if (data) {
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    
                    if ([dataDict[@"success"] isEqualToString:@"true"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            self.totalNumberOfPages=[dataDict[@"message"][@"total_pages"] intValue];
                            NSArray * salonsArray = dataDict[@"message"][@"results"];
                            for (NSDictionary *sDict in salonsArray) {
                                Salon *salon = [Salon salonWithID:[sDict[@"salon_id"] stringValue] ];
                                if (sDict[@"salon_name"]!=[NSNull null]) {
                                   salon.salon_name=sDict[@"salon_name"];
                                }
                                if (sDict[@"salon_description"] !=[NSNull null]) {
                                    salon.salon_description = sDict[@"salon_description"];
                                }
                                if (sDict[@"salon_phone"]!= [NSNull null]   ) {
                                   salon.salon_phone = sDict[@"salon_phone"];
                                }
                                if (sDict[@"salon_lat"] != [NSNull null]) {
                                     salon.salon_lat = [sDict[@"salon_lat"] doubleValue];
                                }
                                if (sDict[@"salon_lon"] != [NSNull null]) {
                                     salon.salon_long = [sDict[@"salon_lon"] doubleValue];
                                }
                                if (sDict[@"salon_address_1"] != [NSNull null]) {
                                    salon.salon_address_1 = sDict[@"salon_address_1"];
                                }
                                if (sDict[@"salon_address_2"] != [NSNull null]) {
                                    salon.salon_address_2 = sDict[@"salon_address_2"];
                                }
                                if (sDict[@"salon_address_3"] != [NSNull null]) {
                                    salon.salon_address_3 = sDict[@"salon_address_3"];
                                }
                                if (sDict[@"city_name"] != [NSNull null]) {
                                    salon.city_name = sDict[@"city_name"];
                                }
                                if (sDict[@"county_name"] != [NSNull null]) {
                                   salon.country_name =sDict[@"county_name"];
                                }
                                if (sDict[@"country_name"] != [NSNull null]) {
                                    salon.country_name = sDict[@"country_name"];
                                }
                                if (sDict[@"salon_landline"] != [NSNull null]) {
                                    salon.salon_landline = sDict[@"salon_landline"];
                                }
                                if (sDict[@"salon_website"] != [NSNull null]) {
                                    salon.salon_website = sDict[@"salon_website"];
                                }
                                if (sDict[@"salon_image"] != [NSNull null]) {
                                    salon.salon_image = sDict[@"salon_image"];
                                }
                                if (sDict[@"salon_type"] != [NSNull null]) {
                                    salon.salon_type = sDict[@"salon_type"];
                                }
                                if (sDict[@"rating"] != [NSNull null]) {
                                    salon.ratings = [sDict[@"rating"] doubleValue];
                                }
                                if (sDict[@"reviews"] != [NSNull null]) {
                                    salon.reviews = [sDict[@"reviews"] doubleValue];
                                }
                                if (sDict[@"salon_tier"] != [NSNull null]) {
                                   salon.salon_tier = [sDict[@"salon_tier"] doubleValue];
                                }
                                if (sDict[@"is_favourite"] != [NSNull null]) {
                                    salon.is_favourite = [sDict[@"is_favourite"] boolValue];
                                }
                                if (sDict[@"distance"] != [NSNull null]) {
                                   salon.distance = [sDict[@"distance"] doubleValue];
                                }
                               
                            
                                [self.arryResults addObject:salon];
                                
                            }
                           
                            if ([self.myDelegate respondsToSelector:@selector(reloadFavSalonsData)]) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.myDelegate reloadFavSalonsData];
                                });
                                
                            }
                           
                            [self finishedDownloading];
                            
                        });
                        
                        
                        
                    }
                    break;
                    
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[WSCore dismissNetworkLoading];
                        [self finishedDownloading];
                    });
                    break;
            }
            
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (error) {
                    //[WSCore dismissNetworkLoading];
                    [self finishedDownloading];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@", [error localizedDescription] ]message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                    return ;
                }

                //[WSCore dismissNetworkLoading];
                [self finishedDownloading];
                [WSCore showServerErrorAlert];
            });
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        //[WSCore showNetworkLoading];
        [self startDownloading];
        [task resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }

    
}

-(void)removeFavSalonObjectsFromArray{
    
    [self.arryResults removeAllObjects];
}
-(NSMutableArray*)fetchFavSalons{
    NSLog(@"Fav array %lu",(unsigned long)self.arryResults.count);
    return self.arryResults;
}
-(int)fetchTotalNumberOfPages{
    
    return self.totalNumberOfPages;
}

-(void)finishedDownloading{
    if ([self.myDelegate respondsToSelector:@selector(downloadingFavSalonsFinished)]) {
        [self.myDelegate downloadingFavSalonsFinished];
    }
}

-(void)startDownloading{
    if ([self.myDelegate respondsToSelector:@selector(downloadingFavSalonsStarted)]) {
        [self.myDelegate downloadingFavSalonsStarted];
    }
}
@end
