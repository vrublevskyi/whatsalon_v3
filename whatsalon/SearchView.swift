//
//  SearchView.swift
//  whatsalon
//
//  Created by admin on 10/5/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit

protocol SearchViewDelegate: class {
    func searchByTextTapped(_ text: String, on controller: SearchView)
    func currentLocationTapped(on controller: SearchView)
    func locationTapped(on controller: SearchView)
}

class SearchView: NibDesignable {

    //MARK: - Outlets
    @IBOutlet private var searchSalonTextField: UITextField! {
        didSet {
            searchSalonTextField.addTarget(self, action: #selector(textFieldDidChange(_:)),
                                for: .editingChanged)
        }
    }
    @IBOutlet private var searchLocationButton: UIButton!
    @IBOutlet private var searchLocationShowConstraint: NSLayoutConstraint!
    @IBOutlet private var locationView: UIView!
    
    //MARK: - Properties
    //TODO: change to active constraints

    var searchIsActive = false {
        didSet {
            locationView.isHidden = searchIsActive ? false : true
            searchLocationShowConstraint.constant = searchIsActive ? 30 : 0
        }
    }

    
    var searchedlocations: String? {
        didSet {
            searchLocationButton.setTitle(searchedlocations, for: .normal)
        }
    }
    
    weak var delegate: SearchViewDelegate?
    
    //MARK: - Actions

    @IBAction func currentLocationTapped(_ sender: Any) {
        delegate?.currentLocationTapped(on:self)
    }
    @IBAction func locationTapped() {
        delegate?.locationTapped(on: self)
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        delegate?.searchByTextTapped(textField.text ?? "", on: self)
    }
}

