//
//  CurrencyItem.m
//  whatsalon
//
//  Created by Graham Connolly on 19/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CurrencyItem.h"

@implementation CurrencyItem

- (id)init {
    self = [super init];
    if (self) {
        [self initHelper];
    }
    return self;
}

-(void)initHelper{
    self.conversionRate = @"1";
    self.currencyName = @"Euro";
    self.currencyId = @"1";
    self.symbolUtf8 = @"\u20ac";
}

-(void)updatedCurrencySymbol:(NSString *)currency{
    self.symbolUtf8 =currency;
    [[NSUserDefaults standardUserDefaults] setObject:self.symbolUtf8 forKey:@"kCurrencySymbol"];
}
@end
