//
//  Date+transform.swift
//  WhatSalon
//
//  Created by admin on 8/27/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

extension Date {
    func localToUTC() -> Date? {
        let dateFormatter = DateFormatters.generalTimeFormatter.extract()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.string(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.calendar = NSCalendar.current

        return dateFormatter.date(from: dt)
    }

    func localFromUTC() -> Date? {
        let dateFormatter = DateFormatters.generalTimeFormatter.extract()
        dateFormatter.timeZone = TimeZone.current

        let dt = dateFormatter.string(from: self)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.calendar = NSCalendar.current

        return dateFormatter.date(from: dt)
    }
}
