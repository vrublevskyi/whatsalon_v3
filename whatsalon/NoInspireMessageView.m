//
//  NoInspireMessageView.m
//  whatsalon
//
//  Created by Graham Connolly on 22/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "NoInspireMessageView.h"

@implementation NoInspireMessageView
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        //1. load .xib
        [[NSBundle mainBundle] loadNibNamed:@"NoInspireMessageView" owner:self options:nil];
        
        //2 adjust bounds
        self.bounds = self.view.bounds;
        
        
        self.inspireText.adjustsFontSizeToFitWidth=YES;
        self.inspireText.textColor=kCellLinesColour;
        self.inspireTitle.textColor=[UIColor grayColor];
        self.inspireTitle.adjustsFontSizeToFitWidth=YES;
        
        UIImage * templateImage = [[UIImage imageNamed:@"Search"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.inspireImage.image = templateImage;
        self.inspireImage.tintColor=kWhatSalonBlue;
        //3 add as subview
        [self addSubview:self.view];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //load the interface file from .xib
        [[NSBundle mainBundle] loadNibNamed:@"NoInspireMessageView" owner:self options:nil];
        
        //Add as a subview
        [self addSubview:self.view];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */



@end
