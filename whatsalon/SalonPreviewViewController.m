//
//  SalonPreviewViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 14/10/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//

#import "SalonPreviewViewController.h"
#import "OpeningDay.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ColoredImage.h"
#import "SalonServiceCategory.h"
#import "TabMakeBookingViewController.h"


@interface SalonPreviewViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *salonProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *salonName;
@property (weak, nonatomic) IBOutlet UILabel *salonAddressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *salonImageViewGallery1;
@property (weak, nonatomic) IBOutlet UIImageView *salonImageViewGallery2;
@property (weak, nonatomic) IBOutlet UIImageView *salonImageViewGallery3;
@property (weak, nonatomic) IBOutlet UIImageView *salonImageViewGallery4;
@property (weak, nonatomic) IBOutlet UIImageView *salonImageViewGallery5;
@property (weak, nonatomic) IBOutlet UIImageView *salonImageViewGallery6;
@property (weak, nonatomic) IBOutlet UILabel *mondayTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tuesdayTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *wednesdayTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *thursdayTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *fridayTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *saturdayTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sundayTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starsImageView;

//services
@property (weak, nonatomic) IBOutlet UIImageView *serviceImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImageView3;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImageView4;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImageView5;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImageView6;

@property (nonatomic) NSMutableArray * imagesArray;

//preview frame
@property (weak, nonatomic) IBOutlet UIView *salonContentHolder;
@property (weak, nonatomic) IBOutlet UIView *previewFrameView;

//service images
@property (weak, nonatomic) IBOutlet UIImageView *serviceImage1;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImage2;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImage3;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImage4;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImage5;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImage6;

@end

@implementation SalonPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSMutableArray * servicesImageArray = [[NSMutableArray alloc] initWithObjects:self.serviceImage1,self.serviceImage2,self.serviceImage3,self.serviceImage4,self.serviceImage5,self.serviceImage6,nil];
    
    self.salonImageViewGallery1.image = [UIImage coloredImage_resizeableImageWithColor:kBackgroundColor];
    self.salonImageViewGallery2.image = [UIImage coloredImage_resizeableImageWithColor:kBackgroundColor];
    self.salonImageViewGallery3.image = [UIImage coloredImage_resizeableImageWithColor:kBackgroundColor];
    self.salonImageViewGallery4.image = [UIImage coloredImage_resizeableImageWithColor:kBackgroundColor];
    self.salonImageViewGallery5.image = [UIImage coloredImage_resizeableImageWithColor:kBackgroundColor];
    self.salonImageViewGallery6.image = [UIImage coloredImage_resizeableImageWithColor:kBackgroundColor];
    
    
    self.salonName.text=self.salonData.salon_name;
    self.salonAddressLabel.text = [self.salonData fetchFullAddress];
    self.salonAddressLabel.numberOfLines=0;
    self.starsImageView.image = [WSCore getStarRatingImage:(int)self.salonData.ratings];
    if ((int)self.salonData.reviews==0) {
        self.starsImageView.image = [self.starsImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.starsImageView setTintColor:[UIColor lightGrayColor]];
        NSLog(@"No stars");
    }else{
        self.starsImageView.image = [self.starsImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.starsImageView setTintColor:kWhatSalonBlue];
    }
    
    for (OpeningDay*day in self.salonData.openingHours) {
        
        switch ([day.dayOfWeek integerValue]) {
            case 1:
                if (day.isOpened) {
                    self.sundayTimeLabel.text = [NSString stringWithFormat:@"%@ - %@  ",[WSCore getShortTimeFromDate:day.startTime WithFormat:@"HH:mm"],[WSCore getShortTimeFromDate:day.endTime WithFormat:@"HH:mm"]];
                }
                else{
                    self.sundayTimeLabel.text = @"Closed";
                }
                break;
            case 2:
                if (day.isOpened) {
                    self.mondayTimeLabel.text = [NSString stringWithFormat:@"%@ - %@  ",[WSCore getShortTimeFromDate:day.startTime WithFormat:@"HH:mm"],[WSCore getShortTimeFromDate:day.endTime WithFormat:@"HH:mm"]];
                }
                else{
                    self.mondayTimeLabel.text = @"Closed";
                }

                break;
            case 3:
                if (day.isOpened) {
                    self.tuesdayTimeLabel.text = [NSString stringWithFormat:@"%@ - %@  ",[WSCore getShortTimeFromDate:day.startTime WithFormat:@"HH:mm"],[WSCore getShortTimeFromDate:day.endTime WithFormat:@"HH:mm"]];
                }
                else{
                    self.tuesdayTimeLabel.text = @"Closed";
                }
                
                break;
            case 4:
                if (day.isOpened) {
                    self.wednesdayTimeLabel.text = [NSString stringWithFormat:@"%@ - %@  ",[WSCore getShortTimeFromDate:day.startTime WithFormat:@"HH:mm"],[WSCore getShortTimeFromDate:day.endTime WithFormat:@"HH:mm"]];
                }
                else{
                    self.wednesdayTimeLabel.text = @"Closed";
                }
                break;
            case 5:
                if (day.isOpened) {
                    self.thursdayTimeLabel.text = [NSString stringWithFormat:@"%@ - %@  ",[WSCore getShortTimeFromDate:day.startTime WithFormat:@"HH:mm"],[WSCore getShortTimeFromDate:day.endTime WithFormat:@"HH:mm"]];
                }
                else{
                    self.thursdayTimeLabel.text = @"Closed";
                }

                break;
            case 6:
                if (day.isOpened) {
                    self.fridayTimeLabel.text = [NSString stringWithFormat:@"%@ - %@  ",[WSCore getShortTimeFromDate:day.startTime WithFormat:@"HH:mm"],[WSCore getShortTimeFromDate:day.endTime WithFormat:@"HH:mm"]];
                }
                else{
                    self.fridayTimeLabel.text = @"Closed";
                }
                break;
            case 7:
                if (day.isOpened) {
                    self.saturdayTimeLabel.text = [NSString stringWithFormat:@"%@ - %@  ",[WSCore getShortTimeFromDate:day.startTime WithFormat:@"HH:mm"],[WSCore getShortTimeFromDate:day.endTime WithFormat:@"HH:mm"]];
                }
                else{
                    self.saturdayTimeLabel.text = @"Closed";
                }
                break;
            default:
                break;
        }
    }
    
    if ([self.salonData fetchSlideShowGallery].count>0) {
        self.imagesArray= [[NSMutableArray alloc] initWithArray:[self.salonData fetchSlideShowGallery]];
        [self.imagesArray removeLastObject];
        if ([self.imagesArray objectAtIndex:0]) {
            [self.salonImageViewGallery1 sd_setImageWithURL:[self.imagesArray
                                                             objectAtIndex:0] placeholderImage:[UIImage imageNamed:@"black"]];

            self.salonImageViewGallery1.layer.cornerRadius=kCornerRadius;
            self.salonImageViewGallery1.layer.masksToBounds=YES;
        
        }
        
        
        if (self.imagesArray.count>=2) {
            
            [self.salonImageViewGallery2 sd_setImageWithURL:[self.imagesArray
                                                             objectAtIndex:1] placeholderImage:[UIImage imageNamed:@"black"]];
            
            self.salonImageViewGallery2.layer.cornerRadius=kCornerRadius;
            self.salonImageViewGallery2.layer.masksToBounds=YES;
            
        }
        
        if (self.imagesArray.count>=3) {
            
            [self.salonImageViewGallery3 sd_setImageWithURL:[self.imagesArray
                                                             objectAtIndex:2] placeholderImage:[UIImage imageNamed:@"black"]];
            
            self.salonImageViewGallery3.layer.cornerRadius=kCornerRadius;
            self.salonImageViewGallery3.layer.masksToBounds=YES;
        }
        if (self.imagesArray.count>=4) {
            
            [self.salonImageViewGallery4 sd_setImageWithURL:[self.imagesArray
                                                             objectAtIndex:3] placeholderImage:[UIImage imageNamed:@"black"]];
            
            self.salonImageViewGallery4.layer.cornerRadius=kCornerRadius;
            self.salonImageViewGallery4.layer.masksToBounds=YES;
        }
        if (self.imagesArray.count>=5) {
            
            [self.salonImageViewGallery5 sd_setImageWithURL:[self.imagesArray
                                                             objectAtIndex:4] placeholderImage:[UIImage imageNamed:@"black"]];
            
            self.salonImageViewGallery5.layer.cornerRadius=kCornerRadius;
            self.salonImageViewGallery5.layer.masksToBounds=YES;
        }
        if (self.imagesArray.count>=6) {
            
            [self.salonImageViewGallery6 sd_setImageWithURL:[self.imagesArray
                                                             objectAtIndex:5] placeholderImage:[UIImage imageNamed:@"black"]];
            
            self.salonImageViewGallery6.layer.cornerRadius=kCornerRadius;
            self.salonImageViewGallery6.layer.masksToBounds=YES;
        }

        
    }
    
    
    if (self.salonData.salon_image.length <1) {
        self.salonProfileImage.image = kPlace_Holder_Image;
        self.salonProfileImage.contentMode=UIViewContentModeScaleAspectFill;
        
        
    }
    else{
        [self.salonProfileImage sd_setImageWithURL:[NSURL URLWithString:self.salonData.salon_image] placeholderImage:kPlace_Holder_Image];
    }
    
    self.view.backgroundColor=[UIColor whiteColor];
    self.salonProfileImage.contentMode=UIViewContentModeScaleAspectFill;
    self.salonImageViewGallery1.contentMode = UIViewContentModeScaleAspectFill;
    self.salonImageViewGallery2.contentMode = UIViewContentModeScaleAspectFill;
    self.salonImageViewGallery3.contentMode = UIViewContentModeScaleAspectFill;
    self.salonImageViewGallery4.contentMode = UIViewContentModeScaleAspectFill;
    self.salonImageViewGallery5.contentMode = UIViewContentModeScaleAspectFill;
    self.salonImageViewGallery6.contentMode = UIViewContentModeScaleAspectFill;
    
    self.salonProfileImage.layer.cornerRadius=self.salonProfileImage.frame.size.width/2.0f;
    self.previewFrameView.backgroundColor=[UIColor clearColor];
    self.salonContentHolder.backgroundColor=[UIColor clearColor];
    

    CGSize size = CGSizeMake(self.view.frame.size.width, 488);
    self.preferredContentSize =size;
    
    //categories
    //start from right and work in
    //tint
    BOOL hasCategories;
    for(NSString* key in self.salonData.salonCategories) {
        BOOL bValue = [[self.salonData.salonCategories objectForKey:key] boolValue];
        if (bValue==YES) {
            hasCategories=YES;
            break;
        }
    }
    
    NSMutableArray * servicesArray = [NSMutableArray array];
    if (hasCategories) {
        
       // NSLog(@"into category for images");
        for(NSString *key in [self.salonData.salonCategories allKeys]) {
           
            //NSLog(@"all keys %@",[self.salonData.salonCategories allKeys]);
            if ([key isEqualToString:@"Hair"] && [[self.salonData.salonCategories valueForKey:@"Hair"] boolValue]) {
                
                SalonServiceCategory * serviceCat = [SalonServiceCategory serviceWithServiceType:ServiceTypeHair];
                [servicesArray addObject:serviceCat];
                
                
            } else if ([key isEqualToString:@"Nails"] && [[self.salonData.salonCategories valueForKey:@"Nails"] boolValue]) {
                SalonServiceCategory * serviceCat = [SalonServiceCategory serviceWithServiceType:ServiceTypeNails];
                [servicesArray addObject:serviceCat];
                
            }
            else if ([key isEqualToString:@"Massage"] && [[self.salonData.salonCategories valueForKey:@"Massage"] boolValue]) {
                SalonServiceCategory * serviceCat = [SalonServiceCategory serviceWithServiceType:ServiceTypeMassage];
                [servicesArray addObject:serviceCat];
                
            }
            else  if ([key isEqualToString:@"Hair Removal"] && [[self.salonData.salonCategories valueForKey:@"Hair Removal"] boolValue]) {
                SalonServiceCategory * serviceCat = [SalonServiceCategory serviceWithServiceType:ServiceTypeHairRemoval];
                [servicesArray addObject:serviceCat];
                
            }
            else  if ([key isEqualToString:@"Face"] && [[self.salonData.salonCategories valueForKey:@"Face"] boolValue]) {
                SalonServiceCategory * serviceCat = [SalonServiceCategory serviceWithServiceType:ServiceTypeFace];
                [servicesArray addObject:serviceCat];
                
            }
            else if ([key isEqualToString:@"Body"] && [[self.salonData.salonCategories valueForKey:@"Body"] boolValue]) {
                SalonServiceCategory * serviceCat = [SalonServiceCategory serviceWithServiceType:ServiceTypeBody];
                [servicesArray addObject:serviceCat];

            }
            
        }

    }
    
   // NSLog(@"Salon array size %lu",(unsigned long)servicesArray.count);

    for (int i=0; i<servicesArray.count; i++) {
     //   NSLog(@"Service %@",[servicesArray objectAtIndex:i]);
        SalonServiceCategory * obj = [servicesArray objectAtIndex:i];
        UIImageView * imageView = [servicesImageArray objectAtIndex:i];
        imageView.image = obj.serviceImage;
      
        imageView.image = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [imageView setTintColor:kWhatSalonBlue];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
- (NSArray*)previewActionItems {
    
    // setup a list of preview actions
    UIPreviewAction *action1 = [UIPreviewAction actionWithTitle:@"Action 1" style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        NSLog(@"Action 1 triggered");
    }];
    
    UIPreviewAction *action2 = [UIPreviewAction actionWithTitle:@"Destructive Action" style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        NSLog(@"Destructive Action triggered");
    }];
    
    UIPreviewAction *action3 = [UIPreviewAction actionWithTitle:@"Selected Action" style:UIPreviewActionStyleSelected handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        NSLog(@"Selected Action triggered");
    }];
    
    // add them to an arrary
    NSArray *actions = @[action1, action2, action3];
    
    UIPreviewActionGroup *group1 = [UIPreviewActionGroup actionGroupWithTitle:@"Action Group" style:UIPreviewActionStyleDefault actions:actions];
    NSArray *group = @[group1];
    
    // and return them
    return group;
}*/


- (NSArray *)previewActionItems {
    
    NSMutableArray* actions = [NSMutableArray array];
    // setup a list of preview actions
    //make booking if tier 1 else call
    NSString * callToAction;
    if (self.salonData.salon_tier==3.0) {
        callToAction=@"Call to Book";
    }
    else{
        callToAction = @"Make Booking";
    }
    /*
    UIPreviewAction *action1 = [UIPreviewAction actionWithTitle:callToAction style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        NSLog(@"Action 1 triggered");
        
        
        if ([self.delegate respondsToSelector:@selector(salonPreviewViewController:didSelectMakeBookingOrCallActionItem:WithSalon:)]) {
            [self.delegate salonPreviewViewController:self didSelectMakeBookingOrCallActionItem:action WithSalon:self.salonData];
            
        }

    }];
    [actions addObject:action1];
    */
    if ((int)self.salonData.ratings>0) {
        UIPreviewAction *action2 = [UIPreviewAction actionWithTitle:@"Read Reviews" style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
            NSLog(@"Destructive Action triggered");
            
            if ([self.delegate respondsToSelector:@selector(salonPreviewViewController:didSelectReadReviewsActionItem:WithSalon:)]) {
                [self.delegate salonPreviewViewController:self didSelectReadReviewsActionItem:action WithSalon:self.salonData];
            }
        }];
        [actions addObject:action2];
    }
    
    if (self.imagesArray.count>=2) {
        UIPreviewAction *action3 = [UIPreviewAction actionWithTitle:@"See Gallery" style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
            NSLog(@"Selected Action triggered");
            
            
            if ([self.delegate respondsToSelector:@selector(salonPreviewViewController:didSelectSeeGalleryActionItem:WithSalon:)]) {
                [self.delegate salonPreviewViewController:self didSelectSeeGalleryActionItem:action WithSalon:self.salonData];
            }
        }];
        [actions addObject:action3];
    }
    
    return actions;
}


@end
