//
//  StyleCategoryListModel.h
//  whatsalon
//
//  Created by Graham Connolly on 04/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StyleCategoryListModel : NSObject{
    
    NSMutableArray * completeArray;
    NSMutableArray * maleArray;
    NSMutableArray * femaleArray;
}

+(instancetype) getInstance;

-(void)setStyles;

-(NSMutableArray *)fetchFemaleStylesList;

-(NSMutableArray *)fetchMaleStylesList;

-(int)fetchMaleStartPage;
-(int)fetchFemalStartPage;
@end
