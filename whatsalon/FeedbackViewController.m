//
//  FeedbackViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 02/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "FeedbackViewController.h"
#import "RESideMenu.h"
#import "GCNetworkManager.h"
#import "User.h"
#import "UIView+AlertCompatibility.h"
#import "UIColor+FlatUI.h"
#import "UIFont+FlatUI.h"
#import "FUIButton.h"
#import "whatsalon-Swift.h"

@interface FeedbackViewController ()<GCNetworkManagerDelegate>

/*! @brief represents the feedback text view. */
@property (weak, nonatomic) IBOutlet UITextView *feedBackText;

/*! @brief represents the submit button. */
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

/*! @brief represents the network manager. */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief reprsents the submit action. */
- (IBAction)submit:(id)sender;
@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [WSCore statusBarColor:StatusBarColorBlack];
    self.feedBackText.layer.cornerRadius=3.0;
    self.feedBackText.layer.masksToBounds=YES;
    self.feedBackText.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.feedBackText.layer.borderWidth=0.5;
    
    self.submitButton.layer.cornerRadius=3.0;
    self.submitButton.layer.masksToBounds=YES;
    
    self.feedBackText.text=@"";
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.networkManager=[[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    /*
    if (self.navigationController.navigationBar.tintColor ==[UIColor whiteColor]) {
        [WSCore nonTransparentNavigationBarOnView:self];
        self.navigationController.navigationBar.tintColor=self.view.tintColor;
    }
     */
    [WSCore flatNavBarOnView:self];
    [Gradients addPinkToPurpleHorizontalLayerWithView:_submitButton];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore flatNavBarOnView:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}


#pragma mark - GCNetworkManager
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
        dispatch_async(dispatch_get_main_queue(), ^{
         [WSCore showServerErrorAlert];
    });
   
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.feedBackText.text=@"";
        [UIView showSimpleAlertWithTitle:@"Thank You!" message:@"Your feedback helps us improve WhatSalon." cancelButtonTitle:@"OK"];
    });
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    
    [self.view endEditing:YES];
}

- (IBAction)submit:(id)sender {
    
    if (self.feedBackText.text.length<2) {
        
        [UIView showSimpleAlertWithTitle:@"Oh Oh" message:@"Empty text cannot be submitted" cancelButtonTitle:@"OK"];
        NSLog(@"Show");
    }
    else{
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kFeed_Back_URL]];
        NSString * accessToken = [[User getInstance] fetchAccessToken];
        
       
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_content=%@",self.feedBackText.text]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&device_used=ios"]];
        NSString * deviceNumber = [[UIDevice currentDevice] systemVersion];
        deviceNumber = [deviceNumber stringByReplacingOccurrencesOfString:@"." withString:@"_"];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&device_version_number=%@",deviceNumber]];
        
        NSString * deviceName = [WSCore fetchDeviceName];
        deviceName = [deviceName stringByReplacingOccurrencesOfString:@"," withString:@"_"];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&device_model_number=%@",deviceName]];
        
    
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
        
    }
    
    
    
}

/*!
 only used in the RESideMenu application (legacy)
 */
- (IBAction)showMenu:(id)sender {
    
    [self.sideMenuViewController presentMenuViewController];
    
}
@end
