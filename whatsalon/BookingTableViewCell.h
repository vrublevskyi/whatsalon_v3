//
//  BookingTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 27/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyBookings.h"

@interface BookingTableViewCell : UITableViewCell
@property (nonatomic) IBOutlet UIImageView * salonImageView;

@property (nonatomic) IBOutlet UILabel * serviceLabel;
@property (nonatomic) IBOutlet UILabel * salonName;
@property (nonatomic) IBOutlet UIImageView * starsImageView;
@property (nonatomic) IBOutlet UILabel * dateLabel;
@property (nonatomic) IBOutlet UILabel * bookingRefLabel;
@property (nonatomic) BOOL isPrevious;

-(void)setCellImageWithURL: (NSString*) url;

-(void)updateCellWithBooking: (MyBookings *) booking;

@property (nonatomic) Salon *salon;
@property (nonatomic) IBOutlet UIView*cancelView;
@property (nonatomic) IBOutlet UILabel *cancelTitle;

@end
