//
//  WalletTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 25/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "WalletTableViewCell.h"


@implementation WalletTableViewCell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.cellTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 180, 22)];
        self.cellTitleLabel.text=@"Your Code";
        self.cellTitleLabel.textAlignment=NSTextAlignmentLeft;
        [self.contentView addSubview:self.cellTitleLabel];
        
        self.cellSubTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.contentView.frame.size.width-100, 10, 90, 22)];
        self.cellSubTitleLabel.textAlignment=NSTextAlignmentRight;
        self.cellSubTitleLabel.text=@"Code";
        [self.contentView addSubview:self.cellSubTitleLabel];
        
        self.button = [[FUIButton alloc] initWithFrame:CGRectMake(10, self.frame.size.height-kButtonHeight-10, self.contentView.bounds.size.width - 20, kButtonHeight)];
        [self.button setTitle:@"Log Out" forState:UIControlStateNormal];
        self.button.backgroundColor = kWhatSalonBlue;
        self.button.layer.cornerRadius=3.0;
        self.button.buttonColor = kWhatSalonBlue;
        self.button.shadowColor = kWhatSalonBlueShadow;
        self.button.shadowHeight = 1.0f;
        self.button.cornerRadius = kCornerRadius;
        [self.button addTarget:self action:@selector(buttonPress) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.button];
        
        self.contentView.layer.masksToBounds=YES;
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)buttonPress{
    if ([self.delegate respondsToSelector:@selector(didTriggerActionWithType:)]) {
        [self.delegate didTriggerActionWithType:self.isShareCell];
    }
}
-(void)setUpCellIsCodeCell: (BOOL) yn{
    self.isShareCell=yn;
    CGRect buttonframe = self.button.frame;
    buttonframe.origin.y = self.frame.size.height-15-self.button.frame.size.height;
    self.button.frame=buttonframe;
    NSLog(@"IN");
    if (yn) {
        NSLog(@"1.1");
        [self.button setTitle:@"Share Your Code" forState:UIControlStateNormal];
         NSLog(@"1.2");
        self.cellTitleLabel.text=@"Your Code";
         NSLog(@"1.3");
        
    }else{
         NSLog(@"2.1");
        
        [WSCore addTopLine:self.contentView :kCellLinesColour :0.5];
        [self.button setTitle:@"Redeem A Code" forState:UIControlStateNormal];
         NSLog(@"2.2");
        self.cellTitleLabel.text=@"Current Balance";
         NSLog(@"2.3");
        self.cellSubTitleLabel.text = [NSString stringWithFormat:@"%@%.2f",CURRENCY_SYMBOL,[[[User getInstance] fetchUsersBalance] doubleValue]];
         NSLog(@"2.4");
    }
    NSLog(@"IN");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
