//
//  EmptyWalletView.m
//  whatsalon
//
//  Created by admin on 10/1/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import "EmptyWalletView.h"
#import "whatsalon-Swift.h"

@implementation EmptyWalletView
@synthesize delegate;

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void) customInit {
    [[NSBundle mainBundle] loadNibNamed:@"EmptyWalletView" owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = self.bounds;
}

-(void)setAddCardButton:(UIButton *)addCardButton
{
    _addCardButton = addCardButton;
    _addCardButton.clipsToBounds = true;
    _addCardButton.layer.cornerRadius = 2;
    [Gradients addPinkToPurpleHorizontalLayerWithView:_addCardButton];

}

- (IBAction)addCardTapped:(id)sender {
    [self.delegate onAddCardTapped:self];
}

@end
