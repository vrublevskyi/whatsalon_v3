//
//  RESViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "RESideMenu.h"

@interface RESViewController : RESideMenu <RESideMenuDelegate>

-(void)blurBackgroundImage;

@end
