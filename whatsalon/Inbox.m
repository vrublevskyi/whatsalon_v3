//
//  Inbox.m
//  whatsalon
//
//  Created by Graham Connolly on 26/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "Inbox.h"

@implementation Inbox

-(instancetype) initWithInboxID:(NSString *)i_id{
    
    self=[super init];
    if (self) {
        self.inbox_id=i_id;
        self.to =@"";
        self.from=@"";
        self.subject=@"";
        self.content=@"";
        self.recieved=@"";
       
        
    }
    return self;
}

+(instancetype) inboxWithInboxID:(NSString *)i_id{
    
    return [[self alloc] initWithInboxID:(NSString *)i_id];
}
@end
