//
//  MenuViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "MenuViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "BrowseViewController.h"
#import "SettingsViewController.h"
#import "RESViewController.h"
#import "RESideMenu/RESideMenu.h"
#import "User.h"
#import "DiscoveryViewController.h"
#import "FavouriteViewController.h"
#import "ShortSettingsTableViewController.h"
#import "EditProfileViewController.h"
#import "WalkthroughViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ColoredImage.h"
#import "CallOutOptionsViewController.h"
#import "UITableView+ReloadTransition.h"
#import "FUIAlertView.h"
#import "SalonReviewViewController.h"
#import "InboxViewController.h"
#import "WalletViewController.h"

@interface MenuViewController ()<GCNetworkManagerDelegate>

/*
  inboxButton - UIButton to go to the inbox page
 */
@property (weak, nonatomic) IBOutlet UIButton *inboxButton;
/*
 settingsButton - UIButton to go to the settings page
 */
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;

/*
 loggedOutOptions - NSMutableArray holding the menu options for when the user is logged out
 */
@property (nonatomic) NSMutableArray * loggedOutOptions;

/*
 loggedOutImages - NSArray holding the menu images for when the user logs out
 */
@property (nonatomic) NSArray * loggedOutImages;

/*
 loginSignUpLabel - UILabel prompting the user to Login or Signup
 */
@property (nonatomic) UILabel * loginSignUpLabel;
/*
 loginBackgroundView - UIView which holds the loginSignUpLabel UIlabel
 */
@property (nonatomic) UIView * loginBackgroundView;
/*
 notificationView - UIView for holding inbox notifications
 */
@property (nonatomic) UIView * notificationView;

@property (nonatomic) UIButton * switchButton;

@property (nonatomic) BOOL isSalonSearch;

@property (nonatomic) UIView * settingsBackgroundView;

@property (nonatomic) GCNetworkManager * networkManager;

@property (nonatomic) UIView * emailBackgroundView;
@end

@implementation MenuViewController

#pragma mark - UIView LifeCycle Methods
/*
 viewDidLoad
 Creates two notifcations;
    BlurMenuImage - which triggers the addBlurToMenu actions
    userLoggedIn - which triggers the userLoggedIn action
 
 updates the users balance by calling [WSCore getUsersBalance]
 
 initializes the menuOptions NSArray
 initializes the images NSArray
 
 initializes the loggedOutOptions
 
 calls [self tableViewInit]
 
 adds a tap gesture recognizer to the profile image - this allows the profile image to act as a button
 
 calls [self avatarButtonSetUp]
 
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToBookings:) name:@"reloadData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn) name:kRefreshMenuNotification object:nil];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        //get users balance
        [WSCore getUsersBalance];
        
    });
    
    
    
    self.loggedInMenuOptions = [[NSArray alloc] initWithObjects:@"",@"Discover",@"Favourites",@"My Bookings",@"Wallet", nil];
    self.images = @[@" ",@"Compass",@"White_Heart", @"clock",@"Wallet"];
    
    self.loggedOutOptions = [[NSMutableArray alloc] initWithObjects:@"",@"Discover",@"Log In/Sign Up",@"My Bookings",nil];
    self.loggedOutImages = @[@" ",@"Compass",@"White_Heart", @"clock"];
    
    
    [self tableViewInit];
    
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarClickAction)];
    tap.numberOfTapsRequired=1;
    [self.profileImage addGestureRecognizer:tap];
    self.profileImage.userInteractionEnabled=YES;
    
    [self avatarButtonSetUp];
    
    
    self.isSalonSearch=YES;
    
    if (IS_iPHONE4) {
         [self avatarAndProfileAppearance];
        
    }
    /*
   if (!IS_IOS_8_OR_LATER && IS_iPHONE4) {
        self.inboxButton.hidden=YES;
        self.emailBackgroundView.hidden=YES;
    }
    */
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
    self.networkManager.parentView=self.view;
    
    self.salonLogo.userInteractionEnabled=YES;
    
    UITapGestureRecognizer * toFeedback = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toFeedback)];
    toFeedback.numberOfTapsRequired=1;
    [self.salonLogo addGestureRecognizer:toFeedback];
    
}

-(void)toFeedback{
    
    if([[User getInstance] isUserLoggedIn]){
        UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
        navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"feedback"]];
        [self.sideMenuViewController hideMenuViewController];
    }

}

-(void)switchMenu{
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    if (self.isSalonSearch) {
        [self.switchButton setTitle:@"Switch to Salon search" forState:UIControlStateNormal];
        self.loggedInMenuOptions = [[NSArray alloc] initWithObjects:@"",@"Callout",@"Favourites",@"My Bookings", nil];
        self.images = @[@" ",@"suitcase_icon",@"White_Heart", @"clock",];

        
        self.isSalonSearch=NO;
        

        CallOutOptionsViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"CallOutVC"];
        navigationController.viewControllers=@[cvc];
        
        [self tableViewFadeIn];
    }
    else{
        [self.switchButton setTitle:@"Switch to Callout" forState:UIControlStateNormal];
        self.loggedInMenuOptions = [[NSArray alloc] initWithObjects:@"",@"Browse",@"Favourites",@"My Bookings",@"Feedback", nil];
        self.images = @[@" ",@"Compass",@"White_Heart", @"clock",@"speech.png",];

        self.isSalonSearch=YES;
        
        BrowseViewController * bvc =[[self storyboard] instantiateViewControllerWithIdentifier:@"browseController"] ;
        navigationController.viewControllers = @[bvc];
        [self tableViewFadeIn];
    }
    
   

}

-(void)goToBookings: (NSNotification *)notification{
    
    /*
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    //[navigationController popToRootViewControllerAnimated:YES];
    
    NSLog(@"root controller %@",[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController]);
    for (UIViewController *controller in navigationController.viewControllers) {
        
        NSLog(@"controllers %@",[controller class]);
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[MenuViewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
        }
    }
     */
    
    
    /*
    NSDictionary * userInfo = [notification userInfo];
    NSLog(@"UserInfo = %@", userInfo);
    
    NSData *encodedObject = [userInfo objectForKey:@"Salon"];
    Salon * object = (Salon *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    
    NSLog(@"Go to reviews with salon %@",object.salon_name);
   
    [self.sideMenuViewController presentMenuViewController];
    
   
    if ([[User getInstance] isUserLoggedIn]) {
        NSLog(@"Show reviews");
        SalonReviewViewController *viewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"SalonReviewController"];
        viewController.modalPresentationStyle = UIModalPresentationCurrentContext;
        //viewController.transitioningDelegate = self;
        
        
       //viewController.confirmID=self.bookingData.bookingId;
       // viewController.booking=self.bookingData;
       // viewController.myDelegate=self;
        
        [self presentViewController:viewController animated:YES completion:nil];
    }
   
    */
    
    
}
-(void)tableViewFadeIn{
    
    [self.tableViewMenu reloadDataAnimated:YES];
    [self performSelector:@selector(showScreen) withObject:nil afterDelay:0.8];
    
    
}
-(void)showScreen{
    [self.sideMenuViewController hideMenuViewController];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
}

/*
 viewDidLayoutSubviews
 handles the layout for the subviews of the iPhone4 series
 */
-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    if (IS_iPHONE4){
        
        self.profileImage.frame = CGRectMake(96, 43, 80, 80);
        self.nameLabel.frame = CGRectMake(86, 131, 108, 21);
        self.pointsLabel.frame = CGRectMake(62, 149, 42, 24);
        self.rewardPoints.frame = CGRectMake(112, 150, 126, 21);
        self.infoButton.frame = CGRectMake(180, 150, self.infoButton.frame.size.width, self.infoButton.frame.size.height);
        self.salonLogo.frame = CGRectMake(115, 414, 51, 53);
        self.tableViewMenu.frame = CGRectMake(0, 170, 273, 228);
        self.profileImage.layer.cornerRadius = roundf(self.profileImage.frame.size.width/2.0);
        self.settingsButton.frame = CGRectMake(160, self.settingsButton.frame.origin.y-20, self.settingsButton.frame.size.width, self.settingsButton.frame.size.height);
        self.salonLogo.frame = CGRectMake(self.salonLogo.frame.origin.x, self.salonLogo.frame.origin.y-30, self.salonLogo.frame.size.width, self.salonLogo.frame.size.height);
        self.inboxButton.frame= CGRectMake(self.inboxButton.frame.origin.x+10, self.inboxButton.frame.origin.y-20, self.inboxButton.frame.size.width, self.inboxButton.frame.size.height);
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Set Up Methods





/*
 tableViewInit
 sets the tableViewMenu delegates and datasources and customises its appearance
 */
- (void)tableViewInit
{
    self.tableViewMenu.delegate=self;
    self.tableViewMenu.dataSource=self;
    self.tableViewMenu.opaque = YES;
    self.tableViewMenu.backgroundColor = [UIColor clearColor];
    self.tableViewMenu.backgroundView=nil;
    self.tableViewMenu.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableViewMenu.bounces=NO;
    self.tableViewMenu.scrollsToTop=NO;
    UIView * footerView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableViewMenu.tableFooterView = footerView;
    self.tableViewMenu.center = CGPointMake(0, self.view.frame.origin.y);
}



/*
 avatar button set up
 
    customizing the buttons for settings and inbox.
 
    The buttons are clear with a semi transparent view behind it. This decision was made so there would be button icons would not be transparent also if transparency was set on the butotn itself
 */
- (void)avatarButtonSetUp
{
    self.inboxButton.layer.cornerRadius=roundf(self.inboxButton.bounds.size.width/2.0f);
    self.inboxButton.layer.masksToBounds=YES;
    self.settingsButton.layer.cornerRadius=roundf(self.settingsButton.frame.size.width/2.0f);
    self.settingsButton.layer.masksToBounds=YES;
    
    if (IS_iPHONE4) {
        self.settingsBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(160, self.settingsButton.frame.origin.y-20, self.settingsButton.frame.size.width, self.settingsButton.frame.size.height)];
    }
    else{
       self.settingsBackgroundView = [[UIView alloc] initWithFrame:self.settingsButton.frame];
    }
    
    self.settingsBackgroundView.backgroundColor=kGroupedTableGray;
    self.settingsBackgroundView.layer.cornerRadius = self.settingsButton.layer.cornerRadius;
    self.settingsBackgroundView.alpha=0.2;
    [self.view insertSubview:self.settingsBackgroundView belowSubview:self.settingsButton];
    
    if (IS_iPHONE4) {
        self.emailBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(self.inboxButton.frame.origin.x+10,self.settingsBackgroundView.frame.origin.y, self.settingsButton.frame.size.width, self.settingsButton.frame.size.height)];
    }
    else{
        self.emailBackgroundView = [[UIView alloc] initWithFrame:self.inboxButton.frame];
    }
   // UIView * emailBackgroundView = [[UIView alloc] initWithFrame:self.inboxButton.frame];
    self.emailBackgroundView.backgroundColor=kGroupedTableGray;
    self.emailBackgroundView.layer.cornerRadius = self.settingsButton.layer.cornerRadius;
    self.emailBackgroundView.alpha=0.2;
    //self.emailBackgroundView = emailBackgroundView;
    [self.view insertSubview:self.emailBackgroundView belowSubview:self.inboxButton];
    
    //remove for inbox
    
    UIImage *gear = [[UIImage imageNamed:@"gear_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.settingsButton setImage:gear forState:UIControlStateNormal];
    self.settingsButton.tintColor = [UIColor whiteColor];
    
    [self.settingsButton addTarget:self action:@selector(toSettings) forControlEvents:UIControlEventTouchUpInside];
    
    [self.inboxButton addTarget:self action:@selector(toInbox) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *email = [[UIImage imageNamed:@"email_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.inboxButton setImage:email forState:UIControlStateNormal];
    self.inboxButton.tintColor = [UIColor whiteColor];
    
    
    
}



/*
 userLoggedIn
 calls the avatarAppearance
 */
-(void)userLoggedIn{
    
    [self avatarAndProfileAppearance];
}



/*
 toSettings
 If the user is not signed in, pop to the shortSettings View controller
 if the user is signed in, go to the full login
 */
-(void)toSettings{
    
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    

    if (![[User getInstance] isUserLoggedIn]) {
        ShortSettingsTableViewController * shortSettings = [self.storyboard instantiateViewControllerWithIdentifier:@"shortSettingsVC"];
        navigationController.viewControllers=@[shortSettings];
        [self.sideMenuViewController hideMenuViewController];
    }
    else if([[User getInstance] isUserLoggedIn]){
        SettingsViewController * sVc = [[self storyboard] instantiateViewControllerWithIdentifier:@"settingsViewController"];
        navigationController.viewControllers = @[sVc];
        [self.sideMenuViewController hideMenuViewController];
    }

}



/*
 toInbox - inbox action
 */
-(void)toInbox{
    
    NSLog(@"To inbox");
    
    //inboxVC
   
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;

    InboxViewController * iVC = [self.storyboard instantiateViewControllerWithIdentifier:@"inboxVC"];
    navigationController.viewControllers =@[iVC];
    [self.sideMenuViewController hideMenuViewController];
}



/*
 avatarAction
 method which handles the selection of the Avatar
 if the user is logged in then show the EditProfileViewController
 if the user is not logged in, then perform the toLogin method
 */
-(void)avatarClickAction{
    
    NSLog(@"Avatar click");
    if ([[User getInstance] isUserLoggedIn]) {
        EditProfileViewController * editProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileController"];
        editProfile.myDelegate=self;
        editProfile.isFromMenu=YES;
        [self presentViewController:editProfile animated:YES completion:^{
            [WSCore statusBarColor:StatusBarColorBlack];
        }];
       
    }
    else{
       
        [self toLoginViewController];
    }
   
}




- (void)toLoginSignUpSettingsView:(UINavigationController *)navigationController
{
    SettingsViewController * sVc = [[self storyboard] instantiateViewControllerWithIdentifier:@"settingsViewController"];
    navigationController.viewControllers = @[sVc];
    [self.sideMenuViewController hideMenuViewController];
}



#pragma mark UITableView Delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    
    switch (indexPath.row) {
            
        case 0:{
            
            
            break;
        }
        case 1:{
            
            if (self.isSalonSearch) {
                /*
                BrowseViewController * bvc =[[self storyboard] instantiateViewControllerWithIdentifier:@"browseController"] ;
                navigationController.viewControllers = @[bvc];
                [self.sideMenuViewController hideMenuViewController];
                 */
                DiscoveryViewController * disc = [self.storyboard instantiateViewControllerWithIdentifier:@"discovery1"];
                navigationController.viewControllers=@[disc];
                [self.sideMenuViewController hideMenuViewController];
            }
            else if(!self.isSalonSearch){
                CallOutOptionsViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"CallOutVC"];
                navigationController.viewControllers=@[cvc];
                
                [self.sideMenuViewController hideMenuViewController];
            }
            
            
            break;
        }
        case 2:{
            
            
        //if the user is logged then go to the Favourites section
            if ([[User getInstance] isUserLoggedIn]) {
                FavouriteViewController *fvc =[self.storyboard instantiateViewControllerWithIdentifier:@"favVC"];
                fvc.isFromMenu=YES;
                navigationController.viewControllers=@[fvc];
                [self.sideMenuViewController hideMenuViewController];
            }
            
            
            else if(![[User getInstance] isUserLoggedIn]){
                
                //else go to log in sign up
                [self toLoginSignUpSettingsView:navigationController];
            }
           
            break;
        }
        case 3:
            
            //if logged in go to my bookings
            if ([[User getInstance] isUserLoggedIn]) {
                navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"myBookingsController"]];
                [self.sideMenuViewController hideMenuViewController];
            }
            else{
                
                // else go to log in sign up
                [self toLoginSignUpSettingsView:navigationController];
            }
    
            break;
            case 4:{
            
                /*
            if (![[User getInstance] isUserLoggedIn]) {
                [self toLoginSignUpSettingsView:navigationController];

            }
            else if([[User getInstance] isUserLoggedIn]){
                navigationController.viewControllers = @[[self.storyboard instantiateViewControllerWithIdentifier:@"feedback"]];
                [self.sideMenuViewController hideMenuViewController];
            }
                 */
                
                NSLog(@"Wallet");
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(walletCancelledSoRefreshTheScreen)
                                                             name:@"WalletNotification"
                                                           object:nil];
                
                [self performSegueWithIdentifier:@"WalletVC" sender:self];

            
            break;
        
                /*
            case 5:{
                NSLog(@"Wallet");
                
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(walletCancelledSoRefreshTheScreen)
                                                             name:@"WalletNotification"
                                                           object:nil];
                
                [self performSegueWithIdentifier:@"WalletVC" sender:self];
        
            }
                break;
                 
                 */
                
        }
        
        
            
        default:
        break;
    }
             
}


#pragma mark - Wallet delegate
-(void)walletCancelledSoRefreshTheScreen{
    NSLog(@"Refreshed");
    [self updateBalance];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"WalletNotification" object:nil];
}

#pragma mark -
#pragma mark UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    if ([[User getInstance] isUserLoggedIn]) {
         return self.loggedInMenuOptions.count;
    }
    return self.loggedOutOptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //handle custom cells
    UILabel * optionLabel = (UILabel *)[cell viewWithTag:kMenuCellLabel];
    UIImageView * image = (UIImageView *)[cell viewWithTag:kMenuCellImage];
    
    //no interaction for cell one
    if (indexPath.row ==0) {
        cell.userInteractionEnabled=NO;
    }
   
    //set contents of cells
    if ([[User getInstance] isUserLoggedIn]) {
        
    
        optionLabel.text = [self.loggedInMenuOptions objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor clearColor];
        optionLabel.textColor = [UIColor whiteColor];
        optionLabel.highlightedTextColor = [UIColor lightGrayColor];
    
        image.image = [UIImage imageNamed:[self.images objectAtIndex:indexPath.row]];
        image.image = [image.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [image setTintColor:[UIColor whiteColor]];
        
       
    }
    else if(![[User getInstance] isUserLoggedIn])  {
        
        
        optionLabel.text = [self.loggedOutOptions objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor clearColor];
        optionLabel.textColor = [UIColor whiteColor];
        optionLabel.highlightedTextColor = [UIColor lightGrayColor];
        
        image.image = [UIImage imageNamed:[self.loggedOutImages objectAtIndex:indexPath.row]];
        image.image = [image.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [image setTintColor:[UIColor whiteColor]];
        
        
        if (indexPath.row==2||indexPath.row==3) {
            optionLabel.textColor = [UIColor lightGrayColor];
            optionLabel.highlightedTextColor = [UIColor lightGrayColor];
            optionLabel.alpha=0.8;
            
            image.image = [UIImage imageNamed:[self.loggedOutImages objectAtIndex:indexPath.row]];
            image.image = [image.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [image setTintColor:[UIColor lightGrayColor]];
            image.alpha=0.8;
           

        }
        

    }
    
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //size of cell height

    return 38;
    
}



#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController{
   
    [WSCore statusBarColor:StatusBarColorWhite];
    [self avatarAndProfileAppearance];
    if ([WSCore isDummyMode]) {
        self.switchButton.hidden=YES;
    }
    self.navigationController.delegate=nil;
    
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController{
   
    
}


- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController{
    
    [WSCore statusBarColor:StatusBarColorBlack];
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController{
  
}

#pragma mark - EditProfileDelegate
-(void)updateMenuProfile{
 
    self.nameLabel.text = [[User getInstance] fetchFirstName];
    [self getAndSetUsersProfileImage];
    
}

- (void)getAndSetUsersProfileImage {
    
    //check if users avatar is available - means they signed in via facebook
    //check if users image url is available
    //back up - check if image data available
    //else set default image
   if ([[User getInstance] hasImageURL])  {
      
        NSLog(@"URL for avatar image %@",[[User getInstance] fetchImageURL]);
        
        UIImage * oldImage = self.profileImage.image;
        [self.profileImage sd_setImageWithURL:[[User getInstance] fetchImageURL ] placeholderImage:oldImage
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 
                                        if (IS_iPHONE4) {
                                            self.sideMenuViewController.backgroundImage=self.profileImage.image;
                                        }else{
                                             self.sideMenuViewController.backgroundImage=[WSCore facebookImageView:self.profileImage];
                                        }
                                
                             }];
       
   }
    else  {
        
        self.profileImage.image = [UIImage imageNamed:kProfile_image];
        self.sideMenuViewController.backgroundImage=[UIImage imageNamed:kMenu_wallpaper];
        
    }
    
}

- (void)updateBalance {
    self.nameLabel.adjustsFontSizeToFitWidth=YES;
    self.rewardPoints.text = @"Balance";
    
    NSString * price =[[User getInstance] fetchUsersBalance];
    NSLog(@"Price %@",price);
    self.pointsLabel.text = [NSString stringWithFormat:@"%@%.02f ",CURRENCY_SYMBOL,[price floatValue]];
    
    
    self.pointsLabel.hidden=NO;
    self.rewardPoints.hidden=NO;
    
    self.infoButton.hidden=NO;
}

/*
 handles the avatarAppearance whether the user is logged in or logged out
 */
-(void)avatarAndProfileAppearance{
    
    [self.tableViewMenu reloadData];
    
    if (![[User getInstance] isUserLoggedIn]) {
        self.infoButton.hidden=YES;
        self.inboxButton.alpha=0.4;
        self.inboxButton.tintColor=[UIColor lightGrayColor];
        self.inboxButton.userInteractionEnabled=NO;
        
        [self.notificationView removeFromSuperview];
        
    
        self.profileImage.image = [UIImage imageNamed:kProfile_image];
        self.sideMenuViewController.backgroundImage=[UIImage imageNamed:kMenu_wallpaper];
    
        self.profileImage.userInteractionEnabled=YES;
        
        UILabel * label = [[UILabel alloc] initWithFrame:self.profileImage.frame];
        label.text = @"Log In or Sign Up";
        label.textColor=[UIColor whiteColor];
        label.textAlignment=NSTextAlignmentCenter;
        label.numberOfLines = 0;
        self.loginSignUpLabel=label;
        [self.profileImage addSubview:self.loginSignUpLabel ];
        //self.loginSignUpLabel.center =self.profileImage.center;
        
        self.nameLabel.text = @"Welcome";
        self.nameLabel.textAlignment=NSTextAlignmentCenter;
        self.nameLabel.font = [UIFont systemFontOfSize:16.0f];
        self.pointsLabel.hidden=YES;
        self.rewardPoints.hidden=YES;

    }  else{
        
        //self.notificationView = [WSCore addNotificationBubbleToView:self.inboxButton WithNumber:@"9+"];
        //[self.inboxButton addSubview:self.notificationView];
        self.inboxButton.alpha=1.0;
        self.inboxButton.tintColor=[UIColor whiteColor];
        self.inboxButton.userInteractionEnabled=YES;
        
        self.profileImage.hidden=NO;
        self.profileImage.contentMode=UIViewContentModeScaleAspectFill;
        self.profileImage.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth);
        [self.loginBackgroundView removeFromSuperview];
        [self.loginSignUpLabel removeFromSuperview];
        
        
        [self getAndSetUsersProfileImage];
        
        
        
        if ([[User getInstance] hasCreditInAccount]) {
            [self updateBalance];
        }
        else{
            self.pointsLabel.hidden=YES;
            self.rewardPoints.hidden=YES;
            self.infoButton.hidden=YES;
        }
        
       

    }

    // use the image's layer to mask the image into a circle
    self.profileImage.layer.cornerRadius = roundf(self.profileImage.frame.size.width/2.0);
    self.profileImage.layer.masksToBounds = YES;
    //round edges of points label
    self.pointsLabel.layer.cornerRadius = 3;
   
}


-(void)toLoginViewController{
 
    NSLog(@"Login sign up");
     UINavigationController *navigationController = (UINavigationController *)self.sideMenuViewController.contentViewController;
    [self toLoginSignUpSettingsView:navigationController];
}
- (IBAction)showBalanceInfo:(id)sender {
    
    
    FUIAlertView *alertView = [[FUIAlertView alloc] initWithTitle:@"What is this?"
                                                          message:@"Your balance can be used against the portion of your booking (10%) that you pay to WhatSalon. \n\n To update your balance, please go to Wallet then 'Redeem code'."
                                                         delegate:nil cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    alertView.titleLabel.textColor = [UIColor cloudsColor];
    //alertView.titleLabel.font = [UIFont boldFlatFontOfSize:16];
    alertView.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    alertView.messageLabel.textColor = [UIColor cloudsColor];
    alertView.messageLabel.font = [UIFont systemFontOfSize:14.0];
    //alertView.messageLabel.font = [UIFont flatFontOfSize:14];
    alertView.backgroundOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    alertView.alertContainer.backgroundColor = kWhatSalonBlue;
    alertView.defaultButtonColor = [UIColor cloudsColor];
    alertView.defaultButtonShadowColor = [UIColor asbestosColor];
    alertView.defaultButtonShadowHeight=kShadowHeight;
    //alertView.defaultButtonFont = [UIFont boldFlatFontOfSize:16];
    alertView.defaultButtonTitleColor = [UIColor asbestosColor];
    [alertView show];
    
    /*
    FUIAlertView *alertView = [[FUIAlertView alloc] initWithTitle:@"What is this?"
                                                          message:@"Your balance can be used against the portion of your booking (10%) that you pay to WhatSalon. \n\n Lucky enough to have a redeem code, enter it below and see what you get!!"
                                                         delegate:nil cancelButtonTitle:@"Cancel"
                                                otherButtonTitles: @"Submit",nil];
    alertView.alertViewStyle=FUIAlertViewStylePlainTextInput;
    alertView.titleLabel.textColor = [UIColor cloudsColor];
    alertView.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    alertView.messageLabel.textColor = [UIColor cloudsColor];
    alertView.messageLabel.font = [UIFont systemFontOfSize:14.0];
    alertView.backgroundOverlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    alertView.alertContainer.backgroundColor = kWhatSalonBlue;
    alertView.defaultButtonColor = [UIColor cloudsColor];
   // alertView.defaultButtonTitleColor=[UIColor pomegranateColor];
    alertView.defaultButtonShadowColor = [UIColor asbestosColor];
    alertView.defaultButtonShadowHeight=kShadowHeight;
    alertView.defaultButtonTitleColor = [UIColor asbestosColor];
    [alertView.buttons[1] setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    [[alertView textFieldAtIndex:0] setPlaceholder:@"Enter Redeem Code"];
    [[alertView textFieldAtIndex:0] setBackgroundColor:[UIColor cloudsColor]];
    [alertView textFieldAtIndex:0].layer.cornerRadius=kCornerRadius;
    [alertView textFieldAtIndex:0].autocapitalizationType=UITextAutocapitalizationTypeAllCharacters;
    
    UITextField * text = [alertView textFieldAtIndex:0];
    
    alertView.onOkAction=^{
        
        
        [self submitRedeemCodeWithCode:text.text];
    };
    [alertView show];
    */

}



-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",jsonDictionary[kMessageIndex]] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    [[User getInstance] updateUsersBalance:jsonDictionary[@"message"][@"balance"]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:jsonDictionary[@"message"][@"message"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    NSLog(@"%@",jsonDictionary[@"message"][@"message"]);
    [alert show];
    
    [self updateBalance];
    
}
-(void)submitRedeemCodeWithCode: (NSString *) code{
    
    [self.view endEditing:YES];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kRedeem_Code_URL]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken ]];params = [params stringByAppendingString:[NSString stringWithFormat:@"&discount_code=%@",code]];
    
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}

@end
