//
//  ChangePasswordViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 09/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "User.h"
#import "GCNetworkManager.h"
#import "UIView+AlertCompatibility.h"
#import "whatsalon-Swift.h"

@interface ChangePasswordViewController ()<UITextFieldDelegate,GCNetworkManagerDelegate>

/*! @brief represents the view for the old password view. */
@property (weak, nonatomic) IBOutlet UIView *viewForOldPassword;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

/*! @brief represents the view for the old text field. */
@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;

/*! @brief represents the view for the password. */
@property (weak, nonatomic) IBOutlet UIView *viewForNewPassword;

/*! @brief represents the textfield for the password. */
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

/*! @brief represents the view for the confirm old password. */
@property (weak, nonatomic) IBOutlet UIView *viewForConfirmOldPassword;

/*! @brief represents the confirm password textfield. */
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextfield;

/*! @brief represents the reset password button. */
@property (weak, nonatomic) IBOutlet UIButton *resetPasswordButton;

/*! @brief represents the network manager for making requests. */
@property (nonatomic) GCNetworkManager * changePasswordNetworkManager;

/*! @brief represents the network manager for making requests. */
@property (nonatomic) GCNetworkManager * resetPasswordNetworkManager;

/*! @brief represents the save changes action. */
- (IBAction)saveChanges:(id)sender;

/*! @brief represents the reset password. */
- (IBAction)resetPassword:(id)sender;



@end

@implementation ChangePasswordViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.changePasswordNetworkManager = [[GCNetworkManager alloc] init];
    self.changePasswordNetworkManager.delegate =self;
    self.changePasswordNetworkManager.parentView = self.view;
    
    self.resetPasswordNetworkManager = [[GCNetworkManager alloc] init];
    self.resetPasswordNetworkManager.delegate =self;
    self.resetPasswordNetworkManager.parentView = self.view;
    
    self.oldPasswordTextField.secureTextEntry=YES;
    self.oldPasswordTextField.delegate=self;
    self.oldPasswordTextField.returnKeyType=UIReturnKeyNext;
    self.oldPasswordTextField.tag=0;
    
    self.passwordTextField.secureTextEntry=YES;
    self.passwordTextField.delegate=self;
    self.passwordTextField.tag=1;
    self.passwordTextField.returnKeyType=UIReturnKeyNext;
    
    self.confirmPasswordTextfield.secureTextEntry=YES;
    self.confirmPasswordTextfield.delegate=self;
    self.confirmPasswordTextfield.returnKeyType=UIReturnKeyDone;
    self.confirmPasswordTextfield.tag=2;
    
    
    [self configureButtonsUI];
    [Gradients addPinkToPurpleHorizontalLayerWithView:_saveButton];

    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore statusBarColor:StatusBarColorBlack];
    [WSCore flatNavBarOnView:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GCNetworkManager
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        if (manager==self.changePasswordNetworkManager) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:jsonDictionary[@"message"] message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }else{
            [UIView showSimpleAlertWithTitle:jsonDictionary[@"message"] message:@"" cancelButtonTitle:@"OK"];
        }
        
        
    });

}

-(void) configureButtonsUI {
    self.resetButton.clipsToBounds = true;
    self.resetButton.layer.cornerRadius =2;
    self.resetButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.resetButton.layer.borderWidth = 2;
    
    self.saveButton.clipsToBounds = true;
    self.saveButton.layer.cornerRadius = 2;
    
}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (manager == self.changePasswordNetworkManager) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:jsonDictionary[@"message"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.tag=1;
            [alert show];
        }
        else{
            
            [UIView showSimpleAlertWithTitle:jsonDictionary[@"message"] message:@"" cancelButtonTitle:@"OK"];
            

        }
        
    });
    
}

- (IBAction)saveChanges:(id)sender {
    NSLog(@"Save");

}

#pragma mark - UIAlertView delegate methods
// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(alertView.tag==2){
        
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            [self resetUsersPassword];
        }
        
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}

#pragma mark - UITextField delegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  
    
    UIView *view = [self.view viewWithTag:textField.tag + 1];
    if (!view)
        [textField resignFirstResponder];
    else
        [view becomeFirstResponder];
    return YES;
}
- (IBAction)saveAction:(id)sender {
    
    
    if (self.oldPasswordTextField.text.length == 0 || self.passwordTextField.text.length == 0 || self.confirmPasswordTextfield.text.length==0) {
        
        [UIView showSimpleAlertWithTitle:@"Oops" message:@"Please fill in all fields." cancelButtonTitle:@"OK"];
        
        
    }
    else if(self.passwordTextField.text.length <4){
        
        [UIView showSimpleAlertWithTitle:@"Oops" message:@"Passwords must be at least 4 characters long." cancelButtonTitle:@"OK"];
        
        
    }
    else{
        
        if ([self.confirmPasswordTextfield.text isEqualToString:self.passwordTextField.text]) {
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kChange_Password_URL]];
            NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&old_password=%@",self.oldPasswordTextField.text]];
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&new_password=%@",self.confirmPasswordTextfield.text]];
            
            [self.changePasswordNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
            
            
        }//end of passwords match
        else{
            
            [UIView showSimpleAlertWithTitle:@"New Passwords do not match." message:@"" cancelButtonTitle:@"OK"];
            
        }
        
    }
    
    [self.view endEditing:YES];
}

- (IBAction)resetPassword:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Reset Password" message:@"A new random password will be generated and emailed to you." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Continue",nil];
    alert.tag=2;
    [alert show];
}

-(void)resetUsersPassword{
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kResetPassword_URL]];
    NSString * params = [NSString stringWithFormat:@"email=%@",[[User getInstance] fecthEmail]];
    
    [self.resetPasswordNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
}
@end
