//
//  WelcomeTableViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 25/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "WelcomeTableViewController.h"

@interface WelcomeTableViewController ()

@end

@implementation WelcomeTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //all navigation bar stuff
    [self navigationBarSetUp];
    
   
}


-(void)navigationBarSetUp{
    //make navigation bar completely transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
    
    //change font style of navigation bar title
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:kAppFont size:18],
      NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    
    
    self.loginButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.loginButton.layer.borderWidth=1.0;
    self.loginButton.layer.cornerRadius=6.0;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    if (IS_iPHONE4) {
        
#warning fix autolayout constraints for tableview header
        //self.automaticallyAdjustsScrollViewInsets = NO;
        self.userAvatar.frame = CGRectMake(108, 19, 105, 108);
        self.topView.frame =CGRectMake(0, 64, 320, 146);
        self.topView.frame = self.topView.frame;
        self.forgotPassword.frame = CGRectMake(79, 145, 163, 30);
        self.bottomView.frame = CGRectMake(0, 300, 320, 180);
    }
}


@end
