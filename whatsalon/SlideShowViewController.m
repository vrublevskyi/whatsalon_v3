//
//  SlideShowViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 10/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SlideShowViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SlideShowViewController ()<UIScrollViewDelegate>

/*! @brief represents the UIPageControl */
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

/*! @brief represents the UIScrollView. */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

/*! @brief represents an NSMutableArray for holding UIViews. */
@property (nonatomic, strong) NSMutableArray *pageViews;

/*! @brief determines the visible pages. */
- (void)loadVisiblePages;

/*! @brief loads in a page.
 
 @param page - the page to be loaded in.
 
 */
- (void)loadPage:(NSInteger)page;

/*! @brief destroys a page.
 
 @param page - the page to be destroyed.
 
 */
- (void)purgePage:(NSInteger)page;

/*! @brief represents an array of salon images. */
@property (nonatomic) NSMutableArray * salonImages;
@end

@implementation SlideShowViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    if ([WSCore isDummyMode]) {
        
        //self.salonImages = [[NSMutableArray alloc] initWithArray:self.dummyImageArray];
        //[self.salonImages removeLastObject];
    }else{
        if ([self.salonData fetchSlideShowGallery].count ==0) {
            [self.salonImages removeAllObjects];
            self.salonImages =self.salonData.placeHolderImageGallery;
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Images" message:[NSString stringWithFormat:@"Unable to donwload images for %@. Please try again later.",self.salonData.salon_name] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }else{
            self.salonImages = [[NSMutableArray alloc] initWithArray:[self.salonData fetchSlideShowGallery]];
    
            [self.salonImages removeLastObject];
           
        }

    }
    
    NSInteger pageCount = self.salonImages.count;
    
  
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
  
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
    self.scrollView.delegate=self;
 
    
    self.view.backgroundColor=[UIColor blackColor];
    
    UITapGestureRecognizer * closeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeGesture)];
    closeGesture.numberOfTapsRequired=1;
    UILabel * closeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70,25,  60, 30)];
    closeLabel.backgroundColor=[UIColor blackColor];
    closeLabel.alpha=0.6;
    closeLabel.text = @"Close";
    closeLabel.textColor=[UIColor whiteColor];
    closeLabel.layer.cornerRadius=3.0;
    closeLabel.clipsToBounds=YES;
    closeLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:closeLabel];
    [closeLabel addGestureRecognizer:closeGesture];
    closeLabel.userInteractionEnabled=YES;
    [self.view bringSubviewToFront:closeLabel];
    
    
    
    
    UISwipeGestureRecognizer * swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.view addGestureRecognizer:swipeDown];
    
    UISwipeGestureRecognizer * swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    
    [self.view addGestureRecognizer:swipeUp];

}

-(void)swipe{
    
    [self closeGesture];
}

-(void)closeGesture{
    
    if ([self.myDelegate respondsToSelector:@selector(getLastPage:)]) {
        
        [self.myDelegate getLastPage:self.pageControl.currentPage];
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    CGSize pagesScrollViewSize = self.view.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.salonImages.count, pagesScrollViewSize.height);
    
    // Start point - Set initial content offset of scrollview
    self.scrollView.contentOffset = CGPointMake(pagesScrollViewSize.width * self.startIndex, self.scrollView.contentOffset.y);
 
    [self loadVisiblePages];
}

- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.salonImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // 1
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        
        /*
         Updated to fix iPhone 4
         
         */
      
        CGRect frame;
        if (IS_iPHONE4) {
            frame = CGRectMake(0, 0, self.scrollView.frame.size.width, 480);
        }
        else{
            frame = self.scrollView.bounds;
        }
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        
        UIImageView * newPageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"empty_cell"]];
        if ([WSCore isDummyMode]) {
            newPageView.image = [self.salonImages objectAtIndex:page];
        }
        else{
             [newPageView sd_setImageWithURL:[self.salonImages objectAtIndex:page] placeholderImage:[UIImage imageNamed:@"black"]];
        }
       
        newPageView.contentMode = UIViewContentModeScaleAspectFit;
        newPageView.frame = frame;
        [self.scrollView addSubview:newPageView];
       
        
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.salonImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
    // Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    // Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.salonImages.count; i++) {
        [self purgePage:i];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Load the pages that are now on screen
    [self loadVisiblePages];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (IS_iPHONE4) {
        self.pageControl.frame= CGRectMake(0, self.view.frame.size.height-self.pageControl.frame.size.height, self.view.frame.size.width, self.pageControl.frame.size.height);
        
       
    }
}

@end
