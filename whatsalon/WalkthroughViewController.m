//
//  WalkthroughViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 09/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "WalkthroughViewController.h"
#import "SettingsViewController.h"


@interface WalkthroughViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *logIn;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UIButton *signUp;

@property (nonatomic, strong) NSArray *pageImages;
@property (nonatomic, strong) NSMutableArray *pageViews;
@property (nonatomic) NSMutableArray * titles;
@property (nonatomic) NSMutableArray * paragraphs;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (nonatomic) int pageNo;
- (IBAction)closeWalkthrough:(id)sender;


- (void)loadVisiblePages;
- (void)loadPage:(NSInteger)page;
- (void)purgePage:(NSInteger)page;

@property (nonatomic) BOOL startAnimation;
@end

@implementation WalkthroughViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.scrollView.delegate=self;
    self.scrollView.pagingEnabled=YES;

    // 1
    self.pageImages = [NSArray arrayWithObjects:
                       [UIImage imageNamed:@"walkthrough_1a.JPG"],
                       [UIImage imageNamed:@"walkthrough_2.jpg"],
                       [UIImage imageNamed:@"walkthrough_3.jpg"],
                       [UIImage imageNamed:@"walkthrough_4.jpg"],
                       nil];
    
    NSInteger pageCount = self.pageImages.count;
    
    // 2
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    // 3
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
    self.scrollView.pagingEnabled=YES;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.titles = [[NSMutableArray alloc] initWithObjects:@"BROWSE",@"BOOK",@"BE BEAUTIFUL", nil];
    self.paragraphs = [[NSMutableArray alloc] initWithObjects:@"Take a virtual tour of all the salons near you. Browse beautiful photos and read reviews of previous customers.",@"Simply select the service and even the stylist you'd like to see. Choose the time that suits you best and book!",@"Go relax and enjoy yourself in one of our amazing salons. You deserve it!", nil];
    
    self.backgroundImage.image = [self.pageImages objectAtIndex:0];
    
    self.startAnimation=NO;
    
    UIView * blackView = [[UIView alloc]initWithFrame:self.backgroundImage.frame];
    blackView.backgroundColor=[UIColor colorWithRed:51.0/255.0 green:34.0/255.0 blue:0/255.0f alpha:03];
    blackView.alpha=0.4;
    [self.backgroundImage addSubview:blackView];
    
    self.signUp.backgroundColor=kWhatSalonBlue;
    self.signUp.layer.cornerRadius=3.0f;
    [self.signUp setTitle:@"LOG IN" forState:UIControlStateNormal];
    [self.signUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.logIn.backgroundColor=kWhatSalonBlue;
    self.logIn.layer.cornerRadius=3.0f;
    [self.logIn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.logIn setTitle:@"SIGN UP" forState:UIControlStateNormal];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.pageImages.count, pagesScrollViewSize.height);
    
    // 5
    [self loadVisiblePages];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // 1
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        // 2
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        // 3
        /*
        UIImageView *newPageView = [[UIImageView alloc] initWithImage:[self.pageImages objectAtIndex:page]];
        newPageView.contentMode = UIViewContentModeScaleAspectFit;
         */
        UIView * newPageView = [[UIView alloc] init];
        newPageView.frame = frame;
        newPageView.backgroundColor=[UIColor clearColor];
        
        [self.scrollView addSubview:newPageView];
       

        
        if (page==0) {
            /*
            UIView * coverView = [[UIView alloc] initWithFrame:newPageView.frame];
            coverView.backgroundColor=[UIColor blackColor];
            coverView.alpha=0.4;
            [newPageView addSubview:coverView];
            */
            UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake((frame.size.width-200)/2, 100, 200, 146)];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            imageView.image = [UIImage imageNamed:@"white_logo"];
            [newPageView addSubview:imageView];
            
            UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, self.view.frame.size.height-240, 240, 100)];
            infoLabel.numberOfLines=0;
            infoLabel.text = @"BOOK HAIR & BEAUTY APPOINTMENTS IN AN INSTANT";
            infoLabel.font = [UIFont boldSystemFontOfSize:21.0];
            infoLabel.textAlignment=NSTextAlignmentCenter;
            infoLabel.textColor=[UIColor whiteColor];
            infoLabel.center=CGPointMake(newPageView.center.x, infoLabel.center.y);
            [newPageView addSubview:infoLabel];
            
        }
        else{
            /*
            UIView * coverView = [[UIView alloc] initWithFrame:newPageView.frame];
            coverView.backgroundColor=[UIColor blackColor];
            coverView.alpha=0.4;
            [newPageView addSubview:coverView];
             */
            UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(16, self.view.frame.size.height-268, 288, 24)];
            title.font = [UIFont boldSystemFontOfSize:21.0f];
            title.textColor=[UIColor whiteColor];
            title.text = [self.titles objectAtIndex:page-1];
            [newPageView addSubview:title];
            
            UILabel * descText = [[UILabel alloc] initWithFrame:CGRectMake(16, self.view.frame.size.height-238, 260, 100)];
            //descText.text = @"Take a virtual tour of all the salons near you. Browse beautiful photos and read reviews of previous customers.";
            descText.text = [self.paragraphs objectAtIndex:page-1];
            descText.font = [UIFont systemFontOfSize:18.0f];
            descText.textColor=[UIColor whiteColor];
            descText.numberOfLines=0;
            [newPageView addSubview:descText];
        }
        // 4
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

-(void)fadeImage: (NSInteger) pageNo{
    
    NSLog(@"Page No %ld",(long)pageNo);
    NSLog(@"%@",[self.pageImages objectAtIndex:pageNo]);
    UIImage * toImage = [self.pageImages objectAtIndex:pageNo];
    [UIView transitionWithView:self.view
                      duration:0.33f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.backgroundImage.image =toImage;
                    } completion:NULL];
}


- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}


- (IBAction)closeWalkthrough:(id)sender {
    /*
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.myDelegate respondsToSelector:@selector(walkthroughIsDismissed)]) {
            [self.myDelegate walkthroughIsDismissed];
        }
    }];
     */
    [self dismissAndPresentLoginSignUpScreen];
}

- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
    // Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    // Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.pageImages.count; i++) {
        [self purgePage:i];
    }
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"END");
    
    
    if (self.startAnimation) {
        UIImage * toImage = [self.pageImages objectAtIndex:self.pageControl.currentPage];
        [UIView transitionWithView:self.backgroundImage
                          duration:0.4f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.backgroundImage.image = toImage;
                        } completion:nil];
    }
    
    self.startAnimation=NO;
    
     
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSLog(@"DId scroll");
    self.startAnimation=YES;
    
    
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {


    [self loadVisiblePages];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)dismissAndPresentLoginSignUpScreen{
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.myDelegate respondsToSelector:@selector(goToLoginSignUp)]) {
            [self.myDelegate goToLoginSignUp];
        }
    }];

}
- (IBAction)login:(id)sender {
    [self dismissAndPresentLoginSignUpScreen];
}

- (IBAction)signUp:(id)sender {
    [self dismissAndPresentLoginSignUpScreen];

}


@end
