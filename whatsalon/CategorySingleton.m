//
//  CategorySingleton.m
//  whatsalon
//
//  Created by Graham Connolly on 30/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "CategorySingleton.h"
#import "ServiceCategory.h"

@implementation CategorySingleton

static CategorySingleton *singletonInstance;

+ (instancetype)getInstance{
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
}

-(void)setCategories {
    
    //NSArray * messageArray = dataDictionary[kMessageIndex];
    NSMutableArray * hairArray =[[NSMutableArray alloc] initWithObjects:@"Hair", nil];
    NSMutableArray * nailsArray = [[NSMutableArray alloc] initWithObjects:@"Nails", nil];
    NSMutableArray * waxingArray = [[NSMutableArray alloc] initWithObjects:@"Waxing", nil];
    NSMutableArray * faceArray = [[NSMutableArray alloc] initWithObjects:@"Face", nil];
    NSMutableArray * eyesArray = [[NSMutableArray alloc] initWithObjects:@"Eyes", nil];
    NSMutableArray * massageArray = [[NSMutableArray alloc] initWithObjects:@"Massage", nil];
    NSMutableArray * tanArray = [[NSMutableArray alloc] initWithObjects:@"Tanning", nil];
    
    NSMutableDictionary * messageDict = [[NSUserDefaults standardUserDefaults] objectForKey:kCatDictionaryKey];
    for (id key in messageDict) {
        
        ServiceCategory * servC = [[ServiceCategory alloc] init];
        servC.catID = key[@"category_id"];
        servC.catName = key[@"parent"];
        servC.catService = key[@"category_name"];
        if ([servC.catName isEqualToString:@"Hair"]) {
            [hairArray addObject:servC];
            
        }
        else if([servC.catName isEqualToString:@"Nails"]){
            [nailsArray addObject:servC];
        }
        else if ([servC.catName isEqualToString:@"Waxing"]){
            [waxingArray addObject:servC];
        }
        else if ([servC.catName isEqualToString:@"Face"]){
            [faceArray addObject:servC];
        }
        else if([servC.catName isEqualToString:@"Eyes"]){
            [eyesArray addObject:servC];
        }
        else if([servC.catName isEqualToString:@"Massage"]){
            [massageArray addObject:servC];
        }
        else if([servC.catName isEqualToString:@"Tanning"]){
            [tanArray addObject:servC];
        }
        
    }
    completeArray = [[NSMutableArray alloc] initWithObjects:@"All Services", nil];
    
    [completeArray addObjectsFromArray: hairArray];
    [completeArray addObjectsFromArray:nailsArray];
    [completeArray addObjectsFromArray:waxingArray];
    [completeArray addObjectsFromArray:faceArray];
    [completeArray addObjectsFromArray:eyesArray];
    [completeArray addObjectsFromArray:massageArray];
    [completeArray addObjectsFromArray:tanArray];
    
}


-(NSMutableArray *)getSummitCategories{
    completeArray = [[NSMutableArray alloc] initWithObjects:@"All Services",@"Hair",@"Nails",@"Massage",@"Face", nil];
    
    return completeArray;
    
}
-(NSMutableArray*)getCategories {
    
    return completeArray;
}

@end
