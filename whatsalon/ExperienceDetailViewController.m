//
//  ExperienceDetailViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 25/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "ExperienceDetailViewController.h"
#import <MessageUI/MessageUI.h>
#import "UIView+AlertCompatibility.h"
#import "GCNetworkManager.h"
#import "User.h"

@interface ExperienceDetailViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIWebViewDelegate,GCNetworkManagerDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *rightNavBarHolder;
@property (weak, nonatomic) IBOutlet UIButton *heart;
@property (weak, nonatomic) IBOutlet UIButton *chat;
@property (weak, nonatomic) IBOutlet UIButton *email;
@property (weak, nonatomic) IBOutlet UIButton *shareActivity;
@property (nonatomic) BOOL isFinishedLoading;
- (IBAction)showShareSheet:(id)sender;


@property (weak, nonatomic) IBOutlet UIView *centerNavBarHolder;
@property (nonatomic) GCNetworkManager * networkManager;
@end

@implementation ExperienceDetailViewController

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [self.networkManager cancelTask];
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    if (!self.isFinishedLoading) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //[WSCore showNetworkLoadingOnView:self.view WithTitle:@"Loading page..."];
            [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        });
    }
    
   
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    self.isFinishedLoading=YES;
     dispatch_async(dispatch_get_main_queue(), ^{
          [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
     });
    
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        //[WSCore dismissNetworkLoadingOnView:self.view];
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    });
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage * heartImage = self.heart.imageView.image;
    if (self.expItem.is_user_favourite) {
        heartImage = [UIImage imageNamed:@"White_Heart_Solid"];
    }
    else{
        heartImage = [UIImage imageNamed:@"White_Heart"];
    }
    //UIImage * heartImage = self.heart.imageView.image;
    [self.heart setImage:[heartImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.heart.tintColor=kWhatSalonBlue;
    
    
    
    self.centerNavBarHolder.backgroundColor=[UIColor clearColor];
    
    UIImage *chatImage = self.chat.imageView.image;
    [self.chat setImage:[chatImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.chat.tintColor = kWhatSalonBlue;
    self.rightNavBarHolder.backgroundColor=[UIColor clearColor];
    
    UIImage * emailImage = self.email.imageView.image;
    [self.email setImage:[emailImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.email.tintColor=kWhatSalonBlue;
    
    UIImage * share = self.shareActivity.imageView.image;
    [self.shareActivity setImage:[share imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.shareActivity.tintColor=kWhatSalonBlue;
    
    [self.email addTarget:self action:@selector(showEmail) forControlEvents:UIControlEventTouchUpInside];

    [self.chat addTarget:self action:@selector(showChat) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.webView.delegate=self;
    
    NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.expItem.link]];
    [self.webView loadRequest:request];
    
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate=self;
}

-(void)showChat{
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *message = [NSString stringWithFormat:@"WhatSalon Blog: %@ , %@",self.expItem.title,self.expItem.link];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}
-(void)showEmail{
    // Email Subject
    NSString *emailTitle = @"WhatSalon Blog";
    // Email Content
    NSString * messageBody = [NSString stringWithFormat:@"WhatSalon Blog: %@ , %@",self.expItem.title,self.expItem.link];;
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setMessageBody:messageBody isHTML:YES];
    [mc setSubject:emailTitle];
 
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showShareSheet:(id)sender {
    
    UIImage * image = [WSCore takeScreenShotOfView:self.view];
    [self shareText:[NSString stringWithFormat:@"WhatSalon Blog: %@ , %@",self.expItem.title,self.expItem.link] andImage:image andUrl:[NSURL URLWithString:@"www.whatsalon.com"]];
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    activityController.excludedActivityTypes = @[UIActivityTypePrint,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList,UIActivityTypeAirDrop,UIActivityTypeCopyToPasteboard];
    [self presentViewController:activityController animated:YES completion:nil];
    
}

- (IBAction)favouriteAction:(id)sender {
    
    if ([[User getInstance] isUserLoggedIn]) {
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kFavourite_Experience_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&experience_id=%@",self.expItem.experience_id]];
        
        
        if (self.expItem.is_user_favourite) {
            NSLog(@"\n\n *** Remove from fav ****\n\n");
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&unfavourite=1"]];
        }
        else{
            NSLog(@"\n\n *** Add fav **** \n\n");
        }
        
        self.heart.userInteractionEnabled=NO;
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        
    }

    
    
    
    
    
   

}

#pragma mark - GCNetworkManger
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    self.heart.userInteractionEnabled=YES;
    UIImage * heartImage = self.heart.imageView.image;
    if (self.expItem.is_user_favourite) {
        
        heartImage = [UIImage imageNamed:@"White_Heart"];
        self.expItem.is_user_favourite=NO;;
        if ([self.myDelegate respondsToSelector:@selector(updateBlogPostAsNotFavouriteWithBlogID:)]) {
            [self.myDelegate updateBlogPostAsNotFavouriteWithBlogID:self.expItem.experience_id];
        }

        
        
    }
    else{
        
        self.expItem.is_user_favourite=YES;
        heartImage = [UIImage imageNamed:@"White_Heart_Solid"];
        
        if ([self.myDelegate respondsToSelector:@selector(updateBlogPostAsFavouriteWithBlogID:)]) {
            [self.myDelegate updateBlogPostAsFavouriteWithBlogID:self.expItem.experience_id];
        }
        
           }
    
    [self.heart setImage:[heartImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.heart.tintColor=kWhatSalonBlue;

}
-(void)manager:(GCNetworkManager *)manager handleServerFailureWithError:(NSError *)error{
    
}

@end
