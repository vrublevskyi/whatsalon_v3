//
//  ServicesTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 10/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "ServicesTableViewCell.h"
#import "FriendlyService.h"
#import "UILabel+Boldify.h"

@implementation ServicesTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
         self.clipsToBounds = YES;
        self.serviceNameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.serviceNameLabel];
        self.priceLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.priceLabel];
    }
    return self;
}
-(void) setUpServicesTableViewCellWithPhorestService: (id) ps{
    NSString * service_name;
    NSString * price;
    if ([ps isKindOfClass:[SalonService class]]) {
        SalonService * p = (SalonService *)ps;
        service_name=p.service_name;
        price= p.price;
    }
    else{
        FriendlyService *f = (FriendlyService *)ps;
        service_name=f.friendly_name;
        
    }
    NSLog(@"PS %@",service_name);
    NSLog(@"Services");
    CGRect serviceNameFrame = self.contentView.frame;
    serviceNameFrame.size.width=(self.contentView.frame.size.width-20)/2.0f;
    serviceNameFrame.origin.x = 10;
    self.serviceNameLabel.frame=serviceNameFrame;
    self.serviceNameLabel.text=service_name;
    self.serviceNameLabel.numberOfLines=0;
    self.serviceNameLabel.font = [UIFont systemFontOfSize:14.0f];
    
    if (service_name.length>=22) {
        self.serviceNameLabel.adjustsFontSizeToFitWidth=YES;
    }
    
    if (![WSCore stringIsEmpty:price]) {
        CGRect priceLabelFrame = self.contentView.frame;
        priceLabelFrame.size.width=(self.contentView.frame.size.width/2.0f)-10;
        if (IS_iPHONE4) {
            priceLabelFrame.size.width=(self.contentView.frame.size.width/2.0f)-40;
        }
        priceLabelFrame.origin.x=self.frame.size.width-priceLabelFrame.size.width;
        self.priceLabel.frame=priceLabelFrame;
        NSString * priceLabelString;
        NSLog(@"Currency Sybmol %@",self.currencySymbol);
        if (self.currencySymbol.length>0) {
            priceLabelString =[NSString stringWithFormat:@"from %@%.2f",self.currencySymbol,[price doubleValue]];
        }
        else{
            priceLabelString =[NSString stringWithFormat:@"from %@%.2f",CURRENCY_SYMBOL,[price doubleValue]];
        }
        
        
        self.priceLabel.text = priceLabelString;
        [self.priceLabel boldSubstring:@"from"];
        self.priceLabel.numberOfLines=0;
        self.priceLabel.textAlignment=NSTextAlignmentRight;
        self.priceLabel.textColor=[UIColor silverColor];
    }
    
}
@end
