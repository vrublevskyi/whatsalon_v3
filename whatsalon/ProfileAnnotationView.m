//
//  ProfileAnnotationView.m
//  whatsalon
//
//  Created by Graham Connolly on 13/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "ProfileAnnotationView.h"
#import <QuartzCore/QuartzCore.h>

@implementation ProfileAnnotationView


-(id) initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier annotationWithImage:(UIImage *)anonViewImage withUserImage:(UIImage *)userImage{
    self = [self initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    self.image = [self drawImage: anonViewImage withBadge:userImage];
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    //size of pin
    self.frame = CGRectMake(25.0, -25.0, 60.0, 70.0);
    
    
    self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mapRing2.png"]];
    self.imageView.frame = CGRectMake(-100.00, -100.00, 260.00, 260.00);
    [self addSubview:self.imageView];
    
    return self;
    
    
}

-(UIImage *)drawImage:(UIImage*)pinImage withBadge:(UIImage *)user{
        
    // Start the image context
    UIGraphicsBeginImageContextWithOptions(pinImage.size, NO, 0.0);
    UIImage *resultImage = nil;
    
    // Get the graphics context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Draw the first image
    [pinImage drawInRect:CGRectMake(0, 0, pinImage.size.width, pinImage.size.height)];
    
    // Get the frame of the second image
    CGRect rect = CGRectMake(7.0, 6.0, user.size.width/3.2, user.size.height/3.2);
    
    // Add the path of an ellipse to the context
    // If the rect is a square the shape will be a circle
    CGContextAddEllipseInRect(context, rect);
    // Clip the context to that path
    CGContextClip(context);
    
    // Do the second image which will be clipped to that circle
    [user drawInRect:rect];
    
    // Get the result
    resultImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the image context
    UIGraphicsEndImageContext();
    
    return resultImage;

}


@end
