//
//  TimerViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 29/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSCore.h"
#import "FUIButton.h"
#import "FUITextField.h"

@interface TimerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageTopConstraint;

@property (weak, nonatomic) IBOutlet UIView *holderView;

@property (strong, nonatomic) IBOutlet FUIButton *vip_btn;
- (IBAction)vipAccess:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *countdownLabel;
@property (nonatomic) NSDate * launchDate;
@property (nonatomic) NSTimer * timer;

@property (weak, nonatomic) IBOutlet FUIButton *email_btn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLayout;

@property (weak, nonatomic) IBOutlet UIView *emailView;
- (IBAction)submitEmail:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerYLayoutCountdownTimer;

@property (weak, nonatomic) IBOutlet FUITextField *emailTextfield;
-(void)updateLabel;
@end
