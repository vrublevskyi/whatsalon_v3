//
//  AddAddressVC.swift
//  WhatSalon
//
//  Created by Yriy Malyts on 7/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import MapKit
import EmptyKit
import CoreLocation

@objc protocol AddAddressVCDelegate: class {
    func onAddressSelected(location: CLLocation,  address: String, on controller:AddAddressVC)
}

class AddAddressVC: UIViewController {

    @IBOutlet var searchBar: UISearchBar?

    @IBOutlet weak var resultsTableView: UITableView!{
        didSet {
            resultsTableView.tableFooterView = UIView()
        }
    }
    
    fileprivate let searchCompleter = MKLocalSearchCompleter()
    fileprivate var searchResults: [MKLocalSearchCompletion] = []
    
    @objc var delegate: AddAddressVCDelegate?
    
    let geocoder = CLGeocoder()

    //MARK: Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        searchCompleter.delegate = self
        self.title = "Enter location"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_x"), style: .plain, target: self, action: #selector(close))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .lightGray
        self.searchBar?.becomeFirstResponder()
    }

    @objc func close() {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: - Search Bar Delegate

extension AddAddressVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCompleter.queryFragment = searchText
    }
}

// MARK: - MKLocalSearchCompleter Delegate

extension AddAddressVC: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        resultsTableView.reloadData()
    }
}

// MARK: - UITableView Data Source

extension AddAddressVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        
        cell.textLabel?.text = searchResult.title
        cell.detailTextLabel?.text = searchResult.subtitle
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        cell.detailTextLabel?.numberOfLines = 0
        
        return cell
    }
}

// MARK: - UITableView Delegate

extension AddAddressVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedResult = searchResults[indexPath.row]
        locationFromResult(selectedResult) { (coordinate) in
            let lat  = coordinate?.latitude
            let lon = coordinate?.longitude
            let location = CLLocation(latitude:lat!, longitude: lon!)
            self.delegate?.onAddressSelected(location: location, address: selectedResult.title, on: self)
        }        
    }
}

// MARK: - DZNEmptyDataSet Source

extension AddAddressVC: EmptyDataSource, EmptyDelegate {
    func titleForEmpty(in view: UIView) -> NSAttributedString? {
        return NSAttributedString(string: "No Results Found", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0), NSAttributedString.Key.foregroundColor: UIColor.black])
    }

    func verticalOffsetForEmpty(in view: UIView) -> CGFloat {
        return -view.bounds.height/3
    }
}



extension AddAddressVC {
    func locationFromResult(_ result: MKLocalSearchCompletion, completion: @escaping (_ coordinate: CLLocationCoordinate2D?) -> Void) {
        
        let request = MKLocalSearch.Request(completion: result)
        let search = MKLocalSearch(request: request)
        
        search.start { (response, _) in
            completion(response?.mapItems.first?.placemark.location?.coordinate)
        }
    }
}
