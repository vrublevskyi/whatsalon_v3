//
//  IBCore.m
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "WSCore.h"
#import "Reachability.h"
#import "ProgressHUD.h"
#import "ServiceCategory.h"
#import "CategorySingleton.h"
#import <UIImage+ImageEffects.h>
#import "UIImage+Resize.h"
#import "MRProgress.h"
#import "StyleCategoryListModel.h"
#import "User.h"
#import "UIView+AlertCompatibility.h"
#import "PhorestCategories.h"
#import "NSString+VersionNumber.h"
#import "Salon.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "Booking.h"
#import "UIView+AlertCompatibility.h"




@implementation WSCore


static BookingType bkType = BookingTypeNone;
static LoginType logType = LoginTypeNone;
static TabType tabType = TabTypeDefault;

+(void)setTabType: (TabType) tt{
    tabType = tt;
}
+(TabType) fetchTabType{
    return tabType;
}
+(void)resetTabType{
    tabType=TabTypeDefault;
}

+(void)logTabType{

        NSString *result = nil;
        
        switch(tabType) {
            case TabTypeDefault:
                result = @"Discovery Screen";
                break;
            case TabTypeSearch:
                result = @"Search Screen";
                break;
            case TabTypeFavourites:
                result = @"Favourites Screen";
                break;
            case TabTypeMyBookings:
                result = @"Bookings Screen";
                break;
            default:
                result = @"unknown Screen";
        }
        
    
    //NSLog(@" ******* \n\nThe current Tab is %@ \n\n\n **********",result);
}
+(NSString *) shortDateStringWithUnixTimestamp: (double) timestamp{
    
    NSTimeInterval _interval=timestamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"dd  MMM, yyyy"];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}
+(void)setBookingType: (BookingType) bk{
    bkType=bk;
}
+(BookingType) fetchBookingType{
    return bkType;
}
+(void)resetBookingType{
    //NSLog(@"Booking type reset");
    bkType= BookingTypeNone;
}

+(void)setLoginType: (LoginType) lt{
    logType = lt;
}
+(LoginType) fetchLoginType{
    return logType;
}
+(void)resetLoginType{
    logType= LoginTypeNone;
}

#pragma mark - Customisation of views



+ (UIView *)blurredViewForNav:(UIView *) view below: (UIView *) belowView{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = view.bounds;
        view.backgroundColor=[UIColor clearColor];
        [view insertSubview:visualEffectView belowSubview:belowView];
        
    }
    
    else{
        
        view.backgroundColor=[UIColor colorWithWhite:1.0 alpha:0.9];
        
    }
    
    
    return view;
}

+(UIView *)addDarkBlurToView: (UIView *) viewToBlur{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = viewToBlur.bounds;
        viewToBlur.backgroundColor=[UIColor clearColor];
        [viewToBlur insertSubview:visualEffectView atIndex:0];
        
    }
    
    else{
        
        viewToBlur.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.9];
        
    }
    
    return viewToBlur;
}

+(UIView *)addBlurToViewForPicker: (UIView*) viewForPicker{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = viewForPicker.bounds;
        viewForPicker.backgroundColor=[UIColor clearColor];
        [viewForPicker addSubview:visualEffectView];
        
    }
    
    else{
        
        viewForPicker.backgroundColor=[UIColor colorWithWhite:1.0 alpha:0.9];
        
    }
    
    
    return viewForPicker;
}

+(UIView *)addExtraLightBlurAsSubviewToView: (UIView *)viewToBlur{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = viewToBlur.bounds;
        
        [viewToBlur addSubview:visualEffectView];
    }
    else{
        UIView * blackView = [[UIView alloc] initWithFrame:viewToBlur.frame];
        blackView.backgroundColor=[UIColor blackColor];
        blackView.alpha=0.6;
        [viewToBlur addSubview:blackView];
    }
    
    return viewToBlur;
    
}

+(UIView *)addLightBlurAsSubviewToView: (UIView *)viewToBlur{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = viewToBlur.bounds;
        
        [viewToBlur addSubview:visualEffectView];
    }
    else{
        UIView * blackView = [[UIView alloc] initWithFrame:viewToBlur.frame];
        blackView.backgroundColor=[UIColor blackColor];
        blackView.alpha=0.6;
        [viewToBlur addSubview:blackView];
    }
    
    return viewToBlur;
    
}

+(UIView *)addDarkBlurAsSubviewToView: (UIView *)viewToBlur{
    
    if (IS_IOS_8_OR_LATER) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = viewToBlur.bounds;
        
        [viewToBlur addSubview:visualEffectView];
    }
    else{
        UIView * blackView = [[UIView alloc] initWithFrame:viewToBlur.frame];
        blackView.backgroundColor=[UIColor blackColor];
        blackView.alpha=0.8;
        [viewToBlur addSubview:blackView];
    }
    
    return viewToBlur;
    
}

#pragma mark - UINavigationController Custom Methods
+ (void) returnNavBar:(UINavigationController *)navController
{
    [navController setNavigationBarHidden:NO animated:NO];
}


#pragma mark - Adding lines to views


+(void)addMiddleLineOnView: (UIView *) view WithColor: (UIColor *) color AndHeight: (CGFloat) h{
    UIView * middleView = [[UIView alloc] initWithFrame:CGRectMake(0, (CGRectGetHeight(view.frame)/2.0f) -h, CGRectGetWidth(view.frame), h)];
    middleView.backgroundColor=color;
    [view addSubview:middleView];
}
+ (void)addBottomLine: (UIView *) view : (UIColor *) color
{
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, view.frame.size.height -0.5, view.frame.size.width, 0.5)];
    bottomLineView.backgroundColor = color;
    [view addSubview:bottomLineView];
}
+ (void)addBottomLine: (UIView *) view WithColor: (UIColor *) color AndWithLineWidthHeight: (CGFloat)lwh
{
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, view.frame.size.height -lwh, view.frame.size.width, lwh)];
    bottomLineView.backgroundColor = color;
    [view addSubview:bottomLineView];
}

+(void)addBottomIndentedLine: (UIView *) view : (UIColor *) color
{
    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(15, view.frame.size.height -0.5, view.frame.size.width-15, 0.5)];
    bottomLineView.backgroundColor = color;
    [view addSubview:bottomLineView];
}

+(void)addRightLineToView: (UIView *) view withWidth: (float)size{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(view.frame.size.width-size, 0, size, view.frame.size.height)];
    lineView.backgroundColor = kCellLinesColour;
    [view addSubview:lineView];
}

+(void)addRightLineToView: (UIView *) view withWidth: (float)size AndColor:(UIColor *)color{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(view.frame.size.width-size, 0, size, view.frame.size.height)];
    lineView.backgroundColor = color;
    [view addSubview:lineView];
}


+ (void)addTopLine : (UIView *) view : (UIColor *) color : (float) lineHeight
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, lineHeight)];
    lineView.backgroundColor = color;
    
    [view addSubview:lineView];
}

+ (void)addTopLine : (UIView *) view : (UIColor *) color : (float) lineHeight WithYvalue : (float) y
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, y, view.frame.size.width, lineHeight)];
    lineView.backgroundColor = color;
    
    [view addSubview:lineView];
}






#pragma mark - Checking for Networks methods
+(BOOL)isNetworkReachable
{
    
    /*
     Check the internet connection
     Author: @Apple
     Uses reachability class supplied by Apple;
     */
    Reachability * reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == ReachableViaWiFi) {
        
        //Connected to WiFi
        return YES;
    }
    else if (networkStatus ==ReachableViaWWAN) {
        //Connected to Wan
        return YES;
        
    }
    else if(networkStatus==NotReachable){
        
        return NO;
    }
    else{
        return NO;
    }
    
    return NO;
}

#pragma mark - Network Loading Methods

+ (void)showNetworkLoading
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    [ProgressHUD show:@"Please wait..."];
    
    //[MRProgressOverlayView showOverlayAddedTo: [[UIApplication sharedApplication] keyWindow] title:@"Loading..." mode:MRProgressOverlayViewModeIndeterminateSmall animated:YES];
   
}

+ (void)dismissNetworkLoading
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    [ProgressHUD dismiss];
    //[MRProgressOverlayView dismissOverlayForView:[[UIApplication sharedApplication] keyWindow]animated:YES];
}

+(void)showNetworkLoadingOnView:(UIView *)view{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    [MRProgressOverlayView showOverlayAddedTo:view title:@"Please wait..." mode:MRProgressOverlayViewModeIndeterminateSmall animated:YES];
}

+(void)dismissNetworkLoadingOnView:(UIView *)view{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    [MRProgressOverlayView dismissOverlayForView:view animated:YES];
   
    

    
}

+(void)showNetworkLoadingOnView:(UIView *)view WithTitle: (NSString *) string{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    [MRProgressOverlayView showOverlayAddedTo:view title:string mode:MRProgressOverlayViewModeIndeterminateSmall animated:YES];
}

#pragma mark - Show Alerts
+(void)showNetworkErrorAlert{
    
    [UIView showSimpleAlertWithTitle:@"Network Problem" message:@"It looks like you're not connected to the internet." cancelButtonTitle:@"OK"];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

+(void)showServerErrorAlert{
    
    [UIView showSimpleAlertWithTitle:@"Connection Problem" message:@"An Error occurred when trying to connect to the WhatSalon server. Please try again later." cancelButtonTitle: @"OK"];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

+(void)showLoginSignUpAlert{
    
    
    [UIView showSimpleAlertWithTitle:@"Login or Sign up" message:@"Please Login or Sign up in order to use this feature" cancelButtonTitle:@"OK"];
}


+ (void)errorAlert:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [WSCore dismissNetworkLoading];
        
        [UIView showSimpleAlertWithTitle:@"Error" message:[NSString stringWithFormat:@"Error: %@",[error userInfo] ] cancelButtonTitle:@"OK"];
        
    });
   
}

#pragma mark - Network Calls

+(void)getUsersBalance{
    
   
    if ([[User getInstance] fetchAccessToken]) {
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_User_Balance_URL]];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:90];
        NSString * param = [[NSString alloc] initWithString:[NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]]];
    
        if ([[User getInstance] fetchKey].length!=0) {
            param = [param stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
        }
         
        [request setHTTPMethod:@"POST"];
        [ request setHTTPBody:[param dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (data) {
                NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *) response;
                NSDictionary * dataDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                      
                        
                        if ([dataDictionary[kSuccessIndex] isEqualToString:@"true"]) {
                           
                            [[User getInstance] updateUsersBalance:dataDictionary[kMessageIndex]];
                           
                        }
                        break;
                        
                    default:
                        break;
                }

            }
            
        }];
        
        if ([WSCore isNetworkReachable]) {
            [task resume];
        }
        
    }
    
    

}

+(void)downloadCategoriesList{
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSalon_Categories_URL]];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *) response;
        NSDictionary * dataDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        if (data &&dataDictionary!=nil) {
            
            
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                {
                    NSMutableDictionary * messageDict = dataDictionary[kMessageIndex];
                    
                    NSMutableDictionary * existingDict = [[NSUserDefaults standardUserDefaults] objectForKey:kCatDictionaryKey];
                    if (![messageDict isEqual: existingDict]) {
                     
                        
                        
                        [[NSUserDefaults standardUserDefaults] setObject:messageDict forKey:kCatDictionaryKey];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                    }
                    CategorySingleton * catSingletone = [CategorySingleton getInstance];
                    [catSingletone setCategories];
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // [WSCore dismissNetworkLoading];
                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    });
                    
                }
                    break;
                    
                default:
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                        
                        
                        
                    });
                    
                    break;
            }
            
        }
        else{
            [WSCore errorAlert:error];
        }
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        //[WSCore showNetworkLoading];
        [task resume];
    }
    else{
        
        //[WSCore showNetworkErrorAlert];
        //NSMutableArray * completeArray = kServicesArray;
        //CategorySingleton * catSingletone = [CategorySingleton getInstance];
        //[catSingletone setCategories:completeArray];
    }
    
    
}

+(void)downloadStyleLists{
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kStyle_Category_URL]];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        if (data && dataDict!=nil) {
            switch (httpResp.statusCode) {
                case kOKStatusCode:{
                    
                    
                    
                    NSMutableDictionary * messageDict = dataDict[@"message"];
                    NSMutableDictionary *existingStyles =[[NSUserDefaults standardUserDefaults] objectForKey:kStyleListKey];
                    if (![messageDict isEqual:existingStyles]) {
                      
                       
                        [[NSUserDefaults standardUserDefaults] setObject:messageDict forKey:kStyleListKey];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                    }
                    
                    [[StyleCategoryListModel getInstance] setStyles];
                    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                }
                    break;
                    
                default:
                    break;
            }
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        [task resume];
    }
    
}

+(void)cancelSearchWithSearchId : (NSString *) s_id{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kCancel_Tier2_Search_URL]];
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&search_id=%@",s_id]];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
     
        if (data && dataDict!=nil) {
            switch (httpResp.statusCode) {
                case kOKStatusCode:
                    if ([dataDict[@"success"] isEqualToString:@"true"]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [WSCore dismissNetworkLoading];
                            
                            [UIView showSimpleAlertWithTitle:@"Canceled Search" message:@"" cancelButtonTitle:@"OK"];
                            
                        });
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [WSCore dismissNetworkLoading];
                            
                            [UIView showSimpleAlertWithTitle:dataDict[@"message"] message:@"" cancelButtonTitle:@"OK"];
                            
                        });
                    }
                    break;
                    
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [WSCore dismissNetworkLoading];
                        [WSCore showServerErrorAlert];
                    });
                    break;
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [WSCore dismissNetworkLoading];
                [WSCore showServerErrorAlert];
            });
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [WSCore showNetworkLoading];
        [task resume];
    }
    else{
        [WSCore showNetworkErrorAlert];
    }
}

+(void)downloadPhorestCategoriesList{
    
    [[PhorestCategories getInstance] downloadCategories];
    
}

+(void)checkAppVersion{
    
    NSString* actualVersion = kVersionNumber;
    actualVersion = [actualVersion shortenedVersionNumberString];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_APP_version_URL]];
    NSString * params = @"platform=ios";
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    NSURLSession * sharedSession = [NSURLSession sharedSession];
    NSURLSessionDataTask * dataTask = [sharedSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
        
        if (!error) {
            switch (httpResp.statusCode) {
                case kOKStatusCode:{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString * requiredVersion = dataDict[@"message"];
                        requiredVersion =[requiredVersion shortenedVersionNumberString];
                        NSLog(@"V: \n\n %@",dataDict);
                        NSLog(@"Current %@ Required %@",actualVersion,requiredVersion);
                        if ([actualVersion integerValue] != [requiredVersion integerValue]) {
                            
                            //[UIView showSimpleAlertWithTitle:@"Update Available" message:[NSString stringWithFormat:@"Version %@ is now available for WhatSalon. For the best experience please update.",requiredVersion] cancelButtonTitle:@"OK"];
                        }
                        else{
                         
                        }
                    });
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
        else{
           
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [dataTask resume];
    }
    else{
      
    }
}


#pragma mark - Configurations
+(NSURLSessionConfiguration *)sessionConfigWithTimeOut{
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.allowsCellularAccess = YES;
    
    sessionConfig.timeoutIntervalForRequest =60.f;
    sessionConfig.timeoutIntervalForResource =60.f;
    
    return sessionConfig;
}

+(NSDate *)getConfiguredStartDate{
    NSDate* date = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
    NSDateComponents *minuteComponents = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:date];
    
    int iMinutes = (int)minuteComponents.minute;
    if ((iMinutes > 0) && (iMinutes <= 15))
        iMinutes = 15;
    if ((iMinutes > 15) && (iMinutes <= 30))
        iMinutes = 30;
    if ((iMinutes > 30) && (iMinutes <= 45))
        iMinutes = 45;
    if ((iMinutes > 45) && (iMinutes < 60))
    {
        iMinutes = 0;
        minuteComponents.hour++;
    }
    
    minuteComponents.minute = iMinutes;
    NSDate* gDate = [gregorian dateFromComponents:minuteComponents];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH:mm"];
    NSString* sTime = [df stringFromDate:gDate];
    NSDate * dateFromString = [df dateFromString:sTime];
   
    
    return dateFromString;
}

#pragma mark - BOOLs
+(BOOL)isDeviceIPhone{
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"]){
        
        return YES;
    }
    else{
        return NO;
    }
    
}

+(BOOL)stringIsEmpty: (NSString *) string{
    if (string.length == 0|| [string isEqual: [NSNull null]] || string==nil || ![string isKindOfClass:[NSString class]]||string.length == 0 ){
     
        return YES;
    }
    else{
        return NO;
    }
    
}

#pragma mark - UIImage methods

+(UIImage *)facebookImageView: (UIImageView *)iv{
    
    return iv.image;
}

+(UIImage *) blurMenuImage{
    
    NSLog(@"Blur Menu Image");
    UIImage * img;
    
    
    if(IS_iPHONE5_or_Above){
        
        
       
        if ([[User getInstance] isUserLoggedIn]) {
            
           // NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",kImageDataKey,accessToken]];;
            NSData * imageData; /*= [[User getInstance] fetchUserImagePath:[[User getInstance] fetchAccessToken]] ;*/
           
            if (imageData){
                img=[UIImage imageWithData:imageData];
                
            }
            else{
                img=[UIImage imageNamed:kProfile_image];
            }
            
        }
        else{
            img=[UIImage imageNamed:kProfile_image];
            
        }
        
        
        if (!IS_IOS_8_OR_LATER) {
            NSLog(@"Apply dark effecet");
            //img = [img applyDarkEffect];
            NSData * imageData ;/*= [[User getInstance] fetchUserImagePath:[[User getInstance] fetchAccessToken]] ;*/
            NSLog(@"Image Data %@",imageData);
            if (imageData){
                img=[UIImage imageWithData:imageData];
                
            }


        }
        
        return img;
        
    }
    else{
        NSLog(@"Iphone 4");
        //iPhone 4
        //img = [UIImage imageNamed:@"blue_menu_default"];
        if ([[User getInstance] isUserLoggedIn]) {
            
            // NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",kImageDataKey,accessToken]];;
            NSData * imageData; /* = [[User getInstance] fetchUserImagePath:[[User getInstance] fetchAccessToken]] ;*/
            NSLog(@"Image data %@",imageData);
            
            if (imageData){
                img=[UIImage imageWithData:imageData];
                
            }
            else{
                //img=[UIImage imageNamed:kProfile_image];
            }
            
        }
        else{
            img=[UIImage imageNamed:kProfile_image];
            
        }

        
    }
    
    return img;
}

+(UIImage *)resizeImage: (UIImage *) img{
    
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = 320.0/480.0;
    
    if(imgRatio!=maxRatio){
        if(imgRatio < maxRatio){
            imgRatio = 480.0 / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = 480.0;
        }
        else{
            imgRatio = 320.0 / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = 320.0;
        }
    }
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(UIImage *)drawCustomUserPin:(UIImage*)userImage{
    
    
    
    UIImage *bluePinImage = [UIImage imageNamed:@"pinWithHole"]; //background image
    UIImage *image       = userImage; //foreground image
    
    CGSize newSize = CGSizeMake(bluePinImage.size.width/2.5, bluePinImage.size.height/2.5);
    UIGraphicsBeginImageContextWithOptions(newSize, // context size
                                           NO,      // opaque?
                                           0);
    [image drawInRect:CGRectMake(5,5,22,22)];
    [bluePinImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];// Use existing opacity as is
    
    
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}


#pragma mark - CGSize
+ (CGSize)screenSize {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    if ((NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        return CGSizeMake(screenSize.height, screenSize.width);
    } else {
        return screenSize;
    }
}

#pragma mark - Day Suffix
+ (NSString *)daySuffixForDate:(NSDate *)date {
    NSInteger day_of_month = [[[NSCalendar currentCalendar] components:NSDayCalendarUnit fromDate:date] day];
    switch (day_of_month) {
        case 1:
        case 21:
        case 31: return @"st";
        case 2:
        case 22: return @"nd";
        case 3:
        case 23: return @"rd";
        default: return @"th";
    }
}

+(void)nonTransparentNavigationBarOnView: (UIViewController *) cont{
    
    //show navigation bar - hidden in TabBarController
    [cont.navigationController setNavigationBarHidden:NO animated:NO];
    
    //ensure navigation bar is visible - it was made invisible in `BrowseViewController`
    [cont.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    cont.navigationController.navigationBar.tintColor=kWhatSalonBlue;
    [cont.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName]];
    cont.navigationController.navigationBar.barTintColor=nil;
    //set back button color
    
}
+(void)transparentNavigationBarOnView: (UIViewController *) cont{
    
    [cont.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    cont.navigationController.navigationBar.shadowImage = [UIImage new];
    cont.navigationController.navigationBar.translucent = YES;
    cont.navigationController.view.backgroundColor = [UIColor clearColor];
    
    
}

+(void)transparentNavigationBar: (UINavigationBar *)navBar{
    [navBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    navBar.shadowImage = [UIImage new];
    navBar.translucent = YES;
    navBar.backgroundColor = [UIColor clearColor];

    
}

+(void)flatNavBarOnView: (UIViewController *) vc{
    
    [vc.navigationController.navigationBar setBackgroundImage:[UIImage new]forBarMetrics:UIBarMetricsDefault];
    vc.navigationController.navigationBar.shadowImage = [UIImage new];
    //vc.navigationController.navigationBar.translucent = YES;
    vc.navigationController.view.backgroundColor = [UIColor clearColor];
    vc.navigationController.navigationBar.tintColor=kWhatSalonSubTextColor;
    [vc.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName]];
    vc.navigationController.navigationBar.barTintColor=nil;

    UIView * whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, vc.view.frame.size.width, kStatusBarHeight + vc.navigationController.navigationBar.frame.size.height)];
    whiteView.backgroundColor=[UIColor whiteColor];
    [vc.view insertSubview:whiteView belowSubview:vc.navigationController.navigationBar];
}
+(NSString*) fetchDeviceName{
    
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString * code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    return code;
}

+(NSInteger)numericDayOfWeek{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSInteger weekday = [comps weekday];
    NSLog(@"Weekday %ld",(long)weekday);
    
    return weekday;
}

+(void)statusBarColor:(enum StatusBarColor) color{
    
    
    if(color
       ==StatusBarColorBlack) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        //isBlack = NO;
        
    }else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
       // black = YES;
    }
    
    
}

+(void)hideStatusBar: (BOOL) hide{
    [[UIApplication sharedApplication] setStatusBarHidden:hide withAnimation:UIStatusBarAnimationSlide];
    
   
}


+(void)makePhoneCallWithNumber: (NSString *) number withViewController: (UIViewController *)view AndWithSalonName: (NSString *)salonName{
    NSLog(@"number %@",number);
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    
    if (IS_IOS_8_OR_LATER) {
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Phone"
                                              message:[NSString stringWithFormat:@"You are about to call %@. Continue?",salonName]
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"No"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Yes"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                       NSString *phoneNumber = [@"tel://" stringByAppendingString:number];
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                                   }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [view presentViewController:alertController animated:YES completion:nil];
        
    }else{
        NSString *phoneNumber = [@"telprompt://" stringByAppendingString:number];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }

}
/*
+(BOOL)isLiveEnvironment{
    
    
     return [[NSUserDefaults standardUserDefaults] boolForKey:@"isLive"];
}
 */
+(void)setLiveEnvironment: (BOOL) yesNo{
    
    [[NSUserDefaults standardUserDefaults] setBool:yesNo forKey:@"isLive"];
}

/*
+(NSMutableArray *)getDummyTier1Salons{
    Salon * salon1 = [[Salon alloc] initWithSalonID:@"101"];
    salon1.salon_name =@"Madison Lane";
    salon1.salon_description=@"Your Little Hair & Beauty Empire. Your very own team of hair and beauty experts with everything you need to look fabulous all under one roof. From waxing, tanning, nails & facials to hair extensions, eyelash extensions, hair styling and make-up.";
    salon1.salon_phone=@"";
    salon1.salon_address_1=@"3 Winthrop Arcade";
    salon1.salon_address_2=@"Winthrop Street";
    salon1.salon_address_3=@"Cork";
    salon1.city_name=@"Cork City";
    //add image
    salon1.salon_image=@"http://sms.whatsalon.com/salon_images/16/All_Dolled_Up_1.JPG";
    salon1.salon_type=@"Tier1";
    salon1.ratings=1;
    salon1.reviews=10;
    salon1.is_favourite=NO;
    salon1.salon_lat=51.8981959;
    salon1.salon_long=-8.4707587;
    salon1.open=YES;
    salon1.distance=0.5;
    
    Salon * salon2 = [[Salon alloc] initWithSalonID:@"102"];
    salon2.salon_name =@"Adore";
    salon2.salon_description=@"Re-invent yourself in Adore's state-of-the-art hair salon.  Our hair, beauty and intense pulsed light consultants offer personalized consultations to ensure that each client receives the highest possible standard of service and also achieves the desired result.";
    salon2.salon_phone=@"";
    salon2.salon_address_1=@"127 Oliver Plukett St";
    salon2.salon_address_2=@"";
    salon2.salon_address_3=@"Cork";
    salon2.city_name=@"Cork City";
    //add image
    salon2.salon_image=@"http://sms.whatsalon.com/salon_images/15/City_Wax_6.jpg";
    salon2.salon_type=@"Tier1";
    salon2.ratings=1;
    salon2.reviews=10;
    salon2.is_favourite=NO;
    salon2.salon_lat=51.8984613;
    salon2.salon_long=-8.4685048;
    salon2.open=YES;
    salon2.distance=0.8;

    Salon * salon3 = [[Salon alloc] initWithSalonID:@"103"];
    salon3.salon_name =@"Therapie Cork";
    salon3.salon_description=@"Your Little Hair & Beauty Empire. Your very own team of hair and beauty experts with everything you need to look fabulous all under one roof. From waxing, tanning, nails & facials to hair extensions, eyelash extensions, hair styling and make-up.";
    salon3.salon_phone=@"";
    salon3.salon_address_1=@"17B Opera Lane";
    salon3.salon_address_2=@"";
    salon3.salon_address_3=@"Cork";
    salon3.city_name=@"Cork City";
    //add image
    salon3.salon_image=@"http://sms.whatsalon.com/salon_images/16/All_Dolled_Up_1.JPG";
    salon3.salon_type=@"Tier1";
    salon3.ratings=1;
    salon3.reviews=10;
    salon3.is_favourite=NO;
    salon3.salon_lat=51.899019;
    salon3.salon_long=-8.472540;
    salon3.open=YES;
    salon3.distance=1.0;

    Salon * salon4 = [[Salon alloc] initWithSalonID:@"104"];
    salon4.salon_name =@"Darcy's Hairdressing";
    salon4.salon_description=@"Your Little Hair & Beauty Empire. Your very own team of hair and beauty experts with everything you need to look fabulous all under one roof. From waxing, tanning, nails & facials to hair extensions, eyelash extensions, hair styling and make-up.";
    salon4.salon_phone=@"";
    salon4.salon_address_1=@"27/28 Paul Street";
    salon4.salon_address_2=@"";
    salon4.salon_address_3=@"Cork";
    salon4.city_name=@"Cork City";
    //add image
    salon4.salon_image=@"http://sms.whatsalon.com/salon_images/16/All_Dolled_Up_1.JPG";
    salon4.salon_type=@"Tier1";
    salon4.ratings=1;
    salon4.reviews=10;
    salon4.is_favourite=NO;
    salon4.salon_lat=51.8990234;
    salon4.salon_long=-8.4757111;
    salon4.open=YES;
    salon4.distance=1.5;

    Salon * salon5 = [[Salon alloc] initWithSalonID:@"105"];
    salon5.salon_name =@"All Dolled Up";
    salon5.salon_description=@"We are seriously healthy hair obsessed. Experts in Colour & Hair Condition. Proud to work with leading brands Redken & Morrocan Oil,  we are Passionate about change.";
    salon5.salon_phone=@"";
    salon5.salon_address_1=@"The Old Square";
    salon5.salon_address_2=@"";
    salon5.salon_address_3=@"Ballincollig";
    salon5.city_name=@"Cork City";
    //add image
    salon5.salon_image=@"http://sms.whatsalon.com/salon_images/16/All_Dolled_Up_1.JPG";
    salon5.salon_type=@"Tier1";
    salon5.ratings=1;
    salon5.reviews=10;
    salon5.is_favourite=NO;
    salon5.salon_lat=51.887354;
    salon5.salon_long=-8.5998696;
    salon5.open=YES;
    salon5.distance=9.8;

    Salon * salon6 = [[Salon alloc] initWithSalonID:@"106"];
    salon6.salon_name =@"The Style house";
    salon6.salon_description=@"Great hair never goes out of style! The Style House are experts in everyday beautiful hair care for women and men. We also specialise in occasion hair and have a call out service for weddings etc.";
    salon6.salon_phone=@"";
    salon6.salon_address_1=@"Tower Cross";
    salon6.salon_address_2=@"Tower";
    salon6.salon_address_3=@"Blarney";
    salon6.city_name=@"Cork City";
    //add image
    salon6.salon_image=@"http://sms.whatsalon.com/salon_images/16/All_Dolled_Up_1.JPG";
    salon6.salon_type=@"Tier1";
    salon6.ratings=1;
    salon6.reviews=10;
    salon6.is_favourite=NO;
    salon6.salon_lat=51.92515;
    salon6.salon_long=-8.6075684;
    salon6.open=YES;
    salon6.distance=14.2;
    
    NSMutableArray * array = [[NSMutableArray alloc] initWithObjects:salon1,salon2,salon3,salon4,salon5,salon6, nil];
    
    return array;

    
    
}
*/


+(NSMutableArray *)getDummySalonGallery{
    
    NSMutableArray * images = [[NSMutableArray alloc] initWithObjects:[UIImage imageNamed:@"All_Dolled_Up_1.JPG"],[UIImage imageNamed:@"All_Dolled_Up_2.JPG"], [UIImage imageNamed:@"All_Dolled_Up_3.JPG"],[UIImage imageNamed:@"All_Dolled_Up_4.JPG"], [UIImage imageNamed:@"All_Dolled_Up_5.JPG"], [UIImage imageNamed:@"All_Dolled_Up_6.JPG"], [UIImage imageNamed:@"All_Dolled_Up_7.JPG"], [UIImage imageNamed:@"All_Dolled_Up_8.JPG"],[UIImage imageNamed:@"All_Dolled_Up_9.JPG"],[UIImage imageNamed:@"All_Dolled_Up_10.JPG"], nil];
    
    return images;
    
}

+(NSMutableArray *)getDummyServices{
    
    NSMutableArray * services = [[NSMutableArray alloc] initWithObjects:@"Blowdry (short hair)",@"Blowdry (medium hair)",@"Blowdry (long hair)",@"Upstyle",@"GHD Curls",@"Ladies Cut (Junior Stylist)",@"Ladies Cut (Senior Stylist)",@"Gents Cut (Junior Stylist)",@"Gents Cut (Senior Stylist)", nil];
    
    return services;
}

+(NSMutableArray *)getDummyStylist{
    
    NSMutableArray * stylist = [[NSMutableArray alloc] initWithObjects:@"Next Available Stylist",@"Cliona O'Hanlon",@"Ciara Dineen",@"Liz Connolly",@"Graham O'Reilly",@"Miriam O'Brien", nil];
    return stylist;
    
}

+(NSMutableArray *)getDummyTimes{
    
    NSMutableArray * times = [[NSMutableArray alloc] initWithObjects:@"1.30 p.m",@"2.30 pm",@"3.45 pm", nil];
    return times;
}

+(BOOL)isDummyMode{
    
    return NO;
}

+(UIImage *)getStarRatingImage: (double) rating{
    
    UIImage * image;
    NSLog(@"rating %f int rating %d",rating,(int)rating);
    switch ((int)rating) {
        case 0:
            image=[UIImage imageNamed:@"white_no_star_rating"];
            break;
        case 1:
            image=[UIImage imageNamed:@"white_one_star_rating"];
            break;
        case 2:
            image = [UIImage imageNamed:@"white_two_star_rating"];
            break;
        case 3:
            image=[UIImage imageNamed:@"white_three_star_rating"];
            break;
        case 4:
            image = [UIImage imageNamed:@"white_four_star_rating"];
            break;
        case 5:
            image = [UIImage imageNamed:@"white_five_star_rating"];
            break;
        default:
            image = [UIImage imageNamed:@"white_four_star_rating"];
            break;
    }
    
    return image;

}

+(NSString *)base64EncodeWithString: (NSString *)string{
   
    NSData *nsdata = [string
                      dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    return base64Encoded;
}

+(UIView *)addNotificationBubbleToView: (UIView *) host WithNumber: (NSString *) num WithFrame: (CGRect) frame{
    
    UILabel * notifLabel = [[UILabel alloc] initWithFrame:frame];
    notifLabel.text = num;
    notifLabel.font = [UIFont systemFontOfSize:14.0f];
    notifLabel.textColor = [UIColor whiteColor];
    notifLabel.textAlignment=NSTextAlignmentCenter;
    notifLabel.adjustsFontSizeToFitWidth=YES;
    notifLabel.backgroundColor=kWhatSalonBlue;
    notifLabel.layer.cornerRadius=roundf(notifLabel.frame.size.width/2.0f);
    notifLabel.layer.masksToBounds=YES;
    
    
    [host addSubview:notifLabel];
    
    return notifLabel;
}

+(NSDictionary *)getCountryCodes{
    // Country code
    NSDictionary *dictCodes = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"93",@"AF",@"355",@"AL",@"213",@"DZ",@"1",@"AS",
                               @"376",@"AD",@"244",@"AO",@"1",@"AI",@"1",@"AG",
                               @"54",@"AR",@"374",@"AM",@"297",@"AW",@"61",@"AU",
                               @"43",@"AT",@"994",@"AZ",@"1",@"BS",@"973",@"BH",
                               @"880",@"BD",@"1",@"BB",@"375",@"BY",@"32",@"BE",
                               @"501",@"BZ",@"229",@"BJ",@"1",@"BM",@"975",@"BT",
                               @"387",@"BA",@"267",@"BW",@"55",@"BR",@"246",@"IO",
                               @"359",@"BG",@"226",@"BF",@"257",@"BI",@"855",@"KH",
                               @"237",@"CM",@"1",@"CA",@"238",@"CV",@"345",@"KY",
                               @"236",@"CF",@"235",@"TD",@"56",@"CL",@"86",@"CN",
                               @"61",@"CX",@"57",@"CO",@"269",@"KM",@"242",@"CG",
                               @"682",@"CK",@"506",@"CR",@"385",@"HR",@"53",@"CU",
                               @"537",@"CY",@"420",@"CZ",@"45",@"DK",@"253",@"DJ",
                               @"1",@"DM",@"1",@"DO",@"593",@"EC",@"20",@"EG",
                               @"503",@"SV",@"240",@"GQ",@"291",@"ER",@"372",@"EE",
                               @"251",@"ET",@"298",@"FO",@"679",@"FJ",@"358",@"FI",
                               @"33",@"FR",@"594",@"GF",@"689",@"PF",@"241",@"GA",
                               @"220",@"GM",@"995",@"GE",@"49",@"DE",@"233",@"GH",
                               @"350",@"GI",@"30",@"GR",@"299",@"GL",@"1",@"GD",
                               @"590",@"GP",@"1",@"GU",@"502",@"GT",@"224",@"GN",
                               @"245",@"GW",@"595",@"GY",@"509",@"HT",@"504",@"HN",
                               @"36",@"HU",@"354",@"IS",@"91",@"IN",@"62",@"ID",
                               @"964",@"IQ",@"353",@"IE",@"972",@"IL",@"39",@"IT",
                               @"1",@"JM",@"81",@"JP",@"962",@"JO",@"77",@"KZ",
                               @"254",@"KE",@"686",@"KI",@"965",@"KW",@"996",@"KG",
                               @"371",@"LV",@"961",@"LB",@"266",@"LS",@"231",@"LR",
                               @"423",@"LI",@"370",@"LT",@"352",@"LU",@"261",@"MG",
                               @"265",@"MW",@"60",@"MY",@"960",@"MV",@"223",@"ML",
                               @"356",@"MT",@"692",@"MH",@"596",@"MQ",@"222",@"MR",
                               @"230",@"MU",@"262",@"YT",@"52",@"MX",@"377",@"MC",
                               @"976",@"MN",@"382",@"ME",@"1",@"MS",@"212",@"MA",
                               @"95",@"MM",@"264",@"NA",@"674",@"NR",@"977",@"NP",
                               @"31",@"NL",@"599",@"AN",@"687",@"NC",@"64",@"NZ",
                               @"505",@"NI",@"227",@"NE",@"234",@"NG",@"683",@"NU",
                               @"672",@"NF",@"1",@"MP",@"47",@"NO",@"968",@"OM",
                               @"92",@"PK",@"680",@"PW",@"507",@"PA",@"675",@"PG",
                               @"595",@"PY",@"51",@"PE",@"63",@"PH",@"48",@"PL",
                               @"351",@"PT",@"1",@"PR",@"974",@"QA",@"40",@"RO",
                               @"250",@"RW",@"685",@"WS",@"378",@"SM",@"966",@"SA",
                               @"221",@"SN",@"381",@"RS",@"248",@"SC",@"232",@"SL",
                               @"65",@"SG",@"421",@"SK",@"386",@"SI",@"677",@"SB",
                               @"27",@"ZA",@"500",@"GS",@"34",@"ES",@"94",@"LK",
                               @"249",@"SD",@"597",@"SR",@"268",@"SZ",@"46",@"SE",
                               @"41",@"CH",@"992",@"TJ",@"66",@"TH",@"228",@"TG",
                               @"690",@"TK",@"676",@"TO",@"1",@"TT",@"216",@"TN",
                               @"90",@"TR",@"993",@"TM",@"1",@"TC",@"688",@"TV",
                               @"256",@"UG",@"380",@"UA",@"971",@"AE",@"44",@"GB",
                               @"1",@"US",@"598",@"UY",@"998",@"UZ",@"678",@"VU",
                               @"681",@"WF",@"967",@"YE",@"260",@"ZM",@"263",@"ZW",
                               @"591",@"BO",@"673",@"BN",@"61",@"CC",@"243",@"CD",
                               @"225",@"CI",@"500",@"FK",@"44",@"GG",@"379",@"VA",
                               @"852",@"HK",@"98",@"IR",@"44",@"IM",@"44",@"JE",
                               @"850",@"KP",@"82",@"KR",@"856",@"LA",@"218",@"LY",
                               @"853",@"MO",@"389",@"MK",@"691",@"FM",@"373",@"MD",
                               @"258",@"MZ",@"970",@"PS",@"872",@"PN",@"262",@"RE",
                               @"7",@"RU",@"590",@"BL",@"290",@"SH",@"1",@"KN",
                               @"1",@"LC",@"590",@"MF",@"508",@"PM",@"1",@"VC",
                               @"239",@"ST",@"252",@"SO",@"47",@"SJ",@"963",@"SY",
                               @"886",@"TW",@"255",@"TZ",@"670",@"TL",@"58",@"VE",
                               @"84",@"VN",@"1",@"VG",@"1",@"VI",@"672",@"AQ",
                               @"358",@"AX",@"47",@"BV",@"599",@"BQ",@"599",@"CW",
                               @"689",@"TF",@"1",@"SX",@"211",@"SS",@"212",@"EH",
                               @"972",@"IL", nil];
    return dictCodes;
}

+(NSString *)getUsersCountryCode{
    
    NSDictionary * dictCodes =[[NSDictionary alloc] initWithDictionary:[WSCore getCountryCodes] ];
    NSLog(@"Dict code %@",dictCodes);
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSLog(@"Country Code %@",countryCode);
    NSString *callingCode = [dictCodes objectForKey:countryCode];
    
   
    return callingCode;
}

+(void)setInvisibleNavBar: (UINavigationBar *)navBar{
    
    //make navigation bar completely transparent
    [navBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    navBar.shadowImage = [UIImage new];
    navBar.translucent = YES;
    navBar.backgroundColor = [UIColor clearColor];
    [WSCore addBottomLine:navBar :[UIColor blackColor]];
    
    
    

}

+(BOOL)showWalkthrough{
   
    
    return ![[NSUserDefaults standardUserDefaults] boolForKey:@"kTutorial1"];
    
}

+(void)walkthroughHasBeenShown{

    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"kTutorial1"];
}

+(NSDictionary *)currentCountryCodes{
    
    NSDictionary * currentCountryCodes = @{@"+353" : @"Ireland",
                                           @"+44":@"United Kingdom",
                                           @"+1":@"United States"};
    return currentCountryCodes;
}

+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day WithHour: (NSInteger)hr AndMin: (NSInteger)min{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    [calendar setTimeZone:[NSTimeZone systemTimeZone]];
    NSDateComponents *components = [[NSDateComponents alloc] init] ;
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    [components setHour:hr];
    [components setMinute:min];
    
    return [calendar dateFromComponents:components];
}

+(BOOL)isApptTimeGreaterThan24Hours: (NSString *) apptDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [[NSDate alloc] init];
    
    date = [dateFormatter dateFromString:apptDate];
    
    NSDate *currentDate = [NSDate date];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSHourCalendarUnit
                                               fromDate:currentDate
                                                 toDate:date
                                                options:0];
    NSLog(@"Number of hours between now and booking date = %ld",(long)components.hour);
    NSLog(@"Appointment date %@",apptDate);
    
    if (components.hour>24) {
        return YES;
    }

    
    return NO;
}
+(NSDate *)oneHourBeforeUsersApointmentDate: (NSDate *) apptTime{
    
    NSDate *currentDate = [NSDate date];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit
                                               fromDate:currentDate
                                                 toDate:apptTime
                                                options:0];
    
    if (components.hour>=1) {
        
        NSTimeInterval secondsPerHour = 60 * 60;
        NSDate *givenDate = apptTime; // what you have already
        NSDate *earlierDate = [givenDate dateByAddingTimeInterval:-secondsPerHour];
        NSLog(@"Earlier date %@",earlierDate);
        return earlierDate;
    
    }
    
        NSTimeInterval secondsPer15mins = 15 * 60;
        NSDate *givenDate = apptTime; // what you have already
        NSDate *earlierDate = [givenDate dateByAddingTimeInterval:-secondsPer15mins];
     NSLog(@"Earlier date %@",earlierDate);
        return earlierDate;
   
}

/*
+(void)setReviewReminderWithTitle: (NSString *) title AndAlertBody:(NSString *) body AndWithBookingDateString: (NSString *) dateString{
    
    NSLog(@"Date String %@",dateString);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [[NSDate alloc] init];
    
    date = [dateFormatter dateFromString:dateString];
    NSLog(@"Date from String %@",[dateFormatter dateFromString:dateString]);
    date = [date dateByAddingTimeInterval:(60*30)];
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate=date;
    localNotification.alertBody = body ;
    
    localNotification.userInfo = @{@"title" : title, @"body" : body,@"type" : @"1"};
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
   // NSLog(@"Reminder Fire date %@",localNotification.fireDate);
    
    //NSLog(@"Reminder Notification--->: %@", localNotification);
    
    
}
*/
+(void)setReviewReminderWithTitle: (NSString *) title AndAlertBody:(NSString *) body AndWithBookingDateString: (NSString *) dateString AndSalon: (Salon *)salon{
    
    NSLog(@"Date String %@",dateString);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [[NSDate alloc] init];
    
    date = [dateFormatter dateFromString:dateString];
    NSLog(@"Date from String %@",[dateFormatter dateFromString:dateString]);
    date = [date dateByAddingTimeInterval:(60*30)];
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate=date;
    localNotification.alertBody = body ;
    
    localNotification.userInfo = @{@"title" : title, @"body" : body,@"type" : @"2",
                                   @"salon_id": salon.salon_id, @"salon_name" : salon.salon_name};
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
   // NSLog(@"Reminder Fire date %@",localNotification.fireDate);
    
    //NSLog(@"New *** Reminder Notification--->: %@", localNotification);
    
    
}

+(NSString *) getShortTimeFromDate : (NSString *) date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate * tempDate = [dateFormatter dateFromString: date];
    
    [dateFormatter setDateFormat:@"h:mm a"];
    return [dateFormatter stringFromDate:tempDate];
}

+(NSString *) getShortTimeFromDate : (NSString *) date WithFormat: (NSString *) format{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate * tempDate = [dateFormatter dateFromString: date];
    
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:tempDate];
}

+(void)setLocalNotificationWithTitle: (NSString *) title AndAlertBody:(NSString *)body AndWithBookingDateString:(NSString *)dateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [[NSDate alloc] init];
    
    date = [dateFormatter dateFromString:dateString];
    NSLog(@"Date from String %@",[dateFormatter dateFromString:dateString]);
    
    
    
    [self setLocalNotificationWithTitle:title  AndAlertBody:body AndWithBookingDate:date];
}


+ (void)scheduleNotification:(NSDate *)date body:(NSString *)body title:(NSString *)title localNotification:(UILocalNotification *)localNotification {
    
//     else if (differenceInMinutes>30){
//     
//     NSLog(@" **** Greater than 30 mins *******");
//     NSTimeInterval secondsPer15mins = 15 * 60;
//     NSDate *givenDate = date; // what you have already
//     NSDate *earlierDate = [givenDate dateByAddingTimeInterval:-secondsPer15mins];
//     NSLog(@"\n\n Less than one hour %@ \n",earlierDate);
//     
//     
//     localNotification.fireDate=earlierDate;
//     localNotification.alertBody = [NSString stringWithFormat:@"%@ %@",body,[self getDeviceShortDateFromString:[NSString stringWithFormat:@"%@",date] WithFormat:@"yyyy-MM-dd HH:mm:ss ZZZZZ"]];
//     
//     
//     }
    
    localNotification.userInfo = @{@"title" : title, @"body" : body, @"date" : date};
    
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    //NSLog(@"Fire date %@",localNotification.fireDate);
    
   // NSLog(@"Notification--->: %@", localNotification);
    
    [[[UIApplication sharedApplication] scheduledLocalNotifications] enumerateObjectsUsingBlock:^(UILocalNotification *notification, NSUInteger idx, BOOL *stop) {
     //   NSLog(@"Notification %lu: %@",(unsigned long)idx, notification);
    }];
}


+(void)setLocalNotificationWithTitle: (NSString *) title AndAlertBody: (NSString *) body AndWithBookingDate: (NSDate *)date{
    
    NSDate *currentDate = [NSDate date];
    
    
    NSTimeInterval differenceInTime = [date timeIntervalSinceDate:currentDate];
    int differenceInMinutes = differenceInTime/60;
    
   // NSLog(@"%d Minutes before appointment",differenceInMinutes);
    
    //if booking is made within 1 hour remind user 20 mins before hand
    
    //remind user 60 mins before booking if booking is made 2 hours before hand
    //remind user 2 hours before if booking is made greater than 2 hours before hand
    
    //60 mins
    //or within the hour 15
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.timeZone = [NSTimeZone systemTimeZone];
   
    if (differenceInMinutes>=120 && differenceInMinutes>140) {//remind the user 2 hours before hand if bookin gis made greater than 2 hours
        
        NSLog(@"***** Greater than 2 hours *********");
        NSTimeInterval secondsPerHour = (60 * 60)*2;//two hours before appointment
        NSDate *givenDate = date; // what you have already
        NSDate *earlierDate = [givenDate dateByAddingTimeInterval:-secondsPerHour];
         NSLog(@"\n\n Greater than 2 hours %@ \n\n",earlierDate);
        localNotification.fireDate=earlierDate;
        localNotification.alertBody = [NSString stringWithFormat:@"%@ %@",body,[self getDeviceMonthAndYearShortDateFromString:[NSString stringWithFormat:@"%@",date] WithFormat:@"yyyy-MM-dd HH:mm:ss ZZZZZ"]];
        
        [self scheduleNotification:date body:body title:title localNotification:localNotification];

    }
    
    else if (differenceInMinutes>60 && differenceInMinutes>90) {
     
        NSLog(@"****** Greater than 1 hour *******");
        NSTimeInterval secondsPerHour = 60 * 60;
        NSDate *givenDate = date; // what you have already
        NSDate *earlierDate = [givenDate dateByAddingTimeInterval:-secondsPerHour];
        
        
           NSLog(@"\n\n Greater than an hour %@ \n\n",earlierDate);
        
        
        localNotification.fireDate=earlierDate;
        localNotification.alertBody = [NSString stringWithFormat:@"%@ %@",body,[self getDeviceMonthAndYearShortDateFromString:[NSString stringWithFormat:@"%@",date] WithFormat:@"yyyy-MM-dd HH:mm:ss ZZZZZ"]];
        
        [self scheduleNotification:date body:body title:title localNotification:localNotification];


    }
}
+(NSMutableArray *)searchSuggestions{
    
    NSMutableArray * array = [[NSMutableArray alloc] initWithObjects:@"Adore",
    @"The Summit Salon",
    @"Citywax",
    @"The Style house",
    @"Madison Lane",
    @"Mark Vincent",
    nil];
    return array;
}

+(NSMutableArray *)getDaysOfTheWeek{
    
    NSMutableArray * dateArray=[[NSMutableArray alloc]initWithArray:[self daysInWeek:0 fromDate:[NSDate date] ]];
    
    NSLog(@"DateArray %@",dateArray);
    return dateArray;
}

+(NSArray*)daysInWeek:(int)weekOffset fromDate:(NSDate*)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    //ask for current week
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    comps=[calendar components:NSWeekCalendarUnit|NSYearCalendarUnit fromDate:date];
    //create date on week start
    NSDate* weekstart=[calendar dateFromComponents:comps];
    if (weekOffset>0) {
        NSDateComponents* moveWeeks=[[NSDateComponents alloc] init];
        moveWeeks.weekOfYear=1;
        weekstart=[calendar dateByAddingComponents:moveWeeks toDate:weekstart options:0];
    }
    
    //add 7 days
    NSMutableArray* week=[NSMutableArray arrayWithCapacity:7];
    for (int i=1; i<=7; i++) {
        NSDateComponents *compsToAdd = [[NSDateComponents alloc] init];
        compsToAdd.day=i;
        NSDate *nextDate = [calendar dateByAddingComponents:compsToAdd toDate:weekstart options:0];
        [week addObject:nextDate];
        
    }
    return [NSArray arrayWithArray:week];
}

+(void)changeNavigationTitleColor: (UIColor *)navColor OnViewController: (UIViewController *) vc{
    vc.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : navColor};
}




+(NSInteger)getCurrentHour{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate * tempDate = [NSDate date];
    NSCalendar *gregorianCal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComps = [gregorianCal components: (NSHourCalendarUnit | NSMinuteCalendarUnit)
                                                  fromDate: tempDate];
    // Then use it
    [dateComps minute];
    [dateComps hour];
    
    
    return [dateComps hour] ;
}


+(NSString *)getDeviceMonthAndYearShortDateFromString: (NSString *) date WithFormat: (NSString *)format{
   

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    //[dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:format];
    NSLog(@"Date %@",date);
    NSDate * tempDate =[dateFormatter dateFromString: date];
    NSLog(@"Temp date %@",tempDate);
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"MMM yyyy"];
    if (tempDate == nil) {
        return @" ";
    }
    
    return [dateFormatter2 stringFromDate:tempDate];

}

+(void)setReviewReminderWithSalon: (Salon *) salon WithBookingObj: (Booking *) bookObj AndWithChosenDate: (NSDate *)date{
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    NSTimeInterval secondsPerHalfHour = 60 * 30;
    NSLog(@"End of the appointment time-date %@",date);
    NSDate *givenDate = date; // what you have already
    NSDate *earlierDate = [givenDate dateByAddingTimeInterval:secondsPerHalfHour];
    localNotification.fireDate=earlierDate;
    NSLog(@"Time to fire the appointment %@",earlierDate);
    if (earlierDate) {
        NSLog(@"has date");
        localNotification.alertBody = [NSString stringWithFormat:@"Please take a moment to review your experience at %@ today.",salon.salon_name];
        NSData *encodedSalonObject = [NSKeyedArchiver archivedDataWithRootObject:salon];
        NSData *encodedBookingObject = [NSKeyedArchiver archivedDataWithRootObject:bookObj];
        localNotification.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                      encodedSalonObject, @"Salon",
                                      encodedBookingObject, @"Booking",
                                      @"booking",@"type",nil];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
}

+(void)saveViewAsImageWithView: (UIView *) view{
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size,view.opaque,2.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    [UIView showSimpleAlertWithTitle:@"Saved to Photos" message:@"Your booking reference has been saved for your records and can be found in your photos. \n \n This may also act as proof of purchase." cancelButtonTitle:@"OK"];
}

+(NSMutableArray *)getYearsArrayWithNumberOfYears: (int) noOfYears{
    
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *now = [NSDate date];
    NSDateComponents *comps = [gregorian components:NSYearCalendarUnit fromDate:now];
    int year = (int)[comps year];
    
    NSMutableArray *years = [NSMutableArray array];
    for (int y = year; y < year + noOfYears; y++) {
        [years addObject:@(y)];
    }
    
    NSLog(@"Years %@",years);
    return years;
}

+(NSMutableArray *)getMonthsArray{
    
    NSMutableArray * array= [[NSMutableArray alloc] initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil];
    NSLog(@"months %@",array);
    return array;
}

+(NSArray *)getSupportedCardsArray{
    NSArray * array = [[NSArray alloc] initWithObjects:@"VISA",@"MasterCard", nil];
    
    return array;
}


+(NSDate *) getDateFromStringWithFormat:(NSString *) format WithDateString: (NSString *) date{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    formatter.dateFormat = format;
    
    return [formatter dateFromString:date];
}

+(void)popInViewAnimationWithView: (UIView *) container{
    
    
    container.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        // animate it to the identity transform (100% scale)
        container.transform = CGAffineTransformIdentity;
    } completion:nil];

}

+(void)popOutViewAnimationWithView: (UIView *) container{
    // animate it to the identity transform (100% scale)

    container.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        container.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:nil];
    
}

+(void)addNoSalonsPlaceholderForTableView: (UITableView *)table{
    
    UIView * noDataView = [[UIView alloc] initWithFrame:table.frame];
    noDataView.backgroundColor=kBackgroundColor;
    UIImageView * placeHolderImage = [[UIImageView alloc] initWithFrame:noDataView.frame];
    placeHolderImage.image = [UIImage imageNamed:@"background_candy_stripe"];
    placeHolderImage.contentMode = UIViewContentModeScaleAspectFill;
    [noDataView addSubview:placeHolderImage];
  
    UIView * warningView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, table.frame.size.width, 140)];
    warningView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.7];
    warningView.layer.masksToBounds=YES;
    [placeHolderImage addSubview:warningView];
   
    warningView.center = placeHolderImage.center;
    
    UILabel * descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, warningView.frame.size.height/2.0f, warningView.frame.size.width, warningView.frame.size.height/2.0f)];
    descriptionLabel.text = @"We don't have any salons in your area. Please check back soon!";
    descriptionLabel.textColor=kWhatSalonSubTextColor;
    descriptionLabel.numberOfLines=2;
    descriptionLabel.font = [UIFont systemFontOfSize:15.0f];
    descriptionLabel.textAlignment=NSTextAlignmentCenter;
    [warningView addSubview:descriptionLabel];
    
    UIImageView * sadFace = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    sadFace.image = [UIImage imageNamed:@"sad_face"];
    [warningView addSubview:sadFace];
    sadFace.center = CGPointMake(warningView.frame.size.width/2.0f, 30);
    
    UILabel * bummerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, sadFace.frame.size.height, warningView.frame.size.width, 50)];
    bummerLabel.text = @"Bummer!";
    bummerLabel.textColor=kWhatSalonSubTextColor;
    bummerLabel.textAlignment = NSTextAlignmentCenter;
    bummerLabel.font = [UIFont boldSystemFontOfSize:18];
    [warningView addSubview:bummerLabel];
    table.backgroundView =noDataView;
    
    sadFace.image = [sadFace.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    sadFace.tintColor=kWhatSalonSubTextColor;
}

+(void)removeNoSalonsPlaceholderForTableView: (UITableView *)table{
    table.backgroundView=nil;
}
+(void)removeDescriptiveTableViewBackgroundWithTableView: (UITableView *) tv{
    tv.backgroundView=nil;
}
+(void)addDescriptiveTableViewBackgroundWithTableView: (UITableView *) tv withTitle: (NSString *)title withSubtitle: (NSString *) sub andWithIcon: (UIImage *) icon{
    
    UIView * noDataView = [[UIView alloc] initWithFrame:tv.frame];
    noDataView.backgroundColor=kBackgroundColor;
    UIImageView * placeHolderImage = [[UIImageView alloc] initWithFrame:noDataView.frame];
    placeHolderImage.image = [UIImage imageNamed:@"background_candy_stripe"];
    placeHolderImage.contentMode = UIViewContentModeScaleAspectFill;
    [noDataView addSubview:placeHolderImage];
    
    UIView * warningView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tv.frame.size.width, 140)];
    warningView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.7];
    warningView.layer.masksToBounds=YES;
    [placeHolderImage addSubview:warningView];
    
    warningView.center = placeHolderImage.center;
    
    UILabel * descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, warningView.frame.size.height/2.0f, warningView.frame.size.width, warningView.frame.size.height/2.0f)];
    descriptionLabel.text = sub;
    descriptionLabel.textColor=kWhatSalonSubTextColor;
    descriptionLabel.numberOfLines=2;
    descriptionLabel.font = [UIFont systemFontOfSize:15.0f];
    descriptionLabel.textAlignment=NSTextAlignmentCenter;
    [warningView addSubview:descriptionLabel];
    
    UIImageView * sadFace = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    sadFace.image = icon;
    [warningView addSubview:sadFace];
    sadFace.center = CGPointMake(warningView.frame.size.width/2.0f, 30);
    
    UILabel * bummerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, sadFace.frame.size.height, warningView.frame.size.width, 50)];
    bummerLabel.text = title;
    bummerLabel.textColor=kWhatSalonSubTextColor;
    bummerLabel.textAlignment = NSTextAlignmentCenter;
    bummerLabel.font = [UIFont boldSystemFontOfSize:18];
    [warningView addSubview:bummerLabel];
    tv.backgroundView =noDataView;
    
    sadFace.image = [sadFace.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    sadFace.tintColor=kWhatSalonSubTextColor;

}

+(void)setDefaultNavBar: (UIViewController*)controller{
    
    [WSCore statusBarColor:StatusBarColorBlack];
    [controller.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    [controller.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    controller.navigationController.navigationBar.tintColor=kWhatSalonBlue;
}

+(UIImage *)takeScreenShotOfView: (UIView *) view{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size,view.opaque,2.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return viewImage;
}

+(UIView *)floatingView: (UIView *)view{
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0, 4.0);
    view.layer.shadowRadius = kCornerRadius;
    view.layer.shadowOpacity = 0.6;
    view.layer.masksToBounds = NO;
    
    return view;
}

+(UIView *)floatingView: (UIView *)view WithAlpha:(float) alpha {
    view.layer.shadowColor = [UIColor colorWithWhite:0.0 alpha:alpha].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0, 4.0);
    view.layer.shadowRadius = kCornerRadius;
    view.layer.shadowOpacity = 0.6;
    view.layer.masksToBounds = NO;
    
    return view;
}

+(NSString *)vowelCheckString: (NSString *) str{
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([str.lowercaseString hasPrefix:@"a"] ||[str.lowercaseString hasPrefix:@"e"] || [str.lowercaseString hasPrefix:@"i"] ||[str.lowercaseString hasPrefix:@"o"]){
        str = [NSString stringWithFormat:@"an %@",str];
    }
    else{
        str = [NSString stringWithFormat:@"a %@",str];
    }
    
    return str;
}

#pragma mark - UITabBarNavigationAnimation
// pass a param to describe the state change, an animated flag and a completion block matching UIView animations completion
/*
+ (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated WithTabBarController: (UITabBarController *) tc OnView: (UIView *) view completion:(void (^)(BOOL))completion {
    
    // bail if the current state matches the desired state
    if ([self tabBarIsVisible:tc OnView:view] == visible) return;
    
    // get a frame calculation ready
    CGRect frame = tc.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = (visible)? -height : height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.3 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        tc.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    } completion:completion];
}

+(void)hideTabAnimated: (BOOL)animated WithTabBarController: (UITabBarController *) tc{
    // get a frame calculation ready
    CGRect frame = tc.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.3 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        tc.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    } completion:nil];

}
// know the current state
+ (BOOL)tabBarIsVisible: (UITabBarController *)tc OnView: (UIView *) view {
    return tc.tabBar.frame.origin.y < CGRectGetMaxY(view.frame);
}
*/
/*
+(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
*/
//use in conjunction with OS
+(NSString *) fetchOperatingSystem{
    NSString * deviceNumber = [[UIDevice currentDevice] systemVersion];
    deviceNumber = [deviceNumber stringByReplacingOccurrencesOfString:@"." withString:@"_"];
    
    return deviceNumber;
}

+(void)registerForUILocalNotifications{
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        
        
            UIUserNotificationSettings * notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeSound categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        
        
        
        
        }
    
        else{
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge];
    }
    
    

}

+(void)updateUsersCityAndCountry: (CLLocation *) location{
    CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
    
    [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
    {
        
        
        CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
        //NSString *countryCode = myPlacemark.ISOcountryCode;
        NSString *countryName = myPlacemark.country;
        NSString *cityName= myPlacemark.locality;
        
        
        if ([[User getInstance] isUserLoggedIn] && cityName.length>0 && countryName.length>0) {
            [[User getInstance] updateUsersCityAndCountry:[NSString stringWithFormat:@"%@, %@",cityName,countryName]];
        }
        else if([[User getInstance] isUserLoggedIn] ){
            [[User getInstance] updateUsersCityAndCountry:@""];
        }
        
        
    }];
}
@end
