//
//  TabReviewOptionsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyBookings.h"
#import "Salon.h"


/*!
     @protocol BookingsReviewDelegate
  
     @brief The BookingsReviewDelegate protocol
  
     It's a protocol used to notify the delegate that
 */

@protocol BookingsReviewDelegate <NSObject>

@optional

/*! @brief an action that notifies the delegate that the user wishes to book again with the salon.
    
    @param salon - the chosen Salon.
 
 */
-(void)bookAgainWithSalon: (Salon*)salon;


/*! @brief an action that notifies the delegate that the users wishes to cancel his/her appointment. 
 
    @param booking - MyBookings object.
 
 */
-(void)canceledAppointment: (MyBookings *)booking;

/*!
 
    @brief an action that notifies that a review has been added.
 
 */
-(void)addedReview;
@end


@interface TabReviewOptionsViewController : UIViewController

/*! 
 @brief represents the BookingsReviewDelegate.
 
 */
@property (nonatomic,assign) id<BookingsReviewDelegate> delegate;

/*! 
 @brief represents the booking data
 */
@property (nonatomic) MyBookings * bookingData;

/*! @brief represents the first label for the first action. */
@property (weak, nonatomic) IBOutlet UILabel *label1;

/*! @brief represents the second label for the second action. */
@property (weak, nonatomic) IBOutlet UILabel *label2;

/*! @brief represents the third label for the third action. */
@property (weak, nonatomic) IBOutlet UILabel *label3;

/*! @brief represents action for the first option. */
- (IBAction)buttonAction1:(id)sender;

/*! @brief represents the action for the third  option. */
- (IBAction)buttonAction3:(id)sender;

/*! @brief represents the action for the second option. */
- (IBAction)buttonAction2:(id)sender;


@end
