//
//  DatePickerCell.swift
//  WhatSalon
//
//  Created by admin on 9/13/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import Reusable
import SwiftDate
import JTAppleCalendar

class DatePickerCell: JTAppleCell, NibReusable {
    
    //MARK: Outlets
    @IBOutlet private var dayLabel:         UILabel!
    @IBOutlet private var backGroundView:   UIView!
    
    //MARK: Properties
    var selectedDate = Date()

    var day: Int? {
        didSet {
            dayLabel.text = String(describing: day ?? 0)
        }
    }
    
    var isSelectedDay: Bool = false {
        didSet {
            let unselectedStateFont = UIFont.systemFont(ofSize: 9)
            let selectedStateFont = UIFont.boldSystemFont(ofSize: 9)
            
            if isSelectedDay  {
                Gradient.addPinkToPurpleDiagonalGradient(on: backGroundView)
            } else {
                backGroundColor = .white
            }
            dayLabel.font = isSelectedDay ? selectedStateFont : unselectedStateFont
            dayLabel.textColor = isSelectedDay ? .white : .darkGray
        }
    }
    
    var isDayOfWeek: Bool = false {
        didSet {
            backGroundColor = isDayOfWeek ? .lightGrayCalendar : .white
        }
    }

    var belongToSelectedMonth = false  {
        didSet {
            if !belongToSelectedMonth {
            self.backGroundColor = .white
            self.dayLabel.textColor = .clear
            self.isUserInteractionEnabled = false
            } else {
                self.isUserInteractionEnabled = true
            }
        }
    }
    
    var activeDate:Date?  {
        didSet {
           
            if (activeDate ?? Date()) <= Date() && belongToSelectedMonth{
                self.dayLabel.textColor = .lightGray
                self.isUserInteractionEnabled = false
            }
        }
    }

   private var backGroundColor: UIColor? {
        didSet {
            backGroundView.backgroundColor = backGroundColor
        }
    }
}
