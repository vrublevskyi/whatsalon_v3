//
//  MontYearView.swift
//  WhatSalon
//
//  Created by admin on 8/28/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class MontYearView: NibDesignable {
    
    //MARK: Outlets
    
    @IBOutlet private var monthYearLabel: UILabel!
    
    //MARK: Properties
    
    var date: String? {
        didSet {
            monthYearLabel.text = date
        }
    }
    
    var textColor: UIColor? {
        didSet {
            monthYearLabel.textColor = textColor
        }
    }
    
    var font: UIFont? {
        didSet {
            monthYearLabel.font = font
        }
    }
}
