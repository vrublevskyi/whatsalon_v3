//
//  DatePickerVC.swift
//  WhatSalon
//
//  Created by admin on 9/13/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import JTAppleCalendar
import SwiftDate

@objc protocol DatePickerViewDelegate: class {
    func didSelect(date: Date?, on controller: DatePickerView)
}

class DatePickerView: UIViewController {
    //MARK: Outlets
    @IBOutlet fileprivate weak var monthsScrollView: UIScrollView! {
        didSet {
             monthsList()
             setupSlideScrollView(configureMonth())
        }
    }
    @IBOutlet fileprivate var monthsCrollContainerView: UIView! {
        didSet {
            Gradient.addPinkToPurpleHorizontalGradient(on: monthsCrollContainerView)
        }
    }
    
    @IBOutlet weak var calendarView: JTAppleCalendarView! {
        didSet {
            date = Date().date
            calendarView.register(cellType: DatePickerCell.self)
            calendarView.scrollToDate(date, animateScroll: false)
            calendarView.selectDates([date])
        }
    }
    
    //MARK: Properties
    @objc weak var delegate: DatePickerViewDelegate?
    private var  months:        [Date] = []
    private var  startDate:     Date?
    private var  endDate:       Date?
    private var  startOfWeek:   Date?
    private var  endOfWeek:     Date?
    
    private var date = Date() {
        didSet {
            startDate = date.startOfMonth().date.localFromUTC() ?? Date()
            endDate = date.endOfMonth().date
            startOfWeek = date.startOfWeek?.localFromUTC() ?? Date()
            endOfWeek = date.endOfWeek ?? Date()
        }
    }
    
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkMarkNavigationButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Pick a Date"
    }
    
   @objc var showTodayDate = false {
        didSet {
            if showTodayDate {
                monthsScrollView.setContentOffset(CGPoint(x: monthsScrollView.frame.size.width * 12 , y: 0), animated: true)
                date = Date().date
                self.scrollTo(date)
            }
        }
    }
    
    func checkMarkNavigationButton() {
        //CHECK MARK Button
        let image = UIImage(named: "checkmark_icon")!
        
        let imgView: UIImageView = UIImageView()
        imgView.image = image.withRenderingMode(.alwaysTemplate)

        imgView.tintColor = .lightGray

        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        button.layer.cornerRadius = 12.5;
        button.layer.borderWidth = 1
        button.setBackgroundImage(imgView.image, for: .normal)
        button.addTarget(self, action: #selector(selectDate), for: .touchUpInside)
        button.layer.borderColor = UIColor.lightGray.cgColor

        button.widthAnchor.constraint(equalToConstant: 25).isActive = true
        button.heightAnchor.constraint(equalToConstant: 25).isActive = true
        let item1 = UIBarButtonItem(customView: button)

        self.navigationItem.setRightBarButtonItems([item1], animated: true)
    }
    
    @objc private func selectDate() {
        self.navigationController?.popViewController(animated: true)
        self.delegate?.didSelect(date: date, on: self)
    }
    
    fileprivate func monthsList()
    {
        //append current month
        months.append(Date().date)
        
        for index in 1...12 {
            
            let previousMonth = Calendar.current.date(byAdding: .month, value: -index, to: Date())
            let nextMonth = Calendar.current.date(byAdding: .month, value: index, to: Date())
            months.append(nextMonth?.date ?? Date())
            months.append(previousMonth?.date ?? Date())
        }
        let sortedMonths = months.sorted(by: { $0.date > $1.date})
        months = sortedMonths.reversed()
    }
    
    private func configureMonth() -> [MontYearView]{
        let monthsView: [MontYearView] = self.months.compactMap {
            let date = $0.date.toFormat("MMMM yyyy").uppercased()
            let monthYearView = MontYearView()
            monthYearView.date = date
            return monthYearView
        }
        return monthsView
    }
}

extension DatePickerView: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        calendarView.minimumLineSpacing = -0.5
        calendarView.minimumInteritemSpacing = -0.4
        
        calendarView.scrollDirection = .vertical
        calendarView.scrollingMode = .stopAtEachCalendarFrame
        
        var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone   = TimeZone(identifier: "UTC")!

        let start           = self.startDate?.localFromUTC() ?? Date()
        let end             = self.endDate ?? Date()
        let params          = ConfigurationParameters(startDate: start, endDate: end, numberOfRows: 1, calendar: calendar, generateInDates: .forFirstMonthOnly, generateOutDates: .off)
        return params
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configure(cell: cell, with: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell: DatePickerCell = calendar.dequeueReusableCell(for: indexPath, cellType: DatePickerCell.self)

        configure(cell: cell, with: cellState)
        if cellState.dateBelongsTo == .thisMonth {
            currentWeek(date,cell)
        }
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configure(cell: cell, with: cellState)
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        startOfWeek = date.startOfWeek?.localFromUTC() ?? Date()
        endOfWeek = date.endOfWeek ?? Date()
        self.date = date
        delegate?.didSelect(date: date, on: self)
        calendarView.reloadData()
        configure(cell: cell, with: cellState)
      

    }
    
    func configure(cell: JTAppleCell?, with state: CellState) {
        guard let cell = cell as? DatePickerCell else { return }
        calendarView.deselectAllDates()
        cell.day = state.date.day
        cell.selectedDate = state.date
        cell.isSelectedDay = state.isSelected
        
        //TODO: - check why not working correct without true/false
        if state.dateBelongsTo != .thisMonth {
            cell.belongToSelectedMonth = false
        } else {
            cell.belongToSelectedMonth = true
        }
        cell.activeDate = state.date
    }
    
    func currentWeek(_ date: Date, _ cell: DatePickerCell?) {
        if date >= (startOfWeek ?? Date()) && date <= (endOfWeek  ?? Date()){
                cell?.day = date.day
                cell?.selectedDate = date
                cell?.isDayOfWeek = true
        }
    }
}

extension DatePickerView: UIScrollViewDelegate {
        func setupSlideScrollView(_ monthsViews : [UIView]) {
            var lastMonthView: UIView?
            
            for monthView in monthsViews {
                monthsScrollView.addSubview(monthView)
                monthView.translatesAutoresizingMaskIntoConstraints = false
                
                monthView.topAnchor.constraint(equalTo: monthsScrollView.topAnchor).isActive = true
                monthView.bottomAnchor.constraint(equalTo: monthsScrollView.bottomAnchor).isActive = true
                monthView.heightAnchor.constraint(equalTo: monthsScrollView.heightAnchor).isActive = true
                monthView.widthAnchor.constraint(equalTo: monthsScrollView.widthAnchor).isActive = true
                
                if let lastMonthView = lastMonthView {
                    monthView.leadingAnchor.constraint(equalTo: lastMonthView.trailingAnchor).isActive = true
                }
                else {
                    monthView.leadingAnchor.constraint(equalTo: monthsScrollView.leadingAnchor).isActive = true
                }
                
                if monthView == monthsViews.last {
                    monthView.trailingAnchor.constraint(equalTo: monthsScrollView.trailingAnchor).isActive = true
                }
                
                lastMonthView = monthView
            }
            let currentDateIndex:CGFloat = 12.0
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                self.monthsScrollView.setContentOffset(CGPoint(x: self.monthsScrollView.frame.size.width * currentDateIndex , y: 0), animated: true)
            }
        }
        
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            let index = round(scrollView.contentOffset.x / monthsScrollView.frame.size.width)
            let date = months[Int(index)]
            
            var selectedMonth = "\(date.month)-1-\(date.year)".toDate()?.date ?? Date()
            self.date = selectedMonth

            if selectedMonth.date.month == Date().month && selectedMonth.date.year == Date().year {
                selectedMonth = Date().date
            }
            scrollTo(selectedMonth)
        }
        
        private func scrollTo(_ selectedDate: Date) {
            calendarView.deselectAllDates()
            calendarView.reloadData()
            calendarView.scrollToDate(selectedDate)
            calendarView.selectDates([selectedDate])
    }
}
