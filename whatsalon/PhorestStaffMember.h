//
//  PhorestStaffMember.h
//  whatsalon
//
//  Created by Graham Connolly on 28/11/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//


/*!
 @header PhorestStaffMember.h
  
 @brief This is the header file for PhorestStaffMember.
 
 @remark refactoring functionality is badly broken in Xcode 7. PhorestStaffMember should be changed to a more general object.
  
  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd.
 @version    -
 */
#import <Foundation/Foundation.h>
#import "IsalonBookingDetails.h"


@interface PhorestStaffMember : NSObject

/*! @brief represents the staff members first name. */
@property (nonatomic) NSString * staff_first_name;

/*! @brief represents the staff members last name. */
@property (nonatomic) NSString * staff_last_name;

/*! @brief represents the staff id. */
@property (nonatomic) NSString * staff_id;

/*! @brief represents the times the staff member does. */
@property (nonatomic) NSMutableArray * times;

/*! @brief determines if this object is an any stylist. */
@property (nonatomic,assign) BOOL isAnyStylist;

/*! @brief determines if the staff member is working or not. */
@property (nonatomic) BOOL isWorking;

/*! @brief represents the price of the staff memeber. */
@property (nonatomic) NSString * staff_service_price;

/*! @brief object comparison. */
-(BOOL)isEqual:(id)object;

/*! @brief represents the unique iSalon booking details for iSalon bookings. */
@property (nonatomic) IsalonBookingDetails * isalon_booking_details;
@end
