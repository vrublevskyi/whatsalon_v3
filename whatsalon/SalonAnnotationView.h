//
//  SalonAnnotationView.h
//  whatsalon
//
//  Created by Graham Connolly on 15/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//


/*!
 @header SalonAnnotationView.h
  
 @brief This is the header file for the SalonAnnotationView.h
 
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.3
 */

#import <MapKit/MapKit.h>

@interface SalonAnnotationView : MKAnnotationView

/*! @brief represents the salon image type as a string. */
@property (nonatomic,copy) NSString * salonImageType;

@end
