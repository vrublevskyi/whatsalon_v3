//
//  SalonReviewTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 18/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SalonReviewTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIColor+FlatUI.h"
#import "UIImage+ColoredImage.h"
#import "UIView+AlertCompatibility.h"

@implementation SalonReviewTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //number of reviews;
        self.numberOfReviewsLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 304, 21)];
        [self.contentView addSubview:self.numberOfReviewsLabel];
        
        //image
        self.userImage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 40, 40, 40)];
        self.userImage.layer.cornerRadius = self.userImage.frame.size.width/2.0f;
        self.userImage.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.userImage.layer.borderWidth=1.0f;
        self.userImage.layer.masksToBounds=YES;
        [self.contentView addSubview:self.userImage];
        //name
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(56, 40, 236, 15)];
        self.nameLabel.text = @"user_name";
        [self.contentView addSubview:self.nameLabel];
        
        //image rating
        self.imageRatingImage = [[UIImageView alloc] initWithFrame:CGRectMake(56, 60, 100, 20)];
        self.imageRatingImage.contentMode = UIViewContentModeScaleAspectFit;
        self.imageRatingImage.image = [WSCore getStarRatingImage:0];
        [self.contentView addSubview:self.imageRatingImage];
        
        //date label
        self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width-100, 60, 128, 21)];
        self.dateLabel.text=@"date";
        [self.contentView addSubview:self.dateLabel];
        
        self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 83, 304, 80)];
        self.messageLabel.text=@"message goes here";
        self.messageLabel.numberOfLines=0;
        [self.contentView addSubview:self.messageLabel];
        
        self.viewAllButton = [[UIButton alloc] initWithFrame:CGRectMake(120, 171, 80, 40)];
        [self.viewAllButton addTarget:self action:@selector(showAllReviews) forControlEvents:UIControlEventTouchUpInside];
        [self.viewAllButton setTitle:@"View All" forState:UIControlStateNormal];
        [self.viewAllButton.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        
        [self.contentView addSubview:self.viewAllButton];
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.contentView.layer.masksToBounds=YES;
        self.layer.masksToBounds=YES;
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setUpReviewPageWithReview : (SalonReview *) review AndWithNumberOfReviews: (int) numberOfReviews{
    
    if(numberOfReviews==0){
        self.nameLabel.text=@"";
        self.dateLabel.text=@"";
        self.imageRatingImage.hidden=YES;
        self.userImage.hidden=YES;
        self.writeReviewImage.hidden=YES;
        self.writeYourReviewLabel.hidden=YES;
        self.numberOfReviewsLabel.text=@"No reviews submitted yet";
        [self.numberOfReviewsLabel setFont:[UIFont systemFontOfSize:14.0f]];
        self.numberOfReviewsLabel.textColor = [UIColor lightGrayColor];
        self.numberOfReviewsLabel.alpha=0.7;
        self.messageLabel.hidden=YES;
        
        self.addReviewButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
        [self.addReviewButton setTitle:@"Add Your Review" forState:UIControlStateNormal];
        [self.addReviewButton.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
        [self.addReviewButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
        [self.contentView addSubview:self.addReviewButton];
        self.addReviewButton.center = self.contentView.center;
        self.addReviewButton.layer.cornerRadius=kCornerRadius;
        self.addReviewButton.layer.borderColor=kWhatSalonBlue.CGColor
        ;
        [self.addReviewButton addTarget:self action:@selector(addYourReview) forControlEvents:UIControlEventTouchUpInside];
        self.addReviewButton.layer.borderWidth=1.0;
        return;
    }
    
    self.imageRatingImage.hidden=NO;
    self.userImage.hidden=NO;
    self.numberOfReviewsLabel.textColor=[UIColor blackColor];
    
    self.addReviewButton.hidden = YES;
    NSString * firstName;
    NSString * lastName;
    if (review.user_name.length==0 && review.user_last_name.length==0) {
        firstName= @"WhatSalon";
        lastName = @"User";
    }
    else{
        firstName = review.user_name;
        lastName = review.user_last_name;
    }
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",firstName, lastName];
    self.dateLabel.text = review.date_reviewed;
    NSString * message = review.message;
    message = [message stringByReplacingOccurrencesOfString:@"+" withString:@" "];
   
    self.imageRatingImage.image = [UIImage imageNamed:@"white_four_star_rating"];
    
    self.imageRatingImage.image = [self.imageRatingImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.imageRatingImage setTintColor:kWhatSalonBlue];
    self.userImage.image = [UIImage imageNamed:@"dummy_avatar2.jpg"];
    self.userImage.contentMode = UIViewContentModeScaleAspectFit;
    
    if (self.listView) {
        self.numberOfReviewsLabel.hidden=YES;
    }
    else if (numberOfReviews==1) {
        self.numberOfReviewsLabel.text = [NSString stringWithFormat:@"%d Review",numberOfReviews];
    }else{
        self.numberOfReviewsLabel.text = [NSString stringWithFormat:@"%d Reviews",numberOfReviews];
    }
    
    
    self.numberOfReviewsLabel.font = [UIFont systemFontOfSize:14.0f];
    self.nameLabel.font = [UIFont systemFontOfSize:14.0f];
    
    
    /*
     message frame
     */
    CGRect messageFrame = self.messageLabel.frame;
    messageFrame.size.width=self.frame.size.width-20;
    messageFrame.origin.x=10;

    messageFrame.size.height=100;
   
    
    self.messageLabel.frame=messageFrame;
    
 
    self.messageLabel.font = [UIFont systemFontOfSize:13.0f];
    self.messageLabel.text=message;

    self.dateLabel.font = [UIFont systemFontOfSize:14.0f];
    self.dateLabel.text = [WSCore getDeviceMonthAndYearShortDateFromString:review.date_reviewed WithFormat:@"yyyy-MM-dd"];
    CGRect dateFrame = self.dateLabel.frame;
    dateFrame.origin.x = self.frame.size.width-self.dateLabel.frame.size.width-10;
    self.dateLabel.frame=dateFrame;
    self.dateLabel.textAlignment=NSTextAlignmentRight;
    
    CGRect viewAllButtonFrame = self.viewAllButton.frame;
    viewAllButtonFrame.origin.y = self.frame.size.height-10-self.viewAllButton.frame.size.height;
    self.viewAllButton.frame=viewAllButtonFrame;
    self.viewAllButton.center = CGPointMake(self.center.x, self.viewAllButton.center.y);
    [self.viewAllButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    self.viewAllButton.layer.borderColor=kWhatSalonBlue.CGColor;
    self.viewAllButton.layer.borderWidth=1.0f;
    self.viewAllButton.layer.cornerRadius=3.0f;
    
    self.dateLabel.textColor= [UIColor lightGrayColor];;
    
    UIImage * profileImage = [UIImage imageNamed:kNo_Avatar_Image_String];
    
    //if the user has no avatar
    if (review.user_avatar_url.length <1) {
        self.userImage.image = profileImage;
        self.userImage.contentMode=UIViewContentModeScaleAspectFit;
        self.userImage.layer.borderWidth=0;
    }
    else{
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:review.user_avatar_url]
                           placeholderImage:profileImage];
        self.userImage.contentMode=UIViewContentModeScaleAspectFill;
       
    }
    
    //hides the image rating
    self.imageRatingImage.hidden=YES;
 
    
    [WSCore addBottomIndentedLine:self :[UIColor cloudsColor]];
    
    
    
    if (self.listView) {
        self.viewAllButton.hidden=YES;
        self.contentView.backgroundColor=[UIColor clearColor];
        self.backgroundColor=[UIColor clearColor];
        self.backgroundView=nil;
        self.messageLabel.textColor=[UIColor cloudsColor];
        self.dateLabel.textColor = [UIColor cloudsColor];
        self.nameLabel.textColor = [UIColor cloudsColor];
    }
    else{
       
        if ((int)numberOfReviews>1) {
            self.viewAllButton.hidden=NO;
        }
        else{
            self.viewAllButton.hidden=YES;
        }
    }

    
    if ([[User getInstance] isUserLoggedIn] && !self.listView) {
        self.writeYourReviewLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width-140, 8, 140, 20)];
        self.writeYourReviewLabel.font = [UIFont systemFontOfSize:12.0f];
        self.writeYourReviewLabel.text = @"Write Your Review";
        self.writeYourReviewLabel.textColor = [UIColor lightGrayColor];
        self.writeYourReviewLabel.userInteractionEnabled=YES;
        
        UITapGestureRecognizer * tapWriteReviewLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addYourReview)];
        tapWriteReviewLabel.numberOfTapsRequired=1;
        [self.writeYourReviewLabel addGestureRecognizer:tapWriteReviewLabel];
        
        
        if (self.writeReviewImage==nil) {
            
            self.writeReviewImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"write_your_review_icon"]];
            self.writeReviewImage.frame = CGRectMake(self.contentView.frame.size.width-30, 10, 15, 15);
            self.writeReviewImage.userInteractionEnabled=NO;
            [self.contentView addSubview:self.writeReviewImage];
        }
        
        
        [self addSubview:self.writeYourReviewLabel];
    }
    
}

#pragma mark - Protocol
-(void)addYourReview{
    
    if ([[User getInstance] isUserLoggedIn]) {
      
        if ([self.delegate respondsToSelector:@selector(didSelectAddYourReview)]) {
            [self.delegate didSelectAddYourReview];
        }
    }
    else{
        [UIView showSimpleAlertWithTitle:@"Log In Or Sign Up" message:@"Please log in or sign up to make a review" cancelButtonTitle:@"OK"];
    }
    
}
-(void)showAllReviews{
    
    
    if ([self.delegate respondsToSelector:@selector(didSelectRowToShowAll) ]) {
        [self.delegate didSelectRowToShowAll];
    }
}

@end
