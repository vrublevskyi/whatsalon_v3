//
//  MKMapView-Additions.h
//  vevoke
//
//  Created by Alexandru Chis on 11/24/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface MKMapView (MKMapView_Additions)

+ (double)distanceFromStartCoordinate:(CLLocationCoordinate2D)startCoordinate
					  toEndCoordinate:(CLLocationCoordinate2D)endCoordinate;

+ (MKCoordinateRegion)regionForAnnotations:(NSArray *)annotations;
+ (MKCoordinateRegion)regionArroundAnnotationWithYOffset:(id<MKAnnotation>) annotation andOffset:(float) offset;
+ (MKCoordinateRegion)regionArroundAnnotation:(MKMapView*) annotation;
+(CLLocationCoordinate2D)translateCoord:(CLLocationCoordinate2D)coord MetersLat:(double)metersLat MetersLong:(double)metersLong;

+ (MKCoordinateRegion)regionArroundGPSPosition:(CLLocationCoordinate2D)coordinate  andOffset:(float) offset andCurrentRegion:(MKCoordinateRegion) currentRegion ;
@end
