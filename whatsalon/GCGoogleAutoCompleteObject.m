//
//  GCGoogleAutoCompleteObject.m
//  GoogleAutoComplete
//
//  Created by Graham Connolly on 04/11/2015.
//  Copyright © 2015 GrahamConnolly. All rights reserved.
//

#import "GCGoogleAutoCompleteObject.h"

@implementation GCGoogleAutoCompleteObject


-(instancetype)initWithPrediction: (NSDictionary *) prediction{
    self = [super init];
    if (self) {
        if (prediction[@"description"] !=nil && prediction[@"place_id"] !=nil) {
            self.name = prediction[@"description"];
            self.placeID = prediction[@"place_id"];
            
            if (prediction[@"terms"] !=nil) {
                
                NSArray * predictionArray = prediction[@"terms"];
                for (int i=0; i<predictionArray.count; i++) {
                    NSDictionary * terms = predictionArray[i];
                    if (i==0) {
                        NSLog(@"1st %@",terms);
                        self.title = terms[@"value"];
                    }
                    else{
                        NSLog(@"Subtitle %@",terms);
                        if (self.subtitle.length==0) {
                          self.subtitle= terms[@"value"];
                        }
                        else{
                            self.subtitle = [self.subtitle stringByAppendingString:[NSString stringWithFormat:@", %@",terms[@"value"]]];
                        }
                        
                    }
                }
                /*
                for (NSDictionary * terms in prediction[@"terms"]) {
                    NSLog(@"Terms %@",terms);
                }
                 */
            }
        }
       
    }
    return self;
}
@end
