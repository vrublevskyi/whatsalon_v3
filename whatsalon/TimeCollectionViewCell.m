//
//  TimeCollectionViewCell.m
//  Horizontal TableView
//
//  Created by Graham Connolly on 12/12/2014.
//  Copyright (c) 2014 Graham Connolly. All rights reserved.
//

#import "TimeCollectionViewCell.h"

@implementation TimeCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)/2)];
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)/2.0f, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)/2)];
        self.timeLabel.textColor=[UIColor whiteColor];
        self.priceLabel.textColor = [UIColor whiteColor];
        self.midLabel.textColor = [UIColor whiteColor];
        
        self.priceLabel.textAlignment=NSTextAlignmentCenter;
        self.timeLabel.textAlignment=NSTextAlignmentCenter;
        
        self.midLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)/2.0)];
        [self.contentView addSubview:self.midLabel];
        
        self.midLabel.center = self.contentView.center;
        self.midLabel.hidden=YES;
        self.midLabel.textAlignment=NSTextAlignmentCenter;
        
        [self.contentView addSubview:self.priceLabel];
        [self.contentView addSubview:self.timeLabel];
        
        self.priceLabel.text =@"Price";
        self.timeLabel.text = @"Time";
        self.midLabel.text = @"Mid Price";
    
    
        [WSCore addMiddleLineOnView:self.contentView WithColor:[UIColor whiteColor] AndHeight:1.0f];
        
    }
    
    return self;
    
}
-(void)setSelected:(BOOL)selected{
    self.backgroundColor = selected?[UIColor whiteColor]:[UIColor clearColor];
    self.timeLabel.textColor =selected?[UIColor blackColor] :[UIColor whiteColor];
    self.priceLabel.textColor =selected?[UIColor blackColor] :[UIColor whiteColor];
    self.midLabel.textColor =selected?[UIColor blackColor] :[UIColor whiteColor];
    [super setSelected:selected];
}


@end
