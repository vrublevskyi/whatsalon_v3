//
//  OptionsReviewViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "OptionsReviewViewController.h"
#import "GetDirectionsViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SalonReviewViewController.h"
#import "UIView+AlertCompatibility.h"
#import "SocialShareViewController.h"
#import "GCNetworkManager.h"
#import "User.h"

@interface OptionsReviewViewController ()<UIViewControllerTransitioningDelegate,ReviewDelegate,GCNetworkManagerDelegate>
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *optionsView;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (nonatomic) GCNetworkManager * networkManager;
@end

@implementation OptionsReviewViewController

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    [UIView showSimpleAlertWithTitle:@"Oops" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    //[UIView showSimpleAlertWithTitle:@"Canceled" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
    self.label3.alpha=0.6;
    self.button3.enabled=NO;
    
    NSLog(@"cancel booking id %@",self.bookingData.bookingId);
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Canceled" message:jsonDictionary[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    alert.tag=333;
    [alert show];
    //if ([self.delegate performSelector:@selector(canceledAppointment:)]) {
      //  [self.delegate canceledAppointment:self.bookingData];
    //}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==333) {
        if(buttonIndex==[alertView cancelButtonIndex]){
            if ([self.delegate respondsToSelector:@selector(canceledAppointment:)]) {
                [self.delegate canceledAppointment:self.bookingData];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        
    }
    else if(alertView.tag==701){
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            [self cancelBooking];
        }
    }
}

-(void)cancelBooking{
    NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&booking_id=%@",self.bookingData.bookingId]];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kCancel_Appointment_URL]];
    [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [WSCore statusBarColor:StatusBarColorWhite];
    self.view.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.8];
    self.optionsView.backgroundColor=kWhatSalonBlue;
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.parentView=self.view;
    self.networkManager.delegate=self;
    
    
    
    NSLog(@"occurred  ? %d %@",self.bookingData.bookingOccurred,self.bookingData.servceName);
     NSLog(@"IS salon still active %@", StringFromBOOL(self.bookingData.active));
    if (self.bookingData.bookingOccurred) {
        NSLog(@"Booking occurred");
        // Date has passed
        UIImage *image = [[UIImage imageNamed:@"reload_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.button1 setImage:image forState:UIControlStateNormal];
        
       
        if (!self.bookingData.active) {
            self.label1.alpha=0.6;
            self.button1.enabled=NO;
            self.button1.alpha=0.6;
        }
        else{
            self.label1.alpha=1.0;
            self.button1.enabled=YES;
            self.button1.alpha=1.0;
        }
        
        self.label1.text=@"Book";
        
          UIImage *image2 = [[UIImage imageNamed:@"star_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.button2 setImage:image2 forState:UIControlStateNormal];
       
        self.label2.text=@"Rate";
        
        if (self.bookingData.reviewOccurred ) {
            self.button2.enabled=NO;
            self.label2.alpha=0.6;
        }
        
        UIImage *image3 = [[UIImage imageNamed:@"Share"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        [self.button3 setImage:image3 forState:UIControlStateNormal];
        self.button3.tintColor=[UIColor whiteColor];
        self.label3.text = @"Share";
        
        
    }else{
          UIImage *image = [[UIImage imageNamed:@"Map"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.button1 setImage:image forState:UIControlStateNormal];
     
        self.label1.text=@"Map";
        
        UIImage *image2 = [[UIImage imageNamed:@"Telephone"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        [self.button2 setImage:image2 forState:UIControlStateNormal];
        self.label2.text=@"Call";
        
        UIImage *image3 = [kCancelButtonImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        [self.button3 setImage:image3 forState:UIControlStateNormal];
        self.button3.tintColor=[UIColor whiteColor];
        self.label3.text = @"Cancel \n Appointment";
        self.label3.font=[UIFont systemFontOfSize:14.0f];
        self.label3.numberOfLines=0;
        self.label3.adjustsFontSizeToFitWidth=YES;
        

    }
    
    self.button1.tintColor=[UIColor whiteColor];
    self.button2.tintColor=[UIColor whiteColor];
    
    
    
    
    self.label2.textColor=[UIColor whiteColor];
    self.label2.textAlignment=NSTextAlignmentCenter;

    self.label1.textColor=[UIColor whiteColor];
    self.label1.textAlignment=NSTextAlignmentCenter;

    self.label3.textColor=[UIColor whiteColor];
    self.label3.textAlignment=NSTextAlignmentCenter;
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 Handles dimissal of screen
 */
- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
 The action corresponding to button1 press
 Allows the user to booking again
 or if the time of booking has not passed then shows the user the map view
 */
- (IBAction)buttonAction1:(id)sender {
    
    
    if (self.bookingData.bookingOccurred) {
       
        if (self.bookingData.active) {
            
            if ([self.delegate respondsToSelector:@selector(bookAgainWithSalon:)]) {
                [self.delegate bookAgainWithSalon:self.bookingData.bookingSalon];
            }
        }
        
    }else{
        
        GetDirectionsViewController *destinationViewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
        destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
        destinationViewController.transitioningDelegate = self;
        destinationViewController.salonMapData = self.bookingData.bookingSalon;
        
        [self presentViewController:destinationViewController animated:YES completion:NULL];
    }

}

/*
 Handles the action of button2
 if the date has passed then allow the user to review
 else allow the user to call
 */
- (IBAction)buttonAction2:(id)sender {
    
    if (self.bookingData.bookingOccurred){
        SalonReviewViewController *viewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"SalonReviewController"];
        viewController.modalPresentationStyle = UIModalPresentationCustom;
        viewController.transitioningDelegate = self;
        
        
        viewController.confirmID=self.bookingData.bookingId;
        viewController.booking=self.bookingData;
        viewController.myDelegate=self;
        
        [self presentViewController:viewController animated:YES completion:nil];
        
    }
    else{
        
        NSLog(@"Phone %@",self.bookingData.bookingSalon.salon_landline);
        if (self.bookingData.bookingSalon.salon_landline.length >=6) {
            [WSCore makePhoneCallWithNumber:self.bookingData.bookingSalon.salon_landline withViewController:self AndWithSalonName:self.bookingData.bookingSalon.salon_name];
        }
        else{
            [UIView showSimpleAlertWithTitle:@"No number provided." message:@"Please contact WhatSalon for further assistance." cancelButtonTitle:@"OK"];
        }
        
    }

}

/*
 Handles the actions of button 3
 Presents the social page
 */
- (IBAction)buttonAction3:(id)sender {
    
    if(self.bookingData.bookingOccurred){
        SocialShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"socialVC"];
        shareVC.modalPresentationStyle=UIModalPresentationCustom;
        shareVC.transitioningDelegate=self;
        shareVC.messageToShare= [NSString stringWithFormat: @"I just booked an appointment for an %@ in %@ using WhatSalon.",self.bookingData.servceName,self.bookingData.bookingSalon.salon_name];
        [self presentViewController:shareVC animated:YES completion:nil];
    }
    else{
        NSLog(@"Email");
        
        UIAlertView * cancelAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:@"If you cancel a booking your account will still be charged the booking fee. Do you wish to cancel?" delegate:self cancelButtonTitle:@"No, it's OK" otherButtonTitles:@"Yes, Cancel Booking", nil];
        cancelAlert.tag=701;
        [cancelAlert show];
        
    
    }
    
}


#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    //reurn new instance of dismiss controller
    return [[DismissDetailTransition alloc] init];
}
@end
