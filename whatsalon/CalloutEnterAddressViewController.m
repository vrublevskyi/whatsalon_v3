//
//  CalloutEnterAddressViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 08/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CalloutEnterAddressViewController.h"
#import "FUITextField.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface CalloutEnterAddressViewController ()<UITextFieldDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet FUITextField *addressTextfield;
@property (weak, nonatomic) IBOutlet FUITextField *appartmentNo;
@property (weak, nonatomic) IBOutlet FUITextField *zipCode;
@property (weak, nonatomic) IBOutlet UITextView *addressTextView;
@property (weak, nonatomic) IBOutlet UITextView *appartmentNotes;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextView *appointmentNotes;

@property (weak, nonatomic) IBOutlet FUITextField *phoneNumber;
- (IBAction)save:(id)sender;

@property (nonatomic) UILabel *addressNotesPlaceholder;
@property (nonatomic) UILabel * appointmentNotesPlaceholder;
@end

@implementation CalloutEnterAddressViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=kBackgroundColor;
   self.automaticallyAdjustsScrollViewInsets = NO;
    
    [WSCore nonTransparentNavigationBarOnView:self];
    
    NSMutableArray * textfields = [[NSMutableArray alloc] initWithObjects:self.addressTextfield,self.appartmentNo,self.zipCode,self.phoneNumber, nil];
    for (FUITextField * tf in textfields) {
     
        tf.font = [UIFont systemFontOfSize:15.0f];
        tf.backgroundColor = [UIColor clearColor];
        tf.edgeInsets = UIEdgeInsetsMake(4.0f, 15.0f, 4.0f, 15.0f);
        tf.textFieldColor = [UIColor whiteColor];
        tf.borderColor = [UIColor concreteColor];
        tf.borderWidth = 1.5f;
        tf.cornerRadius = kCornerRadius;
        //tf.delegate=self;

    }
    
    self.addressTextView.delegate=self;
    /*
    self.addressTextfield.nextTextField =self.appartmentNo;
    self.appartmentNo.nextTextField=self.zipCode;
    self.zipCode.nextTextField=self.phoneNumber;
    self.phoneNumber.nextTextField=nil;
    self.phoneNumber.nextTextView=self.addressTextView;
    
    self.addressTextView.delegate=self;
    self.appartmentNotes.delegate=self;
    self.addressTextView.returnKeyType=UIReturnKeyNext;
    self.appartmentNotes.returnKeyType=UIReturnKeyDone;
    */
   
    self.addressNotesPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.addressTextView.frame.size.width, 50)];
    self.addressNotesPlaceholder.font = self.addressTextfield.font;
    self.addressNotesPlaceholder.text = @"Parking instructions, appartment or room number, etc.";
    self.addressNotesPlaceholder.textColor=[UIColor asbestosColor];
    self.addressNotesPlaceholder.alpha=0.4;
    self.addressNotesPlaceholder.numberOfLines=2;
    self.addressTextView.text=@"";
    [self.addressTextView addSubview:self.addressNotesPlaceholder];
    
    self.appointmentNotesPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.addressTextView.frame.size.width, 50)];
    self.appointmentNotesPlaceholder.font = self.addressTextfield.font;
    self.appointmentNotesPlaceholder.text = @"Parking instructions, appartment or room number, etc.";
    self.appointmentNotesPlaceholder.textColor=[UIColor asbestosColor];
    self.appointmentNotesPlaceholder.alpha=0.4;
    self.appointmentNotesPlaceholder.numberOfLines=2;
    self.appointmentNotes.text=@"";
    [self.appointmentNotes addSubview:self.appointmentNotesPlaceholder];
    
     self.addressTextView.layer.borderColor=[UIColor concreteColor].CGColor;
    self.addressTextView.layer.cornerRadius=kCornerRadius;
    self.appointmentNotes.layer.borderColor=[UIColor concreteColor].CGColor;
    self.appointmentNotes.layer.cornerRadius=kCornerRadius;
    
    self.appointmentNotes.delegate=self;
    self.appartmentNotes.delegate=self;
}

/*
- (BOOL)textFieldShouldReturn:(FUITextField *)theTextField {
    NSLog(@"TF");
    UITextField *next = theTextField.nextTextField;
    UIView * nextView = theTextField.nextTextView;
    if (next) {
        [next becomeFirstResponder];
    }
    else if(nextView){
        [nextView becomeFirstResponder];
    }else{
        [theTextField resignFirstResponder];
    }
 
    
    return NO;
}
 */
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSLog(@"TV");
    
    if (textView==self.addressTextView &&text.length!=0) {
        self.addressNotesPlaceholder.hidden=YES;
    }
    else if (textView==self.addressTextView &&text.length==0){
        self.addressNotesPlaceholder.hidden=NO;
    }
    else if (textView==self.appointmentNotes && text.length!=0){
        self.appointmentNotesPlaceholder.hidden=YES;
    }
    else if(textView==self.appointmentNotes && text.length==0){
        self.appointmentNotesPlaceholder.hidden=NO;

    }
    NSRange resultRange = [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet] options:NSBackwardsSearch];
    if ([text length] == 1 && resultRange.location != NSNotFound) {
        if (textView == self.addressTextView) {
            
        }
        [textView resignFirstResponder];
        
        return NO;
    }
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if (textView==self.addressTextView) {
       
        self.addressTextView.layer.borderWidth=1.5;
        
        self.appointmentNotes.layer.borderWidth=0.0;
        
    }else{
        self.addressTextView.layer.borderWidth=0.0;
        
        self.appointmentNotes.layer.borderWidth=1.5;
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(id)sender {
    NSLog(@"Save");
    if ([self.delegate respondsToSelector:@selector(didUpdateAddress:)]) {
        NSLog(@"Responds to delegate");
        NSString * address = [NSString stringWithFormat:@"%@%@",self.addressTextfield.text,self.appartmentNo.text];
        [self.delegate didUpdateAddress:address];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
