//
//  OpeningDay.m
//  whatsalon
//
//  Created by Graham Connolly on 13/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "OpeningDay.h"

@implementation OpeningDay


-(instancetype) initWithDayOfWeek:(NSString *)day_of_week WithStartTime:(NSString *)startTime WithEndTime:(NSString *)endTime WithDayName:(NSString *)day_name AndIsOpene:(BOOL)isOpened{
    
    self=[super init];
    if (self) {
        self.dayOfWeek=day_of_week;
        self.startTime =startTime;
        self.endTime=endTime;
        self.dayName=day_name;
        self.isOpened=isOpened;
        
                
    }
    return self;
}

+(instancetype) openingDayWithDayOfWeek:(NSString *)day_of_week WithStartTime:(NSString *)startTime WithEndTime:(NSString *)endTime WithDayName:(NSString *)day_name AndIsOpene:(BOOL)isOpened{
    
    return [[self alloc] initWithDayOfWeek:day_of_week WithStartTime:startTime WithEndTime:endTime WithDayName:day_name AndIsOpene:isOpened];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.dayOfWeek = [aDecoder decodeObjectForKey:@"day_of_week"];
        self.startTime = [aDecoder decodeObjectForKey:@"startTime"];
        self.endTime = [aDecoder decodeObjectForKey:@"endTime"];
        self.dayName = [aDecoder decodeObjectForKey:@"dayName"];
        self.isOpened = [aDecoder decodeBoolForKey:@"isOpened"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.dayOfWeek forKey:@"day_of_week"];
    [aCoder encodeObject:self.startTime forKey:@"start_time"];
    [aCoder encodeObject:self.endTime forKey:@"endTime"];
    [aCoder encodeObject:self.dayName forKey: @"dayName"];
    [aCoder encodeBool:self.isOpened forKey: @"isOpened"];
  
}

@end
