//
//  InspireImage.m
//  whatsalon
//
//  Created by Graham Connolly on 03/07/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "InspireImage.h"

@implementation InspireImage

- (id) initWithImageName: (NSString *) url{
    self = [super init];
    
    if (self) {
        self.imageName = url;
    }
    return self;
}

+(id) inspireImageWithImageName :(NSString *) url{
    return [[self alloc]  initWithImageName:url];
}

-(instancetype)initWithStyle_id:(NSString *)s_id{
    self = [super init];
    if (self) {
        self.style_id=s_id;
    }
    return self;
}

+(instancetype) inspireWithStyle_id:(NSString *)s_id{
    return [[self alloc] initWithStyle_id:s_id];
}

-(NSString *)fetchFullAddress{
    
    return [NSString stringWithFormat:@"%@,%@,%@",self.salon_address_1,self.salon_address_2,self.salon_address_3 ];
}
@end
