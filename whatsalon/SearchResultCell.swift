//
//  SearchResultCell.swift
//  whatsalon
//
//  Created by admin on 10/5/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit
import Reusable
import Alamofire
import AlamofireImage


class SearchResultCell: UITableViewCell, NibReusable {

    //MARK: -Outlets
    //TODO: check why gradient not working correct without width
    @IBOutlet private var salonImageView: UIImageView!
    @IBOutlet private var saloncityLabel: UILabel!
    @IBOutlet private var salonAddressLabel: UILabel!
    @IBOutlet private var salonNameLabel: UILabel!
    
    @IBOutlet private var gradientView: UIView! {
        didSet {
            Gradient.addAppBlueToPurpleHorizontalGradient(on: self.gradientView)
        }
    }
    
    //MARK: - Properties
    var name: String? {
        didSet {
            salonNameLabel.text = name
        }
    }
    var address: String? {
        didSet {
            salonAddressLabel.text = address
        }
    }
    var city: String? {
        didSet {
            saloncityLabel.text = city
        }
    }
    var imageURL: String? {
        didSet {
            salonImageView?.af_setImage(withURL: NSURL(string: imageURL ?? "") as! URL)
        }
    }
}
