//
//  SearchMessageView.h
//  whatsalon
//
//  Created by Graham Connolly on 03/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchMessageView : UIView

/*! @brief represents the search title label. */
@property (strong, nonatomic) IBOutlet UILabel *searchTitle;

/*! @brief represents the search text label. */
@property (strong, nonatomic) IBOutlet UILabel *searchText;

/*! @brief represents the search image image view. */
@property (strong, nonatomic) IBOutlet UIImageView *searchImage;

/*! @brief represents the view for holding the content. */
@property (strong, nonatomic) IBOutlet UIView *view;

@end
