//
//  PresentDetailTransition.h
//  whatsalon
//
//  Created by Graham Connolly on 08/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

/*!
 @header PresentDetailTransition.h
  
 @brief This is the header file for the PresentingDetailTransition..
  
 This file is for a custom fade transition.
  
 @author Graham Connolly
 @copyright  2014 What Applications Ltd.
 @version    -
 */

#import <Foundation/Foundation.h>

@interface PresentDetailTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end
