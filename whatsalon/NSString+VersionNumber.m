//
//  NSString+VersionNumber.m
//  whatsalon
//
//  Created by Graham Connolly on 22/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "NSString+VersionNumber.h"

@implementation NSString (VersionNumber)

- (NSString *)shortenedVersionNumberString {
    static NSString *const unnecessaryVersionSuffix = @".0";
    NSString *shortenedVersionNumber = self;
    
    while ([shortenedVersionNumber hasSuffix:unnecessaryVersionSuffix]) {
        shortenedVersionNumber = [shortenedVersionNumber substringToIndex:shortenedVersionNumber.length - unnecessaryVersionSuffix.length];
    }
    
    return shortenedVersionNumber;
}
@end
