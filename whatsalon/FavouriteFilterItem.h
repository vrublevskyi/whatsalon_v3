//
//  FavouriteFilterItem.h
//  whatsalon
//
//  Created by Graham Connolly on 18/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FavouriteFilterItem : NSObject

/*! @brief represents the type of the favourite item. */
@property (nonatomic) NSString * type;

/*! @brief represents the total number of favourites of this type. */
@property (nonatomic) NSString * number;

/*! @brief represents the image name. */
@property (nonatomic) NSString *imageName;

/*! @brief represents the favourite id. */
@property (nonatomic) int fav_id;
@end
