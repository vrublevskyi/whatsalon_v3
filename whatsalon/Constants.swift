//
//  Constants.swift
//  whatsalon
//
//  Created by admin on 10/5/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit

struct LayoutDefaults {

    //MARK: - Colors

    //Default pink color
    static let defPinkColor = UIColor(red: 182/255, green: 65/255, blue: 153/255, alpha: 1)

}
