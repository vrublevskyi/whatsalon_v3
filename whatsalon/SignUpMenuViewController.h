//
//  SignUpMenuViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 19/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpMenuViewController : UIViewController

@property (nonatomic) BOOL isSignUp;
@property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
- (IBAction)facebookSignUp:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *emailSignUp;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (weak, nonatomic) IBOutlet UIView *emailBtn;
@end
