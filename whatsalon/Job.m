//
//  Job.m
//  whatsalon
//
//  Created by Graham Connolly on 05/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "Job.h"

@implementation Job

-(instancetype)initWithConfirmID : (NSString *)c_id{
    self = [super init];
    if (self) {
        self.confirm_ID=c_id;
       
    }
    return self;
}

+(instancetype) jboWithConfirmID: (NSString *)c_id{
    return [[self alloc] initWithConfirmID:c_id];
}
@end
