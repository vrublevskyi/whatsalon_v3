//
//  CategorySingleton.h
//  whatsalon
//
//  Created by Graham Connolly on 30/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategorySingleton : NSObject{
    
    NSMutableArray * completeArray;
}

+(instancetype) getInstance;

-(void)setCategories;

-(NSMutableArray*)getCategories;

//TODO: Remove after websummit
-(NSMutableArray *)getSummitCategories;

@end
