//
//  TherapistTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 07/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TherapistCellDelegate <NSObject>

@optional
-(void)showBio: (NSString *) name;
-(void)showLegend;

@end
@interface TherapistTableViewCell : UITableViewCell

@property (nonatomic) id<TherapistCellDelegate> delegate;
-(void)setCellImageWithURL: (NSString*) url;
@property (nonatomic) UIImageView *therapistBackgroundImage;
@property (nonatomic) UILabel *timeLabel;
@property (nonatomic) UILabel *priceLabel;
@property (nonatomic)  UILabel *nameLabel;
@property (nonatomic) UIImageView *favImageView;
@property (nonatomic) UILabel * selectFavLabel;

@end
