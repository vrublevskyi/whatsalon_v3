//
//  GCCameraViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 12/10/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//

#import "GCCameraViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "MCCamFocusSquare.h"

@interface GCCameraViewController ()<UIGestureRecognizerDelegate>
@property (nonatomic) AVCaptureSession * captureSession;
@property (nonatomic) AVCaptureStillImageOutput * stillImageOutput;
@property (nonatomic) UIImageView *photoImageView;
@property (nonatomic) MCCamFocusSquare * camFocus;
@property (nonatomic) UIButton * retakePhoto;
@property (nonatomic) UIButton * saveButton;
@property (nonatomic) UIImage * imageToSave;
@property (nonatomic) UIButton * captureButton;
@property (nonatomic) UIImageView * cameraViewImageView;
@property (nonatomic) BOOL isFrontFace;
@property (nonatomic) UILabel * tapToFocusLabel;

@end

@implementation GCCameraViewController


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:touch.view];
    [self focus:touchPoint];
    
    if (self.camFocus)
    {
        [self.camFocus removeFromSuperview];
    }
    if ([touch view] == self.frameForCapture)
    {
        self.camFocus = [[MCCamFocusSquare alloc]initWithFrame:CGRectMake(touchPoint.x-40, touchPoint.y-40, 80, 80)];
        [self.camFocus setBackgroundColor:[UIColor clearColor]];
        [self.frameForCapture addSubview:self.camFocus];
        [self.camFocus setNeedsDisplay];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:1.5];
        [self.camFocus setAlpha:0.0];
        [UIView commitAnimations];
    }
}

- (void) focus:(CGPoint) aPoint;
{
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [captureDeviceClass defaultDeviceWithMediaType:AVMediaTypeVideo];
        if([device isFocusPointOfInterestSupported] &&
           [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            double screenWidth = screenRect.size.width;
            double screenHeight = screenRect.size.height;
            double focus_x = aPoint.x/screenWidth;
            double focus_y = aPoint.y/screenHeight;
            if([device lockForConfiguration:nil]) {
                [device setFocusPointOfInterest:CGPointMake(focus_x,focus_y)];
                [device setFocusMode:AVCaptureFocusModeAutoFocus];
                if ([device isExposureModeSupported:AVCaptureExposureModeAutoExpose]){
                    [device setExposureMode:AVCaptureExposureModeAutoExpose];
                }
                [device unlockForConfiguration];
            }
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.frameForCapture = [[UIView alloc] initWithFrame:self.view.frame];
    
    [self.view addSubview:self.frameForCapture];
    self.photoImageView = [[UIImageView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:self.photoImageView];
    
    
    
    self.photoImageView.contentMode  = UIViewContentModeScaleAspectFill;
    NSLog(@"Camera");
    //[MCCore hideStatusBar:YES];
   // [MCCore flatNavBarOnView:self WithColor:[UIColor amethystColor]];
    self.view.backgroundColor=kBackgroundColor;
    
    self.frameForCapture.backgroundColor = [UIColor blackColor];;
    
    UIButton * cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 20, 44, 44)];
    [cancelButton setImage:kCancelButtonImage forState:UIControlStateNormal];
    cancelButton.imageView.image = [cancelButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    cancelButton.tintColor = [UIColor whiteColor];
    [cancelButton addTarget:self action:@selector(dismissViewCamera) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelButton];
    
    [WSCore transparentNavigationBarOnView:self];
    
    [WSCore hideStatusBar:YES];
    
    self.retakePhoto = [[UIButton alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height-45, (self.view.frame.size.width/2)-15, 40)];
    [self.retakePhoto setTitle:@"Retake Photo" forState:UIControlStateNormal];
    [self.view addSubview:self.retakePhoto];
    [self.retakePhoto addTarget:self action:@selector(cameraRetake) forControlEvents:UIControlEventTouchUpInside];
    [self.retakePhoto setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.6]];
    [self.retakePhoto setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    self.retakePhoto.layer.cornerRadius=kCornerRadius;
    self.retakePhoto.layer.masksToBounds=YES;
    self.retakePhoto.hidden=YES;
   
    UIView * blacklayer = [[UIView alloc] initWithFrame:self.view.frame];
    blacklayer.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    [self.photoImageView addSubview:blacklayer];
   
    
    self.saveButton = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)+5, self.view.frame.size.height-45, (self.view.frame.size.width/2)-15, 40)];
    [self.saveButton setTitle:@"Save" forState:UIControlStateNormal];
    [self.view addSubview:self.saveButton];
    [self.saveButton addTarget:self action:@selector(saveImage) forControlEvents:UIControlEventTouchUpInside];
    [self.saveButton setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.6]];
    [self.saveButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    self.saveButton.layer.cornerRadius=kCornerRadius;
    self.saveButton.layer.masksToBounds=YES;
    self.saveButton.hidden=YES;
    
    //capture
    self.captureButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-50, (self.view.frame.size.width/2)-15, 40)];
    [self.captureButton setTitle:@"Capture" forState:UIControlStateNormal];
    [self.view addSubview:self.captureButton];
    [self.captureButton addTarget:self action:@selector(capture:) forControlEvents:UIControlEventTouchUpInside];
    [self.captureButton setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.6]];
    [self.captureButton setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
    self.captureButton.layer.cornerRadius=kCornerRadius;
    self.captureButton.layer.masksToBounds=YES;
    self.captureButton.hidden=NO;
    self.captureButton.center = CGPointMake(self.view.center.x, self.captureButton.center.y);
    
    UIButton * changeCameraView = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-55, 20, 44, 44)];
    [changeCameraView setImage:[UIImage imageNamed:@"CameraRotate"] forState:UIControlStateNormal];
    changeCameraView.imageView.image = [changeCameraView.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    changeCameraView.tintColor = [UIColor whiteColor];
    [changeCameraView addTarget:self action:@selector(switchCamera:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:changeCameraView];
  
    [self setUpCameraFeed];
    UILabel * tapToFocusLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-40-40, 120, 40)];
    tapToFocusLabel.text=@"Tap to Focus";
    [self.view addSubview:tapToFocusLabel];
    tapToFocusLabel.center = CGPointMake(self.view.center.x, tapToFocusLabel.center.y);
    tapToFocusLabel.textColor = [UIColor whiteColor];
    tapToFocusLabel.alpha=0.6;
    tapToFocusLabel.textAlignment = NSTextAlignmentCenter;
    self.tapToFocusLabel=tapToFocusLabel;
}

-(void)saveImage{
    if ([self.delegate respondsToSelector:@selector(cameraView:didFinishWithImage:)]) {
        
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate cameraView:self didFinishWithImage:self.imageToSave];
        }];
        
}
}
-(void)cameraRetake{
    self.photoImageView.image = nil;
    self.retakePhoto.hidden=YES;
    self.saveButton.hidden=YES;
    self.captureButton.hidden=NO;
    self.tapToFocusLabel.hidden=NO;
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [WSCore hideStatusBar:NO];
    
    self.photoImageView=nil;
    
    
}
-(void)dismissViewCamera{
    NSLog(@"Dismiss");
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    
    //[self setUpCameraFeed];
    
}
- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}


-(void)setUpCameraFeed{
    
    if (![self.captureSession isRunning]) {
        self.captureSession = [[AVCaptureSession alloc] init];
        [self.captureSession setSessionPreset:AVCaptureSessionPresetPhoto];
        AVCaptureDevice * inputDevice=nil;
        inputDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];//default device for taking background camera
        
        
        NSError *error;
        AVCaptureDeviceInput * deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:inputDevice error:&error];//& basically saves the error
        
        if ([self.captureSession canAddInput:deviceInput]) {
            NSLog(@"Can add Input");
            [self.captureSession addInput:deviceInput];
        }
        
        //live feed
        AVCaptureVideoPreviewLayer * videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
        videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill ;
        
        CALayer * rootLayer = [[self view] layer];
        rootLayer.masksToBounds=YES;
        
        CGRect frame = self.frameForCapture.frame;
        
        videoPreviewLayer.frame = frame;
        
        [rootLayer insertSublayer:videoPreviewLayer atIndex:0];
        
        
        self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        NSDictionary * outputSettings =[ [NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG,AVVideoCodecKey, nil];
        self.stillImageOutput.outputSettings = outputSettings;
        
        [self.captureSession addOutput:self.stillImageOutput];
        
        
        [self.captureSession startRunning];
        self.frameForCapture.backgroundColor=[UIColor clearColor];
        self.frameForCapture.userInteractionEnabled=YES;
        
    }
    
}

- (IBAction)capture:(id)sender {
    

    
    AVCaptureConnection * videoConntection = nil;
    
    for (AVCaptureConnection * connection in self.stillImageOutput.connections) {
        for (AVCaptureInputPort * port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                videoConntection = connection;
                break;
            }
            
        }
        if (videoConntection) {
            break;
        }
    }
    
    //async
    [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConntection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        
        if (imageDataSampleBuffer != NULL) {
            NSData * imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
            
            UIImage * image = [UIImage imageWithData:imageData];
            //added front camera flip
            if (self.isFrontFace) {
                UIImage* flippedImage = [UIImage imageWithCGImage:image.CGImage
                                                            scale:image.scale
                                                      orientation:UIImageOrientationLeftMirrored];
                image=flippedImage;
            }
            
            UIImage * savedImage = [self scaleImage:image ToSize:self.view.frame.size];
            self.imageToSave=savedImage;
            self.photoImageView.image = image;
            self.photoImageView.contentMode  = UIViewContentModeScaleAspectFill;
            self.retakePhoto.hidden=NO;
            self.saveButton.hidden=NO;
            self.captureButton.hidden=YES;
            self.tapToFocusLabel.hidden=YES;
                   }
    }];
}

- (UIImage *)scaleImage: (UIImage *) image ToSize:(CGSize)newSize {
    
    CGRect scaledImageRect = CGRectZero;
    
    CGFloat aspectWidth = newSize.width / image.size.width;
    CGFloat aspectHeight = newSize.height / image.size.height;
    CGFloat aspectRatio = MIN ( aspectWidth, aspectHeight );
    
    scaledImageRect.size.width = image.size.width * aspectRatio;
    scaledImageRect.size.height = image.size.height * aspectRatio;
    scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0f;
    scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0f;
    
    UIGraphicsBeginImageContextWithOptions( newSize, NO, 0 );
    [image drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
    
}


- (IBAction)switchCamera:(id)sender {
    //Change camera source
    if(self.captureSession)
    {
        //Indicate that some changes will be made to the session
        [self.captureSession beginConfiguration];
        
        //Remove existing input
        AVCaptureInput* currentCameraInput = [_captureSession.inputs objectAtIndex:0];
        [self.captureSession removeInput:currentCameraInput];
        
        //Get new input
        AVCaptureDevice *newCamera = nil;
        if(((AVCaptureDeviceInput*)currentCameraInput).device.position == AVCaptureDevicePositionBack)
        {
            if (!self.isFrontFace) {
                self.isFrontFace = YES;
            }
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionFront];
            //[self.cameraPositionBtn setTitle:@"Use Back" forState:UIControlStateNormal];
        }
        else
        {
            if (self.isFrontFace) {
                self.isFrontFace = NO;
            }
            newCamera = [self cameraWithPosition:AVCaptureDevicePositionBack];
           // [self.cameraPositionBtn setTitle:@"Use Front" forState:UIControlStateNormal];
            
        }
        
        //Add input to session
        NSError *err = nil;
        AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:newCamera error:&err];
        if(!newVideoInput || err)
        {
            NSLog(@"Error creating capture device input: %@", err.localizedDescription);
        }
        else
        {
            [self.captureSession addInput:newVideoInput];
        }
        
        //Commit all the configuration changes at once
        [self.captureSession commitConfiguration];
    }
    
}
// Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position) return device;
    }
    return nil;
}


@end
