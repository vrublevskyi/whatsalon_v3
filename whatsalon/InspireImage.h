//
//  InspireImage.h
//  whatsalon
//
//  Created by Graham Connolly on 03/07/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InspireImage : NSObject

@property (nonatomic) NSString * imageName;
@property (nonatomic) NSString * stylist;
@property (nonatomic) NSString * salon;
@property (nonatomic,assign) BOOL isBookable;


@property (nonatomic) double distance;
@property (nonatomic) BOOL is_favourite;
@property (nonatomic) NSString * salon_name;
@property (nonatomic) NSString * style_gender;
@property (nonatomic) NSString * style_id;
@property (nonatomic) NSString * image_name;
@property (nonatomic) NSString * style_type_id;
@property (nonatomic) NSString * style_type_name;
@property (nonatomic) NSString * stylist_name;

//salon obj
@property (nonatomic) NSString * salon_id;
@property (nonatomic) NSString * salon_des;
@property (nonatomic) NSString * salon_phone;
@property (nonatomic) NSString * salon_lat;
@property (nonatomic) NSString * salon_lon;
@property (nonatomic) NSString * salon_address_1;
@property (nonatomic) NSString * salon_address_2;
@property (nonatomic) NSString * salon_address_3;
@property (nonatomic) NSString * salon_city_name;
@property (nonatomic) NSString * salon_country_name;
@property (nonatomic) NSString * salon_landline;
@property (nonatomic) NSString * salon_image_url;
@property (nonatomic) NSString * salon_rating;
@property (nonatomic) NSString * salon_review;
@property (nonatomic) NSString *salon_tier;



//Designated Initializer
- (id) initWithImageName: (NSString *) url;

//Convenience Constructor
+(id) inspireImageWithImageName :(NSString *) url;

- (instancetype) initWithStyle_id : (NSString *)s_id;
+ (instancetype) inspireWithStyle_id: (NSString *)s_id;

-(NSString *)fetchFullAddress;
@end
