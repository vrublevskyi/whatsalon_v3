//
//  IsalonBookingDetails.h
//  whatsalon
//
//  Created by Graham Connolly on 21/12/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header IsalonBookingDetails.h
  
 @brief This is the header file for IsalonBookingDetails.h
  
 This file represents the iSalon details for making an iSalon booking.
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.3
 */
#import <Foundation/Foundation.h>

@interface IsalonBookingDetails : NSObject

/*! @brief represents the iSalon service id. */
@property (nonatomic) NSString * iSalon_service_id;

/*! @brief represents the iSalon salon id. */
@property (nonatomic) NSString * salon_id;

/*! @brief represents the iSalon specific staff id. */
@property (nonatomic) NSString * staff_id;//isalon specific staff id

/*! @brief represents the start time of the appointment. */
@property (nonatomic) NSString * start_time;//start time of the appointment


/*! @brief fetches an NSString of the booking description.
    @return NSString
 */
-(NSString *) fetchBookingDescription;
@end
