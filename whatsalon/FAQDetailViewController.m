//
//  FAQDetailViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 22/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "FAQDetailViewController.h"

@interface FAQDetailViewController ()

@end

@implementation FAQDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"FAQ's";
    self.view.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    
    NSString* sContent = [NSString stringWithFormat:@"<html><body><font face='Helvetica Neue,HelveticaNeue-Light'> %@ </font></body> </html>", self.faqDetailItem.content];
    [self.webview loadHTMLString:sContent baseURL:nil];
    self.webview.userInteractionEnabled=YES;
    self.webview.dataDetectorTypes=UIDataDetectorTypeLink;
    self.webview.dataDetectorTypes=UIDataDetectorTypePhoneNumber;
    self.webview.backgroundColor = [UIColor clearColor];
    [self.webview setOpaque:YES];
    

}

@end
