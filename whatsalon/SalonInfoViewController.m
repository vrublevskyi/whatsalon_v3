//
//  SalonInfoViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 08/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonInfoViewController.h"
#import "SalonService.h"

@interface SalonInfoViewController ()


@property (weak, nonatomic) IBOutlet UITextView *bioTextView;
@property (weak, nonatomic) IBOutlet UILabel *bioSalonNameLabel;

//hours
@property (nonatomic) NSArray * workingDaysArray;
@property (nonatomic) NSArray * workingTimesArray;

@property (nonatomic,assign) BOOL isPriceShowing;

@property (nonatomic) NSMutableArray * priceList;
@property (nonatomic) NSMutableDictionary * anotherMainDictionary;
@property (nonatomic) NSArray * keyArray;



@end

@implementation SalonInfoViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.priceList = [[NSMutableArray alloc] init];
    [self.priceList addObjectsFromArray:self.prices];
    
    self.bioTextView.text = self.salonInfoData.salon_description;
    self.bioSalonNameLabel.text = self.salonInfoData.salon_name;
    self.bioTextView.font = [UIFont systemFontOfSize:15.0f];
    if (self.salonInfoData.salon_description.length<5) {
        self.bioTextView.text = @"No description supplied.";
        self.bioTextView.textColor = [UIColor lightGrayColor];
        self.bioTextView.font = [UIFont fontWithName:@"Helvetica-Neue" size:14.0f];
    }
    
    self.view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.60];
    self.pricesTableView.delegate =self;
    self.pricesTableView.dataSource = self;
    
   
    //hours
    self.workingDaysArray = [[NSArray alloc] initWithObjects:@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",@"Sunday", nil];
    self.workingTimesArray = [[NSArray alloc] initWithObjects:@"9a.m - 4p.m",@"9a.m - 5p.m",@"9a.m - 5p.m",@"9a.m - 5p.m",@"9a.m - 4p.m",@"9a.m - 5.30p.m",@"Closed", nil];
    
    
    self.isPriceShowing=NO;
    self.bioView.hidden=NO;
    
    //view for dismiss the controller
    UIView * dismissView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, (self.view.bounds.size.height - self.pricesTableView.bounds.size.height)- self.selectionHeaderView.bounds.size.height)];
    dismissView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:dismissView];
    
    UITapGestureRecognizer * dismissTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissTapView)];
    dismissTap.numberOfTapsRequired=1;
    [dismissView addGestureRecognizer:dismissTap];


    self.anotherMainDictionary = [[NSMutableDictionary alloc] init];
    
  //remove all header objects
    for (int i=0; i<self.priceList.count; i++) {
        SalonService * phorestService = [self.priceList objectAtIndex:i];
        if (phorestService.isHeader) {
            [self.priceList removeObjectAtIndex:i];
        }
    }
    
   
   
    for (SalonService * service in self.priceList) {
        NSMutableArray * array = [self.anotherMainDictionary valueForKey:service.category_name];
        if (array) {
            [array addObject:service];
            [self.anotherMainDictionary setValue:array forKey:service.category_name];
        } else {
            NSMutableArray * newArray = [NSMutableArray arrayWithObject:service];
            [self.anotherMainDictionary setValue:newArray forKey:service.category_name];
        }
    }
    
    
    self.keyArray = [[NSArray alloc] init];
    self.keyArray = [self.anotherMainDictionary allKeys];
}

-(void)dismissTapView{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - UITableView datasource & delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (self.isPriceShowing) {
        return self.keyArray.count;

    }
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.isPriceShowing) {
        
        NSMutableArray * array = [self.anotherMainDictionary objectForKey:[self.keyArray objectAtIndex:section]];
            return array.count;
        
    }else{
        
        return self.workingDaysArray.count;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (self.isPriceShowing) {
        UILabel *myLabel = [[UILabel alloc] init];
        myLabel.frame = CGRectMake(15, 0, 320, 20);
        myLabel.backgroundColor = [UIColor clearColor];
        myLabel.font = [UIFont systemFontOfSize:15.0];
        
        NSString *key = [self.keyArray objectAtIndex:section];
        myLabel.text = key;
        
        UIView *headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor colorWithRed:245.0/255 green:245.0/255 blue:247.0/255 alpha:1.0];
        [headerView addSubview:myLabel];
        
        return headerView;
    }
    
    return nil;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellId = @"Cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (self.isPriceShowing) {
        
        NSMutableArray * mArray =  [self.anotherMainDictionary objectForKey:[self.keyArray objectAtIndex:indexPath.section]];
        SalonService * pServ = mArray[indexPath.row];
        cell.textLabel.text = pServ.service_name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"From €%@",pServ.price];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:18];
        
        cell.userInteractionEnabled=NO;
        tableView.scrollEnabled=YES;
    }
    else{
        
        
        cell.textLabel.text = [self.workingDaysArray objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [self.workingTimesArray objectAtIndex:indexPath.row];
        cell.userInteractionEnabled = NO;
        
        if (IS_iPHONE4) {
            tableView.scrollEnabled=YES;
        }else{
            
            tableView.scrollEnabled=NO;
        }
        
    }
    
    
    
    return cell;
}

- (IBAction)segmentChangValue:(id)sender {
    
    if (self.segmentedController.selectedSegmentIndex == 0) {
        
      
        self.bioView.hidden=NO;
        
    }
    if (self.segmentedController.selectedSegmentIndex == 1) {
       
        self.bioView.hidden=YES;
        self.isPriceShowing=NO;
        [self.pricesTableView reloadData];
       
    }
    if (self.segmentedController.selectedSegmentIndex ==2) {
      
        self.bioView.hidden=YES;
        self.isPriceShowing=YES;
        [self.pricesTableView reloadData];
      
    }

}
@end
