//
//  SearchResultsTableViewMainCell.h
//  SearchResultsPageExample
//
//  Created by Graham Connolly on 05/05/2014.
//  Copyright (c) 2014 Graham Connolly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TYMProgressBarView.h"

@interface SearchResultsTableViewMainCell : UITableViewCell
@property (nonatomic,assign) BOOL showInfo;
@property (nonatomic,strong) UILabel * hoursLabel;
@property (nonatomic,strong) UILabel * minsLabel;
@property (nonatomic,strong) UILabel * titleLabel;
@property (nonatomic) UILabel *addressLabel;
@property (nonatomic) UILabel * reviewsLabel;
@property (nonatomic) UILabel * priceLabel;
@property (nonatomic) UIButton * bookButton;
@property (nonatomic) TYMProgressBarView * progressBar;
@property (nonatomic,strong) UILabel * progressBarSecondsLabel;
@property (nonatomic,strong) UIImageView * starImageView;
@property (nonatomic,strong) UIImageView * dotsImageView;
@property (nonatomic,strong) UIView * infoView;
@property (nonatomic,strong) UITextView * textInfo;
@property (nonatomic,strong) UIImageView * salonImageV;
@property (nonatomic,strong) UIColor * textColor;
@property (nonatomic) NSString * job_confirm_id;
@property (nonatomic) NSString * job_online_payment;
@property (nonatomic) NSString * job_price;

-(void)methods;

-(void)stopMethods;


@end
