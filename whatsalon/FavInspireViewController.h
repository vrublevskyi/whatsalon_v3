//
//  FavInspireViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavInspireViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *overlayBlockView;
@property (weak, nonatomic) IBOutlet UIView *viewForFavNavBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;


- (IBAction)changeCollectionData:(id)sender;
- (IBAction)showMenu:(id)sender;

@end
