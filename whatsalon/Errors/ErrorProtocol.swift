import UIKit
import Alamofire

protocol ErrorProtocol: Error {
    var statusCode: Int { get }
    var description: String { get }
}

extension ErrorProtocol {
    @discardableResult
    func log(function: String = #function, file: String = #file, line: Int = #line) -> Self {
        return self
    }
}
