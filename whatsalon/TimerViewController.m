//
//  TimerViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 29/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "TimerViewController.h"
#import "User.h"
#import "UIView+AlertCompatibility.h"
#import <QuartzCore/QuartzCore.h>
#import "MRProgress.h"
#import "GCNetworkManager.h"
#import "DiscoveryViewController.h"
#import <Crashlytics/Crashlytics.h>
#import "AFHTTPRequestOperationManager.h"



@interface TimerViewController ()<UITextFieldDelegate,GCNetworkManagerDelegate,UITextFieldDelegate>

@property (nonatomic) int seconds_to_launch;
@property (weak, nonatomic) IBOutlet UILabel *slogan;
@property (nonatomic) NSURLSessionDataTask * dataTask;
@property (weak, nonatomic) IBOutlet UILabel *days;
@property (weak, nonatomic) IBOutlet UILabel *hrs;
@property (weak, nonatomic) IBOutlet UILabel *mins;
@property (weak, nonatomic) IBOutlet UILabel *seconds;
@property (nonatomic) UIAlertController *alertController;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (nonatomic)  CGRect keyboardRect;

- (IBAction)refresh:(id)sender;
@property (nonatomic) GCNetworkManager * inviteCodeNetworkManager;
@property (nonatomic) GCNetworkManager * secondsToLaunchNetworkManager;

@property (nonatomic) GCNetworkManager * emailSignUp;

@property (nonatomic) UITextField * inviteCodeTF;
@end

@implementation TimerViewController

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
   // self.scrollView.contentOffset = CGPointZero;//hack needed to get scroll view to work
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.emailView.backgroundColor=[UIColor clearColor];
    
    self.inviteCodeNetworkManager = [[GCNetworkManager alloc] init];
    self.inviteCodeNetworkManager.delegate =self;
    self.inviteCodeNetworkManager.parentView = self.view;
    
    self.secondsToLaunchNetworkManager = [[GCNetworkManager alloc] init];
    self.secondsToLaunchNetworkManager.delegate = self;
    self.secondsToLaunchNetworkManager.parentView = self.view;
    self.countdownLabel.alpha=0.0;
    
    self.slogan.alpha=0.0;
    self.days.alpha=0;
    self.hrs.alpha=0;
    self.mins.alpha=0;
    self.seconds.alpha=0;
    
    self.vip_btn.buttonColor = [UIColor cloudsColor];
    self.vip_btn.shadowColor = [UIColor silverColor];
    self.vip_btn.shadowHeight = 1.0f;
    self.vip_btn.cornerRadius = 3.0f;
    [self.vip_btn setTitleColor:[UIColor silverColor] forState:UIControlStateNormal];
    [self.vip_btn setTitle:@"Already have a VIP Code?" forState:UIControlStateNormal];
    
    self.email_btn.buttonColor = kWhatSalonBlue;
    self.email_btn.shadowColor = kWhatSalonBlueShadow;
    self.email_btn.shadowHeight = 1.0f;
    self.email_btn.cornerRadius = 3.0f;
    [self.email_btn setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
    [self.email_btn setTitle:@"Submit Email" forState:UIControlStateNormal];
    
  
    self.emailTextfield.backgroundColor = [UIColor clearColor];
    self.emailTextfield.edgeInsets = UIEdgeInsetsMake(4.0f, 15.0f, 4.0f, 15.0f);
    self.emailTextfield.textFieldColor = [UIColor whiteColor];
    self.emailTextfield.borderColor = [UIColor silverColor];
    self.emailTextfield.borderWidth = 0.5f;
    self.emailTextfield.cornerRadius = 3.0f;
    self.emailTextfield.placeholder=@"Enter Email Adress for early access";
    self.emailTextfield.keyboardType=UIKeyboardTypeEmailAddress;
    self.emailTextfield.autocapitalizationType=UITextAutocapitalizationTypeNone;
    self.emailTextfield.autocorrectionType=UITextAutocorrectionTypeNo;
    self.emailTextfield.returnKeyType=UIReturnKeyDone;

    self.emailTextfield.delegate=self;
 
    self.refreshButton.hidden=YES;
    
    UIImage *image = [[UIImage imageNamed:@"refresh_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.refreshButton setImage:image forState:UIControlStateNormal];

    [self.refreshButton setTintColor:kWhatSalonBlue];
    
    self.logoImageView.image = [self.logoImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.logoImageView setTintColor:kWhatSalonBlue];
    
    self.emailSignUp = [[GCNetworkManager alloc] init];
    self.emailSignUp.parentView=self.view;
    self.emailSignUp.delegate=self;
   
    

    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField==self.emailTextfield) {
       
        self.topLayout.constant = +160;
        self.centerYLayoutCountdownTimer.constant= +160;
        [self.emailView setNeedsUpdateConstraints];
        [self.holderView setNeedsUpdateConstraints];
  
        if (IS_iPHONE4) {
            self.logoImageView.alpha=0.0;
        }
        [UIView animateWithDuration:0.5 animations:^{
            self.logoImageView.alpha=0.0;
            [self.emailView layoutIfNeeded];
            [self.holderView layoutIfNeeded];
            [self.logoImageView layoutIfNeeded];
        }];
        
        
        
    }
   
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField==self.emailTextfield) {
        self.topLayout.constant = 0;
       
        self.centerYLayoutCountdownTimer.constant= 0;
        [self.emailView setNeedsUpdateConstraints];
        [self.holderView setNeedsUpdateConstraints];
       
     
        if (IS_iPHONE4) {
            self.logoImageView.alpha=1.0;
        }else{
            [UIView animateWithDuration:0.8 animations:^{
                self.logoImageView.alpha=1.0;
            }];
        }
        
        [UIView animateWithDuration:0.5 animations:^{
            [self.emailView layoutIfNeeded];
            [self.holderView layoutIfNeeded];
            
        }];

    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.view endEditing:YES];
    if (textField==self.emailTextfield) {
        
        [self performEmailSubmission];
    }else{
        
        [self performSelector:@selector(submit:) withObject:nil];
    }
    

    
    return YES;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    self.countdownLabel.alpha=0.0;
    
    [self fetchSecondsToLaucnh];
    
    

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)WelcomeAlert {
    //"The invitation code is valid"
    
    
    if ([UIAlertController class]) {
        // use UIAlertController
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Welcome to WhatSalon!"
                                   message:@""
                                   preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alert animated:YES completion:nil];
        
        double delayInSeconds = 1.0;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:^{
                //[self performSelector:@selector(performSegue) withObject:nil afterDelay:0.3];
                [self performSelectorOnMainThread:@selector(performSegue) withObject:nil waitUntilDone:YES];
            }];
            
            
        });

    }
    else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Welcome to WhatSalon!" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
        alert.tag=1;
        [alert show];
        
        double delayInSeconds = 1.0;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alert dismissWithClickedButtonIndex:0 animated:YES];
            
            [self performSelectorOnMainThread:@selector(performSegue) withObject:nil waitUntilDone:YES];
            //[self performSelector:@selector(performSegue) withObject:nil afterDelay:0.3];
        });

    }
    
    
}


//test here
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary {
    
    if (manager==self.emailSignUp) {
        NSLog(@"JSON %@",jsonDictionary);
        [UIView showSimpleAlertWithTitle:@"Error" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
    }
    else if (manager==self.inviteCodeNetworkManager) {
        [UIView showSimpleAlertWithTitle:@"Oh Oh" message:[NSString stringWithFormat:@"%@.",jsonDictionary[@"message"]] cancelButtonTitle:@"OK"];
    }
    else if(manager==self.secondsToLaunchNetworkManager){
        [WSCore showServerErrorAlert];
        NSLog(@"Show refresh");
        self.refreshButton.hidden=NO;
    }
    
    
    
}

-(void)manager:(GCNetworkManager *)manager handleServerFailureWithError:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Handle server error");
        self.refreshButton.hidden=NO;
    });
}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    if (manager==self.emailSignUp) {
        NSLog(@"JSON %@",jsonDictionary);
        [UIView showSimpleAlertWithTitle:@"Thanks for applying we'll be in touch soon." message:nil cancelButtonTitle:@"OK"];
    }
    else if (manager==self.inviteCodeNetworkManager) {
        
        //add to default
        [self.timer invalidate];
        [[User getInstance]setInviteCodeToUsed ];
        [self.view endEditing:YES];
        [self WelcomeAlert];
    }
    
    else if (manager==self.secondsToLaunchNetworkManager) {
        self.seconds_to_launch = [jsonDictionary[@"message"] intValue];
        
        
        self.launchDate = [NSDate dateWithTimeIntervalSinceNow:self.seconds_to_launch];
        NSDate * currentDate = [NSDate date];
        
        if ([self.launchDate timeIntervalSinceNow]< [currentDate timeIntervalSinceNow]) {
            NSLog(@"Date has passed");
            [self performSelectorOnMainThread:@selector(performSegue) withObject:nil waitUntilDone:YES];
            
        }
        else{
            self.refreshButton.hidden=YES;
            NSLog(@"Date has not passed");
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLabel) userInfo:nil repeats:YES];
            self.countdownLabel.adjustsFontSizeToFitWidth=YES;
        }
        
       
    }
    
    
    
}

-(void)checkInviteCode: (NSString *) string{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kUse_Invite_URL]];
    NSString * params = [NSString stringWithFormat:@"invitation_code=%@",string];
    //params = [params stringByAppendingString:@"&only_check=1"];
    
    
    
    [self.inviteCodeNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
   }

-(void)performSegue{
    NSLog(@"CAlled perform Segue");
    
    /*
    [self performSegueWithIdentifier:@"timerToBrowse" sender:self];
    */
    
    /*
     Go to discovery page
     */
    
    [self performSegueWithIdentifier:@"discovery" sender:self];
    
}

-(void)updateLabel{
    
    if (self.countdownLabel.alpha==0.0) {
        self.vip_btn.hidden=NO;
    
        [UIView animateWithDuration:0.8 animations:^{
            self.slogan.alpha=1.0;
            self.days.alpha=1;
            self.hrs.alpha=1;
            self.mins.alpha=1;
            self.seconds.alpha=1;
            self.countdownLabel.alpha=1;
            
            self.vip_btn.alpha=1.0;
            
        }];
        
    }
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    int units = NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    NSDateComponents *components = [calendar components:units fromDate:[NSDate date] toDate:self.launchDate options:0];
    
    [self.countdownLabel setText:[NSString stringWithFormat:@"%ld : %ld : %ld : %ld",(long) [components day], (long)[components hour], (long)[components minute], (long)[components second]]];
    
    if (([components day]==0 && [components hour]==0&&[components minute]==0 && [components second]==0)|| [[User getInstance] hasUserUsedInviteCode]) {
        [self.timer invalidate];
        NSLog(@"Called");
        //[self performSegueWithIdentifier:@"timerToBrowse" sender:self];
        [self performSegue];
    }
}

-(void)fetchSecondsToLaucnh{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kCountdown_timer_URL]];

    [self.secondsToLaunchNetworkManager loadWithURL:url WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}
- (IBAction)vipAccess:(id)sender {
    
   
    
    if ([UIAlertController class]) {
        self.alertController = [UIAlertController
                                              alertControllerWithTitle:@"VIP Access"
                                              message:@"Lucky enough to be a VIP? Enter your 5 digit invite code below."
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        [self.alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
             textField.placeholder = @"Invite Code";
             textField.autocapitalizationType=UITextAutocapitalizationTypeAllCharacters;
             textField.autocorrectionType=UITextAutocorrectionTypeNo;
             textField.textAlignment=NSTextAlignmentCenter;
             textField.returnKeyType=UIReturnKeyDone;

            }];

        UIAlertAction * cancelButton = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Submit"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       self.inviteCodeTF= self.alertController.textFields.firstObject;
                                       [self submit:self];
                                       
                                       
                                   }];
        [self.alertController addAction:cancelButton];
        [self.alertController addAction:okAction];
        
        [self presentViewController:self.alertController animated:YES completion:nil];
        
        return;
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"VIP Access"
                                                        message:@"Lucky enough to be a VIP? Enter your 5 digit invite code below."
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Submit", nil];
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        UITextField *tf = [alert textFieldAtIndex:0];
        tf.placeholder = @"#####";
        tf.autocapitalizationType=UITextAutocapitalizationTypeAllCharacters;
       tf.autocorrectionType=UITextAutocorrectionTypeNo;
        alert.tag=223;
        [alert show];
    }
    
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex ==0) {
            
        }
    }
    else if(alertView.tag==223){
        if (buttonIndex!=[alertView cancelButtonIndex]) {
            UITextField *password = [alertView textFieldAtIndex:0];
            self.inviteCodeTF=password;
            NSLog(@"Password: %@", password.text);
            if (self.inviteCodeTF.text.length==0) {
                
                [UIView showSimpleAlertWithTitle:@"Oh Oh!" message:@"Invite Code cannot be empty." cancelButtonTitle:@"OK"];
                
                
            }
            else if (self.inviteCodeTF.text.length<=4){
                
                [UIView showSimpleAlertWithTitle:@"Oh Oh" message:@"Invite Code must be five characters long." cancelButtonTitle:@"OK"];
                
            }
            else{
                
                
                [self checkInviteCode:self.inviteCodeTF.text];
            }
 
        }
        

    }
}

- (IBAction)submit:(id)sender {
    
    
    if ([UIAlertController class]) {
        [self.alertController dismissViewControllerAnimated:YES completion:nil];
    }
    
    
    
    if (self.inviteCodeTF.text.length==0) {
        
        [UIView showSimpleAlertWithTitle:@"Oh Oh!" message:@"Invite Code cannot be empty." cancelButtonTitle:@"OK"];
        
        
    }
    else if (self.inviteCodeTF.text.length<=4){
        
        [UIView showSimpleAlertWithTitle:@"Oh Oh" message:@"Invite Code must be five characters long." cancelButtonTitle:@"OK"];
        
    }
    else{
        
        
        [self checkInviteCode:self.inviteCodeTF.text];
    }
    
}
 
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"timerToBrowse"]) {
       [self.view.layer removeAllAnimations];
        [self.timer invalidate];
        [[User getInstance] setInviteCodeToUsed];
        self.timer=nil;
    
    }
    else{
        self.countdownLabel.alpha=0.0;
        self.slogan.alpha=0.0;
        self.days.alpha=0;
        self.hrs.alpha=0;
        self.mins.alpha=0;
        self.seconds.alpha=0;
        [self.timer invalidate];
        self.timer=nil;
        [self.dataTask cancel];
        [self.navigationController setNavigationBarHidden:NO];
    }
    
    
    

}

- (IBAction)partnersAccess:(id)sender {
    
    [self performSegueWithIdentifier:@"partnerAccess" sender:self];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    
    [self.view endEditing:YES];
}


- (IBAction)refresh:(id)sender {
    
    self.countdownLabel.alpha=0.0;
    
    [self fetchSecondsToLaucnh];
}
- (IBAction)submitEmail:(id)sender {
    [self.view endEditing:YES];
    if (self.emailTextfield.text.length==0) {
        [UIView showSimpleAlertWithTitle:@"Oh Oh" message:@"Please enter a valid email address" cancelButtonTitle:@"OK"];
    }else{
        [self performEmailSubmission];
    }
}

-(void)performEmailSubmission{
    NSString * email = [NSString stringWithFormat:@"email_address=%@",[self.emailTextfield.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
    [self.emailSignUp loadWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@vip_access",kTestAPI_URL]] withParams:email WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
}
@end
