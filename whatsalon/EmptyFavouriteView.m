//
//  EmptyFavouriteView.m
//  whatsalon
//
//  Created by admin on 10/2/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import "EmptyFavouriteView.h"
#import "whatsalon-Swift.h"

@implementation EmptyFavouriteView
@synthesize delegate;


-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}

-(void) customInit {
    [[NSBundle mainBundle] loadNibNamed:@"EmptyFavouriteView" owner:self options:nil];
    [self addSubview:self.view];
    self.view.frame = self.bounds;
    self.startDiscoveringButton.clipsToBounds = true;
    self.startDiscoveringButton.layer.cornerRadius = 2;
}

-(void)setStartDiscoveringButton:(UIButton *)startDiscoveringButton 
{
    _startDiscoveringButton = startDiscoveringButton;
    _startDiscoveringButton.clipsToBounds = true;
    _startDiscoveringButton.layer.cornerRadius = 2;
    
    [Gradients addBlueToPurpleHorizontalLayerWithView:_startDiscoveringButton];
}

- (IBAction)startDiscovering:(id)sender {
    [self.delegate onStardDiscoveringTapped:self];
}

@end
