//
//  SalonReview.h
//  whatsalon
//
//  Created by Graham Connolly on 18/05/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header SalonReview.h
  
 @brief This is the header file represents the SalonReview object.
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.2
 */

#import <Foundation/Foundation.h>

@interface SalonReview : NSObject

/*! @brief represents the date reviewed. */
@property (nonatomic) NSString * date_reviewed;

/*! @brief reprresents the message of the review. */
@property (nonatomic) NSString * message;

/*! @brief the review image submitted. */
@property (nonatomic) NSString * review_image;

/*! @brief the salon rating. */
@property (nonatomic) NSString *salon_rating;

/*! @brief the stylists rating. */
@property (nonatomic) NSString *stylist_rating;

/*! @brief the users last name. */
@property (nonatomic) NSString *user_last_name;

/*! @brief the users first name. */
@property (nonatomic) NSString *user_name;

/*! @brief the string represenation of the users avatar url. */
@property (nonatomic) NSString * user_avatar_url;

/*! @brief creates an instance of the SalonReview object.
    
    @param firstName NSString
 
    @param lastName NSString
 
    @return instancetype an instance of SalonReview
 
 */
 
-(instancetype) initWithReviewer: (NSString *) firstName AndWithLastName : (NSString *) lastName;

/*! @brief creates an instance of the SalonReview object.
    
    @param firstName NSString
 
    @param lastName NSString
 
    @return instancetype 
 
 */
+(instancetype) reviewWithReviewer: (NSString *) firstName AndWithLastName : (NSString *) lastName;
@end
