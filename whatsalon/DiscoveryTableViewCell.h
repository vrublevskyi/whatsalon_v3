//
//  DiscoveryTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 11/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header DiscoveryTableViewCell.h
  
 @brief This is the header file for the DiscoveryTabeViewCell which is features in the TabDiscoveryViewController.
  
 This file contains the most important method and property decalarations. 
 
 It contains one main method that is used for setting up the cell
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    many micro updates
 */
#import <UIKit/UIKit.h>
#import "DiscoveryItem.h"

@interface DiscoveryTableViewCell : UITableViewCell

/*! Represents the title */
@property (nonatomic) NSString * title;

/*! represents the background image on the cell */
@property (nonatomic) UIImageView * backgroundImage;

/*! represents whether the table view cell should be customised as the salon nearest */
@property (nonatomic) BOOL isSalonsNearMe;

/*! represents the title UILabel */
@property (nonatomic) UILabel * titleLabel;

/*! an NSString representing the url for the image       */
@property (nonatomic) NSString * imageURL;

/*! represents  discovery table view icon. Each cell has a different icon*/
@property (nonatomic) UIImageView * icon;

/*! represents a tint that is applied to the cell */
@property (nonatomic) UIView * tintView;

/*! represents a line that undlines the title of the cell */
@property (nonatomic) UIView *underLineView;

/*!
 @brief sets up the appearance of the Discovery TableView Cell
 
 @param disc represents the DiscoveryItem to customise the cell
 
 @para yn determines if the cell should be 'Salons Nearby' or the other categories.
 
 */
-(void)setUpDiscoveryCell:(DiscoveryItem *) disc AndIsSalonsNearMe : (BOOL) yn;

@end
