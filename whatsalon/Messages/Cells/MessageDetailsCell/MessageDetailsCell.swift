//
//  MessageDetailsCell.swift
//  whatsalon
//
//  Created by admin on 10/10/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit
import Reusable

class MessageDetailsCell: UITableViewCell, NibReusable {
    
    @IBOutlet private var salonImageVIew: UIImageView!
    @IBOutlet private var salonMessageLabel: UILabel!
    
    
    var salonImage: UIImage? {
        didSet {
            salonImageVIew.image = salonImage
        }
    }
    
    var message: String? {
        didSet {
            salonMessageLabel.text = message
        }
    }
}
