//
//  MessageCell.swift
//  whatsalon
//
//  Created by admin on 10/9/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit
import Reusable

class MessageCell: UITableViewCell, NibReusable {
    
    //MARK: - Outlets
    
    @IBOutlet private var userImageView: UIImageView!
    @IBOutlet private var messageTextlabel: UILabel!
    @IBOutlet private var timeLabel: UILabel!
    @IBOutlet private var messagesStatusLabel: UILabel!
    @IBOutlet private var userNamelabel: UILabel!
    
    //MARK: - Properties
    var userImageURL: String? {
        didSet {
            
        }
    }
    
    var message: String? {
        didSet {
            messageTextlabel.text = message
        }
    }
    
    var time: String? {
        didSet {
            timeLabel.text = time
        }
    }
    
    var messageUnread = false {
        didSet {
            messagesStatusLabel.isHidden = !messageUnread
        }
    }
    
    var salonName: String? {
        didSet {
            userNamelabel.text = salonName
        }
    }
    
    //delete
    var setImage: UIImage? {
        didSet {
            userImageView.image = setImage
        }
    }
}
