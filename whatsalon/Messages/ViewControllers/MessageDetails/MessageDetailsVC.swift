//
//  MessageDetailsVC.swift
//  whatsalon
//
//  Created by admin on 10/10/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit
import Reusable

class MessageDetailsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(cellType: MessageDetailsCell.self)
        }
    }
    
    //MARK: - Properties
   @objc var salonName: String? {
        didSet {
            self.title = salonName
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.shadowImage = UIImage()
        
        let gradientView = UIView(frame: navigationController!.navigationBar.bounds)
        Gradient.addPinkToPurpleHorizontalGradient(on: gradientView)

        self.navigationController?.navigationBar.setBackgroundImage(gradientView.toImage(),
                                                                    for: .default)
    }
}

//tableView datasource, delegate
extension MessageDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MessageDetailsCell = tableView.dequeueReusableCell(for: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50;
    }
}
