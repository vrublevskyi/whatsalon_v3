//
//  MessagesVC.swift
//  whatsalon
//
//  Created by admin on 10/9/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

import UIKit
import EmptyKit

@objc protocol MessagesVCDelegate:class {
    func onMessageSelected(_ salonName: String, on controller: MessagesVC)
}

class MessagesVC: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet private var newMessagesCountLabel: UILabel!
    @IBOutlet private var gradientView: UIView! {
        didSet {
          
            Gradient.addPinkToPurpleHorizontalGradient(on: gradientView)
        }
    }
    
    @objc @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.ept.delegate = self
            tableView.ept.dataSource = self
            tableView.register(cellType: MessageCell.self)
        }
    }
    
    //MARK: - Properties
   @objc weak var delegate: MessagesVCDelegate?
    var messages = Message.mocked() {
        didSet {
            tableView.reloadData()
        }
    }
    @objc var newMessages: Int = 0{
        didSet {
            newMessagesCountLabel.text = "\(newMessages) " + "Unread Messages"
            self.tableView.reloadData()
        }
    }
    
    //MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newMessages = 5
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
}
//tableView datasource, delegate
extension MessagesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newMessages
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        let cell: MessageCell = tableView.dequeueReusableCell(for: indexPath)
        
        cell.salonName =        message.salonName
        cell.time =             message.time
        cell.messageUnread =    message.status ?? false
        cell.setImage =         message.image
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = messages[indexPath.row]

        delegate?.onMessageSelected(message.salonName ?? "", on: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75;
    }
}

//emptyView
extension MessagesVC: EmptyDataSource, EmptyDelegate {
    func customViewForEmpty(in view: UIView) -> UIView? {
        let view = MessagesEmptyView()
        return view
    }
}
