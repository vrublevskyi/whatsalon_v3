//
//  GCStackedGridLayout.m
//  StaggeredCollectionView
//
//  Created by Graham Connolly on 21/10/2014.
//  Copyright (c) 2014 Graham Connolly. All rights reserved.
//

#import "GCStackedGridLayout.h"
#import "GCStackedGridLayoutSection.h"

@interface GCStackedGridLayout (){
    /*
     The delegate property of a collection view layout is just a standard UICollectionViewDelegate. But in the case of this layout, you’re defining that it must be a StackedGridLayoutDelegate (remember, it inherits from UICollectionViewDelegate). So the _myDelegate variable will be used as a cache of the delegate property, but cast to an id <StackedGridLayoutDelegate> so that the compiler doesn’t complain when you try to call methods defined in the StackedGridLayoutDelegate protocol.
     */
    __unsafe_unretained id<StackedGridLayoutDelegate> _myDelegate;
    NSMutableArray * _sectionData;//holds sections
    CGFloat _height;//height for contents
    
}

@end
@implementation GCStackedGridLayout


-(void)prepareLayout{
    
    //1
    /*
     First you must call the implementation of the super class, as it may want to do some setup itself.
     */
    [super prepareLayout];
    _height=0.0f;
    
    //2
    /*
     You set up the internal state, first by caching the delegate to a variable of the required type, as explained earlier. Then you created the section data array and initialize the height.
     */
    _myDelegate = (id<StackedGridLayoutDelegate>)self.collectionView.delegate;
    _sectionData = [NSMutableArray new];
    _headerHeight = 0.0f;
    
    //3
    /*
     You create a couple of variables that will be used frequently. The first is the current origin that will be used to track where in the collection view you currently are as the sections are being created. The second is the number of sections, which is determined by asking the collection view.
     
     You may have thought that the best way to ask for the number of sections would be to ask the collection view’s data source, but that would be wrong. Apple says to ask the collection view, since this assures that the layout and collection view both have the same value.
     */
    CGPoint currentOrigin = CGPointZero;
    NSInteger numberOfSections = self.collectionView.numberOfSections;
    
    //4
    /*
     Loop through the number of sections.
     */
    for (NSInteger i = 0; i < numberOfSections; i++) {
        
        //5
        /*
         The first thing to do for each section is to increase the layout's height by the section header height. This ensures that there's enough room left for the header before the section starts. You also update the current origin to reflect this new height.
         */
        _height +=self.headerHeight;
        currentOrigin.y = _height;
        
        //6
        /*
         You determine the number of columns, items, and item insets for this section.
         */
        NSInteger numberOfColumns = [_myDelegate collectionView:self.collectionView
                                                         layout:self numberOfColumnsInSection:i];
        NSInteger numberOfItems = [self.collectionView numberOfItemsInSection:i];
        
        UIEdgeInsets itemInsets =
        [_myDelegate collectionView:self.collectionView
                             layout:self itemInsetsForSectionAtIndex:i];
        
        //7
        /*
         You create a new section object, passing in the various bits of required information. 
         Ath this stage, you might want to glance back at the section object implementation to remind yourself why you're passing in each of these values
         */
        GCStackedGridLayoutSection * section = [[GCStackedGridLayoutSection alloc] initWithOrigin:currentOrigin with:self.collectionView.bounds.size.width columns:numberOfColumns itemInsets:itemInsets];
        
        //8
        /*
         Loop through the items for the section
         */
        for (NSInteger j =0; j < numberOfItems; j++) {
            //9
            /*
             Calculate the items width by insetting the column width by the right amount, and then ask the delegate for the item size for the item at the required width.
             */
            CGFloat itemWidth = (section.columnWidth - section.itemInsets.left - section.itemInsets.right);
            NSIndexPath * itemIndexPath = [NSIndexPath indexPathForItem:j inSection:i];
            CGSize itemSize = [_myDelegate collectionView:self.collectionView layout:self
                                     sizeForItemWithWidth:itemWidth atIndexPath:itemIndexPath];
            
            //10
            /*
             Once the item size is known, you tell the section to add the item. Again, you may want to take a moment to remind yourself of the sections implementation.
             */
            [section addItemOfSize:itemSize forIndex:j];
            
            
        }
        
        //11
        /*
         Now that the section is fully populated with all of its items' data, you add it to the internal array
         */
        [_sectionData addObject:section];
        
        //12
        /*
         Finally, you update the height and current origin values. Once this loop is completed for all sections, the height variable will equal the height of the collection views content.
         */
        _height +=section.frame.size.height;
        currentOrigin.y = _height;
        
        
    }
}

/*
 Tells the collection view the size of the content. Rememeber that this is used just like the content size of a scroll view - to indicate the size of the scrollable region and to set up scroll bars properly
 
    The width is the size of the collection view itself and the height is the one calculated in prepareLayout.
 */

- (CGSize) collectionViewContentSize{
    return CGSizeMake(self.collectionView.bounds.size.width, _height);
}


/*
 Layout attributes
 
 */

/*
 This method finds the section object for the item and then asks the section for the frame of the item. It creates attributes as appropriate, and returns them.
 
 */
-(UICollectionViewLayoutAttributes*) layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    GCStackedGridLayoutSection * section = _sectionData[indexPath.section];
    
    UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    attributes.frame = [section frameForItemAtIndex:indexPath.item];
    
    return attributes;
}

/*
 Layout attributes for supplementary views. You need this because this layout will support header views.
 
 THis one fins the right section and creates a layout attributes object to describe the section header. It calculates the frame by looking at the frame of the section and moving up by the height of the header
 */

-(UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath{
    
    GCStackedGridLayoutSection * section = _sectionData[indexPath.section];
    
    UICollectionViewLayoutAttributes * attributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:elementKind withIndexPath:indexPath];
    
    CGRect sectionFrame = section.frame;
    CGRect headerFrame = CGRectMake(0.0f, sectionFrame.origin.y - self.headerHeight, sectionFrame.size.width, self.headerHeight);
    attributes.frame = headerFrame;
    
    return attributes;
}

/*
 Returns all the attributes for all elements (cells, supplemenary views, and decoration views) withing a give rectangle.
 */
-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    
    //1
    /*
     It creates an array to hold all the attributes
     */
    NSMutableArray * attributes = [NSMutableArray new];
    
    //2
    /*
     It enumerates each section item in turn
     */
    [_sectionData enumerateObjectsUsingBlock:^(GCStackedGridLayoutSection* section, NSUInteger sectionIndex, BOOL *stop) {
        
        //3
        /*
         It calculates the section frame and header frame and stores them in local variables for use later
         */
        CGRect sectionFrame = section.frame;
        CGRect headerFrame = CGRectMake(0.0f, sectionFrame.origin.y = self.headerHeight, sectionFrame.size.width, self.headerHeight);
        
        //4
        /*
         If the header frame intersects with the rectangle, then the header is visible and you need to return a layout attribute for it. So here you create one and add it to the attributes array
         */
        if (CGRectIntersectsRect(headerFrame, rect)) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForItem:0 inSection:sectionIndex];
            
            UICollectionViewLayoutAttributes * la = [self layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader atIndexPath:indexPath];
            [attributes addObject:la];
        }
        
        //5
        /*
         If the sections frame intersects with the rectangle, then at least one of its items must be visible
         */
        if (CGRectIntersectsRect(sectionFrame, rect)) {
            //6
            /*
             It loops through the items for the section
             */
            for (NSInteger index = 0; index < section.numberOfItems; index++) {
               
                //7
                /*
                 For each item, the method obtains its frame by assking the section
                 */
                CGRect frame = [section frameForItemAtIndex:index];
                
                //8
                /*
                 If the items frame interects with the rectangle, the the method creates layout attributes for it and adds them to the attributes array.
                 */
                if (CGRectIntersectsRect(frame, rect)) {
                    NSIndexPath * indexPath = [NSIndexPath indexPathForItem:index inSection:sectionIndex];
                    
                    UICollectionViewLayoutAttributes * la = [self layoutAttributesForItemAtIndexPath:indexPath];
                    [attributes addObject:la];
                }
                
            }
        }
    }];
    
    //9
    /*
     Finally, once the method has collected all the attributes of visible elements within the rectangle, it returns the array.
     */
    return attributes;
}
@end
