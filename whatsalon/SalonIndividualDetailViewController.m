//
//  DetailSalonTableViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 06/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "SalonIndividualDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MakeBookingViewController.h"
#import "GetDirectionsViewController.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "User.h"
#import "UIView+AlertCompatibility.h"
#import "SlideShowViewController.h"
#import "BiographyTableViewCell.h"

#define kMapCache @"WSMapCache"

#import "GCPopInTransition.h"
#import "GCPopOutTransition.h"
#import "GCImagePresentTransition.h"
#import "GCImageDismissTransition.h"

#import "ServicesIconTableViewCell.h"
#import "DirectionsMapTableViewCell.h"

#define kStaticMap @"AIzaSyD0RrghF_SmwrHUfHZ_EcTBNrvpMCQs0Nk"

#import "GCSplitPresentTransition.h"
#import "GCSplitDismissTransition.h"
#import <QuartzCore/QuartzCore.h>
#import "OpeningDay.h"
#import "SalonReviewTableViewCell.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SalonReviewListViewController.h"
#import "SelectCategoryViewController.h"
#import "ConfirmViewController.h"
#import "WSCoachMarksView.h"






@interface SalonIndividualDetailViewController ()<UITableViewDataSource,UITableViewDelegate,UIViewControllerTransitioningDelegate,SlideShowDelegate,SalonReviewDelegate,WSCoachMarksViewDelegate,GCNetworkManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *noOfPagesLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *salonTitle;
@property (weak, nonatomic) IBOutlet UIView *holderView;

@property (weak, nonatomic) IBOutlet UIImageView *starRating;
@property (weak, nonatomic) IBOutlet UILabel *noOfReviews;
@property (weak, nonatomic) IBOutlet UIButton *favButton;

@property (nonatomic, strong) NSMutableArray *pageViews;
@property (nonatomic) NSString * fullAddress;

- (void)loadVisiblePages;
- (void)loadPage:(NSInteger)page;
- (void)purgePage:(NSInteger)page;

@property (nonatomic) NSInteger noOfPages;

@property (nonatomic) UIButton * bookButton;

@property (nonatomic,strong) NSMutableArray * salonImages;
@property (nonatomic,assign) BOOL isFav;



@property (nonatomic,assign) BOOL showMoreBiography;




@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (nonatomic) NSMutableArray * buttonsArray;

@property (nonatomic,weak) MKMapSnapshotOptions *options;
@property (nonatomic,weak) MKMapSnapshotter *snapshotter;

@property (nonatomic) NSInteger currentPage;

@property (nonatomic) CGFloat h;

@property (nonatomic) NSTimer * scrollTimer;

@property (nonatomic) BOOL isDirectsionsTransition;
@property (nonatomic) BOOL isSlideShowTranition;
@property (nonatomic) UIImage * mapImage;
@property (nonatomic) BOOL showReviews;

@property (nonatomic) BOOL showCategoryList;

@property (nonatomic) GCNetworkManager * favNetworkManager;

@property (nonatomic) WSCoachMarksView *coachMarksView;
@end

@implementation SalonIndividualDetailViewController

-(void)setUpWalkThrough{
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{0,kNavBarAndStatusBarHeight},{self.view.frame.size.width,self.scrollView.frame.size.height}}],
                                @"caption": @"Slide show"
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{self.favButton.frame.origin.x,self.favButton.frame.origin.y+kNavBarAndStatusBarHeight},{self.favButton.frame.size.width,self.favButton.frame.size.height}}],
                                @"caption": @"Like this Salon, add it to your favourites",
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{0,230},{self.view.frame.size.width,100+115+50}}],
                                @"caption": @"Learn about the Salon"
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{10.0f,182.0f},{300.0f,56.0f}}],
                                @"caption": @"View and manage your friends & family"
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{10.0f,245.0f},{300.0f,56.0f}}],
                                @"caption": @"Invite friends to get more photos"
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:(CGRect){{0.0f,410.0f},{320.0f,50.0f}}],
                                @"caption": @"Keep your guests informed with your wedding details"
                                }
                            ];
   self.coachMarksView = [[WSCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    [self.navigationController.view addSubview:self.coachMarksView];
    self.coachMarksView.lblColor = [UIColor whiteColor];
    self.coachMarksView.delegate=self;
    [self.coachMarksView start];

}
- (void)coachMarksView:(WSCoachMarksView*)coachMarksView willNavigateToIndex:(NSUInteger)index{
    NSLog(@"Index %lu",(unsigned long)index);
    
    if (index==2) {
        NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];

    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"is from search %@",StringFromBOOL(self.unwindBackToSearchFilter));
   // [self setUpWalkThrough];
    [WSCore addTopLine:self.footerView :kCellLinesColour :0.5];

    //adjust details
    self.salonTitle.text = self.salonData.salon_name;
    self.salonTitle.adjustsFontSizeToFitWidth=YES;
    self.fullAddress = [self.salonData fetchFullAddress];
    
    
    if ([WSCore isDummyMode]) {
       // self.salonImages =[[NSMutableArray alloc] initWithArray: [WSCore getDummySalonGallery]];
    }
    else{
        
        //if there are no images have just show the default images
        if ([self.salonData fetchSlideShowGallery].count ==0) {
            [self.salonImages removeAllObjects];
            self.salonImages =self.salonData.placeHolderImageGallery;
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Images" message:[NSString stringWithFormat:@"Unable to download images for %@. Please try again later.",self.salonData.salon_name] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
            //keep calling image gallery until you get images
            
        }else{
            self.salonImages = [[NSMutableArray alloc] initWithArray:[self.salonData fetchSlideShowGallery]];
            NSLog(@"Salon images count %lu",(unsigned long)self.salonImages.count);
            [self.salonImages removeLastObject];
            
        }

        
        
    }
   
    //self.starRating.image = [UIImage imageNamed:@"white_four_star_rating"];
    NSLog(@"ratings %d",(int)self.salonData.ratings);
    self.starRating.image = [WSCore getStarRatingImage:(int)self.salonData.ratings];
    
    NSLog(@"Salon reviews %f",self.salonData.reviews);
    if ((int)self.salonData.reviews==1) {
        self.noOfReviews.text =[NSString stringWithFormat:@"%d review",(int)self.salonData.reviews];
        

    }else{
        self.noOfReviews.text =[NSString stringWithFormat:@"%d reviews",(int)self.salonData.reviews];
    }
    
    if ((int)self.salonData.reviews==0) {
        self.starRating.image = [self.starRating.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.starRating setTintColor:[UIColor lightGrayColor]];
        NSLog(@"No stars");
    }else{
        self.starRating.image = [self.starRating.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.starRating setTintColor:kWhatSalonBlue];
    }
  
    if (self.salonData.is_favourite){
        
        [self.favButton setImage:[UIImage imageNamed:@"fav_heart"] forState:UIControlStateNormal];
        self.isFav=YES;
    }
    
    [self.favButton addTarget:self action:@selector(markAsFavourite) forControlEvents:UIControlEventTouchUpInside];
    
   self.holderView.backgroundColor=[UIColor whiteColor];
    self.holderView.userInteractionEnabled=YES;
    
 

    
    NSInteger pageCount = self.salonImages.count;
    
    self.noOfPages=pageCount;
   
    
    
    // 3
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
    self.scrollView.delegate=self;
    
    self.noOfPagesLabel.text = [NSString stringWithFormat:@"%d/%ld",1,(long)self.noOfPages];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;

    self.bookButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.bookButton.frame = CGRectMake(10, 0, self.view.frame.size.width-20, 50) ;
    //self.bookButton.alpha=0.8;
    self.bookButton.layer.cornerRadius=3.0f;
    self.bookButton.layer.masksToBounds=YES;
    
    self.bookButton.backgroundColor = kWhatSalonBlue;
    

    [self.bookButton setTitle:@"See Availability" forState:UIControlStateNormal];
    
    
    [self.bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal
     ];
    [self.bookButton addTarget:self action:@selector(book) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.bookButton];
    [WSCore floatingView:self.bookButton];
    
    UITapGestureRecognizer * scrollTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toSlideShow)];
    scrollTap.numberOfTapsRequired=1;
    [self.scrollView addGestureRecognizer:scrollTap];
    
    self.scrollView.userInteractionEnabled=YES;
    self.holderView.backgroundColor=kBackgroundColor;
    
    self.view.backgroundColor=kBackgroundColor;
    
    
    self.footerView.backgroundColor=kBackgroundColor;

    
    //MKSnapshotter
    NSString * mapName = [NSString stringWithFormat:@"map%f%f",self.salonData.salon_lat,self.salonData.salon_long];
    
    if (IS_IOS_8_OR_LATER) {
        if (![self checkIfMapExistsAtKey:mapName]) {
            
            MKMapSnapshotOptions * snapOptions= [[MKMapSnapshotOptions alloc] init];
            self.options=snapOptions;
            CLLocation * salonLocation = [[CLLocation alloc] initWithLatitude:self.salonData.salon_lat longitude:self.salonData.salon_long];
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(salonLocation.coordinate, 300, 300);
            self.options.region = region;
            self.options.size = self.view.frame.size;
            self.options.scale = [[UIScreen mainScreen] scale];
            
            MKMapSnapshotter * mapSnapShot = [[MKMapSnapshotter alloc] initWithOptions:self.options];
            self.snapshotter =mapSnapShot;
            [self.snapshotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error) {
                if (error) {
                    NSLog(@"[Error] %@", error);
                    return;
                }
                
                UIImage *image = snapshot.image;
                self.mapImage = image;
                NSData *data = UIImagePNGRepresentation(image);
                
                [self saveMapDataToCache:data WithKey:mapName];
                
                
                
            }];
            
        }else{
            NSLog(@"From Cache");
            UIImage * dataImage = [UIImage imageWithData:[self fetchFileAtKey:mapName]];
            self.mapImage= dataImage;
            
        }

    }
    
    
    
    
    
    
    self.scrollTimer =[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    
    if (self.salonData.openingHours.count==0) {
        UILabel* monday = [[UILabel alloc] initWithFrame:CGRectMake(8, 37, 240, 20)];
        monday.text = @"No Opening hours provided.";
        monday.font=[UIFont systemFontOfSize:14.0f];
        monday.textColor = [UIColor lightGrayColor];
        [self.footerView addSubview:monday];

    }
    else{
     
        UILabel* monday = [[UILabel alloc] initWithFrame:CGRectMake(8, 37, 170, 20)];
        monday.text = @"Monday";
        monday.font=[UIFont systemFontOfSize:14.0f];
        monday.textColor = [UIColor lightGrayColor];
        [self.footerView addSubview:monday];
        
        UILabel* tuesday = [[UILabel alloc] initWithFrame:CGRectMake(8, 60, 170, 20)];
        tuesday.text = @"Tuesday";
        tuesday.font=[UIFont systemFontOfSize:14.0f];
        tuesday.textColor = [UIColor lightGrayColor];
        [self.footerView addSubview:tuesday];
        
        UILabel* weds = [[UILabel alloc] initWithFrame:CGRectMake(8, 83, 170, 20)];
        weds.text = @"Wednesday";
        weds.textColor = [UIColor lightGrayColor];
        weds.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:weds];
        
        UILabel* thurs = [[UILabel alloc] initWithFrame:CGRectMake(8, 106, 170, 20)];
        thurs.text = @"Thursday";
        thurs.textColor = [UIColor lightGrayColor];
        thurs.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:thurs];
        
        UILabel* fri = [[UILabel alloc] initWithFrame:CGRectMake(8, 129, 170, 20)];
        fri.text = @"Friday";
        fri.textColor = [UIColor lightGrayColor];
        fri.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:fri];
        
        UILabel* sat = [[UILabel alloc] initWithFrame:CGRectMake(8, 152, 170, 20)];
        sat.text = @"Saturday";
        sat.textColor = [UIColor lightGrayColor];
        sat.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:sat];
        
        UILabel* sun = [[UILabel alloc] initWithFrame:CGRectMake(8, 175, 170, 20)];
        sun.text = @"Sunday";
        sun.textColor = [UIColor lightGrayColor];
        sun.font=[UIFont systemFontOfSize:14.0f];
        [self.footerView addSubview:sun];
        
        UILabel * chosenLabel;
        for (OpeningDay*day in self.salonData.openingHours) {
            
            switch ([day.dayOfWeek integerValue]) {
                case 1:
                    sun.text=day.dayName;
                    chosenLabel=sun;
                    break;
                case 2:
                    monday.text=day.dayName;
                    chosenLabel=monday;
                    break;
                case 3:
                    tuesday.text=day.dayName;
                    chosenLabel=tuesday;
                    break;
                case 4:
                    weds.text=day.dayName;
                    chosenLabel=weds;
                    break;
                case 5:
                    thurs.text=day.dayName;
                    chosenLabel=thurs;
                    break;
                case 6:
                    fri.text=day.dayName;
                    chosenLabel=fri;
                    break;
                case 7:
                    sat.text=day.dayName;
                    chosenLabel=sat;
                    break;
                default:
                    break;
            }
            
            
            UILabel * time = [[UILabel alloc] initWithFrame:CGRectMake(chosenLabel.frame.origin.x-10, chosenLabel.frame.origin.y, self.view.frame.size.width-10, 18)];
            time.font=[UIFont systemFontOfSize:14.0f];
            if (day.isOpened) {
                time.text = [NSString stringWithFormat:@"%@ - %@  ",[WSCore getShortTimeFromDate:day.startTime WithFormat:@"HH:mm"],[WSCore getShortTimeFromDate:day.endTime WithFormat:@"HH:mm"]];
            }
            else{
                time.text = @"Closed  ";
            }
            
            time.textColor=[UIColor lightGrayColor];
            time.textAlignment=NSTextAlignmentRight;
            [self.footerView addSubview:time];
            
            
        }

    }
        [self.tableView registerClass:[ServicesIconTableViewCell class] forCellReuseIdentifier:@"iconCell"];
    
    [self.tableView registerClass:[DirectionsMapTableViewCell class] forCellReuseIdentifier:@"mapCell"];
    
    [self.tableView registerClass:[SalonReviewTableViewCell class] forCellReuseIdentifier:@"reviewCell"];

    //self.transition = [[GCSplitPresentTransition alloc] init];
    //self.dismissTransition = [[GCSplitDismissTransition alloc] init];
    
    self.noOfPagesLabel.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.4];
    self.noOfPagesLabel.layer.cornerRadius=6.0;
    self.noOfPagesLabel.layer.masksToBounds=YES;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.showCategoryList=NO;
    
    self.favNetworkManager = [[GCNetworkManager alloc] init];
    self.favNetworkManager.delegate=self;
    
}

-(NSString *) getTimeFromDate : (NSString *) date{
    
    //date = [date stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    //date = [date stringByReplacingOccurrencesOfString:@"Z" withString:@" "];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate * tempDate = [dateFormatter dateFromString: date];
    
    [dateFormatter setDateFormat:@"h:mm a"];
    return [dateFormatter stringFromDate:tempDate];
}


- (void)onTimer {
    
    
    NSInteger numberOfPages = self.salonImages.count - 1;
    if (self.currentPage==numberOfPages) {
        
        NSLog(@"End Timer");
        [self.scrollTimer invalidate];
        return;
    }
    
    // Updates the variable h, adding 100 (put your own value here!)
    self.h += CGRectGetWidth(self.scrollView.frame);
    
    //This makes the scrollView scroll to the desired position
    [self.scrollView setContentOffset:CGPointMake(self.h, 0) animated:YES];
    
    //[self loadVisiblePages];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self.scrollTimer invalidate];
  
}



-(void)goToMap: (UITableViewCell *)cell{
    
   
    
    //dispatch_async(dispatch_get_main_queue(), ^{
        self.isDirectsionsTransition=YES;
        
        //self.transition.sourceView=cell;
        //self.dismissTransition.sourceView=cell;
        

        GetDirectionsViewController *destinationViewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
        destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
        destinationViewController.transitioningDelegate = self;
        destinationViewController.salonMapData = self.salonData;
        [self.scrollTimer invalidate];
        [self presentViewController:destinationViewController animated:YES completion:nil];
   // });
    

    
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self releaseMKMapSnapshotMem];
    [self.scrollTimer invalidate];
}
-(void)releaseMKMapSnapshotMem{
    self.snapshotter=nil;
    self.options=nil;
    
    
}

//MKMapSnapShotter
-(void)saveMapDataToCache: (NSData *) data WithKey: (NSString *) key{
    
    //check if the cache exits, if not create it
    if ([self createCacheIfItDoesNotExist]) {
        NSString *path;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        path = [[paths objectAtIndex:0] stringByAppendingPathComponent:kMapCache];
        path = [path stringByAppendingPathComponent:key];
        
        [[NSFileManager defaultManager] createFileAtPath:path
                                                contents:data
                                              attributes:nil];
    }
    //An error occurred when creating the caching
    
}

-(BOOL)createCacheIfItDoesNotExist{
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:kMapCache];
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])	//Does directory already exist?
    {
        if (![[NSFileManager defaultManager] createDirectoryAtPath:path
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create directory error: %@", error);
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}

-(BOOL)checkIfMapExistsAtKey: (NSString *) key{
    
    NSString *path2;
    NSArray *path2s = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path2 = [[path2s objectAtIndex:0] stringByAppendingPathComponent:kMapCache];
    path2 = [path2 stringByAppendingPathComponent:key];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path2])
    {
        //File exists
        NSData *file1 = [[NSData alloc] initWithContentsOfFile:path2];
        if (file1)
        {
            
            return YES;
        }
    }
    
    return NO;
    
}

-(NSData *)fetchFileAtKey: (NSString *)key{
    
    
    NSString *path2;
    NSArray *path2s = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path2 = [[path2s objectAtIndex:0] stringByAppendingPathComponent:kMapCache];
    path2 = [path2 stringByAppendingPathComponent:key];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path2])
    {
        //File exists
        NSData *file1 = [[NSData alloc] initWithContentsOfFile:path2];
        if (file1)
        {
            
            return file1;
        }
    }
    
    return nil;
    
}

-(void)toSlideShow{
    
    self.isSlideShowTranition=YES;
    [self.scrollTimer invalidate];
    SlideShowViewController *slideShow = [self.storyboard instantiateViewControllerWithIdentifier:@"SlideShowVC"];
    if (IS_iPHONE5_or_Above) {
        slideShow.modalPresentationStyle=UIModalPresentationCustom;
        slideShow.transitioningDelegate=self;
    }
    else{
        slideShow.modalPresentationStyle=UIModalPresentationCurrentContext;
    }
    
    if ([WSCore isDummyMode]) {
        slideShow.dummyImageArray=self.salonImages;
    }
    slideShow.salonData=self.salonData;
    slideShow.startIndex=self.currentPage;
    slideShow.myDelegate=self;
    [self presentViewController:slideShow animated:YES completion:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"bookB"]) {
        
        MakeBookingViewController * bookVC = (MakeBookingViewController *)segue.destinationViewController;
        if (![WSCore isDummyMode]) {
            bookVC.salonData = self.salonData;
            //[bookVC.backgroundImage sd_setImageWithURL:[[self.salonData fetchSlideShowGallery] objectAtIndex:0]];
            bookVC.isFromFavourite=self.isFromFavourite;
            NSLog(@"Is from fav %d",self.isFromFavourite);
            bookVC.unwindBackToSearchFilter=self.unwindBackToSearchFilter;

        }
        else{
            bookVC.salonData=self.salonData;
        }
        
        
    }
    
    else if ([segue.identifier isEqualToString:@"toMap"]) {
        
        GetDirectionsViewController *destinationViewController =
        (GetDirectionsViewController *)segue.destinationViewController;
        destinationViewController.salonMapData = self.salonData;
        destinationViewController.isSplitTransition=YES;

        
    }
    
    [self.scrollTimer invalidate];

}

-(void)book{
    [self.scrollTimer invalidate];
    /*
    self.showCategoryList=YES;
    
    
    SelectCategoryViewController * s = [self.storyboard instantiateViewControllerWithIdentifier:@"selectCategoryVC"];
    s.modalPresentationStyle = UIModalPresentationCustom;
    s.transitioningDelegate = self;
    [self presentViewController:s animated:YES completion:nil];
     */
    /*
    if (self.isFromLastMinuteBooking) {
        
        ConfirmViewController * confirmViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"confirmVC"];
        confirmViewController.salonData = self.lastMin.lastMinuteSalon;
        confirmViewController.isFromLastMinPopToSalonIndividual=YES;
        
        Booking * booking = [Booking bookingWithSalonID:self.lastMin.lastMinuteSalon.salon_id];
        NSString * day = self.chosenDay;
        day  = [day stringByAppendingString:[NSString stringWithFormat:@", %@",[self.lastMin getSimpleDateISO8601]]];
        //self.chosenDay = [self.chosenDay stringByAppendingString:[NSString stringWithFormat:@", %@",[lastMin getSimpleDateISO8601]]];
        booking.chosenDay = day;
        booking.chosenStylist = @"Any stylist";
        booking.staff_id = self.lastMin.staff_phorest_id;
        booking.chosenTime = [self.lastMin getSimpleDateISO8601]
        ;
        //booking.room_id = lastMin.r
        booking.slot_id = self.lastMin.slot_id;
        booking.internal_start_datetime = self.lastMin.startTime;
        booking.internal_end_datetime = self.lastMin.endTime;
        booking.endTime = self.lastMin.endTime;
        booking.chosentService = self.chosenService;
        booking.chosenPrice = [NSString stringWithFormat:@"%.2f",[self.lastMin.service_price doubleValue]];
        confirmViewController.bookingObj=booking;
        
        
        NSLog(@"Booking time %@",booking.chosenTime);
        NSArray *timeSegments = [booking.chosenTime componentsSeparatedByString:@":"];
        NSLog(@"%@",timeSegments);
        
        //get pm/am
        NSArray * minsAmPmSegments = [timeSegments[1] componentsSeparatedByString:@" "];
        NSLog(@"Min %@",minsAmPmSegments[0]);
        NSLog(@"AM/PM %@",minsAmPmSegments[1]);
        
        NSDate *currentDate = self.chosenDate;
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate]; // Get necessary date components
        
        [components month]; //gives you month
        [components day]; //gives you day
        [components year]; // gives you year
        
        NSString* firstString = [timeSegments objectAtIndex:0];
        NSLog(@"\n\nFirst String (HOUR) %@",firstString);
        NSString* secondString = [minsAmPmSegments objectAtIndex:0];
        NSLog(@"Second string (MIN) %@",secondString);
        NSLog(@"AM OR PM %@\n\n\n", minsAmPmSegments[1]);
        if ([[minsAmPmSegments[1] lowercaseString ]isEqualToString:@"pm"]) {
            firstString = [NSString stringWithFormat:@"%d",[firstString intValue] + 12];
            NSLog(@"\n\nFirst String (HOUR) in 24 format %@",firstString);
        }
        
        NSDate * fullDate = [WSCore dateWithYear:[components year] month:[components month] day:[components day] WithHour:[firstString intValue] AndMin:[secondString intValue]];
        NSLog(@"Full Date %@",fullDate);
        confirmViewController.chosenDate=fullDate;
        
        [self presentViewController:confirmViewController animated:YES completion:nil];
    }else{
     */
        [self performSegueWithIdentifier:@"bookB" sender:self];
    //}
    
}
- (void)loadPage:(NSInteger)page {
    if (page < 0 || page >= self.salonImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // 1
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null]) {
        // 2
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        
        // 3
        //UIImageView *newPageView = [[UIImageView alloc] initWithImage:[self.salonImages objectAtIndex:page]];
        UIImageView * newPageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"empty_cell"]];
        
        if ([WSCore isDummyMode]) {
            newPageView.image = [self.salonImages objectAtIndex:page];
        }
        else{
           [newPageView sd_setImageWithURL:[self.salonImages objectAtIndex:page] placeholderImage:[UIImage imageNamed:@"black"]];
        }
        
        newPageView.contentMode = UIViewContentModeScaleAspectFill;
        newPageView.frame = frame;
        [self.scrollView addSubview:newPageView];
        // 4
        [self.pageViews replaceObjectAtIndex:page withObject:newPageView];
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.salonImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (void)loadVisiblePages {
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    // self.pageControl.currentPage = page;
    self.currentPage=page;
    
    self.noOfPagesLabel.text = [NSString stringWithFormat:@"%ld/%ld",(unsigned long)page+1,(unsigned long)self.noOfPages];
    // Work out which pages you want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
    // Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
    // Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.salonImages.count; i++) {
        [self purgePage:i];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView==self.scrollView) {
        [self loadVisiblePages];
    }
    else{
        CGRect frame = self.bookButton.frame;
        frame.origin.y = scrollView.contentOffset.y + self.tableView.frame.size.height - self.bookButton.frame.size.height-10;
        self.bookButton.frame = frame;
        
        [self.view bringSubviewToFront:self.bookButton];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    NSLog(@"**** Salon individual page ****");
    [WSCore statusBarColor:StatusBarColorBlack];
   
    if (self.navigationController.navigationBar.tintColor==[UIColor whiteColor]) {
        [WSCore nonTransparentNavigationBarOnView:self];
    }
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    // 4
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.salonImages.count, pagesScrollViewSize.height);
    
    // 5
    [self loadVisiblePages];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    NSLog(@"NUmber of rows %d",3+ (int)self.salonData.hasReview);
    return 3 + (int)self.salonData.hasReview;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
  
    NSString *bioCell = @"bioCell";
    
    UITableViewCell *cell;
    
    if (indexPath.row==0) {
        cell = [tableView dequeueReusableCellWithIdentifier:bioCell forIndexPath:indexPath];
        
        NSArray *viewsToRemove = [cell.contentView subviews];
        for (UIView *v in viewsToRemove) {
            [v removeFromSuperview];
        }
        
        
        //UILabel * biographyTitle;
        UILabel * descriptionLabel;
        if (descriptionLabel==nil) {
           
  
            NSInteger noOfLines=0;
            CGFloat readMoreLabelY = 0.0f;
            NSString * moreText;
            if (self.showMoreBiography) {
            
                noOfLines=0;
                readMoreLabelY=185;
                moreText=@"Less";
            }
            else{
                
             
                noOfLines=3;
                readMoreLabelY=115;
                moreText=@"Read More";
            }
          
            descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, cell.contentView.frame.size.width-20, 60)];
            if (self.salonData.salon_description.length==0) {
                descriptionLabel.text = @"   No description available";
                descriptionLabel.textColor=[UIColor lightGrayColor];
            }else{
                  descriptionLabel.text =self.salonData.salon_description;
            }
          
            descriptionLabel.font = [UIFont systemFontOfSize:15.0f];
            NSDictionary *attributes = @{NSFontAttributeName: descriptionLabel.font};
            
            CGRect rect = [descriptionLabel.text boundingRectWithSize:CGSizeMake(cell.contentView.frame.size.width-20, 170)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:attributes
                                                      context:nil];

            
            if (self.showMoreBiography) {
                    
              
                CGRect currentLabelFrame = descriptionLabel.frame;
                
                currentLabelFrame.size.height = rect.size.height;
                
                descriptionLabel.frame = currentLabelFrame;
                descriptionLabel.adjustsFontSizeToFitWidth=YES;
            }
            
       
            
            if (!self.showMoreBiography) {
                CAGradientLayer *l = [CAGradientLayer layer];
                l.frame = descriptionLabel.bounds;
                l.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor clearColor].CGColor];
                l.startPoint = CGPointMake(1.f, .1f);
                l.endPoint = CGPointMake(1.0f, 1.0f);
                descriptionLabel.layer.mask = l;
                descriptionLabel.adjustsFontSizeToFitWidth=NO;

            }
            UILabel * moreLabel = [[UILabel alloc] initWithFrame:CGRectMake(105, readMoreLabelY, 110, 40)];
            moreLabel.text = moreText;
            moreLabel.font = [UIFont systemFontOfSize:14.0f];
            moreLabel.textColor=kWhatSalonBlue;
            moreLabel.textAlignment=NSTextAlignmentCenter;
            moreLabel.layer.borderColor=kWhatSalonBlue.CGColor;
            moreLabel.layer.borderWidth=1.0f;
            moreLabel.layer.cornerRadius=3.0f;
            [cell.contentView addSubview:moreLabel];
            

            descriptionLabel.numberOfLines=noOfLines;
            [cell.contentView addSubview:descriptionLabel];
        }

    }
    if(indexPath.row==1){
        
        ServicesIconTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"iconCell" forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.clipsToBounds=YES;
        cell.backgroundColor=kBackgroundColor;
        //cell.saloObj=self.salonData;
       
        [cell setUpCellWithSalonObject:self.salonData];
        return cell;
    }
    if (indexPath.row==2) {
        DirectionsMapTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"mapCell" forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.clipsToBounds=YES;
        cell.backgroundColor=kBackgroundColor;
        if ([self.salonData fetchFullAddress].length>=30) {
            cell.directionsLabel.frame = CGRectMake(cell.directionsLabel.frame.origin.x, cell.directionsLabel.frame.origin.y, cell.directionsLabel.frame.size.width, 40);
            cell.directionsLabel.numberOfLines=0;
        }
        cell.directionsLabel.text=[self.salonData fetchFullAddress];
       cell.mapImageView.image = self.mapImage;
        
        
        cell.backgroundColor=kBackgroundColor;
        return cell;

    }
    if (indexPath.row==3) {
        SalonReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reviewCell" forIndexPath:indexPath];
        cell.delegate=self;
        [cell setUpReviewPageWithReview:self.salonData.salonReview AndWithNumberOfReviews:(int)self.salonData.reviews];
        
        if (!IS_IOS_8_OR_LATER) {
            
        }
        return cell;
    }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.clipsToBounds=YES;
    cell.backgroundColor=kBackgroundColor;
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        if (self.showMoreBiography) {
            return 250;
        }
        return 170;
      

    }
    else if(indexPath.row==1){
        return 80;
    }
    else if (indexPath.row==2){
        return 90;
    }
    else if(indexPath.row==3){
        if ((int)self.salonData.reviews>1) {
            return 220;
        }
        return 180;
    }
    return self.tableView.rowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row==0) {
        if (self.showMoreBiography) {
            NSLog(@"Show less");
            self.showMoreBiography=NO;
        }else{
            NSLog(@"Show more");
            self.showMoreBiography=YES;
        }
      
 
        NSArray * rowArray = [[NSArray alloc] initWithObjects:indexPath, nil];
        [tableView reloadRowsAtIndexPaths:rowArray withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    if (indexPath.row==1) {
        //if (!self.isFromLastMinuteBooking) {
            [self book];
        //}
        
    }
    if (indexPath.row==2) {
        //[self goToMap:cell];
        self.isDirectsionsTransition=YES;
              GetDirectionsViewController *destinationViewController =
        [self.storyboard instantiateViewControllerWithIdentifier:@"getDirectionsController"];
        destinationViewController.modalPresentationStyle = UIModalPresentationCustom;
        destinationViewController.transitioningDelegate = self;
        destinationViewController.salonMapData = self.salonData;
        [self.scrollTimer invalidate];
        [self presentViewController:destinationViewController animated:YES completion:nil];
    }
    
}

#pragma mark - UIViewControllerTransitioningDelegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    if (self.showReviews || self.showCategoryList) {
        //self.showReviews=NO;
        self.bookButton.hidden=YES;
        
        return [[PresentDetailTransition alloc] init];
    } else if (self.isSlideShowTranition) {
        self.favButton.alpha=0.0;
        self.noOfPagesLabel.alpha=0.0;
        if ([WSCore isDummyMode]) {
            return [[GCImagePresentTransition alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.scrollView.frame.size.height) AndImage:self.salonImages[self.currentPage]];
        }
        return [[GCImagePresentTransition alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.scrollView.frame.size.height) AndImagePath:self.salonImages[self.currentPage]];
    }
    else if(self.isDirectsionsTransition){
       return [[GCPopInTransition alloc] init];
        //return self.transition;
    }
   
    
    return nil;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    if (self.showReviews || self.showCategoryList) {
        self.bookButton.hidden=NO;
        self.showReviews=NO;
        self.showCategoryList=NO;
        return [[DismissDetailTransition alloc] init];
    } else if (self.isSlideShowTranition) {
        self.isSlideShowTranition=NO;
        
        [UIView animateWithDuration:0.4f delay:0.6 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.favButton.alpha=1.0f;
            self.noOfPagesLabel.alpha=1.0f;
        } completion:nil];
        
        if ([WSCore isDummyMode]) {
            return [[GCImageDismissTransition alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.scrollView.frame.size.height) AndImage:self.salonImages[self.currentPage]];
        }
    
        return [[GCImageDismissTransition alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.scrollView.frame.size.height) AndImagePath:self.salonImages[self.currentPage]];
    }
    else if(self.isDirectsionsTransition){
        self.isDirectsionsTransition=NO;
        
        CGRect cellRect = [self.tableView rectForFooterInSection:0];
        cellRect = CGRectOffset(cellRect, -self.tableView.contentOffset.x, -self.tableView.contentOffset.y+ cellRect.size.height);
        //return [[GCPopOutTransition alloc] init];//WithFrame:cellRect];
        return [[DismissDetailTransition alloc] init];
        //return self.dismissTransition;
    }
    
    return nil;
    
    
    
}


#pragma mark - mark as fovourite

#pragma mark - GCNetwork Delegate
-(void)manager:(GCNetworkManager *)manager handleServerFailureWithError:(NSError *)error{
    [WSCore showServerErrorAlert];
    [self enableFavButton];
}
- (void)favButtonState {
    if (self.isFav) {
        self.isFav=NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"Un fav");
            if (!self.isFromFavourite) {
                [self postNotificationWithSalonRemoveFavourite:self.salonData.salon_id];  
            }
            
            [self.favButton setImage:[UIImage imageNamed:@"White_Heart_With_Gray_Solid"] forState:UIControlStateNormal];
            [self enableFavButton];
        });
        
        
        
    }
    else{
        self.isFav=YES;
        NSLog(@"fav");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.favButton setImage:[UIImage imageNamed:@"fav_heart"] forState:UIControlStateNormal];
            
            if (!self.isFromFavourite) {
                [self postNotificationWithSalonIsFavourite:self.salonData.salon_id];
            }
            
            // [self showLikeCompletion];
            [self enableFavButton];
        });
        
        
    }
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    [self favButtonState];
}
-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
     if ([jsonDictionary[@"message"] isEqualToString:@"This user already favourited this salon"]) {
        
         
         [self favButtonState];

     }
   
    [self enableFavButton];
}
- (void)disableFavButton {
    self.favButton.enabled=NO;
    self.favButton.alpha=0.5;
}
-(void)enableFavButton{
    self.favButton.enabled=YES;
    self.favButton.alpha=1.0;
}

-(void)markAsFavourite{
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey];
    if (![[User getInstance] isUserLoggedIn]) {
        [WSCore showLoginSignUpAlert];
        return;
    }
    
    
    if (self.isFav) {
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonData.salon_id]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&unmark=false"]];
        [self.favNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        [self disableFavButton];
    }
    else{
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",[[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonData.salon_id]];
         [self.favNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
        [self disableFavButton];
    }
    
    /*
    if (self.isFav) {
       
       
            
        //[self.favButton setImage:[UIImage imageNamed:@"White_Heart"] forState:UIControlStateNormal];
         [self.favButton setImage:[UIImage imageNamed:@"White_Heart_With_Gray_Solid"] forState:UIControlStateNormal];
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",accessToken];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonData.salon_id]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&unmark=false"]];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
            if (data) {
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                        
                        if ([dict[@"success"] isEqualToString:@"false"]) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [UIView showSimpleAlertWithTitle:@"Oops" message:@"An error occurred." cancelButtonTitle:@"OK"];
                            });
                        }
                        break;
                        
                    default:
                        break;
                }
            }
        }];
        
        if ([WSCore isNetworkReachable]) {
            [task resume];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self postNotificationWithSalonRemoveFavourite:self.salonData.salon_id];
        });
        
        self.isFav=NO;
    }
    else{
        
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kMark_favourite_URL]];
        NSString * params = [NSString stringWithFormat:@"user_id=%@",[[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&salon_id=%@",self.salonData.salon_id]];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
            if (data) {
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                        
                        if ([dict[@"success"] isEqualToString:@"false"]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [UIView showSimpleAlertWithTitle:@"Oops" message:@"An error occurred." cancelButtonTitle:@"OK"];
                            });
                        }
                        break;
                        
                    default:
                        break;
                }
            }
        }];
        
        if ([WSCore isNetworkReachable]) {
            [task resume];
        }
        self.isFav=YES;
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
                
        [self.favButton setImage:[UIImage imageNamed:@"fav_heart"] forState:UIControlStateNormal];
                
            
            [self postNotificationWithSalonIsFavourite:self.salonData.salon_id];
            [self showLikeCompletion];
        });
    }
*/
    
    
    
}

-(void)showLikeCompletion{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Added to Favourites!" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    
    double delayInSeconds = 1.0;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    });
    
}

#pragma marks - Notifcations
-(void)postNotificationWithSalonIsFavourite:(NSString *)salonId //post notification method and logic
{
    NSString *notificationName = @"salonIsAddedToFavourites";
    NSString *key = @"Salon_id";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:salonId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

- (void)postNotificationWithSalonRemoveFavourite:(NSString *)salonId //post notification method and logic
{
    NSString *notificationName = @"salonIsNotFavourite";
    NSString *key = @"Salon_id";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:salonId forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

- (IBAction)dayChange:(id)sender {
    
    UIButton * btn = (UIButton *)sender;
    NSLog(@"Btn tag = %ld",(long)btn.tag);
    
    for (UIButton *dayBtn in self.buttonsArray) {
                if (dayBtn.tag==btn.tag) {
                    dayBtn.layer.cornerRadius=dayBtn.frame.size.height/2.0f;
                    dayBtn.layer.borderWidth=1.0f;
                    dayBtn.layer.borderColor=kWhatSalonBlue.CGColor;
                    [dayBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dayBtn.backgroundColor=kWhatSalonBlue;
                }
                else{
                    dayBtn.layer.cornerRadius=0;
                    dayBtn.layer.borderColor=[UIColor clearColor].CGColor;
                    dayBtn.layer.borderWidth=0.0f;
                    [dayBtn setTitleColor:kWhatSalonBlue forState:UIControlStateNormal];
                    dayBtn.backgroundColor=[UIColor clearColor];
                }
            }
}


#pragma mark - SlideShowDelegate

-(void)getLastPage:(NSInteger)lastPage{
    
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    // Start point - Set initial content offset of scrollview
    self.scrollView.contentOffset = CGPointMake(pagesScrollViewSize.width * lastPage, self.scrollView.contentOffset.y);
}

#pragma mark - SalonReviewDelegate
-(void)didSelectRowToShowAll{
    //[UIView showSimpleAlertWithTitle:@"SHOW LIST" message:@"" cancelButtonTitle:@"OK"];
    self.showReviews=YES;
    SalonReviewListViewController * salonReview = [self.storyboard instantiateViewControllerWithIdentifier:@"reviewList"];
    salonReview.salonData=self.salonData;
    salonReview.transitioningDelegate=self;
    salonReview.modalPresentationStyle = UIModalPresentationCustom;
    //salonReview.isList=YES;
    [self presentViewController:salonReview animated:YES completion:nil];
    
    
}

#pragma mark - UNWIND Segue
- (IBAction)unwindToSalonIndividualFromLastMin:(UIStoryboardSegue *)unwindSegue
{
    [self.scrollTimer invalidate];
    self.scrollTimer=nil;
    [self performSegueWithIdentifier:@"unwindFromSalonIndividualToLastMinute" sender:self];
    
}


@end
