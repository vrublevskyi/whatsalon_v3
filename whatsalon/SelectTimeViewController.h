//
//  SelectTimeViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 10/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header SelectTimeViewController.h
  
 @brief This is the header file for SelectTimeViewController.
 
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.2
 */

#import <UIKit/UIKit.h>
#import "SalonAvailableTime.h"

/*!
     @protocol SelectTimeDelegate
  
     @brief The SelectTimeDelegate protocol
  
     It's a protocol used to notify the delegate that a time has been selected. 
 */

@class SelectTimeViewController;
@protocol SelectTimeDelegate <NSObject>
- (void) onTimeSelected: (NSString *) stylist sender:(SelectTimeViewController*) sender;
    @end

@interface SelectTimeViewController : UIViewController

/*!
 @brief represents the SelectTimeDelegate.
 */
@property (nonatomic) id<SelectTimeDelegate> delegate;

/*!
 @brief represents the NSMutableArray for the times array.
 */
@property (nonatomic) NSMutableArray * timesArray;

@end
