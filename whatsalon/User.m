//
//  User.m
//  whatsalon
//
//  Created by Graham Connolly on 11/07/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "User.h"
#import "UICKeyChainStore.h"
#import "PrimativeComparison.h"
#import "GCFacebookHelper.h"


@implementation User 

static User *singletonInstance;
+(instancetype)getInstance{
 
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        
    
     
    });
    
    
    return sharedInstance;
}

#pragma mark - Users first name and last name

/*!
 * @discussion fetches the users firstName from the NSUserDefault key kFirstNamekey.
    If the users first name is 0, set it to nil. The server would often return the users name
    as 0 in some cases. This is just a precaution.
 * @return self.firstName NSString representing the users first name.
 */
-(NSString *)fetchFirstName{
    
    if (self.firstName == nil) {
        self.firstName =[[NSUserDefaults standardUserDefaults] objectForKey:kFirstNameKey];
        
        if ([self.firstName isEqualToString:@"0"]) {
            self.firstName=nil;
        }
    }
    
    return self.firstName;
}

/*!
 * @discussion Updates the users first name. Sets the variable firstName to parameter firstName. It also updates NSUserDefaults for key kFirstNameKey.
 * @param firstName NSString
 */
-(void)updateUsersFirstName: (NSString *) firstName{
    self.firstName = firstName;
    [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:kFirstNameKey];
}


/*!
 * @discussion fetches the users Last Name. If the lastName is nil fetch it from NSUserDefaults key kSurnameKey. If the users last name is 0 set it to nil.
 * @return self.lastName represents the users Last name
 */

-(NSString *)fetchLastName{
    

    //NSLog(@"Last name %@",self.lastName);
    if (self.lastName ==nil) {
        self.lastName = [[NSUserDefaults standardUserDefaults] objectForKey:kSurnameKey];
        
        if ([self.lastName isEqualToString:@"0"]) {
            self.lastName=nil;
        }
    }
    
    return self.lastName;
}

/*!
 * @discussion updates the users last name. Updates NSUserDefaults representing the key for Surname.
 * @param lastName NSString
 */
-(void)updateUsersLastName: (NSString *) lastName{
    self.lastName = lastName;
    [[NSUserDefaults standardUserDefaults] setObject:lastName forKey:kSurnameKey];
   
}

/*!
 * @discussion removes the users first name and last name from the local variables self.lastName and self.firstName. It also remove both from NSUserDefaults for key kFirstNameKey and kLastNameKey.
 */
-(void)removeUsersFirstNameAndLastName{
    self.lastName = nil;
    self.firstName = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kFirstNameKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSurnameKey];
}


/* =======================================*/

#pragma mark - Profile set up classes


/*!
 * @discussion Sets up the users profile using the NSDictionary fBdict. fbDict is an NSDictionary which contains the returned profile from the server following Facebook Login (Facebook sign up).
        
        - updates the users gender for key kGenderKey. Store in NSUserDefaults.
        - sets up the users email for kEmailKey. Stored in UICKeyChainStore
        - sets up the users key for key s_key. Stored in UICKeyChainStore
        - sets up the users last name for key user_last_name. Stored in UICKeyChainStore
        - sets up the users first name for key kFirstNameKey. Stored in NSUserDefaults
        - updates the users accesstoken
        - updates the users phone number
        - updates the users credit card reference
        - updates the users credit card details
            checks whether the credit card_details count is not 0. If its not then update the users credit card details.
                + updates the card first name
                + updates the card last name
                + updates the card number
                + updates the card type
                + updates the cards expiry
                + updates the users credit card reference
        - checks if the users has validated Twilio
        - updates the users image url.
        - Set boolean to YES for Facebook login
        - Synchronizes NSUserdefaults
 
 
 * @param fbDict NSDictionary
 */
-(void)setUpFacebookProfile: (NSDictionary *)fbDict{

    
   
    //[[NSUserDefaults standardUserDefaults] setObject:fbDict[@"message"][@"gender"] forKey:kGenderKey];
    [self updateGender:fbDict[@"message"][@"gender"]];
    //[UICKeyChainStore setString:fbDict[@"message"][@"user_email"] forKey:kEmailKey];
    [self updateEmailAddress:fbDict[@"message"][@"user_email"]];
    //[UICKeyChainStore setString:fbDict[@"message"][@"secret_key"] forKey:@"s_key"];
    [self updateKey:fbDict[@"message"][@"secret_key"]];
    //[[NSUserDefaults standardUserDefaults] setObject:fbDict[@"message"][@"user_last_name"] forKey:kSurnameKey];
    [self updateUsersLastName:fbDict[@"message"][@"user_last_name"]];
    //[[NSUserDefaults standardUserDefaults] setObject:fbDict[@"message"][@"user_name"] forKey:kFirstNameKey];
    [self updateUsersFirstName:fbDict[@"message"][@"user_name"]];
    
    [self updateAccessToken:fbDict[@"message"][@"user_id"] ];
    [self updatePhoneNumber:fbDict[@"message"][@"user_phone"] ];
    //[self updateCreditCardPaymentRef:fbDict[@"message"][@"payment_ref"] ] ;
    //[[NSUserDefaults standardUserDefaults] setObject:fbDict[@"message"][@"payment_ref"] forKey:kCreditCardPaymentRef];
    
    if ([fbDict[@"message"][@"card_details"] count]!=0) {
        if (fbDict[@"message"][@"card_details"][@"card_first_name"]) {
            
            [self updateCreditCardFirstName:fbDict[@"message"][@"card_details"][@"card_first_name"]];
        }
        if (fbDict[@"message"][@"card_details"][@"card_last_name"]!=[NSNull null]) {
            
            [self updateCreditCardLastName:fbDict[@"message"][@"card_details"][@"card_last_name"]];
        }
        if (fbDict[@"message"][@"card_details"][@"card_number"]!=[NSNull null]) {
           
            [self updateCreditCardNo:fbDict[@"message"][@"card_details"][@"card_number"]];
        }
        
        if (fbDict[@"message"][@"card_details"][@"card_type"]!=[NSNull null]) {
           
            [self updateCreditCardType:fbDict[@"message"][@"card_details"][@"card_type"]];
        }
        if (fbDict[@"message"][@"card_details"][@"expiry"]!=[NSNull null]) {
         
            [self updateCreditCardExp:fbDict[@"message"][@"card_details"][@"expiry"]];
        }
        if (fbDict[@"message"][@"card_details"][@"payment_ref"]!=[NSNull null]) {
          
            [self updateCreditCardPaymentRef:[NSString stringWithFormat:@"%@",fbDict[@"message"][@"card_details"][@"payment_ref"]]];

        }
    }
  
    if (fbDict[@"message"][@"twilio_valid"] !=[NSNull null]) {
        BOOL yn = [fbDict[@"message"][@"twilio_valid"] boolValue];
        
        [self updateTwilioVerified:yn ];
       
    }
    
    
    [self updateUsersBalance:[NSString stringWithFormat:@"%@",fbDict[@"message"][@"money_balance"]]];
    [self setImageURL:fbDict[@"message"][@"user_avatar"]];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"facebookSignUp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

/*!
 * @discussion updates the user profile using profDict. profDict is an NSDictionary representing the users profile parsed from the server on Sign up.
        - updates the users gender
        - updates the users balance
        - updates the users email address
        - updates the users key
        - updates the users first name for key FirstName
        - updates the users last name for key kSurnameKey
        - updates the users accessToken
        - updates the users phone number
        - checks that the credit card details are greater than 0
            + updates the credit card first name
            + updates the credit card last name
            + updates the credit card number
            + updates the credit card card type
            + updates the credit card exiry
        - updates whether Twilio is verified
        - updates the image url
        - Synchornizes the NSUserDefaults.
 * @param profDict NSDictionary representing the JSON object.
 */
-(void)setUpProfile:(NSDictionary *)profDict{
    
    NSDictionary *dictionary = profDict;
    
    [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"message"][@"gender"] forKey:kGenderKey];
    
    [self updateUsersBalance:dictionary[@"message"][@"money_balance"]];
    
    [UICKeyChainStore setString:dictionary[@"message"][@"user_email"] forKey:kEmailKey];
    [UICKeyChainStore setString:dictionary[@"message"][@"secret_key"] forKey:@"s_key"];
    [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"message"][@"user_last_name"] forKey:kSurnameKey];
    [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"message"][@"user_name"] forKey:kFirstNameKey];
    
    [[User getInstance] updateAccessToken:dictionary[@"message"][@"user_id"] ];
    
    [[User getInstance] updatePhoneNumber:dictionary[@"message"][@"user_phone"] ];
    [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"message"][@"paymentref"] forKey:kCreditCardPaymentRef];
    
    if ([dictionary[@"message"][@"card_details"] count ]>1) {
        [[User getInstance] updateCreditCardFirstName:dictionary[@"message"][@"card_details"][@"card_first_name"]];
        
        [[User getInstance] updateCreditCardLastName:dictionary[@"message"][@"card_details"][@"card_last_name"] ];
        
        [[User getInstance] updateCreditCardNo:dictionary[@"message"][@"card_details"][@"card_number"]];
        
       // [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"message"][@"card_details"][@"card_type"] forKey:kCreditCardTypeKey];
        [[User getInstance] updateCreditCardType:dictionary[@"message"][@"card_details"][@"card_type"]];
        [[User getInstance] updateCreditCardExp:dictionary[@"message"][@"card_details"][@"expiry"]];
    }
    
    BOOL yn = [dictionary[@"message"][@"twilio_valid"] boolValue];
   // NSLog(@"Twilio verified %d",yn);
    [[User getInstance] updateTwilioVerified:yn];
    //NSLog(@"Is Twilio verified %d",[[User getInstance] isTwilioVerified]);
    
    
    [[User getInstance] setImageURL:dictionary[@"message"][@"user_avatar"]];
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}

/*
-(NSString *)fetchUsersSocialAvatar{
    
    if (self.social_avatar_url==nil) {
        self.social_avatar_url =[[NSUserDefaults standardUserDefaults] objectForKey:@"social_image_url"];
    }
    return self.social_avatar_url;
}
-(void)updateUsersAvatarURL: (NSString *)users_avatar_url{
    self.social_avatar_url = users_avatar_url;
    [[NSUserDefaults standardUserDefaults] setObject:users_avatar_url forKey:@"social_image_url"];
}
-(void)removeUsersAvatarURL{
    self.social_avatar_url = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"social_image_url"];
}
 */


/* ===================================================== */




/*!
 * @discussion Fetches the users Gender. If the gender variable is nil then fetch from the NSUserDefaults object for kGenderKey.
 * @return self.gender NSString representing the users gender
 */
#pragma mark - Users Gender
-(NSString *)fetchGender{
    
    if (self.gender==nil) {
        self.gender =[[NSUserDefaults standardUserDefaults] objectForKey:kGenderKey];
    }
    
    
    return self.gender;
}

/*!
 * @discussion Updates the users gender. Updates the gender variable, also updates the NSUserDefaults object for key kGenderKey
 * @param gender NSString representing the users gender
 */
-(void)updateGender: (NSString *)gender {
    self.gender=gender;
    [[NSUserDefaults standardUserDefaults] setObject:gender forKey: kGenderKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*!
 * @discussion Sets the gender variable to nil. Removes the object for key in NSUserDefaults.
 */
-(void)removeUsersGender{
    self.gender=nil;
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kGenderKey];
}


/* ========================================================= */




#pragma mark - Users AccessToken

/*!
 * @discussion fetch users access token from UICKeyChainStore. If local variable accessToken is nill then fetch from UICKeyChainStore.
 * @return self.accessToken represents the users accessToken
 */
-(NSString *)fetchAccessToken{
    
   // NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:kAccessTokenKey];
    //NSString * accessToken = [UICKeyChainStore stringForKey:kAccessTokenKey];
    
    //NSLog(@"Access Token %@",accessToken);
    if (self.accessToken==nil) {
        self.accessToken =[UICKeyChainStore stringForKey:kAccessTokenKey];
    }
   //NSLog(@"Access Token Variable %@",self.accessToken);
    
    return self.accessToken;
    
    //return accessToken;
}


/*!
 * @discussion Remove the users accessToken. Sets self.accessToken to nil. Removes the accessToken from the UICKeyChainStore.
 */
-(void)removeUserAccessToken{
    self.accessToken = nil;
    [UICKeyChainStore removeItemForKey:kAccessTokenKey];
}


/*!
 * @discussion Updates the users accessToken. Sets the accessToken variable. Updates the UICKeyChainStore for the key kAccessTokenKey.
 */
-(void)updateAccessToken:(NSString *)token{
    
    self.accessToken = token;
    [UICKeyChainStore setString:[NSString stringWithFormat:@"%@",token] forKey:kAccessTokenKey];
    
}


/* =========================================================== */



#pragma mark - Users Email Adress
/*!
 * @discussion fetches the users email address. If the email variable is nil, fetch the value from UICKeyChainStore.
 * @return self.email returns the email address of the user
 */
-(NSString *)fecthEmail{
    
    //NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:kEmailKey];
   // NSString * email =[UICKeyChainStore stringForKey:kEmailKey];
    
    if (self.email==nil) {
        self.email =[UICKeyChainStore stringForKey:kEmailKey];
    }
    return self.email;
}


/*!
 * @discussion Removes the users Email address. Set the email variable to nil. Removes the item for key kEMmailKey from UICKeyChainStore.
 */
-(void)removeEmailAddress{
    self.email = nil;
    [UICKeyChainStore removeItemForKey:kEmailKey];
}


/*!
 * @discussion updates the users email address. Sets the email variable. Updates the UICKeyChainStore for kEmailKey
 * @param email NSString represents the users email address
 */
-(void)updateEmailAddress:(NSString *)email{
    
    //[[NSUserDefaults standardUserDefaults] setObject:email forKey:kEmailKey];
    self.email = email;
    [UICKeyChainStore setString:email forKey:kEmailKey];
}



/*========================================================================= */


#pragma mark - Users phone number

/*!
 * @discussion fetches the users phone number. If the phoneNumber is nil, fetch the users phone number from UICKeyChainStore.
 * @return self.phoneNumber NSString returns the users phone Number.
 */
-(NSString *)fetchPhoneNumber{
   
    //NSString * phoneNUmber = [[NSUserDefaults standardUserDefaults] objectForKey:kPhoneKey];
    //NSString * phoneNUmber = [UICKeyChainStore stringForKey:kPhoneKey];
    //NSLog(@"fetch phone %@",phoneNUmber);
    
    if (self.phoneNumber==nil) {
        self.phoneNumber = [UICKeyChainStore stringForKey:kPhoneKey];
    }
    return self.phoneNumber;
}


/*!
 * @discussion removes the users phone number. Sets the phoneNumber to nil. Removes the item for Key kPhoneKey from the UICKeyChainStore.
 */
-(void)removeUsersPhoneNumber{
    self.phoneNumber=nil;
    [UICKeyChainStore removeItemForKey:kPhoneKey];
}


/*!
 * @discussion Updates the users phone number. Sets the phoneNumber variable. Updates the UICKeyStore for kPhoneKey.
 * @param phone NSString which represents the users phone number
 */
-(void)updatePhoneNumber: (NSString *)phone{
    
    //NSLog(@"**Phone =  %@ **",phone);
    //[[NSUserDefaults standardUserDefaults] setObject:phone forKey:kPhoneKey];
    self.phoneNumber = phone;
    [UICKeyChainStore setString:phone forKey:kPhoneKey];
}


/* ============================================================ */


#pragma mark - Users balance 

/*!
 * @discussion Updates the users balance. Sets the usersBalance variable to bal. Updates the NSUserDefaults for kUSersBalanceKey
 * @param bal NSString representing the new balance.
 */
-(void)updateUsersBalance: (NSString *)bal{
    
    self.usersBalance =bal;
    [[NSUserDefaults standardUserDefaults] setObject:bal forKey:kUserBalanceKey];
    
    //NSLog(@"Users balance %@, gets from balance key %@",self.usersBalance, [[NSUserDefaults standardUserDefaults] objectForKey:kUserBalanceKey]);
}

/*!
 * @discussion Fetches the users balance. If the usersBalance variable is nil fetch from the NUSerDefaults for Key kUsersBalanceKey
 * @return self.usersBalance NSString value representing the uers balance.
 */
-(NSString *)fetchUsersBalance{
    
    //NSString *userBalance = [[NSUserDefaults standardUserDefaults] objectForKey:kUserBalanceKey];
    if (self.usersBalance == nil) {
        self.usersBalance =[[NSUserDefaults standardUserDefaults] objectForKey:kUserBalanceKey];
    }
    
    return self.usersBalance;
}



/*!
 * @discussion Removes the users balance. Sets the users credit card balance varialbe to nil. Remove the corresponding balance in NSUserDefaults.
 */
-(void)removeUsersBalance{
    self.usersBalance=nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserBalanceKey];
}




/* ==================================================== */

/*
#pragma mark - Updates the users imagePathData
-(void)updateUserImagePath: (NSData *)imageData{
   
    self.imagePathData = imageData;
    [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:[NSString stringWithFormat:@"%@%@",kImageDataKey,[[User getInstance] fecthEmail]]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
-(NSData *)fetchUserImagePath: (NSString *)userToken{
    
    if (self.imagePathData==nil) {
        self.imagePathData =[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",kImageDataKey,[[User getInstance] fecthEmail]]];
    }
    
    return self.imagePathData;
    
//    [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",kImageDataKey,[[User getInstance] fecthEmail]]];
//    
//    return [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",kImageDataKey,[[User getInstance] fecthEmail]]];;
    
}
-(void)removeUsersImagePathData{
    self.imagePathData = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@%@",kImageDataKey,[[User getInstance] fecthEmail]]];
}
 
 */


#pragma mark - Download Users Balance
/*!
 * @discussion Downloads the users balance from the WhatSalon Server. Updates the users balance.
 */
-(void)downloadUsersBalance{
    
    NSString * accessToken = [self fetchAccessToken];
    if (accessToken) {
        
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_User_Balance_URL]];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:90];
        NSString * param = [[NSString alloc] initWithString:[NSString stringWithFormat:@"user_id=%@",accessToken]];
        [request setHTTPMethod:@"POST"];
        [ request setHTTPBody:[param dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (data) {
                NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *) response;
                NSDictionary * dataDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                switch (httpResp.statusCode) {
                    case kOKStatusCode:
                        //NSLog(@"Get users balance %@",dataDictionary);
                        
                        if ([dataDictionary[kSuccessIndex] isEqualToString:@"true"]) {
                            
                            [self updateUsersBalance:dataDictionary[kMessageIndex]];
                            
                            //[[NSUserDefaults standardUserDefaults] setObject:dataDictionary[kMessageIndex] forKey:kUserBalanceKey];
                        }
                        break;
                        
                    default:
                        break;
                }
                
            }
            
        }];
        
        if ([WSCore isNetworkReachable]) {
            [task resume];
        }
        
    }
    
    
    

    
}


/* ==================================================== */



#pragma mark - Users CC Details

/*!
 * @discussion Updates the user credit card profile.
        - Updates the cc firsname and the self.ccFirstName. Stores in the UICKeyChainStore
        - Updates the cc lastName and the self.ccLastName. Stores in the UICKeyChainStore
        - Updates the cc no and the self.ccCardNo. Sotres in the UICKeyChainStore
        - Updates the credit card type and self.ccType. Stores in the UICKeyChainStore
        - Updates the credit card expiry date and the sellf.ccExp. Stores in the UICKeyChainStore
        - Updates the credit card payment ref and the self.paymentRef. Store in the UICKeyChainStore.
        - SYynchronize the UICKeyChainStore
 
 * @param firstName NSString represents the First Name on the card
            lastName NSString represents the last name on the card
            no NSString represents the CardNumber
            type NSString represents the Creidit Card Type
            exp NSString represents the Credit Card Expiry
            ref NSString represents the Payment Reference number
 
 */

-(void)updateCreditCardDetailsWithFirstName:(NSString *) firstName withLastName: (NSString *) lastName withCardNumber: (NSString *) no withCardType: (NSString *) type withExp : (NSString *) exp withPaymentRef: (NSString *) ref{
    
    //first Name
    self.ccFirstName =firstName;
    [UICKeyChainStore setString:firstName forKey:kCreditCardFirstNameKey];
    
    //lastName
    self.ccLastName = lastName;
    [UICKeyChainStore setString:lastName forKey:kCreditCardLastNameKey];
    
    //card no
    self.ccCardNo = no;
    [UICKeyChainStore setString:no forKey:kCreditCardNumberKey];
    
    //card type
    self.ccType = type;
    [UICKeyChainStore setString:type forKey:kCreditCardTypeKey];
    
    //exp
    self.ccExp=exp;
    [UICKeyChainStore setString:exp forKey:kCreditCardExpKey];
    
    
    //payment ref
    self.ccPaymentRef=ref;
    [UICKeyChainStore setString:ref forKey:kCreditCardPaymentRef];
    
    //synch
    UICKeyChainStore *store = [UICKeyChainStore keyChainStore];
   
    [store synchronize];
    
    [self updateDoesUserHaveCard:@"YES"];
}


/*!
 * @discussion Fetches the cc first Name. If the ccFirstName is nil then fetch from UICKeyChainStore.
 * @return self.ccFirstName NSString represents the first name on the credit card
 */
-(NSString *) fetchCreditCardFirstName{
    
    
    if (self.ccFirstName==nil) {
        self.ccFirstName =[UICKeyChainStore stringForKey:kCreditCardFirstNameKey];
    }
    //NSString * ccFirstName = [UICKeyChainStore stringForKey:kCreditCardFirstNameKey];


    
    return self.ccFirstName;
}

/*!
 * @discussion Updates the first name on the CC. Sets self.ccFirstName to firstName. Stores first name in UICKeyChainStore.
 * @param firstName NSString new string for firstName
 */
-(void) updateCreditCardFirstName: (NSString *) firstName{
    
   //update key chain
    
    self.ccFirstName = firstName;
    UICKeyChainStore *store = [UICKeyChainStore keyChainStore];
    
    [store setString:firstName forKey:kCreditCardFirstNameKey];
    
    
    [store synchronize]; // Write to keychain.
}

/*!
 * @discussion Fetches the ccLastName. If the ccLasttName is nil then fetch from UICKeyChainStore.
 * @return self.ccLastName NSString represents the last name on the credit card
 */
-(NSString *) fetchCreditCardLastName{
 
    if (self.ccLastName==nil) {
        self.ccLastName =[UICKeyChainStore stringForKey:kCreditCardLastNameKey];
    }
    return self.ccLastName;
    //return [UICKeyChainStore stringForKey:kCreditCardLastNameKey];
}

/*!
 * @discussion Updates the last name on the CC. Sets self.ccLastName to lastName. Stores last name in UICKeyChainStore.
 * @param lastName NSString new string for lastName
 */
-(void)updateCreditCardLastName: (NSString *) lastName{
    
    self.ccLastName=lastName;
    UICKeyChainStore * store = [UICKeyChainStore keyChainStore];
    [store setString:lastName forKey:kCreditCardLastNameKey];
    
    [store synchronize];
}

/*!
 * @discussion fetches the CC no. If self.ccCardNo is nil fetch it from UICKeyChanStore
 * @return self.ccCardNo NSString representing the cc no
 */

-(NSString *) fetchCreditCardNo{
    
    if (self.ccCardNo==nil) {
        self.ccCardNo =[UICKeyChainStore stringForKey:kCreditCardNumberKey];
    }
    return self.ccCardNo;
}


/*!
 * @discussion updates the credit card  no. Updates the variable self.ccCardNo. Updates the UICKeyChainStore. Synchronizes the UICKeyChainStore.
 * @param no NSString represents the credit card no
 */
-(void)updateCreditCardNo: (NSString *) no{
   
    //update key chain
    self.ccCardNo = no;
    UICKeyChainStore * store = [UICKeyChainStore keyChainStore];
    [store setString:no forKey:kCreditCardNumberKey];
    [store synchronize];
}


/*!
 * @discussion fetches the Credit Card Type. If nil assign from the UICKeyChainStore
 * @return self.ccType NSString which represents type
 */
-(NSString *) fetchCreditCardType{
    
    if (self.ccType==nil) {
        self.ccType =[UICKeyChainStore stringForKey:kCreditCardTypeKey];;
    }
    
    return self.ccType;
}

/*!
 * @discussion Updates the Credit Card type. Sets the variable self.ccType to cType. Updates the UICKeyChainStore.
 * @param cType NSString represents the cType
 */
-(void)updateCreditCardType: (NSString *) cType{
    
    //update key chain
    self.ccType = cType;
    UICKeyChainStore * store = [UICKeyChainStore keyChainStore];
    [store setString:cType forKey:kCreditCardTypeKey];
    [store synchronize];
}


/*!
 * @discussion fetch the credit card exp. if ccExp is nil, fetch from UICKeyChainStore
 * @return self.ccExp represents the expiry type of the card
 */
-(NSString *) fetchCreditCardExp{
    
    if (self.ccExp==nil) {
        self.ccExp =[UICKeyChainStore stringForKey:kCreditCardExpKey];;
    }
    return self.ccExp;

}


/*!
 * @discussion Updates the credit card type. Sets the self.ccExp to exp. Updates the keyChainStore.
 * @param exp NSString represents the expiry
 */
-(void)updateCreditCardExp: (NSString *) exp{
    
    self.ccExp = exp;
    UICKeyChainStore * store = [UICKeyChainStore keyChainStore];
    [store setString:exp forKey:kCreditCardExpKey];
    [store synchronize];
    
}


/*!
 * @discussion fetch Credit Card Payment Ref. If ccPaymentRef is nil fetch from UICKeyChainStore
 * @return self.ccPaymentRef NSString represents the payment ref
 */
-(NSString *) fetchCreditCardPaymentRef{
   
    
    if (self.ccPaymentRef==nil) {
        self.ccPaymentRef = [UICKeyChainStore stringForKey:kCreditCardPaymentRef];;
    }
    return self.ccPaymentRef;
}


/*!
 * @discussion Updates the CreditCard Payment ref. Sets self.ccPatmentRef to ref. Updates the KeyChainStore.
 * @param ref NSString Represents the payment ref
 */
-(void)updateCreditCardPaymentRef: (NSString *) ref{
    
    //update key chain
    self.ccPaymentRef = ref;
    UICKeyChainStore * store = [UICKeyChainStore keyChainStore];
    [store setString:ref forKey:kCreditCardPaymentRef];
    [store synchronize];
}

//remove credit card info
/*!
 * @discussion Removes the credit card information.
        - sets the ccFirstName to nil. Removes from KeyChain
        - sets the ccLastName to nil. Removes from KeyChain
        - sets the cardNo to nil. Removes from KeyChain
        - sets the ccType to nil. Removes from KeyChain
        - sets the ccExp to nil. Removes from KeyChain
        - sets the payment ref to nil. Removes from KeyChain
        - sychronizes the keystore
 */
-(void)removeCreditCardInfo{
    
    if ([UICKeyChainStore stringForKey:kCreditCardPaymentRef]) {
       
        //first Name
        self.ccFirstName = nil;
        [UICKeyChainStore removeItemForKey:kCreditCardFirstNameKey];
        
        //lastName
        self.ccLastName=nil;
        [UICKeyChainStore removeItemForKey:kCreditCardLastNameKey];
        
        //card no
        self.ccCardNo=nil;
        [UICKeyChainStore removeItemForKey:kCreditCardNumberKey];
        
        //card type
        self.ccType = nil;
       [UICKeyChainStore removeItemForKey:kCreditCardTypeKey];
        
        //exp
        self.ccExp = nil;
       [UICKeyChainStore removeItemForKey:kCreditCardExpKey];
        
        //payment ref
        //self.paymentRef = nil;
        self.ccPaymentRef=nil;
        [UICKeyChainStore removeItemForKey:kCreditCardPaymentRef];
        
        //synch
        UICKeyChainStore *store = [UICKeyChainStore keyChainStore];
        [store synchronize];

    }
    
    [self updateDoesUserHaveCard:@"NO"];
    NSLog(@"Remove");
    
}

/*!
 * @discussion gets the credit card Image Type
 * @return img UIImage representing the credit card type
 */
-(NSString *)getCreditCardImageType{
    NSArray * cardArray = [WSCore getSupportedCardsArray];
    NSString *img;
    for (NSString * card in cardArray) {
        if ([card.lowercaseString isEqualToString:[[User getInstance] fetchCreditCardType].lowercaseString]) {
            if ([card.lowercaseString isEqualToString:@"visa"]) {
                img = @"Visa";
                return img;
                
            }
            else if([card.lowercaseString isEqualToString:@"mastercard"]){
                img = @"MasterCard";
                return img;
            }else if([card.lowercaseString isEqualToString:@"visa"]){
                return img;
            }
        }
    }
    return img;
}

/* =============================================================== */


#pragma mark - ImageURL for user avatar

/*!
 * @discussion updates self.usersImageURL with urlString
 * @param urlString NSString representing the url
 */
-(void)setImageURL: (NSString *)urlString{
    self.usersImageURL = [NSURL URLWithString:urlString];
    
    [[NSUserDefaults standardUserDefaults] setObject:urlString forKey:kImageURLKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

    //NSLog(@"%@",[self fetchImageURL]);
}

/*!
 * @discussion removes the users image url. Set the usersImageURL to nil. Removes object for key in NSUserDefaults.
 */
-(void)removeUsersImageURL{
    self.usersImageURL = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kImageURLKey];
}

/*!
 * @discussion fetches the imageURL. If self.usersImageURL is nil fetch from NSUserDefaults.
 * @return self.usersImageURL NSURL the user for the image
 */
-(NSURL *)fetchImageURL{
    
    //NSString*urlString = [[NSUserDefaults standardUserDefaults] objectForKey:kImageURLKey];
    if (self.usersImageURL==nil) {
        self.usersImageURL = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:kImageURLKey]];
    }
    //NSLog(@"Avatar = %@",self.usersImageURL);
    return self.usersImageURL;
}
/*!
 * @discussion Checks whether the user has an image URL i.e User Avatar
 * @return bool
 */
-(BOOL)hasImageURL{
    if ([[self fetchImageURL] absoluteString].length==0) {
        return NO;
    }
    return YES;
}


/* ============================================================================ */


#pragma mark - S key
/*!
 * @discussion updates self.key and updates the UICKeyChainStore
 * @param key - NSString representing the key
 */
-(void)updateKey: (NSString *) key{
    self.key = key;
    [UICKeyChainStore setString:key forKey:@"s_key"];
   
}


/*!
 * @discussion Remove the s_key. Sets key to nil and removes the item from the KeyChain.
 */

-(void)removeTheKey{
    self.key=nil;
    [UICKeyChainStore removeItemForKey:@"s_key"];
    
}

/*!
 * @discussion fetches the key. If key is equal to nil fetch from UICKeyChainStore.
 * @return self.key NSString represents the key
 */
-(NSString *)fetchKey{
    
    if (self.key==nil) {
        self.key =[UICKeyChainStore stringForKey:@"s_key"];
    }
    // NSString * key = [UICKeyChainStore stringForKey:@"s_key"];
    return self.key;
}



/* ========================================================================= */
#pragma mark - Users log out method
/*!
 * @discussion logout - used to log the user out of the app and clear user information
        - set facebookSignUp bool to NO
        - if facebook session is active then close and clear token information
        - remove User Access Token
        - remove Email Address
        - remove users phone number
        - remove the key
        - remove the users image url
        - remove the users first name and last name
        - remove the users gender
        - remove the users card balance
        - remove the Credit Card Info   
        - set local sign up to NO
        - synchronize to NSUserDefaults
        - synchronize the UICKeyChain
        - post notification to update RESideMenu background (only used in left side menu)
 
 */

-(void)logout{
    
     [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"facebookSignUp"];
    
 
    GCFacebookHelper * fh = [[GCFacebookHelper alloc] init];
    [fh logout];
    
   
    [self removeUserAccessToken];
    [self removeEmailAddress];
    [self removeUsersPhoneNumber];
    [self removeTheKey];
    /*
    [UICKeyChainStore removeItemForKey:kAccessTokenKey];
    self.accessToken=nil;
    [UICKeyChainStore removeItemForKey:kEmailKey];
    self.email = nil;
    [UICKeyChainStore removeItemForKey:kPhoneKey];
    self.phoneNumber = nil;
    [UICKeyChainStore removeItemForKey:@"s_key"];
    self.key = nil;
    */
    
    //[self removeUsersImagePathData];
    
    [self removeUsersImageURL];
   /*
    if ([[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@%@",kImageDataKey,[[User getInstance]fetchAccessToken]]]!=nil
)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@%@",kImageDataKey,[[User getInstance]fetchAccessToken]]];
    }
    */
    [self removeUsersFirstNameAndLastName];
    [self removeUsersGender];
    [self removeUsersBalance];
    
    /*
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kFirstNameKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSurnameKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kGenderKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserBalanceKey];
   */
    
    [self updateTwilioVerified:NO];
    [self removeCreditCardInfo];
    
    [self setLocalSignUp:NO];
    
    
    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:kImageURLKey];
    //[self removeUsersImageURL];
    
    //remove latitude and longitude
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lat"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"long"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    UICKeyChainStore * store = [UICKeyChainStore keyChainStore];
    [store synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshMenuNotification object:nil userInfo:nil];//blur the menu image
    
    [self resetUsersCurrnecy];
    [self resetNumberOfExperiencesFav];
    [self resetNoOfSalonsFav];
    [self resetUsersNeedsToLogOut];
    [self resetIsUserBanned];
    [self resetDoesUserHaveCard];
   
    
    
    
}



/* ====================================================================== */


-(CLLocation *)fetchUsersLocation{
    
   
    NSString * lat;
    NSString * lon;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"lat"]) {
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:@"lat"];
        
    }
    else{
        lat=nil;
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"long"]) {
     
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:@"long"];
    }
    else{
        lon = nil;
    }
    
    //NSLog(@"\n ************** \n Users Lat %@ \n\n long %@ \n ***************** \n",lat,lon);
    
    CLLocation *location;
    if (lat!=nil || lon!=nil) {
         location = [[CLLocation alloc] initWithLatitude:[lat floatValue] longitude:[lon floatValue]];
    }
   
    
    return location;
}

-(void) updateUsersLocationWithLocation : (CLLocation *) loc{
    
    NSString *latitude = [[NSString alloc] initWithFormat:@"%g", loc.coordinate.latitude];
    NSString * longitude = [[NSString alloc] initWithFormat:@"%g", loc.coordinate.longitude];
    [[NSUserDefaults standardUserDefaults] setObject:latitude forKey:@"lat"];
    [[NSUserDefaults standardUserDefaults] setObject:longitude forKey:@"long"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}


/* ================================================================== */


-(BOOL)isUserLoggedIn{
    
    if ([[User getInstance] fetchAccessToken]!=nil) {
        return YES;
    }
    return NO;
}

-(BOOL)hasCreditCard{
 
    //NSLog(@"Credit card NO %@",[[User getInstance] fetchCreditCardNo]);
    if ([[User getInstance] fetchCreditCardNo]==nil) {
        return NO;
    }
    return YES;
}

-(BOOL)hasFirstNameAndLastName{
    
    if ([[User getInstance] fetchFirstName] ==nil || [[User getInstance] fetchLastName]==nil) {
        return NO;
    }
    return YES;
}

-(BOOL)hasEnoughCreditForBooking:(NSString *)price{
    
    return [[User getInstance] isUserLoggedIn] && [[[User getInstance] fetchUsersBalance] integerValue] >=[price integerValue];
    
}

-(void)setInviteCodeToUsed{
    
 
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"inviteCode"];
}
-(BOOL)hasUserUsedInviteCode{
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"inviteCode"];
}

-(BOOL)hasShownTwilio{
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:kHas_Show_Twilio];
}

-(void)markTwilioAsShown: (BOOL) yn{
    
    [[NSUserDefaults standardUserDefaults] setBool:yn forKey:kHas_Show_Twilio];
}

-(BOOL)isTwilioVerified{
   // NSLog(@"Twilio is verified %@",StringFromBOOL([self.isTwilioVerifiedYesNoOrNil boolValue]));
    if (self.isTwilioVerifiedYesNoOrNil==nil) {
        self.isTwilioVerifiedYesNoOrNil = [NSNumber numberWithBool:[[NSUserDefaults standardUserDefaults] boolForKey:kIs_Twilio_verified]];
    }
    
   // return [[NSUserDefaults standardUserDefaults] boolForKey:kIs_Twilio_verified];
    return [self.isTwilioVerifiedYesNoOrNil boolValue];
}

-(void)updateTwilioVerified: (BOOL) yn{
    self.isTwilioVerifiedYesNoOrNil = [NSNumber numberWithBool:yn];
    [[NSUserDefaults standardUserDefaults] setBool:yn forKey:kIs_Twilio_verified];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //NSLog(@"%d is twilio verified %@",yn,StringFromBOOL([self isTwilioVerified]));
}


-(BOOL)canMakeBooking{
    if ([[User getInstance] isUserLoggedIn] && [[User getInstance] isTwilioVerified] && [[User getInstance] hasCreditCard]) {
        return YES;
    }
    return NO;
}

-(NSString *)buttonTitleState{
    
    if (![[User getInstance] isUserLoggedIn]) {
        return @"Login or Sign Up";
    }else if(![[User getInstance] isTwilioVerified]){
        return @"Verify Phone Number";
    }
    else if(![[User getInstance] hasCreditCard]){
        return @"Add Payment Info";
    }
    return @"Book Now";
}

-(NSString *)buttonTitleStateWithServicePrice: (NSString *) servicePrice{
    
    
    double usersBalanceDouble = [[[User getInstance] fetchUsersBalance] doubleValue] ; // extract a double from your text field
    //double usersBalanceDouble = 19.20;
    NSDecimalNumber* usersBalance = [[NSDecimalNumber alloc] initWithDouble:usersBalanceDouble]; //create an NSDecimalNumber using your previously extracted double
    
    double chosenPrice = [servicePrice doubleValue];
    double bookingPrice = (chosenPrice/100)*10;
   
    NSDecimalNumber* bookingPriceDM= [[NSDecimalNumber alloc] initWithDouble:bookingPrice];

    
    NSString *title;
    if (![[User getInstance] isUserLoggedIn]) {
        title= @"Login or Sign Up";
    }
   else  if(![[User getInstance] isTwilioVerified]){
        title= @"Verify Phone Number";
    }
    //booking price is greater than users balance and the user doesnt have a credit card
    else  if([bookingPriceDM compare:usersBalance]==NSOrderedDescending && ![[User getInstance] hasCreditCard]){
        title= @"Add Payment Info";
    }
    
    else{
        title=@"Book Now";
    }
    
    return title;
}

-(int)bookingDestinationScreen{
    
    if (![[User getInstance] isUserLoggedIn]) {
        return 1;
    }else if(![[User getInstance] isTwilioVerified]){
        return 2;
    }
    else if(![[User getInstance] hasCreditCard]){
        return 3;
    }
    return 0;
}


-(BOOL)isLocalSignUp{
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"isLocalSignUp"];
}//temp

-(void)setLocalSignUp: (BOOL) yn{
    
    [[NSUserDefaults standardUserDefaults] setBool:yn forKey:@"isLocalSignUp"];
}//temp

-(BOOL)hasCreditInAccount{
    double usersBalanceDouble = [[self fetchUsersBalance] doubleValue] ; // extract a double from your text field
    NSDecimalNumber* usersBalance = [[NSDecimalNumber alloc] initWithDouble:usersBalanceDouble]; //create an NSDecimalNumber using your previously extracted double
   
    
    if ([usersBalance compare:[NSDecimalNumber zero]] == NSOrderedSame){
        return NO;
    }
    
    return YES;
}

-(BOOL)isSocialSignUp{
   

    return [[NSUserDefaults standardUserDefaults] boolForKey:@"facebookSignUp"];
}


-(void)updateProfile{
    
    if (!self.isFetchingProfile) {
        
        if ([[User getInstance] isUserLoggedIn]) {
            //NSLog(@"Update profile");
            if (self.profileManager ==nil) {
                self.profileManager = [[GCNetworkManager alloc] init];
                self.profileManager.delegate=self;
                self.profileManager.shouldNotShowMessages=YES;
                
            }
            
            NSString * params = [NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]];
            params = [params stringByAppendingString:[NSString stringWithFormat:@"&secret_key=%@",[[User getInstance] fetchKey]]];
            params =[ params stringByAppendingString:[NSString stringWithFormat:@"&device_os=%@",[WSCore fetchOperatingSystem]]];
            params =[ params stringByAppendingString:[NSString stringWithFormat:@"&device_model=%@",[WSCore fetchDeviceName]]];
            
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Profile_Status_URL]];
            
            self.isFetchingProfile=YES;
            [self.profileManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:NO];
            
        }

        
    }
    
    
    
}

-(void)cancelUpdate{
    if (self.isFetchingProfile) {
        [self.profileManager cancelTask];
        self.isFetchingProfile=NO;
    }
}

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    //NSLog(@"*****\n\n\nFailure jsonDIctionary %@ \n\n\n so log out \n\n\n\n\n ********",jsonDictionary);
    
    self.isFetchingProfile=NO;
    [[User getInstance] logout];
}

-(void)manager:(GCNetworkManager *)manager handleServerFailureWithError:(NSError *)error{
    
    self.isFetchingProfile=NO;
    //NSLog(@"**********\n\n\nServer failure %@ \n\n\n ******",error.debugDescription);
}
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
   // NSLog(@"********\n\n\n Sucess json dictionary %@ \n\n\n ******",jsonDictionary);
    if (jsonDictionary[@"message"][@"balance"] !=[NSNull null]) {
        [self updateUsersBalance:jsonDictionary[@"message"][@"balance"]];
    }
    
    
    if ([jsonDictionary[@"message"][@"currency_object"] count] !=0) {
        
        CurrencyItem * currency = [[CurrencyItem alloc] init];
        if (jsonDictionary[@"message"][@"currency_object"][@"conversion_rate_vs_euro"] !=[NSNull null]) {
            currency.conversionRate = jsonDictionary[@"message"][@"currency_object"][@"conversion_rate_vs_euro"];
            
        }
        if (jsonDictionary[@"message"][@"currency_object"][@"currency_name"] !=[NSNull null]) {
            currency.currencyName = jsonDictionary[@"message"][@"currency_object"][@"currency_name"];
            
        }
        if (jsonDictionary[@"message"][@"currency_object"][@"id"] !=[NSNull null]) {
            currency.currencyId = jsonDictionary[@"message"][@"currency_object"][@"id"];
            
        }
        if (jsonDictionary[@"message"][@"currency_object"][@"symbol_utf8"] !=[NSNull null]) {
            NSLog(@"Has currency");
            [ currency updatedCurrencySymbol:[NSString stringWithFormat:@"%@", jsonDictionary[@"message"][@"currency_object"][@"symbol_utf8"] ]];
           // currency.symbolUtf8 = [NSString stringWithFormat:@"%@", jsonDictionary[@"message"][@"currency_object"][@"symbol_utf8"] ];
            
            //NSLog(@"Currency symbol %@",currency.symbolUtf8);
        }
        
       

        self.usersCurrency = currency;
        
    }
    if ([jsonDictionary[@"message"][@"card_details"] count]!=0) {
        if (jsonDictionary[@"message"][@"card_details"][@"card_first_name"]) {
            
            [self updateCreditCardFirstName:jsonDictionary[@"message"][@"card_details"][@"card_first_name"]];
        }
        if (jsonDictionary[@"message"][@"card_details"][@"card_last_name"]!=[NSNull null]) {
            
            [self updateCreditCardLastName:jsonDictionary[@"message"][@"card_details"][@"card_last_name"]];
        }
        if (jsonDictionary[@"message"][@"card_details"][@"card_number"]!=[NSNull null]) {
            
            [self updateCreditCardNo:jsonDictionary[@"message"][@"card_details"][@"card_number"]];
        }
        
        if (jsonDictionary[@"message"][@"card_details"][@"card_type"]!=[NSNull null]) {
            
            [self updateCreditCardType:jsonDictionary[@"message"][@"card_details"][@"card_type"]];
        }
        if (jsonDictionary[@"message"][@"card_details"][@"expiry"]!=[NSNull null]) {
            
            [self updateCreditCardExp:jsonDictionary[@"message"][@"card_details"][@"expiry"]];
        }
        if (jsonDictionary[@"message"][@"card_details"][@"payment_ref"]!=[NSNull null]) {
            
            [self updateCreditCardPaymentRef:[NSString stringWithFormat:@"%@",jsonDictionary[@"message"][@"card_details"][@"payment_ref"]]];
            
        }
    }
    else if ([[User getInstance] hasCreditCard]){
        [self removeCreditCardInfo];
    }

    if (jsonDictionary[@"message"][@"has_card"] !=[NSNull null]) {
        [self updateDoesUserHaveCard:jsonDictionary[@"message"][@"has_card"]
         ];
    }
    
    if (jsonDictionary[@"message"][@"is_banned"] !=[NSNull null]) {
        [self updateIsUserBanned:jsonDictionary[@"message"][@"is_banned"] ];
    }
    
    if (jsonDictionary[@"message"][@"needs_logout"] !=[NSNull null]) {
        [self updateUserNeedsToLogOut:jsonDictionary[@"message"][@"needs_logout"]];
    }
   
    if (jsonDictionary[@"message"][@"no_of_exp_fav"] !=[NSNull null]) {
        [self updateNumberOfExperiencesFavourite:jsonDictionary[@"message"][@"no_of_exp_fav"] ];
    }
    
    if (jsonDictionary[@"message"][@"no_of_salons_fav"] !=[NSNull null]) {
        [self updateNumberOfSalonFavourites:jsonDictionary[@"message"][@"no_of_salons_fav"]];
    }
    if (jsonDictionary[@"message"][@"phone_number"] !=[NSNull null]) {
        [ self updatePhoneNumber:jsonDictionary[@"message"][@"phone_number"]];
    }
    if (jsonDictionary[@"message"][@"secret_key"]!=[NSNull null]) {
        [self updateKey:jsonDictionary[@"message"][@"secret_key"]];
    }
    if (jsonDictionary[@"message"][@"twilio_verified"] !=[NSNull null]) {
        [self updateTwilioVerified:[jsonDictionary[@"message"][@"twilio_verified"] boolValue]];
    }
    if (jsonDictionary[@"message"][@"user_avatar"] !=[NSNull null]) {
        
        [self setImageURL:jsonDictionary[@"message"][@"user_avatar"]];
    }
    
    //check if user should be logged out
    if ([self needsToLogOut]==YES) {
       // NSLog(@"Log out user");
        [[User getInstance] logout];
    }
    
    
    self.isFetchingProfile=NO;
}


#pragma mark - Users Currency
/**
 Grabs the users chosen currency symbol
 @returns NSString representing the users currency
 */

-(NSString *) fetchCurrencySymbol{
    if (self.usersCurrency!=nil) {
        
        return self.usersCurrency.symbolUtf8;
    }
    
   
    return [[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol];
}

/**
 Updates the users currency
 @param cur CurrencyItem
 */

-(void)updateUsersCurrency: (CurrencyItem *) cur{

    self.usersCurrency = cur;
}

/**
 fetches the users currency ( A Currnecy Item)
 @returns CurrencyItem
 */

-(CurrencyItem *)fetchUsersCurrency{
    
    if (self.usersCurrency==nil) {
        self.usersCurrency = [[CurrencyItem alloc] init];
    }
    return self.usersCurrency;
}

/**
 reset Users Currency
 */

-(void)resetUsersCurrnecy{
    self.usersCurrency=nil;
}


#pragma mark - Number of Favourites
/**
 Update the number of favourites Experiences
 @param no NSString representing the number of favourited Experienced items. Uses a string as it will be represented later as a string
 */

-(void)updateNumberOfExperiencesFavourite: (NSString *)no{
    self.noOfExpFav=no;
    
   // NSLog(@"number of exp fav %@",[self fetchStringNumberOfFavouriteExperiences]);
}

/**
 resets the number of experiences favourited
 */

-(void)resetNumberOfExperiencesFav{
    self.noOfExpFav=@"";
}

/**
 fetch the string number of favourites experiences
 @return noOfExpFav - the number of experiences favourited
 */

-(NSString *)fetchStringNumberOfFavouriteExperiences{
    if (self.noOfExpFav.length==0) {
        self.noOfExpFav = @"0";
    }
    return self.noOfExpFav;
}

/**
 Update the number of salon favourites
 @param no NSString representing th enumber of salons favourited
 */

-(void)updateNumberOfSalonFavourites : (NSString *) no{
    self.noOfSalonsFav = no;
    
    //NSLog(@"number of salons fav %@",[self fetchStringNumberOfFavouriteSalons]);
}

/**
 fetch the string number of favourited salons
 @returns noOfSalonsFav NSString representing the number of salons favourited
 */

-(NSString *)fetchStringNumberOfFavouriteSalons{
    
    if (self.noOfSalonsFav.length==0) {
        self.noOfSalonsFav = @"0";
    }

    return self.noOfSalonsFav;
}

/**
 resets the number of salons favourited
 */

-(void)resetNoOfSalonsFav{
    self.noOfSalonsFav=@"";
}
/**
 reset users needs to log out
 */

-(void) resetUsersNeedsToLogOut{
    self.needsLogOutYesNoOrNil=nil;
}

#pragma mark - User needs to logout
/**
 updates whether the user needs to log out or not
 @param logout JSON string representing whether the user needs to log out or not
 */

-(void)updateUserNeedsToLogOut: (NSString *) logout{
    BOOL yN = [logout boolValue];
    self.needsLogOutYesNoOrNil = [NSNumber numberWithBool:yN];
    //NSLog(@"Needs to log out json %@ needs to logout boolean %@",logout, StringFromBOOL([self needsToLogOut]));
}

/**
 returns whether the user needs to log out or not
 @returns bool whether the user needs to log out or not
 */

-(BOOL)needsToLogOut{
    //NSLog(@"****** \n\n\n Log user out = %@ \n\n\n",StringFromBOOL([self.needsLogOutYesNoOrNil boolValue]) );
    
    return [self.needsLogOutYesNoOrNil boolValue];
}
/**
 resets the NSNumber that represents the whether the user needs to log in or log out
 */

-(void)resetDoesUserNeedToLogOut{
    self.needsLogOutYesNoOrNil=nil;
}

#pragma mark - Is the user banned
/**
 resets the NSNumber that represents whether the user is banned or not
 */

-(void)resetIsUserBanned{
    self.isBannedYesNoOrNil=nil;
}
/**
 updates whether the user is banned or not
 @param banned an NSString that represents whether the users account it banned or not
 */

-(void)updateIsUserBanned: (NSString *) banned{
    BOOL yN = [banned boolValue];
    self.isBannedYesNoOrNil = [NSNumber numberWithBool:yN];
    
   // NSLog(@"banned %@ is banned %@ Is user banned %@",banned,self.isBannedYesNoOrNil,StringFromBOOL([self.isBannedYesNoOrNil boolValue]));
}

/**
 states whether the user is banned or not
 @returns bool representing whether the user is band or not
 */

-(BOOL)isUserBanned{
    return [self.isBannedYesNoOrNil boolValue];
}


#pragma mark - Does the user have a card
/**
 update Does the user have a card
 @param card NSString string representing whether the user needs a card or not
 */

-(void)updateDoesUserHaveCard: (NSString *) card{
   
    BOOL yN = [card boolValue];
    self.hasCardYesNoOrNil  = [NSNumber numberWithBool:yN];
    
    
    
}
/**
 conditional statement that checks whether the user has a card or not
 @returns bool representing whether the user has a card or not
 */

-(BOOL)doesUserHaveCard{
    if (self.hasCardYesNoOrNil==nil) {
        NSLog(@"Has card is nil");
       self.hasCardYesNoOrNil = [NSNumber numberWithBool: [self hasCreditCard]];
    }
    
    return [self.hasCardYesNoOrNil boolValue];
}


/**
 resets the NSNumber that states whether a user has a card or not
 */

-(void)resetDoesUserHaveCard{
    self.hasCardYesNoOrNil=nil;

}

-(NSString *) fetchUsersCityAndCountry{
    return self.usersCityAndCounty;
}

-(void)updateUsersCityAndCountry:(NSString *) cityCountry{
    
    self.usersCityAndCounty = cityCountry;
    
}

-(BOOL) hasUsersCityAndCounty{
    if (self.usersCityAndCounty.length>0) {
        return YES;
    }
    
    return NO;
}

-(CLLocation *) fetchFarAwayLocation{
    

    return [[CLLocation alloc] initWithLatitude:53.270668 longitude:-9.056791];
}

-(void)updateGuestUsersLocation:(CLLocation *)gl{
    self.guestUserLocation = gl;
    
    [self updateUsersLocationWithLocation:gl];
}

-(CLLocation *)fetchGuestUsersLocation{
    return self.guestUserLocation;
}
@end
