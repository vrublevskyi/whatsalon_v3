//
//  PolicyDetailViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 22/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "PolicyDetailViewController.h"


@interface PolicyDetailViewController ()

/*! @brief represents the webview. */
@property (weak, nonatomic) IBOutlet UIWebView *webview;

/*! @brief represents the content for the data dictionary. */
@property (strong,nonatomic) NSDictionary * contentDataDictionary;

/*! @brief represents the pages items array. */
@property (nonatomic) NSMutableArray * pagesItemsArray;

@end

@implementation PolicyDetailViewController




- (void)parsePageContentJSON
{
    //parse JSON
    
    id message = [self.contentDataDictionary objectForKey:@"message"];
    if ([message respondsToSelector:@selector(objectAtIndex:)]) {
        NSMutableArray * arrItems = [self.contentDataDictionary objectForKey:@"message"];
        
        [arrItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSLog(@"**%@",[obj objectForKey:@"page_title"]);
            NSLog(@"++ %@",[obj objectForKey:@"page_description"]);
            
            ContentItem * contentItem = [[ContentItem alloc] init];
            contentItem.pageId = [obj objectForKey:@"page_id"];
            contentItem.padeTitle = [obj objectForKey:@"page_title"];
            contentItem.pageDescription = [obj objectForKey:@"page_description"];
            
            [self.pagesItemsArray addObject:contentItem];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Load webpage");
                [self loadWebpage:nil];
            });
        }];
    }
}

- (void)successfulHttpRequest:(NSData *)data
{
    NSError * jsonError;
    
    self.contentDataDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
    
    if (!jsonError) {
        if ([[self.contentDataDictionary objectForKey:@"success"] isEqualToString:@"true"])  {
            
            [self parsePageContentJSON];
            
        }
        else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView * failedAlert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"There was an error conncetion to the API." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [WSCore dismissNetworkLoading];
                
                [failedAlert show];
                
            });
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.pageID isEqualToString:kContent_pageID_WhatCities]) {
        self.title = @"WhatSalon Cities";
    }
    else if([self.pageID isEqualToString:kContent_pageID_cancelPolicy]){
        self.title = @"Cancellation Policy";
    }else{
        self.title= @"Terms & Conditions";
    }
    
    self.pagesItemsArray = [[NSMutableArray alloc] init];
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kContent_URL]];
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url
                              cachePolicy:0 timeoutInterval:90];
    
    
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (data) {
            
            NSHTTPURLResponse * httpResp = (NSHTTPURLResponse *)response;
            
            if (httpResp.statusCode == 200) {
                
                [self successfulHttpRequest:data];//end of jsonError
            }else{
                
                NSLog(@"HTTP error %ld",(long)httpResp.statusCode);
            }
        }
        else{
            [WSCore errorAlert:error];
        }
        
    }];
    
    if ([WSCore isNetworkReachable]) {
        
        [WSCore showNetworkLoading];
        
        [task resume];
    }else{
        
        [WSCore showNetworkErrorAlert];
    }
    
}

-(void)loadWebpage: (NSString *) page_id{
    
    NSString * sContent;
        
        for (int i = 0; i < self.pagesItemsArray.count; i++)
        {
            ContentItem* page = [self.pagesItemsArray objectAtIndex:i];
            if ([page.pageId isEqualToString:self.pageID])
            {
                 sContent = [NSString stringWithFormat:@"<html><body><font face='Helvetica Neue,HelveticaNeue-Light'> %@ </font></body> </html>", page.pageDescription];
                
                break;
            }
    }
    
    [self.webview loadHTMLString:sContent baseURL:nil];
    
    self.webview.backgroundColor = [UIColor clearColor];
    [self.webview setOpaque:NO];

    [WSCore dismissNetworkLoading];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
