//
//  MyBookingsCell.h
//  whatsalon
//
//  Created by admin on 10/2/18.
//  Copyright © 2018 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyBookings.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyBookingsCell : UITableViewCell

@property(strong, nonatomic) MyBookings *booking;

@end

NS_ASSUME_NONNULL_END
