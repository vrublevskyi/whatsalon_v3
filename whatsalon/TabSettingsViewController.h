//
//  LoggedInSettingsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 20/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


/*!
     @protocol SettingsDelegate
  
     @brief The SettingsDelegate protocol
  
     It's a protocol used to notify the delegate of a change that has occured.
 */
@protocol SettingsDelegate <NSObject>

@optional

/*! @brief notifies the delegate that the user has logged out. */
-(void)didLogOut;

/*! @brief notifies the delegate to update the gear icon color. */
-(void)updateGearColor;

@end
@interface TabSettingsViewController : UIViewController

/*! @brief represents the SettingsDelegate object. */
@property (nonatomic) id<SettingsDelegate> delegate;

/*! @brief the action that handles a cancel */

- (IBAction)cancel:(id)sender;

/*! @brief represents the tableview. */
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
