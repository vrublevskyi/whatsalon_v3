//
//  ConfirmReviewViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 15/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "ConfirmReviewViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "User.h"


@interface ConfirmReviewViewController ()<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *confirmationView;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIButton *smsButton;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (nonatomic) UIDynamicAnimator * animator;
@end

@implementation ConfirmReviewViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [WSCore statusBarColor:StatusBarColorWhite];
    
    UIColor * blackColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    self.view.backgroundColor=blackColor;
    
    self.imageView.layer.cornerRadius=3.0;
    self.imageView.layer.masksToBounds=YES;
    self.imageView.image=self.takenImage;
    
    self.overlayView.layer.cornerRadius=3.0;
    self.overlayView.layer.masksToBounds=YES;
    
    [WSCore addTopLine:self.facebookButton :[UIColor lightGrayColor] :0.5];
    [WSCore addTopLine:self.smsButton :[UIColor lightGrayColor] :0.5];
    [WSCore addTopLine:self.emailButton :[UIColor lightGrayColor] :0.5];
    [WSCore addTopLine:self.twitterButton :[UIColor lightGrayColor] :0.5];
    
    [WSCore addRightLineToView:self.facebookButton withWidth:0.5f];
    [WSCore addRightLineToView:self.twitterButton withWidth:0.5f];
    [WSCore addRightLineToView:self.emailButton withWidth:0.5f];
    
    self.confirmationView.layer.cornerRadius=3.0;
    self.confirmationView.layer.masksToBounds=YES;
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    //self.salonName.text=self.booking.salon_name;
    self.salonName.adjustsFontSizeToFitWidth=YES;
    self.salonName.text=self.booking.bookingSalon.salon_name;
    self.stylistName.text =self.booking.stylistName;
    
    
    
    self.confirmButton.backgroundColor=kWhatSalonBlue;
    NSLog(@"Confirm id %@",self.confirmID);
}

-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
    self.confirmationView.frame=CGRectMake(0, -320, self.confirmationView.frame.size.width, self.confirmationView.frame.size.height);
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
    UISnapBehavior * snap = [[UISnapBehavior alloc] initWithItem:self.confirmationView snapToPoint:self.view.center];
    [self.animator addBehavior:snap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancelButton:(id)sender {
    
    [self.animator removeAllBehaviors];
    
    UISnapBehavior * snap = [[UISnapBehavior alloc] initWithItem:self.confirmationView snapToPoint:CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMaxY(self.view.bounds) +188.0f)];
    
    [self.animator addBehavior:snap];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)confirmButton:(id)sender {
    
    [self submitInspireImage];
    
}

- (IBAction)twitter:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *twitterPost = [SLComposeViewController
                                                composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        
        [twitterPost setInitialText:@"WhatSalon!!"];
        [twitterPost addURL:[NSURL URLWithString:@"http://www.whatsalon.com/"]];
        [twitterPost addImage:self.imageView.image];
        
        [self presentViewController:twitterPost animated:YES completion:nil];
        
        [twitterPost setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        
    }
    else{
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Twitter Error" message:@"Cannot post to Twitter, please ensure that you are logged into Twitter in the device settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

}

- (IBAction)faceBook:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *fbPost = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        [fbPost setInitialText:@"Get Inspired With WhatSalon!!"];
        //[fbPost addURL:[NSURL URLWithString:@"http://www.whatsalon.com/"]];
        [fbPost addImage:self.imageView.image];
        
        [self presentViewController:fbPost animated:YES completion:nil];
        
        [fbPost setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
    }
    else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Facebook Error" message:@"Cannot post to Facebook, please ensure that you are logged into Facebook in the device settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)email:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]==YES) {
        // Email Subject
        NSString *emailTitle = @"Get Inspired With WhatSalon!";
        // Email Content
        NSString *messageBody = @"<h1>Join me at WhatSalon!</h1>";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@""];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
       
        NSData *myData =UIImageJPEGRepresentation(self.imageView.image, 1.0);
        [mc addAttachmentData:myData mimeType:@"image/jpeg" fileName:@"GetInspired"];
        [mc setMessageBody:messageBody isHTML:YES];
        [mc setToRecipients:toRecipents];
        
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An email cannot be sent from this device. Please ensure you have set up an email address on your phone." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)message:(id)sender {
    
    MFMessageComposeViewController * text = [[MFMessageComposeViewController alloc] init];
    [text setMessageComposeDelegate:self];
    if ([MFMessageComposeViewController canSendText]) {
        
        [text setRecipients:nil];
        [text setBody:@"Join me on WhatSalon!!"];
        NSData * imgData = UIImageJPEGRepresentation(self.imageView.image, 1.0);
        [text addAttachmentData:imgData typeIdentifier:@"public.data" filename:@"image.png"];
        [self presentViewController:text animated:YES completion:nil];
    }
    else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"The current device is not capable of sending text messages." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    

}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"SMS cancelled");
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            NSLog(@"SMS sent");
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)submitInspireImage{
    
    
    if([self.myDelegate respondsToSelector:@selector(secondViewControllerDismissed:WithImage:)])
    {
        [self.myDelegate secondViewControllerDismissed:YES WithImage:self.takenImage];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==11) {
        if (buttonIndex ==[alertView cancelButtonIndex]) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}


@end
