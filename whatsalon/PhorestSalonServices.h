//
//  PhorestSalonServices.h
//  whatsalon
//
//  Created by Graham Connolly on 20/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhorestSalonServices : NSObject

@property (nonatomic) NSString * salon_id;

-(instancetype) initWithSalonID: (NSString *) s_id;
+(instancetype) servicesListWithSalonID: (NSString *) s_id;
@end
