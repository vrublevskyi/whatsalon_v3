//
//  Therapist.m
//  whatsalon
//
//  Created by Graham Connolly on 07/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "Therapist.h"

@implementation Therapist


-(instancetype) initWithTherapistID:(NSString *)s_id{
    
    self=[super init];
    if (self) {
        self.therapist_id=s_id;
        self.name=@"";
        self.price=@"€10";
        self.pic_url=@"http://test.sms.whatsalon.com/salon_images/353/Adore_2_02_02_2015.jpg";
        self.time=@"10.00a.m";
                
    }
    return self;
}

+(instancetype) bookingWithTherapistID:(NSString *)s_id{
    
    return [[self alloc] initWithTherapistID:s_id];
}

@end
