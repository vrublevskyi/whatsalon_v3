//
//  GCCameraViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 12/10/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GCCameraViewController;

@protocol GCCameraDelegate <NSObject>

@optional
-(void)cameraView: (GCCameraViewController *) cameraView didFinishWithImage:(UIImage *) image;

@end
@interface GCCameraViewController : UIViewController

@property (nonatomic) UIView *frameForCapture;



@property (nonatomic) id<GCCameraDelegate> delegate;
@end
