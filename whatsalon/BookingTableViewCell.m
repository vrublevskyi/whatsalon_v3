//
//  BookingTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 27/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "BookingTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+ColoredImage.h"
#import "UILabel+Boldify.h"
#import "UIColor+FlatUI.h"

#define DEGREES_TO_RADIANS(x) (M_PI * x / 180.0)

//#define cellHeight 85

@implementation BookingTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
       // self.salonImageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, self.contentView.bounds.size.height)];
        //self.salonImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        //[self.contentView addSubview:self.salonImageView];
        self.salonImageView.image=[UIImage imageNamed:@"empty_cell"];
        self.salonImageView.contentMode=UIViewContentModeScaleAspectFill;
        self.salonImageView.layer.cornerRadius=kCornerRadius;
        self.salonImageView.layer.masksToBounds=YES;
       
        
        //self.serviceLabel =[[UILabel alloc] initWithFrame:CGRectMake(self.salonImageView.frame.size.width+10, 10, 180, 21)];
        self.serviceLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:15.0f];
        self.serviceLabel.adjustsFontSizeToFitWidth=YES;
        self.serviceLabel.textColor=[UIColor lightGrayColor];
        [self.contentView addSubview:self.serviceLabel];
        
        //self.salonName = [[UILabel alloc] initWithFrame:CGRectMake(80, 30, 180, 44)];
        self.salonName.text = @"-";
        self.salonName.font = [UIFont  systemFontOfSize:15.0f];
        //self.salonName.adjustsFontSizeToFitWidth=YES;
        self.salonName.numberOfLines=0;
        [self.contentView addSubview:self.salonName];
        self.salonName.center = CGPointMake(self.salonName.center.x, self.salonName.center.y);
        
       // self.starsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(80, 75, 84, 21)];
        self.starsImageView.autoresizingMask=UIViewAutoresizingFlexibleWidth;
        ;
        self.starsImageView.image=[UIImage imageNamed:@"fourStar"];
        self.starsImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.starsImageView];
        
        //self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - 90, 5, 80, 40)];
        self.dateLabel.text=@" - ";
        self.dateLabel.textAlignment=NSTextAlignmentRight;
        self.dateLabel.numberOfLines=0;

        self.dateLabel.font = [UIFont boldSystemFontOfSize:12.0f];
        [self.contentView addSubview:self.dateLabel];
        
        //self.bookingRefLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-100, 55, 90, 44)];
        self.bookingRefLabel.textAlignment=NSTextAlignmentRight;
        self.bookingRefLabel.text=@"-";
        self.bookingRefLabel.numberOfLines=0;
        self.bookingRefLabel.textColor=[UIColor lightGrayColor];
        
        self.clipsToBounds=YES;
        [self.contentView addSubview:self.bookingRefLabel];
        
        
       
    
        self.salonName.alpha=0.5;
        self.serviceLabel.text=@"Cancel";
        self.cancelTitle.hidden=YES;
        self.cancelView.hidden=YES;

        
    }
    return self;
}

-(void)setCellImageWithURL: (NSString*) url{
    
    if (url.length <1) {
        self.salonImageView.image = kPlace_Holder_Image;
        self.salonImageView.contentMode=UIViewContentModeScaleAspectFill;
        self.salonImageView.layer.cornerRadius=kCornerRadius;
        self.salonImageView.layer.masksToBounds=YES;
        
    }
    else{
        [self.salonImageView sd_setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:kPlace_Holder_Image];
        self.salonImageView.contentMode=UIViewContentModeScaleAspectFill;
        self.salonImageView.layer.cornerRadius=kCornerRadius;
        self.salonImageView.layer.masksToBounds=YES;
    }
    
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)updateCellWithBooking: (MyBookings *) booking{
    
   
    //bookings = [self.bookingsArray objectAtIndex:indexPath.row];
    self.serviceLabel.text = booking.servceName;
    CGRect fullFrame;
    if (IS_IOS_8_OR_LATER) {
         fullFrame = self.frame;
    }
    else{
        fullFrame = self.frame;
    }
  
    if (booking.isCancelled==YES) {
         NSLog(@"booking %@",StringFromBOOL(booking.isCancelled));
        //self.cancelView = [[UIView alloc]initWithFrame:self.frame];
        //[self.contentView addSubview:self.cancelView];
        self.cancelView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.7];
        self.cancelView.frame=fullFrame;
        self.cancelTitle.frame = fullFrame;
        self.cancelTitle.hidden=NO;
        self.cancelView.hidden=NO;
        self.cancelView.clipsToBounds=YES;
        self.cancelView.layer.masksToBounds=YES;
        [self bringSubviewToFront:self.cancelView];
        
        //self.cancelTitle =[[UILabel alloc] initWithFrame:self.cancelView.frame];
        self.cancelTitle.text = @"Cancelled";
        self.cancelTitle.font = [UIFont boldSystemFontOfSize:20.0f];
        self.cancelTitle.textColor = [UIColor alizarinColor];
        self.cancelTitle.textAlignment=NSTextAlignmentCenter;
        //[self.contentView addSubview:self.cancelTitle];
        //[self.contentView bringSubviewToFront:self.cancelTitle];
        self.cancelTitle.center=self.cancelView.center;
        self.cancelTitle.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(315));
    }
    else{
        //[self.cancelView removeFromSuperview];
        //[self.cancelTitle removeFromSuperview];
        self.cancelView.hidden=YES;
        self.cancelTitle.hidden=YES;
        self.cancelTitle.text=@"";
        self.salonName.alpha=1.0;
    }

    NSLog(@"Cancel view frame %@",NSStringFromCGRect(fullFrame));
    if (booking.stylistName.length!=0) {
        self.salonName.text = [NSString stringWithFormat:@"%@ \nwith %@",booking.bookingSalon.salon_name,booking.stylistName];
        self.salonName.adjustsFontSizeToFitWidth=YES;
        self.salonName.numberOfLines=0;
    }else{
        self.salonName.text = booking.bookingSalon.salon_name;
    }
    
    
    [self.salonImageView sd_setImageWithURL:[NSURL URLWithString:booking.bookingSalon.salon_image] placeholderImage:[UIImage imageNamed:kPlace_Holder_Cell_Image]];
    self.salonImageView.layer.cornerRadius=kCornerRadius;
    self.salonImageView.layer.masksToBounds=YES;
    self.salonImageView.layer.borderColor=[UIColor silverColor].CGColor
    ;
    self.salonImageView.layer.borderWidth=1.0f;
    
    self.dateLabel.adjustsFontSizeToFitWidth=YES;
    self.dateLabel.textColor=[UIColor lightGrayColor];
    
    
    
    NSString *date = [NSString stringWithFormat:@"%@",booking.bookingStartTime];
    if (date.length>6 && ![date isEqualToString:@"NOW"]) {
        
        NSDateFormatter *dformat = [[NSDateFormatter alloc]init];
        [dformat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [dformat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *date = [dformat dateFromString:booking.bookingStartTime];
        
        [dformat setDateFormat:@"h:mm a"];
        NSString * time = [dformat stringFromDate:date];
        [dformat setDateFormat:@"dd MMM"];
        NSString * dateStr = [dformat stringFromDate:date];
        
        //date = [date substringToIndex:6];
        self.dateLabel.text =   [NSString stringWithFormat:@"%@ \n %@",time,dateStr];
        self.dateLabel.numberOfLines=0;
    }
    else{
        self.dateLabel.text=@"";
    }
    
    self.starsImageView.image =[WSCore getStarRatingImage:0];
    self.starsImageView.image = [self.starsImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.starsImageView setTintColor:kWhatSalonBlue];
    self.starsImageView.tintColor=kWhatSalonBlue;
    
    //adds a sepeartor
    UILabel * seperator = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height-0.5,self.contentView.frame.size.width,0.5)];
    seperator.backgroundColor = [UIColor blackColor];
    [self.contentView addSubview:seperator];
    
    CGRect priceFrame = self.bookingRefLabel.frame;
    
    if (!self.isPrevious) {
        self.starsImageView.hidden=YES;
        
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:[NSLocale currentLocale]];
        NSNumber* number = [numberFormatter  numberFromString:booking.bookingDeposit];
        if (!number) {
            [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
            number = [numberFormatter  numberFromString:booking.bookingDeposit];
        }
        NSNumber* number2 = [numberFormatter  numberFromString:booking.bookingTotal];
        if (!number2) {
            [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
            number2 = [numberFormatter  numberFromString:booking.bookingTotal];
        }
        float leftToPay =[number2 doubleValue] - [number doubleValue];
        
        //self.bookingRefLabel.text = [NSString stringWithFormat:@"%@%.2f due",CURRENCY_SYMBOL,leftToPay];
        
         self.bookingRefLabel.text = [NSString stringWithFormat:@"%@%.2f due",booking.bookingSalon.currencySymbolUtf8,leftToPay];
        
        
        self.bookingRefLabel.font = [UIFont boldSystemFontOfSize:15.0f];
        self.
       // [self.bookingRefLabel boldSubstring:[NSString stringWithFormat:@"%@%.2f due",CURRENCY_SYMBOL,leftToPay]];
        self.bookingRefLabel.textColor=[UIColor alizarinColor];
        priceFrame =CGRectMake(self.salonImageView.frame.size.width+10, self.frame.size.height-30, 300, 25);
        self.bookingRefLabel.frame=priceFrame;
        self.bookingRefLabel.textAlignment=NSTextAlignmentLeft;
    }
    else{
        self.starsImageView.hidden=NO;
        
        
        //self.bookingRefLabel.text = [NSString stringWithFormat:@"%@%@ total paid",CURRENCY_SYMBOL,booking.bookingTotal];
         self.bookingRefLabel.text = [NSString stringWithFormat:@"%@%@ total paid",booking.bookingSalon.currencySymbolUtf8,booking.bookingTotal];
        
        
        self.bookingRefLabel.textAlignment=NSTextAlignmentRight;
        self.bookingRefLabel.textColor=[UIColor lightGrayColor];
        
        self.bookingRefLabel.frame=priceFrame;
        priceFrame =CGRectMake(self.frame.size.width-100, 55, 90, 44);
        self.bookingRefLabel.frame=priceFrame;
        
        self.starsImageView.image=[WSCore getStarRatingImage:self.salon.ratings];
        
        self.starsImageView.image = [self.starsImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        if (self.salon.ratings==0) {
            [self.starsImageView setTintColor:kBackgroundColor];
        }else{
            [self.starsImageView setTintColor:kWhatSalonBlue];
        }
    }
    
    self.bookingRefLabel.font = [UIFont boldSystemFontOfSize:self.bookingRefLabel.font.pointSize];
    
    self.starsImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.bookingRefLabel.textAlignment=NSTextAlignmentRight;
    
}
@end
