//
//  SearchTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 09/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell

/*! @brief represents the salon image view. */
@property (nonatomic,strong) UIImageView * salonImage;

/*! @brief represents the salon name label. */
@property (nonatomic,strong) UILabel * salonName;

/*! @brief represents the salon address label. */
@property (nonatomic,strong) UILabel * salonAddress;

/*! @brief represents the search term that was searched. */
@property (nonatomic) NSString * searchTerm;

/*! @brief sets up the cell image using the url string pointer to the image. */
-(void)setCellImageWithURL: (NSString *)url;

/*! @brief sets up the cell with the Salon object. */
-(void) setUpCellWithSalon: (Salon *)salon;
@end
