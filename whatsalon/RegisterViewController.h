//
//  RegisterViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 22/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RegisterViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;
@property (weak, nonatomic) IBOutlet UIView *viewForEmailTF;
@property (weak, nonatomic) IBOutlet UIView *viewForPasswordTF;
@property (weak, nonatomic) IBOutlet UIView *viewForPhoneNumber;
@property (weak, nonatomic) IBOutlet UIView *smsVerificationView;
@property (weak, nonatomic) IBOutlet UIButton *areaCodeButton;
@property (weak, nonatomic) IBOutlet UITextField *emailAddressTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UIView *viewForAreaCodePicker;
@property (nonatomic,assign) BOOL isCountryCodeViewShowing;
@property (nonatomic,strong) NSMutableArray * countryCodes;
@property (nonatomic,strong) NSMutableArray * countries;
@property (strong,nonatomic) IBOutlet UIPickerView * countryCodePicker;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *smsVerifcationLabel;
@property (weak, nonatomic) IBOutlet UITextField *vericationCodeTextField;
@property (weak, nonatomic) UIBarButtonItem *resendMessageBarButton;
@property (strong, nonatomic) IBOutlet UINavigationItem *myNavigationItem;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic,assign) BOOL isTier1Booking;
- (IBAction)signUp:(id)sender;
- (IBAction)cancelRegistration:(id)sender;
- (IBAction)changeAreaCode:(id)sender;
- (IBAction)verifySignUp:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *firstNameView;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UIView *lastNameView;
@property (weak, nonatomic) IBOutlet UITextField *lastName;

@property (nonatomic) BOOL isFromSettings;
@property (nonatomic) BOOL isFromLocalSignUpBookingScreen;
@end
