//
//  CalloutEnterAddressViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 08/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol EnterAddressDelegate <NSObject>

@optional
-(void)didUpdateAddress: (NSString *) address;

@end
@interface CalloutEnterAddressViewController : UIViewController
@property (nonatomic,assign) id<EnterAddressDelegate> delegate;

@end
