//
//  TermsAndConditionsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 20/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WhatSalonDotComViewController : UIViewController <UIWebViewDelegate>

/*! @brief represents the webview for hosting the website. */
@property (strong, nonatomic) IBOutlet UIWebView *webView;

/*! @brief represents the activity indicator. */
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

/*! @brief represents the page title. */
@property (weak,nonatomic) NSString * pageTitle;

/*! @brief represents the url string. */
@property (weak,nonatomic) NSString * urlString;
@end
