//
//  FriendsInspireViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 26/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FriendsInspireViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *connectFB_btn;

- (IBAction)connectToFB:(id)sender;

@end
