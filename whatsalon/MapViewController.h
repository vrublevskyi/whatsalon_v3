//
//  MapViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 08/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ProfileAnnotationView.h"


@interface MapViewController : UIViewController
@property (strong, nonatomic) IBOutlet MKMapView *map;
@property (nonatomic) NSMutableArray * salonArray;
@property (nonatomic) Salon*  selelctedSalon;

@property (weak, nonatomic) IBOutlet UIButton *seeDIrectoryButton;
- (IBAction)seeDirectory:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *seeDirectoryDescription;

@end
