//
//  LoginPageViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 19/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LoginDelegate <NSObject>

-(void)Tier1Login;

@end
@interface LoginPageViewController : UIViewController
@property (nonatomic, assign) id<LoginDelegate>  myDelegate;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *userAvatar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *CancelBarButton;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigationBar;
@property (weak, nonatomic) IBOutlet UITextField *emailAddressTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (nonatomic,assign) BOOL isTier1Booking;
@property (nonatomic,assign) BOOL isTier2Booking;
@property (nonatomic,assign) BOOL isMapBooking;

- (IBAction)cancel:(id)sender;
- (IBAction)login:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end
