//
//  UIView+AlertCompatibility.h
//  whatsalon
//
//  Created by Graham Connolly on 07/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//
//  adapted from here: http://stackoverflow.com/a/26126909/2654425
//

/*!
 @header UIView+AlertCompatibility.h
  
 @brief This is the header file for the UIView+AlertCompatibility.h.
  
 This file is a handy category for supporting basic alerts for iOS7 & iOS8+.
 
 iOS8 deprecated UIAlertView
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    1.0.1
 */

#import <UIKit/UIKit.h>

@interface UIView (AlertCompatibility)

/*
 @brief creates a simple Alert view and displays it.
 
 @param title - the title of the Alert.
 
 @param message - the message of the Alert.
 
 @param cancelButtonTitle - the title of the cancel button title.
 
 */
+( void )showSimpleAlertWithTitle:( NSString * )title
                          message:( NSString * )message
                cancelButtonTitle:( NSString * )cancelButtonTitle;
@end
