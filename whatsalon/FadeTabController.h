//
//  FadeTabController.h
//  whatsalon
//
//  Created by Graham Connolly on 10/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FadeTabController : UITabBarController

@end
