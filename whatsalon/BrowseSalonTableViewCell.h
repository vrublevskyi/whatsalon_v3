//
//  BrowseSalonTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 20/11/2015.
//  Copyright © 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header BrowseSalonTableViewCell.h
  
 @brief This is the header file is for the BrowseSalonTableViewCell featured in the TabBrowseViewController.h
  
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd
 @version
 */
#import <UIKit/UIKit.h>

@protocol BrowseSalonTableViewCellDelegate <NSObject>

@optional
-(void)didClickOnMapPin:(Salon* )salon;

-(void)didAddFavourite:(Salon *)salon;

-(void)didUnFavourite: (Salon *)salon;

-(void)didClickIntantBookFor: (Salon *)salon;
@end
@interface BrowseSalonTableViewCell : UITableViewCell<GCNetworkManagerDelegate>


/*! @brief the envelope/bubble behind the salon information*/

/*! @brief the BrowseSalonTableViewCellDelegate */
@property (nonatomic) id<BrowseSalonTableViewCellDelegate> myDelegate;

-(void)setUpCellWithSalon: (id) s;

@property (nonatomic) BOOL isNearest;

/*!
 @brief determines if the cell should have an instant booking label or not
 */
@property (nonatomic) BOOL hasInstantLabel;
@end
