//
//  TabDiscoveryViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 30/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

/*!
 @header TabDiscoveryViewController.h
  
 @brief This is the header file for TabDiscoveryViewController.h
  
  
 @author Graham Connolly
 @copyright  2015 WhatApplications Ltd.
 @version    10+
 */
#import <UIKit/UIKit.h>


@interface TabDiscoveryViewController : UIViewController
@end
