//
//  ServicesTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 10/06/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalonService.h"

@interface ServicesTableViewCell : UITableViewCell

@property (nonatomic) UILabel * serviceNameLabel;
@property (nonatomic) UILabel * priceLabel;
@property (nonatomic) NSString *currencySymbol;

-(void) setUpServicesTableViewCellWithPhorestService: (id) ps;
@end
