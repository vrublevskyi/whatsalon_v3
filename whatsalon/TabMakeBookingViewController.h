//
//  TabMakeBookingViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 27/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header TabMakeBookingViewController.h
  
 @brief This is the header file for TabMakeBookingViewController.h
  
  
 @author Graham Connolly
 @copyright  2015 What Application Ltd.
 @version    10+.
 */
#import <UIKit/UIKit.h>

@interface TabMakeBookingViewController : UIViewController

/*! @brief represents the salon object. */
@property (nonatomic) Salon * salonData;

/*! @brief an action for selecting the service. */
- (IBAction)selectService:(id)sender;

/*! @brief an action for selecting the stylist. */
- (IBAction)selectStylist:(id)sender;

/*! @brief an action for selecting the time. */
- (IBAction)selectTime:(id)sender;

/*! @brief represents the background image. */
//@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

/*! @brief determines a booking is being made from the favourites screen. */
@property (nonatomic) BOOL isFromFavourite;

/*! @brief determines if a booking is being made via the Search Filter screen. i.e unwind to the search filter after the booking has taken place.
 */
@property (nonatomic) BOOL unwindBackToSearchFilter;
@end
