//
//  DiscoveryServicesTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 12/01/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "DiscoveryServicesTableViewCell.h"



@implementation DiscoveryServicesTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       
        self.mainImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        self.mainImage.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.mainImage.image=[UIImage imageNamed:@"nails.jpg"];
        [self.contentView insertSubview:self.mainImage atIndex:0];
        
        UIView * grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.bounds.size.width, self.contentView.bounds.size.height)];
        grayView.backgroundColor = [UIColor darkGrayColor];
        grayView.alpha = 0.3;
        grayView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        [self.contentView insertSubview:grayView atIndex:1];
        
        UIView * holderView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, 320, 100)];
        holderView.backgroundColor = [UIColor clearColor];
        holderView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.contentView insertSubview:holderView atIndex:2];
        holderView.center = self.contentView.center;
        
        
       // UILabel * todayLabel = [[UILabel alloc] initWithFrame:CGRectMake(46, 50, 110, 35)];
        UILabel * todayLabel = [[UILabel alloc] initWithFrame:CGRectMake(46, 60, 110, 35)];
        todayLabel.text = @"Today";
        todayLabel.textColor = [UIColor whiteColor];
        todayLabel.textAlignment = NSTextAlignmentCenter;
        
        //todayLabel.center = CGPointMake(self.contentView.center.x, todayLabel.center.y);
        todayLabel.backgroundColor=kWhatSalonBlue;
        todayLabel.alpha=0.8;
        todayLabel.layer.cornerRadius=3.0;
        todayLabel.layer.masksToBounds=YES;
        
        //[self.contentView insertSubview:todayLabel atIndex:2];
        [holderView addSubview:todayLabel];
        
        //UILabel * tomorrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(164, 50, 110, 35)];
        UILabel * tomorrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(164, 60, 110, 35)];
        tomorrowLabel.text = @"Tomorrow";
        tomorrowLabel.textColor = [UIColor whiteColor];
        tomorrowLabel.textAlignment = NSTextAlignmentCenter;
        tomorrowLabel.alpha=0.8;
        tomorrowLabel.layer.cornerRadius=3.0;
        tomorrowLabel.layer.masksToBounds=YES;
       
        tomorrowLabel.backgroundColor=kWhatSalonBlue;
        
        //[self.contentView insertSubview:tomorrowLabel atIndex:2];
        [holderView addSubview:tomorrowLabel];
        
        //self.titleLabel =[[UILabel alloc] initWithFrame:CGRectMake(0, 15, 320, 25)];
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
        self.titleLabel.text = @"Need to find a last minute nails appointment?";
        self.titleLabel.numberOfLines=2;
       // self.titleLabel.font = [UIFont boldSystemFontOfSize:24.0f];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        self.titleLabel.textColor = [UIColor whiteColor];
      //  self.titleLabel.adjustsFontSizeToFitWidth=YES;
        self.titleLabel.textAlignment=NSTextAlignmentCenter;
        //self.titleLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        //[self.titleLabel setCenter:CGPointMake(self.titleLabel.center.x, self.contentView.center.y)];
        
        //[self.contentView insertSubview:self.titleLabel atIndex:2];
        [holderView addSubview:self.titleLabel];
        
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, DiscoveryCellHeight-55, 50, 35)];
        self.priceLabel.adjustsFontSizeToFitWidth=YES;
        self.priceLabel.backgroundColor=[UIColor blackColor];
        self.priceLabel.textColor=[UIColor whiteColor];
        self.priceLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        self.priceLabel.numberOfLines=2;
        self.priceLabel.textAlignment=NSTextAlignmentCenter;
        self.priceLabel.text = [NSString stringWithFormat:@"From %@",self.price];
        self.priceLabel.alpha=0.8;
        
        [self.contentView insertSubview:self.priceLabel atIndex:2];
        
        self.contentView.clipsToBounds=YES;
        self.clipsToBounds = YES;
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPriceOfService:(NSString *)price{
    self.priceLabel.text = [NSString stringWithFormat:@"From €%@",price];
}
-(void)setCellImageWithURL: (NSString *)url{
    
}
@end
