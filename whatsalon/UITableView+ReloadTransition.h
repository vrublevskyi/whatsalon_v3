//
//  UITableView+ReloadTransition.h
//  whatsalon
//
//  Created by Graham Connolly on 25/07/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (ReloadTransition)
- (void)reloadDataWithFade:(BOOL)animated;
- (void)reloadDataAnimated:(BOOL)animated;
@end
