//
//  UIView+AlertCompatibility.m
//  whatsalon
//
//  Created by Graham Connolly on 07/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "UIView+AlertCompatibility.h"

@implementation UIView (AlertCompatibility)


+( void )showSimpleAlertWithTitle:( NSString * )title
                          message:( NSString * )message
                cancelButtonTitle:( NSString * )cancelButtonTitle
{
    
    if( ![UIAlertController class])
    {
     
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: title
                                                        message: message
                                                       delegate: nil
                                              cancelButtonTitle: cancelButtonTitle
                                              otherButtonTitles: nil];
        [alert show];
    
    }
    else
    {
        
   
        // nil titles break alert interface on iOS 8.0, so we'll be using empty strings
        UIAlertController *alert = [UIAlertController alertControllerWithTitle: title == nil ? @"": title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle: cancelButtonTitle
                                                                style: UIAlertActionStyleDefault
                                                              handler: nil];
        
        [alert addAction: defaultAction];
        
        
        UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        while (topViewController.presentedViewController) {
            topViewController = topViewController.presentedViewController;
        }
        [topViewController presentViewController:alert animated:YES completion:nil];
        
       
    }
    
}


@end
