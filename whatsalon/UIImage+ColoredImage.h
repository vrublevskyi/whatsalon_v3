//
//  UIImage+ColoredImage.h
//  whatsalon
//
//  Created by Graham Connolly on 27/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ColoredImage)

+ (UIImage *)coloredImage_resizeableImageWithColor:(UIColor *)color;
@end
