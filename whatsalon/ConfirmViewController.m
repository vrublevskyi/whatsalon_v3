//
//  ConfirmViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 29/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "ConfirmViewController.h"
#import "UIImage+ImageEffects.h"
#import "CongratulationsScreenViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SalonBookingModel.h"
#import "UIView+AlertCompatibility.h"


@interface ConfirmViewController ()<ServicesDelegate>

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tutorialTraceBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *traceBottom;
@property (nonatomic,assign) BOOL isLogoTraced;
@property (nonatomic,assign) float logoToTraceOriginY;
@property(nonatomic) NSArray * xArray;
@property (nonatomic) NSArray *yArray;

@property(nonatomic) NSInteger count;
@property (nonatomic,assign) BOOL hasStarted;
@property (nonatomic,assign) BOOL isFinished;

@property (nonatomic) SalonBookingModel * salonBookingModel;


@end

@implementation ConfirmViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    [self navigationBarSetUp];
    
    
    
    
    NSLog(@"image url %@",[[self.salonData fetchSlideShowGallery
                           ] objectAtIndex:0]);
    [self.backgroundImageView sd_setImageWithURL:[[self.salonData fetchSlideShowGallery
                                         ] objectAtIndex:0]];

    
    //[WSCore addDarkBlurAsSubviewToView:self.backgroundImageView];
    UIView * blackView = [[UIView alloc] initWithFrame:self.backgroundImageView.frame];
    blackView.backgroundColor=[UIColor blackColor
                               ];
    blackView.alpha=0.7;
    [self.backgroundImageView addSubview:blackView];

    
    
    self.salonNameLabel.text = self.salonData.salon_name;
    self.salonNameLabel.adjustsFontSizeToFitWidth=YES;
    self.addressNameLabel.text = [self.salonData fetchFullAddress];
    self.addressNameLabel.adjustsFontSizeToFitWidth=YES;
    self.serviceNameLabel.text = self.bookingObj.chosenService;
    self.serviceNameLabel.adjustsFontSizeToFitWidth=YES;
    self.stylistNameLabel.text = self.bookingObj.chosenStylist;
    self.stylistNameLabel.adjustsFontSizeToFitWidth=YES;
    self.fullPriceAmountLabel.text = [NSString stringWithFormat:@"%@%@",CURRENCY_SYMBOL,self.bookingObj.chosenPrice];
    self.fullPriceAmountLabel.adjustsFontSizeToFitWidth=YES;
    double discount = [self.bookingObj.chosenPrice doubleValue];
    discount = discount/100*90;
    self.priceAtSalonLabel.text = [NSString stringWithFormat:@"%@%.2lf",CURRENCY_SYMBOL,discount];
    self.discountAmountLabel.text = [NSString stringWithFormat:@"%@%.2lf",CURRENCY_SYMBOL,[self.bookingObj.chosenPrice doubleValue]/100*10];
    self.discountAmountLabel.textColor=kWhatSalonBlue;
    self.discountAmountLabel.font=[UIFont boldSystemFontOfSize:18.0f];
    self.dateSelectedLabel.text = self.bookingObj.chosenDay;
    
    [WSCore addBottomLine:self.viewForNavigationBar :[UIColor whiteColor]];
    [WSCore addBottomLine:self.viewForPrice :[UIColor whiteColor]];
    [WSCore addBottomLine:self.viewForSalonInfo :[UIColor whiteColor]];
    [WSCore addBottomLine:self.viewForSalonStylist :[UIColor whiteColor]];
    
    self.viewForPrice.backgroundColor = [UIColor clearColor];
    self.viewForSalonStylist.backgroundColor = [UIColor clearColor];
    self.viewForSalonInfo.backgroundColor = [UIColor clearColor];
    
    self.salonNameLabel.textColor = [UIColor whiteColor];
    self.addressNameLabel.textColor = [UIColor whiteColor];
    self.serviceNameLabel.textColor = [UIColor whiteColor];
    self.stylistNameLabel.textColor = [UIColor whiteColor];
    self.dateSelectedLabel.textColor = [UIColor whiteColor];
    self.fullPriceAmountLabel.textColor = [UIColor whiteColor];
    self.priceAtSalonLabel.textColor = [UIColor whiteColor];
    //self.discountAmountLabel.textColor = [UIColor whiteColor];
    
    self.logoToTraceOriginY = 406;
    
    
    
    UILabel * traceLabel = [[UILabel alloc] initWithFrame:CGRectMake(51, 346, 218, 47)];
    traceLabel.textAlignment=NSTextAlignmentCenter;
    traceLabel.text = @"If everything is OK, trace the logo to reveal the Confirm & Book button";
    traceLabel.font=[UIFont systemFontOfSize:13.0f];
    traceLabel.textColor = [UIColor whiteColor];
    traceLabel.numberOfLines=2;
    traceLabel.adjustsFontSizeToFitWidth=YES;
    //self.instructionsToTraceLabel = traceLabel;
    //[self.view addSubview:self.instructionsToTraceLabel];
    
    UIImageView * trace = [[UIImageView alloc] initWithFrame:CGRectMake(108, 406, 105, 141)];
    trace.contentMode=UIViewContentModeScaleToFill;
    trace.image = [UIImage imageNamed:@"wsbutton"];
    //self.traceImageView = trace;
    //[self.view addSubview:self.traceImageView];
    
    UIImageView * traceT = [[UIImageView alloc] initWithFrame:CGRectMake(108, 406, 105, 141)];
    traceT.contentMode=UIViewContentModeScaleToFill;
    traceT.image = [UIImage imageNamed:@"tutorial0001"];
    //self.traceTutorialImageView = trace;
    //[self.view insertSubview:self.traceTutorialImageView belowSubview:self.traceImageView];

    if (IS_iPHONE4) {
        self.traceImageView.hidden=YES;
        self.traceTutorialImageView.hidden=YES;
        self.confirmButton.hidden=NO;
        self.confirmButton.alpha=1.0;
        self.instructionsToTraceLabel.hidden=YES;
        
    }
    else{
        self.confirmButton.hidden=YES;
        self.confirmButton.alpha=0.0;
        
        
        self.traceTutorialImageView.alpha=0.8;
        self.traceImageView.userInteractionEnabled=YES;
        
        self.traceTutorialImageView.animationImages = [NSArray arrayWithObjects:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0001" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0002" ofType:@"png"]],
            [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0003" ofType:@"png"]],
            [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0004" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0005" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0006" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0007" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0008" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0009" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0010" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0011" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0022" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0023" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0024" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0025" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0026" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0027" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0028" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0029" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0030" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0031" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0032" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0033" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0034" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0035" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0036" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0037" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0038" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0039" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0040" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0041" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0042" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0043" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0044" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0045" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0046" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0047" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0048" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0049" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0050" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0051" ofType:@"png"]],[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0052" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0053" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0054" ofType:@"png"]],
                                                       [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"tutorial0055" ofType:@"png"]],nil];
                                                       
                                                       
        
        
        
        
               
        self.traceTutorialImageView.animationDuration=1.5f;
        self.traceTutorialImageView.animationRepeatCount=1;
        
        self.xArray = @[@253.0f,
                        @258.0f,@258.0f,@262.0f,@277.0f,@266.0f,@266.0f,@265.0f,
                        @264.0f,@260.0f,@255.0f,@250.0f,@242.0f,@224.0f,@210.0f,@188.0f,
                        @180.0f,@172.0f,@156.0f,@144.0f,@134.0f,@116.0f,@98.0f,@80.0f,@67.0f,
                        @65.0f,@62.0f,@62.0f,@63.0f,@63.0f,@67.0f,
                        @71.0f,@74.0f,@79.0f,@90.0f,@97.0f,@104.0f,@118.0f,@126.0f,
                        @139.0f,@149.0f,@158.0f,@171.0f,@177.0f,@188.0f,@191.0f,@199.0f,
                        @204.0f,@206.0f,@207.0f,@205.0f,@204.0f,@200.0f,@194.0f,
                        @184.0f,@174.0f,@161.0f,@147.0f,@136.0f,@128.0f,@118.0f,@103.0f];
        
        self.yArray = @[@139.0f,@127.0f,@119.0f,@107.0f,@102.0f,@94.0f,@83.0f,@74.0f,@67.0f,
                        @63.0f,@53.0f,@45.0f,@37.0f,@29.0f,@23.0f,@17.0f,@17.0f,@18.0f,@20.0f,@24.0f,@21.0f,
                        @31.0f,@45.0f,@58.0f,@71.0f,@83.0f,@97.0f,
                        @114.0f,@130.0f,@138.0f,@147.0f,@164.0f,
                        @172.0f,@180.0f,@198.0f,@208.0f,@214.0f,@230.0f,@239.0f,@248.0f,@255.0f,
                        @266.0f,@276.0f,@284.0f,@296.0f,@299.0f,@310.0f,@324.0f,@336.0f,
                        @344.0f,@354.0f,@364.0f,@375.0f,@385.0f,
                        @386.0f,@396.0f,@396.0f,@396.0f,@395.0f,@389.0f,@383.0f,@366.0f];
        
        self.count = 0;
        
        self.isFinished=NO;

        [self performSelector:@selector(animateTutorial) withObject:self afterDelay:1.5f];
        
        [self performSelector:@selector(animateTutorial) withObject:self afterDelay:6.5f];

    }

    
    self.salonBookingModel = [[SalonBookingModel alloc] init];
    self.salonBookingModel.view = self.view;
    self.salonBookingModel.myDelegate=self;
 
    [self.confirmButton setTitle:[NSString stringWithFormat:@"Confirm & Pay %@%.2lf Deposit",CURRENCY_SYMBOL,[self.bookingObj.chosenPrice doubleValue]/100*10] forState:UIControlStateNormal];
    
}

-(void)animateTutorial{
    NSLog(@"Animate tutorial");
    if (!self.hasStarted) {
        NSLog(@"Start animation");
        [self.traceTutorialImageView startAnimating];
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [super touchesMoved:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self.traceImageView];
    
    self.hasStarted=YES;
    CGFloat xFloat = currentPoint.x;
    CGFloat yFloat = currentPoint.y;
    
    
    if (self.isFinished==NO) {
        if (_count<=_xArray.count-1) {
            if ((xFloat <=[[_xArray objectAtIndex:_count] floatValue]/3.0f + 30.0f && xFloat >= [[_xArray objectAtIndex:_count] floatValue]/3.0f - 30.0f) && (yFloat <= [[_yArray objectAtIndex:_count] floatValue]/3.0f + 30.0f && yFloat >= [[_yArray objectAtIndex:_count] floatValue]/3.0f - 30.0f)) {
              
                _count ++;
                NSString *fileLocation = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"wsbutton_%ld",(long)_count] ofType:@"png"];
                UIImage* yourImage = [[UIImage alloc] initWithContentsOfFile:fileLocation] ;
                self.traceImageView.image = yourImage;
                while (_count>=56&& _count<=_xArray.count) {
                    NSString *fileLocation = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"wsbutton_%ld",(long)_count] ofType:@"png"];
                    UIImage* yourImage = [[UIImage alloc] initWithContentsOfFile:fileLocation] ;
                    self.traceImageView.image = yourImage;
                    _count++;
                }
            }
            
        }else{
            self.isFinished=YES;
            
            //[self.confirmButton setTitle:[NSString stringWithFormat:@"Confirm & Pay €%.2lf Deposit",[self.bookingObj.chosenPrice doubleValue]/100*10] forState:UIControlStateNormal];
            
            if (IS_iPHONE5_or_Above) {
                
                [self.traceImageView setNeedsUpdateConstraints];
                //[self.traceTutorialImageView setNeedsUpdateConstraints];
                [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.traceTutorialImageView.hidden=YES;
                    
                        self.traceImageView.frame = CGRectMake(self.traceImageView.frame.origin.x, self.logoToTraceOriginY - 40, self.traceImageView.frame.size.width, self.traceImageView.frame.size.height);
                    //self.tutorialTraceBottom.constant=+50;
                    self.traceBottom.constant=+50;
                    [self.traceTutorialImageView layoutIfNeeded];
                    [self.traceImageView layoutIfNeeded];
                    
                    self.instructionsToTraceLabel.alpha=0;
                    
                    self.isLogoTraced =YES;
                    self.confirmButton.hidden=NO;
                    self.confirmButton.alpha=1.0;
                   
                } completion:^(BOOL finished) {
                    if (self.isFinished==NO) {
                        self.confirmButton.hidden=YES;
                    }
                }];
                
                
                
            }
           
            
            
        }
        }
        
   // }
    
}


-(void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.traceImageView];
    UIView* v = [self.view hitTest:location withEvent:nil];
    
    if (v)
        [self animateTutorial];
    
}
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.traceImageView];
    UIView* v = [self.view hitTest:location withEvent:nil];
    
    if (v)
        [self animateTutorial];
    
}



#pragma mark - UIGestureRecognizer Delegate Methods

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
   }

-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)navigationBarSetUp{
    //make navigation bar completely transparent
    self.viewForNavigationBar.backgroundColor=[UIColor clearColor];
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
  
    [self.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont systemFontOfSize: 18.0f],
      NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    
    [self.cancelBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize: 18.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    
}


- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)confirm:(id)sender {
    
    self.confirmButton.userInteractionEnabled=NO;
    self.confirmButton.hidden=YES;
    if ([WSCore isDummyMode]) {
        
        [WSCore showNetworkLoadingOnView:self.view];
        [self performSelector:@selector(goToCongratsScreenInDummyMode) withObject:nil afterDelay:1.5];
        
    }else{
        
        if (self.isFromLastMinPopToSalonIndividual||self.isFromLastMinute) {
           
            //[self performSegueWithIdentifier:@"confirmation" sender:self];
            //[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            [self.salonBookingModel phorestPaymentWithApptRef:self.bookingObj.appointment_reference_number];
            
        }
        else{
            [self.salonBookingModel phorestPaymentWithApptRef:self.bookingObj.appointment_reference_number];
        }
        

    }
    
    //get users balance if successful
    //push to next screen if successful
    
    
}

-(void)goToCongratsScreenInDummyMode{
    
    [WSCore dismissNetworkLoadingOnView:self.view];
    [self performSegueWithIdentifier:@"confirmation" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"confirmation"]) {
        CongratulationsScreenViewController * destinationViewController = (CongratulationsScreenViewController *)segue.destinationViewController;
        destinationViewController.salonData= self.salonData;
        destinationViewController.bookingObj=self.bookingObj;
        destinationViewController.chosenDate=self.chosenDate;
        destinationViewController.isFromFavourite=self.isFromFavourite;
        NSLog(@"Is from fav %d",self.isFromFavourite);
        destinationViewController.isFromLastMinPopToSalonIndividual=self.isFromLastMinPopToSalonIndividual;
        destinationViewController.isFromLastMinute=self.isFromLastMinute;
        destinationViewController.unwindBackToSearchFilter=self.unwindBackToSearchFilter;
        
       
        
        
        self.traceTutorialImageView.animationImages=nil;
    }
}

#pragma mark - Services Delegate

-(void)bookingResult:(BOOL)success WithMessage:(NSString *)message{
    
    NSLog(@"message %@",message);
    if (success) {
        
        [WSCore getUsersBalance];
        
        self.confirmButton.hidden=NO;
        self.instructionsToTraceLabel.alpha=0.0;
        
        [UIView animateWithDuration:1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            //move back down
            
            self.confirmButton.alpha=0.0;
            self.traceImageView.frame = CGRectMake(self.traceImageView.frame.origin.x, self.logoToTraceOriginY-20, self.traceImageView.frame.size.width, self.traceImageView.frame.size.height);
            self.traceTutorialImageView.frame=self.traceImageView.frame;
            self.instructionsToTraceLabel.frame = CGRectMake(self.instructionsToTraceLabel.frame.origin.x, self.instructionsToTraceLabel.frame.origin.y, self.instructionsToTraceLabel.frame.size.width, 35);
            self.traceImageView.image = [UIImage imageNamed:@"wsbutton"];
            self.traceImageView.alpha=0.0;
            self.traceTutorialImageView.alpha=0.0;
            self.isLogoTraced =NO;
            
            
        } completion:^(BOOL finished) {
            [self performSegueWithIdentifier:@"confirmation" sender:self];
        }];

    }
    else{
        
        self.confirmButton.userInteractionEnabled=YES;
        self.confirmButton.hidden=NO;
        [UIView showSimpleAlertWithTitle:@"A booking could not be confirmed." message:[NSString stringWithFormat:@"%@",message] cancelButtonTitle:@"OK"];
    }
}



@end
