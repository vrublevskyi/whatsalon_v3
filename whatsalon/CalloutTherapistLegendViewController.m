//
//  CalloutTherapistLegendViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 17/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import "CalloutTherapistLegendViewController.h"

@interface CalloutTherapistLegendViewController ()

@end

@implementation CalloutTherapistLegendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [WSCore addDarkBlurToView:self.view];
    
    self.ratingImage.image = [self.ratingImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.ratingImage setTintColor:[UIColor cloudsColor]];
    
    self.hygieneImageView.image = [self.hygieneImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.hygieneImageView setTintColor:[UIColor cloudsColor]];
    
    self.timingImageView.image = [self.timingImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.timingImageView setTintColor:[UIColor cloudsColor]];
    
    self.timeKeepingLabel.textColor=[UIColor cloudsColor];
    
    self.ratingLabel.textColor=[UIColor cloudsColor];
    
    self.hygieneLabel.textColor=[UIColor cloudsColor];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
