//
//  SalonDetailTableViewCell.m
//  whatsalon
//
//  Created by Graham Connolly on 01/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "SalonDetailTableViewCell.h"

@implementation SalonDetailTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(15, 11, 150, 21)];
        self.label.text = @"First";
        self.label.textAlignment= NSTextAlignmentLeft;
        self.label.adjustsFontSizeToFitWidth = YES;
        self.label.textColor = [UIColor whiteColor];
        self.label.font = [UIFont fontWithName:@"Helvetica-Neue" size:17.0];
        
        [self.contentView addSubview:self.label];
        
        self.detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(261, 11, 44, 21)];
        self.detailLabel.text = @"First";
        self.detailLabel.textAlignment= NSTextAlignmentLeft;
        self.detailLabel.adjustsFontSizeToFitWidth = YES;
        self.detailLabel.textColor = [UIColor whiteColor];
        self.detailLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:17.0];
        
        [self.contentView addSubview:self.detailLabel];
        
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
