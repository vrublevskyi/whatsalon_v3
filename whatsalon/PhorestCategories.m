//
//  PhorestCategories.m
//  whatsalon
//
//  Created by Graham Connolly on 20/10/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "PhorestCategories.h"

@implementation PhorestCategories



+(instancetype)getInstance{
    
    static id sharedInstance = nil;
   
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        
       
        
    });
    return sharedInstance;
}

-(void)downloadCategories{
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kGet_Phorest_Categories_URL]];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:0 timeoutInterval:80.0f];
    
    NSURLSession * session = [NSURLSession sessionWithConfiguration:[WSCore sessionConfigWithTimeOut]];
    NSURLSessionDataTask * dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
        NSDictionary * dataDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        if (error==nil) {
            
            switch (httpResponse.statusCode) {
                case kOKStatusCode:
                    
                    if ([dataDict[kSuccessIndex] isEqualToString:@"true"]) {
                        NSLog(@"Phorest categories: \n\n %@",dataDict);
                       
                        if (self.completeArray==nil) {
                            self.completeArray = [[NSMutableArray alloc] init];
                        }
                        
                        
                    }
                    else{
                        NSLog(@"Error");
                    }
                    break;
                    
                default:
                    NSLog(@"Server Error");
                    break;
            }
        }
        else{
            
            NSLog(@"Error Occured");
        }
    }];
    
    if ([WSCore isNetworkReachable]) {
        [dataTask resume];
    }
    else{
        NSLog(@"Server error");
    }
}

-(NSMutableArray *)fetchCategories{
    
    if(self.completeArray!=nil){
        
        return self.completeArray;
    }
    return nil;
}
@end
