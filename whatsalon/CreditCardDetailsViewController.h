//
//  CreditCardDetailsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 01/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


/*!
     @protocol CreditCardDetailsVCDelegate
  
     @brief The CreditCardDetailsVCDelegate protocol
  
     It's a protocol to notify the delegate that a change has occurred.
 */
@protocol CreditCardDetailsVCDelegate <NSObject>

@optional
/*! @brief represents the action when a card is added. */
-(void)didAddCard;

@end
@interface CreditCardDetailsViewController : UIViewController

/*! @brief represents the CreditCardDetailsVCDelegate object. */
@property (nonatomic) id<CreditCardDetailsVCDelegate> delegate;
@end
