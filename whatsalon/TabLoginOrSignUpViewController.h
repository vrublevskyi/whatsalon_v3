//
//  TabLoginOrSignUpViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 21/07/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//


/*!
 @header TabLoginOrSignUpViewController.h
  
 @brief This is the header file for the TabLoginOrSignUpViewController.h
  
 This is the 'filter' login screen (Here you can choose to login,signup or connect through Facebook.
  
 @author Graham Connolly
 @copyright  2015 What Applications Ltd.
 @version    -
 */
#import <UIKit/UIKit.h>

/*!
     @protocol LoginDelegate
  
     @brief The LoginDelegate protocol
  
     It's a protocol used to notify the delegate.
 */
@protocol LoginDelegate <NSObject>

/*!
 * @discussion userDidLoginWithFacebook -
   notifies that the user has logged in with Facebook
 */
@optional
/*! @brief used to notify that the user has logged in with Facebook. */
-(void)userDidLoginWithFacebook;

/*! @brief used to notify that the login signup view has been cancelled. */
-(void)didCancelLoginSignUpView;
@end
@interface TabLoginOrSignUpViewController : UIViewController


@property (nonatomic) id<LoginDelegate> delegate;

/*! @brief represents the cancel button. */
@property (weak, nonatomic) IBOutlet UIButton *cancel;

/*! @brief represents the logo image view. */
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;

/*! @brief represents the backgroundImage. */
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

/*! @brief represents the Facebook button. */
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;

/*! @brief represents the sign up button. */
@property (weak, nonatomic) IBOutlet UIButton *signUp;

/*! @brief represents the login button. */
@property (weak, nonatomic) IBOutlet UIButton *loginButton;


/*! @brief the login with Facebook action. */
- (IBAction)loginWithFacebook:(id)sender;

/*! @brief the sign up button action. */
- (IBAction)signUp:(id)sender;

/*! @brief the cancel button action. */
- (IBAction)cancel:(id)sender;

/*! @brief the login buton action. */
- (IBAction)Login:(id)sender;


@end
