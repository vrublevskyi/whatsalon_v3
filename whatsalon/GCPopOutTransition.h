//
//  GCPopOutTransition.h
//  whatsalon
//
//  Created by Graham Connolly on 17/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GCPopOutTransition : NSObject
<UIViewControllerAnimatedTransitioning>

@property (nonatomic) CGRect frame;


-(instancetype)initWithFrame: (CGRect) frame;
@end
