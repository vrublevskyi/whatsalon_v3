//
//  BookUIViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 06/02/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"

@interface MakeBookingViewController : UIViewController
@property (nonatomic) Salon * salonData;
- (IBAction)selectService:(id)sender;
- (IBAction)selectStylist:(id)sender;
- (IBAction)selectTime:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (nonatomic) BOOL isFromFavourite;
@property (nonatomic) BOOL unwindBackToSearchFilter;
@end
