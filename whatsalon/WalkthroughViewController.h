//
//  WalkthroughViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 09/03/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol WalkthroughDelegate <NSObject>

@optional
-(void)walkthroughIsDismissed;
-(void)goToLoginSignUp;

@end
@interface WalkthroughViewController : UIViewController

@property (nonatomic,assign) id<WalkthroughDelegate> myDelegate;
- (IBAction)login:(id)sender;
- (IBAction)signUp:(id)sender;

@end
