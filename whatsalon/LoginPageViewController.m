//
//  LoginPageViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 19/04/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "LoginPageViewController.h"
#import "constants.h"
#import "User.h"
#import "GCNetworkManager.h"
#import "UIView+AlertCompatibility.h"




@interface LoginPageViewController ()<GCNetworkManagerDelegate>

@property (nonatomic) GCNetworkManager * loginNetworkManager;

@end

@implementation LoginPageViewController

-(void)manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    [UIView showSimpleAlertWithTitle:@"Oh Oh" message:jsonDictionary[@"message"] cancelButtonTitle:@"OK"];
   
}

-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"%@",jsonDictionary);
        [[User getInstance] setUpProfile:jsonDictionary];
        
        [[User getInstance] updateCreditCardDetailsWithFirstName:jsonDictionary[@"message"][@"card_details"][@"card_first_name"] withLastName:jsonDictionary[@"message"][@"card_details"][@"card_last_name"] withCardNumber:jsonDictionary[@"message"][@"card_details"][@"card_number"] withCardType:jsonDictionary[@"message"][@"card_details"][@"card_type"] withExp:jsonDictionary[@"message"][@"card_details"][@"expiry"] withPaymentRef:jsonDictionary[@"message"][@"card_details"][@"payment_ref"]];

        if (self.isTier1Booking) {
            NSLog(@"IS tier 1");
            [self performSegueWithIdentifier:@"loginUnwindToBook" sender:self];
            
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"BlurMenuImage" object:nil userInfo:nil];//blur the menu image
        }
        
        else{
            
            //is settings
            NSLog(@"IS settings");
            
           
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
            
                [self performSegueWithIdentifier:@"unwindToSettings" sender:self];
            });
            [[NSNotificationCenter defaultCenter] postNotificationName:kRefreshMenuNotification object:nil userInfo:nil];//blur the menu image
            
        }
    });
    
    [[User getInstance] downloadUsersBalance];

}
#pragma mark - UIViewController delegate methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [WSCore addDarkBlurAsSubviewToView:self.backgroundImage];
    [self navigationBarSetUp];
    self.title = @"Welcome back";
    
    [WSCore addTopLine:self.emailView :kCellLinesColour :0.5f];
    [WSCore addBottomIndentedLine:self.emailView :kCellLinesColour];
    [WSCore addBottomLine:self.passwordView :kCellLinesColour];
    
    self.loginButton.backgroundColor=kWhatSalonBlue;
    self.loginButton.layer.cornerRadius=3.0f;
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.emailAddressTextField.keyboardType=UIKeyboardTypeEmailAddress;
    self.emailAddressTextField.autocapitalizationType=UITextAutocapitalizationTypeNone;
    self.emailAddressTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.passwordTextField.secureTextEntry=YES;
    
    self.userAvatar.clipsToBounds=YES;
    self.userAvatar.layer.cornerRadius = self.userAvatar.frame.size.width/2.0;
    self.userAvatar.hidden=YES;
    self.loginNetworkManager = [[GCNetworkManager alloc] init];
    self.loginNetworkManager.delegate=self;
    self.loginNetworkManager.parentView=self.view;
    
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (IS_iPHONE4) {
        self.userAvatar.frame = CGRectMake(self.userAvatar.frame.origin.x+10, self.userAvatar.frame.origin.y, 83, 83);
        self.userAvatar.image = [UIImage imageNamed:@"grayCameraAvatar.png"];
        self.userAvatar.contentMode=UIViewContentModeScaleToFill;
        self.userAvatar.layer.borderColor=[UIColor lightGrayColor].CGColor;
        self.userAvatar.layer.cornerRadius=self.userAvatar.frame.size.width/2;
        self.userAvatar.layer.borderWidth=1.0;
        self.userAvatar.layer.masksToBounds=YES;
        
    }
    
}

#pragma mark - Set up methods

-(void)navigationBarSetUp{
    //make navigation bar completely transparent
    self.viewForNavigationBar.backgroundColor=[UIColor clearColor];
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
    
    [self.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent = YES;
  
    self.navigationBar.tintColor=[UIColor whiteColor];
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
   
    [self.CancelBarButton setImage:[UIImage imageNamed:@"icon_x_cancel"]];
    self.CancelBarButton.title=@"";

    
    
}

#pragma mark - UITapGestureMethod

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}



#pragma mark - IBActions
- (IBAction)cancel:(id)sender {
    
      [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)login:(id)sender {
    
    //log in
    [self logInConnection];
    
    
}

#pragma mark - Logging in stuff
-(void)logInConnection{
    NSLog(@"Login IN");

    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kLogin_URL]];
    NSString * params = [NSString stringWithFormat:@"password=%@",self.passwordTextField.text];
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&email=%@",self.emailAddressTextField.text]];
    NSLog(@"params %@",params);
    
    [self.loginNetworkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];
    
}

- (IBAction)forgotPassword:(id)sender {
}
@end
