//
//  CallOutServiceViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 02/04/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallOutServiceViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic) NSString *chosenService;
@property (nonatomic) CGRect buttonRect;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic) UIPageControl *pageControl;

@property (nonatomic, strong) NSArray *pageImages;
@property (nonatomic, strong) NSMutableArray *pageViews;

- (void)loadVisiblePages;
- (void)loadPage:(NSInteger)page;
- (void)purgePage:(NSInteger)page;

//@property (nonatomic) UIImageView * backgroundImage;
//@property (nonatomic) BOOL startAnimation;

@property (nonatomic) NSMutableArray * titles;
@property (nonatomic) NSMutableArray * paragraphs;
@end
