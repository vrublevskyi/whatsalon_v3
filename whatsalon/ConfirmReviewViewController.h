//
//  ConfirmReviewViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 15/05/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalonReviewViewController.h"
#import "MyBookings.h"

@protocol ConfirmDelegate <NSObject>
-(void) secondViewControllerDismissed:(BOOL)hasPic WithImage: (UIImage *)image;
@end

@interface ConfirmReviewViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *salonLabel;
@property (strong, nonatomic) IBOutlet UILabel *stylistName;
@property (strong, nonatomic) IBOutlet UILabel *stylistLabel;

@property (strong, nonatomic) IBOutlet UILabel *salonName;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak,nonatomic) UIImage *takenImage;
@property (nonatomic,assign) id <ConfirmDelegate> myDelegate;
@property (nonatomic) NSString * confirmID;

@property (nonatomic) MyBookings * booking;
- (IBAction)twitter:(id)sender;
- (IBAction)faceBook:(id)sender;
- (IBAction)email:(id)sender;
- (IBAction)message:(id)sender;
@end
