//
//  TabDirectorySalonTableViewCell.h
//  whatsalon
//
//  Created by Graham Connolly on 21/08/2015.
//  Copyright (c) 2015 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalonTier4.h"

@interface TabDirectorySalonTableViewCell : UITableViewCell

/*! @brief represents the salon image view. */
@property (weak, nonatomic) IBOutlet UIImageView *salonImage;

/*! @brief represents the salon title label. */
@property (weak, nonatomic) IBOutlet UILabel *salonTitle;

/*! @brief represents the salon address label. */
@property (weak, nonatomic) IBOutlet UILabel *salonAddress;

/*! @brief represents the distance label. */
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

/*! @brief configures the cells appearance. 
 
    @param salon - the SalonTier4 object.
 
 */
 -(void)configureCell: (SalonTier4 *) salon;
@end
