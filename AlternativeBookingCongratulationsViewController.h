//
//  AlternativeBookingCongratulationsViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 15/08/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Salon.h"

@interface AlternativeBookingCongratulationsViewController : UIViewController

@property (nonatomic)Salon * salonData;
@property (nonatomic) NSString * chosenService;
@property (nonatomic) NSString * chosenStylist;
@property (nonatomic) double chargedPrice;
@property (nonatomic) double remainingPrice;
@property (nonatomic) NSString * chosenDate;
@property (nonatomic) NSString * price;
@property (nonatomic,assign) BOOL isMapView;//if its coming from the map view
- (IBAction)share:(id)sender;

@end
