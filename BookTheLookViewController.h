//
//  BookTheLookViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 30/09/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InspireImage.h"

@interface BookTheLookViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *viewForService;
@property (strong, nonatomic) IBOutlet UIView *viewForTime;
@property (strong, nonatomic) IBOutlet UIView *addressView;
@property (strong, nonatomic) IBOutlet UILabel *selectTime;


- (IBAction)getDirections:(id)sender;
@property (nonatomic) InspireImage * inspire;
@property (strong, nonatomic) IBOutlet UIButton *findAppointment;
- (IBAction)findAppointment:(id)sender;
@end
