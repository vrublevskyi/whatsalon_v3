//
//  ChangeEmailAndPhoneViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 12/06/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "ChangeEmailAndPhoneViewController.h"
#import "User.h"
#import "GCNetworkManager.h"
#import "UIView+AlertCompatibility.h"

@interface ChangeEmailAndPhoneViewController () <UITextFieldDelegate,GCNetworkManagerDelegate>

/*! @brief represnts the view for the email text field. */
@property (weak, nonatomic) IBOutlet UIView *viewForEmailTextfield;

/*! @brief represents the view for the phone text field. */
@property (weak, nonatomic) IBOutlet UIView *viewForPhoneTextField;

/*! @brief represents the email textfield. */
@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;

/*! @brief represents the phone textfield. */
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

/*! @brief represents the network manager. */
@property (nonatomic) GCNetworkManager * networkManager;

/*! @brief represents the save details action. */
- (IBAction)saveDetails:(id)sender;

@end

@implementation ChangeEmailAndPhoneViewController


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [WSCore flatNavBarOnView:self];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title=@"Phone & Email";
    
    self.networkManager = [[GCNetworkManager alloc] init];
    self.networkManager.delegate =self;
    self.networkManager.parentView = self.view;
    
    
    self.emailTextfield.text = [[User getInstance] fecthEmail];
    self.emailTextfield.autocapitalizationType=UITextAutocapitalizationTypeNone;
    self.emailTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextfield.returnKeyType=UIReturnKeyNext;
    self.emailTextfield.tag=0;
    self.emailTextfield.delegate=self;
    
  
    self.phoneTextField.text=[NSString stringWithFormat:@"%@",[[User getInstance] fetchPhoneNumber] ];
    self.phoneTextField.returnKeyType=UIReturnKeyDone;
    self.phoneTextField.tag=1;
    self.phoneTextField.delegate=self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (IBAction)saveDetails:(id)sender {
    
    if ([self.phoneTextField.text isEqualToString: [[User getInstance] fetchPhoneNumber]]) {
        [UIView showSimpleAlertWithTitle:@"Cannot change Phone" message:@"You cannot change your phone number at this present time. This feature is coming soon." cancelButtonTitle:@"OK"];
    }
    else{
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kUpdate_User_URL]];
        NSString * params = [[NSString alloc] initWithString:[NSString stringWithFormat:@"user_id=%@",[[User getInstance] fetchAccessToken]]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&user_email=%@",self.emailTextfield.text] ];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&phone_number=%@",[self.phoneTextField.text stringByReplacingOccurrencesOfString:@"+" withString:@""]]];
        
        [self.networkManager loadWithURL:url withParams:params WithHTTPMethod:@"POST" AndWithLoadingDialog:YES];

    }
    
    
}


#pragma mark - UIAlertView delegate methods
// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 1) {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            NSString * phone = self.phoneTextField.text;
            phone = [phone stringByReplacingOccurrencesOfString:@"+" withString:@""];
            [[User getInstance] updateEmailAddress:self.emailTextfield.text];
            [[User getInstance] updatePhoneNumber:phone];
            
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}

#pragma mark - UITextField delegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    UIView *view = [self.view viewWithTag:textField.tag + 1];
    if (!view)
        [textField resignFirstResponder];
    else
        [view becomeFirstResponder];
    return YES;
}

#pragma mark - GCNetworkManager
-(void)manager:(GCNetworkManager *)manager handleSuccessMessageWithDictionary:(NSDictionary *)jsonDictionary{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Changes Saved" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    alert.tag=1;
    [alert show];
}

-(void) manager:(GCNetworkManager *)manager handleFailureMessageWitDictionary:(NSDictionary *)jsonDictionary{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:jsonDictionary[@"message"] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

@end
