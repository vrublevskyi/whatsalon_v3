//
//  ContentViewController.h
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import "WSCore.h"


@interface BrowseViewController : UIViewController<UITableViewDataSource,UITableViewDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate>


@property (nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UINavigationItem *navbarItem;

@property (nonatomic) NSInteger nearMePageCurrentPage;
@property (nonatomic) NSInteger totalNearMePageCount;
@property(nonatomic,assign) BOOL isSearchEnabled;

- (IBAction)mapButton:(id)sender;
- (IBAction)showMenu:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
- (IBAction)searchVC:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;

@property (nonatomic) BOOL loggedInWithFaceBook;

@property (nonatomic) BOOL hasDiscoveryLatLong;
@property (nonatomic) NSString * discoveryLat;
@property (nonatomic) NSString *discoveryLon;
@property (nonatomic) NSString *browseTitle;

@end
