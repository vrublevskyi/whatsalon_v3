//
//  ContentViewController.m
//  whatsalon
//
//  Created by Graham Connolly on 11/03/2014.
//  Copyright (c) 2014 What Applications Ltd. All rights reserved.
//

#import "BrowseViewController.h"
#import "Salon.h"
#import "WSCore.h"
#import "BrowseTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "TabBarController.h"
#import "ResultsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UITableView+ReloadTransition.h"
#import "User.h"
#import "MapViewController.h"
#import "AllSalonsModel.h"
#import "NoSalonsMessageView.h"
#import "NoLocationMessageView.h"
#import "UIView+AlertCompatibility.h"
#import "CongratulationsScreenViewController.h"
#import "SalonIndividualDetailViewController.h"
#import "FilterSearchViewController.h"
#import "FUIButton.h"
#import "UIColor+FlatUI.h"
#import "PresentDetailTransition.h"
#import "DismissDetailTransition.h"
#import "SearchSalonsTableViewController.h"
#import "WalkthroughViewController.h"
#import "TwilioVerifiyViewController.h"
#import "SettingsViewController.h"
#import "MyBookingsViewController.h"

#import "Booking.h"
#import "Harpy.h"
#import "SelectCategoryViewController.h"


const int kLoadingCellTag =1273;//any random numeric

@interface BrowseViewController () <CLLocationManagerDelegate,UIViewControllerTransitioningDelegate,AllSalonsDelegate,FilterDelegate,WalkthroughDelegate,SearchDelegate>


//for dummy data
@property (nonatomic,strong) NSMutableArray * allSalons;

@property (nonatomic,strong) CLLocationManager * locationManager;
@property (nonatomic) NSDateFormatter * dateFormatter;
@property (nonatomic) UIRefreshControl * refreshControl;
@property (nonatomic) AllSalonsModel * allSalonsModel;
@property (nonatomic) CLLocation * location;

@property (nonatomic,assign) BOOL isLoading;
@property (nonatomic,assign) BOOL hasLocation;
@property (nonatomic,assign) BOOL isLoadingTryAgain;
@property (nonatomic,assign) BOOL isTryingAgain;
@property (nonatomic,assign) BOOL isNoLocationViewShowing;
@property (nonatomic,assign) BOOL isNoSalonsLocationViewShowing;
@property (nonatomic,assign) BOOL isFetchingLocation;

@property (nonatomic) FUIButton * filterButton;
//@property (nonatomic) UIButton * filterButton;

@property (nonatomic) BOOL instantBookOnly;
@property (nonatomic) NSMutableArray * directoryArray;
@property (nonatomic) NSInteger directoryCurrentPage;
@property (nonatomic) NSInteger totalDirectoryPageCount;
@property (nonatomic) int filterDistance;

@property (nonatomic) NSString *searchString;
@property (nonatomic) BOOL isLoadingSalons;

@property (nonatomic) int selectedCategory;

@end

@implementation BrowseViewController


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void)fetchLocationHUD{
    if (self.isFetchingLocation==NO) {
        
        [WSCore showNetworkLoadingOnView:self.view WithTitle:@"Getting Location"];
        self.isFetchingLocation=YES;
        
    }
    
}


-(void)dismissLocationHUD{
    if (self.isFetchingLocation==YES) {
      
        [WSCore dismissNetworkLoadingOnView:self.view];
        self.isFetchingLocation =NO;
    }
}

- (void)checkIfLocationIsDenied {
    
  
    if([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        //Denied
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            
            NSString *title =@"Location services are off for WhatSalon";
            NSString *message = @"To use location you must turn on in the Location Services in Settings";
                
            [UIView showSimpleAlertWithTitle:title message:message cancelButtonTitle:@"OK"];

            
            [self addNoLocationMessageView];
            
        }
        //authorized
        else
        {
            if (!self.isTryingAgain) {
                [self fetchLocationHUD];
            }
           
            if (self.locationManager==nil) {
                self.locationManager = [[CLLocationManager alloc] init];
            }
            
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters;
            self.locationManager.distanceFilter=500.0;//half km
            
            // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self.locationManager requestWhenInUseAuthorization];
            }
            [self.locationManager startUpdatingLocation];
            
            if (self.isTryingAgain) {
             [self showTryAgainLoadingSalonHUD];
                self.isTryingAgain=NO;
            }
            
        

        }
        
    }
    else if (![CLLocationManager locationServicesEnabled]){
        

        NSString *title =@"Location services are off";
        NSString *message = @"To use location you must turn on in the Location Services in Settings";
            
        [UIView showSimpleAlertWithTitle:title message:message cancelButtonTitle:@"OK"];
        
        [self addNoLocationMessageView];
        
        
    }

}

-(void)tryAgain{
    
  
    self.isTryingAgain=YES;
    [self checkIfLocationIsDenied];
    
    
}
-(void)addNoLocationMessageView{
    
    UIView * noDataView = [[UIView alloc] initWithFrame:self.tableView.bounds];
    noDataView.backgroundColor=kBackgroundColor;
    
    NoLocationMessageView * noLocationMessage = [[NoLocationMessageView alloc] init];
    noLocationMessage.view.backgroundColor=[UIColor clearColor];
    [noDataView addSubview:noLocationMessage];
    noLocationMessage.center=CGPointMake(noDataView.frame.size.width/2.0, noDataView.frame.size.height/2.0);
    [noLocationMessage.tryAgainButton addTarget:self action:@selector(tryAgain) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.backgroundView=noDataView;
    self.isNoLocationViewShowing=YES;
}

-(void)removeNoLocationMessageView{
    self.tableView.backgroundView=nil;
    self.isNoLocationViewShowing=NO;
    
}


-(void)showTryAgainLoadingSalonHUD{
    if (self.isLoadingTryAgain==NO) {
        
        [WSCore showNetworkLoadingOnView:self.view WithTitle:@"Getting Location"];
        self.isLoadingTryAgain=YES;
        
    }

}

-(void)dismissTryAgainHUD{
    if (self.isLoadingTryAgain==YES) {
        [WSCore dismissNetworkLoadingOnView:self.view];
        self.isLoadingTryAgain=NO;
    }
}


-(void)showDownloadingSalonsHUDWithPrompt:(NSString *)prompt{
    
    if (self.isLoading==NO) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        [WSCore showNetworkLoadingOnView:self.view WithTitle:prompt];
        self.isLoading=YES;
    }
    
    
}
-(void)dismissSalonsHUD{
    
    if (self.isLoading==YES) {
        [WSCore dismissNetworkLoadingOnView:self.view];
        self.isLoading =NO;
        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    }
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    [self.allSalonsModel stopNetworkTask];
    
    if (self.refreshControl.isRefreshing) {
        [self.refreshControl endRefreshing];
    }
}
-(void)checkAppVersion{
    [WSCore checkAppVersion];
}
#pragma mark - Application lifecycle methods

//adds footer view
- (void)footerView
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kButtonHeight+120)];//adds 120 because inital starting height of table view is 'self.tableView.frame.size.height+100
    footerView.backgroundColor=[UIColor clearColor];
    self.tableView.tableFooterView=footerView;
}

//hides the footerview
-(void)hideFooterView{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
  
    self.tableView.tableFooterView=footerView;
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height+100);//make tableview height bigger to have less free space.
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationItem.title = self.browseTitle;
    
    [[Harpy sharedInstance] checkVersion];
    [WSCore statusBarColor:StatusBarColorBlack];
    
    if ([WSCore showWalkthrough]) {
    
        WalkthroughViewController * wVC = [self.storyboard instantiateViewControllerWithIdentifier:@"walkthroughVC"];
        wVC.myDelegate=self;
        [self presentViewController:wVC animated:YES completion:nil];
        
        [WSCore walkthroughHasBeenShown];
    
    }
    
    //return;
    self.instantBookOnly=YES;
    self.directoryArray = [NSMutableArray array];

    
    self.isLoading=NO;
    self.hasLocation=NO;
    self.isFetchingLocation=NO;
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = kBackgroundColor;
    self.refreshControl.tintColor = [UIColor silverColor];
    [self.refreshControl addTarget:self
                            action:@selector(getLatest)
                  forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
    
    
    [self navigationBarSetUp];//the 'custom' navigatin bar set up
    self.view.backgroundColor = [UIColor lightGrayColor];
    [self tablesSetUp];//the set up of the tables
    
   
    
    self.allSalons = [NSMutableArray array];
    self.nearMePageCurrentPage=1;
   
    
    NSString *notifAddToFavs = @"salonIsAddedToFavourites";
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateSalonAsFavourite:)
     name:notifAddToFavs
     object:nil];
    
    NSString *notifRemoveFromFav = @"salonIsNotFavourite";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSalonRemoveFavourite:) name:notifRemoveFromFav object:nil];
    
    
    if (![WSCore isDummyMode]) {
        self.allSalonsModel = [[AllSalonsModel alloc] init];
        self.allSalonsModel.myDelegate=self;
        self.allSalonsModel.parentView=self.view;
    }
    /*
    else{
        self.allSalons = [[NSMutableArray alloc] initWithArray:[WSCore getDummyTier1Salons]];
    }
    */
   
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [self checkIfLocationIsDenied];
    
    
 
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.filterButton = [[FUIButton alloc] init];
    //self.filterButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.filterButton.frame = CGRectMake(10, self.view.frame.size.height-60, self.view.frame.size.width-20, 50) ;
    
    self.filterButton.buttonColor = kWhatSalonBlue;
    self.filterButton.shadowColor = kWhatSalonBlueShadow;
    self.filterButton.shadowHeight = 1.5f;
    self.filterButton.cornerRadius = 3.0f;
    self.filterButton.alpha=0.95;
    
    self.filterButton.layer.cornerRadius=3.0f;
    self.filterButton.layer.masksToBounds=YES;
    
    self.filterButton.backgroundColor = kWhatSalonBlue;
    [self.filterButton setTitle:@"Filter Search" forState:UIControlStateNormal];
    [self.filterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal
     ];
    [self.filterButton addTarget:self action:@selector(filter) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.filterButton];
    [self.view bringSubviewToFront:self.filterButton];
    
    [WSCore floatingView:self.filterButton];
    
    self.totalDirectoryPageCount=1;
   
    self.filterDistance=0;
    
    self.buttonView.backgroundColor=[UIColor clearColor];
   

    [self.searchButton setImage:[self.searchButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.searchButton.imageView setTintColor:kWhatSalonBlue];
    [self.mapButton setImage:[self.mapButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
     [self.mapButton.imageView setTintColor:kWhatSalonBlue];
    
        
    self.view.backgroundColor=kBackgroundColor;
    self.tableView.backgroundColor=kBackgroundColor;
    
    if (self.loggedInWithFaceBook) {
        if (![[User getInstance] isTwilioVerified]) {
            
        }
    }
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kButtonHeight+20)];
    footerView.backgroundColor=[UIColor clearColor];
    self.tableView.tableFooterView=footerView;
    
    //[self footerView];
    
    self.tableView.hidden=YES;
    
    self.selectedCategory=0;

}


-(void)filter{
    NSLog(@"Filter Search");
   
    [self.allSalonsModel stopNetworkTask];
    FilterSearchViewController * filter = [self.storyboard instantiateViewControllerWithIdentifier:@"filterVC"];
     filter.modalPresentationStyle = UIModalPresentationCurrentContext;
    filter.myDelegate=self;
    //filter.filterDistance = self.filterDistance;
    //filter.isInstantBookOnly=self.instantBookOnly;
    filter.browseCategoryForFiltering=self.selectedCategory;
    [self presentViewController:filter animated:YES completion:^{
        if (self.allSalons.count>0) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:0 animated:NO];
        }
        
    }];
}
-(void)hideRefreshIfVisible{
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }

}
 
#pragma mark - CLLocationManager Methods
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if(error.code == kCLErrorDenied) {
        [self.locationManager stopUpdatingLocation];
        [self dismissLocationHUD];
        [UIView showSimpleAlertWithTitle:@"Error retrieving location." message:@"You have denied access to the location service. WhatSalon uses your location to show you nearby salons." cancelButtonTitle:@"OK"];
        
        
        [self hideRefreshIfVisible];
    }
    else if(error.code == kCLErrorLocationUnknown) {
        // retry
    }
    else {
        [self dismissLocationHUD];
        [UIView showSimpleAlertWithTitle:@"Error retrieving location." message:@"WhatSalon needs your location to show you nearby salons." cancelButtonTitle:@"OK"];
       
        
        [self hideRefreshIfVisible];
    }
    
    [self addNoLocationMessageView];
}

// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{

    if (self.isLoadingTryAgain==YES) {
        [self dismissTryAgainHUD];
    }
    if (self.tableView.backgroundView!=nil &&self.hasLocation==NO) {
        
        [self removeNoLocationMessageView];
    }
 
    self.location = [locations lastObject];
    if (self.hasLocation==NO) {
        
        if (self.tableView.backgroundView!=nil) {
            self.tableView.backgroundView=nil;
        }
        
        if (self.isFetchingLocation) {
            [self dismissLocationHUD];
        }
        
        if (![WSCore isDummyMode]) {
            [self performSelector:@selector(setSalonURLAndCall) withObject:nil afterDelay:1.0];
        }
       
        self.hasLocation=YES;
    }
    
    [[User getInstance] updateUsersLocationWithLocation:self.location];
    
}

#pragma mark - NSNotification methods

- (void)refreshTableView:(NSNotification *)notif {
    [self.tableView reloadDataWithFade:YES];
    //[self.tableView reloadDataAnimated:YES];
}

-(void)updateSalonRemoveFavourite:(NSNotification *)notification {
    NSString *key = @"Salon_id";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    
    for (int i =0; i<self.allSalons.count; i++) {
        Salon * salon = [self.allSalons objectAtIndex:i];
        if ([salon.salon_id isEqualToString: stringValueToUse]) {
          
            salon.is_favourite=NO;
            [self.allSalons replaceObjectAtIndex:i withObject:salon];
            

            break;
        }
    
    }
       dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        
    });
    
    
}


-(void)updateSalonAsFavourite:(NSNotification *)notification {
    NSString *key = @"Salon_id";
    NSDictionary *dictionary = [notification userInfo];
    NSString *stringValueToUse = [dictionary valueForKey:key];
    
    for (int i =0; i<self.allSalons.count; i++) {
        Salon * salon = [self.allSalons objectAtIndex:i];
      
        if ([salon.salon_id isEqualToString: stringValueToUse]) {
           salon.is_favourite=YES;
            [self.allSalons replaceObjectAtIndex:i withObject:salon];
            
            
            
            break;
        }
        
    
    }
        
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;

    [self navigationBarSetUp];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
}


- (void)resetPageAndCall {
    [self.directoryArray removeAllObjects];
    self.totalDirectoryPageCount=0;
    self.directoryCurrentPage=1;
    self.nearMePageCurrentPage=1;
    self.totalNearMePageCount=0;
    [self.allSalons removeAllObjects];
    [self setSalonURLAndCall];
}

-(void)getLatest{

  
   
    
    if (self.hasLocation==NO) {
       
        if (self.locationManager==nil) {
           
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters;
            self.locationManager.distanceFilter=500.0;//half km
            // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self.locationManager requestWhenInUseAuthorization];
            }
            [self.locationManager startUpdatingLocation];
        }
        
        
        return;
    }
    
    
    
    [self resetPageAndCall];
    
    
}


#pragma mark - Set up methods




- (void)tablesSetUp
{
    //Near me & fav table
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.allowsSelectionDuringEditing = YES;
    self.tableView.backgroundColor = [UIColor lightGrayColor];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = footer;
    
    [[self tableView] registerClass:[BrowseTableViewCell class] forCellReuseIdentifier:@"Cell"];
   
    
   
}


-(void)navigationBarSetUp{
    
       //show navigation bar - hidden in TabBarController
    if (self.navigationController.navigationBar.hidden ==YES) {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
   
    //in order to remove 'Back' from back button in SalonDetailViewController
    //self.navigationItem.title= @"Browse";
    self.navigationController.navigationBar.tintColor=self.view.tintColor;
    
    
}


#pragma mark - UITableView datasource & delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([WSCore isDummyMode]) {
        return 0;//[WSCore getDummyTier1Salons].count;
    }
    if (tableView == self.tableView) {
        
        if (self.instantBookOnly) {
            
            if (self.nearMePageCurrentPage<self.totalNearMePageCount && self.allSalons.count!=0) {
                
                return self.allSalons.count+1;
            }
            
            
            return self.allSalons.count;
        }
        else{
            
            if (self.directoryCurrentPage<self.totalDirectoryPageCount) {
                return self.directoryArray.count+1;
            }
            
            return self.directoryArray.count;
        }
        
      
       
  
    }
    
    return 0;
}

- (BrowseTableViewCell *)nearMeCellForIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    BrowseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Salon * salon = nil;
    
    if ([WSCore isDummyMode]) {
       // salon = [[WSCore getDummyTier1Salons] objectAtIndex:indexPath.row];
    }
    else{
        salon = [self.allSalons objectAtIndex:indexPath.row];
    }
    [cell setUpCellWithSalon:salon];
    
    return cell;
}

-(UITableViewCell *)tier2Cell: (UITableView *) tableView :(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"plainCell" forIndexPath:indexPath];
   // if (self.isFiltered == YES) {
        
        Salon * salon = nil;
        //salon = [self.searchResults objectAtIndex:indexPath.row];
        salon = [self.directoryArray objectAtIndex:indexPath.row];
        cell.textLabel.text = salon.salon_name;
        cell.textLabel.adjustsFontSizeToFitWidth=YES;
    
        cell.detailTextLabel.text = @"";
        if (salon.salon_address_1.length!=0) {
            cell.detailTextLabel.text = [cell.detailTextLabel.text stringByAppendingString:[NSString stringWithFormat:@"%@",salon.salon_address_1]];
        }
        if (salon.salon_address_2.length!=0) {
            cell.detailTextLabel.text = [cell.detailTextLabel.text stringByAppendingString:[NSString stringWithFormat:@", %@",salon.salon_address_2]];
        }
        if (salon.salon_address_3.length!=0) {
            cell.detailTextLabel.text = [cell.detailTextLabel.text stringByAppendingString:[NSString stringWithFormat:@", %@",salon.salon_address_3]];
        }
        if (salon.city_name.length!=0 ) {
                cell.detailTextLabel.text = [cell.detailTextLabel.text stringByAppendingString:[NSString stringWithFormat:@", %@",salon.city_name]];
            
        }
        cell.detailTextLabel.numberOfLines=0;
        //cell.detailTextLabel.text =[NSString stringWithFormat:@"%@, %@ %@", salon.salon_address_1,salon.salon_address_2,salon.salon_address_3];
        cell.detailTextLabel.textColor=kCellLinesColour;
    /*
    if (salon.salon_tier!=1) {
        cell.imageView.image = [UIImage imageNamed:@"add_image_icon"];
        cell.imageView.image = [cell.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.imageView setTintColor:[UIColor lightGrayColor]];
    }else{
        cell.imageView.image=nil;
    }
    */
    if (salon.salon_landline.length>1 && [salon.salon_type isEqualToString:@"1"]) {
        UIImageView *checkmark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Telephone"]];
        cell.accessoryView = checkmark;
        
    }
    else{
        cell.accessoryView = UITableViewCellAccessoryNone;
    }
    
    /*
}else{
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
     */
    return cell;


}

/*
- (SearchTableViewCell *)searchCellForIndexPath:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell" forIndexPath:indexPath];
    
    if (self.isFiltered == YES) {
        Salon * salon = nil;
        salon = [self.searchResults objectAtIndex:indexPath.row];
        

        cell.salonAddress.text = @"";
        
        if (salon.salon_address_1.length!=0) {
            cell.salonAddress.text = [cell.salonAddress.text stringByAppendingString:[NSString stringWithFormat:@"%@",salon.salon_address_1]];
        }
        if (salon.salon_address_2.length!=0) {
            cell.salonAddress.text = [cell.salonAddress.text stringByAppendingString:[NSString stringWithFormat:@", %@",salon.salon_address_2]];
        }
        if (salon.salon_address_3.length!=0) {
            cell.salonAddress.text = [cell.salonAddress.text stringByAppendingString:[NSString stringWithFormat:@", %@",salon.salon_address_3]];
        }
        if (salon.city_name.length!=0 ) {
            cell.salonAddress.text = [cell.salonAddress.text stringByAppendingString:[NSString stringWithFormat:@", %@",salon.city_name]];
            
        }

        if (cell.salonAddress.text.length>30) {
            cell.salonAddress.frame = CGRectMake(102, 32, 175, 41);
            cell.salonName.frame=CGRectMake(102, 13, 165, 21);
        }
        else{
            cell.salonName.frame = CGRectMake(102, 20, 165, 21);
            cell.salonAddress.frame=CGRectMake(102, 39, 175, 21);
            
        }
        
        [cell setCellImageWithURL:salon.salon_image];
        cell.salonName.text = salon.salon_name;
        UIImageView *checkmark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blueSalonImage"]];
        cell.accessoryView = checkmark;
        
        
    }else{
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    return cell;
}
*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = nil;
   
        //if its `Near me` or `Favourites`
    if (tableView == self.tableView) {
       
        if (self.instantBookOnly) {
          
            if(indexPath.row <self.allSalons.count){
                
                cell=[self nearMeCellForIndexPath:indexPath tableView:tableView];
            }
            else{
                cell=[self loadingCell];
            }
            
        }
        else if(!self.instantBookOnly){
            
            if(indexPath.row <self.directoryArray.count){
                
                cell=[self tier2Cell:tableView :indexPath];
            }
            else{
                cell=[self loadingCell];
            }
        }
        
        
    }
    
    return cell;
}



-(UITableViewCell *)loadingCell{
   
    
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    UIActivityIndicatorView * activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = cell.center;
    activityIndicator.color=[UIColor silverColor];
    //activityIndicator.tintColor=[UIColor silverColor];
    cell.backgroundColor = kBackgroundColor;
    [cell addSubview:activityIndicator];
    cell.tag=kLoadingCellTag;
    [activityIndicator startAnimating];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (cell.tag==kLoadingCellTag) {
        
        
        [self hideFooterView];
        
        if (self.instantBookOnly) {
           
            if (!self.isLoadingSalons) {
                self.isLoadingSalons=YES;//added
                self.nearMePageCurrentPage++;
                [self performSelector:@selector(setSalonURLAndCall) withObject:nil afterDelay:1.5f];

            }
        }else if(!self.instantBookOnly){
            
            NSLog(@"Tier 2");
            self.directoryCurrentPage++;
            [self performSelector:@selector(setSalonURLAndCall) withObject:nil afterDelay:1.5f];
        }
    }
    else{
        //update footer height
        [self footerView];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tableView) {
       
  
        if (self.instantBookOnly) {
             return CellHeight;
        }
        return 60.0f;
    

    }
    
 
    
    return CellHeight;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"Is instant %d",self.instantBookOnly);
    
        //if near me is not hidden or favTableNot hidden perform a segue
        //if (!self.nearMeTableHidden || !self.favTableHidden) {
    
    if (self.instantBookOnly) {
        [self performSegueWithIdentifier:@"detailB" sender:self];
    }
    else{
  
        NSLog(@"Make call");
        Salon *directorySalon = [self.directoryArray objectAtIndex:indexPath.row];
        if (directorySalon.salon_tier==1) {
            NSLog(@"Tier 1 go to SalonInd");
            [self performSegueWithIdentifier:@"detailB" sender:self];
        }
        else{
            NSLog(@"Call %@",directorySalon.salon_landline);
            if (!directorySalon.salon_landline.length<1) {
                NSString * number = directorySalon.salon_landline;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [WSCore makePhoneCallWithNumber:number withViewController:self AndWithSalonName:directorySalon.salon_name];
                });
    
        
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView showSimpleAlertWithTitle:@"No number provided" message:[NSString stringWithFormat:@"No number has been provided by %@. Please contact WhatSalon for futher assistance.",directorySalon.salon_name] cancelButtonTitle:@"OK"];
                });
            }

        }
        

    }
    
        //}

    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
        if([segue.identifier isEqualToString:@"detailB"]){
        SalonIndividualDetailViewController * detailSalon = (SalonIndividualDetailViewController *)segue.destinationViewController;
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        detailSalon.salonData = [self.allSalons objectAtIndex:indexPath.row];
        
    }
    
}


#pragma mark - Actions


- (IBAction)mapButton:(id)sender {
    
   
    
        if (!IS_iPHONE4) {
            MapViewController*viewController =
            [self.storyboard instantiateViewControllerWithIdentifier:@"mapVC"];
            
            if (self.instantBookOnly) {
                viewController.salonArray = self.allSalons;
            }
            else{
                viewController.salonArray=self.directoryArray;
            }
            
            [UIView beginAnimations:@"View Flip" context:nil];
            [UIView setAnimationDuration:0.80];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            
            [UIView setAnimationTransition:
             UIViewAnimationTransitionFlipFromRight
                                   forView:self.navigationController.view cache:NO];
            
            [self.navigationController pushViewController:viewController animated:YES];
            [UIView commitAnimations];
            
        }else{
            
            [self performSegueWithIdentifier:@"toMap" sender:self];
        }
    
}



#pragma mark - ResideMenu Action
- (IBAction)showMenu:(id)sender {
    
    [self.sideMenuViewController presentMenuViewController];
   
    
}


#pragma mark - UNWIND Segue
- (IBAction)unwindToBrowse:(UIStoryboardSegue *)unwindSegue
{
    UIViewController* sourceViewController = unwindSegue.sourceViewController;
    
    if ([sourceViewController isKindOfClass:[CongratulationsScreenViewController class]])
    {
        [WSCore nonTransparentNavigationBarOnView:self];
    }
}


#pragma mark - mark UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"Alert");
    
    if (alertView.tag==111) {
      
        if (buttonIndex == 1) {
         
            if (IS_IOS_8_OR_LATER) {
                NSLog(@"iOS 8");
                // Send the user to the Settings for this app
                if (&UIApplicationOpenSettingsURLString != NULL) {
                    
                    [self performSelector:@selector(openSettings) withObject:self afterDelay:0.5];
                }
            }
            else{
                [self.locationManager startUpdatingLocation];
            }
            
        }
    }
    
    
}

-(void)openSettings{
    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    
    [[UIApplication sharedApplication] openURL:appSettings];
}

-(void)setSalonURLAndCall{
    
   
    NSURL * url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",kTestAPI_URL,kSearch_Salons_URL]];
    NSString * params = [NSString string];
    
    
    if (self.instantBookOnly) {

       // params=[NSString stringWithFormat:@"tier=1"];
        
        params=[NSString stringWithFormat:@"tier_1=1"];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_2=1"]];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&tier_3=1"]];
        NSLog(@"New search features implemented");
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&page=%ld",(long)self.nearMePageCurrentPage]];
    }
    else if(!self.instantBookOnly){
        //params = [NSString stringWithFormat:@"tier=2"];
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&page=%ld",(long)self.directoryCurrentPage]];
        params = [params stringByAppendingString:@"&per_page=15"];
    }
    /*
    NSLog(@"Search String %@",self.discoverySearchString);
    if (self.discoverySearchString.length!=0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&search_string=%@",self.discoverySearchString]];
    }
    */
   
    
    if (self.hasDiscoveryLatLong) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%@&longitude=%@",self.discoveryLat,self.discoveryLon]];
    }
    else if(self.location.coordinate.latitude!=0 && self.location.coordinate.longitude!=0){
       params = [params stringByAppendingString:[NSString stringWithFormat:@"&latitude=%f&longitude=%f",self.location.coordinate.latitude,self.location.coordinate.longitude]];
    }
    
    if (self.filterDistance==0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%@",kSearchRadius]];
    }
    else{
        NSLog(@"Filter distance %d",self.filterDistance);
         params = [params stringByAppendingString:[NSString stringWithFormat:@"&radius=%d",self.filterDistance]];
    }
    
    
    NSString * accessToken =[[User getInstance] fetchAccessToken];
    if (accessToken!=nil) {
        params =[params stringByAppendingString:[NSString stringWithFormat:@"&user_id=%@",accessToken]];
    }
    
    if (self.selectedCategory!=0) {
        params = [params stringByAppendingString:[NSString stringWithFormat:@"&filter_category_id=%d",self.selectedCategory]];
    }

    [self.allSalonsModel load:url withParams:params AndWithSpinner:YES];
   
    
    
}

- (void)addNoDataMessage {
  
    
    [WSCore addNoSalonsPlaceholderForTableView:self.tableView];
    self.isNoSalonsLocationViewShowing=YES;
    
}


#pragma mark - AllSalons Delegate ie. refresh tableview
-(void) reloadSalonDataWithSalons:(NSMutableArray *)salons AndWithPageNumber:(NSInteger)pageNo{

    self.isLoadingSalons=NO;
    self.tableView.hidden=NO;
    
    [self.tableView reloadDataWithFade:YES];
    //[self.tableView reloadDataAnimated:YES];
    if (salons.count==0) {
        [self addNoDataMessage];
    }
    else{
        if (self.instantBookOnly) {
            
            [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

            self.allSalons=salons;
            self.totalNearMePageCount=pageNo;
        }else{
            [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];

            self.directoryArray=salons;
            self.totalDirectoryPageCount=pageNo;
        }
        
        self.tableView.backgroundView=nil;
        self.isNoSalonsLocationViewShowing=NO;
    }
    
    if (self.refreshControl) {
        [self.refreshControl endRefreshing];
    }
}

#pragma mark - FilterDelegate
-(void)filterSearchWithInstantBook : (BOOL) instant WithDistance: (int) distance AndWithCategory:(int)cat{
    NSLog(@"Filter Search now - from Delegate");
    
    NSLog(@"Is instant %d",instant);
    
    //self.tableView.contentOffset = CGPointMake(0, 0 - self.tableView.contentInset.top);
    self.instantBookOnly=instant;
    
    if (cat!=0) {
        self.selectedCategory=cat;
        NSLog(@"Selected category %d",self.selectedCategory);
    }
    //removed for intial submission
    /*
    NSLog(@"Delegate filter distance %d",distance);
    if (distance!=0) {
        self.filterDistance=distance;
    }
     */
    [self resetPageAndCall];
    
}

#pragma mark - UIViewControllerTransitioningDelegate




-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    //return new instance of custom transition
    return [[PresentDetailTransition alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return [[DismissDetailTransition alloc] init];
}


- (IBAction)searchVC:(id)sender {
    
    SearchSalonsTableViewController * searchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVC"];
    //searchVC.modalPresentationStyle = UIModalPresentationCustom;
   //searchVC.transitioningDelegate = self;
    //searchVC.delegate=self;
   //[self presentViewController:searchVC animated:YES completion:nil];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:searchVC animated:NO];
   
    
}

#pragma mark - WalkThroughDelegate
-(void)walkthroughIsDismissed{
   
    NSLog(@"Walkthrough is dimissed");
    
    [self.refreshControl beginRefreshing];
    [self getLatest];
    [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y-self.refreshControl.frame.size.height) animated:YES];
}

-(void)goToLoginSignUp{
    
    SettingsViewController * settings = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.0;
    transition.type = kCATransitionFade;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:settings animated:NO];
}

#pragma mark - SearchSalons Delegate
-(void)makeSearchBasedOnSearchTerm:(NSString *)searchTerm{
    
    if (searchTerm.length!=0) {
        self.searchString=searchTerm;
        [self.refreshControl beginRefreshing];
        [self getLatest];
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y-self.refreshControl.frame.size.height) animated:YES];
    }
    else{
        self.searchString=nil;
    }
    
}

-(void)goToPrefilledSearch{
    
    SearchSalonsTableViewController * searchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVC"];
    searchVC.modalPresentationStyle = UIModalPresentationCustom;
    searchVC.transitioningDelegate = self;
    searchVC.searchTerm=self.searchString;
    searchVC.delegate=self;
    [self presentViewController:searchVC animated:YES completion:nil];

    
}
 - (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

     if (self.searchString.length!=0) {
         
         UIView * sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 80)];
         sectionView.backgroundColor=[UIColor silverColor];
         UIView * view = [[UIView alloc] initWithFrame:CGRectMake(5, 2, self.tableView.frame.size.width-10, 60)];
         view.backgroundColor=[UIColor cloudsColor];
         view.layer.cornerRadius=3.0f;
        
         [sectionView addSubview:view];
         
         UILabel * titleLabel;
         if (self.searchString.length>20) {
             titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 200, 50)];
             titleLabel.numberOfLines=2;
         }else{
             titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, 200, 30)];
              titleLabel.adjustsFontSizeToFitWidth=YES;
         }
         
         titleLabel.text = [NSString stringWithFormat:@"Results For %@",self.searchString];
         titleLabel.font = [UIFont systemFontOfSize:14.0f];
         
         
         //added tap gesture
         UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToPrefilledSearch)];
         tap.numberOfTapsRequired=1;
         [titleLabel addGestureRecognizer:tap];
         titleLabel.userInteractionEnabled=YES;
        
         titleLabel.textColor = [UIColor silverColor];
         [view addSubview:titleLabel];
          titleLabel.center=CGPointMake(titleLabel.center.x, view.center.y);
         
         
         FUIButton * button = [[FUIButton alloc]init];
         button.frame = CGRectMake(view.frame.size.width-60, 15, 50, 30);
         [button setTitle:@"Reset" forState:UIControlStateNormal];
         button.titleLabel.font = [UIFont systemFontOfSize:12.0f];
         
         [view addSubview:button];
         [button addTarget:self action:@selector(clearHistory) forControlEvents:UIControlEventTouchUpInside];
         button.buttonColor = kWhatSalonBlue;
         button.shadowColor = kWhatSalonBlueShadow;
         button.shadowHeight = 1.0f;
         button.cornerRadius = 3.0f;
         [button setTitleColor:[UIColor cloudsColor] forState:UIControlStateNormal];
         [button setTitleColor:[UIColor cloudsColor] forState:UIControlStateHighlighted];
         return sectionView;
         
     }
     return nil;
 
 
 }
 
 -(void)clearHistory{
 
     NSLog(@"Clear history");
 
     self.searchString=nil;
    
     [self getLatest];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (self.searchString.length!=0) {
        return 64.0f;
        
    }
    return 0;
}

@end
